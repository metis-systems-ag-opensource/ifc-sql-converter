# Makefile
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2019, Metis AG
# License: MIT License
# 
#
# Makefile for the 'ifc-server' project

TOP = .
LOCAL_PATH = $(shell realpath -s --relative-base=. "$(TOP)")

BASH = $(shell command -v bash)
MORE = $(shell command -v more)

PREFIX = /usr/local

DIRS = Common EXPRESS STEP Utils
CLUPDIRS = cgen Tests
MF = --no-print-directory
TBIN = $(TOP)/bin
TTMPL = $(TOP)/Templates/static
BUTILS = $(TOP)/butils
CW = $(BASH) $(BUTILS)/cw.sh
XPINSTDIR = "$(shell echo ~/IFC)"
XPCONFDIR = "$(shell echo ~/.config/ifc)"

all: basedirs build_all to_bin
	@echo "The build process is now complete."
	@echo "Use '$(MAKE) help' for further instructions."

build_all: Common/libcommon.a EXPRESS/xpp STEP/stp Utils/ifc-conf

help:
	@$(MORE) doc/make_help

#plugin: EXPRESS/xpp

basedirs: cgen

cgen:
	@$(CW) "Creating \"$@\"" mkdir -p "$@"

Common/libcommon.a:
	@echo; echo "Building '$@' ..."
	@cd Common; $(MAKE) -j

EXPRESS/xpp:
	@echo; echo "Building '$@' ..."
	@cd EXPRESS; $(MAKE) -j

STEP/stp:
	@echo; echo "Building '$@' ..."
	@cd STEP; $(MAKE) -j

Utils/ifc-conf:
	@echo; echo "Building '$@' ..."
	@cd Utils; $(MAKE)

to_bin:
	@$(CW) -q "Installing programs in $(TBIN)" true
	@$(CW) -eq " ." mkdir -p "$(TBIN)"
	@$(CW) -e "." cp EXPRESS/xpp STEP/stp Utils/ifc-conf "$(TBIN)"
	@$(CW) -q "Installing templates in $(TTMPL)" true
	@$(CW) -eq " ." mkdir -p "$(TTMPL)"
	@$(CW) -q "." cp -a EXPRESS/Templates/static/*.cc "$(TTMPL)"
	@$(CW) -e "." cp -a EXPRESS/Templates/static/*.h "$(TTMPL)"

$(TOP)/bin/xpp $(TOP)/bin/xpp-conf $(TOP)/Templates \
$(TOP)/butils/gen-ifc.sh $(TOP)/conf/express.conf: to_bin

install-xpp: $(TOP)/bin/xpp $(TOP)/bin/xpp-conf $(TOP)/Templates \
	     $(TOP)/butils/gen-ifc.sh $(TOP)/conf/express.conf
	@$(BASH) butils/install.sh "$(PREFIX)" xpp

local_install-xpp: $(TOP)/bin/xpp $(TOP)/bin/ifc-conf $(TOP)/Templates \
		   $(TOP)/butils/gen-ifc.sh $(TOP)/conf/express.conf
	@$(BASH) butils/install.sh local xpp

install-stp: $(TOP)/bin/stp $(TOP)/bin/ifc-conf $(TOP)/conf/step.conf
	@$(BASH) butils/install.sh "$(PREFIX)"

local_install-stp: $(TOP)/bin/stp $(TOP)/bin/ifc-conf $(TOP)/conf/step.conf
	@$(BASH) butils/install.sh local

clean:
	@for d in $(DIRS); do $(MAKE) $(MF) -C "$$d" $@; done
	@echo -n "Emptying:"
	@for d in $(CLUPDIRS); do \
	    echo -n " '$$d'"; rm -rf "$$d"/*; \
	done; \
	echo " done."

cleanall: clean clean_plugins remove_bin remove_libtmpl remove_buildtmp

clean_plugins:
	@$(CW) "Emptying 'plugins'" rm -rf plugins/*

remove_bin:
	@$(CW) -q "Removing '$(TBIN)'" true
	@$(CW) -q " ." test ! -e "$(TBIN)" -o -d "$(TBIN)"
	@$(CW) -e "." rm -rf "$(TBIN)"

remove_libtmpl:
	@$(CW) -q "Removing $(TTMPL) tree" true
	@$(CW) -q " ." test ! -e "$(TTMPL)" -o -d "$(TTMPL)"
	@$(CW) -e "." rm -rf "$(TOP)/Templates"

remove_buildtmp:
	@$(CW) -q "Removing the '$(TOP)/dbschema' tree" true
	@$(CW) -q " -" test ! -e "$(TOP)/dbschema" -o -d "$(TOP)/dbschema"
	@$(CW) -e "." rm -rf "$(TOP)/dbschema"
	@$(CW) -q "Removing the '$(TOP)/plugins' tree" true
	@$(CW) -q " -" test ! -e "$(TOP)/plugins" -o -d "$(TOP)/plugins"
	@$(CW) -e "." rm -rf "$(TOP)/plugins"

.PHONY: to_bin help
