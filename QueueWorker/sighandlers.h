/* sighandlers.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Install signal handlers and export a 'sig_jmpbuf' structure.
**
*/
#ifndef SIGHANDLERS_H
#define SIGHANDLERS_H

#include <cstdbool>
#include <csetjmp>

/*! May be an alternative for polling for SIGHUP */
extern sigjmp_buf myjmpbuf;

/*! @returns `true` if `SIGHUP` was caught somewhere and `false`, otherwise. */
bool caught_hup (bool reset = true);

/*! @returns `true` if either `SIGINT` or `SIGHUP` was caught somewhere and
**           `false`, otherwise.
*/
bool caught_term (bool reset = true);

/*! ... */
bool child_terminated (bool reset = true);

/*! @returns `true` if SIGHUP is ignored (the signal handler does nothing
**          and even continues interrupted system calls).
*/
bool hup_ignored();

/*! Disables or enables the (polling?) capture of `SIGHUP` signals.
**
** @param `ignore` - disables a `SIGHUP` capture if `true`, and enables it
**                   if `false`.
*/
void ignore_hup (bool ignore = true);

/*! Installs signal handlers for `SIGCHLD`, `SIGHUP`, `SIGINT` and `SIGTERM`. */
void install_signals();

/*! Deactivates the four signals otherwise managed here ... */
void deactivate_signals();

#endif /*SIGHANDLERS_H*/
