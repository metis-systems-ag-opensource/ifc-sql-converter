# Was wird benötigt?

1. Ein Mechanismus, der es ermöglicht, eine Datei hochzuladen, ohne dass diese
   Datei gleich in die Queue gepackt wird, bevor sie fertig hochgeladen wurde.
   Dazu wird ein `incoming`-Verzeichnis benötigt, in dem noch nicht fertig
   hochgeladene Dateien angelegt werden.

2. Ein Warteschlangenmechanismus, d.h. eine Anordnung von Dateien nach
   Eingangszeitpunkt. Die dazu notwendige Kodierung kann über die Dateinamen
   erfolgen. Allerdings sollte auch ein Bezug auf den originalen Dateinamen
   enthalten sein. Das lässt sich machen, indem in der Warteschlange nur
   Beschreibungsdateien abgelegt werden, die den Verweis auf die jeweilige
   hochgeladene Datei enthalten (im `incoming`-Verzeichnis). Eine solche
   Beschreibingsdatei wird zunächst unter einem temporären Namen angelegt,
   damit das Programm zur Verwaltung der Warteschlange nicht außerplanmäßig
   darauf zugreift, während sie noch angelegt wird. Nachdem die
   Beschreibungsdatei vollständig ist, wird die nur noch umbenannt (diese
   Operation ist "atomar", so dass es zu keinen Konflikten kommen sollte).

3. Die Warteschlangenverwaltung sollte eine Liste der Beschreibungsdateien
   einlesen, und diese nacheinander abarbeiten. Sind keine Beschreibungsdateien
   vorhanden, dann legt sich die Warteschlangenverwaltung für einen vorher
   festgelegten Zeitpunkt schlafen und versucht es danach erneut.
   Fertige abgearbeitete Beschreibungen werden (zusammen mit den IFC-Dateien)
   gelöscht (bzw. umbenannt, wobei der Bearbeitungszustand in diesen
   gespeichert werden muss. Sobald die eingelesene Liste abgearbeitet wurde,
   wird erneut eine neue Liste der Beschreibungsdateien eingelesen.

4. Bearbeitungszustände: Nachdem eine Beschreibunngsdatei angelegt wurde,
   wird diese nur noch von der Warteschlangenverwaltung modofiziert. Allerdings
   kann sie (per REST) abgefragt werden. Der effizienteste Mechanismus läuft
   hier wieder per `rename()`: Der REST-Server fragt mittels des Dateinamens
   den aktuellen Bearbeitungszustand ab. Ändert sich dieser, so benennt die
   Warteschlangenverwaltung die Datei um, *nachdem* sie alle Informationen
   über den Bearbeitungszustand in dieser Datei abgelegt hat.

5. Der Name der Beschreibungsdateien sollte sich aus dem genauen Zeitpunkt
   ihres Anlegens ergeben, sowie einem Zustands-Tag:

     - INQUEUE - Die IFC-Datei wartet auf eine weitere Bearbeitung
     - WIP     - Die IFC-Datei wird gerade eingelesen
     - DONE    - Die IFC-Datei wurde erfolgreich eingelesen
     - WARN    - Die IFC-Datei wurde zwar eingelesen, hatte aber ein paar
                 Inkompatibilitäten zur IFC-Beschreibung (Soft-Errors)
     - FAILED  - Die IFC-Datei konnte wegen schwerer Fehler nicht eingelesen
                 werden.

   Das Namensformat der Dateien sollte wie folgt lauten:
   _Datum(Short ISO)_-_Uhrzeit(Short ISO)_._msec(4d)_._Bearbeitungszustand_

6. Der anlegende REST-Service sollte den ersten Teil (ohne den
   Bearbeitungszustand) als Tag auffassen, mit dem der Bearbeitungszustand der
   Datei abgefragt werden kann. Dieser Zustand sollte also beim `POST`-Request
   zurückgegeben werden.
