/* main.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Main progrm of the 'QueueWorker' program
**
*/

#include <csignal>
#include <cstdarg>
#include <cstdio>
#include <cstring>

#include <sysexits.h>

#include <iostream>

#include "Common/fnutil.h"

#include "readconf.h"
#include "redirectout.h"
#include "rstexcept.h"
#include "process_queue.h"
#include "sighandlers.h"

using std::cerr, std::endl;

#define DEFAULTCFG "ifcrest"

[[noreturn]] static void usage (const char *format, ...);

static const char *progname = "qworker";

static void get_progname (int argc, char *argv[])
{
    if (argc > 0) {
	progname = basename (*argv);
    }
}

int main (int argc, char *argv[])
{
    const char *cfgfile = nullptr, *logfile = nullptr;
    get_progname (argc, argv);

    // Scanning all options which have an argument generically (with an inner
    // loop). Required for this are two C arrays: one for the option names
    // (characters), and one which holds pointers to the corresponding option
    // values.
    char argopts[] = { 'c', 'l' };
    char const**argoptargs[] = { &cfgfile, &logfile };

    /* Short argument parsing ... */
    for (int ix = 1; ix < argc; ++ix) {
	char *arg = argv[ix];
	bool argoptfound = false;

	// Non-option arguments are explicitely forbidden here!
	if (*arg != '-') { usage ("No non-option arguments allowed"); }

	// The `-h` option causes a short usage message to be issued on
	// the standard output channel, and the (successful) termination
	// of the program.
	if (arg[1] == 'h') { usage (nullptr); }

	// Parsing all options which have arguments in an inner loop (using
	// `argopts[]` and `argoptargs[]`).
	for (size_t jx = 0; jx < sizeof(argopts); ++jx) {
	    char opt = argopts[jx];
	    char const **optargp = argoptargs[jx];
	    if (arg[1] == opt) {
		if (*optargp) { usage ("Ambiguous '-%c' option", opt); }
		if (arg[2]) {
		    *optargp = &arg[2];
		} else if (ix + 1 > argc) {
		    usage ("Missing argument for '-%c' option", opt);
		} else {
		    *optargp = argv[++ix];
		}
		argoptfound = true; break;
	    }
	}
	if (argoptfound) { continue; }
	usage ("Invalid option '%s'", arg);
    }

    if (! cfgfile) { cfgfile = DEFAULTCFG; }

    if (logfile) {
	// Redirecting stdout/cout and stderr/cerr zo thr logfile...
	redirectout (logfile);
    }

    for (;;) {
	Config cfg;
	try {
	    cfg = readconf (cfgfile);
	} catch (ConfigError &e) {
	    cerr << progname << " configuration error: " << e.what() << endl;
	    exit (1);
	}
	try {
	    process_queue (cfg);
	} catch (TerminateSig &ts) {
	    cerr << progname << " terminating ..." << endl;
	    break;
	} catch (RestartSig &rs) {
	    cerr << progname << " reloading ..." << endl;
	}
    }

    return 0;
}

[[noreturn]] static void usage (const char *format, ...)
{
    if (format) {
	va_list args;
	fprintf (stderr, "%s: ", progname);
	va_start (args, format); vfprintf (stderr, format, args); va_end (args);
	fputs ("\n", stderr);
	exit (EX_USAGE);
    }
    printf ("Usage: %s [-c configfile] [-l logfile]\n"
	    "       %s -h\n",
	    progname, progname);
    exit (0);
}
