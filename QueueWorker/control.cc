/* control.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Controling an externally executed program.
**
** ATTENTION! This module uses external code (`pstreams` from
** http://pstreams.sourceforge.net/). This code is stored in the sub-directory
** `PStreams`. In order to meet the license requirements of this project, this
** sub-directory contains the complete source code (including LICENSE file and
** AUTHORS list) of the latest version (1.0.3) of this project. This project
** is used, because mixing the i/o-mechanisms of C and C++ is difficult, and
** the `pstreams` project copes with the low-level UNIX pipe-i/o in a C++
** conforming way.
**
*/

#include <cstring>
#include <iostream>
#include <regex>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include "Common/config.h"
#include "Common/fifo.h"
#include "Common/sutil.h"

#include "PStreams/pstream.h"

#include "control.h"

using std::runtime_error, std::invalid_argument;
using std::cerr, std::cout, std::endl, std::flush;
using std::string, std::getline, std::operator""s, std::stoul, std::to_string;
using std::vector;

#define FATAL_PREFIX "FATAL!"
#define FATAL_PREFIX_LEN (sizeof (FATAL_PREFIX) - 1)
#define ERROR_PREFIX "Error:"
#define ERROR_PREFIX_LEN (sizeof (ERROR_PREFIX) - 1)
#define FILEID_PREFIX "FILEID="
#define FILEID_PREFIX_LEN (sizeof (FILEID_PREFIX) - 1)
#define MODELID_PREFIX "MODELID="
#define MODELID_PREFIX_LEN (sizeof (MODELID_PREFIX) - 1)
#define IID_PREFIX "#"
#define IID_PREFIX_LEN (sizeof (IID_PREFIX) - 1)

using Messages = Fifo<string>;

struct iidb {
    size_t instid, dbid;
};

struct Ivs {
    Messages msglist;
    string fileid;
    vector<iidb> iidblist;
    size_t modelid;
};

struct Ptab {
    const char *prefix;
    bool (*process) (Ivs &, const string &, int);
};

static string cdel (const char *pfx)
{
    size_t len = strlen (pfx);
    while (len > 0 && (pfx[len - 1] == ':' || pfx[len - 1] == '=')) { --len; }
    return string (pfx, len);
}

static bool process_error (Ivs &ivdata, const string &text, int lc)
{
    if (is_prefix (FATAL_PREFIX, text, true)) {
	ivdata.msglist.push (trim (text));
	// Catching a 'FATAL!' message means that the STEP-parser completely
	// failed. ONLY in this case, "true" is returned (indicating the
	// failure).
	return true;
    } else if (is_prefix (ERROR_PREFIX, text, true)) {
	ivdata.msglist.push (trim (text.substr (ERROR_PREFIX_LEN)));
    } else if (text.size() > 0 && (text[0] == ' ' || text[0] == '\t')) {
	string &lastmsg = ivdata.msglist.last();
	lastmsg.append (1, ' ');
	lastmsg.append (trim (text));
    }
    // In each other case, "false" is returned (indicating success).
    return false;
}

static bool process_fileid (Ivs &ivdata, const string &text, int lc)
{
    auto &fileid = ivdata.fileid;
    if (fileid.empty()) {
	fileid = trim (text.substr (FILEID_PREFIX_LEN));
    } else {
	cerr << "WARNING! << '" << cdel (FILEID_PREFIX) << "' already set!" <<
		endl << "Skipping line " << lc << "." << endl;
    }
    // This function _always_ succeeds
    return false;
}

static bool process_modelid (Ivs &ivdata, const string &text, int lc)
{
    auto &modelid = ivdata.modelid;
    if (ivdata.modelid == 0) {
	try {
	    modelid = stoul (trim (text.substr (MODELID_PREFIX_LEN)));
	} catch (invalid_argument &e) {
	    cerr << "WARNING! Invalid '" << MODELID_PREFIX << "'." <<
		    endl << "Skipping line " << lc << "." << endl;
	}
    } else {
	cerr << "WARNING! '" << cdel (MODELID_PREFIX) << "' already set!" <<
		endl << "Skipping line " << lc << "." << endl;
    }
    // This function _always_ succeeds
    return false;
}

static bool process_iidb (Ivs &ivdata, const string &text, int lc)
{
    try {
	size_t textlen = text.size();
	size_t iid, dbid, dx, nx = 0;

	iid = stoul (text.substr (1), &nx);
	if (nx == 0 || nx > textlen) { goto ERROR; }
	++nx; if (text[nx] != '=') { goto ERROR; }
	dbid = stoul (text.substr (++nx), &dx);
	if (dx == 0 || nx + dx > textlen) { goto ERROR; }
	ivdata.iidblist.push_back (iidb { iid, dbid });
    } catch (invalid_argument &e) {
	// Catching conversion errors
	goto ERROR;
    }
    // This function _always_ succeeds
    return false;
ERROR:
    cerr << "WARNING! Invalid instance-id/database-id association!" <<
	    endl << "Skipping line " << lc << "." << endl;
    // This function _always_ succeeds
    return false;
}

static struct Ptab ptab[] = {
    { IID_PREFIX, process_iidb },
    { ERROR_PREFIX, process_error },
    { FATAL_PREFIX, process_error },
    { FILEID_PREFIX, process_fileid },
    { MODELID_PREFIX, process_modelid },
    { nullptr, nullptr }
};
    
static bool process_inputline (Ivs &ivdata, const string &text, int lc)
{
    const char *pfx;
    for (unsigned ix = 0; (pfx = ptab[ix].prefix); ++ix) {
	if (is_prefix (pfx, text, true)) {
	    return ptab[ix].process (ivdata, text, lc);
	}
    }
    // No matching function found. Instead – if the line starts with BL or HT,
    // it is assumed to be a line continuation.
    if (text.size() > 0 && (text[0] == ' ' || text[0] == '\t')) {
	return process_error (ivdata, text, lc);
    } else {
	// Otherwise, a warning is written to the standard error channel, and
	// then the line completely ignored.
	cerr << "WARNING! Unknown data found in line " << lc << endl <<
		"Skipping this line." << endl;
	return false;
    }
}

void process_ifc (const string &prog, QueueItem &qit)
{
    bool failed = false;
    Ivs ivdata;
    redi::pstream::argv_type args { qit.ifcfile };
    redi::ipstream xprog (prog, args);

    if (! xprog.is_open()) {
	throw runtime_error ("Attempt to spawn \"" + prog + "\" failed");
    }

    int lc = 0;
    // 
    while (! xprog.eof()) {
	string xpline;
	getline (xprog, xpline); ++lc;
	if (process_inputline (ivdata, xpline, lc)) { failed = true; }
    }
    int ec = xprog.close();
    if (ec == 0 && ! failed) {
	qit.fileid = ivdata.fileid;
	qit.modelid = ivdata.modelid;
	qit.messages = std::move (ivdata.msglist);
    } else {
	string fail = "FATAL! '" + prog + "' failed (exit code: " +
		      to_string (ec) + ")";
	qit.fileid = "";
	qit.modelid = 0;
	qit.messages = Fifo<string> { std::move (fail) };
    }

    update_item (qit);
    change_itemstatus (qit, (ec == 0 ? ItemStatus::done : ItemStatus::failed));
}
