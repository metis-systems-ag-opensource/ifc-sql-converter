# Makefile
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# License: MIT License
#
# Makefile for the 'QueueWorker' project.
#
# 'QueueWorker' is a service process whose task is the processing of
# incoming IFC files. This is done by handling a (persistent) queue
# (a list of files which are ordered by their filenames¹⁾ which contain
#  informations about the IFC-files transferred to the server, and values
#  which are probably returned from the IFC database).
# Each queue item is processed, and removed from the queue afterwards.
#
# ¹⁾ The filenames consist of the creation date as a timestamp in ISO 8601
#    short format plus a four digit msec value + a processing state (as a
#    suffix). This one the one hand allows for a sort order by creation date,
#    and on the other hand a removal from the queue as atomic operation and
#    without losing the content 
#    from the queue is done by modifying this suffix

TOP = ..
COMMON = $(TOP)/Common
PSTREAMS = PStreams
DOCDIR = doxydoc
DOXYDIR = doxydoc
DOXYFILE = QueueWorker.doxygen
LOCALDOC = $(DOCDIR)/latex/refman.pdf

CC = cc
CPP = c++
STD = -std=c++17
LD = c++

DOXYGEN = $(shell which doxygen || echo 'echo "NOOP(doxygen not found):"')

PSFX =
CFLAGS = -Wall -g -I. -I$(TOP) -c
LDFLAGS = -g
LDLIBS = -L$(COMMON) -lcommon -lstdc++fs
MV = --no-print-directory

OBJS = control.o lockopen.o process_queue.o readconf.o readqueue.o \
       redirectout.o sighandlers.o writem.o main.o


DOC = $(DOCDIR)/queueworker-refman.pdf

TARGETS = qworker$(PSFX)

all: $(TARGETS)

%.o : %.cc
	$(CPP) $(STD) $(CFLAGS) $< -o $@

%.o : %.c
	$(CC) $(CFLAGS) $< -o $@

qworker$(PSFX): $(OBJS) $(COMMON)/libcommon.a
	$(LD) $(LDFLAGS) $(OBJS) $(LDLIBS) -o $@

$(COMMON)/libcommon.a:
	@cd $(COMMON); $(MAKE)

control.o: control.cc control.h readconf.h

lockopen.o: lockopen.c lockopen.h

process_queue.o: process_queue.cc process_queue.h control.h readqueue.h \
		 rstexcept.h sighandlers.h

readconf.o: readconf.cc readconf.h

readqueue.o: readqueue.cc readqueue.h writem.h

redirectout.o: redirectout.cc redirectout.h

sighandlers.o: sighandlers.cc sighandlers.h

writem.o: writem.cc writem.h readqueue.h lockopen.h

main.o: main.cc readconf.h redirectout.h rstexcept.h process_queue.h \
	sighandlers.h

clean:
	@echo "Cleaning up in $(shell pwd)"
	@# Standard output-, backup- and core-files
	@rm -f core a.out *.o *.bak *.bck *~ *.~ out errs
	@# Generated targets
	@rm -f $(TARGETS)
	@echo "Cleaning up docs (QueueWorker/$(DOXYDIR)) and logfiles"
	@rm -rf "$(DOXYDIR)" *.log

cleanall: clean
	@cd $(COMMON); $(MAKE) $(MF) clean
	@echo "Removing the generated documentation ($(DOC))"
	@rm -f $(DOC)
