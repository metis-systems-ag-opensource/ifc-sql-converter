/* lockopen.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** C function for open and locking a file (C style `FILE *`).
**
*/
#ifndef LOCKOPEN_H
#define LOCKOPEN_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

/* Using advisory locking here! */
FILE *lock_fopen (const char *filename, const char *mode);

/* flushes the output buffer to `fp`, then truncates `fp`
** at the current file position.
*/

int truncate_here (FILE *fp);

#ifdef __cplusplus
}
#endif

#endif /*LOCKOPEN_H*/
