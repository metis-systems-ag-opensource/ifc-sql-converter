/* process_queue.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Processing of the queue.
**
*/

#include <ctime>
#include <iostream>
#include <stdexcept>
#include <string>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "Common/fifo.h"
#include "Common/sutil.h"
#include "Common/sysutils.h"

#include "control.h"
#include "readqueue.h"
#include "rstexcept.h"
#include "sighandlers.h"

#include "process_queue.h"

/* Importing some names from `std` into the local namespace */
using std::cerr, std::cout, std::endl, std::flush;
using std::exception, std::invalid_argument, std::runtime_error;
using std::string, std::operator""s, std::stol, std::stoul;

/* Converting the configuration value `QWDELAY` into a `struct timespec` value
** to be used in the `nanosleep()` invocations in `process_topitem()` and
** `process_queue()`. If `QWDELAY` does not exist, or has an invalid value,
** `DEFAULT_DELAY` (defined in the header file) is used instead.
**
** @param `cfg` - the content of the configuration file.
*/
static struct timespec get_delay (Config &cfg)
{
    const string dlycfg = "QWDELAY";
    struct timespec res;
    if (cfg.has (dlycfg)) {
	try {
	    res = s2time (cfg[dlycfg]);
	} catch (invalid_argument &e) {
	    cerr << "WARNING! Invalid '" << dlycfg <<
		    "' value. Using the default." << endl;
	    res = s2time (DEFAULT_DELAY);
	}
    } else {
	res = s2time (DEFAULT_DELAY);
    }
    return res;
}

/* Processing the top queue item, removing it from the queue by the way
**
**  @param `q` - the current queue (...)
**  @param `prog` - the pathname of the program which is used for processing
**                  the data referenced by the queue item
**
**  @returns `false` if the queue was empty (need a re-read), and `true` after
**           the top queue item was processed.
*/
static bool process_topitem (Queue &q, const string &prog, struct timespec dly)
{
    // Need to re-read the queue if it is empty
    if (q.empty()) { return false; }
    QueueItem qit = q.shift();
    /* Doing this in a sub-process ... but why?
    ** Because the sub-process should not be interrupted, even if the main
    ** process is ...
    */
    pid_t pid = fork();
    switch (pid) {
	case -1: /*ERROR*/
	    if (caught_hup()) { throw RestartSig(); }
	    if (caught_term()) { throw TerminateSig(); }
	    q.unshift (qit); nanosleep (&dly, nullptr);
	    break;
	case 0:  /*CHILD*/
	    deactivate_signals();
	    change_itemstatus (qit, ItemStatus::wip);
	    try {
		process_ifc (prog, qit);
		exit (0);
	    } catch (exception &e) {
		change_itemstatus (qit, ItemStatus::failed);
		exit (1);
	    }
	default: /*PARENT*/ {
	    // Do nothing here, because all actions take place in the child
	    // process, and the child exit status is caught with a signal
	    // handler.
	    // 'pause()' cannot be used here, because the SIGCHLD handler
	    // restarts an interrupted system call, which would lead to
	    // continuing the "pause" and 'process_topitem()' would never
	    // return after the child terminates. Instead, a form of
	    // "busy waiting" will be used here, which checks if the child
	    // had terminated.
	    //int pstat;
	    //waitpid (pid, &pstat, 0);
	    
	    while (! child_terminated()) {
		if (caught_hup()) { throw RestartSig(); }
		if (caught_term()) { throw TerminateSig(); }
		sleep (1);
	    }
	}
    }
    return true;
}

/*! Process the queue.
**
**  @param `cfg` - the content of the configuration file for this program.
*/
void process_queue (Config &cfg)
{
    Queue q;
    struct timespec delay = get_delay (cfg);
    for (;;) {
	if (caught_hup()) { throw RestartSig(); }
	if (caught_term()) { throw TerminateSig(); }
	if (! process_topitem (q, cfg["PROCESSOR"], delay)) {
	    q = readqueue (cfg["WORKDIR"], ItemStatus::inQueue);
	    if (q.empty()) {
		// Sleep some time
		struct timespec wn = delay;
		if (caught_hup()) { throw RestartSig(); }
		if (caught_term()) { throw TerminateSig(); }
		nanosleep (&wn, nullptr);
	    }
	}
    }
}
