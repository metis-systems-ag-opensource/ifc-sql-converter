/* sighandlers.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Install signal handlers and export a 'sig_jmpbuf' structure.
**
*/

#include <csignal>
#include <sys/types.h>
#include <sys/wait.h>

#include "sighandlers.h"

sigjmp_buf myjmpbuf;

static bool ignore_hup_ = false;
static bool caught_hup_ = false;
static bool caught_term_ = false;
static bool child_terminated_ = false;

static void handle_sigchld (int sig)
{
    int wstatus;
    (void) wait (&wstatus);
    child_terminated_ = true;
    /*##was: pid_t childpid = wait (&wstatus);*/
}

static void handle_sighup (int sig)
{
    if (! ignore_hup_) {
	caught_hup_ = true;
    }
}

static void handle_term (int sig)
{
    caught_term_ = true;
}

bool caught_hup (bool reset)
{
    bool res = caught_hup_;
    if (reset) { caught_hup_ = false; }
    return res;
}

bool caught_term (bool reset)
{
    bool res = caught_term_;
    if (reset) { caught_term_ = false; }
    return res;
}

/*! ... */
bool child_terminated (bool reset)
{
    bool res = child_terminated_;
    if (reset) { child_terminated_ = false; }
    return res;
}

bool hup_ignored()
{
    return ignore_hup_;
}

void ignore_hup (bool ignore)
{
    ignore_hup_ = ignore;
}

void install_signals()
{
    ignore_hup_ = true;
    caught_hup_ = false;
    caught_term_ = false;
    struct sigaction ign_action, hup_action, chld_action, term_action;

    /* Install the "ignore" signal handlers for `SIGUSR1` and `SIGUSR2`. */
    ign_action.sa_handler = SIG_IGN;
    sigemptyset (&ign_action.sa_mask);
    ign_action.sa_flags = 0;
    sigaction (SIGUSR1, &ign_action, nullptr);
    sigaction (SIGUSR2, &ign_action, nullptr);

    /* Install the `SIGHUP` handler */
    hup_action.sa_handler = handle_sighup;
    sigemptyset (&hup_action.sa_mask);
    hup_action.sa_flags = SA_RESTART;
    sigaction (SIGHUP, &hup_action, nullptr);

    /* Install the `SIGCHLD` handler */
    chld_action.sa_handler = handle_sigchld;
    sigemptyset (&chld_action.sa_mask);
    sigaddset (&chld_action.sa_mask, SIGHUP);
    sigaddset (&chld_action.sa_mask, SIGTERM);
    sigaddset (&chld_action.sa_mask, SIGINT);
    chld_action.sa_flags = SA_RESTART;
    sigaction (SIGCHLD, &chld_action, nullptr);

    /* Install the `SIGINT` handler */
    term_action.sa_handler = handle_term;
    sigemptyset (&term_action.sa_mask);
    sigaddset (&term_action.sa_mask, SIGHUP);
    sigaddset (&term_action.sa_mask, SIGTERM);
    term_action.sa_flags = 0;
    sigaction (SIGINT, &term_action, nullptr);

    /* Install the `SIGTERM` handler */
    term_action.sa_handler = handle_term;
    sigemptyset (&term_action.sa_mask);
    sigaddset (&term_action.sa_mask, SIGHUP);
    sigaddset (&term_action.sa_mask, SIGINT);
    term_action.sa_flags = 0;
    sigaction (SIGTERM, &term_action, nullptr);
}

/*! Deactivates the four signals otherwise managed here ... */
void deactivate_signals()
{
    struct sigaction deactivate_sig;
    int signals[] = { SIGINT, SIGTERM, SIGHUP, SIGCHLD };
    for (auto sig: signals) {
	deactivate_sig.sa_handler = SIG_IGN;
	sigemptyset (&deactivate_sig.sa_mask);
	deactivate_sig.sa_flags = 0;
	sigaction (sig, &deactivate_sig, nullptr);
    }
}
