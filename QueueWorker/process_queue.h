/* process_queue.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface part of `process_queue.{cc,h}`.
** (Processing of the queue.)
**
*/
#ifndef PROCESS_ITEM_H
#define PROCESS_ITEM_H

#include "Common/config.h"

/* Default delay - used if the configuration item `QWDELAY` is either not
** present, or has an invalid value.
*/
#define DEFAULT_DELAY "5.3"

/*! Processing of the queue.
**
**  @param `cfg` - the content of the configuration file of this program.
*/
void process_queue (Config &cfg);

#endif /*PROCESS_ITEM_H*/
