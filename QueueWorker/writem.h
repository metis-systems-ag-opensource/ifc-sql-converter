/* writem.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface part of `writem.{cc,h}`
** (Low-level (system dependent) backend of `update_item()` from
**  `readqueue.{cc,h}`)
**
*/
#ifndef WRITEM_H
#define WRITEM_H

#include "readqueue.h"

void write_itemdata (const QueueItem &qit, const char *filename);

#endif /*WRITEM_H*/
