/* readqueue.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file for `readqueue.cc`
** (Read the descriptions of files from a "queue" directiory)
**
*/
#ifndef READQUEUE_H
#define READQUEUE_H

#include <string>
#include <vector>

#include "Common/fifo.h"

/*! The valid item status' internal representation. */
enum class ItemStatus : uint8_t {
    err = 0,
    inQueue = 1,
    wip = 2,
    done = 4,
    failed = 8,
    processed = done | failed,
    all = inQueue | wip | done | failed,
};

/*! POD structure containing the complete status of a `QueueItem`. */
struct QueueItem {
    std::string filename, ifcfile, fileid;
    Fifo<std::string> messages;
    size_t modelid;
    ItemStatus status;
};

using Queue = Fifo<QueueItem>;

/*! Reads a `QueueItem` file with the given path `fpath`.
**
**  @param `fpath` - the pathname of the `QueueItem` file.
**
**  @returns the internal representation of the `QueueItem` file.
*/
QueueItem readstatus (const std::string &fpath);

/*! Reads all `QueueItem` values (files) of a given `ItemStatus`.
**
**  @param `dpath` - the directory which holds the persistent version of the
**                   queue.
**  @param `s` - Filter all elements which don't have the given `ItemStatus`.
**
**  @returns the internal representation of the queue which is used for
**           further processing.
*/
Queue readqueue (const std::string &dpath,
		 const ItemStatus &s = ItemStatus::inQueue);

/*! Changes the `ItemStatus` of a `QueueItem`.
**
**  @param `qit` - the queue item whose status is to be changed.
**  @param `newstat` - the new item status.
**
**  @returns the old status of the item.
*/
ItemStatus change_itemstatus (QueueItem &qit, ItemStatus newstat);

/*! Stores the in-memory version of the `QueueItem` in the corresponding
**  file.
**
**  @param `qit` - the queue item whose status is to be stored.
*/
void update_item (QueueItem &qit);

#endif /*READQUEUE_H*/
