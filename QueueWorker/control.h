/* control.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface part of `control.{cc,h}`
** (Controling an externally executed program.)
**
** ATTENTION! This module uses external code (`pstreams` from
** http://pstreams.sourceforge.net/). This code is stored in the sub-directory
** `PStreams`. In order to meet the license requirements of this project, this
** sub-directory contains the complete source code (including LICENSE file and
** AUTHORS list) of the latest version (1.0.3) of this project. This project
** is used, because mixing the i/o-mechanisms of C and C++ is difficult, and
** the `pstreams` project copes with the low-level UNIX pipe-i/o in a C++
** conforming way.
**
*/
#ifndef CONTROL_H
#define CONTROL_H

#include <string>

#include "readqueue.h"

void process_ifc (const std::string &prog, QueueItem &qit);

#endif /*CONTROL_H*/
