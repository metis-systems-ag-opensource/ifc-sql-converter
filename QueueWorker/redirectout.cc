/* redirectout.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Redirecting stdout/cout and stderr/cerr (irreversibly) by replacing the
** underlying file descriptors ...
**
** ATTENTION! This is a UNIX specific module. Don't assume that it works
** under some other operating system. It was merely written, because
** the program using it uses the C standard i/o as well as the C++ stream i/o,
** and there is currently no mechanism in for redirecting both output standard
** channels at the same time.
**
*/

#include <cerrno>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <iostream>
#include <stdexcept>

#include "redirectout.h"

using std::cerr, std::cout, std::flush;
using std::runtime_error;
using std::string, std::operator""s;

void redirectout (const string &file)
{
    const int stdout_fd = fileno (stdout), stderr_fd = fileno (stderr);
    int ec, fd = open (file.c_str(), O_CREAT|O_APPEND|O_WRONLY, 0755);
    if (fd < 0) {
	throw runtime_error ("Attempt to open '" + file + "' failed - " +
			     strerror (errno));
    }
    cout << flush;
    if (dup2 (fd, stdout_fd) < 0) {
	ec = errno; close (fd); errno = ec;
	throw runtime_error
	    ("Attempt to redirect stdout to '" + file + "' failed - " +
	     strerror (errno));
    }
    cerr << flush;
    if (dup2 (fd, stderr_fd) < 0) {
	ec = errno; close (fd); errno = ec;
	throw runtime_error
	    ("Attempt to redirect stderr to '" + file + "' failed - " +
	     strerror (errno));
    }

    /* 'fd' is no longer required! */
    close (fd);
}
