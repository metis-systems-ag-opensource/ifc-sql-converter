/* readconf.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file got 'readconf.cc'
** (Read and check the configuration)
**
*/
#ifndef READCONF_H
#define READCONF_H

#include <string>
#include <stdexcept>

#include "Common/config.h"

class ConfigError : public std::runtime_error {
public:
    ConfigError (const std::string &msg)
        : std::runtime_error (msg)
    { }
    std::string what() {
	using std::operator""s;
	return "Configuration ERROR: "s + std::runtime_error::what();
    }
};

Config readconf (const std::string &cfgfile);

#endif /*READCONF_H*/
