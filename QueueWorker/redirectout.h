/* redirectout.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface part of `redirectout.{cc,h}`
** (Redirecting stdout/cout and stderr/cerr (irreversibly) by replacing the
**  underlying file descriptors ...)
**
** ATTENTION! This is a UNIX specific module. Don't assume that it works
** under some other operating system. It was merely written, because
** the program using it uses the C standard i/o as well as the C++ stream i/o,
** and there is currently no mechanism in for redirecting both output standard
** channels at the same time.
**
*/
#ifndef REDIRECTOUT_H
#define REDIRECTOUT_H

#include <string>

void redirectout (const std::string &file);

#endif /*REDIRECTOUT_H*/
