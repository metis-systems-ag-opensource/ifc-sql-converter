/* rstexcept.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Special exception types for forcing a restart or a termination ...
**
*/
#ifndef RSTEXCEPT_H
#define RSTEXCEPT_H

class RestartSig {
public:
    RestartSig() { }
};

class TerminateSig {
public:
    TerminateSig() { }
};

#endif /*RSTEXCEPT_H*/
