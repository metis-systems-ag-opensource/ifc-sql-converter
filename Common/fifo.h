/*! \file fifo.h
**
** \brief A *First in first out* list type realised as a C++ class template.
**
** This type is a container type i used for learning to program and use
** the C++ class template types. Nonetheless, this template type is used
** in many places within 'Common', 'EXPRESS' and 'STEP'. It is meant as
** memory efficient alternative to the standard C++ (double linked) list types.
**
** The second reason for implementing this type is the fact, that it is rather
** difficult to create a real first in/first out type with the standard
** template types, so this alternative – even if not completely standards
** compliant – was the better alternative.
**
** I'm even now in the process of learning C++, so (maybe) in the future i will
** probably replace some parts of this type (especially the iterator sub-
** classes).
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2017, Metis AG
** License: MIT License
**
** Template-type implementing a FIFO (first-in-first-out) list. The main reason
** for implementing this list type is the fact that in EBNF rules, lists of
** elements are this way. The standard library's template types don't make the
** implementation of *first in/first out* lists easy.
**
** A second reason is the fact that implementing this template type helped me
** to (far better) understand the template mechanism of C++.
**
** I named the operations for appending/prepending elements after the similar
** operations in the **Perl** programming language: `push` and `unshift`; also,
** the operations for removing elements from the beginning/end of a `Fifo` list
** are named `shift` and `pop`. Last not least, this template type has a
** conversion operator (=> `bool`) which allows objects of this type to be
** used in conditions of `if` statements and `while` or `for` loops.
**
*/
#ifndef FIFO_H
#define FIFO_H

#include <cstddef>
//#include <cstdio>

#include <string>
#include <stdexcept>
#include <utility>

/*! The template type currently uses no special allocators, because i currently
**  don't understand this mechanism completely. So, the only template parameter
**  is the type of the elements of the Fifo.
*/
template <class T> class Fifo {
    //! Internally, a `Fifo` is a single linked list with a pointer to the
    //! first element and one to the last element.
    struct Item {
	Item *next; T value;
	Item () : next(nullptr) { }
	Item (const T &val) : next(nullptr), value(val) { }
	Item (T &&val) : next(nullptr), value(std::move(val)) { }
	Item (T &val) : next(nullptr), value(val) { }
	Item (const Item &x) : next(x.next), value(x.value) { }
	Item (Item &&x) : next(x.next), value(std::move(x.value)) { }
    };
    //! First/Last pointers
    Item *lfirst, *llast;
    //! As an optimisation, the `length` (aka `size`) of the `Fifo` object is
    //! stored as a `size_t` (unsigned) integer value.
    size_t llength;

    /*! The only purpose for the following (internal) member function is
    **  the test for a non-emptiness of the `Fifo` object.
    */
    void check_not_empty (const std::string &s) {
	if (!lfirst) {
	    throw std::runtime_error ("Fifo::" + s + ": Empty fifo");
	}
    }

    /* Because copying of a `Fifo` content is used more than once, i added
    ** an (internal) member function which does the heavy work in these cases.
    */
    inline void copy (Fifo &dst, const Fifo &src) {
	dst.lfirst = dst.llast = nullptr;
	llength = 0;
	for (Item *oit = src.lfirst; oit; oit = oit->next) {
	    Item *nit = new Item (oit->value);
	    if (!lfirst) { dst.lfirst = nit; } else { dst.llast->next = nit; }
	    dst.llast = nit; ++llength;
	}
    }

public:

    // The Iterator-class is declared/defined here, which makes it part of the
    // namespace of the ‘Fifo’-lists. A ‘Fifo’-iterator is declared somewhat
    // like:
    //   Fifo<`some type´>::Iterator iterator ...
    // It mostly works like the Iterators for the list-types of the ‘standard
    // template library’, but there is no comparison with a non-existing item
    // for finding the end of the list. Instead, the function ‘atend()’ exists,
    // which returns true if the iterator reached the end of the ‘Fifo’ and
    // false otherwise. Additionally, there is a function ‘reset()’, which sets
    // the iterator again to the first element of the ‘Fifo’.
    // Attention! Elements of this iterator-class are not immune against
    // modifications in the list itself (removal of the first or last element).
    /*! Iterator class for *mutable* `Fifo` objects. */
    class Iterator {
	Fifo *_base;
	Item *_current, *_previous;

	// Helper function which does the real work behind both '++' operators
	Iterator &advance () {
	    if (!_current) {
		throw std::runtime_error ("Fifo::Iterator::++: No more items");
	    }
	    _previous = _current;
	    _current = _previous->next;
	    return *this;
	}

	// Helper function which does the real work behind both 'insert()'
	// (public) member functions
	void insert_helper (Item *item) {
	    if (_base->lfirst == nullptr) { _current = _previous = nullptr; }
	    if (_current == _base->lfirst) {
		item->next = _base->lfirst;
		if (! _base->lfirst) { _base->llast = item; }
		_base->lfirst = item;
		_current = item; _previous = nullptr;
	    } else {
		item->next = _current;
		_previous->next = item; _current = item;
	    }
	    ++_base->llength;
	}

    public:

	//! Default constructor
	Iterator () : _base(nullptr), _current(nullptr), _previous(nullptr) { }

	//! "Value" constructor which gets a pointer to a `Fifo` object as
	//! first argument.
	Iterator (Fifo<T> *x, bool toEnd = false)
	    : _base(x), _current(toEnd ? nullptr : _base->lfirst),
	      _previous(toEnd ? _base->llast : nullptr)
	{ }

	//! Copying of an iterator is explicitely allowed during an
	// initialization (e.g. when supplied as value-parameter in a
	// function-invocation) ...
	Iterator (const Iterator &x)
	    : _base(x._base), _current(x._current), _previous(x._previous)
	{ }

	//! Moving of an iterator makes sense, if it is used (e.g.) in an
	//! expression of the `Fifo::Iterator` type.
	//! The Move-constructor must remove the '_base' and '_current'-values
	// from its input argument 'x' ...
	Iterator (Iterator &&x)
	    : _base(x._base), _current(x._current), _previous(x._previous)
	{
	    x._base = nullptr; x._current = x._previous = nullptr;
	}

	//! Copy assignment (a lot like the 'Copy constructor').
	Iterator & operator= (const Iterator &x) {
	    _base = x._base; _current = x._current; _previous = x._previous;
	    return *this;
	}

	//! Move assignment (a lot like the 'Move constructor').
	Iterator & operator= (Iterator &&x) {
	    _base = x._base; _current = x._current; _previous = x._previous;
	    x._base = nullptr; x._current = x._previous = nullptr;
	    return *this;
	}

	//! Destroying of an iterator (does not much) ...
	~Iterator () { _base = nullptr; _current = _previous = nullptr; }


	//! Reset the iterator to the first element of the Fifo it is attached
	//! to ...
	void reset (Fifo<T> *b = nullptr) {
	    if (b) { _base = b; }
	    _current = _base->lfirst; _previous = nullptr;
	}

	//! Advance the iterator (prefix versions) ...
	Iterator &operator++() { return advance (); }
	//! Advance the iterator (postfix versions) ...
	Iterator &operator++(int) { return advance (); }

	/*! Remove the element at the current iterator position from the list.
	** If this was the first or last element in the list, the corresponding
	** pointers (lfirst, llast) in the Fifo pointed to are adjusted
	** accordingly ...
	*/
	void remove () {
	    if (_current) {
		if (_current != _base->lfirst) {
		    // _current points to the first element of the Fifo<T>...
		    // Remove the current element from the list and then
		    // 'delete' it ...
		    _previous->next = _current->next;
		    _current->next = nullptr; delete _current;
		    if (!(_current = _previous->next)) {
			_base->llast = _previous;
		    }
		    --_base->llength;
		} else if (! _base->lfirst) {
		    // The Fifo<T> is empty! This means that the iterator also
		    // is "empty", meaning: it points to no element in the
		    // Fifo<T>, but points to the fifo itself nonetheless.
		    _base->llast = nullptr;
		    _current = _previous = nullptr;
		    _base->llength = 0;
		} else {
		    // The list is not empty!
		    _current = _base->lfirst;
		    if ((_base->lfirst = _current->next)) {
			--_base->llength;
		    } else {
			// The list is now empty!
			 _base->llast = nullptr; _base->llength = 0;
		    }
		    // Delete the (previously) current element
		    _current->next = nullptr; delete _current;
		    _current = _base->lfirst;
		    _previous = nullptr;
		}
	    }
	}

	/*! Insert a new item at the current position.
	**  If the Fifo is empty, this means that the iterator is reset to
	**  its beginning (which is identical to its end).
	**  After the insertion, the iterator points to the inserted item.
	*/
	bool insert (const T &value) {
	    Item *newit = new Item (value);
	    if (! newit) { return false; }
	    insert_helper (newit);
	    return true;
	}

	/*! Insert a new item at the current position.
	**  This operation is very similar to `insert (const T&)`. except that
	**  it is assumed that the original value being inserted is no longer
	**  used after the insertion. Even if C++ doesn't support this directly
	**  (in the current versions), the use of this operation can be
	**  enforced by using the `std::move()` conversion operation.
	*/
	bool insert (T &&value) {
	    Item *newit = new Item (std::move (value));
	    if (! newit) { return false; }
	    insert_helper (newit);
	    return true;
	}

	/*! Remove all elements beginning with the current iterator position
	**  from the current `Fifo` and return these elements as a new `Fifo`
	**  object. (This is more like a 'splitting' operation, even if the
	**  elements are really removed from the `Fifo` object.)
	*/
	Fifo<T> truncate () {
	    if (! _base) {
		throw std::runtime_error
		    ("Iterator is not attached to a Fifo");
	    }
	    Fifo<T> res;
	    if (_current) {
		res.lfirst = _current;
		res.llast = _base->llast;
		if ((_base->llast = _previous)) {
		    _base->llast->next = nullptr;
		} else {
		    // Was at the beginning of the Fifo, the iterator
		    // references. This means that the original Fifo is empty
		    // afterwards.
		    _base->lfirst = nullptr;
		}
		// Now, fix the lengths of both Fifos and reset the iterator to
		// the beginning of the original Fifo ...
		reset(); _base->recount(); res.recount();
	    }
	    return res;
	}

	/*! The special feature of this iterator class is the fact, that one
	**  doesn't need to create an iterator on the fly for testing if the
	**  end of a `Fifo` was reached.
	**  Instead, this member function returns ‘true’ at the end of a ‘Fifo’
	**  and false otherwise.
	*/
	bool atend () { return _current == nullptr; }

	/*! Return (a reference to) the element's value the iterator currently
	**  points to.
	*/
	T & operator* () { return _current->value; }

	/*! Return a pointer to the element's value the iterator currently
	**  points to.
	*/
	T * operator-> () { return &_current->value; }

	/*! The `!=` operator was added later, in order to allow the standard
	**  operation for testing if the end of the `Fifo` was reached. This
	**  operation allows for `Fifo<T>` values being used in an iterative
	**  loop over all elements, like `for (auto x: Fifo<T>-value) { ... }`.
	*/
	bool operator!= (const Iterator &r) {
	    return (_base != r._base) || (_current != r._current);
	}

	/*! The `==` operator is simply the complement of the `==` operator,
	**  but is implemented directly here.
	*/
	bool operator== (const Iterator &r) {
	    return (_base == r._base) && (_current == r._current);
	}
    };

    /*! Because the standard uses lower case names, i later added a type
    **  alias for the `Iterator` class.
    */
    using iterator = Iterator;

    /*! The `ConstIterator` class is required for cases, where a
    **  `const Fifo<T> &` is used (e.g. when using `Fifo<T>` values as
    **  immutable function parameters. It contains does most of the operations
    **  the `Iterator` class has, with the exception of operations which would
    **  modify the underlying `Fifo<T>` object. Additionally, the `*` and `->`
    **  operators return `const` references and pointers.
    **  These modifications make this class usable in places where iterators
    **  on `const Fifo<T> &` object references (or `const Fifo<T> *` pointers)
    **  are used, e.g. in `for (auto v: *const-fifo-object*) { ... }` and the
    **  like.
    */
    class ConstIterator {
	const Fifo *_base;
	const Item *_current;
	ConstIterator &advance () {
	    if (!_current) {
		throw std::runtime_error
		    ("Fifo::ConstIterator::++: No more items");
	    }
	    _current = _current->next;
	    return *this;
	}

    public:
	//! Default constructor
	ConstIterator() : _base(nullptr), _current(nullptr) { }

	//! Value constructor
	ConstIterator (const Fifo<T> *x, bool toEnd = false)
	    : _base(x), _current(toEnd ? nullptr : _base->lfirst)
	{ }

	//! Copying constructor.
	ConstIterator (const ConstIterator &x)
	    : _base(x._base), _current(x._current)
	{ }

	//! Move constructor.
	ConstIterator (ConstIterator &&x)
	    : _base(x._base), _current(x._current)
	{
	    x._base = nullptr; x._current = nullptr;
	}

	//! Copy assignment.
	ConstIterator & operator= (const ConstIterator &x) {
	    _base = x._base; _current = x._current; return *this;
	}

	//! Move assignment.
	ConstIterator & operator= (ConstIterator &&x) {
	    _base = x._base; _current = x._current;
	    x._base = nullptr; x._current = nullptr;
	    return *this;
	}

	//! Destructor.
	~ConstIterator () { _base = nullptr; _current = nullptr; }


	/*! Reset the iterator to the first element of the Fifo it is attached
	**  to ...
	*/
	void reset (Fifo<T> *b = nullptr) {
	    if (b) { _base = b; }
	    _current = _base->lfirst;
	}

	//! Advance the iterator (prefix version).
	ConstIterator &operator++() { return advance (); }

	//! Advance the iterator (postfix version).
	ConstIterator &operator++(int) { return advance (); }

	/*! Fast test for the end of the underlying `Fifo`. This is the same
	**  as `Fifo::Iterator::atend()`.
	*/
	bool atend () { return _current == nullptr; }

	/*! Returns a reference to the element's value the iterator currently
	**  points to.
	*/
	const T & operator* () const { return _current->value; }

	/*! Returns a pointer to the element's value the iterator currently
	**  points to.
	*/
	const T * operator-> () const { return &_current->value; }

	/*! This function was implemented for the same reason
	**  `Iterator::operator!=()` was implemented (using it in an iteration
	**  `for` loop).
	*/
	bool operator!= (const ConstIterator &r) {
	    return (_base != r._base) || (_current != r._current);
	}

	/*! Same function as `Iterator::operator==()`. */
	bool operator== (const ConstIterator &r) {
	    return (_base == r._base) && (_current == r._current);
	}
    };

    /*! The C++ standard uses the name `const_iterator`, so i introduced a type
    **  alias with this name for `ConstIterator` here.
    */
    using const_iterator = ConstIterator;

    // Default constructor of `Fifo<T>`.
    Fifo () {
	lfirst = llast = nullptr; llength = 0;
    }

    /*! A value constructor for creating a single element `Fifo<T>`. This is
    **  especially useful in something like the actions of parser rules (Yacc),
    **  where results consisting of exactly one element are required ... like
    **  lists which must not be empty.
    */
    Fifo (const T &single) {
	Item *newitem = new Item (single);
	lfirst = llast = newitem; llength = 1;
    }

    /*! A value constructor for one-element lists, but in this case with an
    **  argument which is expected to be thrown away after constructing the
    **  `Fifo<T>` object.
    */
    Fifo (T &&single) {
	Item *newitem = new Item (std::move(single));
	lfirst = llast = newitem; llength = 1;
    }

    /*! Copy constructor.
    **  This constructor creates a complete copy of the list of values in ‘x’.
    */
    Fifo (const Fifo<T> &x) {
	copy (*this, x);
    }

    /*! Move constructor.
    **  This constructor copies the internal list pointers (`lfirst`, `llast`)
    **  and the current size (`llength`) from the parameter object `x` and then
    **  invalidates these member variables in this parameter object.
    **  This constructor is used in situations where `Fifo<T>` values are
    **  created on the fly, like in expressions.
    */
    Fifo (Fifo<T> &&x)
	: lfirst(x.lfirst), llast(x.llast), llength(x.llength)
    {
	x.lfirst = nullptr; x.llast = nullptr; x.llength = 0;
    }

    /*! A value constructor which uses the C++11 feature
    **  ‘std::initializer_list’. This constructor makes the creation of
    **  `Fifo<T>` objects on the fly a lot easier, e.g.:
    **     Fifo<int> x {1, 3, 5, 7, 9};
    */
    Fifo (std::initializer_list<T> vl)
	: lfirst(nullptr), llast(nullptr), llength(0)
    {
	for (auto it = vl.begin(); it != vl.end(); ++it) {
	    Item *newitem = new Item ((*it));
	    if (lfirst) { llast->next = newitem; } else { lfirst = newitem; }
	    llast = newitem; ++llength;
	}
    }

    /*! Removes each value from the `Fifo<T>` object, leaving it empty as
    **  result.
    */
    void clear () {
	Item *item;
	while (lfirst) {
	    item = lfirst->next; lfirst->next = nullptr;
	    delete lfirst; lfirst = item;
	}
	llast = nullptr; llength = 0;
    }

    /*! Destructor. Internally it simply invokes ‘Fifo<T>::clear()’.
    */
    ~Fifo () {
	clear ();
    }

    //! Copy assignment. Uses ‘clear()’ and (the hidden) ‘copy()’ ...
    Fifo & operator= (const Fifo<T> &x) {
	clear ();
	copy (*this, x);
	return *this;
    }

    /*! Move assignment. This operator is used in places where a `Fifo<T>`
    **  object is expected to be thrown away after the assignment, like in
    **  `Fifo<T> { ... }` constructs. Like the move constructor, it copies
    **  the internal pointers (`lfirst`, `llast`) and size (`llength`) values
    **  and then invalidates these values in the parameter object `x`.
    */
    Fifo & operator= (Fifo<T> &&x) {
	clear ();
	lfirst = x.lfirst; llast = x.llast; llength = x.llength;
	x.lfirst = nullptr; x.llast = nullptr;
	return *this;
    }

    //! Append an element to the end of a ‘Fifo<T>’-list.
    Fifo & push (const T &newval) {
	Item *newitem = new Item (newval);
	if (! lfirst) { lfirst = newitem; } else { llast->next = newitem; }
	llast = newitem; ++llength;
	return *this;
    }

    /*! A function alias for `Fifo::push()`.
    **  I added this for compatibility with the standard template classes.
    **  The only way to create such a function alias (as far as i know) is
    **  the implementation of a function which simply invokes the function
    **  it was created as an alias for ant returning this function's result.
    */
    Fifo & push_back (const T &newval) { return push (newval); }

    /*! This is a version of `push` to be used where the parameter value is
    **  expected to be thrown away after the `push()` operation. Currently,
    **  this requires an explicit `std::move()' operation on the argument.
    */
    Fifo & push (T &&newval) {
	Item *newitem = new Item (std::move(newval));
	if (! lfirst) { lfirst = newitem; } else { llast->next = newitem; }
	llast = newitem; ++llength;
	return *this;
    }

    /*! Function alias for `push(T &&)`, for making teh `Fifo<T>` template more
    ** standard compliant.
    */
    Fifo & push_back (T &&newval) { return push (std::move (newval)); }

    /*! Append a copy of the content of the `Fifo<T>` argument to the `Fifo<t>`
    **  object.
    */
    Fifo & push (const Fifo<T> &in) {
	for (Item *it = in.lfirst; it; it = it->next) {
	    Item *newitem = new Item (*it);
	    if (lfirst) { llast->next = newitem; } else { lfirst = newitem; }
	    llast = newitem; ++llength;
	}
	return *this;
    }

    /*! The version of `push(const Fifo<T> &)` above has no direct counterpart
    **  in any type of the standard template classes. So i'm free to use my own
    **  name for an alias. Currently, this is `push_back(const Fifo<T> &)`, but
    **  this is not a good name, so this is subject to change.
    */
    Fifo & push_back (const Fifo<T> &in) { return push (in); }

    /*! This version of `push()` should be invoked in situations where an
    **  argument is anyway thrown away after the operation. Its advantage
    ** is the fact that only some pointers are moved; no extra memory is
    ** allocated, no elements copied.
    */
    Fifo & push (Fifo<T> &&in) {
	if (lfirst) { llast->next = in.lfirst; } else { lfirst = in.lfirst; }
	if (in.llast) { llast = in.llast; }
	llength += in.llength;
	in.lfirst = in.llast = nullptr; in.llength = 0;
	return *this;
    }
    /*! Again, a function alias without a counterpart in any of the standard
    **  library's template types. Again subject to change.
    */
    Fifo & push_back (Fifo<T> &in) { return push (std::move(in)); }

    //! Insert an element at the beginning of a ‘Fifo’-list ... 
    Fifo & unshift (const T &newval) {
	Item *newitem = new Item (newval);
	newitem->next = lfirst;
	if (!lfirst) { llast = newitem; }
	lfirst = newitem; ++llength;
	return *this;
    }
    /*! Function alias for `unshift(const T&)`. */
    Fifo &push_front (const T &newval) { return unshift (newval); }

    /*! Version of `unshift()` to b used if the argument is thrown away after
    **  its invocation.
    */
    Fifo & unshift (T &&newval) {
	Item *newitem = new Item (std::move(newval));
	newitem->next = lfirst;
	if (!lfirst) { llast = newitem; }
	lfirst = newitem; ++llength;
	return *this;
    }
    /*! Function alias for `unshift(T&&)`. */
    Fifo &push_front (T &&newval) { return unshift (std::move (newval)); }

    /*! Insert (copies of) the elements of the argument (`Fifo<T>`) in reverse
    **  order at the beginning of the `Fifo` object.
    */
    Fifo & runshift (const Fifo<T> &in) {
	for (Item *it = in.lfirst; it; it = it->next) {
	    Item *newitem = new Item (*it);
	    newitem->next = lfirst;
	    if (! lfirst) { llast = newitem; }
	    lfirst = newitem; ++llength;
	}
	return *this;
    }

    /*! Insert (copies of) the elements of the `Fifo<T>` argument at the
    **  beginning of the `Fifo` object – in the same order they occur in
    **  the argument. (This is similar to something like `s = x + s` for
    **  strings.)
    */
    Fifo & unshift (const Fifo<T> &in) {
	// Why “manually” step by step? I could use the member-functions a lot.
	// The reason is simple. This way, it is faster, because with the
	// exception of ‘Item(const Item &x)’, no other function is invoked.
	Item *ifirst = nullptr, *ilast = nullptr;
	for (Item *it = in.lfirst; it; it = it->next) {
	    Item *newitem = new Item (*it);
	    if (ifirst) { ilast->next = newitem; } else { ifirst = newitem; }
	    ilast = newitem; ++llength;
	}
	if (ifirst) { ilast->next = lfirst; lfirst = ifirst; }
	return *this;
    }

    /*! A version of `unshift()` which assumes that the argument is thrown away
    **  after its invocation.
    */
    Fifo & unshift (Fifo<T> &&in) {
	// Temporary value 'in'. Here, the modification of some pointers should
	// be enough ...
	if (in.lfirst) { // For an empty list, 'unshift()' is a NOOP ...
	    // Append '*this' to the rvalue-list ...
	    in.llast->next = lfirst;
	    // ... and then make '*this' point to this rvalue-list
	    lfirst = in.lfirst;
	    // If '*this' was empty, its 'llast'-pointer must be adjusted ...
	    if (! llast) { llast = in.llast; }
	    llength += in.llength;
	    // Reset the pointers of 'in' (required!)
	    in.lfirst = in.llast = nullptr; in.llength = 0;
	}
	return *this;
    }

    /*! `shift()` removes the first element from the `Fifo` list and returns
    **  it.
    */
    T shift () {
	check_not_empty ("shift");
	Item *item = lfirst->next;
	lfirst->next = nullptr;
	T firstval = std::move(lfirst->value);
	delete lfirst;
	lfirst = item; --llength;
	if (!item) { llast = nullptr; }
	return firstval;
    }
    /*! standard library compliant function alias for `shift()`. */
    T pop_front() { return shift(); }

    /*! Removes the last element from a ‘Fifo’-list. Because of the nature
    **  of this `Fifo<T>` template (single linked list), this operation is
    **  somewhat slow, because the operation must make the `llast` pointer
    **  point to the previous element of the element `llast` currently
    **  points to.
    */
    T pop () {
	check_not_empty ("pop");
	Item *item = lfirst;
	if (item == llast) {
	    lfirst = llast = nullptr;
	} else {
	    while (item->next != llast) { item = item->next; }
	    llast = item; item = llast->next; llast->next = nullptr;
	}
	T lastval(std::move(item->value)); --llength;
	delete item;
	return lastval;
    }

    /*! `first()` returns (a reference to) the first element of a `Fifo`-list.
    */
    T & first () {
	check_not_empty ("first");
	return lfirst->value;
    }

    /*! `last()` returns (a reference to) the last element of a `Fifo`-list. */
    T & last () {
	check_not_empty ("last");
	return llast->value;
    }

    /*! `empty()` returns `true` if the `Fifo` list contains no elements (and
    **  `false` otherwise).
    */
    bool empty () const {
	return lfirst == nullptr;
    }

    /*! `length()` returns the current length of the `Fifo` list. */
    size_t length () const {
	//size_t res = 0;
	//for (Item *i = lfirst; i; i = i->next) { ++res; }
	//return res;
	return llength;
    }
    /*! `size()` is a function alias for `length()`. */
    size_t size () { return llength; }

    /*! `operator[](*n*)` returns a reference to the `n`th element of the
    **  `Fifo` list. If `n` exceeds the length of the list, a
    **  `std::range_error` exception is raised.
    */
    T & operator[] (size_t ix) {
	Item *i = lfirst;
	while (i && ix > 0) { --ix; i = i->next; }
	if (! i) { throw std::range_error ("index out of bounds"); }
	return i->value;
    }

    /*! `recount()` re-calculates the `llength` field of the `Fifo` list (by
    **  counting its elements). It can be used in the (very unlikely) case that
    **  the `llength` isn't in sync with the real list length. Additionally,
    **  the `llast` is fixed.
    */
    size_t recount() {
	size_t rclength = 0;
	Item *it = lfirst;
	if (! it) { llast = it; }
	while (it) {
	    ++rclength;
	    if (! it->next) { llast = it; }
	    it = it->next;
	}
	llength = rclength;
	return llength;
    }

    /*! creates and returns a `Fifo::Iterator` which points to the first
    **  element of the `Fifo` list.
    */
    Iterator begin () { Iterator res (this); return res; }

    /*! creates and returns a `Fifo::ConstIterator` which points to the
    **  first element of the `Fifo` list.
    */
    ConstIterator begin () const { ConstIterator res (this); return res; }

    /*! creates and returns a `Fifo::Iterator` which (virtually) points
    **  behind the `Fifo` list. Its sole purpose is to function as (a part of)
    **  the termination condition in an iteration `for` loop.
    */
    Iterator end () { Iterator res (this, true); return res; }

    /*! creates and returns a `Fifo::ConstIterator` which (virtually) points
    **  behind the `Fifo` list. Its sole purpose is to function as (a part of)
    **  the termination condition in an iteration `for` loop-
    */
    ConstIterator end () const { ConstIterator res (this, true); return res; }

    /*! This (converstion) operator allows for a `Fifo<T>` object being used
    **  directly in a condition of an `if` statement of a `while` or `for`
    **  loop. E.g., `if (fifoObj)` has the same meaning as
    **  `if (! fifoObj.empty())`. Because i added it at a late time during the
    **  development of this software, it is rarely used.
    */
    operator bool() { return lfirst != nullptr; }
};

/*! This is a (parametrized) type alias for `Fifo::Iterator`. */
template<class T> using FifoIterator = typename Fifo<T>::Iterator;

/*! This is a (parametrized) type alias for `Fifo::ConstIterator`. */
template<class T> using ConstFifoIterator = typename Fifo<T>::ConstIterator;

//template<class T>
//bool operator== (const FifoIterator<T> &lhs, const FifoIterator<T> &rhs)
//{
//    return (lhs.base() == rhs.base()) && (lhs.current() == rhs.current());
//}

#endif /*FIFO_H*/
