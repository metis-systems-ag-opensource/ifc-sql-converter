/*! \file float2string.h
**  Interface part of the module `float2string.{cc,h}`.
**
**  Floating point (`long double`) to `std::string` conversion. This module
**  was written, because the `std::to_string (...)` functions are inappropriate
**  for the use in STEP. Additionally, the format of floating point numbers in
**  STEP differs slightly from those in C/C++.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Interface file for 'float2string.cc'
** (Small float => string generating float-values conforming with the IFC-
**  specification ...)
**
*/
#ifndef FLOAT2STRING_H
#define FLOAT2STRING_H

#include <string>

#include <cstdio>

/*! `epsilon` specifies the maximum precision used for converting a floating
**  point number to a string. It is large enough for floating point numbers
**  being converted to strings without rounding errors which lead to something
**  like 'x.y999999999' (instead of 'x.z'). It is also used for specifying the
**  precision where a conversion stops when the (relative) value being
**  converted is too small for making the result different from a conversion
**  where this value doesn't exist.
*/
const long double epsilon = 1E-12;

/*! \param x			the floating point value to be converted
**  \param maxexp		the maximum/minimum magnitude for producing
**    floating point numbers without an `E+*NNN*` / `E-*NNN*` exponent.
**  \param maxdigits		the maximum number of digits
**
**  `float2string()` converts a floating point number into a `std::string` in
**  a format which is compatible to the IFC (STEP) input format, meaning that
**  a REAL value (EXPRESS/STEP) is in most cases converted into the same format
**  as it was originaly read from an IFC-file. The reason for implementing this
**  function was that the `double` => `std::string` conversion functions from
**  the C++ standard library are completely inappriopriate, meaning: They not
**  even nearly reproduce the string representations of floating point numbers
**  in the format they originally had, and always produce a fixed number of
**  digits in the fractional part. Because even the `%*x*.*y*f` format from
**  the `printf()` alike functions were unable to produce a floating point
**  representation completely compliant with the specifications defined by
**  EXPRESS (ISO-10303-11) or STEP (ISO-10303-21), the conversion is done
**  *by hand* in this function.
*/
std::string float2string (long double x, int maxexp = 6, int maxdigits = 14);

#endif /*FLOAT2STRING_H*/
