/* readline.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
** 
**
** Small drop-in and of line agnostic replacement for the 'getline()' function
** from the standard library ...
**
*/

#include <utility>

#include "readline.h"

#define CR '\15'

using namespace std;

istream& readline (istream& is, string& str)
{
    string line;
    getline (is, line);
    size_t linelen = line.size();
    if (linelen > 0 && line[linelen - 1] == CR) {
	// A 'CR' here means that the line ended with 'CR/LF', otherwise a
	// single CR wouldn't remain at the end of the line.
	line.resize (linelen - 1);
    }
    str = move (line);
    return is;
}
