/* filelist.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** "Load" a list of files which match a specific filename pattern from a given
** directory and all of its sub-directories
**
*/

#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <fnmatch.h>
#include <alloca.h>
#include <cerrno>
#include <system_error>

#include "pathcat.h"

#include "filelist.h"

using std::string;
using std::system_error;
using std::system_category;

static int isdir (const char *dir, struct dirent *de)
{
#if defined(_DEFAULT_SOURCE) || defined(_BSD_SOURCE)
    return de->d_type == DT_DIR;
#else
    size_t de_namelen = _D_EXACT_NAMLEN (de);
    struct stat sb;
    char *path = alloca (strlen (dir) + de_namelen + 2), *p;
    p = stpcpy (path, dir); *p++ = '/';
    memcpy (p, de->d_name, de_namelen); p[de_namelen] = '\0';
    if (lstat (path, &sb)) { return -1; }
    return (sb.st_mode & S_IFMT) == S_IFDIR;
#endif
}

static int xmatch (const char *pattern, const char *ns, size_t sl, int flags)
{
    char *s = (char *) alloca (sl + 1);
    memcpy (s, ns, sl); s[sl] = '\0';
    return fnmatch (pattern, s, flags);
}
    

static int pathlist (const char *dir, const char *pattern,
		     Fifo<string> &files, bool recursive)
{
    int rc = 0;
    Fifo<string> subdirs;
    struct dirent *de;
    DIR *dh = opendir (dir);
    if (! dh) { return -1; }
    while ((de = readdir (dh))) {
	size_t de_namelen = _D_EXACT_NAMLEN (de);
	char *de_name = de->d_name;
	if ((de_namelen == 1 && *de_name == '.')
	||  (de_namelen == 2 && *de_name == '.' && de_name[1] == '.')) {
	    continue;
	}
	rc = isdir (dir, de);
	if (rc < 0) { continue; }
	if (rc) {
	    if (recursive) { subdirs.push (string (de_name, de_namelen)); }
	    continue;
	}
	rc = xmatch (pattern, de_name, de_namelen, FNM_PERIOD);
	if (rc != 0) { continue; } // FNM_NOMATCH or *error*
	files.push (dir / string (de_name, de_namelen));
    }
    closedir (dh);
    rc = 0;
    if (recursive) {
	for (auto &sd: subdirs) {
	    string dp = dir / sd;
	    if (pathlist (dp.c_str(), pattern, files, recursive) < 0) {
		rc = -1;
	    }
	}
    }
    return rc;
}

Fifo<string> filelist (const char *dir, const char *pattern, bool recursive)
{
    Fifo<string> res;
    int rc = pathlist (dir, pattern, res, recursive);
    if (rc < 0 && res.empty()) {
	// TRUE ERROR
	throw system_error (errno, system_category(), "filelist() failed");
    }
    return res;
}
