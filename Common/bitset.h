/*! \file bitset.h
**
** \brief
** A small bit set implementation (`BitSet<IntType, MinInt>`) based on
** `std::vector<bool>`.
**
** `BitSet` is a template class specialised for fast bitset operations on
** integer types. It used `std::vector<bool>` as base (not base type) and
** implements functions for in-/excluding integers into/from it, testing for
** an integer being member of it, as well as returning its elements either
** as a `std::vector<IntType>` of `Fifo<IntType>`.
**
** The second template parameter is the minimum `IntType`-value the bitset
** can contain.
*/
/* $Id$ 
** 
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
** 
** A specialized bit-set for integer types. This class is intended to be used
** in implementation files only (for temporarily used values, such as local
** variables of functions), so it is not relevant that it sometimes may require
** some hundreds of bytes of memory.
**
*/
#ifndef BITSET_H
#define BITSET_H

#include <cstdint>
#include <utility>
#include <vector>

#include "fifo.h"

/*!
** `BitSet` is a Template class for sets of (sub-types of) integer types.
** Background is that sometimes fast set operations for integer types are
** useful, in places in the code where memory requirements are not an issue
** (e.g., as local variables inside of a function).
**
**  @param IntType
**    an integer type (int, unsigned, short, long, ...).
**  @param minval
**    A constant value of the type *IntType*. This value specifies the
**    smallest integer a set of this type can contain.
*/
template<typename IntType, IntType minval,
	 typename _Alloc = std::allocator<IntType>>
struct BitSet {
    //! Internal implementation of the `BitSet` type
    using bitvec = std::vector<bool, _Alloc>;

    BitSet() : v() { }

    /*! `BitSet::has(IntType)` tests for an integer value being member of the
    **  bitset it is applied on.
    **
    **  @returns `true` if the integer value is a member of the bitset,
    **           and `false` otherwise.
    */
    bool has (IntType iv) {
	if (iv - minval < 0) { return false; }
	size_t x = (size_t) (iv - minval);
	if (v.size() <= x) { return false; }
	return v[x];
    }

    /*! `BitSet::insert(IntType)` inserts an integer value into the bitset
    **  it is applied on.
    **
    **  @returns `true` if the integer value could be inserted, and `false`,
    **  otherwise. The latter can occur if
    **   - the integer value is already member of the bitset, or
    **   - it is less than the `minval` template argument.
    */
    bool insert (IntType iv) {
	if (iv < minval) { return false; }
	size_t x = (size_t) (iv - minval);
	if (v.size() <= x) { v.resize (x + 1, false); }
	if (v[x]) { return false; }
	v[x] = true;
	return true;
    }

    /*! `BitSet::include()` is an alias for `BitSet::insert()`. */
    bool include (IntType iv) { return insert (iv); }

    /*! `BitSet::exclude (IntType)` removes an integer value from the bitset
    **  it is applied on.
    **
    **  @returns `true` if the integer value could be removed from the bitset,
    **  and false, otherwise. The latter occurs if
    **   - the integer value was not member of the bitset, or
    **   - it is less than the `minval` template argument.
    */
    bool exclude (IntType iv) {
	if (iv < minval) { return false; }
	size_t x = (size_t) (iv - minval);
	if (v.size() <= x || ! v[x]) { return false; }
	v[x] = false;
	return true;
    }

    /*! `BitSet::fill(const Fifo<IntType> &) insert all elements from the
    **  given `Fifo` into the bitset it is applied on.
    */
    void fill (const Fifo<IntType> &idl) {
	for (auto id: idl) { insert (id); }
    }

    /*! `BitSet::preset(IntType max, bool incl)` recreates the content of the
    **  `BitSet` object it is applied on, either filled with elements from the
    **  template parameter `minval` to the argument `maxval` (if `incl` is
    **  `true`) or as an empty set (if `incl` is `false` or not given).
    **
    **   The purpose of this function is to generally speed up all operations
    **   on the bitset. This goal can be reached if the maximum number the
    **   bitset can contain is known before any operation starts, because no
    **   further memory (de-)allocation takes place apart from the one used in
    **   this member function.
    */
    void preset (IntType maxval, bool incl = false)
    {
	clear(); v.resize ((size_t) (maxval - minval) + 1, incl);
    }

    /*! `BitSet::elements()` returns all elements of the bitset it is applied
    **  on as a `Fifo<IntType>`.
    **
    **  Because of the nature of `BitSet`, these elements are ordered by the
    **  natural order of `IntType`
    */
    Fifo<IntType> elements() {
	Fifo<IntType> res;
	for (IntType x = 0; x < v.size(); ++x) {
	    if (v[x]) { res.push ((IntType) (x + minval)); }
	}
	return res;
    }

    /*! `BitSet::elementv()` returns all elements of the bitset it is applied
    **  on as a `std::vector<IntType>`.
    **
    **  Because of the nature of `BitSet`, these elements are ordered by the
    **  natural order of `IntType`
    */
    std::vector<IntType> elementv() {
	std::vector<IntType> res;
	for (IntType x = 0; x < v.size(); ++x) {
	    if (v[x]) { res.push_back ((IntType) (x + minval)); }
	}
	res.shrink_to_fit();
	return res;
    }

    /*! `BitSet::clear()` removes all elements from the bitset it is applied on
    **  and deallocates the memory used for "storing" these elements.
    */
    void clear() {
	// According to the C++ standard library documentation, `std::swap()`
	// with an empty value is the only way to guarantee a  deallocation of
	// the memory previously used. `shrink_to_fit()` is not guaranteed to
	// do this, even if most implementations should do exactly this
	// – deallocating the memory used.
	bitvec z;
	std::swap (v, z);
    }

private:
    /* Implemented the content as a 'BitSet::bitvec', which itself is a
    ** `std::vector<bool>`, a specialized implementation of the template
    ** class `std::vector<T>`.
    */
    bitvec v;
};

#endif /*BITSET_H*/
