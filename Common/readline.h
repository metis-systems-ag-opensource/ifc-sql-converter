/* readline.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
** 
**
** Interface part of the 'readline.{cc,h}' module.
** (Small drop-in and of line agnostic replacement for the 'getline()' function
**  from the standard library ...)
**
*/
#ifndef READLINE_H
#define READLINE_H

#include <iostream>
#include <string>

std::istream& readline (std::istream& is, std::string& str);

#endif /*READLINE_H*/
