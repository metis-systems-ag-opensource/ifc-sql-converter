/*! \file confsp.h
**
**  \brief A function for determining the pathname of a configuration file
**  by its name.
**
**  Configuration files can't be placed anywhere in the filesystem. They are
**  searched for only in a fixed number of places. A given configuration name
**  specifies the configuration file being searched for.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Interface file for 'confsp.cc'
** (Search a for a configuration file in a fixed number of places by using
**  a configuration name.)
**
*/
#ifndef CONFSP_H
#define CONFSP_H

#include <utility>
#include <string>

/*  Searching a configuration by in a fixed number of places by using a
**  name identifying the the configuration.
**  Returns the pathname of a file and a `true` flag on success or
**  an empty string and a `false` flag on failure.
*/

/*! \param confname - the name (*not filename*) of a configuration to be
**  searched for.
**
**  \returns A pair consisting of a pathname and `true` on success; or
**  an empty string and `false` if no configuration item matching `confname`
**  could be found.
**
**  This function searches through a fixed list places for a configuration
**  file, using `confname`. The name shouldn't be a pathname, and it should
**  not have a suffix (like in `xyz.conf`).
**  It should *not* have a suffix (like in `xyz.conf`), because a suffix is
**  appended if necessary.
*/
std::pair<std::string, bool> findFile (const std::string &confname);

#endif /*CONFSP_H*/
