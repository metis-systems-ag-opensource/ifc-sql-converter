/* dump.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Small function for dumping something at a givenm address
**
*/

#include <iostream>
using std::cerr, std::endl;

#include <cctype>
#include "sutil.h"

#include "dump.h"

using std::string;

static const char *digits = "0123456789ABCDEF";

/* `hexaddr(*p*)` creates a `std::string` value, consisting of a sequence of
** hexadecimal digits containing the address pointed to by the pointer `p`.
** The value is always left-extended by a number of '0' characters making it
** always aligned to a width which corresponds to the hexadecimal
** representation of the maximum maximum value a pointer may have.
** the pointer value (times two) is reached.
*/
static string hexaddr (void *ptr)
{
    static char buf[sizeof(unsigned long long) * 2 + 1];
    string res;
    unsigned long long addr = (unsigned long long) ptr;
    size_t mx = sizeof(unsigned long long);
    size_t ix = mx;
    buf[--ix] = '\0';
    while (addr > 0) {
	buf[--ix] = digits[(size_t) (addr & 15)]; addr >>= 4;
    }
    while (ix > 0 && ix + 15 > mx) { buf[--ix] = '0'; }
    return buf + ix;
}

/* `hexbyte(*b*)` creates a `std::string` value  consisting of a leading blank
** and two hexadecimal digits representing the value of `b`.
*/
static string hexbyte (uint8_t b)
{
    char buf[4] = {
	' ', digits[(size_t) (b >> 4)], digits[(size_t) (b & 15)], 0
    };
    return buf;
}

/* `prbyte (*b*)` creates a string from the given byte value `b`. For values of
** `b` which are printable, this string consists solely of the corresponding
** character value. for unprintable characters, a string containing the UTF-8
** sequence of the unicode "replacement character" (U+FFFD) is created.
*/
static string prbyte (uint8_t b)
{
    char buf[5], *bp;
    bp = buf; *bp++ = ' ';
    if (isprint (b)) {
	*bp++ = (char) b;
    } else {
	*bp++ = (char) 0xef;
	*bp++ = (char) 0xbf;
	*bp++ = (char) 0xbd;
    }
    *bp = '\0';
    return buf;
}

/* `dump(p, s, w)` werite the content of the memory of size `s`, beginning at
** the adress `p` as a hexadecimal dump into a `std::string` value and then
** returns this value. The optional argument `w` specifies the number of
** bytes being hex-dumped per line.
*/
string dump (void *ptr, size_t n, int w)
{
    string res, hexbytes, prbytes;
    uint8_t *addr = (uint8_t *) ptr, *paddr;
    int cc = 0;
    paddr = addr;
    // Write alle lines which are filled upto `w` characters into the string
    while (n-- > 0) {
	uint8_t b = *addr++;
	if (cc++ >= w) {
	    res += hexaddr (paddr) + ": " + hexbytes + "  " + prbytes + EOL;
	    paddr = addr - 1; hexbytes.clear(); prbytes.clear(); cc = 1;
	}
	hexbytes += " " + hexbyte (b);
	prbytes += prbyte (b);
    }
    if (hexbytes.size() > 0) {
	// Align the fields for the hexadecimal and the character values
	while (cc++ < w) {
	    hexbytes += "    "; prbytes += "  ";
	}
	res += hexaddr (paddr) + ": " + hexbytes + "  " + prbytes + EOL;
    }

    return res;
}
