/* tohex.cc
**
** $Id: f359420020046fbe9fb4a93a05f08bc8c5f28c20 $
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Convert a value (unsigned long) into a hexadecimal number (std::string)
**
*/

#include <cstring>
#include <stdexcept>

#include "tohex.h"

using std::out_of_range, std::length_error;
using std::string, std::to_string;


static const char dg[] = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
    'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y', 'Z'
};

static
unsigned to_base_n (unsigned long x, char *buf, size_t bufsz, unsigned base)
{
    unsigned len = 0;
    char *p = buf + bufsz;
    if (base < 2 || base > 36) {
	throw out_of_range ("to_base_n(): base (" + to_string (base) +
			    ") out of range (2 ... 36)");
    }
    if (bufsz < 1) {
	throw length_error ("to_base_n(): buffer to short");
    }
    *--p = '\0';
    while (x > 0) {
	unsigned d = (unsigned) (x % (unsigned long) base);
	x = x / (unsigned long) base;
	if (p > buf) { *--p = dg[d]; ++len; }
    }
    if (p > buf) {
	char *q = buf;
	while ((*q++ = *p++)) { }
    }
    return len;
}

static
unsigned ins_base (string &x, unsigned base)
{
    x.reserve (4);
    switch (base) {
	case 2: x.append ("0b"); return 2;
	case 8: x.append ("0o"); return 2;
	case 10: return 0;
	case 16: x.append ("0x"); return 2;
	default: {
	    char buf[10];
	    unsigned len = to_base_n (base, buf, sizeof(buf), 10);
	    x.append (buf, len); x.push_back ('b');
	    return len + 2;
	}
    }
}

string to_base_n (unsigned long x, unsigned base, unsigned width, char fc)
{
    char buf[sizeof(unsigned long) * 8 + 10];
    unsigned len = to_base_n (x, buf, sizeof(buf), base), pfxlen;
    string res, pfx;
    res.reserve ((width > len ? width : len) + 3);
    pfxlen = ins_base (pfx, base);
    if (width <= pfxlen) { width = 1; } else { width -= pfxlen; }
    if (fc == '0') { res.append (pfx); }
    if (width > len) { res.append ((width - len), fc); }
    if (fc != '0') { res.append (pfx); }
    res.append (buf, len);
    return res;
}

string to_hex (unsigned long x, unsigned width, char fc)
{
    return to_base_n (x, 16, width, fc);
}

string to_dec (unsigned long x, unsigned width, char fc)
{
    return to_base_n (x, 10, width, fc);
}

string to_oct (unsigned long x, unsigned width, char fc)
{
    return to_base_n (x, 8, width, fc);
}

string to_bin (unsigned long x, unsigned width, char fc)
{
    return to_base_n (x, 2, width, fc);
}

string ptr2str (const void *x)
{
    return "[0x" + to_base_n ((unsigned long) x, 16, 1, 0) + "]";
}

#ifdef TESTING

#include <iostream>

using std::cerr, std::cout, std::endl, std::flush;

int main()
{
    unsigned a = 34144;

    cout << "a                  = " << a << endl;
    cout << "to_dec (a)         = " << to_dec (a) << endl;
    cout << "to_hex (a)         = " << to_hex (a) << endl;
    cout << "to_hex (a, 8)      = " << to_hex (a, 8) << endl;
    cout << "to_hex (a, 8, '0') = " << to_hex (a, 8, '0') << endl;

    cout << endl << "At END" << endl;
    return 0;
}

#endif /*TESTING*/
