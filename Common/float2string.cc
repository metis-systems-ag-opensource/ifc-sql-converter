/* float2string.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Small float => string generating float-values conforming with the IFC-
** specification ...
**
*/
#include <cmath>
#include <cstdlib>
#include <cstring>

#include <cstdio>

#include "float2string.h"

using namespace std;

namespace {
    long double ipow (long double x, int n)
    {
	long double res = 1.0;
	if (n == 0) { return res; }
	bool inv = (n < 0);
	if (inv) { n = -n; }
	while (n > 0) {
	    if ((n & 1) != 0) { res *= x; }
	    x *= x; n >>= 1;
	}
	return (inv ? 1.0 / res : res);
    }

    const string dg[] = {
	"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
    };
} /*namespace <empty>*/

string float2string (long double x, int maxexp, int maxdigits)
{
    string res("");
    size_t d;
    bool negative = (x < 0);
    if (negative) { x = -x; }
    if (x == 0.0) { return "0."; }
    if (x >= 1.0) {
	if (x <= ipow(10, maxexp)) {
	    long double z = 1.0;
	    int zp = 0;
	    while (x / z >= 10.0) {
		++zp; z = ipow (10.0, zp);
	    }
	    x /= z;
	    do {
		d = (size_t) floor (x); x -= d;
		res += dg[d]; --maxdigits;
		x *= 10.0;
	    } while (zp-- > 0);
	    res += ".";
	    while (x >= epsilon && maxdigits > 0) {
		d = (size_t) floor (x + epsilon); x -= d;
		res += dg[d]; --maxdigits;
		x *= 10.0;
	    }
	} else {
	    long double z = 1.0;
	    int zp = 0;
	    while (x / z >= 10.0) { zp += 1; z = ipow (10.0, zp); }
	    x /= z;
	    string exp = "E" + to_string (zp);
	    d = (size_t) floor (x); x -= d;
	    res += dg[d] + "."; --maxdigits;
	    x *= 10.0;
	    while (x >= epsilon && maxdigits > 0) {
		d = (size_t) floor (x); x -= d;
		res += dg[d]; --maxdigits;
		x *= 10.0;
	    }
	    res += exp;
	}
    } else if (x >= ipow(10, -maxexp)) {
	res = "0."; --maxdigits;
	while (x >= epsilon && maxdigits > 0) {
	    x *= 10.0; d = (size_t) (floor (x + epsilon)); x -= d;
	    res += dg[d]; --maxdigits;
	}
    } else {
	long double z = 1.0;
	int zp = 0;
	while (x * z < 1.0) { zp += 1; z = ipow (10.0, zp); }
	x *= z;
	string exp = "E-" + to_string (zp);
	d = (size_t) floor (x + epsilon); x -= d;
	res += dg[d] + ".";
	while (x >= epsilon && maxdigits > 0) {
	    x *= 10.0;
	    d = (size_t) floor (x + epsilon); x -= d;
	    res += dg[d]; --maxdigits;
	}
	res += exp;
    }
    res = (negative ? "-" : "") + res;
    return res;
}

#ifdef TESTING
static const string progname (const char *arg)
{
    const char *prog = strrchr (arg, '/');
    if (prog) { ++prog; } else { prog = arg; }
    return prog;
}

#include <iostream>

int main (int argc, char *argv[])
{
    static string prog = progname (*argv);
#if 0
    cout << "ipow(10, 0) = " << ipow(10, 0) << endl;
    cout << "ipow(10, 1) = " << ipow(10, 1) << endl;
    cout << "ipow(10, 2) = " << ipow(10, 2) << endl;
#endif
#if 1
    for (int ix = 1; ix < argc; ++ix) {
	string sv = argv[ix];
	size_t px;
	long double fv = stold (sv, &px);
	if (px == 0 || px < sv.length ()) {
	    cerr << prog << ": Invalid fp-value (" << sv << ")" << endl;
	    continue;
	}
	cout << sv << " => " << fv << " => " << float2string (fv) << endl;
    }
#endif
    return 0;
}
#endif /*TESTING*/
