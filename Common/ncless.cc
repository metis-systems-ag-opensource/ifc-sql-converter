/* ncless.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Implementation part of the 'ncless' module
** (Defining my own "less" functional class)
**
*/

#include <cctype>

#include "ncless.h"

//struct NCLess : std::binary_function<std::string, std::string, bool> {
bool NCLess::operator() (const std::string &x, const std::string &y) const {
    size_t xlen = x.length(), ylen = y.length();
    size_t clen = (xlen < ylen ? xlen : ylen);
    for (size_t ix = 0; ix < clen; ++ix) {
	int cres = tolower (x[ix]) - tolower (y[ix]);
	if (cres != 0) { return cres < 0; }
    }
    return xlen < ylen;
}
//} /*NCLess*/;
