/*! \file fifoout.h
**
**  \brief The definition of the standard output operator (`<<`) for `Fifo<T>`
**  objects.
**
** Standard output operator (`<<`) for `Fifo<T>` objects. This operator
** depends on the existance of the output operator `<<` for the underlying
** type `T`.
**
** This definition was originally part of `fifo.h`, but was moved into this
** file, because `<<` on `Fifo<T>` objects rather seldom used.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
** ostream operator<< () on Fifos ...
**
*/
#ifndef FIFOOUT_H
#define FIFOOUT_H

#include <ostream>

// Declaring the `operator<<()` functionin the namespace `std`.
namespace std {

/*! @param os - A `std:ostream` reference parameter.
**
**  @param list - A `Fifo<T>` value.
**
**  @returns `os` after the writing operation.
*/
template<class T>
ostream & operator<< (ostream &os, Fifo<T> &list)
{
    bool first = true;
    os << "{";
    for (T &it: list) {
	if (first) { first = false; } else { os << ", "; }
	os << it;
    }
    return os << "}";
}

} /*namespace std*/

#endif /*FIFOOUT_H*/
