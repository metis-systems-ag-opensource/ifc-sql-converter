/*! \file sysutils.h
**  Interface part of the `sysutils.{cc,h}` module.
**
**  The `sysutils.{cc,h}` module defines some system functions (like the
**  ID of the current user, or his/her name, or the name of the program which
**  uses this module, or the pathname of the directory where the binary of the
**  this program resides.)
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
** Interface file for 'sysutile.h'
** (Small helper functions for getting
**  a) (some of) a given user's data,
**  b) the same data, but for the effective user,
**  c) the name of the program's binary,
**  d) the path of the directory the program's binary resides in.
**
**  The latter two functions are Linux-specific.)
**
*/
#ifndef SYSUTILS_H
#define SYSUTILS_H

#include <ctime>
#include <pwd.h>
#include <string>

/*! `udata(...)` retrieves the current user's data (username, userid, home
**  directory). The return value is `0` for success or the system's error
**  code (`errno`).
*/
int udata (const std::string &nameuid, std::string &user, uid_t &uid,
	   std::string &home);

/*! `euser(...)` retrieves the userid and home directory of the "effective
**  user".
**
** \remark
**  Unix systems traditionally distinguish between the so-called "real user"
**  of a running program, and the "effective user". The "real user" is
**  (normally) the user who started the program which uses this module. If the
**  program switches to another user (e.g. because the program's binary file
**  was stored with a `setuid` bit enabled), the "effective user" is the user
**  controlling the running program. Therefore, in most cases, "real user" and
**  "effective user" remain the same, but when the context of the running
**  program is another user than the one who it originally started, "real user"
**  and "effective" user differ.
*/
int euser (std::string &user, uid_t &uid, std::string &home);

/*! `username(...)` returns the name of the given user (id or name). */
std::string username (const std::string &nameuid);

/*! `username()` returns the name of the "effective user". */
std::string username();

/*! `userhome(...)` returns the home directory of the given user (id or name).
*/
std::string userhome (const std::string &nameuid);

/*! `userhome()` returns the home directory of the "effective user". */
std::string userhome();

/*! `userid(...)` returns the userid of the given user (id or name). */
uid_t userid (const std::string &nameuid);

/*! `userid()` returns the userid of the "effective user". */
uid_t userid();

/*! `appname` returns the filename of the binary file of the program using this
**  module.
*/
std::string appname();

/*! `appdir` returns the (absolute) pathname of the directory where the binary
**  file of the program using this module resides.
*/
std::string appdir();

/*! `s2time()` converts a string representing a fixed point number
**  (`<integral part>.<fractional part>`) into a `struct timespec`
**  value.
**
**  @param `delay` - a string containing a fixed point number
**
**  @returns the converted (`struct timespec`) value.
*/
struct timespec s2time (const std::string &delay);

struct FileInfo_priv;

class FileInfo {
public:
    FileInfo();
    FileInfo (const std::string &pathname, bool keep_symlinks = false);
    FileInfo (const FileInfo &x);
    FileInfo (FileInfo &&x);
    ~FileInfo();
    void get_info (const std::string &pathname, bool keep_symlinks = false);
    void get_info (bool keep_symlinks = false);
    bool exists();
    bool regular_file();
    bool directory();
    bool symlink();
    bool error();
    int sys_errno() const;
private:
    FileInfo_priv *data;
};

#endif /*SYSUTILS_H*/
