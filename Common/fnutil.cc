/* fnutil.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Utilitiy functions for converting filenames into some other things...
**
*/

#include <cctype>
#include <stdexcept>

#include "filesep.h"
#include "fnutil.h"

using std::string, std::operator""s;

string basename (const string &filename)
{
    char pathsep = PATHSEP;
    const size_t fnlen = filename.size();
    size_t slashpos = filename.find_last_of (pathsep), ep = fnlen - 1;
    string res;
    if (slashpos == string::npos) { return filename; }
    if (slashpos == ep) {
	while (ep > 0 && filename[ep] == pathsep) { --ep; }
	if (ep == 0) {
	    // This means the the filename consists entirely of '/' ...
	    return filename.substr (ep, 1);
	}
	slashpos = filename.find_last_of (pathsep, ep);
	res = filename.substr (slashpos + 1, ep - slashpos);
    } else {
	res = filename.substr (slashpos + 1);
    }
    return res;
}

/*! `suffix()` returns the filename extension (suffix) if such a thing exists.
**  Otherwise, it returns the empty string.
*/
string suffix (const std::string &filename, char sfxdelim)
{
    char pathsep = PATHSEP;
    const char *p = filename.c_str(), *q = p + filename.size();
    while (q > p && *--q != sfxdelim) {
	if (*q == pathsep) { return ""; }
    }
    return (q == p ? ""s : string (++q));
}

string dirname (const string &filename)
{
    char pathsep = PATHSEP;
    size_t lastpspos = filename.find_last_of (pathsep);
    // No '/', '\' or whatever? => the directory is '.'
    if (lastpspos == string::npos) { return "."; }
    // last '/' is at the start of the pathname? => the directory consists of
    // only the path separator ('/', '\' or whatever).
    if (lastpspos == 0) { return string (1, pathsep); }
    // Otherwise, the directory is all but the part beginning with the last
    // occurrence ot the path separator
    return filename.substr (0, lastpspos);
}

string trans_filename (const string &filename)
{
    char pathsep = PATHSEP;
    const size_t fnsz = filename.size();
    size_t ix;
    string res (fnsz + 1, '\0');
    for (ix = 0; ix < fnsz; ++ix) {
	char ch = filename[ix];
	if (isalpha (ch)) {
	    res[ix] = toupper (ch);
	} else if (isdigit (ch) || ch == '_') {
	    res[ix] = ch;
	} else if (ch == '-' || ch == '.' || ch == pathsep) {
	    // Convert '-', '.', and the path element delimiter into '_' ...
	    res[ix] = '_';
	}
	// Ignore all other characters
    }
    res.resize (ix);
    res.shrink_to_fit();
    return res;
}

bool is_abspath (const string &fn)
{
    size_t fnsz = fn.size();
#if defined(__WIN32__) || defined(__WIN64__)
    if ((fnsz >= 3 && isalpha (fn[0]) && fn[1] == ':' && fn[2] == PATHSEP)
    ||  (fnsz >= 2 && fn[0] == PATHSEP && fn[1] == PATHSEP)) {
	return true;
    }
    return false;
#else /* Unix, Linux or MacOS X */
    return fnsz > 0 && fn[0] == PATHSEP;
#endif /* __WIN32__ || __WIN64__ */
}

bool is_pathname (const string &fn)
{
    return fn.find (PATHSEP) != string::npos;
}
