/* confsp.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Search a for a configuration file in a fixed number of places by using
** a configuration name.
**
*/

#include "pathcat.h"
#include "sutil.h"
#include "sysutils.h"

#include "confsp.h"

// The pathname pattern list to be used for searching for a given file. It
// contains templates for the simple template substitution function
// `tmpl_subst()` (from `sutil.{cc,h}`).
static const char *pathlist[] = {
    "%userconfig/ifc/%file",
    "%userconfig/%file",
    "%sysconfig/ifc/%file.conf"
    "%sysconfig/ifc/%file"
    "%sysconfig/%file.conf",
    "%sysconfig/%file",
    "%progdir/../conf/%file.conf",
    "%progdir/%file.conf",
};

// My source code required C++17 for this (and some other features not
// supported in earlier versions of the C++ standard.
using std::pair, std::make_pair, std::string;

pair<string, bool> findFile (const string &confname)
{
    // The list of valid variables for the replacement of the placeholders in
    // the templates in `pathlist[]`.
    Substitutes vars {
	{ "progdir", appdir() },
	{ "home", userhome() },
	{ "userconfig", userhome() / ".config" },
	{ "sysconfig", "/etc" },
	{ "file", confname }
    };

    FileInfo finfo;

    /* Skip the substitution part if 'confname' is a pathname (contains a
    ** pathname separator).
    */
    if (confname.find_first_of (PATHSEP) != string::npos) {
	finfo.get_info (confname);
	if (finfo.exists() && finfo.regular_file()) {
	    return make_pair (confname, true);
	}
	/* Don't perform any substitutions with `pathlist[]` for determining
	** a configuration file name, because the intention was to explicitely
	** specify a configuration file.
	*/
	return make_pair ("", false);
    }

    /* Try to find a configuration file by substituting `confname` in the
    ** predefined `pathlist[]` and testing if one of the results points to
    ** an existing regular file.
    */
    for (auto tmpl: pathlist) {
	string path = tmpl_subst (tmpl, vars);
	finfo.get_info (path);
	if (finfo.exists() && finfo.regular_file()) {
	    return make_pair (path, true);
	}
    }

    /* No valid configuration file was found. */
    return make_pair ("", false);
}

#ifdef TESTING
//! Testing code for the `findFile()` function.

#include <iostream>
#include <string>

using std::cerr, std::cout, std::endl, std::string;

int main (int argc, char *argv[])
{
    if (argc < 2) {
	cout << "Usage: " << *argv << " filename" << endl;
	return 0;
    }
    string file (argv[1]);
    if (file.find_first_of ('/') != string::npos) {
	cerr << *argv << ": No pathname allowed for filename (" <<
		file << ")" << endl;
	return 64;
    }

    auto [path, found] = findFile (file);

    if (found) {
	cout << "Found pathname: \"" << path << "\"." << endl;
    } else {
	cout << "File \"" << file << "\" not found." << endl;
    }

    return 0;
}

#endif /*TESTING*/
