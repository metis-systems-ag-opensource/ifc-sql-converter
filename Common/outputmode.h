/* ../Common/outputmode.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Datatype for controlling the "verbosity" of a program's output.
**
*/
#ifndef OUTPUTMODE_H
#define OUTPUTMODE_H

enum class OutputMode { nothing, essential, all };

#endif /*OUTPUTMODE_H*/
