/* dump-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Test program for 'dump.{cc,h}'
**
*/

#include <iostream>

#include "dump.h"

using std::cout, std::endl;

int main ()
{
    int a = 34;
    char b[] = "oiro23h23hd3h";
    short c[] = { 2443, 16334, 32767, 93, 0, 0x43, 8932 };

    cout << "Dump at " << c << ", 30 bytes:" << endl;
    cout << dump (&c, 30);

    b[0] = a;
    a = c[0];

    cout << "::: " << *b << " :::" << endl;

    cout << endl;
    cout << "Dump at " << (void *) b << ", 16 bytes:" << endl;
    cout << dump (b, 16);

    cout << endl << "At END" << endl;
    return 0;
}

