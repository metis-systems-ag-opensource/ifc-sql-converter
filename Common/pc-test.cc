/* pc-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Testing the operator '/' on (general) strings ...
**
*/

#include <iostream>
#include <string>
#include "pathcat.h"

using namespace std;

int main ()
{
    string a = "abc", b = "def";

    cout << "a = \"" << a << "\", b = \"" << b << "\"" << endl;
    cout << "a / b = \"" << a / b << "\"" << endl;

    cout << endl;

    a = "abc/", b = "def";

    cout << "a = \"" << a << "\", b = \"" << b << "\"" << endl;
    cout << "a / b = \"" << a / b << "\"" << endl;

    cout << endl;

    a = "abc", b = "/def";

    cout << "a = \"" << a << "\", b = \"" << b << "\"" << endl;
    cout << "a / b = \"" << a / b << "\"" << endl;

    cout << endl;

    string path = "//server-x/proc/self/exe";
    cout << "path = \"" << path << "\"" << endl;

    vector<string> splitted = path_split (path);

    cout << "path(splitted) = (";
    bool first = true;
    for (auto el: splitted) {
	if (first) { first = false; } else { cout << ", "; }
	cout << "\"" << el << "\"";
    }
    cout << ")" << endl;

    string path1 = path_join (splitted);

    cout << "path(rejoined) = \"" << path1 << "\"" << endl;

    cout << endl << "At END" << endl;
    return 0;
}
