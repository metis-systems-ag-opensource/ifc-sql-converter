/* timer-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Small test program for 'timer.{cc,h}'
**
*/

#include <iostream>
#include <stdexcept>
#include <string>

#include "readline.h"

#include "timer.h"

using std::cerr, std::cin, std::cout, std::endl, std::flush;
using std::exception, std::runtime_error;
using std::string, std::to_string, std::operator""s;

int main ()
{
    string wb;
    Timer t;

    cout << "Starting the timer t ..." << endl; t.start();

    cout << "Wait a moment, and then press the RETURN key, please!" << endl;
    readline (cin, wb);

    t.stop(); t.precision (9); wb = t.hms();
    cout << "Timer result: " << wb << endl;

    cout << endl;

    cout << "Starting changing the precisions with truncating..." << endl;
    t.rounding (false);

    for (uint8_t rx = 9; rx > 0; --rx) {
	cout << "Changing the timers output precision to " << (int) rx - 1 <<
		" ..." << endl;
	t.precision (rx - 1);
	cout << "Timer result: " << t.hms() << endl;
    }

    cout << "(All values are truncated – NOT rounded.)" << endl;

    cout << endl;

    cout << "Now again, but with rounding..." << endl;
    t.rounding (true);

    for (uint8_t rx = 9; rx > 0; --rx) {
	cout << "Changing the timers output precision to " << (int) rx - 1<<
		" ..." << endl;
	t.precision (rx - 1);
	cout << "Timer result: " << t.hms() << endl;
    }

    cout << "(Now, all values are rounded.)" << endl;

    cout << endl << "At END" << endl;
    return 0;
}
