/*! \file stepconv.h
**  Interface part of the `stepconv.{cc,h}` module.
**
**  This module exports some conversions routines for values of the EXPRESS
**  type `BINARY`.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
** Interface file for 'stepconv.cc'
** (Conversion from BINARY values (STEP) into std::vector<bool> strings
**  and vice versa.)
**
*/
#ifndef BINCONV_H
#define BINCONV_H

#include <string>
#include <vector>

/*! Importing `mintypes.h` solely because of the type alias `BitString` ... */
#include "mintypes.h"

/*! `bin2string(bitstring)` converts a vector of `bool` values into the
**  STEP representation of a value of the type `BINARY` (see ISO-10303-21).
*/
std::string bin2string (const BitString &v);

/*! `string2bin(bitstring_representation)` converts the string
**  `bitstring_representation` (in the STEP format for `BINARY` values) into
**  a vector of `bool` values.
*/
BitString string2bin (const std::string &s);

#endif /*BINCONV_H*/
