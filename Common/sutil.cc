/* sutil.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Small 'std::string' and 'char *' utilities ...
**
*/

#include <cctype>
#include <cstring>
#include <utility>

#include "sutil.h"

using namespace std;

#if 0
#include <cstring>

bool is_prefix (const char *p, const char *s, bool true_prefix)
{
    while (*p && tolower (*p) == tolower (*s)) { ++p; ++s; }
    return (*p == '\0') && (true_prefix ? *s != '\0' : true);
}

bool is_suffix (const char *p, const char *s, bool true_suffix)
{
    size_t pl = strlen (p), *sl = strlen (s);
    if (pl > sl || (true_suffix && pl == sl)) { return false; }
    s += (sl - pl);
    while (*p && tolower (*p) == tolower (*s)) { ++p; ++s; }
    return (*p == '\0');
}
#endif


//enum class lcase : uint8_t { no, yes };

bool is_prefix (const string &p, const string &s, bool trueprefix, bool nocase)
{
    int cres;
    size_t pl = p.size(), sl = s.size();
    if (pl > sl || (trueprefix && pl == sl)) { return false; }
    if (nocase) {
	cres = lccmp (p.c_str(), s.c_str(), pl);
    } else {
	cres = strncmp (p.c_str(), s.c_str(), pl);
    }
    return cres == 0;
}

bool is_suffix (const string &p, const string &s, bool truesuffix, bool nocase)
{
    size_t pl = p.size(), sl = s.size(), sx;
    if (pl > sl || (truesuffix && pl == sl)) { return false; }
    sx = sl - pl;
    if (nocase) {
	return lccmp (p.c_str(), s.c_str() + sx) == 0;
    } else {
	return strcmp (p.c_str(), s.c_str() + sx) == 0;
    }
}

static void caseconv (const string &in, string &out, int (ccv) (int))
{
    ssize_t insize = in.size(), ix;
    // Without using a temporary variable, an invocation of something like
    // 'caseconv (x, x);' would lead to problems. But because 'string' is
    // a data type which uses dynamically allocated memory, i can (without
    // any loss) use this local variable and (as the last operation of this
    // function) move its internal pointers to 'out', thus assigning it
    // without further copying its content (using the rvalue-ref assignment
    // operator of the 'string' data type.
    string tmp(insize + 1, '\0');
    for (ix = 0; ix < insize; ++ix) {
	// Why here an indexed loop (instead of a range-loop)? Because it seems
	// that the simple index is (currently) *much* faster than a loop over
	// an iterator (what a range-loop actually is).
	tmp[ix] = (char) ccv (in[ix]);
    }
    // Enforce using the rvalue-ref assignment operator (moving only pointer
    // values) ...
    tmp.resize(insize);
    out = move (tmp);
}

void makeupper (string &s)
{
    caseconv (s, s, toupper);
}

string uppercase (const string &s)
{
    string res;
    caseconv (s, res, toupper);
    return res;
}

void makelower (string &s)
{
    caseconv (s, s, tolower);
}

string lowercase (const string &s)
{
    string res;
    caseconv (s, res, tolower);
    return res;
}

/* Make the first character in each word of a given string uppercase and the
** following letters of this word lowercase characters. Any non-letter and
** non-digit character ends a word (even a '_'). If a word starts with a
** digit, all following charac ters of this word are treated as if the word
** had started with a letter.
*/
string ucfirst (const string &s)
{
    size_t ix, ssize = s.size();
    string res (ssize + 1, '\0');
    bool next_to_upper = true;
    for (ix = 0; ix < ssize; ++ix) {
	int ch = s[ix];
	if (isalpha (ch)) {
	    if (next_to_upper) {
		ch = toupper (ch); next_to_upper = false;
	    } else {
		ch = tolower (ch);
	    }
	} else if (isdigit (ch)) {
	    /* Treat a digit as the beginning of a word. */
	    next_to_upper = false;
	} else {
	    /* Any other character is treated as a non-word character, meaning:
	    ** the next letter is made upper case.
	    */
	    next_to_upper = true;
	}
	res[ix] = ch;
    }
    res.resize (ssize);
    return res;
}

/* Short/fast comparison of C-strings. Remember: This function works on ASCII-
** (and probably Code-paged) strings only. For UTF-8 encoded strings, a much
** more complex character extraction is required. But for the cases where this
** function is deployed, a simple ASCII string comparison is sufficient.
*/
int lccmp (const string &l, const string &r, size_t len)
{
    return lccmp (l.c_str(), r.c_str(), len);
}

/* The same as for the 'std::string'-version holds fo the C-string version of
** 'lccmp' (ASCII-comparison).
*/
int lccmp (const char *l, const char *r, size_t len)
{
    int lc = 1, rc = 1;
    // Remember: Each C-string consists of at least one character (the NUL
    // character (aka '\0' in C/C++)).
    if (len == 0) {
	do {
	    // If the standard 'char' is unsigned, '*l' and '*r' are zero-
	    // extended 'lc' and 'rc' (respectively). If 'char' is signed,
	    // '*l' and '*r' are sign extended into 'lc' and 'rc'.
	    lc = tolower (*l++);
	    rc = tolower (*r++);
	    // Conditin check:
	    // - If NUL was found first in 'l', 'l' is probably shorter
	    //   than 'r' or has the same length. The termination condition
	    //   is correct.
	    // - If NUL was found in 'r' instead ('*l != NUL'), '*l' and '*r'
	    //   differ. The characters differ in this case, which means the
	    //   loop terminates. The termination condition is correct.
	} while (lc && lc == rc);
    } else {
	lc = rc = 1;
	while (len-- > 0 && lc != 0 && lc == rc) {
	    lc = tolower (*l++);
	    rc = tolower (*r++);
	}
    }
    // The last examined ("lower cased") character codes are now used for
    // determining the result. Because the result should be a difference
    // between two unsigned character codes, the sign-bits (upper bits of the
    // 'int' values) must be masked out. After this step, the result is simply
    // the difference between the last examined "left" and the last examined
    // "right" character. Using a zero-valued termination character for strings
    // was simply a brilliant idea of the original C developers, because this
    // automatically handles the case that both strings have the same content
    // upto the length of the shorter one of them, and then the longer one
    // has additional (non-zero-valued characters).
    return (lc & 0xFF) - (rc & 0xFF);
}

// Helper functions for 'tmpl_subst()' ...
#if __cplusplus >= 201703L
static string get_tag (const string_view &s, size_t &_pos)
#else
static string get_tag (const string &s, size_t &_pos)
#endif
{
    const size_t ssz = s.size();
    size_t pos = _pos, tagx = 0, tagsz = 10;
    string tag(tagsz + 1, '\0');
    char ch = s[pos], tc = 0;
    // "%tag" or "%{tag}"
    if (ch == '{') {
        // "%{tag}"!
        tc = '}';   // "%{" MUST end with "}"
        ++pos;      // Skip the introducing brace
        if (pos >= ssz) {
            // Failure: "%{" at the end of the string 's' (no closing "}") ...
            _pos = pos; return "";
        }
        ch = s[pos];
    }
    // ch is (probably) the first character of the 'tag'.
    if (isalpha (ch) || ch == '_') {
        // For all alphabetical or numerical (digit) characters or the '_' ...
        do {
            // Resize 'tag' if necessary (double the size until it fits for
            // all characters of the 'tag' ... a trick i learned during my
            // university studies).
            if (tagx >= tagsz) { tagsz *= 2; tag.resize (tagsz + 1, '\0'); }
            // Insert the next character of the 'tag' into 'tag' ...
            tag[tagx++] = ch;
            // Go to the next position in 's', but terminate the 'do'-loop if
            // 'pos' reaches the end of 's'.
            if (++pos >= ssz) { break; }
            // Extract the next character from 's'.
            ch = s[pos];
        } while (isalnum (ch) || ch == '_');
    }
    if (tc != 0) {
        if (pos < ssz && s[pos] == tc) {
            // A sequence "%{tag" must always end with a "}" ...
            ++pos;
        } else {
            // ... otherwise we have a problem.
            tagx = 0; tag[tagx] = '\0';
        }
    }
    tag.resize (tagx); tag.shrink_to_fit(); _pos = pos; return tag;
}

static void subst_append (string &s, size_t &sx, size_t &ssz, const string &v)
{
    size_t vsz = v.size(), newlen = sx + vsz;
    if (newlen >= ssz) {
        do { ssz *= 2; } while (newlen >= ssz);
        s.resize (ssz + 1, '\0');
    }
    for (size_t vx = 0; vx < vsz; ++vx) { s[sx++] = v[vx]; }
}

static void subst_append (string &s, size_t &sx, size_t &ssz, char ch)
{
    if (sx >= ssz) { ssz *= 2; s.resize (ssz + 1, '\0'); }
    s[sx++] = ch;
}


// Substitute placeholders in the format '%{tag}' or '%tag' in a template
// string with values from the given map, where 'tag' is a key in this map.
// placeholders without a match in the map remain unchanged in the result,
// as well as singular '%' and incomplete '%{tag' placeholders. 'tag' must
// conform the specifications of a standard identifier (ASCII), meaning at
// least one letter or '_', followed by a (probably empty) sequence of
// letters, digits and '_'.
#if __cplusplus >= 201703L
string tmpl_subst (const string_view &s, Substitutes &svals, bool keep_unknown)
    // C++17 contains a new data type: 'std::string_view'. This data type
    // allows for the use of (constant) C-strings like C++-strings, because
    // it has most of the same member functions 'std::string' has. This allows
    // for a very good optimisation in the case of 'char *'-values (C-strings)
    // being supplied in the invocation, because the C-strings don't need be
    // copied for creating a C++-string (like in 'std::string (const char *)').
#else
string tmpl_subst (const string &s, Substitutes &svals, bool keep_unknown)
    // Versions of C++ before C++17 must use 'std::string', which means:
    // Copying a C-string content for generating a 'std::string'.
#endif
{
    // The good thing here is: the implementation remains the same - regardless
    // of the header having a 'std::string' or 'std::string_view' first
    // argument.
    const size_t ssz = s.size();
    size_t ressz = ssz, ix, resx = 0;
    string tag, tmp(ressz + 1, '\0');
    for (ix = 0; ix < ssz;) {
	char ch = s[ix];
	if (ch == '%') {
	    ++ix; if (ix >= ssz) { subst_append (tmp, resx, ressz, ch); break; }
	    if (s[ix] == ch) {
		++ix; subst_append (tmp, resx, ressz, ch); continue;
	    }
	    size_t jx = ix; tag = get_tag (s, jx);
	    if (! tag.empty() && svals.count (tag) > 0) {
		// Placeholder name ('tag') found in the variables
		subst_append (tmp, resx, ressz, svals[tag]);
	    } else if (keep_unknown) {
		// Placeholder name not found in the variables, but "keeping
		// unknown placeholders" is in effect
		--ix;
		if (resx + (jx - ix) >= ressz) {
		    ressz *= 2; tmp.resize (ressz + 1, '\0');
		}
		while (ix < jx) { tmp[resx++] = s[ix++]; }
	    }
	    ix = jx;
	} else {
	    ++ix; subst_append (tmp, resx, ressz, ch);
	}
    }
    string res (tmp.c_str());
    return res;
}

static const char *hexdigits (bool upper)
{
    return (upper ? "0123456789ABCDEF" : "0123456789abcdef");
}

string u2hex (unsigned long v, unsigned int fw, bool upper)
{
    string res, tmp;
    const char *hexdg = hexdigits (upper);
    int fwc = (int) fw;
    do { tmp = hexdg[v & 0xF] + tmp; v >>= 4; --fwc; } while (v > 0);
    while (fwc > 0) { tmp = '0' + tmp; --fwc; }
    res.reserve (tmp.size() + 1); res = tmp;
    return res;
}

string i2hex (long v, unsigned int fw, bool upper)
{
    if (v > 0) { return u2hex ((unsigned long) v, fw, upper); }
    string res, tmp;
    const char *hexdg = hexdigits (upper);
    int fwc = (int) fw;
    do { tmp = hexdg[v & 0xF] + tmp; v >>= 4; --fwc; } while (v != -1l);
    while (fwc > 0) { tmp = 'F' + tmp; --fwc; }
    res.reserve (tmp.size() + 1); res = tmp;
    return res;
}

void trim (std::string &s, const std::string &chars = " ")
{
    size_t lp = s.find_first_not_of (chars);
    size_t rp = s.find_last_not_of (chars);
    if (rp < s.size()) { s.erase (rp + 1); }
    if (lp < s.size()) { s.erase (0, lp); }
}

string trim (const string &in, const string &chars)
{
    string res (in);
    trim (res);
    return res;
}

/* `isws(string, index)` checks for the character at position `index`
** in `string` being either a blank (' ') or a HT ('\t') character.
** (`isws` ==> `is_white_space`).
*/
bool isws (const string &s, size_t ix)
{
    if (ix >= s.size()) { return false; }
    char ch = s[ix];
    return ch == '\t' || ch == ' ';
}

/* `nows(string, index)` checks for the character at position `index`
** in `string` beine any other than blank (' ') or HT ('\t').
** (`nows` ==> 'no_white_space')
*/
bool nows (const string &s, size_t ix)
{
    if (ix >= s.size()) { return false; }
    char ch = s[ix];
    return ch != '\t' && ch != ' ';
}

/* `eos(string, index)` checks for the `index` pointing behind the end
** of the `string`.
** (`eos` ==> `end_of_string`)
*/
bool eos (const string &s, size_t ix)
{
    return ix >= s.size();
}
