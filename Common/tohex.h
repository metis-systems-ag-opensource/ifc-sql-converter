/* tohex.h
**
** $Id: 723d5d95781b29d2b46e19c43e9410af735d2c84 $
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'tohex.{cc,h}'
** (Convert a value (unsigned long) into a base-N number (std::string))
**
*/
#ifndef TOHEX_H
#define TOHEX_H

#include <string>

std::string to_base_n (unsigned long x, unsigned base,
		       unsigned width = 1, char fc = ' ');

std::string to_hex (unsigned long x, unsigned width = 1, char fc = ' ');

std::string to_dec (unsigned long x, unsigned width = 1, char fc = ' ');

std::string to_oct (unsigned long x, unsigned width = 1, char fc = ' ');

std::string to_bin (unsigned long x, unsigned width = 1, char fc = ' ');

std::string ptr2str (const void *x);

#endif /*TOHEX_H*/
