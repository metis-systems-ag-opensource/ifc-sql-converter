/* config.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** This file is the implementation part of the `config.{cc,h}` module. It
** contains the complete implementation of the `Config` class (member functions
** and the structure of the member variable (which is hidden from the
** interface).
**
*/

#include <cctype>
#include <iostream>
#include <fstream>

#include <map>
#include <stdexcept>
#include <utility>

#include "fnutil.h"
#include "pathcat.h"
#include "readline.h"
#include "sutil.h"
#include "sysutils.h"

#include "config.h"

// These 'using' declarations (imports of objects from the namespace `std`)
// require a C++-compiler which implements the C++17 standard.
using std::cerr, std::endl;
using std::ifstream;
using std::map;
using std::set;
using std::domain_error, std::runtime_error;
using std::string, std::to_string, std::operator""s;
using std::move;

// What to do with the unknown placeholders?


namespace {
//! The Implementation of the internal implementation of `Config`, visible only
//! within this file.
struct ConfigImplX {
    string file;	//!< Pathname of the current configuration or empty.
    ConfData val;	//!< The configuration data (see `config.h` for
			//!< `ConfData`).
};
} /*namespace*/

struct Config_impl : ConfigImplX { };

static bool eoln (const string &s, size_t ix)
{
    return ix >= s.size();
}

// `is_word1st (*ch*)` returns `true` if *ch* is a valid first character of an
// identifier.
static bool is_word1st (char ch)
{
    return isalpha (ch);
}

// `is_wordch (*ch*)` returns `true` if *ch* is a valid character of an
// identifier.
static bool is_wordch (char ch)
{
    return isalnum (ch) || ch == '_';
}

//! Helper type (alias) for making the function prototype of 'parse_line()'
//! more clear.
struct CfgItem {
    string name, value;
    bool append;
};

/* `parse_line(...)` parses a single line of input (given as a string). The
** return value is a tri-state value:
**  - -1 indicates an error,
**  -  0 indicates that the line was empty or a comment/pragma, and
**  - +1 indicates that a configuration item was found which is returned
**    as <key, value>-pair through the parameter `out`.
** The additional two parameters are used if a '#s:on' (alt: '#substitute on')
** of '#s:off' (alt: '#substitute off') pragma was found; the first one being
** `true` indicates that `#s:on` was found, and `false` that `#s:off` was
** found. The second one is set to `true` only for an `#s:on`, and only if
** this `#s:on' was followed by `,u:keep` (alt: `,unknown keep`); it is set
** to `false` if `#s:on` is followed by `,u:remove` (alt: `,unknown remove`).
** This means:
** `#s:on,u:keep` sets `item_subst` and `keep_unknown` to `true`, whereas
** `#s:on,u:remove` sets `item_subst` to `true` and `keep_unknown` to `false`.
*/
static int parse_line (const string &line, CfgItem &out, bool &item_subst,
		       bool &keep_unknown)
{
    unsigned px = 0, qx = 0, rx = 0, sx = 0;
    bool spfx = false, append_to = false;
    if (is_prefix ("#substitute on", line)
    || (spfx = is_prefix ("#s:on", line))) {
	item_subst = true;
	string pltail = line.substr ((spfx ? 5 : 14));
	if (is_prefix (",unknown keep", pltail) ||
	    is_prefix (",u:keep", pltail)) {
	    keep_unknown = true;
	} else if (is_prefix (",unknown remove", pltail) ||
		   is_prefix (",u:remove", pltail)) {
	    keep_unknown = false;
	}
	return 0;
    }
    if (is_prefix ("#substitute off", line) || is_prefix ("#s:off", line)) {
	// Reset to the defaults!
	item_subst = false; keep_unknown = true; return 0;
    }
    while (isws (line, px)) { px++; }
    if (eos (line, px) || line[px] == '#') { return 0; }
    if (! is_word1st (line[px])) { return -1; }
    qx = px + 1; while (! eos (line, qx) && is_wordch (line[qx])) { ++qx; }
    rx = qx; while (isws (line, rx)) { ++rx; }
    if (eos (line, rx) || (line[rx] != '=' && line[rx] != '+')) { return -1; }
    if (line[rx] == '+') {
	// If the character found was '+', the next character _must_ be a '=',
	// otherwise we have a syntax error.
	++rx; if (eoln (line, rx) || line[rx] != '=') { return -1; }
	// '+=' found means: Append to an existing item (with a blank between
	// both) _or_ create a new item with the given value. This is marked
	// by returning 'true' as the third item of the tuple 'CfgItem'
	// (through the 'out' parameter).
	append_to = true;
    }
    ++rx; while (isws (line, rx)) { ++rx; }
    if (eos (line, rx)) { return -1; }
    sx = line.size();
    while (sx > rx && isws (line, sx - 1)) { --sx; }
    out = CfgItem ({ line.substr (px, qx - px),
		     line.substr (rx, sx - rx),
		     append_to });
    return 1;
}

/* `parse_file()' parses an input file stream, using `parse_line()` for doing
** the heavy work.
**
** If `parse_line()` returns `true` through its third argument, substitution
** is performed on each configuration value following the pragma-line, until
** either `#s:off` was found or the end of the file stream was reached. If
** the fourth argument of the `parse_line()` invocation is also true on return
** unknown `%name` and `%{name}` references remain unchanged in configuration
** values.
** Substitution works for a certain number of placeholders which are passed
** through the second parameter `vars`. The successfully parsed configuration
** items are returned through ths third parameter `out`. The value returned
** from an invocation of `parse_line()` is the number of (parse) errors found
** during parsing the complete input file stream.
*/
static int parse_file (ifstream &in, Substitutes &vars, ConfData &out)
{
    bool item_subst = false, keep_unknown = true;
    int lc = 0, ec = 0;
    out.clear();	// Forcing 'out' to be a pure output parameter
    while (! in.eof()) {
	string line; CfgItem it;
	readline (in, line); ++lc;
	switch (parse_line (line, it, item_subst, keep_unknown)) {
	    // 'parse_line()' decides the settings of 'item_subst' and
	    // 'keep_unknown' by simply reading a pragma (meta-configuration)
	    // line. A pragma '#substitute on[,unknown keep]' (short:
	    // '#s:on,u:keep') simply says that for the following configuration
	    // items the substitution of certain placeholders takes place, and
	    // that unknown placeholders remain in the item, whereas
	    // '#substitute on,unknown remove (short: '#s:on,u:remove') enables
	    // substitution, but enforces the removal of unknown placeholders.
	    // '#substitute off' (short: '#s:off') resets the both states to
	    // their defaults.
	    case 0:	// Blank or comment line
		break;
	    case 1: {	// Valid item found
		if (item_subst) {
		    it.value = tmpl_subst (it.value, vars, keep_unknown);
		}
		if (it.append) {
		    // Append to an existing item.
		    auto cpx = out.find (it.name);
		    if (cpx != out.end()) {
			cpx->second = cpx->second +
				      (it.value[0] == ',' ? "" : " ") +
				      it.value;
			break;
		    }
		}
		auto [dummy, done] = out.emplace (it.name, it.value);
		if (! done) {
		    cerr << "In configuration line " << lc <<
			    ": Ambiguous item '" << it.name << "'." << endl;
		    ++ec;
		}
		break;
	    }
	    default:	// (aka -1) Syntax error
		cerr << "In configuration line " << lc << ": Syntax error." <<
			endl;
		++ec;
	}
    }
#if 0	// The error output is replaced by raising an exception now!
    if (ec > 0) {
	cerr << ec << " errors found in configuration." << endl;
    }
#endif
    return ec;
}

// Constructor function of the `Config` class. It creates a `Config_impl`
// object and sets the member variable `Config::_data` to point to this
// object.
Config::Config()
    : _data(new Config_impl)
{
    // _data = new Config_impl;
}

// Constructor function of the `Config` class. This function copies the
// configuration items from the `Config` object `x` into the just created
// object.
Config::Config (const Config &x)
    : _data(new Config_impl (*x._data))
{
    // _data = new Config_impl; *_data = *(x._data);
}

// Constructor function of the `Config` class. This function moves the
// content of the `Config` object `x` to the new object (meaning: copies the
// pointer and then invalidates the `_data` pointer of `x`).
Config::Config (Config &&x)
    : _data(x._data)
{
    x._data = nullptr;
}

// Destructor. It dealloctes the memory the member variable `Config::_data`
// points to.
Config::~Config()
{
    if (_data) { delete _data; }
}

// `operator=(const Config &)`
// In contrast to the constructor functions, the object on the left hand side
// of the assignment operator probably has a defined value, so the object it
// points to must be deallocated before the pointer can be set to a new
// (dynamically created) object.
Config &Config::operator= (const Config &x)
{
    if (_data) { delete _data; _data = nullptr; }
    _data = new Config_impl; *_data = *(x._data);
    return *this;
}

// `operator=(Config&&)`
// The same is true for the "move" assignment operator. But here, the internal
// object `_data` from the right hand side becomes the new value of the `_data`
// pointer of the left hand side (the object this assignment operator was
// defined for), and the `_data` pointer of the right hand side is invalidated
// thereafter.
Config &Config::operator= (Config &&x)
{
    if (_data) { delete _data; _data = nullptr; }
    _data = x._data; x._data = nullptr;
    return *this;
}

// `load(file)` opens the file the pathname `file` referes, parses its content
// (using the local function `parse_file()'), and then stores the parsed values
// and the pathname `file` in the member variable `Config::_data`.
void Config::load (const string &file)
{
    ConfData confdata;
    ifstream in;
    if (! _data) { _data = new Config_impl; }
    in.open (file);
    if (! in.is_open()) {
	throw runtime_error
	    ("Attempt to open configuration file '" + file + "' failed.");
    }
    string homedir = userhome();
    Substitutes vars {
	{ "cfdir", dirname (file) },
	{ "confdir", dirname (file) },
	{ "progdir", appdir() },
	{ "home", homedir },
	{ "userconfig", homedir / ".config" },
	{ "sysconfig", ""s / "etc" },
    };
    int ec = parse_file (in, vars, confdata);
    in.close();
    if (ec > 0) {
	throw runtime_error
	    ("Found " + to_string (ec) + " error" + (ec == 1 ? "" : "s") +
	     " in configuration file '" + file + "'.");
    }
    _data->file = file;
    _data->val = move (confdata);
}

// `loaded()` returns `true` if a configuration file was "loaded" and `false`,
// otherwise.
bool Config::loaded()
{
    return _data && ! _data->file.empty();
}

// `has(name)` searches for an item with the given `name` in the
// configuration, indicating success by returning `true` and failure by
// returning `false`.
bool Config::has (const string &name) const
{
    bool found = false;
    if (_data) {
	auto &val = _data->val;
	auto pair_it = val.find (name);
	if (pair_it != val.end()) { found = true; }
    }
    return found;
}

// `operator[](name)` returns the value of the configuration item whose
// name is `key`.  If the configuration doesn't have an item with a
// matching name, the empty string is returned instead.
// Because the result is always immutable, a small optimisation could be added:
// Instead of always creating a new empty string for an undefined configuration
// item, a static valiable `mtstrg`, initialised with an empty string, is used
// as return value instead.
const string &Config::operator[] (const string &name) const
{
    static string mtstrg;    // Automatically initialises to the empty string
    if (! _data) { throw runtime_error ("No configuration loaded."); }
    auto pair_it = _data->val.find (name);
    return (pair_it == _data->val.end() ? mtstrg : pair_it->second);
}

// `at(*key*)` returns the value of the configuration item whose name is
// `key`. If the configuration doesn't have an item with a matching name,
// an exception (`std::runtime_error`) is raised instead.
const string &Config::at (const string &name) const
{
    if (! _data) { throw runtime_error ("No configuration loaded."); }
    auto pair_it = _data->val.find (name);
    if (pair_it != _data->val.end()) { return pair_it->second; }
    throw domain_error ("No element '" + name + "' found.");
}

// `filename()` returns the pathname of the file loaded. An empty configuration
// leads to a `std::runtime_error()` exception. If there was no configuration
// loaded yet, an empty string is returned.
const std::string &Config::filename() const
{
    if (! _data) { throw runtime_error ("No configuration loaded."); }
    return _data->file;
}

// This utility function can be used to "define" mandatory configuration items.
// It checks for the existance of each configuration item whose name is a
// member of the argument `items`. If all elements of `items` are represented
// in the configuration a `true` flag is returned. If elements of `items` are
// missing th the configuration, a `false` flag is returned, and the second
// (output) argument `missing` contains a list (set) of the names of all
// missing configuration items.
Config::MissingState Config::check_missing (const set<string> &expected) const
{
    bool has_missing = false;
    set<string> missing;
    missing.clear();
    for (auto it: expected) {
	if (! _data || _data->val.count (it) == 0) {
	    missing.insert (it); has_missing = true;
	}
    }
    return MissingState (has_missing, missing);
}

// `items()` returns the names of all existing configuration items.
ConfItems Config::item_names() const
{
    ConfItems res;
    if (_data) {
	for (auto &[key, val]: _data->val) {
	    res.insert (key);
	}
    }
    return res;
}

// `data()` returns an immutable reference to all existing configuration items.
const ConfData &Config::data() const
{
    return _data->val;
}
