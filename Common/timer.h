/*! \file timer.h
**  Interface part of the `timer.{cc,h}` module.
**
**  `timer.{cc,h}` implements a time measuring class which is used by the
**  STEP program.
**
** \remark
**  This module doesn't implement a timer (like an "alarm clock" timer). Its
**  sole purpose is the measuring of execution times.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Small timer class (interface part).
** THIS IS NO TIMER like an alarm timer, but a TIME MEASUREMENT CLASS!
** (Or: This is more a start/stop watch instead of an egg-timer.)
**
*/
#ifndef TIMER_H
#define TIMER_H

#include <cstdint>

/*! `TimerType` is an enumeration type for specifying the type of clock used
**  for time measuring. (Linux supports some different clocks, like the
**  *real time* or the *monotone" clock.
**
** \remark
**  This module is general enugh for using it in other contexts, but the
**  default clock used is the `realtime` clock.
*/
enum TimerType : uint8_t {
    realtime = 0,
    monotone,
#ifdef Linux
    alarm,
    coarse, tai,
    monotone_coarse,
    monotone_raw,
    boottime,
    boottime_alarm,
    process_cputime_id,
    thread_cputime_id
#endif /*Linux*/
};

/*! Hidden implementation of the `Timer` object. */
struct TimerImpl;

/*! The `Timer` class provides a way for runtime measurements, either for a
**  program as a whole, of for some phases of it.
*/
class Timer {
public:
    /*! Default constructor when no argument is given; a value
    ** constructor which allows for the specification of the clock to be
    ** used for time measurement, otherwise.
    */
    Timer (TimerType ttype = TimerType::realtime);

    /*! Value constructor which allows for the precision of the (C string)
    **  output to be set (and optionally the type of the clock used for time
    **  measurement).
    */
    Timer (uint8_t prec, TimerType ttype = TimerType::realtime);

    /*! Copy constructor. */
    Timer (const Timer &x);

    /*! Move constructor.
    **
    ** \remark
    **  Because of the implementation of the timer, there is a difference
    **  between *Copy* and *Move* constructors. It is the construction of a
    **  new `Timer_impl` object with copying all values from the original one
    **  versus moving a `Timer_impl` object.
    */
    Timer (Timer &&x);

    /*! Destructor.
    **
    ** \remark
    **  ~Timer() destroys an existing `Timer_impl` object. If there is no such
    **  object, this function does nothing.
    */
    ~Timer();

    /*! Copy assignment operator. */
    Timer &operator= (const Timer &x);

    /*! Move assignment operator. */
    Timer &operator= (Timer &&x);

    /*! `precison(...)` sets the output precision to a new value. */
    void precision (uint8_t prec);

    /*! `precision()` returns the current output precision. */
    uint8_t precision() const;

    /*! `rounding(true)` activates rounding (versus truncating) for timer
    **  output values. `rounding(false) reverses this effect.
    */
    void rounding (bool rf);

    /*! `rounding()` returns `true` if rounding is acrivated and `false` for
    **  truncating.
    */
    bool rounding() const;

    /*! `start()` reads the current time from the clock specified during the
    **  construction of this object and stores it in the object.
    **
    ** \remark
    **  Because `clock_gettime()' is used the maximum precision is one nano
    **  second depending on the precision of the running operating system.
    */
    void start();

    /*! `stop()` reads the current time from the clock specified during the
    **  construction of this object and stores it in the object.
    */
    void stop();

    /*! `seconds()` returns the time elapsed between the invocations of
    **  `start()` and `stop()`.
    **
    ** \remark
    **  The result is a double because of the (nano second) fractions of
    **  the elapsed time.
    */
    double seconds() const;

    /*! `hms(buf, bufsz)` writes the elapsed time in the format
    **  *H* h *M* m *S* . *fractions* s  into the buffer `buf`
    **  (of size `bufsz`).
    **
    **  \remark
    **   Zero parts for *x*, *y*, *z* and *fractions* are omitted in the
    **   result. If all parts are zero, the output `0s` is generated.
    */
    void hms (char *xbuf, size_t sbufsz) const;

    /*! `hms()` applies `hms(...)` to an internal buffer, whose address is
    *   then returned.
    */
    const char *hms();

    /*! @returns `true` if `start()` was invoked, but not `stop()`, and
    **  `false` in any other case.
    */
    bool running() const;
private:
    /*! Hidden timer implementation. */
    TimerImpl *val;
};

#endif /*TIMER_H*/
