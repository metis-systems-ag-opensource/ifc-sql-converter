/* timer.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Small timer class (implementation part)
** THIS IS NO TIMER like an alarm timer, but a TIME MEASUREMENT CLASS!
** (Or: This is more a start/stop watch instead of an egg-timer.)
**
*/

#include <cstdio>
#include <cstring>
#include <stdexcept>
#include <time.h>

#include "timer.h"

#define BUFSZ 40
#define MAXPREC 9

namespace {
struct TimerImplX {
    /*! start time, set with `start()`. */
    struct timespec startpoint;

    /*! stop point, set with `stop()`. */
    struct timespec stoppoint;

    /*! The timer type specified during the construction of the `Timer` object.
    */
    TimerType ttype;

    /*! The internal buffer used for generating the `hms()` result. */
    char buf[BUFSZ];

    /*! The output precision of the `hms()` output. */
    uint8_t prec;

    /* `true` if `start()` but not yet `stop()` was invoked; `false`,
    ** otherwise.
    */
    bool running;

    /* `true` if rounding to the desired precision is used; `false` if
    ** truncating all fractional digits behind the desired precision is
    ** used.
    */
    bool rounding;
};
} /*namespace*/

struct TimerImpl : TimerImplX { };

Timer::Timer (TimerType ttype)
    : val(new TimerImpl)
{
    val->startpoint = timespec {0, 0};
    val->stoppoint = timespec {0, 0};
    val->ttype = ttype;
    memset (val->buf, 0, BUFSZ);
    val->prec = MAXPREC;
    val->running = false; val->rounding = true;
}

Timer::Timer (uint8_t prec, TimerType ttype)
    : val(new TimerImpl)
{
    if (prec > MAXPREC) {
	char buf[50];
	snprintf (buf, sizeof(buf), "Precision out of range (0..9): %d", prec);
	throw std::runtime_error (buf);
    }
    val->startpoint = timespec {0, 0};
    val->stoppoint = timespec {0, 0};
    val->ttype = ttype;
    memset (val->buf, 0, BUFSZ);
    val->prec = prec;
    val->running = false; val->rounding = true;
}

Timer::Timer (const Timer &x)
    : val(new TimerImpl)
{
    val->startpoint = x.val->startpoint;
    val->stoppoint = x.val->stoppoint;
    val->ttype = x.val->ttype;
    memcpy (val->buf, x.val->buf, sizeof(val->buf));
    val->prec = x.val->prec;
    val->running = x.val->running;
    val->rounding = x.val->rounding;

}

Timer::Timer (Timer &&x)
    : val(x.val)
{
    x.val = nullptr;
}

Timer::~Timer()
{
    delete val;
}

Timer &Timer::operator= (const Timer &x)
{
    val->startpoint = x.val->startpoint;
    val->stoppoint = x.val->stoppoint;
    val->ttype = x.val->ttype;
    memcpy (val->buf, x.val->buf, sizeof(val->buf));
    val->prec = x.val->prec;
    val->running = x.val->running;
    val->rounding = x.val->rounding;
    return *this;
}

Timer &Timer::operator= (Timer &&x)
{
    delete val; val = x.val; x.val = nullptr;
    return *this;
}

void Timer::precision (uint8_t prec)
{
    val->prec = (prec > MAXPREC ? MAXPREC : prec);
}

uint8_t Timer::precision() const
{
    return val->prec;
}

void Timer::rounding (bool rf)
{
    val->rounding = rf;
}

bool Timer::rounding() const
{
    return val->rounding;
}

static clockid_t ttype2clockid (TimerType ttype)
{
    switch (ttype) {
	case realtime:			return CLOCK_REALTIME;
	case monotone:			return CLOCK_MONOTONIC;
#ifdef Linux
	case alarm:			return CLOCK_REALTIME_ALARM;
	case coarse:			return CLOCK_REALTIME_COARSE;
	case tai:			return CLOCK_TAI;
	case monotone_coarse:		return CLOCK_MONOTONIC_COARSE;
	case monotone_raw:		return CLOCK_MONOTONIC_RAW;
	case boottime:			return CLOCK_BOOTTIME;
	case boottime_alarm:		return CLOCK_BOOTTIME_ALARM;
	case process_cputime_id:	return CLOCK_PROCESS_CPUTIME_ID;
	case thread_cputime_id:		return CLOCK_THREAD_CPUTIME_ID;
#endif /*Linux*/
	default:			return CLOCK_REALTIME;
    }
}

void Timer::start()
{
    clockid_t clockid = ttype2clockid (val->ttype);
    clock_gettime (clockid, &val->startpoint);
    val->stoppoint.tv_sec = 0;
    val->stoppoint.tv_nsec = 0;
    val->running = true;
}

void Timer::stop()
{
    clockid_t clockid = ttype2clockid (val->ttype);
    clock_gettime (clockid, &val->stoppoint);
    val->running = false;
}

static void timer_diff (TimerType t, bool running,
			const timespec &sp, const timespec &ep,
			timespec &x)
{
    if (running) {
	clockid_t clockid = ttype2clockid (t);
	clock_gettime (clockid, &x);
    } else {
	x.tv_nsec = ep.tv_nsec;
	x.tv_sec = ep.tv_sec;
    }
    if (sp.tv_nsec > x.tv_nsec) { x.tv_sec -= 1; x.tv_nsec += 1000000000; }
    x.tv_nsec -= sp.tv_nsec; x.tv_sec -= sp.tv_sec;
}

double Timer::seconds() const
{
    timespec x;
    timer_diff (val->ttype, val->running, val->startpoint, val->stoppoint, x);
    return (double) x.tv_sec + (double) x.tv_nsec / 1000000000.0;
}

void Timer::hms (char *xbuf, size_t xbufsz) const
{
    time_t sec;
    long ns;
    int d, h, m, s;
    int prec = val->prec, cc;
    timespec x;
    timer_diff (val->ttype, val->running, val->startpoint, val->stoppoint, x);
    if (! xbuf) { throw std::runtime_error ("No buffer specified"); }
    if (xbufsz < BUFSZ) {
	throw std::runtime_error ("Specified buffer is not long enough");
    }
    if (val->rounding && (cc = prec) < MAXPREC) {
	long mx = 5; while (++cc < MAXPREC) { mx *= 10; }
	x.tv_nsec += mx; 
	if (x.tv_nsec >= 1000000000) { ++x.tv_sec; x.tv_nsec %= 1000000000; }
    }
    sec = x.tv_sec;
    d = sec / 86400; sec -= 86400 * d;
    h = sec / 3600; sec -= 3600 * h;
    m = sec / 60; sec -= 60 * m;
    s = sec; ns = x.tv_nsec;
    char *p = xbuf, *e = p + xbufsz;
    if (d > 0) { snprintf (p, (size_t)(e - p), "%dd", d); p += strlen (p); }
    if (h > 0) { snprintf (p, (size_t)(e - p), "%dh", h); p += strlen (p); }
    if (m > 0) { snprintf (p, (size_t)(e - p), "%dm", m); p += strlen (p); }
    if (s > 0 || (ns > 0 && prec > 0) || (d == 0 && h == 0 && m == 0)) {
	snprintf (p, (size_t)(e - p), "%d", s); p += strlen (p);
	if (prec > 0) {
	    int cc = prec;
	    while (cc < MAXPREC) { ++cc; ns /= 10; }
	    snprintf (p, (size_t)(e - p), ".%0*ld", prec, ns); p += strlen (p);
	}
	if ((size_t) (e - p) >= 2) { *p++ = 's'; *p = '\0'; }
    }
}

const char *Timer::hms()
{
    hms (val->buf, sizeof(val->buf));
    return val->buf;
}

bool Timer::running() const
{
    return val->running;
}
