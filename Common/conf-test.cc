/* conf-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Small test program for 'config.{cc,h}'
**
*/

#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>

#include "fnutil.h"
#include "pathcat.h"
#include "sysutils.h"

#include "config.h"

using std::cerr, std::cout, std::endl, std::flush;
using std::exception, std::runtime_error;
using std::string, std::to_string;

static const char *progname = NULL;

static void set_progname (const char *arg0)
{
    const char *p = strrchr (arg0, '/');
    if (p) { ++p; } else { p = arg0; }
    progname = p;
}

int main (int argc, char *argv[])
{
    string conffile (appdir() / "conf-test.conf");
    Config cfg;

    set_progname (*argv);

    cout << "Loading configuration file \"" << conffile << "\"" << endl;
    try {
	cfg.load (conffile);
	cout << "Configuration loaded" << endl << endl;
    } catch (exception &e) {
	cerr << progname << ": The configuration file contains errors" << endl;
	cerr << "Exception message: " << e.what() << endl;
	cerr << "... Continuing nonetheless ..." << endl << endl;
    }
    auto cfgitems = cfg.item_names();

    for (auto &ciname: cfgitems) {
	cout << ciname << ": |" << cfg[ciname] << "|" << endl;
    }

    cout << endl << "At END" << endl;
    return 0;
}
