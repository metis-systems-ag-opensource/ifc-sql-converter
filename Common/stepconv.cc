/* stepconv.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Conversion from BINARY values (STEP) into std::vector<bool> strings
** and vice versa.
**
*/

#include <iostream>
using std::cerr;

#include <stdexcept>

#include "stepconv.h"

using std::string, std::vector, std::runtime_error, std::domain_error;

string bin2string (const vector<bool> &v)
{
    static const char hexdigit[] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };
    size_t btsz = v.size();
    size_t nbsz = (btsz + 3) >> 2;
    size_t rbsz = (nbsz << 2) - btsz;
    uint8_t nibble = 0; 
    string res (1, hexdigit[rbsz]);
    res.reserve (nbsz + 1);
    for (auto bit: v) {
	nibble = (nibble << 1) | (bit ? 1 : 0);
	rbsz = (rbsz + 1) % 4;
	if (rbsz == 0) { res.push_back (hexdigit[nibble]); nibble = 0; }
    }
    return res;
}

vector<bool> string2bin (const string &s)
{
    static int hexdigitvalue[256];
    static bool initialised = false;
    if (! initialised) {
	const string hexdigits = "0123456789ABCDEF";
	for (auto &x: hexdigitvalue) { x = -1; }
	for (int ix = 0; ix < (int) hexdigits.size(); ++ix) {
	    int ch = (int) hexdigits[ix];
	    hexdigitvalue[ch] = ix;
	}
	initialised = true;
    }
    size_t ssize = s.size();
    vector<bool> res;
    res.reserve ((ssize + 1) * 4);
    int xdv;
    unsigned rbsz;
    if (s.empty()) { goto INVARG; }
    if ((xdv = hexdigitvalue[(unsigned) s[0]]) < 0 || xdv > 3) { goto INVARG; }
    rbsz = (unsigned) xdv;
    if (rbsz > 0 && ssize < 2) { goto INVARG; }
    for (size_t ix = 1; ix < ssize; ++ix) {
	unsigned ch = (unsigned) s[ix];
	if ((xdv = hexdigitvalue[ch]) < 0) { goto INVARG; }
	int mask = 1 << (4 - rbsz); rbsz = 0;
	while ((mask >>= 1) > 0) {
	    res.push_back (xdv & mask ? true : false);
	}
    }
    return res;
INVARG:
    throw domain_error ("Invalid representation of a STEP-binary.");
}

// Testing part for this module. (Not each module needs its own test program;
// sometimes, a macro can be used for making the module itself a test program.)
#ifdef TESTING

#include <iostream>

using std::ostream, std::cout, std::cerr, std::endl;

ostream &operator<< (ostream &out, const vector<bool> &v)
{
    bool first = true;
    out << '[';
    for (auto bit: v) {
	if (first) { first = false; } else { out << ", "; }
	out << (bit ? "true" : "false");
    }
    out << ']';
    return out;
}

int main()
{
    vector<bool> bits {true, false, true, true, false, true};
    string str ("317E");

    cout << "bits = " << bits << " ==> " << bin2string (bits) << endl;
    cout << "str = " << str << " ==> " << string2bin (str) << endl;

    cout << "bin2string (vector<bool>()) = " <<
	    bin2string (vector<bool>()) << endl;
    cout << "string2bin (\"0\") = " << string2bin ("0") << endl;

    cout << "bin2string (vector<bool>{true, false, false, true}) = " <<
	    bin2string (vector<bool>{true, false, false, true}) << endl;
    cout << "string2bin (\"033\") = " << string2bin ("033") << endl;

    cout << endl << "At END" << endl;
    return 0;
}

#endif /*TESTING*/
