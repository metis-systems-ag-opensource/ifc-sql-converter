/* sutil-test1.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Small test program for the 'to_hex()' implementations of the 'sutil.{cc,h}'
** module.
**
*/

#include <iostream>

#include "sutil.h"

using std::cout, std::endl;
using std::string;

int main ()
{
    int x = -3141;
    unsigned int y = 2718;
    cout << "u2hex (343413917) = " << u2hex (343413917) << endl;
    cout << "i2hex (343413917) = " << i2hex (343413917) << endl;
    cout << "x = " << x << ", i2hex (x) = " << i2hex (x, 5) << endl;
    cout << "y = " << y << ", u2hex (y) = " << u2hex (y, 5) << endl;

    cout << endl << "At END" << endl;
    return 0;
}
