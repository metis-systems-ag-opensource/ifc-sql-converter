/*! \file filesep.h
**  Define the path separator depending on the platform where the code which
**  uses this (header-only) module is compiled. The path separator is:
**   - `\\` for Windows and
**   - `/` for any other platform (including Linux).
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Need the pathname element separator ('\' in Windows, '/' elsewhere)
** somewhere.
**
*/
#ifndef FILESEP_H
#define FILESEP_H

#if defined(__WIN32__) || defined(__WIN64__)
//! Windows version of the path separator
#  define PATHSEP '\\'
#else
//! Unix/Linux version of the path separator
#  define PATHSEP '/'
#endif

#endif /*FILESEP_H*/
