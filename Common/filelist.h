/*! \file filelist.h
**  Interface part of the `filelist.{cc,h}` module.
**
**  The module `filelist.{cc,h}` exports a function
**    filelist (const char *dir, chost char *pattern, bool recursive = false)
**  which reads a directory `drectory` and returns each entry which matches
**  `pattern`, save from directory entries. This is somewhat similar to the
**  `libc` function `glob()`, but differs in the fact that only entries in
**  `directory` (and any subdirectory of `directory` iff `recursive` is set
**  to `true`) are returned, meaning that `directory` itself doesn't undergo
**  any pattern matching. The result is a `Fifo<std::string>` containing the
**  full pathnames of all entries matching `pattern` in `directory` (and
**  probably any sub-directories of `directory`).
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'filelist.cc'
** ("Load" a list of files which match a specific filename pattern from a given
**  directory and all of its sub-directories)
**
*/
#ifndef FILELIST_H
#define FILELIST_H

#include <string>

#include "fifo.h"

/*! `filelist()` searches for (non-directory) entries in
**  a given directory which match a given pattern. If `recursive` is given (as
**  `true`), each sub-directory of `dir` is scanned, too.
**
**  @param dir - The directory being scanned.
**  @param pattern - The pattern to be searched for.
**  @param recursive - Include sub-directories of `dir` into the search
**    recursively.
**
**  The return value is a list of pathnames of all entries which match the
**  given `pattern`. These pathnames consist of `dir` path-concatenated with
**  the entries found.
*/
Fifo<std::string> filelist (const char *dir, const char *pattern,
			    bool recursive = false);

#endif /*FILELIST_H*/
