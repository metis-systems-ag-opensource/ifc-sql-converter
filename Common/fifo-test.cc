/* fifo-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
**
** Small test program for my small FIFO-lists
**
*/

#include <iostream>
#include "fifo.h"
#include "fifoout.h"

#if 0
template <class T> std::ostream &out_fifo (std::ostream &out, Fifo<T> f)
{
    typedef class Fifo<T>::Iterator iterator_t;
    bool first = true;
    out << '[';
    for (iterator_t x(f.begin()); ! x.atend (); ++x) {
	if (first) { first = false; } else { out << ", "; }
	out << *x;
    }
    out << ']';
    return out;
}
#endif

int main ()
{
    using namespace std;
    cout << "ATSTART" << endl;

    cout << "Initializing my 'Fifo' with: 33, 44, 55, 66, 77" << endl;
    Fifo<int> f{33, 44, 55, 66, 77};

    cout << "Result: " << f << endl;

    cout << "Appending values (single-value style): 88, 99" << endl;

    f.push(88).push(99);

    cout << "Result: " << f << endl;

    cout << "Popping. last element's value was: " << f.pop() << endl;

    cout << "Remaining list: " << f << endl;

    cout << "Popping. last element's value was: " << f.pop() << endl;

    cout << "Result: " << f << endl;

    cout << "Shifting. First element's value was: " << f.shift() << endl;

    cout << "Result: " << f << endl;

    cout << "Shifting. First element's value was: " << f.shift() << endl;

    cout << "Result: " << f << endl;

    cout << "Removing all elements from my 'Fifo' ..." << endl;

    f.clear ();

    cout << "Result: " << f << endl;

    cout << "Appending elements (single-value style): 11, 99, 88, 0" << endl;

    f.push(11).push(99).push(88).push(0);

    cout << "Result: " << f << endl;

    int vartest = 123;

    cout << "Appending a variable's value: " << vartest << endl;

    f.push (vartest);

    cout << "Result: " << f << endl;

    cout << "Clearing 'Fifo' and then" << endl;
    cout << "appending a list of values: 123, 321, 445, 998, 988, 9431" << endl;

    f.clear (); f.push (Fifo<int>{123, 321, 445, 998, 988, 9431});

    cout << "Result: " << f << endl;

    cout << "Prepending a list of values: 3, 2, 1, 1, 1, 0" << endl;
    f.unshift ({3, 2, 1, 1, 1, 0});

    cout << "Result: " << f << endl;

    cout << "Getting element #0 of the 'Fifo': " << f[0] << endl;

    cout << "Getting element #5 of the 'Fifo': " << f[5] << endl;

    cout << "Creating a second 'Fifo' from: 2, 3, 5, 7, 11, 13, 17, 23" << endl;

    Fifo<int> g{2, 3, 5, 7, 11, 13, 17, 23};

    cout << "Result: " << g << endl;

    cout << "Appending this fifo to 'f' ..." << endl;

    f.push (g);

    cout << "Result: " << f << endl;

    cout << "Removing all elements from 'f' ..." << endl;

    f.clear ();

    cout << "Result: " << f << endl;

    cout << "Reverse prepending 'g' to 'f' ..." << endl;

    f.runshift (g);

    cout << "Result: " << f << endl;

    cout << "Removing all elements from 'f' ..." << endl;

    f.clear ();

    cout << "Result: " << f << endl;

    cout << "Appending elements to 'f': 127, 131, 277" << endl;

    f.push (Fifo<int> {127, 131, 277});

    cout << "Prepending 'g' to 'f' ..." << endl;

    f.unshift (g);

    cout << "Result: " << f << endl;

    cout << "Printing the 'Fifo' elements in standard order ..." << endl;
    bool first = true;
    for (auto it = f.begin(); ! it.atend(); ++it) {
	if (first) { first = false; } else { cout << ", "; }
	cout << (*it);
    }
    cout << endl;

//    cout << "Printing the 'Fifo' elements in reverse order ..." << endl;
//    first = true;
//    for (auto it = f.rbegin(); ! it.atend(); --it) {
//	if (first) { first = false; } else { cout << ", "; }
//	cout << (*it);
//    }
//    cout << endl;

    cout << "ATEND" << endl;

    return 0;
}
