/*! \file config.h
**
** \brief
** A *read only* Configuration file reader/handler class (`Config`).
**
** This is the interface part of the module `config.{cc,h}`. It contains the
** class definition of `Config`, whereas the implementation part (`config.cc`)
** contains the implementation of the member functions and the internals of
** this type.
**
** Purpose of the class `Config` is managing a program's external configuration
** (given in a configuration file). This means:
**  - loading (parsing) a configuration file,
**  - accessing the configuration items (name/value-pairs) of the
**    configuration read.
**
** Configurations loaded by a `Config`-object are *immutable*.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Configuration class (loading configuration data from a file)
**
*/
#ifndef CONFIG_H
#define CONFIG_H

#include <map>
#include <set>
#include <string>

// Simplifying the specifications by using a type aliases for the complex
// template invocations.

/*! Helper type which makes the class definition of `Config` a bit more clear.
**  This type is defined outside of `Config`, because it's used somewhere else,
**  too.
*/
using ConfData = std::map<std::string, std::string>;

/*! Helper type which makes the class definition of `Config` a bit more clear.
**  This type is defined outside of `Config`, because it's used somewhere else,
**  too.
*/
using ConfItems = std::set<std::string>;

/*! The (*real*) implementation of the type `Config` is completely hidden in
**  the implementation part of this module. Only a pointer to an incomplete
**  type (`Config_impl`)is used as the (sole) member variable of the `Config`
**  class.
*/
struct Config_impl;

/*! `Config` is a class for managing configuration values which are loaded
**  from simple text files in a `name = value` format.
**
**  It contains a simple parser for these text files and some operators and
**  functions for accessing configuration values (read only!).
*/
class Config {
public:
    Config();			//!< "Default" constructor
    Config (const Config &x);	//!< Copy constructor
    Config (Config &&x);	//!< Move construtor
    ~Config();			//!< Destructor
    Config &operator= (const Config &x);	//!< Copy assignment operator
    Config &operator= (Config &&x);		//!< Move assignment operator

    /*! `load()` loads a configuration from a text file whose pathname is given
    **  as the argument of this function.
    **
    **  A configuration file is a text file consisting of (apart from comments
    **  and empty lines) `name = value` associations. There are some specially
    **  formatted comments, which allow for the substitution of certain values
    **  taking place:
    **
    **      #s:on
    **      #substitute on
    **
    **  enables substitution, and
    **
    **      #s:off
    **      #substitute off
    **
    **  disables it. Placeholders can be given in two formats:
    **   1. as `%%name`, or
    **   2. as `%{name}`.
    **
    **  The latter format can be used where a configuration item is directly
    **  followed by a character which would otherwise be interpreted as a
    **  part of `name`.
    **
    **  The following placeholders are substituted in the part after a `#s:on`
    **  line (until the next `#s:off` line was found, or the end of the file
    **  was reached):
    **   - `%%cfdir` is replaced by the pathname of the directory which
    **     contains the configuration file.
    **   - `%%progdir` is replaced by the pathname of the directory where the
    **     program using the `Config` object resides in.
    **   - `%%home` is replaced by the home directory of the user who
    **     originally invoked the program which uses the `Config` object.
    **   - `%%userconfig` is replaced by the pathname of a local user's
    **     configuration directory (same as `%%home/.config`).
    **   - `%%sysconfig` is replaced by the pathname of a system wide
    **     configuration directory (`/etc` in Unix/Linux).
    **
    **  A configuration value can contain other placeholder-like parts. These
    **  remain unchanged in the result, even if the placeholders announced
    **  above are substituted.
    */
    void load (const std::string &file);

    /*! `Config::loaded()` checks the state of the configuration.
    **
    **  @returns `true` if a configuration file was successfully parsed and
    **  its values stored (in the member variable `_data`) in the `Config`
    **  object this function is applied on, and, `false`, otherwise.
    */
    bool loaded();

    /*! `has()` checks if a configuration item exists.
    **
    **  @returns `true` if the item whose name is given as argument exists in
    **  `Config` object it is applied on, and `false`, otherwise.
    */
    bool has (const std::string &name) const;

    /*! The index operator returns the value of the configuration item `name`
    **  if `name` exists, and an empty string, otherwise.
    **
    **  In contrast to its normal definition, this function always returns a
    **  constant value.
    */
    const std::string &operator[] (const std::string &name) const;

    /*! This is an alternative index "operator", adopted from the C++ standard
    **  class library. In contrast to `[]`, it raises a `std::domain_error` if
    **  no configuration item `name` exists.
    */
    const std::string &at (const std::string &name) const;

    /*! @returns the complete pathname of the file the current configuration
    **  was loaded from. If no file was loaded yet, it returns the empty
    **  string instead.
    */
    const std::string &filename() const;

    /*! This is the return type of `check_missing()`. */
    typedef std::pair<bool, std::set<std::string>> MissingState;

    /*! `check_missing()` helps to identify missing mandatory items in a
    **  `Config` object.
    **
    **  @param expected - The list of names of configuration items whose
    **  existence is expected.
    **
    **  @returns either `true` and a list (`std::set`) of the names of the
    **  configuration items missing, or `false` and an empty list.
    */
    MissingState check_missing (const std::set<std::string> &expected) const;

    /*! @returns the names of all existing items in the `Config` object it is
    **  applied on.
    */
    ConfItems item_names() const;

    /*! @returns a `ConfData` value, containing all existing items in the
    **  `Config` object it is applied on.
    */
    const ConfData &data() const;
private:
    /*! The implementation of the `Config` class is completely hidden from the
    **  interface. In C and C++ this is done by defining the corresponding
    **  attribute as a pointer to an incomplete (struct-)type.
    */
    Config_impl *_data;
};

#endif /*CONFIG_H*/
