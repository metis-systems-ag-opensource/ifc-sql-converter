/* fifo-test2.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
**
** Test if a Fifo-value generated within a function can be returned as an
** 'rvalue'-reference ...
**
*/

#include <iostream>
#include <cstdint>
#include <string>

#include "fifo.h"
#include "fifoout.h"

using namespace std;

Fifo<int> rvrvref_test () //(Fifo<int> &q)
{
    Fifo<int> x {0, 1, 2, 3, 9, 8, 7, 6};

    //q = (x);
    return x;
    //return Fifo<int> {0, 1, 2, 3, 9, 8, 7, 6};
}

int main ()
{
    cout << "Declaring a new Fifo<int> (x) with default initalization." << endl;
    Fifo<int> x;
    cout << "x.length() = " << x.length () << ", x = " << x << endl << endl;
    //Fifo<int> q;

    cout << "Assigning x with a Fifo-value returned from a function." << endl;
    x = rvrvref_test (); //(q);
    cout << "x.length() = " << x.length () << ", x = " << x << endl << endl;

    cout << "Creating a new Fifo<int> (q) with default initialization." << endl;
    Fifo<int> q;
    cout << "q.length() = " << q.length () << ", q = " << q << endl << endl;

    cout << "Assigning q with x while extending x ..." << endl;
    q = x.push (33);
    cout << "x.length() = " << x.length () << ", x = " << x << endl;
    cout << "q.length() = " << q.length () << ", q = " << q << endl << endl;

    cout << "Assigning 99 to q[3]." << endl;
    q[3] = 99;
    cout << "x.length() = " << x.length () << ", x = " << x << endl;
    cout << "q.length() = " << q.length () << ", q = " << q << endl << endl;

    cout << "Creating a new Fifo<int> (r), initializing it with q." << endl;
    Fifo<int> r (q);
    cout << "r.length() = " << r.length () << ", r = " << r << endl << endl;

    cout << "Creating a new Fifo<int> (s),"
	    " initializing it with a temporary object." << endl;
    Fifo<int> s ((Fifo<int> {99, 88, 77}));
    cout << "s.length() = " << s.length () << ", s = " << s << endl << endl;

    cout << "Creating a new Fifo<int> (t),"
	    " initializing it with the function return value." << endl;
    Fifo<int> t = rvrvref_test();
    cout << "t.length() = " << t.length () << ", t = " << t << endl << endl;

    cout << "At END!" << endl;
    return 0;
}
