/*! \file pathcat.h
**
**  This is a header-only module which defines:
**   - `/` as an operator template for path-concatenating¹ two
**     `std::basic_string<...>` values,
**   - `/` for path-concatenation a C string (`const char *`) with a
**     `std::string`,
**   - `path_split()` - a function for splitting a pathname into its components
**     at each `PATHSEP`²,
**   - `path_join()` - the complement of `path_split()` which concatenates a
**     list of pathname parts (given as `std::vector<std::basic_string<...>>`)
**     with a `PATHSEP` between each two components.
**
** ¹ Path-concatenating is the concatenation of two strings which are assumed
**   to be pathnames with a file separator (`PATHSEP`) between them.
**
** ² `PATHSEP` is imported from the `filesep.h` module and is either `/` or
**   `\\`, depending on the operating a program using this module was generated
**   for.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Split and concatenate pathnames
**
*/

#include <string>
#include <vector>

#include "filesep.h"

/*! template operator function `operator/(...).
**
**  The template operator is based on `basic_string<...>` for making it as
**  flexible as possible.
*/
template<class CharT,
	 class Traits = std::char_traits<CharT>,
	 class Alloc = std::allocator<CharT>
	>
std::basic_string<CharT, Traits, Alloc>
operator/ (const std::basic_string<CharT, Traits, Alloc> &l,
	   const std::basic_string<CharT, Traits, Alloc> &r)
{
    // Extending the `char` constant PATHSEP to a `CharT` value (which can be
    // `char`, `wchar_t` or any other character like type.
    CharT pathsep = PATHSEP;
    if (r.empty()) { return l; }
    //if (l.empty()) { return r; }
    if (l.back() == pathsep || r.front() == pathsep) {
	return l + r;
    }
    return l + pathsep + r;
}

/*! This `operator/()` is a specialisation of the template operator `/` above,
** which has one `const std::string &` and one `const char *` argument, thus
** allowing for C-string constants to be path-concatnated with `std::string`
** pathnames. It was implemented, because the operator template defined above
** doesn't work if both arguments consist of C strings (`const char *`).
*/
extern inline std::string
operator/ (const std::string &l, const char *r)
{
    return l / std::string(r);
}

/*! This `operator/()` is a specialisation of the template operator `/` above,
** which has one `const char *` and one `const std::string &` argument, thus
** allowing for C-string constants to be path-concatnated with `std::string`
** pathnames. The implementation of this operator has the same reason as the
** implementation of the operator with reversed paramater types above.
*/
extern inline std::string
operator/ (const char *l, const std::string &r)
{
    return std::string (l) / r;
}

/*! `path_split(p)` splits a pathname `p` into its components at each `PATHSEP`
**  file separator. Result is a `std::vector` consisting of all of these
**  path components.
*/
template<class CharT,
	 class Traits = std::char_traits<CharT>,
	 class Alloc = std::allocator<CharT>,
	 class Alloc1 = std::allocator<std::basic_string<CharT, Traits, Alloc>>
	> std::vector<std::basic_string<CharT, Traits, Alloc>, Alloc1>
path_split (const std::basic_string<CharT, Traits, Alloc> &path)
{
    using locstr = std::basic_string<CharT, Traits, Alloc>;
    using svec = std::vector<locstr, Alloc1>;
    const CharT pathsep = PATHSEP;
    svec res; size_t ressz = 1, pathlen = path.size(), ix, jx;

    for (ix = 0; ix < pathlen; ++ix) {
	if (path[ix] == pathsep) { ++ressz; }
    }

    res.reserve (ressz);

    for (ix = 0, jx = 0; ix < pathlen; ++ix) {
	if (path[ix] == pathsep) {
	    res.push_back (path.substr (jx, (ix - jx)));
	    jx = ix + 1;
	}
    }

    if (jx < pathlen) { res.push_back (path.substr (jx)); }

    return res;
}

/*! `path_join()` is the complement function of `path_split()`. All elements
**  of the `std::vector<std::basic_string<...>>` arguments are concatenated
**  with a `PATHSEP` between each two components. Result is the concatenated
**  string (`std::basic_string<...>`) value.
*/
template<class CharT, class Traits, class Alloc, class Alloc1>
std::basic_string<CharT, Traits, Alloc>
path_join (
    const std::vector<std::basic_string<CharT, Traits, Alloc>, Alloc1> &sv
)
{
    using locstr = std::basic_string<CharT, Traits, Alloc>;
    //using svec = std::vector<locstr, Alloc1>;
    const CharT pathsep = PATHSEP;
    // Defining two functions as lambda expressions with a capture of exactly
    // one variable from the environment: `pathsep`. These helper functions
    // are used for detecting the `PATHSEP` file separator at the beginning/end
    // of a component.
    auto fips = [pathsep] (const std::string &x) {
	// Test if the first `CharT` element of the `std::basic_string<...> &x`
	// is a `pathsep` or not ...
	return x.size() > 0 && x[0] == pathsep;
    };
    auto lips = [pathsep] (const std::string &x) {
	// Test if the last `CharT` element of the `std::basic_string<...> &x`
	// is a `pathsep` or not ...
	return x.size() > 0 && x[x.size() - 1] == pathsep;
    };
    locstr res; size_t ressz = 0, svlen = sv.size();

    // In iterating loops, where the number of elements processed is unknown,
    // for avoiding an insertion of an unwanted delimiter, it is essential to
    // know if the currently processed element is either the first one or the
    // last one. Because it is very difficult to determine that the current
    // element is the last one, the first element is selected for this
    // purpose. This is simply done by a simple "flipflop" switch (a boolean
    // variable initialised with `true` and reset to `false` in the first
    // iteration of the loop).
    bool first = true;
    // The `bool` vector `is_delimited` is defined for avoiding needless
    // invocations of `fips()` and `lips()` later, because the result of the
    // invocations of both functions remains the same for the second loop.
    std::vector<bool> is_delimited (sv.size(), false);
    // The first loop is solely used for calculating the amount of memory for
    // storing the result. Doing this *before* the allocation takes place
    // seems to be slower than doing all in one loop, but the time needed for
    // each allocation (if only one loop would be used) isn't unessential,
    // which makes the use of two loops (one for calculating the required
    // memory space aod one for the concatenation) with a single memory
    // allocation between them much faster in most cases. In both loops, an
    // index is used instead of an iterator, for two reasons:
    //   1. An index access is simply faster.
    //   2. Handling the `fips()` and `lips()` tests is much more difficult if
    //      the indexes of the previous and current elements are unknown.
    // (The second issue could be resolved with additional state variables, but
    //  this would be less clear.)
    for (size_t ix = 0; ix < svlen; ++ix) {
	if (first) {
	    // The first string never needs to be delimited with a `pathsep`
	    first = false;
	} else if ((lips (sv[ix - 1]) || fips (sv[ix]))) {
	    // Either the previous element ends with a `pathsep` (`lips`) or
	    // the current element begins with one (`fips`). In both cases,
	    // no memory for a `pathsep` must be added, because no `pathsep`
	    // will be inserted later. Keep this information in the `bool`
	    // vector declared just above this loop ... this avoid secondary
	    // invocations of `fips()` and `lips()` later.
	    is_delimited[ix] = true;
	} else {
	    // In all other case, additional memory is required for the
	    // `pathsep`.
	    ++ressz;
	}
	ressz += sv[ix].size();
    }

    // Allocate the required memory for the result.
    res.reserve (ressz);

    // In the second loop, all elements of `sv` are path-concatenated into
    // the result `res`.
    first = true;
    for (size_t ix = 0; ix < svlen; ++ix) {
	if (first) {
	    // No `pathsep` required yet.
	    first = false;
	} else if (! is_delimited[ix]) {
	    // Neither the previous element of `sv` ends with `pathsep` nor
	    // the current one begins with it. (The `bool` vector `is_delimited`
	    // now makes further invocations of `fips()` and `lips()`
	    // needless.)
	    res.push_back (pathsep);
	}
	// Appending the current element to the result. Because the memory
	// required for storing the complete pathname was reserved
	// preliminarily, this operation should do only consist of copying the
	// content of the current element to the end of the result and then
	// adjusting its `size()` counter.
	res += sv[ix];
    }

    // Return the result.
    return res;
}
