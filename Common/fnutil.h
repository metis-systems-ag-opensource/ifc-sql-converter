/*! \file fnutil.h
**  Interface part of the module `fnutil.{cc,h}`.
**
**  The module `fnutil.{cc,h}` provides some functions required (i.a.) in the
**  code generator part if the EXPRESS compiler. They mostly resemble typical
**  shell commands with the same names.
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Interface file for 'fnutil.cc'
** (Utilitiy functions for converting filenames into some other things...)
**
*/
#ifndef FNUTIL_H
#define FNUTIL_H

#include <string>

/*! `basename()` returns the last part of a pathname, given as `std::string`,
**  as a `std::string`.
*/
std::string basename (const std::string &filename);

/*! `suffix()` returns the filename extension (suffix) if such a thing exists.
**  Otherwise, it returns the empty string.
*/
std::string suffix (const std::string &filename, char sfxdelim = '.');

/*! `dirname()` returns all but the last part of a pathname, given as
**  `std::string`, as a `std::string`.
*/
std::string dirname (const std::string &filename);

/*! `trans_filename(p)` translates the  pathname `p` (`std::string`) into
**  something which can directly be used as the name of a C/C++ `#define`
**  macro. It is typically used for generating the `#ifndef` macros in
**  generated header files.
*/
std::string trans_filename (const std::string &filename);

/*! `abspath(p)` returns `true` if the given pathname `p` is absolute (in
**  Unix/Linux this means: begins with a `/`) and `false`, otherwise.
*/
bool is_abspath (const std::string &fn);

/*! `is_pathname(p)` returns `true` if the given filename `p` contains a
**   path separator (`/` in Unix/Linux); otherwise, it returns `false`.
*/
bool is_pathname (const std::string &fn);

#endif /*FNUTIL_H*/
