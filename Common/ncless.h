/*! \file ncless.h
** Interface part of the `ncless.{cc,h}` module.
**
** `ncless.{cc,h}` implements a The string comparison operation `<` as a
** function class `NCLess`. This class is used in the symbol table as
** comparison function for the underlying `std::map` (instead of the default
** class), because identifiers are defined as letter case independent in the
** EXPRESS specification (ISO-10303-11).
**
** Examples:
**
**  > ``NCLess ('a', 'A')`` ==> `false` | (``'a' < 'A'`` ==> ``false``)
**
**  > ``NCLess ('b', 'C')`` ==> `true` | (``'b' < 'C'`` ==> ``false``)
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Defining my own "less" functional class
**
*/
#ifndef NCLESS_H
#define NCLESS_H

#include <string>
#include <functional>

/*! `NCLess` is an instatiation of the function class template
**  `std::binary_function<Param1Type, Param2Type, ReturnType>`.
*/
struct NCLess : std::binary_function<std::string, std::string, bool> {
    //! The "function apply" operator `()` makes this object to something
    //  similar to a function.
    bool operator() (const std::string &x, const std::string &y) const;
} /*NCLess*/;

#endif /*NCLESS_H*/
