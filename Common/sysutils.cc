/* sysutils.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Small helper functions for getting
** a) (some of) a given user's data,
** b) the same data, but for the effective user,
** c) the name of the program's binary,
** d) the path of the directory the program's binary resides in.
**
** The latter two functions are Linux-specific.
**
*/

#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdexcept>
#include <system_error>

#include "filesep.h"
#include "sutil.h"

#include "sysutils.h"

using std::string, std::stoul;
using std::invalid_argument;
using std::system_error, std::system_category;

int udata (const string &nameuid, string &user, uid_t &uid, string &home)
{
    int ec = 0;
    struct passwd *pw = NULL;
    for (;;) {
	if ((pw = getpwnam (nameuid.c_str()))) { ec = 0; break; }
	if ((ec = errno) != EINTR) { break; }
    }
    if (! pw) {
	switch (ec) {
	    case EIO: case EMFILE: case ENFILE: case ENOMEM: case ERANGE:
		endpwent(); return ec;
	    default:
		break;
	}
	size_t ix;
	unsigned long uid = stoul (nameuid, &ix, 0);
	if (ix < nameuid.size()) { return EINVAL; }
	for (;;) {
	    if ((pw = getpwuid ((uid_t) uid))) { break; }
	    if ((ec = errno) != EINTR) {
		if (ec == 0) { ec = ENOENT; }
		endpwent(); return ec;
	    }
	}
    }
    /* FACT: pw is valid! */
    user = pw->pw_name; uid = pw->pw_uid; home = pw->pw_dir;
    endpwent();
    return 0;
}

int euser (string &user, uid_t &uid, string &home)
{
    int ec = 0;
    struct passwd *pw = NULL;
    for (;;) {
	if ((pw = getpwuid (geteuid()))) { break; }
	if ((ec = errno) != EINTR) { break; }
    }
    if (! pw) {
	if (ec == 0) { ec = ENOENT; }
	endpwent(); return ec;
    }
    user = pw->pw_name; uid = pw->pw_uid; home = pw->pw_dir;
    endpwent();
    return 0;
}

static void check4err (int ec, const string &what)
{
    if (ec != 0) { throw system_error (ec, system_category(), what); }
}

string username (const string &nameuid)
{
    string login, home;
    uid_t uid;
    check4err (udata (nameuid, login, uid, home), "username (nameuid)");
    return login;
}

string username()
{
    string login, home;
    uid_t uid;
    check4err (euser (login, uid, home), "username()");
    return login;
}

string userhome (const string &nameuid)
{
    string login, home;
    uid_t uid;
    check4err (udata (nameuid, login, uid, home), "userhome (nameuid)");
    return home;
}

string userhome()
{
    string login, home;
    uid_t uid;
    check4err (euser (login, uid, home), "userhome()");
    return home;
}

uid_t userid (const string &nameuid)
{
    string login, home;
    uid_t uid;
    check4err (udata (nameuid, login, uid, home), "userid (nameuid)");
    return uid;
}

uid_t userid()
{
    string login, home;
    uid_t uid;
    check4err (euser (login, uid, home), "userid()");
    return uid;
}

static const char *_progpath = NULL;

/* This function is _very_ Linux-specific! */
static const char *progpath()
{
    if (! _progpath) {
	char *path = NULL;
	size_t sz = 16;
	ssize_t rlen = 0;
	for (;;) {
	    char *p = (char *) realloc (path, sz);
	    if (! p) { return p; }
	    path = p; rlen = readlink ("/proc/self/exe", path, sz);
	    if (rlen < 0) {
		int ec = errno; free (path); errno = ec; return NULL;
	    }
	    if ((size_t) rlen < sz) { break; }
	    sz = 2 * sz;
	}
	path[rlen] = '\0';
	_progpath = path;
    }
    return _progpath;
}

std::string appname()
{
    const char *pp = progpath();
    if (! pp) { throw system_error (errno, system_category(), "appname()"); }
    const char *pn = basename (pp);
    string res (pn);
    return res;
}

std::string appdir()
{
    const char *pp = progpath();
    if (! pp) { throw system_error (errno, system_category(), "appdir()"); }
    const char *pn = strrchr (pp, PATHSEP);
    string res (pp, (size_t) (pn - pp));
    return res;
}

/*! Converts a string which has the format of a fixed point number
**  (`<integral part>.<fractional part>`) into a `struct timespec`
**  value.
**
**  @param `delay` - a string containing a fixed point number
**
**  @returns the converted (`struct timespec`) value.
*/
struct timespec s2time (const string &delay)
{
    static const char *s2time = "s2time";
    size_t dotx = 0;
    struct timespec res;
    string td = trim (delay);
    if (td.empty()) { throw invalid_argument (s2time); }
    if (td[0] == '.') {
	dotx = 0;
	res.tv_sec = 0;
    } else {
	try {
	    res.tv_sec = stol (td, &dotx);
	} catch (...) {
	    // Somewhat a re-throw, but with another message string
	    throw invalid_argument (s2time);
	}
    }
    if (dotx >= td.size()) {
	// No dot found: No fractional part, but otherwise OK
	res.tv_nsec = 0;
    } else {
	// Not a dot found at this position: This is an ERROR!
	if (td[dotx] != '.') { throw invalid_argument (s2time); }
	// Dot found! Remove all before the dot and the dot itself from the
	// string.
	td.erase (0, dotx + 1);
	// If the string consisted solely of a dot, we have an ERROR!
	if (td.empty() && dotx == 0) { throw invalid_argument (s2time); }
	// Remove any part of the fractional value which may exceed the
	// maximum nano-seconds value.
	if (td.size() > 9) { td.erase (9); }
	// Now, convert the (numerical!) value after the dot to an unsigned
	// integer.
	size_t endfrac = 0, rx = td.size();
	unsigned long nsec;
	try {
	    nsec = stoul (td, &endfrac);
	} catch (...) {
	    // Somewhat a re-throw, but with another message string
	    throw invalid_argument (s2time);
	}
	if (endfrac < td.size()) {
	    // The fractional part was too long => ERROR!
	    throw invalid_argument (s2time);
	}
	// Adjust the fractional part to be a nano-seconds value
	while (rx < 9) { nsec *= 10; }
	res.tv_nsec = nsec;
    }
    return res;
}

struct FileInfo_priv {
    string filename;
    struct stat statbuf;
    int sys_errno;
    bool keep_symlinks, got_data;
};

static
FileInfo_priv *gen_fileinfo (const string &name, struct stat *sp, int err)
{
    FileInfo_priv *finfo = new FileInfo_priv;
    finfo->filename = name;
    if (sp) {
	memcpy (&finfo->statbuf, sp, sizeof(struct stat));
    } else {
	memset (&finfo->statbuf, 0, sizeof(struct stat));
    }
    finfo->sys_errno = err;
    finfo->keep_symlinks = false;
    return finfo;
}

FileInfo::FileInfo()
  : data(gen_fileinfo (string(), nullptr, EINVAL))
{
    data->got_data = false;
}

FileInfo::FileInfo (const string &pathname, bool keep_symlinks)
  : data(gen_fileinfo (pathname, nullptr, EINVAL))
{
    get_info (keep_symlinks);
}

FileInfo::FileInfo (const FileInfo &x)
  : data(gen_fileinfo (x.data->filename, &x.data->statbuf, x.data->sys_errno))
{
    data->got_data = x.data->got_data;
}

FileInfo::FileInfo (FileInfo &&x)
  : data(x.data)
{
    data->got_data = x.data->got_data;
    x.data = nullptr;
}

FileInfo::~FileInfo()
{
    if (data) { delete data; data = nullptr; }
}

void FileInfo::get_info (bool keep_symlinks)
{
    data->sys_errno = 0;
    data->keep_symlinks = keep_symlinks;
    if (stat (data->filename.c_str(), &data->statbuf)) {
	data->sys_errno = errno;
    }
    data->got_data = true;
}

void FileInfo::get_info (const std::string &pathname, bool keep_symlinks)
{
    data->filename = pathname;
    get_info (keep_symlinks);
}

bool FileInfo::exists()
{
    if (! data->got_data) { get_info (data->keep_symlinks); }
    return data->sys_errno == 0;
}

bool FileInfo::regular_file()
{
    if (! data->got_data) { get_info (data->keep_symlinks); }
    return (data->sys_errno == 0 && S_ISREG (data->statbuf.st_mode));
}

bool FileInfo::directory()
{
    if (! data->got_data) { get_info (data->keep_symlinks); }
    return (data->sys_errno == 0 && S_ISDIR (data->statbuf.st_mode));
}

bool FileInfo::symlink()
{
    if (! data->got_data) { get_info (data->keep_symlinks); }
    return (data->sys_errno == 0 && S_ISLNK (data->statbuf.st_mode));
}

bool FileInfo::error()
{
    if (! data->got_data) { get_info (data->keep_symlinks); }
    return (data->sys_errno != 0 && data->sys_errno != ENOENT);
}

int FileInfo::sys_errno() const
{
    return data->sys_errno;
}
