/*! \file mintypes.h
**  Some type aliases used in EXPRESS and STEP
*/
/* $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Some "typedefs" (C++-style) for types used in some of the other modules ...
**
*/
#ifndef MINTYPES_H
#define MINTYPES_H

#include <cstdint>
#include <vector>

/*! The datatype which is used for describing identifiers (EXPRESS/STEP)
*/
using Ident = uint32_t;

/*! Defining the EXPRESS type `REAL` as `double`
*/
using Real = double;

/*! Defining the EXPRESS type `INTEGER` as `int32_t`
*/
using Integer = int32_t;

/*! Defining the EXPRESS type `NATURAL` as `uint32_t`
*/
using Natural = uint32_t;

/*! Defining `BitString`; this type alias which is used for defining the
**  EXPRESS type `BINARY`.
*/
using BitString = std::vector<bool>;

/*! The minimum value for the type `Integer`
*/
#define INTEGER_MIN INT32_MIN
/*! The maximum value for the type `Integer`
*/
#define INTEGER_MAX INT32_MAX
/*! The maximum value for the types `Natural`
*/
#define NATURAL_MAX UINT32_MAX

#endif /*MINTYPES_H*/
