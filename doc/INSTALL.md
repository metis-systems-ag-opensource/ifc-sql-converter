# Building and installing

## 0. Prerequisites

1. A C++ compiler (>= C++17: `g++` / `clang++`, ...)
2. The `binutils` package (for `ar`, `ld`, `as`, ...)
3. A GNU compatible `make`
4. The Parser/Scanner generator pair `flex`/`bison`.
5. `pkg-config`
6. (Optionally) Doxygen (+ `latex`, ...)
7. The *Bash* shell
8. The MySQL/MariaDB developer library package (e.g. `libmariadb-dev`)

## Building the EXPRESS parser, the STEP parser, and the 'ifc-conf' utility

The build-process is quite simple:
```
$ make cleanall; make
...
...
...
$ make to_bin
```

## Installing the EXPRESS parser

### Global installation

```
# make install-xpp
```

### Local installation

```
$ make local_install-xpp
```

##  Installing the STEP parser

### Global installation

```
# make install-stp
```

### Local installation

```
$ make local_install-stp
```

# Generating db-schemas and/or plugins for 'stp'

## DB-schemas

```
$ gen-ifc dbsql
```

## Plugins

```
$ gen-ifc plugins
```

## DB-schemas and plugins simultaneously

```
$ gen-ifc all
```
