Warum eine zweistufige Symboltabelle?

Natürlich würde eine einfache "map" zwischen Zeichenketten und 'TypeStruct *'-
Werten zunächst einmal ausreichen. Das würde allerdings bedeuten, dass jesesmal
wenn ein Identifier gefunden würde, dieser als String in jedem Element eines
'TypeStruct'-Elements, in jeder Anweisung, in jedem Ausdruck aufgeführt werden
müsste. Das ist Speicherplatzverschwendung und kostet außerdem für jeden
Zugriff Zeit.

Bei einer zweistufigen Symboltabelle hingegen sind die 'TypeStruct *'-Elemente
hingegen in einem normalen 'std::vector' enthalten, und können direkt mittels
eines Index (ich habe diesen als Typ 'Ident' vereinbart) addressiert werden.
Die 'std::map' enthält dann nur noch 'Ident'-Werte, also Indices in den
'std::vector'. Um trotzdem auf die Symbolnamen zurückgreifen zu können (mittels
'Ident'-Wert, also Index), wird eine zweite Tabelle mitgeführt, die jeweils
Verweise ('const char *') auf die 'Key'-Elemente der 'std::map' enthält.

Als Konsequenz muss für jeden Identifier, der angetroffen wird, sofort ein
Eintrag in der Symboltabelle angelegt werden. (Schließlich sind alle
'ident'-Werte in der YACC-Beschreibung vom Typ 'Ident'.)
Dieser Schritt hat jedoch auch Vorteile. So kann, bei passendem Typ-System,
frühzeitig der "erwartete" Typ eines Identifiers festgelegt werden, und bei
einer späteren Definition eine Inkonsistenz frühzeitig erkannt werden. Das ist
ein "ungenauer" Typ-Mechanismus (mit automatischer 'forward'-Deklaration).
Dieser Mechanismus müsste die meißten groben Inkonsistenzen abfangen. Was
später noch bleibt, ist im wesentlichen die Überprüfung der Ausdrücke von
"Constraints" ('where clause', 'supertype constraint', ...), sowie die
Anweisungen/Ausdrücke innerhalb von Prozeduren, Funktionen und 'Rules'. Auch
ein extern angegebener 'supertype constraint', kann so passend sofort in die
entsprechenden 'TsEntity'-Elemente der Symboltabelle eingetragen werden.
