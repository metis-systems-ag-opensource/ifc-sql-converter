How to implement a RDBMS-schema which allows for object-oriented features like
'inheritance' and '_polymorphism_' to be mapped into a relational database?
==============================================================================

Original idea
-------------

For an ENTITY we have the following cases:
  a) The ENTITY was declared ABSTRACT. In this case, a SELECT-table must be
     created for the ENTITY.
  b) The ENTITY has no subtypes (is a "_leaf_" ENTITY). In this case, only a
     data table containing the attributes of the ENTITY must be created.
  c) The ENTITY is neither ABSTRACT nor a "_leaf_" ENTITY. In this case, a
     data table as well as a SELECT-table must be created.

    CREATE TABLE <entity> (
      id INTEGER NOT NULL PRIMARY KEY,
      modelid INTEGER NOT NULL,	// base reference to the IFC model
      ... attrs ...
    );


New Idea
--------

... An ENTITY need not have a SUPERTYPE constraint (e.g. SUPERTYPE OF (ONEOF
(...))), because the type hierarchy of each entity was already collected in
an EtHierarchy element. Because the only way to model a type hierarchy with
polymorphism and inheritance is to use tables for _each_ entity type and
connect them through a PRIMARY KEY/FOREIGN KEY relationship. So, in the
complex entity type instance, the (outermost) supertype part doesn't know
anything about the subtypes instances connected to it, but each subtype
instance knows its direct parent type instances (through the foreign key.
But this comes with a price. Each (outest) supertype instance must know the
exact entity type it represents; this will be modeled with a key into a
global table which contains the corresponding type names. Additionally, it
is possible to create views which collect the instance parts of each table
which is part of a complex entity type, forming the complex instance in
this way.

E.g.:

    ENTITY IfcActor
      SUBTYPE OF (IfcObject);
        TheActor: IfcActorSelect;
      ...
    END\_ENTITY;
    ENTITY IfcObject
      ABSTRACT SUPERTYPE OF (ONEOF (...))
      SUBTYPE OF (IfcObjectDefinition);
        ObjectType : OPTIONAL IfcLabel;
      ...
    END\_ENTITY;
    ENTITY IfcObjectDefinition
      ABSTRACT SUPERTYPE OF (ONEOF (...))
      SUBTYPE OF (IfcRoot);
      ...
    END\_ENTITY;
    ENTITY IfcRoot
      ABSTRACT SUPERTYPE OF (ONEOF (...));
      GlobalId : IfcGloballyUniqueId;
      OwnerHistory : IfcOwnerHistory;
      Name : OPTIONAL IfcLabel;
      Description : OPTIONAL IfcText;
      ...
    END\_ENTITY;

could be represented in the following way:

   CREATE TABLE IFCROOT (
     objid INTEGER NOT NULL PRIMARY KEY,
     typeid INTEGER NOT NULL,
     GlobalId CHAR(...) NOT NULL,
     OwnerHistory_key INTEGER NOT NULL, -- foreign key into another table
     Name VARCHAR(...),
     Description VARCHAR(...),
     FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
     FOREIGN KEY (OwnerHistory_key) REFERENCES IFCOWNERHISTORY(objid)
   );
   CREATE TABLE IFCOBJECTDEFINITION (
     objid INTEGER NOT NULL PRIMARY KEY,
     typeid INTEGER NOT NULL,
     IfcRoot_key INTEGER,
     FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
     FOREIGN KEY (IfcRoot_key) REFERENCES IFCROOT(objid)
   );
   CREATE TABLE IFCOBJECT (
     objid INTEGER NOT NULL PRIMARY KEY,
     typeid INTEGER NOT NULL,
     IfcObjectDefinition_key INTEGER NOT NULL,
     ObjectType VARCHAR(...),
     FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
     FOREIGN KEY (IfcObjectDefinition_key) REFERENCES
       IFCOBJECTDEFINITION(objid)
   );
   CREATE TABLE IFCACTOR (
     -- An IfcActor part instances never stands for itself, but is always
     -- part of a complex type. Because it also has no SUBTYPE, we don't
     -- need a key here, as the FOREIGN KEY into IFCOBJECT is solely enough
     -- to identify it.
     IfcObject_key INTEGER NOT NULL,
     TheActor_key INTEGER NOT NULL,
     FOREIGN KEY (IfcObject_key) REFERENCES IFCOBJECT(objid),
     FOREIGN KEY (TheActor_key) REFERENCES IFCACTORSELECT(objid)
   );

So, for the "_leaf_" entity, we don't need a primary key and for the "_root_"
entity, we don't need a foreign key. All these tables are connected through
their foreign keys. A view could be created as:

    CREATE VIEW IFCACTOR_VIEW AS
      SELECT
        IFCROOT.objid, IFCROOT.typeid,  -- Required for DB-identification
        IFCROOT.GlobalId, IFCROOT.OwnerHistory_key, IFCROOT.Name,
        IFCROOT.Description,
          -- IfcRoot part instance attributes
        IFCOBJECT.ObjectType,
          -- IfcObject part instance attributes
        IFCACTOR.TheActor_key
      FROM IFCROOT, IFCOBJECTDEFINITION, IFCOBJECT, IFCACTOR
      WHERE
        IFCOBJECTDEFINITION.IfcRoot_key = IFCROOT.objid AND
        IFCOBJECT.IfcObjectDefinition_key = IFCOBJECTDEFINITION.objid AND
        IFCACTOR.IfcObject_key = IFCOBJECT.objid
    ;

Because '_IfcObjectDefinition_' doesn't contribute direct attributes to
the type hierarchy, it could be avoided iff the INVERSE attributes are not
respected.
Furthermore, the '_objid_' and '_typeid_' attributes of the tables
IFCOBJECTDEFINITION and IFCOBJECT can be avoided, because, as abstract
entities, they are never instantiated with elements, and further, because
they are subtypes, they are always instantiated together with an '_IfcRoot_'
instance. This leads to:

    CREATE TABLE IFCROOT (
      objid INTEGER NOT NULL PRIMARY KEY,
      typeid INTEGER NOT NULL,
      GlobalId CHAR(...) NOT NULL,
      OwnerHistory_key INTEGER NOT NULL, -- foreign key into another table
      Name VARCHAR(...),
      Description VARCHAR(...),
      FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
      FOREIGN KEY (OwnerHistory_key) REFERENCES IFCOWNERHISTORY(objid)
    );
    CREATE TABLE IFCOBJECTDEFINITION (
      IfcRoot_key INTEGER,
      FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
      FOREIGN KEY (IfcRoot_key) REFERENCES IFCROOT(objid)
    );
    CREATE TABLE IFCOBJECT (
      IfcRoot_key INTEGER NOT NULL,
      ObjectType VARCHAR(...),
      FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
      FOREIGN KEY (IfcRoot_key) REFERENCES IFCROOT(objid)
    );
    CREATE TABLE IFCACTOR (
      -- An IfcActor part instances never stands for itself, but is always
      -- part of a complex type. Because it also has no SUBTYPE, we don't
      -- need a key here, as the FOREIGN KEY into IFCOBJECT is solely enough
      -- to identify it.
      IfcRoot_key INTEGER NOT NULL,
      TheActor_key INTEGER NOT NULL,
      FOREIGN KEY (IfcRoot_key) REFERENCES IFCROOT(objid),
      FOREIGN KEY (TheActor_key) REFERENCES IFCACTORSELECT(objid)
    );

(and)

    CREATE VIEW IFCACTOR_VIEW AS
      SELECT
        IFCROOT.objid, IFCROOT.typeid,
        IFCROOT.GlobalId, IFCROOT.OwnerHistory_key, IFCROOT.Name,
        IFCROOT.Description,
        IFCOBJECT.ObjectType,
        IFCACTOR.TheActor_key
      WHERE
        IFCROOT.objid = IFCOBJECTDEFINITION.IfcRoot_key AND
        IFCROOT.objid = IFCOBJECT.IfcRoot_key AND
        IFCROOT.objid = IFCACTOR.IfcRoot_key
    ;

Let's see further:
  No non-root object table doesn't need its own primary key, before they are
  always connected to the root table, which has the key of the complete object
  structure. All the non-root object tables need is a foreign key to their
  corresponding root table(s). Any ENTITY which consists of only INVERSE
  attributes can be excluded from the definition of database tables in the
  first go, as the INVERSE attributes are only constraints which may be
  checked by the object model rather than the database system.

This leads to:

    CREATE TABLE IFCROOT (
      objid INTEGER NOT NULL PRIMARY KEY,
      typeid INTEGER NOT NULL,
      GlobalId CHAR(...) NOT NULL,
      OwnerHistory_key INTEGER NOT NULL, -- foreign key into another table
      Name VARCHAR(...),
      Description VARCHAR(...),
      FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
      FOREIGN KEY (OwnerHistory_key) REFERENCES IFCOWNERHISTORY(objid)
    );
    CREATE TABLE IFCOBJECT (
      IfcRoot_key INTEGER NOT NULL,
      ObjectType VARCHAR(...),
      FOREIGN KEY (typeid) REFERENCES ENTITY_TYPES(id),
      FOREIGN KEY (IfcRoot_key) REFERENCES IFCROOT(objid)
    );
    CREATE TABLE IFCACTOR (
      -- An IfcActor part instances never stands for itself, but is always
      -- part of a complex type. Because it also has no SUBTYPE, we don't
      -- need a key here, as the FOREIGN KEY into IFCOBJECT is solely enough
      -- to identify it.
      IfcRoot_key INTEGER NOT NULL,
      TheActor_key INTEGER NOT NULL,
      FOREIGN KEY (IfcRoot_key) REFERENCES IFCROOT(objid),
      FOREIGN KEY (TheActor_key) REFERENCES IFCACTORSELECT(objid)
    );

(and)

    CREATE VIEW IFCACTOR_VIEW AS
      SELECT
        IFCROOT.objid, IFCROOT.typeid,
        IFCROOT.GlobalId, IFCROOT.OwnerHistory_key, IFCROOT.Name,
        IFCROOT.Description,
        IFCOBJECT.ObjectType,
        IFCACTOR.TheActor_key
      WHERE
        IFCROOT.objid = IFCOBJECT.IfcRoot_key AND
        IFCROOT.objid = IFCACTOR.IfcRoot_key
    ;

So, in the first step, all "_root_" entities need to be selected and their
database tables created. Thereafter, for each non-root table which has
explicit attributes, a table must be created which contains FOREIGN KEY
attributes to their "_root_" tables. Any ENTITY which doesn't have explicit
attributes should be excluded from the table generation. Any FOREIGN KEY
constraint not concerning the type hierarchy should be created in a later
step (using 'ALTER TABLE xyz ADD FOREIGN KEY ... ;'), because otherwise
a more complex dependency analysis were required for creating the tables
in the correct order, a step which i want to avoid if it is avoidable.

As a preparation, i need to collect the "_root_" entities for each entity
and to exclude entities which consist of INVERSE attributes only.


New Idea
--------

Checking if the entity if a leaf entity. If this is the case,
a data table must be generated which contains all (explicit) attributes
of the entity, and (additionally) a field which points to ENTITY\_TYPES. If
this is not the case, a SELECT-table must be created instead, which
consists of each alternative (SUBTYPE) of the given ENTITY, including the
ENTITY itself (with the exception of ABSTRACT entities), and a field which
points to ENTITY\_TYPES.
