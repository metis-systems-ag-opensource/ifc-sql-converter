Es wird zusätzlich ein Typ für ENUMERATION-Identifier benötigt. Wie muss dieser
aussehen?

Ein ENUM-Typ vereinbart nicht nur den Typ als solches, sondern zusätzlich eine
ganze Latte an Identifiern (mit konstanten Werten).

Das hat mehrere Konsequenzen:

    a) Ein ENUM-Identifier kann mit dem Identifier eines anderen ENUMs
       übereinstimmen. Das ist noch nich zwangsläufig ein Konflikt; erst dann,
       wenn dieser Identifier irgendwo auftritt, entsteht ein Konflikt. Dieser
       muss dann durch "Qualifizierung" aufgelöst werden, d.h. der Identifier
       des ENUM-Typs muss dem ENUM-Identifier (mit einem '.' getrennt)
       vorangestellt werden.

    b) Sind hingegen beide ENUM-Typen in verschiedenen (geschachtelten)
       Gültigkeitsbereichen (Scopes) vereinbart, so überschattet der
       ENUM-Identifier des inneren Gültigkeitsbereichs immer den des äußeren.
       In diesem Fall kann der ENUM-Identifier aus dem äußeren Scope nur dann
       verwendet werden, wenn ihm der ENUM-Typ-Identifier (wie oben
       beschrieben) vorangestellt wird. In diesem Fall kann der ENUM-Identifier
       des inneren Scopes in diesem ohne Voranstellen des ENUM-Typ-Identifier
       verwendet werden.

    c) Natürlich kann ein ENUM-Identifier in einem inneren Scope _jeden_
       anderen Identifier aus einem äußeren Scope überlagern, d.h., das
       letzterer innerhalb des ineren Scope unsichtbar wird.

Ein ENUM-Identifier muss also folgende Eigenschaften haben:

    a) Er muss sowohl in die Symboltabelle eingetragen werden, als auch als
       Wert dem entsprechenden ENUM-Typ zugeordnet werden.
    b) Er muss dem Entrag des Typs, dem er angehört zugeordnet werden können.
    c) Er muss mehreren ENUM-Typen im gleichen Scope zugeordnet werden können.

Also:
    id -> ([ENUM-Typ (Ident-Wert) ->(
		 Ident-Wert, /* Für den Eintrag im ENUM-Typ */
		 Wert (int32_t) /* Für die Sortierung */
	    ), ...])

Der entsprechende ENUM-Typ muss folgende Eigenschaften haben:

    id -> (String->Ident-Map /* Zuordnung der ENUM-Namen zu Ident-Werten */
	   Ident->String-Table /* Rückwärtige Zuordnung */
	   Ident->Wert(int32_t)-Zuordnung).


Anm.: (2018-09-27)

Ich habe das Problem ENUMs inzwischen viel einfacher lösen können:

Jeder ENUM-Ident erhält (wie gehabt) einen Symboltabelleneintrag. Dabei kann
ein ENUM-Ident auf der gleichen Ebene (im Gegensatz zu anderen Identifiern)
auch mehrere TypeStruct-Einträge haben. Damit innerhalb dieser Einträge nur bis
zum untersten nach dem passenden TypeStruct gesucht wird, benötigt der
'TsEnumValue'-Eintrag (so habe ich den Typ für ENUM-Idents) genannt, eine
"Suchbarriere" (ein einfaches Flag). Desweiteren benötigt 'TsEnumValue' eine
Ordnungszahl und einen Verweis auf den Eintrag in der Symboltabelle, der den
zugehörigen ENUM-Typ bestimmt. Die Ordnungszahl wird beim Sammeln der
ENUM-Idents irgendwann festegelegt. Der Verweis auf den Symboltabelleneintrag
kann jedoch erst festgelegt werden, wenn der Symboltabelleneintrag für den
ENUM-Typ definiert wurde. Letzteres kann erst am Ende der 'type_declaration:'-
Regel geschehen. Dazu muss zunächst der zum ENUM-Typ gehörende TypeStruct
bestimmt werden (Lesen des semantischen Wertes des 'type'-Elementes der Regel).
Danach wird aus dem 'Ident'-Set dieses Elementes jeder Ident-Wert ausgelesen
und der zugehörige TypeStruct noch einmal aus der Symboltabelle gelesen. Dieser
erhält dann die gewünschte Information (den 'Ident'-Wert des Basistyps).

Mit diesem Schema kann nun zu jedem ENUM-Ident eine Menge von zugehörigen
ENUM-Typen bestimmt werden, aus denen dann (je nach Bedarf) der passende
ausgewählt werden kann. Bei Mehrdeutigkeiten muss sowieso der 'Ident' des ENUM-
Typs vorangestellt werden, do dass eine Bestimmung des passenden ENUM-Idents
darüber gegeben ist.
