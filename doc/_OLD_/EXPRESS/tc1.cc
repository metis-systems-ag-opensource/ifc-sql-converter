
#include <stdexception>
#include <map>
#include <string>
#include <iostream>
#include "filepos.h"

#define isin(e, ec) ((ec).count ((e)) > 0)

using TypeMap = std::map<std::string, TypeStruct *>;

// Generates a type-map from the generic parts of the given type-structure.
// If the type-structure doesn't contain any generic part, the resulting
// type-map remains empty.
// 
void get_genmap (TypeStruct *ints, TypeMap &gmap)
{
    using std::runtime_error;
    using std::string;
    const string *plabel = nullptr;
    TypeStruct *bts = nullptr, *ts = ints;
    while (ts) {
	// I planned this as recursive function ... until i recognized that
	// it is only tail-recursive, which means i can replace the
	// (conditional) recursive invocation with a while-loop where the
	// condition for the recursive invocation now is the condition of the
	// while-loop
	switch (ts->type()) {
	    case TypeStruct::aggregate: {
		// AGGREGATE [: type_label] OF <something>
		// If there is a 'type_label', get it; then get the base-type
		// of the structure for the next round of the loop.
		auto cts = dynamic_cast<TsAggregate *>(ts);
		const string &label = cts->label;
		if (label.size() > 0) { plabel = &label; }
		bts = cts->basetype;
		break;
	    }
	    case TypeStruct::array: {
		// ARRAY <bounds> OF <something>
		// There is no 'type_label', but the base-type may contain one.
		// Nonetheless, get the base-type of the structure for the next
		// round of the loop.
		auto cts = dynamic_cast<TsArray *>(ts);
		bts = cts->basetype;
		break;
	    }
	    case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
		// LIST|BAG|SET <optional_bounds> OF <something>
		// Again, there is no 'type_label', but the base-type may
		// contain one.
		auto cts = dynamic_cast<TsAggregateLike *>(ts);
		bts = cts->basetype;
		break;
	    }
	    case TypeStruct::generic_entity: case TypeStruct::generic: {
		// GENERIC_ENTITY [: type_label]
		// GENERIC [: type_label]
		// There may be a 'type_label', but there is no base-type
		// anymore.
		auto cts = dynamic_cast<TsGeneric *>(ts);
		const string &label = cts->label;
		if (label.size() > 0) { plabel = &label; }
		break;
	    }
	    default: {
		// Normal types => no 'basetype', no 'typelabel'
		// => do nothing ...
		break;
	    }
	}
	if (plabel) {
	    if (isin(*plabel, gmap)) {
		// Housten, we have a problem!
		throw runtime_error (
		    "Same label (" + *plabel + ") used in more than one"
		    " generic part of a type."
		);
	    }
	    gmap.insert (make_pair (*plabel, ts));
	}
	ts = bts; bts = nullptr;
    }
}

// The following functions are solely used for mixing two type-maps generated
// by 'get_genmap()'. Because there is the chance that two types in some
// type-maps match, doesn't mean that both of these types are equal (the
// unequality case must lead to a type error). This comparison runs deep. If
// the types compared are ARRAYs, LISTs, BAGs or SETs, not only their general
// structure must be compared, but also their index ranges, which means that
// expressions must be compared for equality (and i mean _identical_ equality
// - including the order of sub-expressions used in the index expressions. It's
// quite plain to me that in practice only a small part of these functions is
// really used. But this doesn't mean that i can leave something open for mis-
// written code.
//

// Forward-declaration of the _highly_ recursive expression comparator.
bool expr_compare (Expr *expr1, Expr *expr2);

// Comparator for lists of expressions. This function is required for the
// argument lists of function-invocations and (simple) instances (which are
// nonetheless function-invocations).
bool exprlist_compare (Fifo<Expr *> &el1, Fifo<Expr *> &el2)
{
    auto it1 = el1.begin();
    auto it2 = el2.begin();
    while (! it1.atend() && ! it2.atend()) {
	if (! expr_compare (*it1, *it2)) { return false; }
    }
    return it1.atend() && it2.atend();
}

// Comparator for two qualifier (occurring in qualified variables or function-
// invocations)
bool qual_compare (Qualifier *qual1, Qualifier *qual2)
{
    Qualifier::QualType qtt = qual1->type();
    if (qtt != qual->type()) { return false; }
    switch (qtt) {
	case Qualifier::attrqual: {
	    auto aq1 = dynamic_cast<AttrQualifier *>(qual1);
	    auto aq2 = dynamic_cast<AttrQualifier *>(qual2);
	    return aq1->attrid == aq2->attrid;
	}
	case Qualifier::groupqual: {
	    auto gq1 = dynamic_cast<GroupQualifier *>(qual1);
	    auto gq2 = dynamic_cast<GroupQualifier *>(qual2);
	    return aq1->entityid == aq2->entityid;
	}
	case Qualifier::indexqual: {
	    auto xq1 = dynamic_cast<IndexQualifier *>(qual1);
	    auto xq2 = dynamic_cast<IndexQualifier *>(qual2);
	    if (! expr_compare (xq1->lox, xq2->lox)) { return false; }
	    return expr_compare (xq1->hix, xq2->hix);
	}
	default: {
	    return false;
	}
    }
}

// Compare two expression for structual identity.
bool expr_compare (Expr *expr1, Expr *expr2)
{
    if (! expr1 && ! expr2) { return true; }
    if (! expr1 || ! expr2) { return false; }
    Expr::Type ttag = expr1->type();
    if (ttag != expr2->type()) { return false; }
    switch (ttag) {
	case Expr::binary: {
	    auto bx1 = dynamic_cast<BinaryExpr *>(expr1);
	    auto bx2 = dynamic_cast<BinaryExpr *>(expr2);
	    if (bx1->op != bx2->op) { return false; }
	    if (! expr_compare (bx1->left, bx2->left)) { return false; }
	    return expr_compare (bx1->right, bx2->right);
	}
	case Expr::unary: {
	    auto ux1 = dynamic_cast<UnaryExpr *>(expr1);
	    auto ux2 = dynamic_cast<UnaryExpr *>(expr2);
	    if (ux1->op != ux2->op) { return false; }
	    return expr_compare (ux1->right, ux2->right);
	}
	case Expr::std_invoc: {
	    auto siv1 = dynamic_cast<StdInvoc *>(expr1);
	    auto siv2 = dynamic_cast<StdInvoc *>(expr2);
	    if (siv1->stdfunc != siv2->stdfunc) { return false; }
	    return exprlist_compare (siv1->arguments, siv2->arguments);
	}
	case Expr::func_invoc: {
	    auto fiv1 = dynamic_cast<FuncInvoc *>(expr1);
	    auto fiv2 = dynamic_cast<FuncInvoc *>(expr2);
	    if (fiv1->funcref != fiv2->funcref) { return false; }
	    return exprlist_compare (fiv1->arguments, fiv2->arguments);
	}
	case Expr::interval: {
	    auto iv1 = dynamic_cast<Interval *>(expr1);
	    auto iv2 = dynamic_cast<Interval *>(expr2);
	    if (iv1->l_op != iv2->l_op || iv1->r_op != iv2->r_op) {
		return false;
	    }
	    if (! expr_compare (iv1->left, iv2->left)) { return false; }
	    if (! expr_compare (iv1->middle, iv2->middle)) { return false; }
	    return expr_compare (iv1->right, iv2->right);
	}
	case Expr::query: {
	    auto q1 = dynamic_cast<Query *>(expr1);
	    auto q2 = dynamic_cast<Query *>(expr2);
	    if (q1->id != q2->id) { return false; }
	    if (! expr_compare (q1->source, q2->source)) { return false; }
	    return expr_compare (q1->cond, q2->cond);
	}
	case Expr::logical_literal: {
	    auto llit1 = dynamic_cast<LogicalLiteral *>(expr1);
	    auto llit2 = dynamic_cast<LogicalLiteral *>(expr2);
	    return llit1->value == llit2->value;
	}
	case Expr::int_literal: {
	    auto ilit1 = dynamic_cast<IntLiteral *>(expr1);
	    auto ilit2 = dynamic_cast<IntLiteral *>(expr2);
	    return ilit1->value == ilit2->value;
	}
	case Expr::real_literal: {
	    auto rlit1 = dynamic_cast<RealLiteral *>(expr1);
	    auto rlit2 = dynamic_cast<RealLiteral *>(expr2);
	    return rlit1->value == rlit2->value;
	}
	case Expr::binary_literal: {
	    auto blit1 = dynamic_cast<BinaryLiteral *>(expr1);
	    auto blit2 = dynamic_cast<BinaryLiteral *>(expr2);
	    auto bllen = blit1->length();
	    if (bllen != blit2->length()) { return false; }
	    for (uint32_t ix = 0; i < bllen; ++ix) {
		if (blit1->value[ix] != blit2->value[ix]) { return false; }
	    }
	    return true;
	}
	case Expr::string_literal: {
	    auto slit1 = dynamic_cast<StringLiteral *>(expr1);
	    auto slit2 = dynamic_cast<StringLiteral *>(expr2);
	    return slit1->value == slit2->value;
	}
	case Expr::unspec_literal: { return true; }
	case Expr::aggregate: {
	    auto agg1 = dynamic_cast<Aggregate *>(expr1);
	    auto agg2 = dynamic_cast<Aggregate *>(expr2);
	    return exprlist_compare (agg1->values, agg2->values);
	}
	case Expr::nxaggregate: {
	    auto agg1 = dynamic_cast<NxAggregate *>(expr1);
	    auto agg2 = dynamic_cast<NxAggregate *>(expr2);
	    auto it1 = agg1->rvalues.begin();
	    auto it2 = agg2->rvalues.begin();
	    while (! it1.atend() && ! it2.atend()) {
		if (! expr_compare (it1->value, it2->value)) {
		    return false;
		}
		if (! expr_compare (it1->repetition, it2->repetition)) {
		    return false;
		}
	    }
	    return it1.atend() && it2.atend();
	}
	case Expr::enum_value: {
	    auto e1 = dynamic_cast<EnumValue *>(expr1);
	    auto e2 = dynamic_cast<EnumValue *>(expr2);
	    return e1->enumeration == e2->enumeration && e1->value == e2->value;
	}
	case Expr::ref2const: {
	    auto rc1 = dynamic_cast<Ref2Const *>(expr1);
	    auto rc2 = dynamic_cast<Ref2Const *>(expr2);
	    return rc1->id == rc2->id && rc1->ctx == rc2->ctx;
	}
	case Expr::reference: {
	    auto r1 = dynamic_cast<Reference *>(expr1);
	    auto r2 = dynamic_cast<Reference *>(expr2);
	    Reference::RefType rttag = r1->reftype();
	    if (rttag != r2->reftype()) { return false; }
	    switch (rttag) {
		case Reference::sinvocref: {
		    auto ir1 = dynamic_cast<SInvocRef *>(r1);
		    auto ir2 = dynamic_cast<SInvocRef *>(r2);
		    return expr_compare (ir1->invoc, ir2->invoc);
		}
		case Reference::invocref: {
		    auto ir1 = dynamic_cast<InvocRef *>(r1);
		    auto ir2 = dynamic_cast<InvocRef *>(r2);
		    return expr_compare (ir1->invoc, ir2->invoc);
		}
		case Reference::varref: {
		    auto vr1 = dynamic_cast<VarRef *>(r1);
		    auto vr2 = dynamic_cast<VarRef *>(r2);
		    return vr1->id == vr2->is && vr1->context == vr2->context
			&& vr1->_isConst == vr2->_isConst;
		}
		case Reference::selfref: {
		    auto sr1 = dynamic_cast<SelfRef *>(r1);
		    auto sr2 = dynamic_cast<SelfRef *>(r2);
		    return sr1->atype == sr1->atype;
		}
		case Reference::RefRef: {
		    auto sr1 = dynamic_cast<RefRef *>(r1);
		    auto sr2 = dynamic_cast<RefRef *>(r2);
		    if (! expr_compare (sr1->reference, sr2->reference)) {
			return false;
		    }
		    return qual_compare (sr1->qualifier, sr2->qualifier);
		}
		default: {
		    return false;
		}
	    }
	}
	default: {
	    // makes not much sense here, as i already caught all valid cases.
	    // Sadly, the C++-compiler doesn't see it that way, which is the
	    // reason why a 'default'-case should be always given.
	    return false;
	}
    }
}

// Compare two type-structs for structural identity
bool ts_compare (TypeStruct *ts1, TypeStruct *ts2)
{
    TypeStruct::Type ttag = ts1->type();
    if (ttag != ts2->type()) { return false; }
    // FACT: *ts1 and *ts2 are of the same 'TypeStruct'-subtype
    switch (ttag) {
	case TypeStruct::aggregate: {
	    TypeStruct *bts1 = (dynamic_cast<TsAggregate *>(ts1))->basetype;
	    TypeStruct *bts2 = (dynamic_cast<TsAggregate *>(ts2))->basetype;
	    if (bts1->label != bts2->label) { return false; }
	    return ts_compare (bts1, bts2);
	}
	case TypeStruct::generic_entity: case TypeStruct::generic: {
	    TypeStruct *gts1 = dynamic_cast<TsGeneric *>(ts1);
	    TypeStruct *gts2 = dynamic_cast<TsGeneric *>(ts2);
	    return gts1->label == gts2->label;
	}
	case TypeStruct::array: {
	    TypeStruct *ats1 = dynamic_cast<TsArray *>(ts1);
	    TypeStruct *ats2 = dynamic_cast<TsArray *>(ts2);
	    if (ats1->optional != ats2->optional
	    ||  ats1->unique != ats2->unique) {
		return false;
	    }
	    if (! expr_compare (ats1->lwb, ats2->lwb)) { return false; }
	    if (! expr_compare (ats1->upb, ats2->upb)) { return false; }
	    return ts_compare (ats1->basetype, ats2->basetype);
	}
	case TypeStruct::list: {
	    TypeStruct *lts1 = dynamic_cast<TsList *>(ts1);
	    TypeStruct *lts2 = dynamic_cast<TsList *>(ts2);
	    if (ats1->unique != ats2->unique
	    ||  ats1->min != ats2->min
	    ||  ats1->max != ats2->max) {
		return false;
	    }
	    return ts_compare (ats1->basetype, ats2->basetype);
	}
	case TypeStruct::bag: case TypeStruct::set: {
	    TypeStruct *alts1 = dynamic_cast<TsAggregateLike *>(ts1);
	    TypeStruct *alts2 = dynamic_cast<TsAggregateLike *>(ts2);
	    if (alts1->min != alts2->min || alts1->max != alts2->max) {
		return false;
	    }
	    return ts_compare (alts1->basetype, alts2->basetype);
	}
	case TypeStruct::defined: {
	    TypeStruct *dts1 = dynamic_cast<TsDefined *>(ts1);
	    TypeStruct *dts2 = dynamic_cast<TsDefined *>(ts2);
	    return dts1->basetype == dts2->basetype;
	}
	case TypeStruct::typeref: {
	    TypeStruct *rts1 = dynamic_cast<TsTypeRef *>(ts1);
	    TypeStruct *rts2 = dynamic_cast<TsTypeRef *>(ts2);
	    return rts1->id == rts2->id && rts1->isentity == rts2->isentity;
	}
	default: {
	    // Because here remain only basic types (SELECT- and ENUM-types
	    // cannot directly occur where this function is applied), the
	    // result here must be true, as an un-equality was already caught
	    // above (before the 'switch'-statement).
	    return true;
	}
    }
}

// Merge a type-map (in) into another one (out). If an element of 'in' already
// occurs in 'out', no insertion (aka replacement) will take place. Instead,
// the type-structures with the same type-label compared for compatibility
// (here this means: structural identicalness). It is an error if the two type-
// structures don't match; in this case the 'type_label' of the ambiguous type-
// structure is returned through 'ambig_level' and the function result is
// 'false'. Otherwise, 'ambig_value' is left unchanged and the function result
// is 'true'.
bool merge_map (TypeMap &in, TypeMap &out, std::string &ambig_label)
{
    using std::runtime_error;
    using std::string;
    for (auto iit = in.begin(); iit != in.end(); ++iit) {
	const string &label = in->first;
	auto oit = out.find (label);
	if (oit == out.end()) {
	    oit.insert (*iit);
	}
	if (! ts_compare (iit->second, oit->second)) {
	    ambig_label = label; return false;
	}
    }
    return true;
}


// I need a type-comparison for compatibility (not structural identicalness).
// This is meant to comparing an extracted type (from an expression) with
// a given type (say of a function-parameter or an attribute). Compatible
// are (e.g.) INTEGER and REAL with NUMBER, an enumeration-value with the
// corresponding enumeration-type, an entity-value (simple or complex instance)
// with an entity, an ARRAY, LIST, BAG or SET with an AGGREGATE, supposed that
// the corresponding base-types match and that the concrete type instanced for
// this generic type - if set - is compatible (meaning _identical_) to this
// ARRAY-, LIST-, BAG- or SET-type. In an instance-expression, the two sides
// of the '||' must be of an entity-type and/or one of its super-types for
// being compatible. In an assignment, the left hand side _must_ be a qualified
// variable, as must it be in a VAR-parameter of a procedure. I'm using
// a 'shared_ptr<TypeStruct> &' as the type of the argument for a reason:
//   Sometimes i have to extract the type of an expression from the symbol
//   table. Because i don't want to copy these type-structs (and can't it as
//   these type have a strict move-semantic), but sometimes (in the case of
//   simple types) i need to construct a 'TypeStruct'-object dynamically),
//   'shared_ptr<...>'-objects are the only choice.
// But i have a problem therein with 'shared_ptr<...>'-objects. They (in most
// cases) own the pointers they are attached to, meaning that the corresponding
// objects will be deleted if they are not used any longer. This leads to the
// strange situation that some of these objects may be deleted unintentionally.
// I have two ways to solve this problem:
//   1 Making all TypeStruct-pointers "shared" (i don't know if the parser
//     supports this),
//   2 Adding a "deleter" which does simply nothing in the case that an object
//     is only referenced (because it's a symbol-table entry).
// I need to check first if #1 could be made "workable". If this is not the
// case, i need to use #2. Last not least, i'm adding a "deleter"-class here
// (which doesn't delete it's objects) for convenience.

template<class T> class NonDeleter {
  public:
    void operator() (T * ptr) {
	// Do nothing here!
    }
};

using TsKeeper = NonDeleter<TypeStruct>;
using TypeStructSp = shared_ptr<TypeStruct>;


// Check if two type
bool comparable (TypeStructSp &lts, TypeStructSp &rts, bool equality)
{
    TypeStruct::Type lttag = lts->type(), rttag = rts->type(), ttag;
    switch (lttag) {
	case TypeStruct::boolean: case TypeStruct::logical: {
	    return rttag == TypeStruct::boolean ||
		   rttag == TypeStruct::logical ||
		   rttag == TypeStruct::indeterminate;    /* '?' => TsUnset */
	}
	case TypeStruct::integer: {
	    return rttag == TypeStruct::integer ||
		   rttag == TypeStruct::number ||
		   rttag == TypeStruct::indeterminate;
	}
	case TypeStruct::real: {
	    return rttag == TypeStruct::real ||
		   rttag == TypeStruct::number ||
		   rttag == TypeStruct::indeterminate;
	}
	case TypeStruct::number: {
	    return rttag == TypeStruct::integer ||
		   rttag == TypeStruct::real ||
		   rttag == TypeSTruct::number ||
		   rttag == TypeStruct::indeterminate;
	}
	case TypeStruct::string: case TypeStruct::binary:
	case TypeStruct::enumeration: {
	    return rttag == lttag ||
		   rttag == TypeStruct::indeterminate;
	}
	case TypeStruct::indeterminate: {
	    if (equality) { return true; }
	    return rttag == TypeStruct::indeterminate ||
		   rttag == TypeStruct::boolean ||
		   rttag == TypeStruct::logical ||
		   rttag == TypeStruct::integer ||
		   rttag == TypeStruct::real ||
		   rttag == TypeStruct::number ||
		   rttag == TypeStruct::string ||
		   rttag == TypeStruct::binary ||
		   rttag == TypeStruct::enumeration;
	}
	default:
	    return (equality && (lttag == rttag));
    }
}

bool get_type (Symtab &symtab, Expr *e, TypeStructSp &ts);
   
bool same_enums (TypeStructSp &lts, TypeStructSp %rts)
{
    Ident lsym, lctx, rsym, rctx;
    (dynamic_cast<TsEnum *>(lts.get()))->getBackref (lsym, lctx);
    (dynamic_cast<TsEnum *>(rts.get()))->getBackref (rsym, rctx);
    return lsym == rsym && lctx == rctx;
}

bool is_extenum (TypeStruct &ts)
{
    Ident basetype = (dynamic_cast<TsEnum *>(ts.get()))->basedon;
    bool extensible = (dynamic_cast<TsEnum *>(ts.get()))->extesible;
    return basetype > 0 || extensible;
}

// This function assumes that 'lts' and 'rts' are of an aggregate-type of
// the same type
bool compatible_comptypes (Symtab &symtab, TypeStruct *lts, TypeStruct *rts)
{
    TypeStruct::Type lttag = lts->type(), rttag = rts->type();
    TypeStruct *lsts, *rsts;
    if (lttag != rttag) { return false; }
    switch (lts->type()) {
	case TypeStruct::aggregate: {
	    lsts = (dynamic_cast<TsAggregate *>(lts))->basetype;
	    rsts = (dynamic_cast<TsAggregate *>(rts))->basetype;
	    break;
	}
	case TypeStruct::array: {
	    lsts = (dynamic_cast<TsAggregate *>(lts))->basetype;
	    rsts = (dynamic_cast<TsAggregate *>(rts))->basetype;
	    break;
	}
	case TypeStruct::list: case TypeStruct::bag: TypeStruct::set: {
	    lsts = (dynamic_cast<TsAggregateLike *>(lts))->basetype;
	    rsts = (dynamic_cast<TsAggregateLike *>(rts))->basetype;
	    break;
	}
	default:
	    return false;
    }
    if (
}


bool get_type (Symtab &symtab, BinOp op, Expr *left, Expr *right,
	       TypeStructSp &ts)
{
    TypeStructSp lts, rts;
    if (! get_type (symtab, left, lts)) { return false; }
    if (! get_type (symtab, right, rts)) { return false; }
    // First step: reduce both type-structs to their base-types.
    TypeStructSp lbts = get_basetype (symtab, lts);
    TypeStructSp rbts = get_basetype (symtab, rts);
    switch (op) {
	case BinOp::lt: case BinOp::gt: case BinOp::le: case BinOp::ge: {
	    if (! comparable (lbts, rbts, false)) { return false; }
	    if (lts->type() == TypeStruct::enumeration) {
		// FACT: rts is an enumeration, too!
		if (! same_enums (lbts, rbts)) { return false; }
		if (is_extenum (lbts) || is_extenum (rbts)) {
		    // The ISO-Specification of EXPRESS (ISO 10303-11) clearly
		    // says that an ENUMERATION which are extensible or is an
		    // extension of anothe ENUMERATION is *not* ordered, and so
		    // not comparable with one of the operators '<', '>', '<='
		    // and '>='. It is (yet) not clear if the operations are
		    // valid in this case and should only return the
		    // "unspecified value", or if comparisons on these types
		    // are generally illegal. I'm chossing the second way here.
		    // If this is wrong, one has either to remove the complete
		    // 'if'-block, or invert the return-value ...
		    return false;
		}
	    }
	    ts.reset (new TsLogical);
	    return true;
	}
	case BinOp::ne: case BinOp:eq: case BinOp::ine: case BinOp:ieq: {
	    if (! comparable (lbts, rbts, true)) { return false; }
	    // Now, we have not only the ENUMERATIONs which are comparable,
	    // but each other type, too. The problem here are the ENTITY-types.
	    // Are different ENTITY-types (structural) comparable? If so, must
	    // the type-structures of these ENTITY-types be identical? Or is it
	    // only required that they have a similar structure. Are super-/
	    // subtype-relations required for ENTITY-types being comparable?
	    static const std::set<TypeStruct::Type> aggregate_like {
		TypeStruct::array, TypeStruct::list, TypeStruct::bag,
		TypeStruct::set
	    };
	    if (lts->type() == TypeStruct::enumeration) {
		// FACT: rts is an enumeration, too!
		if (! same_enums (lbts, rbts)) { return false; }
	    } else if (isin (lbts->type(), aggregate_like)) {
		if (! compatible_subtypes (symtab, lbts, rbts)) {
		    return false;
		}
	    }
	}
	case BinOp::and_: case BinOp::or_: case BinOp::xor_: {
	    static const std::set<TypeStruct::Type> logical_type {
		TypeStruct::boolean, TypeStruct::logical,
		TypeStruct::indeterminate
	    };
	    if (isin(lbts->type(), logical_type)
	    || isin(rbts->type(), logical_type)) {
		returbn 
	    }
	case BinOp::pow: {
	    
	}

bool get_type (Symtab &symtab, Expr *e, TypeStructSp &ts)
{
    Expr::Type ettag = e->type();
    switch (ettag) {
	case Expr::binary: {
	    auto binexpr = dynamic_cast<BinaryExpr *>(e);
	    TypeStructSp lts, rts;
	    if (! get_type (symtab, binexpr->left, lts)) { return false; }
	    if (! get_type (symtab, binexpr->right, rts)) { return false; }
	}
	case Expr::unary: { }
	case Expr::std_invoc: { }
	case Expr::func_invoc: {
	    // Here, we must determine first, if the function-identifier is not
	    // that of an entity. In this case, we the expression's 'entity'-
	    // flag must be set!
	    auto invoc = dynamic_cast<FuncInvoc *>(ts);
	    Ident id = invoc->funcref;
	    TypeStruct *ts1 = symtab[id];
	    if (ts1->type() == TypeStruct::entity) {
		// In order to avooid code-duplication, i invoke 'get_type()'
		// here recursively. The next invocation should then lead to
		// the 'Expr::instance'-case.
		invoc->setEntity(); return get_type (symtab, e, ts);
	    }
	    // Otherwise, a function-invocation must be checked, and then the
	    // function's return type returned (duplicated!) as the result
	    // of the function ...
	}
	case Expr::interval: {
	    // The parts of an interval-expression must be of type INTEGER,
	    // REAL or number. For mixed INTEGER/REAL/NUMBER-values, the
	    // type NUMBER is assumed. In all cases, the type of an interval
	    // expression is always of type LOGICAL
	    auto iv = dynamic_cast<Interval *>(e);
	    Expr *le = iv->left, *me = iv->middle, *re = iv->right;
	    TypeStructSp lts, mts, rts;
	    if (! get_type (symtab, le, lts)
	    ||  ! get_type (symtab, le, mts)
	    ||  ! get_type (symtab, le, rts)) {
		return false;
	    }
	    std::set<TypeStruct::Type> numerics {
		TypeStruct::integer, TypeStruct::real, TypeStruct::number,
		TypeStruct::indeterminate
	    };
	    if (! isin (lts->type(), numerics)) {
		cerr << le->where() << ": Expression is non-numeric" << endl;
		return false;
	    }
	    if (! isin (mts->type(), numerics)) {
		cerr << me->where() << ": Expression is non-numeric" << endl;
	    }
	    if (! isin (rts->type(), numerics)) {
		cerr << re->where() << ": Expression is non-numeric" << endl;
	    }
	    // What to return in the case that one of the sub-expression of an
	    // interval-expression is indeterminate? Is it important? Because
	    // the standard says that an interval-expression should evaluate to
	    // 'unknown' if one of its participants evaluates to the
	    // indeterminate value.
	    ts->reset (new TsLogical);
	    return true;
	}
	case Expr::query: {
	    // The type of the query is an aggregate. If the type of its
	    // source expression is determined, this type is also the type
	    // of the query, even if the type of the source is a generic type.
	    // In the case of the source being '?' (the unspecified value),
	    // the result must be the unspecified value, too.
	}
	case Expr::logical_literal: {
	    ts->reset (new TsLogical); return true;
	}
	case Expr::int_literal: {
	    ts->reset (new TsInteger); return true;
	}
	case Expr::real_literal: {
	    ts->reset (new TsReal); return true;
	}
	case Expr::binary_literal: {
	    ts->reset (new TsBinary); return true;
	}
	case Expr::string_literal: {
	    ts->reset (new TsString); return true;
	}
	case Expr::unspec_literal: {
	    ts->reset (new TsUnset); return true;
	}
	case Expr::nxaggregate: {
	    // Because of SELECT-types, members of AGGREGATE-values can have
	    // any type. I don't know (yet) if it is enforced by EXPRESS that
	    // all members of an aggregate must have the same values. So its
	    // type can only be AGGREGATE OF GENERIC. Additionally, values of
	    // an NxAggregate cam have repetition factors. These _should_ be
	    // resolved, so that the result is a plain aggregate.
	}
	case Expr::aggregate: { }
	case Expr::enum_value: {
	    // The value is that of the enumeration the value is attached to.
	    // In an EXPRESS data-specification, an enum-value's enum-type may
	    // be unspecfied, but at this point, it is already resolved,
	    // leading directly (via the symbol table) to the corresponding
	    // enumeration-type.
	    auto enumval = dynamic_cast<EnumValue *>(e);
	    auto basetypeid = enumval->enumeration;
	    if (basetypeid == 0) {
		// This is an ERROR! I (yet) don't know if i have done all
		// things correct. In any case, this is an INTERNAL ERROR!
		FilePos &pos = enumval->where();
		Ident id = enumval->value;
		const std::string &name =  symtab.symname (id);
		throw runtime_error (
		    pos + ": INTERNAL ERROR! Enumeration value"
		    " (" + name + ") with unknown base-type found."
		);
	    }
	    ts.reset (symtab[basetypeid], NonDeleter);
	    return true;
	}
	case Expr::ref2const: {
	    // A constant should be de-structured, meaning: it's initial
	    // expression retrieved and this expression's type structure
	    // generated.
	}
	case Expr::reference: {
	    // References are a bit tricky. At first they must be de-
	    // constructed to their base. If this base is a function-
	    // invocation, this invocation's type must be extracted and
	    // then the reference re-constructed (calculating its type in this
	    // process). For 'SELF', the current valid type-environment
	    // TYPE, ENTITY) must be retrieved and instantiated for the
	    // 'SELF'-keyword. Then ...
	}
    }
}


TypeStruct *tcbasetype (TypeStruct *ts)
{
    switch (ts->type()) {
	case TypeStruct::aggregate: {
	    return (dynamic_cast<TsAggregate *>(ts))->basetype;
	}
	case TypeStruct::array: {
	    return (dynamic_cast<TsArray *>(ts))->basetype;
	}
	case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
	    return (dynamic_cast<TsAggregateLike *>(ts))->basetype;
	}
	default: {
	    return ts;
	}
    }
}

bool is_concrete (TypeStruct *ts)
{
    switch (ts->type()) {
	case TypeStruct::aggregate:
	case TypeStruct::generic:
	case TypeStruct::generic_entity:
	    return false;
	default:
	    return true;
    }
}

std::string tcgetlabel (TypeStruct *ts)
{
    switch (ts->type()) {
	case TypeStruct::aggregate: {
	    auto ats = dynamic_cast<TsAggregate *>(ts);
	    if (is_concrete (ats->basteype)) {
		return ats->label;
	    }
	    return tcgetlabel (ats->basetype);
	case TypeStruct::generic: case TypeStruct::generic_entity: {
	    auto gts = dynamic_cast<TsGeneric *>(ts);
	    return gts->label;
	case TypeStruct::array: {
	    auto ats = dynamic_cast<TsArray *>(ts);
	    return tcgetlabel (ats->basetype);
	case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
	    auto ats = dynamic_cast<TsAggregateLike *>(ts);
	    return tcgetlabel (lts->basetype);
	default:
	    return "";
    }
}
	    

bool tcgettype (TypeStruct *ts, map<string, TypeStruct *> gmap,
		TypeStruct * &outts)
{
    bool is_entity = false;
    const string *plabel = nullptr;
    TypeStruct::Type ttag = ts->type();
    switch (ttag) {
	case TypeStruct::aggregate: {
	    auto ats = dynamic_cast<TsAggregate *>(ts);
	    plabel = &ts->label;
	    break;
	}
	case TypeStruct::generic: case TypeStruct::generic_entity: {
	    auto gts = dynamic_cast<TsGeneric *>(ts);
	    plabel = &gts->label;
	    is_entity = fts->entity;
	    break;
	}
	default:
	    break;
    }
    if (typelabel) {
	const string &typelabel = *plabel;
	if (! isin(typelabel, gmap)) {
	    cerr << ts->where() <<
		    ": Unable to determine a concrete function return type." <<
		    endl;
	    return false;
	}
	
	outts = gmap[typelabel];
	return true;
    }
    outts = ts;
    return true;
}
