FUNCTION ROLESOF (v: GENERIC_ENTITY): SET OF STRING;

Result is a set of the fully qualified names of the roles, the given entity
instance 'V' plays in a model described by the EXPRESS-schema. The full
qualified role-names consist of

    SCHEMA.ENTITY.ATTRIBUTE

where SCHEMA is the name of the EXPRESS-schema, ENTITY the name of the entity
therein and ATTRIBUTE, the name of the attribute whose value is 'V'. The
resulting strings are all upper case.
