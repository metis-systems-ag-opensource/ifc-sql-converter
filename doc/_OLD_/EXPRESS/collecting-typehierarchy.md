# Wie sammle ich die Typ-hierarchie?


Jede ENTITY, die 'subtype'-Beziehungen hat, ist zumindest als SUPERTYPE
gekennzeichnet, jede ENTITY, die ein 'subtype' ist, hat die Kennzeichnung
SUBTYPE, sowie des ENTITY-type (bzw. eine Liste der ENTITY-typen), deren
(dessen) SUBTYPE er ist.

## Was bedeutet das im Klartext?

ENTITY-Typen ohne SUPERTYPE-Markierung sind die "Blätter" im Baum der Typ-
hierarchie.

Geht man von so einem Blatt aus, dann die gesamte Typ-Hierarchie von diesem
aus bis zum obersten SUPERTYPE gesammelt werden. Jeder Obertyp erhält hierbei
eine Liste der _direkt_ von ihm abgeleiteten Typen. Wurde ein SUPERTYPE bereits
einmal eingesammelt, dann wird der betreffende SUBTYPE einfach nur noch der
Liste dieses SUPERTYPE hinzugefügt.

Hat ein ENTITY-Typ keine SUBTYPEs und ist auch selbst keiner, dann steht er für
sich selbst und muss auch nicht weiter betrachtet werden.

Ist ein ENTITY-Typ als ABSTRACT markiert (dann ist er in jedem Fall ein
SUPERTYPE), dann muss für diesen Typ ein (virtueller) SELECT-Datentyp angelegt
werden, der sämtliche Untertypen enthält.

## Neue Idee zum Sammeln der Typ-Hierarchie:

1. Nim einen ENTITY-Typ und trage ihn in eine Liste ein.
2. Wenn dieser ENTITY-Typ werder SUBTYPE noch SUPERTYPE ist, dann ignoriere
   ihn ansonsten.
3. Ist der ENTITY-Typ ein SUPERTYPE, dann lege ihn in einer Map ab. Ist er dort
   bereits vorhanden, dann tue hier nichts.
4. Ist der ENTITY-Typ ein SUBTYPE, dann schaue nach, ob sein direkter SUPERTYPE
   bereits in der Map ist. Falls nicht, dann lege letzteren dort ab, mit einem
   Verweis auf den ENTITY-Typ als dessen SUBTYPE. Falls ja, dann füge einen
   weiteren Verweis auf den ENTITY-Typ bei dem betrachteten SUPERTYPE in der
   MAP ab. (Bei einer Liste von SUPERTYPEs muss das für jedes Element in dieser
   Liste geschehen.)

<s>
5. Für jedes Element in der Liste der gesammelten ENTITY-Typen:
    1. Schaue nach, ob der betrachtete ENTITY-Typ ein Attribut von einem
       ENTITY-Typ enthält.
    2. Falls ja:
	1. Ist der ENTITY-Typ des Attributes ein ABSTRACT SUPERTYPE?
	2. Falls ja:
	    1. Generiere einen "virtuellen" SELECT-Typ mit sämtlichen SUBTYPEs
	       des ENTITY-Typs, wobei SUBTYPEs, die ebenfalls als ABSTRACT
	       deklariert sind, als "virtuelle" SELECT-Typen deklariert werden
	       müssen.
</s>

5. Für jede gesammelte ENTITY:
    1. Schaue nach, ob diese ENTITY als ABSTRACT markiert ist.
    2. Falls ja:
	1. Generiere einen Eintrag in die Symboltabelle mit dem Namen der
	   ENTITY mit '\_tmp\_'-Präfix, jedoch als SELECT-Datentyp, mit
	   Komponenten, die aus allen SUBTYPEs dieser ENTITY bestehen. Hierbei
	   müssen natürlich Komponenten, die selbst ABSTRACT sind, wiederum
	   durch '\_tmp\_'-Einträge ersetzt werden.

6. Wenn bei der Tabellen-Generierung nun ein Eintrag vom Typ ABSTRACT ENTITY
   auftritt, dann ersetze diesen durch den '\_tmp\_\<ENTITY-_name_\>' und
   veranlasse die Generierung einer entsprechenden SELECT-Tabelle.
