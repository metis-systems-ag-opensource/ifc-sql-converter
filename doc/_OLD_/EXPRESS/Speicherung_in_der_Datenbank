Jeder ENTITY-Wert - eigentlich jede Instanz - benötigt genau einen Schlüssel,
der global für das jeweilige Modell ist. Dazu muss eine Tabelle mit Schlüssel-
zählern angelegt werden:

    CREATE TABLE ModelNextId (
      model_id INTEGER NOT NULL PRIMARY KEY,
      next_id INTEGER NOT NULL
    );

Bei jedem Speichern muss zunächst ein Schlüssel aus dieser Tabelle besorgt
werden.

Jede Tabelle hat zwei Attribute, die zusammen den Primärschlüssel bilden,
nämlich die (bereits oben angegebene) 'model_id' und den 'key', der aus der
Tabelle 'ModelNextId' gewonnen wird.

Beim Eintraggen von Werten in die DB muss zunächst eine 'model_id' generiert
werden. Diese muss irgendwie aus den HEADER-Daten der STEP-Datei generiert
werden. Anschließend muss ein neuer Eintrag in der Datei 'ModelNextId' für
diese 'model_id' generiert werden, mit einem initialen Wert von 1 für
'next_id'.

Wird die Speicherung einer ENTITY angestoßen, dann muss - passend zur
'model_id', ein Schlüssel generiert werden. Danach muss irgendwo verzeichnet
werden, zu welchem konkreten Typ der Eintrag gehört. Dazu wird eine weitere
Tabelle benötigt:

    CREATE TABLE ModelIdType (
      model_id INTEGER NOT NULL,
      objid INTEGER NOT NULL,
      type VARCHAR(255) NOT NULL,
      PRIMARY KEY (model_id, objid)
    );
    CREATE INDEX ModelIdType_type ON ModelIdType(type ASC);

Anstatt des 'VARCHAR'-Feldes könnte auch ein INTEGER-Wert vorhanden sein, der
auf eine Tabelle mit Namen für die einzelnen ENTITY-Typen verweist.

Dann wäre die Überlegung da, ob nicht für jeden ENTITY-Typ eine Tabelle mit
allen Attributen generiert werden sollte (mit den Attributen aller überge-
ordneten Typen). Eine direkte Realisierung von Vererbungsmechanismen wäre
so nicht mehr gegeben ... aber in einer DB-Repräsentation stört das nicht
weiter, da die direkten Objekt-Zusammenhänge im Objekt-Modell erfasst sind.

Falls notwendig können die verschiedenen ENTITY-Tabellen auch per SQL-UNION
zusammengefasst werden, wobei fehlende Attribute natürluch durch NULL ersetzt
werden würden. Auch kann der Typ ebenfalls mit in einen solchen SQL-Befehl
integriert werden, so dass der Typ im Objektmodell dadurch bestimmt werden kann.



