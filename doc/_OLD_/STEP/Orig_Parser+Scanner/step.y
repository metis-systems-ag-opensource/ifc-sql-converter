/* step.y
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@pps-ag.com
** Copyright: (c) 2018, PPS AG
** 
**
** Simple STEP-parser, parser part ...
**
*/

%skeleton "lalr1.cc"
%require "3.0"
%debug
%defines
%define api.namespace {STEP}
%define parser_class_name {Parser}
%locations

%code requires{
    namespace STEP {
	class Driver;
	class Scanner;
    }

// The following definition is missing when %locations isn't used
#   if YY_NULLPTR
#     if defined __cplusplus && __cplusplus >= 201103L
#       define YY_NULLPTR nullptr
#     else
#       define YY_NULLPTR 0
#     endif
#   endif

}

%parse-param { Scanner &scanner }
%parse-param { Driver &driver }

%code{
#   include <iostream>
#   include <cstdlib>
#   include <fstream>

    /* Including all driver functions ... */
#   include "driver.h"

#undef yylex
#define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert

%start step_file

%initial-action{
    // Initialize the initial location object
    @$.begin.filename = @$.end.filename = &driver.streamname;
}

%error-verbose

/*
%union {
    / * The results from the scanner are all in the form of strings * /
    std::string *tokenstring;
    long long intval;
    struct BitString { unsigned long long bits; int length; } bitstrg;
    unsigned long long binval;
    long double floatval;
    char *strgval;
    logical_t logicval;
    void *ptree;
    twoints_t implevel;
}
*/

/* Tokens which refer to true keywords ... */
%token INVALID END
%token ANCHOR DATA ENDSEC FILE_DESCRIPTION FILE_NAME FILE_POPULATION
%token FILE_SCHEMA HEADER REFERENCE SCHEMA_POPULATION SECTION_CONTEXT
%token SECTION_LANGUAGE SIGNATURE STEPBEGIN STEPEND

/* Tokens which refer to some form of identifiers. These tokens represent
** token classes, with the identifier's name supplied as string by the
** scanner ...
*/
%token CONSTINSTID CONSTVALID INSTANCEID ENUM KEYWORD
%token USERDEFINED VALUEID

/* Tokens which refer to real values (integers, floating point numbers,
** strings, binary literals and URLs). The enumerated values are represented
** by the corresponding "identifier" value and thus are named above ...
*/
%token LOGLIT BINLIT FLOATLIT INTLIT STRGLIT URL BASE64_DATA

/* The result-types of the productions ... */

%{
    //typedef void *scan_t;

    //int yylex (YYSTYPE *lval, YYLTYPE *loc, scan_t scanner);

    //static int get_string (const char *s, std::string &out);
    //static int get_bitstring (const char *repr, BitString &out);
    //static int get_intval (const char *repr, long long &out);
    //static int get_urlval (const char *repr, std::string &out);
    //static int get_constrefid (const char *repr, constrefid_t &out);
    //static int get_refid (const char *repr, refid_t &out);
%}

%%

step_file:
      STEPBEGIN ';'
      header_section[headers]
      anchor_section[anchors]
      reference_section[references]
      data_section_list[data]
      STEPEND ';'
    ;

header_section[header]:
      HEADER ';'
      file_description[desc] ';'
      file_name[name] ';'
      file_schema[schema] ';'
      other_header_items[other]
      ENDSEC ';'
      signature_section[sig]
    ;

file_description[desc]:
      FILE_DESCRIPTION '('
      '(' description_list[dlist] ')' ','
      implementation_level[ilevel]
      ')'
    ;

description_list[res]:
      description_list[list] ',' description_item[item]
    | description_item[item]
    ;

description_item[result]:
      string_value[value]
    ;

implementation_level[level]:
      string_value[orig]
    ;

file_name[result]:
      FILE_NAME '('
      name ','
      timestamp ','
      '(' author_list ')' ','
      '(' organization_list ')' ','
      preprocessor_version ','
      originating_system ','
      authorization
      ')'
    ;

name:
      string256		/* maximum length: 256 */
    ;

timestamp:
      string256		/* maximum length: 256 */
    ;

author_list[result]:
      author_list[list] ',' author[item]
    | author[item]
    ;

author:
      string256		/* maximum length: 256 */
    ;

organization_list[result]:
      organization_list[list] ',' organization[item]
    | organization[item]
    ;

organization:
      string256		/* maximum length: 256 */
    ;

preprocessor_version:
      string256		/* maximum length: 256 */
    ;

originating_system:
      string256		/* maximum length: 256 */
    ;

authorization:
      string256		/* maximum length: 256 */
    ;

file_schema:
      FILE_SCHEMA '('
      '(' schema_list ')'
      ')'
    ;

schema_list[result]:
      schema_list[list] ',' schema_name[item]
    | schema_name[item]
    ;

schema_name:
      string1024	/* maximum length: 1024 */
    ;

other_header_items[res]:
      other_header_items[list] other_header_item[item] ';'
    |
    ;

other_header_item[res]:
      schema_population[spop]
    | file_population[fpop]
    | section_context[sctx]
    | section_language[slang]
    | user_defined[uheader]
    ;

schema_population[res]:
      SCHEMA_POPULATION '('
      '(' external_file_identification_list[exfids] ')'
      ')'
    ;

external_file_identification_list[res]:
      external_file_identification_list[list] ','
      external_file_identification[item]
    | external_file_identification[item]
    ;

external_file_identification:
      '(' external_file_idstrings ')'
    ;

external_file_idstrings:
      string_value ',' string_value ',' string_value
    | string_value ',' string_value
    | string_value
    ;

file_population[res]:
      FILE_POPULATION '('
      governing_schema[gschema] ','
      determination_method[dmethod] ','
      governed_sections[gsections]
      ')'
    ;

governing_schema[res]:
      string_value[val]
    ;

determination_method[res]:
      string_value[val]
    ;

governed_sections[res]:
      '(' section_name_list[list] ')'
    | '$'
    ;

section_name_list[res]:
      section_name_list[list] ',' section_name[item]
    | section_name[item]
    ;

section_name[res]:
      string_value[val]
    ;

section_context[res]:
      SECTION_CONTEXT '('
      section_name_opt[name] ','
      '(' context_ident_list[list] ')'
      ')'
    ;

section_name_opt[res]:
      section_name[name]
    | '$'
    ;

context_ident_list[res]:
      context_ident_list[list] ',' context_ident[item]
    | context_ident[item]
    ;

context_ident[res]:
      string_value[val]	/* unspecified length */
    ;

section_language[res]:
      SECTION_LANGUAGE '('
      section_name_opt[name] ','
      default_language[lang]
      ')'
    ;

default_language[res]:
      string_value[val]
    ;

user_defined[res]:
      USERDEFINED '('
      userdef_param_list[list]
      ')'
    ;

userdef_param_list[res]:
      userdef_param_list[list] ',' userdef_param[item]
    | userdef_param[item]
    ;

userdef_param[res]:
      value[val]    /* Should not have entity_ref_list-values */
    ;

value[res]:
      logical_value
    | int_value
    | float_value
    | string_value
    | bitstring_value
    | enum_value
    | rhs_occurrence_name
    | typed_param
    | '$'
    | '*'
    | '(' value_list ')'
    ;

rhs_occurrence_name[res]:
      INSTANCEID
    | VALUEID
    | CONSTINSTID
    | CONSTVALID
    ;

typed_param[res]:
      KEYWORD '(' value[val] ')'
    | USERDEFINED '(' value[val] ')'
    ;

value_list[res]:
      value_list[list] ',' value[item]
    | value[item]
    |
    ;

anchor_section[res]:
      ANCHOR ';' anchor_list ENDSEC ';' signature_section
    |
    ;

anchor_list[res]:
      anchor_list anchor ';'
    |
    ;

anchor[res]:
      anchor_name[name] '=' anchor_item[item] anchor_tag_list[tags]
    ;

anchor_name[res]:
      url_value[fragid]	/* Only fragment identifiers! */
    ;

anchor_item[res]:
      '$'
    | int_value
    | float_value
    | string_value
    | enum_value
    | bitstring_value
    | resource
    | rhs_occurrence_name
    | '(' anchor_item_list[list] ')'
    ;

resource:
      url_value
    ;

anchor_item_list[res]:
      anchor_item_list[list] ',' anchor_item[item]
    |
    ;

anchor_tag_list:
      /* Currently empty, as i don't know how to define this ... */
    ;

reference_section[res]:
      REFERENCE ';' reference_list[refs] ENDSEC ';'
      signature_section[sig]
    |
    ;

reference_list[res]:
      reference_list[list] reference[item] ';'
    |
    ;

reference[res]:
      lhs_occurrence_name[lhs] '=' resource[rhs]
    ;

lhs_occurrence_name[res]:
      INSTANCEID
    | VALUEID
    ;

signature_section[sig]:
      SIGNATURE signature_content[value] ENDSEC signature_section[valuesig] ';'
    |
    ;

signature_content[sig]:
	/* tell the scanner that is has to scan a BASE64-encoded string */
      BASE64_DATA[data]
	/* BASE64-encoded scanning off */
    ;

data_section_list[res]:
      data_section_list[list] data_section[item]
    | data_section[item]
    ;

data_section[res]:
      DATA data_param_part[par] ';' entity_instance_map[map] ENDSEC ';'
      signature_section[sig]
    ;

data_param_part[res]:
      '(' data_param_list[par] ')'
    |
    ;

data_param_list[res]:
      section_name[name] ',' '(' schema_name[schema] ')'
    | '$' ',' '(' schema_name[schema] ')'
    ;

entity_instance_map[res]:
      entity_instance_map[map] entity_instance[inst] ';'
    |
    ;

entity_instance[res]:
      simple_entity_instance[single]
/*
    | complex_entity_instance[multiple]
*/
    ;

simple_entity_instance[res]:
      INSTANCEID '=' simple_record[value]
    ;

/*
complex_entity_instance:
      INSTANCEID '=' subsuper_record
    ;
*/

simple_record[res]:
      KEYWORD '(' keyword_param_list[params] ')'
    | USERDEFINED '(' keyword_param_list[params] ')'
    ;

keyword_param_list[res]:
      keyword_param_list[list] ',' keyword_param[item]
    | keyword_param[item]
    |
    ;

keyword_param[res]:
      value[val]
    ;

/*
subsuper_record:
      '(' simple_record_list ')'
    ;

simple_record_list:
      simple_record_list simple_record
    | simple_record
    ;
*/


string_value:
      STRGLIT
    ;

string256[result]:
      string_value[value]
    ;

string1024[result]:
      string_value[value]
    ;

bitstring_value:
      BINLIT
    ;

float_value:
      FLOATLIT
    ;

int_value:
      INTLIT
    ;

url_value:
      URL
    ;

enum_value:
      ENUM
    ;

logical_value[res]:
      LOGLIT
    ;

%%

namespace STEP {
    void Parser::error (const location_type &l, const std::string &errmsg)
    {
	std::cerr << "At " << l << ": " << errmsg << std::endl;
    }
}

#if 0
static int get_string (const std::string *s, std::string &out)
{
    const char *p = s;
    size_t length = 0;
    ++p;	// The string gotten from the scanner contains the enclosing '
    while (*p) {
	if (*p == '\'') {
	    ++p; if (! *p) { break; }
	}
	++length; ++p;
    }
    std::string res(length, '\0');
    size_t ix = 0;
    ++s;
    while (*s) {
	if (*s == '\'') {
	    ++s; if (! *s) { break; }
	}
	res[ix++] = *s++;
    }
    out = res;
    return 0;
}

static int octdg (char ch)
{
    switch (ch) {
	case '0': return 0;  case '1': return 1;  case '2': return 2;
	case '3': return 3:  case '4': return 4;  case '5': return 5;
	case '6': return 6;  case '7': return 7;
	default : return -1;
    }
}

static int decdg (char ch)
{
    switch (ch) {
	case '8': return 8;  case '9': return 9;
	default : return octdg (ch);
    }
}

static int hexdg (char ch)
{
    switch (ch) {
	case 'A': case 'a': return 10;  case 'B': case 'b': return 11;
	case 'C': case 'c': return 12;  case 'D': case 'd': return 13;
	case 'E': case 'e': return 14;  case 'F': case 'f': return 15;
	default : return decdg (ch);
    }
}

static int get_bitstring (const char *repr, BitString &out)
{
    int xb, dg; char *p = nullptr;
    Bitstring res;
    char first = repr[1];
    repr += 2;	// The string gotten from the scanner contains the enclosing "
    switch (first) {
	case '0': xb = 0; break;
	case '1': xb = 1; break;
	case '2': xb = 2; break;
	case '3': xb = 3; break;
	default : errno = EINVAL; return -1;
    }
    size_t ll = 8 * sizeof (res.bits); dg = 0;
    while (*repr != '"') {
	if ((dg = hexdg (*repr++)) < 0 || ll <= 0) { break; }
	res.bits = (res.bits << 4) | (typeof(res.bits)) dg;
	res.len += 4; ll -= 4;
    }
    if (*repr != '"') { errno = ERANGE; return -1; }
    if (dg < 0) { errno = EINVAL; return -1; }
    // The following code must be checked against the STEP-specification ...
    // Now truncate the the topmost 4 - xb bits ...
    typeof(res.bits) mask = 0xF >> (4 - xb);
    mask <<= res.len - 4; res.bits &= ~mask;
    res.len -= (4 - xb) % 4;
    out = res;
    return 0;
}

static int get_intval (const char *repr, long long &out)
{
    int dg;
    bool negative = false;
    unsigned long long acc = 0, tmp;
    long long res;
    if (*repr == '+' || *repr == '-') {
	negative = (*repr == '-'); ++repr;
    }
    while ((dg = decdg (*repr++)) >= 0) {   // < 0 means: '\0' found (probably)
	tmp = acc * 10;
	if (tmp < acc) { errno = ERANGE; return -1; }
	acc = tmp + (typeof(acc)) dg;
    }
    res = (long long) acc;
    if (res < 0) { errno = ERANGE; return -1; }
    if (negative) { res = -res; }
    out = res;
    return 0;
}

static bool valid_url (std::string &url)
{
    return true;
}

static int get_urlval (const char *repr, std::string &out)
{
    std::string res(repr + 1, strlen (repr) - 2);
    if (valid_url (res)) {
	out = res;
	return 0;
    }
    errno = EINVAL;
    return -1;
}

static int get_constrefid (const char *repr, constrefid_t &out)
{
    try {
	constrefid_t res (repr);
	out = res;
	return 0;
    } catch (Exception e) {
	errno = EINVAL;
	return -1;
    }
}

static int get_refid (const char *repr, refid_t &out)
{
    try {
	refid_t res (repr);
	out = res;
	return 0;
    } catch (Exception e) {
	errno = EINVAL;
	return -1;
    }
}
#endif
