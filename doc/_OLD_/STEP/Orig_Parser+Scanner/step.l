/* step.l
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@pps-ag.com
** Copyright: (c) 2018, PPS AG
** 
**
** Simple STEP-parser, scanner part ...
**
*/
%{
#include <iostream>
#include <string>

#include "scanner.h"
#undef YY_DECL
#define YY_DECL int STEP::Scanner::yylex(STEP::Parser::semantic_type *const lval, STEP::Parser::location_type *location)

using Token = STEP::Parser::token;
using TokenType = STEP::Parser::token_type;

//#define yyterminate() return Token::END

#define YY_NO_UNISTD_H

#define YY_USER_ACTION do { location->columns(yyleng); } while (0);
#define NEXTLINE do { location->lines(); location->step(); } while (0)

static bool valid_base64 (std::string *const b64sig);

%}

%option debug
%option nodefault
%option yyclass="STEP::Scanner"
%option noyywrap
%option c++

/* Pre-defined regular expressions for the different classes of identifiers
** and values (logical, binary, integer, float, string, enumeration) ...
*/
CONSTINSTNAME	#[A-Z][0-9A-Z]*
CONSTVALNAME	@[A-Z][0-9A-Z]*
ENTITYINSTNAME	#[0-9]+
VALUEINSTNAME	@[0-9]+
ENUMVAL		\.[A-Z][0-9A-Z_]*\.
FLOATVAL	[+-]?[0-9]+\.[0-9]*(E[+-]?[0-9]+)?
INTVAL		[+-]?[0-9]+
KEYWORD		[A-Z][0-9A-Z_]*
UDKEYWORD	![A-Z][0-9A-Z_]*
STRGVAL		'([^']|'')*'
BINARYVAL	\"[0-3][0-9A-F]*\"
URLVAL		\<[^>]+\>
WS		[\a\b\f\r\t\v ]
NOWS		[^\a\b\f\r\t\v ]

/* Start-conditions (exclusive) to be used for scanning comments and SIGNATURE-
** sections ...
*/
%x COMMENT SIG SIG0
%%

%{
    /* This code is executed before the scanner runs into its main loop, i.e.
    ** before any symbol is read ...
    */
    std::string *data_collector = nullptr;
    yylval = lval;
    location->step();
%}

SIGNATURE	%{
    /* Handle a SIGNATURE-section. The parser never sees the 'SIGNATURE'- and
    ** 'ENDSEC'-keywords, but only the signature-value read and (probably) a
    ** terminating semicolon ...
    */
    /* Beginning of a SIGNATURE-section, but no ';' found yet ... */
    BEGIN(SIG0);
%}

<SIG0>{
{WS}+		;
\n		NEXTLINE;
;		%{ BEGIN(SIG); data_collector = new std::string(); %}
.		%{ BEGIN(INITIAL); return Token::INVALID; %}
}
<SIG>{
^{WS}*ENDSEC	%{
	/* The end of a SIGNATURE-section was found. This means that the
	** collected data must be checked and then returned to the parser,
	** together with the token SIGNATURE to indicate that a returned
	** data must be interpreted as a signature value ...
	*/
	BEGIN(INITIAL);
	if (!valid_base64 (data_collector)) {
	    delete data_collector; data_collector = nullptr;
	    return Token::INVALID;
	}
	//yylval->tokenstring = data_collector;
	data_collector = nullptr;
	return Token::SIGNATURE;
    %}
{WS}+		;
\n		NEXTLINE;
{NOWS}+		*data_collector += std::string(yytext, yyleng);
}

"/*"			%{
	/* Comments (these are C-alike comments) are scanned here. The parser
	** _never_ sees anything of them ...
	*/
	/* Begin a comment (NOT NESTED) */
	BEGIN(COMMENT);
    %}
<COMMENT>{
	/* The token above started a comment and set the scanner into the
	** COMMENT-state. In this state, the following pattern-rules (until
	** the terminating '}') are used (and no other ones) ...
	*/
"*/"	    BEGIN(INITIAL);	// End a comment
[^*\n]+	    ; // Do nothing but eating up comment data
"*"	    ; // Eat a single '*' within a comment
\n	    NEXTLINE;	// Eat up an end of line, but advance the line-number
}

"ISO-10303-21"		%{
	/* A STEP-file starts with an 'ISO-10303-21'-token, immediately followed
	** by a semicolon. Nonetheless are these TWO tokens, so the semicolon
	** will _not_ be returned here as part of the token.
	*/
	return Token::STEPBEGIN;
    %}

"END-ISO-10303-21"	%{
	/* A STEP-file ends with an 'END-ISO-10303-21'-token, immediately
	** followed by a semicolon. Again, these are interpreted as two tokens.
	*/
	return Token::STEPEND;
    %}

HEADER			%{
	/* Beginning of a HEADER-section */
	return Token::HEADER;
    %}

REFERENCE		%{
	/* Beginning of the REFERENCE-section */
	return Token::REFERENCE;
    %}

ANCHOR			%{
	/* Beginning of the ANCHOR-section */
	return Token::ANCHOR;
    %}

DATA			%{
	/* Beginning of a DATA-section */
	return Token::DATA;
    %}

ENDSEC			%{
	/* End of a HEADER-/REFERENCE-/ANCHOR-/DATA-section */
	return Token::ENDSEC;
    %}

FILE_DESCRIPTION	%{
	/* FILE_DESCRIPTION header-symbol */
	return Token::FILE_DESCRIPTION;
    %}

FILE_NAME		%{
	/* FILE_NAME header-symbol */
	return Token::FILE_NAME;
    %}

FILE_SCHEMA		%{
	/* FILE_SCHEMA header-symbol */
	return Token::FILE_SCHEMA;
    %}

SCHEMA_POPULATION	%{
	/* SCHEMA_POPULATION header-symbol */
	return Token::SCHEMA_POPULATION;
    %}

FILE_POPULATION		%{
	/* FILE_POPULATION header-symbol */
	return Token::FILE_POPULATION;
    %}

SECTION_LANGUAGE	%{
	/* SECTION_LANGUAGE header-symbol */
	return Token::SECTION_LANGUAGE;
    %}

SECTION_CONTEXT		%{
	/* SECTION_CONTEXT header-symbol */
	return Token::SECTION_CONTEXT;
    %}

{KEYWORD}		%{
	/* The name of an ENTITY used in an instance in a DATA-section */
	//yylval->tokenstring = new std::string (yytext, yyleng);
	return Token::KEYWORD;
    %}

{UDKEYWORD}		%{
	/* A user-defined header-element or ENTITY in an instance in a DATA-
	** section
	*/
	//yylval->tokenstring = new std::string (yytext, yyleng);
	return Token::USERDEFINED;
    %}

\.[FTU]\.		%{
	/* A logical value (one of '.U.', '.F.' or '.T.') */
	// Remove the leading and following '.', as they are no longer required
	// for distinguishing the token-type ...
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 2);
	return Token::LOGLIT;
    %}

{ENUMVAL}		%{
	/* Any other valid identifier enclosed in '.' is an enumeration value.
	*/
	// Remove the leading and following '.', as they are no longer required
	// for distinguishing the token-type ...
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 2);
	return Token::ENUM;
    %}

{CONSTINSTNAME}		%{
	/* '#<IDENT>'. An instance probably defined in an EXPRESS-
	** specification.
	*/
	// Remove the leading '#', as this is no longer required for
	// distinguishing the token-type ...
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 1);
	return Token::CONSTINSTID;
    %}

{CONSTVALNAME}		%{
	/* '@<IDENT>' (e.g. '@PI'). A value defined in an EXPRESS-
	** specification.
	*/
	// Remove the leading '@', as this is no longer required for
	// distinguishing the token-type ...
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 1);
	return Token::CONSTVALID;
    %}

{ENTITYINSTNAME}	%{
	/* '#<integer>'. "Instances" in a DATA-section are named with this type
	** of identifiers.
	*/
	// Remove the leading '#', as this is no longer required for
	// distinguishing the token-type ...
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 1);
	return Token::INSTANCEID;
    %}

{VALUEINSTNAME}		%{
	/* '@<integer>'. A value probably defined in a REFERENCE- or ANCHOR-
	** section has this type of identifier ...
	*/
	// Remove the leading '@', as this is no longer required for
	// distinguishing the token-type ...
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 1);
	return Token::VALUEID;
    %}

{INTVAL}	%{
	/* Integer values */
	//yylval->tokenstring = new std::string (yytext, yyleng);
	return Token::INTLIT;
    %}

{FLOATVAL}	%{
	/* Floating point values (123. 123.456 7.e13 4.23e-10 ...) */
	//yylval->tokenstring = new std::string (yytext, yyleng);
	return Token::FLOATLIT;
    %}

{STRGVAL}	%{
	/* Normal text-strings. Neither this scanner nor the corresponding
	** parser will currently interpret the special \X1... and \X4...-
	** sequences ...
	*/
	// Remove the leading and following ' from the string value (not
	// required).
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 2);
	return Token::STRGLIT;
    %}

{BINARYVAL}	%{
	/* E.g. "23A4", "0", "11BC", "37C4A5623" ... */
	// Remove the leading and following " from the binary value (not
	// required).
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 2);
	return Token::BINLIT;
    %}

{URLVAL}	%{
	/* URLs (complete or anchor-names), enclosed in '<' and '>' ... */
	// Remove the leading < and following > from the url value (not
	// required).
	//yylval->tokenstring = new std::string (yytext + 1, yyleng - 2);
	return Token::URL;
    %}

    /* Rules like this one seem to not invoke YY_USER_ACTION, so this must
    ** either be done manually, or rules like this one being avoided ...
"$" |
"=" |
"*" |
";" |
"," |
"/" |
"(" |
")"
    */

[$=*;,/()]  %{
	/* Delimiting characters and the two tokens for UNDEFINED ($) and
	** UNSPECIFIED (*) values.
	*/
	//YY_USER_ACTION
	//std::cerr << "Found at " << *location << ": " << *yytext << " (" << static_cast<int>(*yytext) << ")" << std::endl;
	return static_cast<TokenType>(*yytext);
    %}

    /* Ignore rows of white-space characters ... */
{WS}+	; /* Ignore text layout characters ... */

    /* Ignore, but advance to the next line ... */
\n	NEXTLINE; /* Handle end of line ... */

.   %{
	/* Let the parser do the work of detecting these INVALID tokens ... */
	std::cerr << "At " << *location << ": Invalid character '"
		  << std::string(yytext,yyleng) << "' ("
		  << static_cast<int>(*yytext) << ")"
		  << std::endl;
	return static_cast<TokenType>(*yytext);
    %}

%%

static bool valid_base64 (std::string *const b64sig)
{
    return true;
}
