#! /bin/bash
# gen-ifc.sh
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: runkharr@googlemail.com
# Copyright: (c) 2021, Boris Jakubith <runkharr@googlemail.com>
# License: GNU General Public License, version 2
#
# Shell script which simplifies the work with 'xpp'

PROG="$(basename "$0")"
BASE="${PROG%.sh}"
PPATH="$(realpath -s "$(dirname "$0")")"
BPATH="$(dirname "$PPATH")"

XPP="$BPATH/bin/xpp"


eecho() {
    echo 1>&2 ${1+"$@"}
}

is_a_number() {
    [[ $1 =~ ^[0-9]+$ ]]
}

abort() {
    local ec=$?
    if is_a_number "$1"; then ec="$1"; shift; fi
    eecho -e "${PROG}:" ${1+"$@"}
    exit $ec
}

warn() {
    eecho "${PROG}: WARNING!" ${1+"$@"}
}

is_an_ident() {
    [[ $1 =~ ^[A-Za-z_][0-9A-Za-z_]*$ ]]
}

check_if_ident() {
    local func="$1" probe="$2"
    is_an_ident "$probe" || \
	abort 2 "$func(): '$probe' is not in the format of a regular" \
		"identifier.\n    Please consider using the 'name=...' form."
}

get_cmds() {
    local cmd cmdpath failed scmd sl ok name tl
    if [ $# -lt 1 ]; then
	abort 2 "get_cmds() invoked without arguments"
    fi
    failed=()
    for cmd in "$@"; do
	unset name tl
	if [ "${cmd#*=}" != "$cmd" ]; then
	    # Variable names in the shell can't consist of non-alphanumeric
	    # characters, so for programs like 'c++', another name must be
	    # used. Additionally, this mode allows for only one of the
	    # programs in this list being used, so the first one really
	    # found is selected. Example: the "command" 'cxx=g++|clang++|c++'
	    # leads to either 'cxx=/usr/bin/g++', or to 'cxx=/usr/bin/clang++',
	    # or to 'cxx=/usr/bin/c++'.
	    name="${cmd%%=*}"; tl="${cmd#$name=}"
	    is_an_ident "$name" || \
		abort 2 "get_cmds(): Invalid argument - ${cmd:Q}"
	    cmd="$tl"
	fi
	if [ "${cmd%|*}" != "$cmd" ]; then
	    # Using the "alternatives" mode
	    IFS_save="$IFS"; IFS='|'; sl=($cmd); IFS="$IFS_save"; ok=false
	    for scmd in "${sl[@]}"; do
		if cmdpath="$(type -p "$scmd")"; then
		    # Shell-internal commands don't have a pathname. They are
		    # valid nonetheless. In this case, the original command
		    # name is used.
		    [ -n "$cmdpath" ] || cmdpath="$scmd"  # shell-internals
		    if [ -n "$name" ]; then
			# If a name was specified, only one element of the
			# list can be returned, so a variable is generated
			# only for the the first existing of these program
			# alternatives
			eval "${name}=${cmdpath:Q}"; ok=true
			break
		    fi
		    check_if_ident get_cmd "$scmd"
		    eval "${scmd}_cmd=\"$cmdpath\""; ok=true
		fi
	    done
	    [ -z "$tl" ] || cmd="$tl"
	    $ok || failed+=("$cmd")
	elif cmdpath="$(type -p "$cmd")"; then
	    # Shell-internal commands don't have a pathname. They are
	    # valid nonetheless. In this case, the original command
	    # name is used.
	    [ -n "$cmdpath" ] || cmdpath="$cmd"           # shell-internals
	    if [ -n "$name" ]; then
		# 'name=<command>' ('name=...') form: Using 'name' as the name
		# of the variable holding the pathname of <command>.
		eval "${name}=\"$cmdpath\""
	    else
		# <command> form: Using '<command>_cmd' as the name of the
		# variable holding the pathname of command
		cmd="${cmd##*/}"
		check_if_ident get_cmd "$cmd"
		eval "${cmd}_cmd=\"$cmdpath\""
	    fi
	else
	    failed+=("$cmd")
	fi
    done
    if [ ${#failed[@]} -gt 0 ]; then
	abort 3 "Some required external programs were not found:" \
		"${failed[@]}"
    fi
}

mkpath() {
    mkdir -p ${1+"$@"} 2>/dev/null
}

gen_code() {
    local ol buildcmd bcout
    buildcmd=("$make_cmd" -C "$1")
    shift

    bcout=()

    echo -n ${1+"$@"} '.'
    while read ol; do
	bcout+=("$ol")
	#?# test $? -eq 0 || { echo "failed."; return 1; }
	echo -n '.'
    done < <( ("${buildcmd[@]}" clean; "${buildcmd[@]}") 2>&1)
    echo " done."
}

install_code() {
    local ol instcmd instout src="$1" inst="$2"
    shift 2
    instcmd=("$make_cmd" -C "$src" PLUGINDIR="$inst" install)

    instcout=()

    echo -n ${1+"$@"} '.'
    while read ol; do
	instout+=("$ol")
	#?# test $? -eq 0 || { echo "failed."; return 1; }
	echo -n '.'
    done < <("${instcmd[@]}" 2>&1)
    echo " done."
}


test -f "$XPP" -a -x "$XPP" || \
    abort 1 "'xpp' not found. Please check your installation!"

get_cmds 'cxx=g++|clang++|c++' 'make'
bbin="$BPATH/bin"; butl="$BPATH/butils"
get_cmds "xppconf=$bbin/xpp-conf|$bbin/ifc-conf|$butl/xpp-conf|$butl/ifc-conf"

if [ $# -lt 1 ]; then
    eecho "Usage: $PROG dbsql|plugins|all"; exit 0
fi

mode="$1"; shift
case "$mode" in
    dbsql|plugins|all) ;;
    *) abort 64 "Invalid mode '$mode'. Trq '$PROG' for help, please!"
esac

# Generating the directory for the plugin sources/SQL scripts (if it doesn't
# already exist.
xpcbn="$(basename "$xppconf")"
test "${xpbcn#xpp}" = "$xpbcn" && md=xpp || md=
CGENDIR="$("$xppconf" $md CGENDIR)"
mkpath "$CGENDIR" || abort "Creating '$CGENDIR' failed."

# Generating the output directories.
PLUGINDIR="$("$xppconf" $md PLUGINDIR)" #|| PLUGINDIR="$BPATH/plugins"
mkpath "$PLUGINDIR" || abort "Creating '$PLUGINDIR' failed."
SQLDIR="$("$xppconf" $md SQLDIR)" #|| SQLDIR="$BPATH/dbschema"
mkpath "$SQLDIR" || abort "Creating '$SQLDIR' failed."

# Getting the pathname of the directory containing the EXPRESS sources
EXPFILES="$("$xppconf" $md EXPFILES)" #|| EXPFILES="$BPATH/exp"
test -d "$EXPFILES" || abort 1 "Source directory '$EXPFILES' not found."

gc=0; ec=0
for expfile in "$EXPFILES"/*; do
    xpname="$(basename "$expfile")"
    test -f "$expfile" || continue
    gc=$((gc + 1))
    if [ "$mode" = dbsql -o "$mode" = all ]; then
	# Generating the SQL-script
	echo "Generating SQL code for '$xpname'"
	if ! "$XPP" db "$expfile" -o "$SQLDIR"; then
	    eecho "${PROG}: Generating DB schema for '$expfile' failed."
	    ec=$((ec + 1))
	    continue
	fi
    fi

    if [ "$mode" = plugins -o "$mode" = all ]; then
	# Generating the plugin source code
	echo "Generating plugin for '$xpname'"
	if ! "$XPP" cpp "$expfile" -o "$CGENDIR"; then
	    eecho "${PROG}: Generating plugin source for '$expfile' failed."
	    ec=$((ec + 1))
	    continue
	fi
    fi
done

test $gc -gt 0 || abort 1 "No input files."
test $ec -lt 1 || abort 1 "Aborting due to previous errors."

if [ "$mode" = plugins -o "$mode" = all ]; then
    for pluginsrc in "$CGENDIR"/*; do
	plname="$(basename "$pluginsrc")"
	test -d "$pluginsrc" || continue
	gen_code "$pluginsrc" "Generating '$plname' plugin" && \
	install_code "$pluginsrc" "$PLUGINDIR" "Installing '$plname' plugin" ||\
	{
	    ec=$((ec + 1))
	}
    done

    test $ec -lt 1 || abort 2 "Aborting due to previous errors."
fi

true
