#! /bin/bash
# install.sh
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2021, Metis AG
# License: All rights reserved
#
# Small installatiuon helper script
#
# Synopsis
#
#  install.sh <prefix>
#  install.sh local
#  install.sh  # w.o. arguments for help

PROG="$(basename "$0")"
PPATH="$(realpath -s "$(dirname "$0")")"
BASE="${PROG%.sh}"
BPATH="$(dirname "$PPATH")"

eecho() {
    echo 1>&2 ${1+"$@"}
}

abort() {
    local ec=$?
    if [[ "$1" =~ ^[1-9][0-9]*$ ]]; then ec="$1"; shift; fi
    eecho "${PROG}:" ${1+"$@"}
    exit $ec
}

warn() {
    eecho "${PROG}: WARNING!" ${1+"$@"}
}

get_cmds() {
    local cmd cmdpath failed scmd sl ok
    if [ $# -lt 1 ]; then
	abort 2 "get_cmds() invoked without arguments"
    fi
    failed=()
    for cmd in "$@"; do
	if [ "${cmd%|*}" != "$cmd" ]; then
	    IFS='|' sl=($cmd); ok=false
	    for scmd in "${sl[@]}"; do
		if cmdpath="$(type -p "$scmd")"; then
		    [ -n "$cmdpath" ] || cmdpath="$scmd"  # shell-internals
		    eval "${scmd}_cmd=\"$cmdpath\""; ok=true
		fi
	    done
	    $ok || failed+=("$cmd")
	elif cmdpath="$(type -p "$cmd")"; then
	    [ -n "$cmdpath" ] || cmdpath="$cmd"		  # shell-internals
	    eval "${cmd}_cmd=\"$cmdpath\""
	else
	    failed+=("$cmd")
	fi
    done
    if [ ${#failed[@]} -gt 0 ]; then
	abort 3 "Some required external programs were not found:" \
		"${failed[@]}"
    fi
}

usage() {
    if [ $# -gt 0 ]; then
	echo 1>&2 "${PROG}:" "$@"
	exit 64
    fi
    cat <<-EOT
Usage: $PROG <install-prefix> [xpp]
       $PROG local [xpp]
       $PROG  # w.o. arguments for this usage message

Arguments:
  <install-prefix>
    Installing the programs and data globally, below <install-prefix>.
    <install-prefix> must be an absolute pathname (e.g. '/usr', '/usr/local',
    '/opt', and so on).
  local
    Installing the programs and data locally, meaning: in the HOME directory
    of the user who invoked this script.

Invoking this script without arguments leads to this usage message being
printed.
EOT
    exit 0
}

## Creating a wrapper script at the specified position
gen_script() {
    local out="$1"
    echo -n "Generating a wrapper script ..."
    # Using 'sed' instead of cat, because the excruciating DOS/Windows EOLs
    # MUST be replaced by standard *NIX EOLs. So, even if this script contains
    # DOS/Windows EOLs, the wrapper script will never contain them, which is
    # ESSENTIAL for the wrapper script, because the '#!'-magic doesn't work if
    # the corresponding line ends in a DOS/Windows EOL.
    "$sed_cmd" -e 's/\r$//' >"$out" <<-'EOT'
	#! /bin/sh
	#
	# Small wrapper script for the programs in the directory which also
	# contains this wrapper script. This script is used instead of direct
	# symbolic links, because some of these programs may depend on the
	# path of the directory they reside in. This script guarantees that
	# these programs are executed with the correct pathnames.
	#
	# ATTENTION! This is an automatically created script file.
	
	PROG="$(basename "$0")"
	PPATH="$(dirname "$0")"
	
	WRPROG="$(readlink "$0")"
	WRNAME="$(basename "$WRPROG")"
	WRPATH="$(dirname "$WRPROG")"
	
	if [ "$PROG" = "$WRNAME" ]; then
	    echo 1>&2 "${PROG}: Invalid reference to this wrapper script."
	    exit 64
	fi
	
	RPATH="$WRPATH/$PROG"
	if [ ! -f "$RPATH" -o ! -x "$RPATH" ]; then
	    echo 1>&2 "${PROG}: Sorry, but '$PROG' doesn't reside here or" \
	              "isn't executable."
	    exit 64
	fi
	
	exec "$RPATH" ${1+"$@"}
	EOT
    chmod a+x "$out"
    echo " done."
}

## Create a symbolic link if it doesn't already exist.
## If the link target already exists and
##  a) is no symbolic link, or
##  b) is a symbolic link which doesn't point to the file to be symlink'd
## issue an error.
##
check_mklink() {
    local src="$1" dst="$2" rp
    if [ -e "$dst" ]; then
	if [ ! -h "$dst" ]; then
	    eecho "${PROG}: >$dst< already exists and is no symbolic link."
	    return 1
	fi
	rp="$(readlink "$dst")"
	if [ "$rp" != "$src" ]; then
	    eecho "${PROG}: >$dst< already exists and points to somewhere else."
	    return 1
	fi
	return 0
    else
	ln -sf "$src" "$dst"
    fi
}

mkpath() {
    mkdir -p 2>/dev/null ${1+"$@"}
}

plugins_generated() {
    local plugins ng
    test -d "$BPATH/plugins" || return 1
    shopt -q nullglob && ng=true || ng=false
    shopt -s nullglob
    plugins=("$BPATH/plugins"/*.so)
    $ng || shopt -u nullglob
    test ${#plugins[@]} -gt 0 || return 1
    return 0
}

get_cmds sed

test $# -gt 0 || usage

## Prepare the pathname variables for the installation
##
pfx="$1"; shift
if [ "$pfx" = local ]; then
    instpfx="$HOME/lib"; bindir="$HOME/bin"
    test -e "$bindir" || mkdir -p "$bindir"
    test -d "$bindir" || abort 1 "ERROR! '$bindir' is no directory."
else
    if [ "${pfx#/}" = "$pfx" ]; then
	usage "<install-prefix> must be an absolute pathname."
    fi
    if [ -e "$pfx" ]; then
	if [ ! -d "$pfx" ]; then
	    usage "<install-prfix> must be a directory."
	fi
    elif ! mkdir -p "$pfx"; then
	abort "Attempt to create >$pfx< failed."
    fi
    instpfx="$pfx/lib"; bindir="$pfx/bin"
fi

xpp=; test $# -lt 1 || { xpp="$1"; shift; }
test -z "$xpp" -o "$xpp" = xpp || usage "Invalid second argument: '$xpp'"

if [ -n "$xpp" ]; then
    cpp="$(type -p c++)" || \
    cpp="$(type -p g++)" || \
    cpp="$(type -p clang++)" || {
	warn "You need a C++ compiler for the output generated by 'xpp'."
    }
    instdir="$instpfx/ifc-express"
    instbin="$instdir/bin"
    insttmpl="$instdir/Templates/static"
    instconf="$instdir/conf"
    instexp="$instdir/IfcExp"
    echo "Creating install tree"
    mkpath "$instdir" || abort "Creating '$instdir' failed."
    mkpath "$instbin" || abort "Creating '$instbin' failed."
    mkpath "$insttmpl" || abort "Creating '$insttmpl' failed."
    mkpath "$instconf" || abort "Creating '$instconf' failed."
    mkpath "$instexp" || abort "Creating '$instexp' failed."
    XPPDIR="$BPATH/EXPRESS"; UTILDIR="$BPATH/Utils"; CONFDIR="$BPATH/conf"
    echo "Installing 'xpp' and 'xpp-conf'"
    install -s -m 0755 "$XPPDIR/xpp" "$instbin"
    install -s -m 0755 "$UTILDIR/ifc-conf" "$instbin/xpp-conf"
    echo "Installing the static part of the plugin sources"
    install -m 0644 "$XPPDIR/Templates/static"/*.{cc,h} "$insttmpl"
    echo "Installing the default configuration file for 'xpp'"
    install -m 0644 "$CONFDIR/express.conf" "$instconf"
    
    # Generating the wrapper script
    gen_script "$instbin/run.sh"

    # Creating symbolic links to the wrapper script for the
    # programs in '$bindir'.
    echo "Linking 'xpp' and 'xpp-conf' to '$bindir'"
    check_mklink "$instbin/run.sh" "$bindir/xpp"
    check_mklink "$instbin/run.sh" "$bindir/xpp-conf"

    # Installing the 'xpp' helper script in '$instbin' and creating a
    # symbolic link to '$instbin/run.sh' for it in '$bindir'
    echo "Installing 'gen-ifc'"
    butils="$BPATH/butils"
    $sed_cmd -e 's/\r$//' < "$butils/gen-ifc.sh" > "$instbin/gen-ifc"
    chmod a+x "$instbin/gen-ifc"
    echo "Linking 'gen-ifc' to '$bindir'"
    check_mklink "$instbin/run.sh" "$bindir/gen-ifc"

    echo "Creating CGENDIR directory"
    cgendir="$("$bindir/xpp-conf" CGENDIR)"
    mkpath "$cgendir" || abort "Creating '$cgendir' failed."

    echo "Installing IFC-EXPRESS files"
    for xpf in "$BPATH/IfcExp"/*.exp; do
	install "$xpf" "$instexp"
    done
else
    instdir="$instpfx/ifc-sql-converter"
    instbin="$instdir/bin"
    instplin="$instdir/lib/ifc-plugins"
    instconf="$instdir/conf"
    echo "Creating install tree"
    mkpath "$instdir" || abort "Creating '$instdir' failed."
    mkpath "$instbin" || abort "Creating '$instbin' failed."
    mkpath "$instplin" || abort "Creating '$instplin' failed."
    mkpath "$instconf" || abort "Creating '$instconf' failed."
    STPDIR="$BPATH/STEP"; UTILDIR="$BPATH/Utils"; CONFDIR="$BPATH/conf"
    echo "Installing 'stp' and 'stp-conf'"
    install -s -m 0755 "$STPDIR/stp" "$instbin"
    install -s -m 0755 "$UTILDIR/ifc-conf" "$instbin/stp-conf"
    echo "Installing the default configuration file for 'stp'"
    install -m 0644 "$CONFDIR/step.conf" "$instconf"

    # Generating the wrapper script
    gen_script "$instbin/run.sh"

    # Creating symbolic links to the wrapper script for the
    # programs in '$bindir'.
    echo "Linking 'stp' and 'stp-conf' to '$bindir'"
    check_mklink "$instbin/run.sh" "$bindir/stp"
    check_mklink "$instbin/run.sh" "$bindir/stp-conf"

    if ! plugins_generated; then
	echo "Creating the IFC plugins"
	bash $BPATH/butils/gen-ifc.sh plugins
    fi

    echo "Installing the IFC plugins"
    for plin in plugins/*.so; do
	install -s -m 0755 "$plin" "$instplin"
    done
fi

echo "Installation complete."
