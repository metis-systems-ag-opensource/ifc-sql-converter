#! /bin/bash
# cmdwrap.sh
#
# $Id: cmdwrap.sh 6525 2021-12-14 13:54:30Z bj@rhiplox $
#
# Author: Boris Jakubith
# E-Mail: runkharr@googlemail.com
# Copyright: (c) 2021, Boris Jakubith <runkharr@googlemail.com>
# License: GNU General Public License, version 2
#
# This small wrapper writes a given message to stdout, invokes a given command,
# and (depending on the success or failure of the command) appends either
# ' done.' or ' failed.' to this message. The error output of the command is
# caught in a temporary file, and additionally sent if requested.

PROG="$(basename "$0")"
BASE="${PROG%.sh}"
PPATH="$(realpath -s "$(dirname "$0")")"

TMPDIR="${TEMP:-${TMP:-/tmp}}"

if [ $# -lt 1 -o "x$1" = x-h ]; then
    cat 1>&2 <<-EOT
	Usage: $PROG [-e] [-q] 'message' command args...
	       $PROG [-h]
	EOT
    exit 0
fi

issue_caught_errors=false
no_issue_on_success=false
while [ $# -gt 0 ]; do
    case "$1" in
	-e) issue_caught_errors=true; shift ;;
	-q) no_issue_on_success=true; shift ;;
	-eq|-qe)
	    issue_caught_errors=true
	    no_issue_on_success=true
	    shift ;;
	-*) echo 1>&2 "${PROG}: Invalid option '$1'."; exit 64 ;;
	*)  break ;;
    esac
done

if [ $# -lt 2 ]; then
    echo 1>&2 "${PROG}: Missing argument(s)."
fi

msg="$1"; shift
cmd="$1"; shift

#echo 1>&2 "##A0: |$msg| |$cmd|"; exit 1

TMPFILE="$(mktemp -p "$TMPDIR" "$BASE-errs.XXXXXXXXXX")"

#echo 1>&2 "##A1: |$TMPFILE|"; exit 1

echo -n "$msg"
"$cmd" 2>"$TMPFILE" ${1+"$@"}
ec=$?

if [ $ec -eq 0 ]; then
    $no_issue_on_success || echo ' done.'
else
    echo ' failed.'
    if $issue_caught_errors; then
	while read line; do
	    echo 1>&2 "  $line"
	done < "$TMPFILE"
    fi
fi

rm -f "$TMPFILE"

exit $ec
