#! /bin/bash
# butils/build.sh
#
# $Id: 71d1c99c8783db9724fffd6b05b8a11748c88659 $
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2021, Metis AG
# License: MIT License
#
# Small wrapper program which shows or hides the command(s) used for building
# an object file or a (static) library

PROG="${0##*/}"

function eecho() {
    echo 1>&2 ${1+"$@"}
}

function usage() {
    if [ $# -gt 0 ]; then
	eecho "${PROG}:" "$@"
	exit 64
    fi
    echo "Usage: $PROG [-t target-name] [-v] build-command build-args"
    exit 0
}

check_cmds() {
    local cmd missing path
    missing=()
    for cmd in "$@"; do
	if path=$(type -p "$cmd"); then
	    eval "$(basename "$cmd" | tr ' ' '_')_cmd=\"${path:-$cmd}\""
	else
	    missing+=("$cmd")
	fi
    done
    if [ ${#missing} -gt 0 ]; then
	eecho "${PROG}: Missing required commands (" "${missing[@]}" ")"
	exit 69
    fi
}

basename_cmd=
tail_cmd=
test_cmd=
check_cmds basename tail test

"$test_cmd" $# -gt 0 || usage

target=
verbose=false
while [ $# -gt 0 ]; do
    case "$1" in
	-v|--verbose)
	    verbose=true; shift ;;
	-t*|--target=?*|--target)
	    if [ "${1#-t}" != "$1" ]; then
		if [ -n "${1#-t}" ]; then
		    target="${1#-t}"
		else
		    test $# -gt 0 || usage "Missing argument for option '-t'"
		    shift; target="$1"
		fi
	    elif [ "${1#--target=}" != "$1" ]; then
		target="${1#--target=}"
	    else
		test $# -gt 0 || usage "Missing argument for option '--target'"
		shift; target="$1"
	    fi
	    shift ;;
	-*) usage "Invalid option '$1'." ;;
	*)  break ;;
    esac
done

"$test_cmd" $# -gt 0 || \
    usage "Missing argument(s). Try '$PROG' (without arguments)" \
	  "for help, please!"

## Analyse the command
ARGS=()
OUTFILE=
ARMODE=
ARCMD=

CMD="$1"; shift
CMDN="$("$basename_cmd" "$CMD")"

case "$CMDN" in
    cc|c++|gcc|g++|clang|clang++)
	## Find the combination '-o <outfile>' or '-o<outfile>' in the
	## remaining arguments ...
	while [ $# -gt 0 ]; do
	    case "$1" in
		-o*)
		    if [ "$1" = -o ]; then
			ARGS+=("$1"); shift; OUTFILE="$1"
		    else
			OUTFILE="${1#-o}"
		    fi
		    ARGS+=("$1"); shift
		    ;;
		*)  ARGS+=("$1"); shift ;;
	    esac
	done
	;;
    ar)
	if [ "$1" = -X32_64 ]; then ARGS+=("$1"); shift; fi
	case "$1" in
	    -*d*|*d*) ARCMD="1"; ARMODE="Modifying" ;;
	    -*m*|*m*) ARCMD="1"; ARMODE="Modifying" ;;
	    -*p*|*p*) ARCMD="1"; ARMODE="Listing members in" ;;
	    -*q*|*q*) ARCMD="1"; ARMODE="Creating" ;;
	    -*r*|*r*) ARCMD="1"; ARMODE="Creating" ;;
	    -*s*|*s*) ARCMD="1"; ARMODE="Modifying" ;;
	    -*t*|*t*) ARCMD="1"; ARMODE="Listing content of" ;;
	    -*x*|*x*) ARCMD="1"; ARMODE="Extracting members from" ;;
	    *) ARMODE="Unknown operation on" ;;
	esac
	ARGS+=("$1"); shift
	while [ $# -gt 0 ]; do
	    case "$1" in
		--plugin=*|--target=*|--output=*|--record-libdeps=*)
		    ARGS+=("$1"); shift
		    ;;
		--plugin|--target|--output|--record-libdeps)
		    ARGS+=("$1"); shift
		    if [ $# -gt 0 ]; then ARGS+=("$1"); shift; fi
		    ;;
		-*|*)  break ;;
	    esac
	done

	case "$ARCMD" in
	    -*[abi]*|*[abi]*) ARGS+=("$1"); shift ;;
	esac
	case "$ARCMD" in
	    -*N*|*N*) ARGS+=("$1"); shifft ;;
	esac
	if [ $# -gt 0 ]; then OUTFILE="$1"; fi
	while [ $# -gt 0 ]; do ARGS+=("$1"); shift; done
	;;
    ranlib)
	while [ $# -gt 0 ]; do ARGS+=("$1"); shift; done
	;;
    *)
	test -n "$target" || usage "Unsupported BUILD command: $CMD"
	while [ $# -gt 0 ]; do ARGS+=("$1"); shift; done
	OUTFILE="$target"
	;;
esac

if $verbose; then
    echo "$CMD" "${ARGS[@]}"
else
    case "$CMDN" in
	ar) 
	    if [ "$ARMODE" = "Creating" ] && [ -e "$OUTFILE" ]; then
		ARMODE="Modifying"
	    fi
	    echo "$ARMODE $OUTFILE"
	    if [ "$ARMODE" = Creating ]; then
		"$CMD" "${ARGS[@]}" 2>&1 | "$tail_cmd" --lines=+2; exit
	    fi
	    exec "$CMD" "${ARGS[@]}"
	    ;;
	ranlib)
	    # Do not issue any message when using 'ranlib'
	    exec "$CMD" "${ARGS[@]}"
	    ;;
	*)
	    echo "Creating $OUTFILE"
	    exec "$CMD" "${ARGS[@]}"
	    ;;
    esac
fi

exec "$CMD" "${ARGS[@]}"
