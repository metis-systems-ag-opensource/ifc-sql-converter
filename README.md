Dies ist das Projekt IFC-Server.

Ziel ist ein WEB- (oder SCGI-)Server, der eine REST-Schnittstelle implementiert,
mit der IFC2X3- und IFC4-Dateien hochgeladen und gespeichert werden bzw. auch
wieder heruntergeladen werden können, und zwar im STEP-Dateiformat. Als
Speichermedium wird eine relationale Datenbank (MySQL/MariaDB) verwendet.

Des weiteren soll es möglich sein, einzelne Komponenten eines gespeicherten
IFC-Modells herunterzuladen, und später auch zu ändern bzw. wieder
abzuspeichern.

Das gesamte Projekt ist ein Mix aus verschiedenen Teil-Projekten, die aus
verschiedenen OSS-Lösungen entnommen werden/wurden, um sie in diesem neuen
Projekt zu vereinen. Folgende Teil-Projekte können bisher identifiziert
werden:

    1. IFC-Parser. Dieser muß die IFC-Spezifikation entgegennehmen, validieren
       und in der relationalen Datenbank unterbringen. Hierbei müssen sowohl
       IFC2X3 und IFC4 (und evtl. noch weitere –neuere– IFC-Versionen)
       unterstützt werden.

    2. EXPRESS-Parser. Dieser muss eine (Teilmenge) einer EXPRSS-Spezifikation
       (IFC-konform) verstehen und daraus Code produzieren, der – sobald
       übersetzt – dynamisch in den STEP-Parser eingebunden werden kann und
       dann für eine korrekte Weiterverarbeitung der IFC-Daten (in diesem Fall
       die Speicherung in einer Datenbank) sorgt.

    3. WEB-Service (oder SCGI-Service). Dieser Anteil steuert die Kommunikation
       zwischen anfragendem Client und dem Rest des Systems. (Dieser Teil
       wird evtl. ausgelagert, da der STEP-Parser als eigenständiges Programm
       vorliegt und somit auch über einen – z.B. in PHP implementierten –
       WEB-Service verwendet werden könnte. Zudem werden Probleme, die mit der
       Speicherverwaltung des STEP-Parsers zusammenhängen, umgangen.)

    4. REST-Spezifikation für den Zugriff auf IFC-Modelle (Anlegen, Lesen,
       Schreiben, Löschen). (Dieser Teil wird evtl. ausgelagert.)

    5. Eine auf der REST-Spezifikation aufbauende Schnittstelle, die den IFC-
       Parser und andere Komponenten nutzt. (Dieser Teil wird evtl.
       ausgelagert.)

Zur Zeit sind nur der EXPRESS-Parser (`xpp`) und der STEP-Parser (`stp`)
lauffähig:
- Ersterer generiert aus einer EXPRESS-Spezifikation einer IFC-Version
  entweder ein SQL-Script (1), oder aber C++-Code (2), aus dem ein Plugin
  für den STEP-Parser genertiert werden kann.
```
(1)  xpp db [-o output-file] (IFC-)EXPRESS-Spezifikation
(2)  xpp cpp [-o output-folder] (IFC-)EXPRESS-Spezifikation
```
- Letzterer erwartet den Code der generierten Plugins an einer - durch
  Konfiguration bestimmten Stelle. Er lädt eine IFC-Datei und stellt mit
  Hilfe der im IFC-Header enthaltenen Informationen fest, welches Plugin
  benötigt wird. Nach dem Abschluss des Ladevoranges wählt `stp` das
  passende Plugin aus und verwendet es, um die gesammelten Daten in eine
  (MySQL/MariaDB)-Datenbank zu übertragen. Da IFC-Dateien recht groß werden
  können, können beide Vorgänge, das Einlesen der IFC-Daten und das schreiben
  dieser Daten in die Datenbank recht lange dauern. Außerdem wird - je nach
  Größe der verwendeten IFC-Datei viel RAM-Speicherplatz benötigt (für eine
  IFC-Datei von etwa 370 MiB ca. 4 bis 8 GiB RAM).

Beide Programme (`xpp` und `stp`) verwenden eine Konfigurationsdatei, die
an bestimmten Stellen und in bestimmter Reihenfolge gesucht wird. Wo gesucht
wird, lässt sich durch Ansicht des Moduls `Common/confsp` (insbeseondere von
`Common/confsp.cc`) ermitteln. Sowohl dieses Modul, als auch die
Konfigurationsdateien ermöglichen die Verwendung bestimmter, außerhalb
definierter Variablen zu. Dazu dienen die Muster `%<name>` (bzw. `%{<name>}`),
wobei *`<name>`* für eine Folge von Buchstaben, Unterstrichen und Ziffern
steht, die den Namen einer dieser außerhalb definierten Variablen beschreiben.
Die Substitution dieser Muster in der Konfigurationsdatei muss ausdrücklich
aktiviert werden, und zwar durch eine Zeile in der Form
```
#substitute on
```
(oder auch `#s:on`), und kann durch Verwendung von
```
#substitute off
```
(alternativ: `#s:off`) abgeschaltet werden. Konfigurationselemente werden
durch eine einfache Zeile in der Form
```
<Elementname> = <Wert>
```
definiert. Für das jeweilige Programm nicht relevante Definitionen werden
ignoriert.

Ich habe mich bei der Entwicklung dieser Programme darum bemüht, die
Abhängigkeiten von externen Programmen und Bibliotheken so ghering wie möglich
zu halten. Außerdem verwende ich kein ausgefeiltes Build-System: `make`,
`ar`, `flex`, `bison`, ein C-Compiler, `Perl` und die `Bash`, sowie die
Standard-Befehle zum Kopieren, Löschen, Verschieben, usw. reichen aus. Auch
die Verwendung von `Doxygen` zur Generierung der Dokumentation muss nicht
verwendet werden.

HINWEIS!
Auch wenn `xpp` und `stp` funktionieren, sind sie noch lange nicht ausgereift.
So kann der `xpp` z.Zt. nur eine Teilmenge der Datenbeschreibungssprache
EXPRESS parsieren, und `stp` kann auch nur STEP soweit erkennen und einlesen,
wie es die bisherigen IFC-Spezifikationen verwenden. Außerdem dauert das
Übertragen der Daten in die Datenbank z.Zt. noch viel zu lange.

Desweiteren sieht der Code - insbesendere der (Bison-)Parser und (Flex-)Scanner
sowohl für `xpp` als auch für `stp` noch ziemlich krude aus, da ich erst
während der Entwicklung gelernt habe, mit C++ richtig umzugehen. Auch machen
sich Scanner und Parser z.Zt. noch nicht die Möglichkeit zu Nutze, direkt
C++-Objekte zu generieren, so dass aufgrund der vielen verwendeten Zeiger und
des nach klassischer C-Manier alloziierten Speicherplatzes der Code sehr
empfindlich ist.

Nichtsdestotrotz können beide Programme bereits verwendet werden.
