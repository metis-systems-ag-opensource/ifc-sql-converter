/* ifcmeta.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Generate the IFC-specific metadata from the data got from the parser.
**
*/

//##DEBUG
//#include <iostream>
//using std::cerr, std::endl, std::flush;
//##

#include "ifcmeta.h"

namespace STEP {

using IFC::IFCModel;

IFCModel gen_ifcmeta (const headersection_t *headers)
{
    IFCModel res { { }, 0, 0, "", "", "", "", "", { }, { }, { } };
    filedescription_t *fdesc = headers->filedescription;
    if (fdesc->description) {
	res.filedescription.reserve (fdesc->description->size());
	for (auto &dsc: *fdesc->description) {
	    res.filedescription.push_back (dsc);
	}
    }
    res.level = fdesc->level; res.sublevel = fdesc->sublevel;
    filename_t *fname = headers->filename;
    res.filename = fname->name; res.timestamp = fname->timestamp;
    res.origsys = fname->orig_sys; res.preprocver = fname->preproc_ver;
    res.authorization = fname->authorization;
    if (fname->authorlist) {
	res.authors.reserve (fname->authorlist->size());
	for (auto &author: *fname->authorlist) {
	    res.authors.push_back (author);
	}
    }
    if (fname->organizationlist) {
	res.organisations.reserve (fname->organizationlist->size());
	for (auto &org: *fname->organizationlist) {
	    res.organisations.push_back (org);
	}
    }
    fileschema_t *schemas = headers->fileschema;
    res.schemas.reserve (schemas->schemanames.size());
    for (auto &schema: schemas->schemanames) {
	res.schemas.push_back (schema);
    }
    return res;
}

} /*namespace STEP*/
