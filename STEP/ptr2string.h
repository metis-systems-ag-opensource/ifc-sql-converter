/* ptr2string.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Interface file for 'ptr2string.cc'
** (Small conversion for 'void * => std::string' (simply writing the address
**  of the 'void *' as string) ...)
**
*/
#ifndef PTR2STRING_H
#define PTR2STRING_H

#include <string>

namespace STEP {

    std::string ptr2string (void *p);

} /*namespace STEP*/

#endif /*PTR2STRING_H*/
