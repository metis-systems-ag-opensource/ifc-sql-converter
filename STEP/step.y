/* step.y
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Simple STEP-parser, parser part ...
**
*/

%skeleton "lalr1.cc"
%require "3.0"
%debug
%defines
%define api.namespace {STEP}
%define api.parser.class {Parser}
%locations

%code requires{
    namespace STEP {
	class Driver;
	class Scanner;
    }

// The following definition is missing when %locations isn't used
#   if YY_NULLPTR
#     if defined __cplusplus && __cplusplus >= 201103L
#       define YY_NULLPTR nullptr
#     else
#       define YY_NULLPTR 0
#     endif
#   endif

}

%parse-param { Scanner &scanner }
%parse-param { Driver &driver }

%code top{
#   include <iostream>
#   include <cstdlib>
#   include <fstream>
#   include <stdexcept>

#   include "Common/sutil.h"

    /* External (completely abstract) type EntityBase */
#   include "eb.h"

    /* Required for the driver (?!?) ... */
#   include "types.h"

    /* Including all driver functions ... */
#   include "driver.h"

    /* For verifying the SIGNATURE-data */
#   include "b64v.h"

    /* Plugin-based "delete" function for instances ... */
#   include "instdestroy.h"

#   include "supported.h"

#undef yylex
#define yylex scanner.yylex
#define yyerror this->error

/* "Global" data (needed for keeping states between different action blocks).
** In reality, this is an attribute of the 'driver' object which was passed
** as argument to the parser.
*/
#define genv (driver.genv)

}

/*%define api.value.type variant*/
%define parse.assert

%start step_file

%initial-action{
    // Initialize the initial location object
    @$.begin.filename = @$.end.filename = &driver.streamname;
}

%define parse.error verbose

%union {
    /* The results from the scanner are all in the form of strings */
    std::string *tokenstring;
    std::string *string;
    stringlist_t *stringlist;
    stringset_t *stringset;

    stepfile_t *stepfile;
    headersection_t *headersection;
    filedescription_t *filedescription;
    filename_t *filename;
    fileschema_t *fileschema;
    optheader_t *optheader;
    optheaderlist_t *optheaderlist;
    schemapopulation_t *schemapopulation;
    filepopulation_t *filepopulation;
    sectionlanguage_t *sectionlanguage;
    sectioncontext_t *sectioncontext;

    value_t *value;
    logicalvalue_t *logicalvalue;
    intvalue_t *intvalue;
    floatvalue_t *floatvalue;
    stringvalue_t *stringvalue;
    bitstringvalue_t *bitstringvalue;
    enumvalue_t *enumvalue;
    constvalidvalue_t *constvalidvalue;
    constvalidvalue_t *constinstidvalue;
    validvalue_t *validvalue;
    instidvalue_t *instidvalue;
    typedparamvalue_t *typedparamvalue;
    nullvalue_t *nullvalue;
    unspecifiedvalue_t *unspecifiedvalue;
    listvalue_t *listvalue;
    urlvalue_t *urlvalue;
    Ident entityname;
    entityvalue_t *entityvalue;
    Fifo<Value *> *valuelist;

    extfileids_t *extfileids;
    extfileidlist_t *exfileidlist;

    anchorsection_t *anchorsection;
    anchor_t *anchor;
    anchorlist_t *anchorlist;

    referencesection_t *referencesection;
    reference_t *reference;
    referencelist_t *referencelist;

    impllevel_t impllevel;

    datasection_t *datasection;
    datasectionlist_t *datasectionlist;
    dataparam_t *dataparam;
    IFC::EntityBase *instance;		/*##was: instance_t *instance;*/
    Fifo<IFC::EntityBase *> *instances;	/*##was: instlist_t *instances;*/

    signature_t *signature;
    std::string *urlstrg;
}

/* Tokens which refer to true keywords ... */
%token INVALID END
%token ANCHOR DATA ENDSEC FILE_DESCRIPTION FILE_NAME FILE_POPULATION
%token FILE_SCHEMA HEADER REFERENCE SCHEMA_POPULATION SECTION_CONTEXT
%token SECTION_LANGUAGE STEPBEGIN STEPEND

/* Tokens which refer to some form of identifiers. These tokens represent
** token classes, with the identifier's name supplied as string by the
** scanner ...
*/
%token <tokenstring> CONSTINSTID CONSTVALID INSTANCEID ENUM KEYWORD
%token <tokenstring> USERDEFINED VALUEID SIGNATURE

/* Tokens which refer to real values (integers, floating point numbers,
** strings, binary literals and URLs). The enumerated values are represented
** by the corresponding "identifier" value and thus are named above ...
*/
%token <tokenstring> LOGLIT BINLIT FLOATLIT INTLIT STRGLIT URL BASE64_DATA

/* The result-types of the productions ... */

%code {
    //typedef void *scan_t;

    //int yylex (YYSTYPE *lval, YYLTYPE *loc, scan_t scanner);

namespace STEP {
    static int get_string (const std::string *s, std::string &out);
    static int get_bitstring (const std::string *repr, bitstringvalue_t &out);
    static int get_intval (const std::string *repr, long long &out);
    static int get_floatval (const std::string *repr, long double &out);
#if 0
    static int get_urlval (const std::string *repr, std::string &out);
#endif
    static void set_constraints (Driver &driver, const impllevel_t &l);
    static std::string constraint_error (const std::string &what,
					 Driver &driver);
    static int get_level (const std::string in, impllevel_t &out);
} /*namespace STEP*/

}

/*%type <stepfile>*/
%type <headersection>	    header_section
%type <filedescription>	    file_description
%type <filename>	    file_name
%type <fileschema>	    file_schema
%type <optheader>	    other_header_item schema_population file_population
%type <optheader>	    section_context section_language user_defined

%type <value>		    userdef_param value constref_value ref_value
%type <value>		    typed_param /*entity_value*/ anchor_item
%type <value>		    string_value bitstring_value float_value int_value
%type <value>		    url_value enum_value logical_value
%type <entityvalue>	    simple_record
%type <entityname>	    entity_name

%type <extfileids>	    external_file_identification
%type <anchorsection>	    anchor_section
%type <anchor>		    anchor
%type <referencesection>    reference_section
%type <reference>	    reference
%type <impllevel>	    implementation_level
%type <datasection>	    data_section
%type <dataparam>	    data_param_part
%type <instance>	    simple_entity_instance entity_instance
%type <signature>	    signature_section
%type <tokenstring>	    signature

%type <stringset>	    schema_list
%type <stringlist>	    description_list author_list organization_list
%type <stringlist>	    governed_sections section_name_list
%type <stringlist>	    context_ident_list 
%type <string>		    section_name_opt
%type <string>		    description_item name timestamp author organization
%type <string>		    preprocessor_version originating_system string1024
%type <string>		    authorization schema_name governing_schema
%type <string>		    determination_method section_name context_ident
%type <string>		    default_language anchor_name string string256

%type <optheaderlist>	    other_header_items
%type <exfileidlist>	    external_file_identification_list
%type <valuelist>	    userdef_param_list value_list anchor_item_list
%type <valuelist>	    keyword_param_list
%type <anchorlist>	    anchor_list
%type <referencelist>	    reference_list
%type <datasectionlist>	    data_section_list

%type <instances>	    entity_instance_list



%%

step_file:
      STEPBEGIN ';'
      header_section[headers]
	{ genv.headers = $headers; }
      anchor_section[anchors]
      reference_section[references]
      data_section_list[datasections]
      STEPEND ';'
	{
	    stepfile_t *res = new StepFile ($headers, $datasections);
	    if ($anchors) { res->add_anchorsection ($anchors); }
	    if ($references) { res->add_referencesection ($references); }
	    driver.stepfile = res;
	}
    ;

header_section[header]:
      HEADER ';'
      file_description[desc] ';'
      file_name[name] ';'
      file_schema[schema] ';'
      other_header_items[other]
      ENDSEC ';'
      signature_section[sig]
	{
	    $header = new headersection_t ($desc, $name, $schema);
	    optheaderlist_t *ohl = $other;
	    if (ohl && ! ohl->empty ()) { $header->add_optheaders (ohl); }
	    signature_t *sig = $sig;
	    if (sig) { $header->add_signature (sig); }
	}
    ;

file_description[desc]:
      FILE_DESCRIPTION '('
      '(' description_list[dlist] ')' ','
      implementation_level[ilevel]
      ')'
	{
	    set_constraints (driver, $ilevel);
	    $desc = new filedescription_t ($dlist, $ilevel.level,
						   $ilevel.sublevel);
	}
    ;

description_list[res]:
      description_list[list] ',' description_item[item]
	{ $res = $list; $res->push (*$item); delete $item; }
    | description_item[item]
	{ $res = new stringlist_t (*$item); delete $item; }
    ;

description_item[res]:
      string[val]
	{ $res = $val; }
    ;

implementation_level[level]:
      string[orig]
	{
	    impllevel_t lev;
	    switch (get_level (*$orig, lev)) {
		case 0:
		    $level = lev;
		    break;
		case 1:
		    yyerror (@orig, "Invalid implementation level: " + *$orig);
		    delete $orig;
		    YYERROR;
		case 2:
		    yyerror (@orig, "Invalid implementation sub-level: " +
				    *$orig);
		    delete $orig;
		    YYERROR;
	    }
	    $level = lev; delete $orig;
	}
    ;

file_name[result]:
      FILE_NAME '('
      name ','
      timestamp[ts] ','
      '(' author_list[alist] ')' ','
      '(' organization_list[olist] ')' ','
      preprocessor_version[ppver] ','
      originating_system[osys] ','
      authorization[auth]
      ')'
	{
	    $result = new filename_t (*$name, *$ts, *$ppver, *$osys, *$auth,
				      $alist, $olist);
	    delete $name; delete $ts; delete $ppver; delete $osys; delete $auth;
	}
    ;

name:
      string256		/* maximum length: 256 */
	{ $name = $string256; }
    ;

timestamp:
      string256		/* maximum length: 256 */
	{ $timestamp = $string256; }
    ;

author_list[res]:
      author_list[list] ',' author[item]
	{ $res = $list; $res->push (*$item); delete $item; }
    | author[item]
	{ $res = new stringlist_t (*$item); delete $item; }
    ;

author:
      string256		/* maximum length: 256 */
	{ $author = $string256; }
    ;

organization_list[res]:
      organization_list[list] ',' organization[item]
	{ $res = $list; $res->push (*$item); delete $item; }
    | organization[item]
	{ $res = new stringlist_t (*$item); delete $item; }
    ;

organization[org]:
      string256		/* maximum length: 256 */
	{ $org = $string256; }
    ;

preprocessor_version[res]:
      string256		/* maximum length: 256 */
	{ $res = $string256; }
    ;

originating_system[res]:
      string256		/* maximum length: 256 */
	{ $res = $string256; }
    ;

authorization[res]:
      string256		/* maximum length: 256 */
	{ $res = $string256; }
    ;

file_schema[res]:
      FILE_SCHEMA '('
      '(' schema_list[list] ')'
      ')'
	{   /* Check/replace the schema names in the list */
	    auto newlist = new stringset_t (supported_schemas (*$list));
	    delete $list;
	    if (newlist->empty()) {
		yyerror (@list, "WARNING! No supported schemas found. This may"
				" cause problems if there is no schema"
				" parameter in the DATA section header.");
	    }
	    auto newsname = supported_schema (genv.default_schema);
	    if (newsname) {
		genv.default_schema = *newsname;
	    } else {
		genv.default_schema =
		    newlist->empty() ? "" : *(newlist->begin());
		if (genv.default_schema.empty()) {
		    yyerror (@list,
			     "WARNING! The default schema is invalid. If the"
			     " DATA section header contains no schema"
			     " parameter, this will lead to problems.");
		}
	    }
	    $res = new fileschema_t (newlist);
	}
    ;

schema_list[res]:
      schema_list[list] ',' schema_name[item]
	{   /* First: get the (upper cased) schema name. */
	    std::string schemaname = uppercase (*$item);
	    /* Second: insert the schema name into the list while checking if
	    **   it was already specified...
	    */
	    stringset_t *schemaset = $list;
	    auto [ipos, done] = schemaset->insert (schemaname);
	    if (! done) {
		/* ... because this is an error! */
		yyerror (@item, "Ambiguous schema definition: " + schemaname);
		delete $list; delete $item; YYERROR;
	    }
	    /* Otherwise, return the modified list (set) and delete the (no
	    ** longer used) '$item' ...
	    */
	    $res = schemaset; delete $item;
	}
    | schema_name[item]
	{   /* Store the default schema name ... */
	    std::string schemaname = uppercase (*$item);
	    genv.default_schema = schemaname;
	    /* ... and then create the schema list (really: a 'stringset_t').*/
	    auto schemaset = new stringset_t ();
	    schemaset->insert (schemaname);
	    $res = schemaset; delete $item;
	}
    ;

schema_name[res]:
      string1024	/* maximum length: 1024 */
	{ $res = $string1024; }
    ;

other_header_items[res]:
      other_header_items[list] other_header_item[item] ';'
	{ $res = $list; if ($item) { $res->push ($item); } }
    | %empty
	{ $res = new optheaderlist_t (); }
    ;

other_header_item[res]:
      schema_population[spop]
	{
	    if (driver.no_schemapopulation) {
		delete $spop;
		yyerror (@spop, constraint_error ("SCHEMA_POPULATION", driver));
		YYERROR;
	    }
	    $res = $spop;
	}
    | file_population[fpop]
	{
	    if (driver.no_filepopulation) {
		delete $fpop;
		yyerror (@fpop, constraint_error ("FILE_POPULATION", driver));
		YYERROR;
	    }
	    $res = $fpop;
	}
    | section_context[sctx]
	{
	    if (driver.no_sectioncontext) {
		delete $sctx;
		yyerror (@sctx, constraint_error ("SECTION_CONTEXT", driver));
		YYERROR;
	    }
	    $res = $sctx;
	}
    | section_language[slang]
	{
	    if (driver.no_sectionlanguage) {
		delete $slang;
		yyerror (@slang, constraint_error ("SECTION_LANGUAGE", driver));
		YYERROR;
	    }
	    $res = $slang;
	}
    | user_defined[uheader]
	{ $res = $uheader; }
    ;

schema_population[res]:
      SCHEMA_POPULATION '('
      '(' external_file_identification_list[extfileids] ')'
      ')'
	{ $res = new SchemaPopulation ($extfileids); }
    ;

external_file_identification_list[res]:
      external_file_identification_list[list] ','
      external_file_identification[item]
	{ $res = $list; $res->push ($item); }
    | external_file_identification[item]
	{ $res = new extfileidlist_t ($item); }
    ;

external_file_identification[res]:
      '(' string[id1] ',' string[id2] ',' string[id3] ')'
	{ $res = new ExtFileIds (*$id1, $id2, $id3); delete $id1; }
    | '(' string[id1] ',' string[id2] ')'
	{ $res = new ExtFileIds (*$id1, $id2, nullptr); delete $id1; }
    | '(' string[id1] ')'
	{ $res = new ExtFileIds (*$id1, nullptr, nullptr); delete $id1; }
    ;

file_population[res]:
      FILE_POPULATION '('
      governing_schema[gschema] ','
      determination_method[dmethod] ','
      governed_sections[gsections]
      ')'
	{
	    $res = new FilePopulation (*$gschema, *$dmethod, $gsections);
	    delete $gschema; delete $dmethod;
	}
    ;

governing_schema[res]:
      string[val]
	{ $res = $val; }
    ;

determination_method[res]:
      string[val]
	{ $res = $val; }
    ;

governed_sections[res]:
      '(' section_name_list[list] ')'
	{ $res = $list; }
    | '$'
	{ $res = nullptr; }
    ;

section_name_list[res]:
      section_name_list[list] ',' section_name[item]
	{ $res = $list; $list->push (*$item); delete $item; }
    | section_name[item]
	{ $res = new stringlist_t (*$item); delete $item; }
    ;

section_name[res]:
      string[val]
	{ $res = $val; }
    ;

section_context[res]:
      SECTION_CONTEXT '('
      section_name_opt[name] ','
      '(' context_ident_list[list] ')'
      ')'
	{ $res = new SectionContext ($name, $list); }
    ;

section_name_opt[res]:
      section_name[name]
	{ $res = $name; }
    | '$'
	{ $res = nullptr; }
    ;

context_ident_list[res]:
      context_ident_list[list] ',' context_ident[item]
	{ $res = $list; $res->push (*$item); delete $item; }
    | context_ident[item]
	{ $res = new stringlist_t (*$item); delete $item; }
    ;

context_ident[res]:
      string[val]	/* unspecified length */
	{ $res = $val; }
    ;

section_language[res]:
      SECTION_LANGUAGE '('
      section_name_opt[name] ','
      default_language[lang]
      ')'
	{ $res = new SectionLanguage ($name, *$lang); delete $lang; }
    ;

default_language[res]:
      string[val]
	{ $res = $val; }
    ;

user_defined[res]:
      USERDEFINED[name] '('
      userdef_param_list[list]
      ')'
	{
	    Ident name = driver.get_entityid ($name);
	    delete $name;
	    $res = new UserHeader (name, $list);
	}
    ;

userdef_param_list[res]:
      userdef_param_list[list] ',' userdef_param[item]
	{ $res = $list; $res->push ($item); }
    | userdef_param[item]
	{ $res = new Fifo<Value *> ($item); }
    ;

userdef_param[res]:
      value[val]    /* Should not have entity_ref_list-values */
	{ $res = $val; }
    ;

value[res]:
      logical_value[val]
	{ $res = $val; }
    | int_value[val]
	{ $res = $val; }
    | float_value[val]
	{ $res = $val; }
    | string_value[val]
	{ $res = $val; }
    | bitstring_value[val]
	{ $res = $val; }
    | enum_value[val]
	{ $res = $val; }
    | constref_value[val]
	{
	    if (driver.no_express_constants) {
		delete $val;
		yyerror (@val, constraint_error ("EXPRESS-constants", driver));
		YYERROR;
	    }
	    $res = $val;
	}
    | ref_value[val]
	{
	    $res = $val;
	}
    | typed_param[val]
	{ $res = $val; }
    | '$'
	{ $res = new NullValue (); }
    | '*'
	{ $res = new UnspecifiedValue (); }
    | '(' value_list[val] ')'
	{ $res = new ListValue ($val); }
/*
    | entity_value[val]
	{ $res = $val; }
*/
    ;

constref_value[res]:
      CONSTINSTID[id]
	{
	    $res = new ConstInstIdValue (*$id); delete $id;
	}
    | CONSTVALID[id]
	{
	    if (driver.no_value_instances) {
		delete $id;
		yyerror (@id, constraint_error ("Value-instances", driver));
		YYERROR;
	    }
	    $res = new ConstValIdValue (*$id); delete $id;
	}
    ;

ref_value[res]:
      INSTANCEID[id]
	{
	    $res = new InstIdValue (std::stoul (*$id)); delete $id;
	}
    | VALUEID[id]
	{
	    if (driver.no_value_instances) {
		delete $id;
		yyerror (@id, constraint_error ("Value-instances", driver));
		YYERROR;
	    }
	    $res = new ValIdValue (std::stoul (*$id)); delete $id;
	}
    ;

typed_param[res]:
      entity_name[name] '(' value[val] ')'
	{
	    $res = new TypedParamValue ($name, $val);
	}
    ;

value_list[res]:
      value_list[list] ',' value[item]
	{ $res = $list; $res->push ($item); }
    | value[item]
	{ $res = new Fifo<Value *> ($item); }
    | %empty
	{ $res = new Fifo<Value *> (); }
    ;

/*
entity_value[res]:
      entity_name[name] '(' keyword_param_list[params] ')'
	{
	    $res = new EntityValue ($name, $params);
	}
    ;
*/

anchor_section[res]:
      ANCHOR ';' anchor_list[list] ENDSEC ';' signature_section[sig]
	{
	    if (driver.no_anchorsection) {
		list_delete ($list);
		if ($sig) { delete $sig; }
		yyerror (@1, constraint_error ("ANCHOR-section", driver));
		YYERROR;
	    }
	    $res = new AnchorSection ($list);
	    if ($sig) { $res->add_signature ($sig); }
	}
    | %empty
	{
	    $res = nullptr;
	}
    ;

anchor_list[res]:
      anchor_list[list] anchor[item] ';'
	{ $res = $list; $res->push ($item); }
    | %empty
	{ $res = new anchorlist_t (); }
    ;

anchor[res]:
      anchor_name[name] '=' anchor_item[item] anchor_tag_list[tags]
	{ $res = new Anchor (*$name, $item); delete $name; }
    ;

anchor_name[res]:
      URL[fragid]	/* Only fragment identifiers! */
	{ $res = $1; }
    ;

anchor_item[res]:
      '$'
	{ $res = new NullValue (); }
    | logical_value[val]
	{ $res = $val; }
    | int_value[val]
	{ $res = $val; }
    | float_value[val]
	{ $res = $val; }
    | string_value[val]
	{ $res = $val; }
    | enum_value[val]
	{ $res = $val; }
    | bitstring_value[val]
	{ $res = $val; }
    | url_value[val]
	{ $res = $val; }
    | constref_value[val]
	{ $res = $val; }
    | ref_value[val]
	{ $res = $val; }
    | '(' anchor_item_list[list] ')'
	{ $res = new ListValue ($list); }
    ;

anchor_item_list[res]:
      anchor_item_list[list] ',' anchor_item[item]
	{ $res = $list; $res->push ($item); }
    | anchor_item[item]
	{ $res = new Fifo<Value *> ($item); }
    ;

anchor_tag_list:
      /* Currently empty, as i don't know how to define this ... */
      %empty
    ;

reference_section[res]:
      REFERENCE ';' reference_list[refs] ENDSEC ';'
      signature_section[sig]
	{
	    if (driver.no_referencesection) {
		list_delete ($refs);
		if ($sig) { delete $sig; }
		yyerror (@1, constraint_error ("REFERENCE-section", driver));
		YYERROR;
	    }
	    $res = new ReferenceSection ($refs);
	    if ($sig) { $res->add_signature ($sig); }
	}
    | %empty
	{ $res = nullptr; }
    ;

reference_list[res]:
      reference_list[list] reference[item] ';'
	{ $res = $list; $res->push ($item); }
    | reference[item]
	{ $res = new referencelist_t ($item); }
    ;

reference[res]:
      INSTANCEID[lhs] '=' URL[rhs]
	{
	    $res = new Reference (std::stoul (*$lhs), *$rhs, false);
	    delete $lhs; delete $rhs;
	}
    | VALUEID[lhs] '=' URL[rhs]
	{
	    $res = new Reference (std::stoul (*$lhs), *$rhs, true);
	    delete $lhs; delete $rhs;
	}
    ;

signature_section[res]:
      signature[val] ';' signature_section[valsig]
	{
	    if (driver.no_signature) {
		delete $val; if ($valsig) { delete $valsig; }
		yyerror (@val, constraint_error ("SIGNATURE-section", driver));
		YYERROR;
	    }
	    $res = new Signature (*$val);
	    if ($valsig) { $res->add_signature ($valsig); }
	    delete $val;
	}
    | %empty
	{ $res = nullptr; }
    ;

data_section_list[res]:
      data_section_list[list] data_section[item]
	{
	    if (driver.single_data_section) {
		delete $item;
		yyerror (@item, "Not more than DATA-section allowed in"
				" implementation level " +
				il2string (driver.il));
		YYERROR;
	    }
	    $res = $list; $res->push ($item);
	}
    | data_section[item]
	{ $res = new datasectionlist_t ($item); }
    ;

data_section[res]:
      DATA data_param_part[par] ';'
	{
	    DataParam *par = $par;
	    DataSection *res = nullptr;
	    std::string schema, *name = nullptr;
	    if ($par) {
		if (driver.no_data_params) {
		    delete $par;
		    yyerror (@par, "No params allowed for DATA-section in"
				   " implementation level " +
				   il2string (driver.il) +
				   "; ignoring the param-part.");
		    schema = genv.default_schema;
		} else {
		    name = par->name;
		    schema = uppercase (par->schema);
		    delete par;
		}
	    } else {
		schema = genv.default_schema;
	    }
	    if (schema.empty()) {
		yyerror (@1, "No valid schema defined. This is a problem!");
		YYERROR;
	    }
	    res = new DataSection (name, schema);
	    // Install the plugin for the EXPRESS object model (IFC2X3/IFC4).
	    // This plugin is required for:
	    //   a) Getting the symbols (tokens) for the EXPRESS datatypes,
	    //   b) Convertinga STEP::Instance into an EXPRESS object,
	    //   c) Fixing the Instance references - allowing for a direct
	    //      (and thus fast) access access to instances by these
	    //      references,
	    //   d) Storing the IFC model in a database.
	    driver.set_schema (schema);
	    genv.current_schema = schema;
	    genv.current_ds = res;
	    genv.instset.clear();
	}
      entity_instance_list[map] ENDSEC ';'
	{
	    DataSection *res = genv.current_ds;
	    res->add_instances ($map);
	    genv.instset.clear();
	}
      signature_section[sig]
	{
	    DataSection *res = genv.current_ds;
	    if ($sig) {
		res->add_signature ($sig);
	    }
	    $res = res; genv.current_ds = nullptr;
	}
    ;

data_param_part[res]:
      '(' section_name_opt[name] ',' '(' schema_name[schema] ')' ')'
	{   // First: check if the name is unique ...
	    std::string &name = *$name;
	    auto ires = genv.data_sections.insert (name);
	    if (! ires.second) {
		// ERROR: data section name is not unique!
		yyerror (@name, "Ambiguous DATA section identifier: " + name);
		delete $name; delete $schema; YYERROR;
	    }
	    // Second: check if the referenced schema name was defined
	    //   (specified in the FILE_SCHEMA-header).
	    std::string &schema = *$schema;
	    if (genv.headers->fileschema->schemanames.count (schema) == 0) {
		// ERROR: the schema must be represented in the FILE_SCHEMA-
		// element of the header-section.
		yyerror (@schema, "Undefined schema identifier: " + schema);
		delete $name; delete $schema; YYERROR;
	    }
	    driver.set_schema (schema);
	    // After completing the checks, generate a new data section header.
	    $res = new DataParam ($name, schema);
	    delete $schema;
	}
    | %empty
	{   // Default: Generating an automatic name and use the first element
	    //   found in the FILE_SCHEMA header. ##0
	    $res = nullptr;
	}
    ;

/* Collect the instances of this data section in a list, but check if the
** instance id is already used or not.
*/
entity_instance_list[res]:
      entity_instance_list[map] entity_instance[inst] ';'
	{
	    unsigned long id = driver.get_instanceid ($inst);
	    if (! genv.instset.insert (id)) {
		yyerror (@inst, "Ambiguous instance (" +
				std::to_string (id) + ")");
		// Don't know (yet) how to handle this one. Probably, an extra
		// member function must be defined in the plugin, a function
		// which executes 'delete <instptr>' on the plugin side.
		instdestroy ($inst);
		YYERROR;
	    }
	    $res = $map; $res->push ($inst);
#if 0
	    unsigned long id = $inst->id;
	    instances_t *map = $map;
	    if (! map) {
		map = new instances_t;
	    } else if (map->count (id) > 0) {
		yyerror (@inst, "Ambiguous instance (" +
				std::to_string (id) + ")");
		delete $inst;
		YYERROR;
	    }
	    (*map)[id] = $inst;
	    $res = map;
#endif
	}
    | %empty
	{ $res = new Fifo<IFC::EntityBase *> (); }
/*##was:	{ $res = new instlist_t(); }*/
    ;

entity_instance[res]:
      simple_entity_instance[single]
	{ $res = $single; }
/*
    | complex_entity_instance[multiple]
*/
    ;

simple_entity_instance[res]:
      INSTANCEID[ref] '=' simple_record[value]
	{
	    Instance stepinst (std::stoul (*$ref), *$value);
	    delete $ref; delete $value;
	    $res = driver.gen_expinstance (stepinst);
	}
    ;

/*
complex_entity_instance:
      INSTANCEID '=' subsuper_record
    ;
*/

simple_record[res]:
      entity_name[name] '(' keyword_param_list[params] ')'
	{
	    Ident id = $name;
	    $res = new EntityValue (id, $params);
	}
    ;

entity_name[res]:
      KEYWORD[name]
	{   /*##0*/
	    $res = driver.get_entityid ($name); delete $name;
	}
    | USERDEFINED[name]
	{   /*##1*/
	    $res = driver.get_entityid ($name); delete $name;
	}
    ;

keyword_param_list[res]:
      keyword_param_list[list] ',' value[item]
	{ $res = $list; $res->push ($item); }
    | value[item]
	{ $res = new Fifo<Value *> ($item); }
    ;

/*
subsuper_record:
      '(' simple_record_list ')'
    ;

simple_record_list:
      simple_record_list simple_record
    | simple_record
    ;
*/


string[res]:
      STRGLIT
	{   /* Get a regular string value */
	    std::string tmp;
	    if (get_string ($1, tmp)) {
		yyerror (@1, "Invalid string literal: '" + *$1 + "'");
		delete $1; YYERROR;
	    }
	    $res = new std::string (tmp);
	    delete $1;
	}
    ;

string256[result]:
      string[value]
	{
	    if ($value->length() > 256) {
		yyerror (@value, "String too long (>= 256 bytes)");
		delete $value;
		YYERROR;
	    }
	    $result = $value;
	}
    ;

string1024[result]:
      string[value]
	{
	    if ($value->length() > 1024) {
		yyerror (@value, "String too long (>= 1024 bytes)");
		delete $value;
		YYERROR;
	    }
	    $result = $value;
	}
    ;

string_value[res]:
      STRGLIT
	{
	    std::string tmp;
	    if (get_string ($1, tmp)) {
		yyerror (@1, "Invalid string value literal: '" + *$1 + "'");
		delete $1; YYERROR;
	    }
	    $res = new StringValue (tmp);
	    delete $1;
	}
    ;

bitstring_value[res]:
      BINLIT
	{
	    bitstringvalue_t out;
	    if (get_bitstring ($1, out)) {
		yyerror (@1, "Invalid binary literal: \"" + *$1 + "\"");
		delete $1; YYERROR;
	    }
	    $res = new BitStringValue (out);
	    delete $1;
	}
    ;

float_value[res]:
      FLOATLIT
	{
	    long double tmp;
	    if (get_floatval ($1, tmp)) {
		yyerror (@1, "Invalid floating point literal: " + *$1);
		delete $1; YYERROR;
	    }
	    $res = new FloatValue (tmp);
	    delete $1;
	}
    ;

int_value[res]:
      INTLIT
	{
	    long long tmp;
	    if (get_intval ($1, tmp)) {
		yyerror (@1, "Invalid integer literal: " + *$1);
		delete $1; YYERROR;
	    }
	    $res = new IntValue (tmp);
	    delete $1;
	}
    ;

url_value[res]:
      URL
	{
	    $res = new UrlValue (*$1);
	    delete $1;
	}
    ;

enum_value[res]:
      ENUM
	{
	    $res = new EnumValue (*$1);
	    delete $1;
	}
    ;

logical_value[res]:
      LOGLIT
	{
	    std::string *repr = $1;
	    if (*repr == ".U.") {
		$res = new LogicalValue (l_undefined);
	    } else if (*repr == ".F.") {
		$res = new LogicalValue (l_false);
	    } else { // *repr == ".T."!
		$res = new LogicalValue (l_true);
	    }
	    delete $1;
	}
    ;

signature[res]:
      SIGNATURE[val]
	{
	    if (! verify_base64 ($val->c_str())) {
		yyerror (@val, "Invalid signature");
		YYERROR;
	    }
	    $res = $val;
	}
;

%%

namespace STEP {
    void Parser::error (const location_type &l, const std::string &errmsg)
    {
	std::cerr << "At " << l << ": " << errmsg << std::endl;
    }

    static int get_string (const std::string *s, std::string &out)
    {
	size_t length = 0;
	size_t ix = 0;
	while (ix < s->length()) {
	    if ((*s)[ix] == '\'') { ++ix; }
	    ++length; ++ix;
	}
	std::string res(length, '\0');
	size_t jx = 0;
	ix = 0;
	while (ix < s->length()) {
	    if ((*s)[ix] == '\'') { ++ix; }
	    res[jx++] = (*s)[ix++];
	}
	out = res;
	return 0;
    }

    static int octdg (char ch)
    {
	switch (ch) {
	    case '0': return 0;  case '1': return 1;  case '2': return 2;
	    case '3': return 3;  case '4': return 4;  case '5': return 5;
	    case '6': return 6;  case '7': return 7;
	    default : return -1;
	}
    }

    static int decdg (char ch)
    {
	switch (ch) {
	    case '8': return 8;  case '9': return 9;
	    default : return octdg (ch);
	}
    }

    static int hexdg (char ch)
    {
	switch (ch) {
	    case 'A': case 'a': return 10;  case 'B': case 'b': return 11;
	    case 'C': case 'c': return 12;  case 'D': case 'd': return 13;
	    case 'E': case 'e': return 14;  case 'F': case 'f': return 15;
	    default : return decdg (ch);
	}
    }

    static int get_bitstring (const std::string *repr, bitstringvalue_t &out)
    {
	int xb, dg;
	size_t rx = 0, rlen = repr->length ();
	BitString res;
	if (rlen == 0
	|| (xb = hexdg ((*repr)[rx++]), xb < 0 || xb > 3)
	|| (rlen == 1 && xb > 0)) {
	    errno = EINVAL; return -1;
	}
	if (rx < rlen) {
	    // Ignore the first xb bits ...
	    int mask = (xb > 0 ? 8 >> xb : 8);
	    do {
		if ((dg = hexdg ((*repr)[rx++])) < 0) {
		    errno = EINVAL; return -1;
		}
		while (mask > 0) {
		    res.push_back ((dg & mask) != 0); mask >>= 1;
		}
		mask = 8;
	    } while (rx < rlen);
	}
	out.value = res;
	return 0;
    }

    static int get_intval (const std::string *repr, long long &out)
    {
	int dg;
	size_t rx = 0;
	bool negative = false;
	unsigned long long acc = 0, tmp;
	long long res;
	if ((*repr)[rx] == '+' || (*repr)[rx] == '-') {
	    negative = ((*repr)[rx] == '-'); ++rx;
	}
	while ((dg = decdg ((*repr)[rx++])) >= 0) {
	// dg < 0 means: '\0' found (probably)
	    tmp = acc * 10;
	    if (tmp < acc) { errno = ERANGE; return -1; }
	    acc = tmp + (decltype(acc)) dg;
	}
	res = (long long) acc;
	if (res < 0) { errno = ERANGE; return -1; }
	if (negative) { res = -res; }
	out = res;
	return 0;
    }

    static int get_floatval (const std::string *repr, long double &out)
    {
	out = std::stold (*repr);
	return 0;
    }

#if 0
    static bool valid_url (std::string &url)
    {
	return true;
    }

    static int get_urlval (const std::string *repr, std::string &out)
    {
	std::string res(*repr);
	if (valid_url (res)) {
	    out = res;
	    return 0;
	}
	errno = EINVAL;
	return -1;
    }
#endif

    static void set_constraints (Driver &driver, const impllevel_t &l)
    {
	if (l.level == 4) {
	    switch (l.sublevel) {
		case 3: break;
		case 2:
		    driver.no_express_constants = true;
		    driver.no_value_instances = true;
		    break;
		case 1:
		    driver.no_express_constants = true;
		    driver.no_value_instances = true;
		    driver.no_referencesection = true;
		    break;
		default: break;
	    }
	} else {
	    if (l.level == 2) {
		// Restrictions additional to level 3 restrictions
		driver.single_data_section = true;
		driver.no_data_params = true;
		driver.no_filepopulation = true;
		driver.no_sectioncontext = true;
		driver.no_short_enums = true;
		driver.no_value_instances = true;
	    }
	    // Level 2 and 3 restrictions
	    driver.no_anchorsection = true;
	    driver.no_referencesection = true;
	    driver.no_schemapopulation = true;
	    driver.classical_encoding = true;
	    driver.no_signature = true;
	    driver.no_express_constants = true;
	    if (l.sublevel > 1) {
		// Sub-level 1 restriction (currently unused)
		driver.external_encoding = true;
	    }
	}
	driver.il = l;
    }

    static std::string constraint_error (const std::string &what,
					 Driver &driver)
    {
	return what + " is not allowed in implementation level " + 
	       il2string (driver.il);
    }

    static bool isws (int ch)
    {
	return ch == ' ' || ch == '\t';
    }

    static int get_level (std::string in, impllevel_t &out)
    {
	size_t ix;
	impllevel_t lev;
	int iv = stoi (in, &ix, 10);
	while (ix < in.length () && isws (in[ix])) { ++ix; }
	if (ix >= in.length () || in[ix] != ';' || iv < 2 || iv > 4) {
	    return 1; // Invalid implementation-level
	}
	lev.level = iv;
	in.erase (0, ix + 1);   // Erase the beginning of the string (incl. ';')
	iv = stoi (in, &ix, 10);
	// level 4 allows for sub-levels 1...3, levels 2 and 3 only for 1 and 2.
	if (ix < in.length () || iv < 1 || iv > (lev.level == 4 ? 3 : 2)) {
	    return 2; // Invalid implementation sub-level
	}
	lev.sublevel = iv;
	out = lev;
	return 0;
    }
} /*namespace STEP*/
