/* types.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
**
** Type definitions required by the parser for constructing the parse tree ...
**
*/
#ifndef TYPES_H
#define TYPES_H

#include <cstdint>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <stdexcept>

#include "eb.h"
#include "typedefs.h"
#include "valtypes.h"

#include "Common/fifo.h"

/* REAL starting point ... */

namespace STEP {

//### Temporary place for this type. Once i solved the problem with the parser,
//### this declaration(s) here will be removed.
//struct EntityBase;
typedef IFC::EntityBase instance_t;
typedef Fifo<IFC::EntityBase *> instlist_t;
//###

class Ptree {
};

// Some required list types (plus corresponding iterator-types) ...
using stringset_t = std::set<std::string>;
using stringlist_t = Fifo<std::string>;
using stringiter_t = Fifo<std::string>::Iterator;
typedef Fifo<optheader_t *> optheaderlist_t;
typedef Fifo<optheader_t *>::Iterator optheaderiter_t;
typedef Fifo<DataSection *> datasectionlist_t;
typedef Fifo<DataSection *>::Iterator datasectioniter_t;
typedef Fifo<extfileids_t *> extfileidlist_t;
typedef Fifo<extfileids_t *>::Iterator extfileiditer_t;
typedef Fifo<Anchor *> anchorlist_t;
typedef Fifo<Anchor *>::Iterator anchoriter_t;
typedef Fifo<Reference *> referencelist_t;
typedef Fifo<Reference *>::Iterator referenceiter_t;
using instances_t = std::vector<instance_t *>;
//##was: typedef std::map<int, instance_t *> instances_t;
//typedef instances_t::iterator inst_iterator_t;
//typedef Fifo<instance_t *> instlist_t;
//##was: typedef std::map<int, instance_t *>::iterator inst_iterator_t;
//##NEW: a map between instance-ids
typedef Fifo<EntityValue *> entitylist_t;
typedef entitylist_t::Iterator entityiter_t;

// A small helper function for deleting a complete Fifo-list of pointers,
// including the objects referenced by the list entries and the list-object
// itself.
template<typename T> void list_delete (Fifo<T *> * &l)
{
    if (l) {
	while (! l->empty ()) {
	    T *x = l->shift ();
	    delete x;
	}
	delete l; l = nullptr;
    }
}

typedef struct ImplLevel impllevel_t;
typedef struct DataParam dataparam_t;

struct StepFile : Ptree {
    headersection_t *headersection;
    anchorsection_t *anchorsection;
    referencesection_t *referencesection;
    datasectionlist_t *datasectionlist;

    StepFile (headersection_t *headers, datasectionlist_t *datasections);

    ~StepFile ();

    void add_anchorsection (anchorsection_t *anchors);

    void add_referencesection (referencesection_t *references);
private:
    // Explicitely forbid copying! We always want to use the pointers
    StepFile (StepFile &x);
    StepFile & operator= (const StepFile &x);
    // Also, an uninitialized creation is not what we want!
    StepFile ();
} /*StepFile*/;

struct HeaderSection : Ptree {
    filedescription_t *filedescription;
    filename_t *filename;
    fileschema_t *fileschema;
    optheaderlist_t *optheaderlist;
    signature_t *signature;

    HeaderSection (filedescription_t *fdesc, filename_t *fname,
		   fileschema_t *fschema);

    ~HeaderSection ();

    void add_optheaders (optheaderlist_t * &optheaders);

    void add_signature (signature_t *sig);
private:
    HeaderSection (HeaderSection &x);
    HeaderSection & operator= (const HeaderSection &x);
    HeaderSection ();
} /*HeaderSection*/;

struct FileDescription : Ptree {
    stringlist_t *description;
    int level, sublevel;

    FileDescription (stringlist_t * &desc, int lev, int sublev);

    ~FileDescription ();
private:
    FileDescription (FileDescription &x);
    FileDescription & operator= (const FileDescription &x);
    FileDescription ();
} /*FileDescription*/;


struct FileName : Ptree {
    std::string name, timestamp, preproc_ver, orig_sys, authorization;
    stringlist_t *authorlist, *organizationlist;

    FileName (std::string fname, std::string fstamp, std::string ppver,
	      std::string osys, std::string auth,
	      stringlist_t * &authors, stringlist_t * &organizations);

    ~FileName ();
private:
    FileName (FileName &x);
    FileName & operator= (const FileName &x);
    FileName ();
} /*FileName*/;

struct FileSchema : Ptree {
    stringset_t schemanames;

    FileSchema (stringset_t * &schemas);

    ~FileSchema ();

    bool noschemas();
private:
    FileSchema (FileSchema &x);
    FileSchema & operator= (const FileSchema &x);
    FileSchema ();
} /*FileSchema*/;

struct OptHeader : Ptree {
    enum Type {
	schemapopulationtype, filepopulationtype, sectionlanguagetype,
	sectioncontexttype, userdefinedtype
    };
    virtual ~OptHeader () = default;
    virtual Type type () = 0;
} /*OptHeader*/;

struct SchemaPopulation : public OptHeader {
    extfileidlist_t *extfileidlist;

    SchemaPopulation (extfileidlist_t *idlist);

    ~SchemaPopulation ();
    Type type () { return schemapopulationtype; }
private:
    SchemaPopulation (SchemaPopulation &x);
    SchemaPopulation & operator= (const SchemaPopulation &x);
    SchemaPopulation ();
} /*SchemaPopulation*/;

struct FilePopulation : public OptHeader {
    std::string schemaname, determinationmethod;
    stringlist_t *governedsections;

    FilePopulation (std::string name, std::string dmeth, stringlist_t *gsects);

    ~FilePopulation ();
    Type type () { return filepopulationtype; }
private:
    FilePopulation (FilePopulation &x);
    FilePopulation & operator= (const FilePopulation &x);
    FilePopulation ();
} /*FilePopulation*/;

struct SectionLanguage : public OptHeader {
    std::string *section, defaultlanguage;

    SectionLanguage (std::string *sect, std::string &lang);

    ~SectionLanguage ();
    Type type () { return sectionlanguagetype; }
private:
    SectionLanguage (SectionLanguage &x);
    SectionLanguage & operator= (const SectionLanguage &x);
    SectionLanguage ();
} /*SectionLanguage*/;

struct SectionContext : OptHeader {
    std::string *section; stringlist_t *contextidentifiers;

    SectionContext (std::string *sect, stringlist_t *idlist);

    ~SectionContext ();
    Type type () { return sectioncontexttype; }
private:
    SectionContext (SectionContext &x);
    SectionContext & operator= (const SectionContext &x);
    SectionContext ();
} /*SectionContext*/;

struct UserHeader : OptHeader {
    Ident name;
    valuelist_t attributes;

    UserHeader (Ident id, Fifo<Value *> * &attrs);

    UserHeader (UserHeader &&x);

    UserHeader & operator= (UserHeader &&x);

    ~UserHeader ();
    Type type () { return userdefinedtype; }
private:
    UserHeader (UserHeader &x);
    UserHeader & operator= (const UserHeader &x);
    UserHeader ();
} /*UserHeader*/;

struct ExtFileIds : Ptree {
    std::string refstruct, *lastvisited, *digest;

    ExtFileIds (const std::string &rs, std::string *lv, std::string *dg);

    ~ExtFileIds ();
private:
    // Forbid ... yet you should know what.
    ExtFileIds (ExtFileIds &x);
    ExtFileIds & operator= (const ExtFileIds &x);
    ExtFileIds ();
} /*ExtFileIds*/;

struct AnchorSection : Ptree {
    anchorlist_t *anchorlist;

    signature_t *signature;

    AnchorSection (anchorlist_t *alist);

    AnchorSection ();

    ~AnchorSection ();

    void add_signature (signature_t *sig);
private:
    AnchorSection (AnchorSection &x);
    AnchorSection & operator= (const AnchorSection &x);
} /*AnchorSection*/;

struct Anchor : Ptree {
    std::string name; value_t *attribute;

    Anchor (const std::string &aname, value_t *attr);

    ~Anchor ();

    void add_signature (signature_t *sig);
private:
    Anchor (Anchor &x);
    Anchor & operator= (const Anchor &x);
    Anchor ();	// The ANCHOR_SECTION may be empty, an ANCHOR *NOT*!
} /*Anchor*/;


struct ReferenceSection : Ptree {
    referencelist_t *referencelist;
    signature_t *signature;

    ReferenceSection (referencelist_t *refs);
    ReferenceSection ();

    ~ReferenceSection ();

    void add_signature (signature_t *sig);
private:
    ReferenceSection (ReferenceSection &x);
    ReferenceSection & operator= (const ReferenceSection &x);
} /*ReferenceSection*/;

struct Reference : Ptree {
    std::string url; refid_t id; bool valueref;

    Reference (refid_t refid, const std::string &refurl,
	       bool valref = false);

    Reference (Reference &x);

    ~Reference ();
private:
    Reference ();
} /*Reference*/;

// Only used by the parser, so it doesn't need any serialization ...
struct ImplLevel {
    short int level, sublevel;
} /*ImplLevel*/;

std::string il2string (impllevel_t &l);

struct DataParam {
    std::string *name, schema;

    DataParam (std::string *sname, std::string &sschema);
    ~DataParam ();

private:
    DataParam (DataParam &x);
    DataParam & operator= (const DataParam &x);
    DataParam ();
} /*DataParam*/;

struct DataSection : Ptree {
    instances_t instances;
    std::string schema, *name;
    signature_t *signature;

    // DATA-section with name and schema-reference
    DataSection (std::string *name, std::string &sschema);

//    // DATA-section without extra parameters!
//    DataSection (instances_t *inst);

    ~DataSection ();

    void add_instances (instlist_t *inst);
    void add_signature (signature_t *sig);
private:
    DataSection (DataSection &x);
    DataSection & operator= (const DataSection &x);
    DataSection ();
} /*DataSection*/;

struct Signature : Ptree {
    std::string value;
    signature_t *valuesig;  /* A SIGNATURE can have a SIGNATURE itself */

    Signature (const std::string &sig);

    ~Signature ();

    void add_signature (signature_t *vsig);
private:
    // Forbid copying
    Signature (Signature &x);
    Signature & operator= (const Signature &x);
    // And an empty signatire also makes not much sense
    Signature ();
} /*Signature*/;

// An entity-tree is constructed by replacing the RefIdValue-attributes with
// EntityValue-attributes (meaning: Direct references to other entities) ...
// This must be done after the parsing, because not all instances entries are
// known before. This process modifies (probably destroys) the entries in the
// Instance-Map of a DATA-section, which means that a DATA-section must be
// re-constructed by serializing the entity-tree by serializing it in an IFC-
// alike manner ...

} /*namespace STEP*/


#endif /*TYPES_H*/
