/* driver.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** "Compiler" driver. All properties concerning the parser are hidden behind
** this module's interface.
**
*/

#include <iostream>
using std::cerr, std::endl, std::flush;

#include <cctype>
#include <fstream>
#include <cassert>
#include <utility>
#include <stdexcept>

#include "Common/timer.h"

#include "readconf.h"
#include "ifcmeta.h"
#include "instdestroy.h"

#include "driver.h"

using namespace std;

namespace STEP {

    Driver::Driver ()
      : streamname("<input stream>"), current_schema(),
	current_plugin(nullptr),  entitytab()
    {
	// Preset the schema type specifying variables
	no_anchorsection = no_referencesection = no_schemapopulation =
	no_signature = no_express_constants = single_data_section =
	no_filepopulation = no_sectionlanguage = no_sectioncontext =
	no_short_enums = classical_encoding = no_data_params =
	no_value_instances = external_encoding = false;

	genv.headers = nullptr; genv.default_schema = "";
	genv.current_schema = ""; genv.data_sections.clear();
	genv.current_ds = nullptr; genv.max_instances = 0;

	// Read the configuration. The argument is NO PATHNAME, but rather
	// the (file-)name of the configuration file. The pathname of this
	// file is deduced from a (hard-coded) list of templates which are
	// filled by data known at this point of time (including this
	// name). The result of 'readconf()' is a map (with some extras).
	conf = readconf ("step");

	// The existance of the path of the "plugin" directory was already
	// checked by 'readconf()', so 'plugin_dir' can be assigned directly.
	plugin_dir = conf["PLUGINDIR"];
    }

    Driver::~Driver()
    {
	if (scanner) { delete scanner; scanner = nullptr; }
	if (parser) { delete parser; parser = nullptr; }
    }

    bool Driver::parse (const char *const filename)
    {
	assert (filename != nullptr);
	std::ifstream in_file (filename);
	if (! in_file.good ()) {
	    //ERROR
	    exit (1);
	}
	streamname = filename;
	return parse_helper (in_file);
    }

    bool Driver::parse (istream &stream, const string &sname)
    {
	if (! stream.good() && stream.eof()) {
	    return false;
	}
	streamname = (sname.empty() ? "<input stream>" : sname);
	return parse_helper (stream);
    }

    void Driver::end_parse()
    {
	delete scanner; scanner = nullptr;
	delete parser; parser = nullptr;
    }

    bool Driver::parse_helper (istream &stream)
    {
	delete scanner;
	try {
	    scanner = new Scanner (&stream);
	} catch (bad_alloc &ba) {
	    cerr << "Failed to allocate scanner: " << ba.what() << endl <<
		    "Exiting!" << endl;
	    exit (1);
	}

	delete parser;
	try {
	    parser = new Parser ((*scanner), (*this));
	} catch (bad_alloc &ba) {
	    cerr << "Failed to allocate parser: " << ba.what() << endl <<
		    "Exiting!" << endl;
	    exit (1);
	}

	const int accept(0);
	if (parser->parse() != accept) {
	    cerr << "Parsing failed!" << endl; return false;
	}

	return true;
    }

    void Driver::process_instances (OutputMode om)
    {
	Timer dt(2);
//cerr << "##1: Generating/converting the model data...\n";
	// Generate the IFC metadata required for the plugin
	cerr << "  Extracting the IFC HEADER data ..." << endl;
	IFCModel ifcmeta = gen_ifcmeta (stepfile->headersection);

	// This routine processes only the first DATA section found, because i
	// have (yet) no mechanism defined to load more than one plugin at the
	// same time; additionally, IFC specifies only one DATA section
	// (without schema type parameter) and the only schema type definition
	// is defined in the header.
//cerr << "##2: Getting the data section..." << flush;
	datasectionlist_t *datasections = stepfile->datasectionlist;
	DataSection *ds = datasections->first();
//cerr << " ds = " << (void *) ds << endl; //##2
//cerr << "##3: Getting the plugin...\n";
	SchemaObj *plugin = current_plugin;
//cerr << " plugin = " << (void *) plugin << endl; //##3
	if (! plugin) {
	    throw runtime_error
		("Driver::process_instances(): No plugin loaded.");
	}
//cerr << "##4: Fixing/converting the instance references within the attributes...\n";
	dt.start();
	cerr << "  Fixing the instance references ..." << endl;
	plugin->FixRefs (ds->instances);
	dt.stop();
	cerr << "    (required time: " << dt.hms() << ")." << endl;

//cerr << "##5: Storing the instances in the database\n";
	cerr << "  Storing the complete IFC model ..." << endl;
	dt.start();
	plugin->store_model (conf, ifcmeta, ds->instances, om);
	dt.stop();
	cerr << "    (required time: " << dt.hms() << ")." << endl;
//cerr << "##6\n";
    }

    void Driver::set_schema (const string &schema)
    {
	const string libdir = plugin_dir;
	SchemaObj *plugin = new SchemaObj;
	if (! plugin) {
	    throw runtime_error
		("FATAL! Attempt to create a new schema object failed.");
	}
	if (plugin->load (schema, libdir) < 0) {
	    delete plugin;
	    throw runtime_error
		("FATAL! Attempt to load plugin for '" + schema + "' from '" +
		 libdir + "' failed.");
	}
	current_schema = schema;
	if (current_plugin) { delete current_plugin; }
	current_plugin = plugin;
	instdestroy_init (plugin);
    }

    Ident Driver::get_entityid (const string &name)
    {
	Ident eos = 1;
	SchemaObj *plugin = current_plugin;
	if (plugin) {
	    Ident x = plugin->name2id (name);
	    eos = (Ident) plugin->symsend();
	    if (x > 0 && x < eos) {
		return x;
	    }
	}
	if (entitytab.has (name)) { return entitytab[name] + eos - 1; }
	Ident id = entitytab[name];
	entitytab[id] = 0;
	return id + eos - 1;
    }

    Ident Driver::get_entityid (const string *name)
    {
	return get_entityid (*name);
    }

    string Driver::get_entityname (Ident id)
    {
	Ident eos = 1;
	SchemaObj *plugin = current_plugin;
	if (plugin) {
	    eos = (Ident) plugin->symsend();
	    if (id < eos) { return plugin->id2name (id); }
	}
	return entitytab.symname (id - (eos - 1));
    }

    EntityTab & Driver::get_table ()
    {
	return entitytab;
    }

    IFC::EntityBase *Driver::gen_expinstance (Instance &stepinst)
    {
	SchemaObj *plugin = current_plugin;
	if (! plugin) {
	    throw runtime_error
		("Driver::get_instanceid(): Plugin not loaded.");
	}
	return plugin->gen_expinstance (stepinst);
    }

    size_t Driver::get_instanceid (const IFC::EntityBase *v)
    {
	SchemaObj *plugin = current_plugin;
	if (! plugin) {
	    throw runtime_error
		("Driver::get_instanceid(): Plugin not loaded.");
	}
	return plugin->get_instanceid (v);
    }

} /*namespace STEP*/
