/* STEP/driver.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'driver.cc' 
** ("Compiler" driver. All properties concerning the parser are hidden behind
**  this module's interface.)
**
*/
#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include <cstddef>
#include <istream>
#include <vector>

#include "Common/symbols.h"
#include "Common/config.h"
#include "Common/outputmode.h"

#include "eb.h"
#include "types.h"
#include "scanner.h"
#include "step.tab.h"
#include "ettab.h"
#include "genv.h"
#include "xload.h"

namespace STEP {

    class Driver {
    public:
	std::string streamname;

	bool no_anchorsection, no_referencesection, no_schemapopulation,
	     no_signature, no_express_constants, single_data_section,
	     no_filepopulation, no_sectionlanguage, no_sectioncontext,
	     no_short_enums, classical_encoding, no_data_params,
	     no_value_instances, external_encoding;

	impllevel_t il;

	stepfile_t *stepfile = nullptr;

	Genv genv;

	Driver();

	virtual ~Driver();

	/**
	** parse - parse from a file
	** @param filename - string filename of a valid input file
	*/
	bool parse (const char *filename);

	/**
	** parse - parse from a c++ input stream
	** @param is - std::istream&, valid input stream
	*/
	bool parse (std::istream &is, const std::string &sname = "");

	void end_parse();

	void process_instances (OutputMode om = OutputMode::essential);

	// Some helper functions (required during the parsing process)
	void set_schema (const std::string &schema);
	Ident get_entityid (const std::string &name);
	Ident get_entityid (const std::string *name);
	std::string get_entityname (Ident id);
	EntityTab & get_table ();
	IFC::EntityBase *gen_expinstance (Instance &stepinst);
	size_t get_instanceid (const IFC::EntityBase *v);

    private:
	bool parse_helper (std::istream &istream);

	Config conf;
	STEP::Scanner *scanner = nullptr;
	STEP::Parser *parser = nullptr;

	std::string plugin_dir;
	std::string current_schema;
	SchemaObj *current_plugin;

//	std::vector<IFC::EntityBase *> instances;

	EntityTab entitytab;

    };

} /*namespace STEP*/

#endif /*DRIVER_H*/
