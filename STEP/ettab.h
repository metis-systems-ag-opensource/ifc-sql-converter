/* ettab.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Mini-header which introduces a type-alias (typedef) for
** 'Symbol<entityvalue_t *>'
**
*/
#ifndef ETTAB_H
#define ETTAB_H

#include <cstdint>

#include "Common/mintypes.h"
#include "Common/symbols.h"

namespace STEP {

using EntityTab = Symbols<Ident, Ident>;

} /*namespace STEP*/

#endif /*ETTAB_H*/
