/* types.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
** Implementations of the methods of the types defined in 'types.h' ...
**
*/

#include <iostream>

#include <string>
#include <map>
#include <stdexcept>
#include <utility>

#include "Common/sutil.h"

#include "types.h"
#include "genv.h"

#include "instdestroy.h"

namespace STEP {

using std::cerr;
using std::string;

/* REAL starting point ... */

// struct StepFile : Ptree {
StepFile::StepFile (headersection_t *headers, datasectionlist_t *datasections)
{
    headersection = headers;
    datasectionlist = datasections;
    anchorsection = nullptr;
    referencesection = nullptr;
}

StepFile::~StepFile () {
    delete headersection;
    list_delete (datasectionlist);
    if (anchorsection) { delete anchorsection; anchorsection = nullptr; }
    if (referencesection) {
	delete referencesection; referencesection = nullptr;
    }
}

void StepFile::add_anchorsection (anchorsection_t *anchors) {
    if (anchorsection) { delete anchorsection; }
    anchorsection = anchors;
}

void StepFile::add_referencesection (referencesection_t *references) {
    if (referencesection) { delete referencesection; }
    referencesection = references;
}

// } /*StepFile*/; 

// struct HeaderSection : Ptree {
HeaderSection::HeaderSection (filedescription_t *fdesc, filename_t *fname,
		   fileschema_t *fschema)
    : filedescription(fdesc), filename(fname), fileschema(fschema),
      optheaderlist(nullptr), signature(nullptr)
{ }

HeaderSection::~HeaderSection () {
    delete filedescription; delete filename; delete fileschema;
    list_delete (optheaderlist);
    if (signature) { delete signature; }
}

void HeaderSection::add_optheaders (optheaderlist_t * &optheaders) {
    optheaderlist = optheaders; optheaders = nullptr;
}

void HeaderSection::add_signature (signature_t *sig) {
    signature = sig;
}
// } /*HeaderSection*/;

// struct FileDescription : Ptree {
FileDescription::FileDescription (stringlist_t * &desc, int lev, int sublev)
    : description(desc), level(lev), sublevel(sublev)
{
    desc = nullptr;
}

FileDescription::~FileDescription () {
    if (description) {
	description->clear (); delete description; description = nullptr;
    }
}
// } /*FileDescription*/;


// struct FileName : Ptree {
FileName::FileName (string fname, string fstamp, string ppver, string osys,
		    string auth, stringlist_t * &authors,
		    stringlist_t * &organizations)
    : name(fname), timestamp(fstamp), preproc_ver(ppver), orig_sys(osys),
      authorization(auth), authorlist(authors), organizationlist(organizations)
{
    authors = nullptr;
    organizations = nullptr;
}

FileName::~FileName () {
    if (authorlist) {
	authorlist->clear (); delete authorlist; authorlist = nullptr;
    }
    if (organizationlist) {
	organizationlist->clear (); delete organizationlist;
	organizationlist = nullptr;
    }
}
// } /*FileName*/;

// struct FileSchema : Ptree {
// The value constructor is destructive on its argument ... meaning that after
// the object creation, the given string list is empty.
FileSchema::FileSchema (stringset_t * &schemas)
    : schemanames(move (*schemas))
{
    delete schemas; schemas = nullptr;
}

FileSchema::~FileSchema () {
    schemanames.clear ();
}

bool FileSchema::noschemas() { return schemanames.empty(); }
// } /*FileSchema*/;


//struct SchemaPopulation : public OptHeader {
SchemaPopulation::SchemaPopulation (extfileidlist_t *idlist)
    : extfileidlist(idlist)
{ }

SchemaPopulation::~SchemaPopulation () {
    list_delete (extfileidlist);
}
//} /*SchemaPopulation*/;

//struct FilePopulation : public OptHeader {
FilePopulation::FilePopulation (string name, string dmeth, stringlist_t *gsects)
    : schemaname(name), determinationmethod(dmeth), governedsections(gsects)
{ }

FilePopulation::~FilePopulation () {
    schemaname.clear (); determinationmethod.clear ();
    if (governedsections) {
	governedsections->clear (); delete governedsections;
	governedsections = nullptr;
    }
}
//} /*FilePopulation*/;

//struct SectionLanguage : public OptHeader {
SectionLanguage::SectionLanguage (string *sect, string &lang)
    : section(sect), defaultlanguage(lang)
{ }

SectionLanguage::~SectionLanguage () {
    if (section) { section->clear (); delete section; section = nullptr; }
    defaultlanguage.clear ();
}
//} /*SectionLanguage*/;

//struct SectionContext : OptHeader {
SectionContext::SectionContext (string *sect, stringlist_t *idlist)
    : section(sect), contextidentifiers(idlist)
{ }

SectionContext::~SectionContext () {
    if (section) { section->clear (); delete section; section = nullptr; }
    if (contextidentifiers) {
	contextidentifiers->clear (); delete contextidentifiers;
	contextidentifiers = nullptr;
    }
}
//} /*SectionContext*/;

//struct UserHeader : OptHeader {
UserHeader::UserHeader (Ident id, Fifo<Value *> * &attrs)
{
    name = id; attributes.reserve (attrs->length ());
    while (! attrs->empty ()) {
	Value *val = attrs->shift ();
	attributes.push_back (val);
    }
    delete attrs; attrs = nullptr;
}

UserHeader::~UserHeader () {
    for (auto it = attributes.begin(); it != attributes.end(); ++it) {
	if (*it) { delete (*it); *it = nullptr; }
    }
    attributes.clear ();
}

UserHeader::UserHeader (UserHeader &&x)
    : name(x.name), attributes(move(x.attributes))
{
//    x.attributes.clear ();
}

UserHeader & UserHeader::operator= (UserHeader &&x) {
    name = x.name;
    attributes = x.attributes;
    x.attributes.clear ();
    return *this;
}
//} /*UserHeader*/;

// struct ExtFileIds : Ptree {
ExtFileIds::ExtFileIds (const string &rs, string *lv, string *dg)
    : refstruct(rs), lastvisited(lv), digest(dg)
{ }

ExtFileIds::~ExtFileIds () {
    refstruct.clear ();
    if (lastvisited) { delete lastvisited; }
    if (digest) { delete digest; }
}
// } /*ExtFileIds*/;

// struct AnchorSection : Ptree {
AnchorSection::AnchorSection (anchorlist_t *alist)
    : anchorlist(alist), signature(nullptr)
{ }

AnchorSection::AnchorSection ()
    : anchorlist(nullptr), signature(nullptr)
{ }

AnchorSection::~AnchorSection ()
{
    list_delete (anchorlist);
}

void AnchorSection::add_signature (signature_t *sig)
{
    signature = sig;
}
// } /*AnchorSection*/;

// struct Anchor : Ptree {
Anchor::Anchor (const string &aname, value_t *attr)
    : name(aname), attribute(attr)
{ }

Anchor::~Anchor ()
{
    name.clear ();  delete attribute;
}
// } /*Anchor*/;


// struct ReferenceSection : Ptree {
ReferenceSection::ReferenceSection (referencelist_t *refs)
    : referencelist(refs), signature(nullptr)
{ }

ReferenceSection::ReferenceSection ()
    : referencelist(nullptr), signature(nullptr)
{ }

ReferenceSection::~ReferenceSection ()
{
    list_delete (referencelist);
}

void ReferenceSection::add_signature (signature_t *sig)
{
    signature = sig;
}
// } /*ReferenceSection*/;

// struct Reference : Ptree {
Reference::Reference (refid_t refid, const string &refurl, bool valref)
    : url(refurl), id(refid), valueref(valref)
{ }

Reference::Reference (Reference &x)
    : url(x.url), id(x.id), valueref(x.valueref)
{ }

Reference::~Reference () { }
// } /*Reference*/;

// struct ImplLevel {
// } /*ImplLevel*/;

string il2string (impllevel_t &l)
{
    return "(" + std::to_string (l.level) +
	   ";" + std::to_string (l.sublevel) +
	   ")";
}

// struct DataSection : Ptree {
DataSection::DataSection (string *sname, string &sschema)
  : instances(), schema(sschema), name(sname), signature(nullptr)
{ }

#if 0
DataSection::DataSection (instances_t *inst)
{
    name = nullptr; schema = nullptr; instances = inst;
    signature = nullptr;
}
#endif

DataSection::~DataSection ()
{
    if (name) { delete name; }
    for (auto &x: instances) { instdestroy (x); x = nullptr; }
    // Completely free 'instances' ...
    instances_t().swap (instances);
    if (signature) { delete signature; }
}

// Add the list of instances to the 'DATA' section structure. This 'add' is
// actually a 'store()', as old instances are replaced with the data from the
// instance list. This member function is a compromise: Normally, i would
// allocate as much memory as required for the instances table, but because
// of the (probably) huge amount of memory required for the data (millions of
// entries), a system may be unable to hold such structures (Fifo-list and
// 'std::vector' in memory together. So, i double the memory amount of memory
// for 'instances' (the 'std::vector') every time its size reached its capacity
// but only if the double capacity doesn't exceed the original size of the
// incoming list. In the latter case, the capacity is at last set to the the
// orginal length of 'inst' (the Fifo-list). This should (hopefully) keep the
// amount of memory required below the amount of memory required for both
// structures.
// After the insertion of the instances, the 'std::vector' instances must be
// sorted by the instance-ids each 'instance_t'-structure contains. A sorted
// instance table allows for an access of the instances by instance-id with
// nearly the same speed as a 'std::map<instance_t *>' would allow.
// A later generation of the tree-structures then could be reduced to pairs
// of instance-ids (or pairs of indices?).
void DataSection::add_instances (instlist_t *inst)
{
    size_t ilsize = inst->size();
    if (! instances.empty()) {
	// Remove the old instances ...
	for (auto &x: instances) { instdestroy (x); x = nullptr; }
	// Force the deletion of the old 'instances' vector ...
	instances_t().swap (instances);
    }
    // Reserve a minimum of 128 entries.
    instances.reserve (128);
    while (! inst->empty()) {
	size_t icap = instances.capacity();
	IFC::EntityBase *instance = inst->shift();
	if (instances.size() >= icap) {
	    icap *= 2; if (icap >= ilsize) { icap = ilsize; }
	    instances.reserve (icap);
	}
	instances.push_back (instance);
    }
    // Using the C++-implementation of the "lambda calculus" (sometimes named
    // anonymous function or closure): '[] (params) { body }'. Together with
    // the automatic type deduction, the instantiated function is of the type
    // 'bool (*) (const instance_t *, const instance_t *)' (or something like
    // that).
#if 0
    sort (instances.begin(), instances.end(),
	  [] (auto a, auto b) { return a->id < b->id; });
#endif
}

void DataSection::add_signature (signature_t *sig)
{
    if (signature) { delete signature; }
    signature = sig;
}
// } /*DataSection*/;

//struct DataParam {
DataParam::DataParam (string *sname, string &sschema)
  : name(sname), schema(sschema)
{ }

DataParam::~DataParam () { }
//} /*DataParam*/;

// struct Signature : Ptree {
Signature::Signature (const string &sig)
    : value(sig), valuesig(nullptr)
{ }

Signature::~Signature ()
{
    value.clear ();
    while (valuesig) {
	signature_t *s = valuesig;
	valuesig = s->valuesig; s->valuesig = nullptr;
	delete s;
    }
}

void Signature::add_signature (signature_t *vsig)
{
    valuesig = vsig;
}
// } /*Signature*/;

} /*namespace STEP*/
