/* readconf.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'readconf.cc'
** (Read and check the configuration)
**
*/
#ifndef READCONF_H
#define READCONF_H

#include <string>

#include "Common/config.h"

Config readconf (const std::string &cfgfile);

#endif /*READCONF_H*/
