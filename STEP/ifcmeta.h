/* ifcmeta.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file of the 'ifcmeta' module.
** (Generate the IFC-specific metadata from the data got from the parser.)
**
*/
#ifndef IFCMETA_H
#define IFCMETA_H

#include "ifcmodel.h"

#include "types.h"

namespace STEP {

IFC::IFCModel gen_ifcmeta (const headersection_t *headers);

} /*namespace STEP*/

#endif /*IFCMETA_H*/
