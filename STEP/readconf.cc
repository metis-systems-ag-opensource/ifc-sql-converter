/* readconf.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Read and check the configuration
**
*/

#include <set>
#include <exception>
#include <stdexcept>
#include <filesystem>

#include "Common/confsp.h"
#include "Common/sutil.h"

#include "readconf.h"

using std::runtime_error, std::exception;
using std::set;
using std::string, std::operator""s;

namespace fs = std::filesystem;

static string to_string (const set<string> &xs, string delim = ", ")
{
    bool first = true;
    string res;
    for (auto &x: xs) {
	if (first) { first = false; } else { res += delim; }
	res += x;
    }
    return res;
}

static bool directory_exists (const string &pathname)
{
    if (! fs::exists (pathname)) { return false; }
    return fs::is_directory (pathname);
}

static bool is_no_directory (const string &pathname)
{
    if (! fs::exists (pathname)) { return true; }
    return ! fs::is_directory (pathname);
}

class ConfigError : public runtime_error {
public:
    ConfigError (const string &msg)
	: runtime_error (msg)
    { }
    string what() { return "Configuration ERROR: "s + runtime_error::what(); }
};

static bool check_sqlite_database (const Config &cfgdata)
{
    if (! cfgdata.has ("DBTYPE") || lowercase (cfgdata["DBTYPE"]) != "SQLite") {
	return false;
    }
    if (! cfgdata.has ("DBDIR")) {
	throw ConfigError ("Missing DBDIR (required for 'SQLite' databases)");
    }
    if (is_no_directory (cfgdata["DBDIR"])) {
	throw ConfigError ("DBDIR points to something which is no directory");
    }
    return true;
}

static bool check_mysql_database (const Config &cfgdata)
{
    if (cfgdata.has ("DBTYPE")) {
	auto dbtype = lowercase (cfgdata["DBTYPE"]);
	if (dbtype != "mysql" && dbtype != "mariadb") { return false; }
    }
    set<string> req4db { "DBSERVER", "DBPORT", "DBAUTH" };
    if (auto [mf, missing] = cfgdata.check_missing (req4db); mf) {
	throw ConfigError
	    ("Missing items: " + to_string (missing));
    }
    // Additional checks may follow here (e.g. is the DBPROT really a port
    // number, or id DBSERVER a domain name).
    return true;
}

Config readconf (const string &cfgfile)
{
    Config cfgdata;
    set<string> required { "PLUGINDIR" };
    set<string> req4nonfiledb { "DBSERVER", "DBPORT", "DBAUTH" };
    set<string> missing;
    auto [path, found] = findFile (cfgfile);
    if (! found) {
	throw runtime_error
	    ("No configuration file \"" + cfgfile + "\" found.");
    }
    cfgdata.load (path);

    // The configuration items tested here _must_ always exist.
    if (auto [mf, missing] = cfgdata.check_missing (required); mf) {
	throw ConfigError ("Missing items: " + to_string (missing));
    }

    string plugindir = cfgdata["PLUGINDIR"];
#if 0
    if (plugindir[0] != '/') {
	throw runtime_error
	    ("Configuration value of PLUGINDIR is no absolute pathname");
    }
#endif
    if (! directory_exists (plugindir)) {
	throw runtime_error
	    ("The value of PLUGINDIR doesn't point to a directory");
    }

    // In a future version, file-based databases may be supported, so the
    // configuration items for non-file-based databases must be checked in an
    // extra condition.
    if (check_sqlite_database (cfgdata)) { return cfgdata; }

    if (check_mysql_database (cfgdata)) { return cfgdata; }

    auto &dbtype = cfgdata["DBTYPE"];
    throw ConfigError
	("The configured database type (" + dbtype + ") is not supported");
}

#ifdef TESTING

#include <iostream>
#include <string>

using std::cerr, std::cout, std::endl, std::string;

int main (int argc, char *argv[])
{
    if (argc < 2) {
	cout << "Usage: " << *argv << " filename" << endl;
	return 0;
    }
    string cfgfile (argv[1]);

    try {
	Config cfg = readconf (cfgfile);

	cout << "The configuration contains the following items:" << endl;
	for (auto &&it: cfg.item_names()) {
	    cout << "  " << it << ": " << cfg[it] << endl;
	}
    } catch (exception &exc) {
	cerr << *argv << ": " << exc.what() << endl;
	return 1;
    }

    return 0;
}

#endif /*TESTING*/
