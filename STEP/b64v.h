/* b64v.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
** Interface file of 'b64v.cc'.
**
** This a version of my small BASE64-decoder which i wrote some time ago. It
** works with a simple finite state engine which i specified manually for it.
**
** This code works unmodified with C and C++. The only restriction is that the
** data to be verified must be given as a C-string (aka 'const char *'), but
** this should not be a problem with 'std::string' (given that is has the
** method 'c_str()' which returns such a thing).
**
*/
#ifndef B64V_H
#define B64V_H

#ifndef __cplusplus
#include <stdbool.h>
#endif /*__cplusplus*/

bool verify_base64 (const char *s);

#endif /*B64V_H*/
