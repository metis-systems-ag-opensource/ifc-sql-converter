/* instdestroy.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Destroy instances (via the 'plugin').
**
*/

#include <stdexcept>

#include "instdestroy.h"

using std::runtime_error;

namespace STEP {

static SchemaObj *plugin = nullptr;

void instdestroy_init (SchemaObj *_plugin)
{
    if (! plugin) { plugin = _plugin; }
}

void instdestroy (IFC::EntityBase *p)
{
    if (! plugin) {
	throw runtime_error ("instdestroy(): Plugin not loaded.");
    }
    plugin->instdestroy (p);
}

} /*namespace STEP*/
