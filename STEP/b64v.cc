/* b64v.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
** This a version of my small BASE64-decoder which i wrote some time ago. It
** works with a simple finite state engine which i specified manually for it.
**
** This code works unmodified with C and C++. The only restriction is that the
** data to be verified must be given as a C-string (aka 'const char *'), but
** this should not be a problem with 'std::string' (given that is has the
** method 'c_str()' which returns such a thing).
**
*/
#ifndef __cplusplus
#include <stdbool.h>
#include <stddef.h>
#else
#include <cstddef>
#endif /*__cplusplus*/

#include "b64v.h"

/* Special B64-codes */
#define BE 66	    // '-' don't know what for (alternative encoding) ...
#define BE 66	    // '-' don't know what for (alternative encoding) ...
#define BP 64	    // '=' a fill-character, used once or twice at the end of
		    // a BASE64 code sequence */
#define BS 65	    // white space (layout characters) which normally should
		    // not disturb a BASE64 code sequence and so are normally
		    // ignored
#define XX 67	    // Characters which are INVALID in a BASE64 code sequence
#define EOS 68	    // End of stream marker (not in table) ...

/* BASE64-decoding table */
static unsigned char detab[] = {
    XX, XX, XX, XX, XX, XX, XX, XX, XX, BS, BS, XX, XX, BS, XX, XX, /* 00-0F */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* 10-1F */
    BS, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, 62, XX, BE, XX, 63, /* 20-2F */
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, XX, XX, XX, BP, XX, XX, /* 30-3F */
    XX,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, /* 40-4F */
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, XX, XX, XX, XX, XX, /* 50-5F */
    XX, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, /* 60-6F */
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, XX, XX, XX, XX, XX, /* 70-7F */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* 80-8F */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* 90-9F */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* A0-AF */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* B0-BF */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* C0-CF */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* D0-DF */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, /* E0-EF */
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX  /* F0-FF */
};

/* States used in the state-engine */
enum State {
    S0 = 0, S1 = 1, S2 = 2, S3 = 3, S4 = 4, S5 = 5, TERM = 6, ERROR = 7
};

static enum State statetrans[8][6] = {
    /*EOS,   BS,    other, BP,    BE,    XX   */
    { TERM,  S0,    S1,    ERROR, ERROR, ERROR },       /*S0*/
    { ERROR, S1,    S2,    ERROR, ERROR, ERROR },       /*S1*/
    { ERROR, S2,    S4,    S3,    ERROR, ERROR },       /*S2*/
    { ERROR, ERROR, S3,    S5,    ERROR, ERROR },       /*S3*/
    { ERROR, S4,    S0,    S5,    ERROR, ERROR },       /*S4*/
    { TERM,  S5,    ERROR, ERROR, ERROR, ERROR },       /*S5*/
    { TERM,  ERROR, ERROR, ERROR, ERROR, ERROR },       /*TERM*/
    { ERROR, ERROR, ERROR, ERROR, ERROR, ERROR },       /*ERROR*/
};

enum Action { Noop, LShift6Add, RShift4Put1, RShift2Put2, LShift6AddPut3 };

static enum Action actiontab[8][6] = {
    /*EOS,  BS,   other,          BP,          BE,   XX  */
    { Noop, Noop, LShift6Add,     Noop,        Noop, Noop },    /*S0*/
    { Noop, Noop, LShift6Add,     Noop,        Noop, Noop },    /*S1*/
    { Noop, Noop, LShift6Add,     Noop,        Noop, Noop },    /*S2*/
    { Noop, Noop, Noop,           RShift4Put1, Noop, Noop },    /*S3*/
    { Noop, Noop, LShift6AddPut3, RShift2Put2, Noop, Noop },    /*S4*/
    { Noop, Noop, Noop,           Noop,        Noop, Noop },    /*S5*/
    { Noop, Noop, Noop,           Noop,        Noop, Noop },    /*TERM*/
    { Noop, Noop, Noop,           Noop,        Noop, Noop },    /*ERROR*/
};

static int chclass (unsigned char c64b)
{
    switch (c64b) {
        case EOS: return 0;
        case BS:  return 1;
        case BP:  return 3;
        case BE:  return 4;
        case XX:  return 5;
        default:  return 2;
    }
}

bool verify_base64 (const char *s)
{
    bool has_consumed_data = false;
    unsigned char c64b;
    int ch, cc;
    unsigned long triple;
    enum State ostate, nstate;
    enum Action action;
    ostate = S0; triple = 0;
    while (ostate != TERM && ostate != ERROR) {
        ch = *s++;
        c64b = detab[ch & 255];
        cc = chclass (c64b);
        nstate = statetrans[(int)ostate][cc];
        action = actiontab[(int)ostate][cc];
        switch (action) {
            case LShift6Add:
                triple = (triple << 6) | c64b;
                break;
            case RShift4Put1:
                triple >>= 4;
                break;
            case RShift2Put2:
                triple >>= 2;
                break;
            case LShift6AddPut3:
                triple = (triple << 6) | c64b;
                break;
            default:
                break;
        }
        if (! has_consumed_data && nstate != S0) { has_consumed_data = true; }
        ostate = nstate;
    }
    return ostate == TERM && has_consumed_data;
}
