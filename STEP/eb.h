/* eb.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Type definition of 'EntityBase'. This is a complete abstract data type
** (meaning it has no real implementation). This means that only pointers
** of this type are valid ... and operations on this type defined in the
** plugin (where this type is also defined).
**
*/
#ifndef EB_H
#define EB_H

namespace IFC {

    struct EntityBase;

} /*namespace IFC*/

#endif /*EB_H*/
