/* genv.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Global environment being used in the parser for keeping informations
** between action blocks.
**
*/
#ifndef GENV_H
#define GENV_H

#include <string>

#include "Common/bitset.h"
#include "types.h"

namespace STEP {

struct Genv {
    stringset_t data_sections;
    std::string default_schema, current_schema;
    BitSet<uint32_t, 0> instset;
    HeaderSection *headers;
    DataSection *current_ds;
    size_t max_instances;
};

} /*namespace STEP*/

#endif /*GENV_H*/
