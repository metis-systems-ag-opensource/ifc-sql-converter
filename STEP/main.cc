/* STEP/main.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Main program of the 'STEP'-parser.
**
*/
#include <fstream>
#include <iostream>
#include <algorithm>
#include <optional>
#include <stdexcept>
#include <sysexits.h>

#include <cstdlib>
#include <cstring>

#include "Common/sutil.h"
#include "Common/timer.h"
#include "Common/outputmode.h"

#include "driver.h"
#include "ptr2string.h"
#include "ettab.h"

using namespace std;

// The standard error output channel 'cerr' is used not only for error messages
// here, but for status and progress messages, too. The reason for this is that
// in testing mode (option '-t' given), the data produced on standard output
// should not be disturbed by the standard status messages always produced by
// this program. The only exception from this is the standard usage message,
// but this is no problem, as the program anyway terminates after issuing this
// message.
//
int main (const int argc, const char **argv)
{
    using namespace STEP;
    Timer mt1(2), mt2;
    Driver driver;
    OutputMode om = OutputMode::essential;
    int argx = 1;
    if (argc < 2 || strcmp (argv[argx], "-h") == 0) {
	cout << "Error: Usage failure" << EOL << flush;
	cerr << "Usage: " << *argv << " [-h|-e|-n|-t|--] IFC-file" EOL EOL
		"Options:" EOL
		"  --"
		"    'End of options'. This 'option' allows for IFC-file"
		" to begin with a '-'." EOL
		"  -h" EOL
		"    Write this message to the standard error output and"
		" terminate." EOL
		"  -e (default)" EOL
		"    Write only essential data to the standard output." EOL
		"  -n" EOL
		"    Don't write anything to the standard output." EOL
		"  -t" EOL
		"    Write all available data to the standard output." EOL
		"None of these options affects the standard error output." EOL;
	return 0;
    }
    if (strcmp (argv[argx], "-e") == 0) {
	// Write only "essential" data to 'stdout'/'cout' (default).
	/*om = OutputMode::essential;*/
	++argx;
    } else if (strcmp (argv[argx], "-n") == 0) {
	// Don't Write any data to 'stdout'/'cout' (default).
	om = OutputMode::nothing; ++argx;
    } else if (strcmp (argv[argx], "-t") == 0) {
	// Write all available data to 'stdout'/'cout' (default).
	om = OutputMode::all; ++argx;
    } else if (strcmp (argv[argx], "--") == 0) {
	++argx;
    }
    if (argx >= argc) {
	if (om != OutputMode::nothing) {
	    cout << "Error: Missing 'IFC-file' argument." EOL;
	}
	cerr << *argv << ": Missing 'IFC-file' argument." EOL;
	cerr << "  Use '" << *argv << "'"
		" (without arguments) for help, please!" EOL;
	return EX_USAGE;
    }
    cerr << "Starting pass 1 (parsing) ..." EOL;
    mt1.start(); // Starting the timer
    mt2 = mt1;   // Assigning copies the complete state!
    // We have now two identical timers
    if (! driver.parse (argv[argx])) {
	if (om != OutputMode::nothing) {
	    cout << "Error: Parsing failure on '" << argv[argx] << "'" EOL;
	}
	cerr << *argv << "Parsing '" << argv[argx] << "' failed." EOL;
	return 1;
    }
    driver.end_parse();
    mt2.stop();  // Stopping timer 2
    cerr << "Parsing '" << argv[argx] << "' complete (required time: " <<
	    mt2.hms() << ")." << endl << " stepfile = " <<
	    STEP::ptr2string (driver.stepfile);
    cerr << EOL "Starting pass 2..." EOL << flush;
    mt2.start(); // Starting timer 2 again (with a new start time)
    try {
	driver.process_instances (om);
    } catch (exception &e) {
	if (om != OutputMode::nothing) {
	    cout << "Error: Processing instances failed." EOL;
	}
	cerr << "Pass 2 failed - " << e.what() << EOL << flush; return 1;
    }
    mt2.stop();  // mt2 now contains the time the second pass needed, and
    mt1.stop();  // mt1 contains the time the complete IFC processing needed
    cerr << "Pass 2 complete (required time: " << mt2.hms() << ")." EOL
	    "Overall required time: " << mt1.hms() << EOL << flush;

    return 0;
}
