/* supported.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Interface file for 'supported.cc'
** (Check if a given schema is supported. If this is not the case (e.g.
**  'IFC2X2*'), then try to replace it with a replacement schema. If this
**  fails, return a 'nullptr', indicating an error.)
**
*/
#ifndef SUPPORTED_H
#define SUPPORTED_H

#include <optional>
#include <set>
#include <string>

namespace STEP {

// Get the "next" supported schema name (if it exists) or return the empty
// std::optional value.
std::optional<std::string> supported_schema (const std::string &schema);

// Generate a new set of schema names by removing unsupported schema names
// from the given set of schema names 'in', and by replacing "partially"
// supported schema names with the next better choice.
std::set<std::string> supported_schemas (std::set<std::string> &in);

} /*namespace STEP*/

#endif /*SUPPORTED_H*/
