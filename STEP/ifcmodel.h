/* ifcmodel.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Model description of an IFC file (all things which are part of the 'HEADER;'
** part of the corresponding STEP-file.
**
*/
#ifndef IFCMODEL_H
#define IFCMODEL_H

#include <vector>
#include <string>

namespace IFC {

struct IFCModel {
    // FILE_DESCRIPTION (`FileDescription` in the database)
    std::vector<std::string> filedescription;
    uint32_t level, sublevel;
    // FILE_NAME (`FileName` in the database)
    std::string filename, timestamp, origsys, preprocver, authorization;
    std::vector<std::string> authors, organisations;
    // FILE_SCHEMA (`FileSchema` in the database)
    std::vector<std::string> schemas;
};

} /*namespace IFC*/

#endif /*IFCMODEL_H*/
