/* instdestroy.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Interface file for 'instdestroy.cc'
** (Destroy instances (via the 'plugin').)
**
*/
#ifndef INSTDESTROY_H
#define INSTDESTROY_H

#include "eb.h"
#include "xload.h"

namespace STEP {

// Need to activate this because of the "plugin" mechanism ...

void instdestroy_init (SchemaObj *_plugin);

// Executes 'delete p' on a pointer of the real implementation of the
// 'IFC::EntityBase'
void instdestroy (IFC::EntityBase *p);

} /*namespace STEP*/

#endif /*INSTDESTROY_H*/
