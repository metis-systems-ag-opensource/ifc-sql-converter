#ifndef SCANNER_H
#define SCANNER_H

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "step.tab.h"
#include "location.hh"

// #undef YY_DECL
// #define YY_DECL int STEP::Scanner::yylex(STEP::Parser::semantic_type *const lval, STEP::Parser::location_type *location)

namespace STEP {

    class Scanner: public yyFlexLexer {
    public:
	Scanner (std::istream *in) : yyFlexLexer (in)
	{
	    loc = new STEP::Parser::location_type();
	};

	using FlexLexer::yylex;

	virtual int yylex (STEP::Parser::semantic_type *const lval,
			   STEP::Parser::location_type *location);
	// YY_DECL defined in step.l
	// Method body created by 'flex' in Scanner.yy.cc

    private:
	/* yylval ptr */
	STEP::Parser::semantic_type *yylval = nullptr;
	/* location ptr */
	STEP::Parser::location_type *loc = nullptr;
    };

} /*namespace STEP*/

#endif /*SCANNER_H*/
