/* typedefs.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Simple type definitions used as "Forwards", being used in 'ifcser.h' and
** 'jsonser.h' ...
**
*/
#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include "Common/mintypes.h"

namespace STEP {

// Typedefs (type-aliases) for the struct-types ...
typedef struct Signature signature_t;
typedef struct StepFile stepfile_t;
typedef struct HeaderSection headersection_t;
typedef struct AnchorSection anchorsection_t;
typedef struct Anchor anchor_t;
typedef struct ReferenceSection referencesection_t;
typedef struct Reference reference_t;
typedef struct DataSection datasection_t;
//typedef struct Instance instance_t;
typedef struct FileDescription filedescription_t;
typedef struct FileName filename_t;
typedef struct FileSchema fileschema_t;
typedef struct OptHeader optheader_t;
typedef struct SchemaPopulation schemapopulation_t;
typedef struct FilePopulation filepopulation_t;
typedef struct SectionLanguage sectionlanguage_t;
typedef struct SectionContext sectioncontext_t;
typedef struct UserHeader userheader_t;
typedef struct Value value_t;
typedef struct LogicalValue logicalvalue_t;
typedef struct IntValue intvalue_t;
typedef struct FloatValue floatvalue_t;
typedef struct StringValue stringvalue_t;
typedef struct BitStringValue bitstringvalue_t;
typedef struct EnumValue enumvalue_t;
typedef struct ConstValIdValue constvalidvalue_t;
typedef struct ConstInstIdValue constinstidvalue_t;
typedef struct ValIdValue validvalue_t;
typedef struct InstIdValue instidvalue_t;
typedef struct TypedParamValue typedparamvalue_t;
typedef struct NullValue nullvalue_t;
typedef struct UnspecifiedValue unspecifiedvalue_t;
typedef struct ListValue listvalue_t;
typedef struct UrlValue urlvalue_t;
typedef struct EntityValue entityvalue_t;
typedef struct ImplLevel impllevel_t;
typedef struct ExtFileIds extfileids_t;
typedef struct Entity entity_t;

} /*namespace STEP*/

#endif /*TYPEDEFS_H*/
