/* supported.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Check if a given schema is supported. If this is not the case (e.g.
** 'IFC2X2*'), then try to replace it with the next higher schema. Otherwise,
** return a 'nullptr', indicating an error.
**
*/

#include <iostream>
#include <set>

#include "Common/sutil.h"

#include "supported.h"

namespace STEP {

using std::cerr, std::endl;
using std::optional;
using std::set;
using std::string;

struct spc { const char *schema, *replace; };

static spc supported_schema_list[] = {
    { "IFC2X2", "IFC2X3" },
    { "IFC2X3", nullptr },
    { "IFC4", nullptr },
    { nullptr, nullptr }
};

optional<string> supported_schema (const string &schema)
{
    spc *s;
    // First round: check for identity of any schema in 'supported_schemas[]'
    // with the given schema name.
    for (unsigned ix = 0; (s = &supported_schema_list[ix])->schema; ++ix) {
	if (schema == s->schema) {
	    // Return a new schema name (which probably matches the given one)
	    return optional<string> (s->replace ? s->replace : s->schema);
	}
    }
    // Second round: check for any schema in 'supported_schemas[]' being a 
    // prefix of the given schema name, but only if the corresponding 'replace'
    // entry is not empty (aka 'nullptr').
    for (unsigned ix = 0; (s = &supported_schema_list[ix])->schema; ++ix) {
	if (s->replace && is_prefix (s->schema, schema)) {
	    // Return a replacement schema.
	    return optional<string> (s->replace);
	}
    }
    // Return _no_ schema.
    return optional<string>();
}

set<string> supported_schemas (set<string> &in)
{
    set<string> res;
    for (auto &sname: in) {
	auto newschema = supported_schema (sname);
	if (! newschema) {
	    cerr << "WARNING! Deleting unsupported schema: " << sname << endl;
	    continue;
	}
	auto [ipos, done] = res.emplace (*newschema);
	if (! done) { continue; }
	if (sname != *newschema) {
	    cerr << "WARNING! The schema '" << sname << "'"
		    " is unsupported by this converter." << endl <<
		    "  But there is a schema name near enough which may"
		    " replace it: '" << *newschema << "'" << endl <<
		    "  This schema is now inserted instead." << endl;
	}
    }
    return res;
}

} /*namespace STEP*/
