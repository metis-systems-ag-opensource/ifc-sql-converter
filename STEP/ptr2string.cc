/* ptr2string.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Small conversion for 'void * => std::string' (simply writing the address
** of the 'void *' as string) ...
**
*/

#include <cstdio>
#include <string>

#include "ptr2string.h"

namespace STEP {

    std::string ptr2string (void *p)
    {
	char buf[128];
	snprintf (buf, sizeof(buf), "%p", p);
	return std::string (buf);
    }

} /*namespace STEP*/
