#! /usr/bin/perl
# rm-model.pl
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@pps-ag.com
# Copyright: (c) 2020, PPS AG
# 
#
# Delete a model specified by either the model's file id or a known model id.

use strict;
#use feature 'state';

#use Digest::MD5;

#use Cwd;
use DBI;

use FindBin;
use lib "$FindBin::Bin", "$FindBin::Bin/../lib", "$ENV{HOME}/lib/perl";
use readconf qw(readconf get_authdata);

sub usage (@);
sub dbclose ($@);
sub dbfail (\@@);
sub dbquit (\@@);

(my $prog = $0) =~ s(.*/)();

(@ARGV > 0) or usage;

my $id = shift;

my ($errs, %CONF) = readconf ('test');

unless ($errs == 0) {
    print STDERR "$prog: The configuration contains error ... aborting!\n";
    exit 1;
}

do {
    my @missing;
    for $_ (qw(DBSERVER DBPORT DBAUTH)) {
	(exists $CONF{$_}) or push @missing, $_;
    }

    if (@missing) {
	print STDERR "$prog: Missing (".(join ', ', map { "'$_'" } @missing).
		     ") from configuration.\n";
	exit 1;
    }
};

@CONF{qw(USERNAME PASSWORD)} = get_authdata ($CONF{DBAUTH}) or
    die "$prog: Reading the authentication data failed - $!\n";

my $dsntmpl = sprintf 'DBI:mysql:database=%%s;host=%s;port=%d',
		      @CONF{qw(DBHOST DBPORT)};

my (@DBH, $MDB, $SDB);
my $dsn = sprintf $dsntmpl, 'IFCModel';
$MDB = DBI->connect ($dsn, @CONF{qw(USERNAME PASSWORD)}, { AutoCommit => 1 })
    or dbfail @DBH, "Connecting 'IFCModel' database failed - $DBI::errstr";

push @DBH, [ $MDB, 0 ];

my $modelid;
if ($id =~ /^\d+$/) {
    my $sth = $MDB->prepare ("SELECT * FROM Model WHERE id = ?");
    $sth->execute ($id) or dbquit @DBH, "Selecting model failed";
    if (my $row = $sth->fetchrow_hashref) {
	$modelid = $row->{id}; $id = $row->{fileid};
    }
    $sth->finish;
} else {
    my $sth = $MDB->prepare ("SELECT * FROM Model WHERE fileid = ?");
    $sth->execute ($id) or dbquit @DBH, "Selecting model failed";
    if (my $row = $sth->fetchrow_hashref) { $modelid = $row->{id}; }
    $sth->finish;
}

(defined $modelid) or
    dbfail @DBH, "No model for id=$id found.\n";

my $sth = $MDB->prepare ("SELECT * FROM FileSchema WHERE ModelId = ?");
$sth->execute ($modelid) or dbquit @DBH, "Selecting schema database failed";
my ($schema);
if (my $row = $sth->fetchrow_hashref) { $schema = $row->{schema}; }
$sth->finish;

(defined $schema) or
    dbfail @DBH, "ERROR! The model $modelid has no valid schema.";

my $schemadb = lc $schema;

# Now delete the data concerning the selected model from the 'IFCModel'
# database.

$MDB->begin_work;
$DBH[$#DBH][1] = 1;

$MDB->do ("DELETE FROM Authors WHERE id = ?", undef, $modelid) or
    dbquit @DBH, "Failed to delete 'Authors[$modelid]'";

$MDB->do ("DELETE FROM Organisations WHERE id = ? ", undef, $modelid) or
    dbquit @DBH, "Failed to delete 'Organisations[$modelid]'";

$MDB->do ("DELETE FROM FileSchema WHERE ModelId = ? ", undef, $modelid) or
    dbquit @DBH, "Failed to delete 'FileSchema[$modelid]'";

$MDB->do ("DELETE FROM FileDescription WHERE ModelId = ? ", undef, $modelid) or
    dbquit @DBH, "Failed to delete 'FileDescription[$modelid]'";

$MDB->do ("DELETE FROM FileName WHERE ModelId = ? ", undef, $modelid) or
    dbquit @DBH, "Failed to delete 'FileName[$modelid]'";

$MDB->do ("DELETE FROM Model WHERE id = ? ", undef, $modelid) or
    dbquit @DBH, "Failed to delete 'Model[$modelid]'";

$dsn = sprintf $dsntmpl, $schemadb;
$SDB = DBI->connect ($dsn, @CONF{qw(USERNAME PASSWORD)}, { AutoCommit => 1 })
    or dbfail @DBH, "Connecting '$schemadb' database failed - $DBI::errstr";

push @DBH, [ $SDB, 0 ];

my @tables;
$sth = $SDB->prepare ("SHOW TABLES");
$sth->execute or dbquit @DBH, "'SHOW TABLES' failed";
while (my $row = $sth->fetchrow_arrayref) {
    local $_ = $row->[0];
    next if (/^(?:Enums|ObjType)$/);
    next if (/^(?:_EnumValues|_SelectAccess|_TypeClass|_TypeIdName)$/);
    push @tables, $_;
}
$sth->finish;

$SDB->begin_work;
$DBH[$#DBH][1] = 1;

for my $table (@tables) {
    my $delcmd = sprintf 'DELETE FROM `%s` WHERE modelid = ?', $table;
    $SDB->do ($delcmd, undef, $modelid) or
	dbquit @DBH, "Failed to delete '$table\[$modelid]'";
}

# Disconnect from the databases ...
dbclose 1, @DBH;
($MDB, $SDB) = ();

print "Sucessfully deleted IFC model '$id'\n";

exit 0;

sub usage (@) {
    if (@_) { print STDERR "$prog: ".(join '', @_)."\n"; exit 64; }
    print "Usage: $prog <file-id|model-id>\n".
	  "       $prog  # w.o. arguments for this usage message.\n";
    exit 0;
}

sub dbclose ($@) {
    my $cmf = shift @_;
    while (@_) {
	my ($DBH, $rbf) = @{pop @_};
	if ($cmf) {
	    $DBH->commit if ($rbf);
	} else {
	    $DBH->rollback if ($rbf);
	}
    }
}

sub dbfail (\@@) {
    my @DBH = @{shift @_};
    print STDERR "$prog: ".(join '', @_)."\n";
    dbclose (0, @DBH);
    exit 1;
}

sub dbquit (\@@) {
    my @DBH = @{shift @_};
    my ($DBH) = @{$DBH[$#DBH]};
    dbfail @DBH, @_, ' - ', $DBH->errstr;
}
