# derived_attributes.pm
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@pps-ag.com
# Copyright: (c) 2020, PPS AG
# 
#
# Module solely consisting of a list (HASH) of names of ENTITY instances which
# contain the 'GlobalId' attribute.

package derived_attributes;
use 5.16.0;
use strict;
use vars qw($RCSID $VERSION);

$RCSID = q$Id$;
$VERSION = '0.01';

use Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
use Carp;

use Readonly;

@ISA = qw(Exporter);
@EXPORT = qw(%DerivedAttributes);
@EXPORT_OK = ();
%EXPORT_TAGS = (all => [ @EXPORT, @EXPORT_OK ]);

#use vars qw(%DerivedAttributes);

Readonly::Hash our %DerivedAttributes => (
    IFC2X3 => {
	IFCORIENTEDEDGE => [0, 1],
	IFCGEOMETRICREPRESENTATIONSUBCONTEXT => [2, 3, 4, 5],
	IFCSIUNIT => [0],
    },
    IFC4 => {
	IFCORIENTEDEDGE => [0, 1],
	IFCMIRROREDPROFILEDEF => [3],
	IFCGEOMETRICREPRESENTATIONSUBCONTEXT => [2, 3, 4, 5],
	IFCSIUNIT => [0],
    },
);

1;
