#! /usr/bin/perl
# find-guids.pl
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# License: MIT License
#
# A small validation program which reads an IFC file and then checks for each
# instance with a GUID ('GlobalId') if a corresponding entry exists in the
# database which corresponds to this file.


use strict;
use feature 'state';

#use Data::Dumper;
#use Data::Dumper::Simple;

use Digest::MD5;

use FindBin;
use lib "$FindBin::Bin", "$FindBin::Bin/../lib", "$ENV{HOME}/lib/perl";

use readconf qw(readconf get_authdata);

# Load a read only HASH whose keys are the names of entities which have the
# 'GlobalId' attribute.
use instances_with_guid;

# A just sufficient parser for IFC files (IFC2x3, IFC4 … probably also newer).
# For getting any data from an IFC file, an IFC parser object requires two
# callback routines, one for the 'HEADER section, and one for the 'DATA'
# section.
use parseifc;

use Cwd;	# 'cwd()' (current working directory)

use DBI;	# Perl's "abstract" database interface.

sub usage (@);		# Usage function
sub getHeader ($$$$);	# Callback routine used in the IFC 'HEADER' section.
sub getGuid ($$$$$);	# Callback routine used in the IFC 'DATA' section.
sub get_fileid ($);	# Calculate the 'file id' being used in the database.
sub myError ($@);	# Disconnect from the DB, then terminate with errormsg.
sub DbgPrint (@);	# 'print ...', but only if $DEBUG is true.

#$Data::Dumper::Indent = 1;

my $DEBUG = 1;

(my $prog = $0) =~ s(.*/)();
(my $basename = $prog) =~ s/\.pl$//;

my %instances = ();

# No command line arguments? => Issue a usage message and terminate.
(@ARGV > 0) or usage;

# The (currently) only argument is the filename of an IFC file.
my $file = shift;

# Read the configuration data for this program from the file 'test.conf',
# which resides somewhere in a fixed configuration path. The first result
# is the number of parse errors occurred while reading the configuration
# file, and the remaining form a HASH of all successfully parsed
# configuration items.
my ($errs, %CFG) = readconf ('test');

# A configuration file containing errors is not accepted.
if ($errs > 0) {
    print STDERR "$prog: The configuration file has errors. Aborting...\n";
    exit 1;
}

# Check if the configuration contains a particular set of items. If only one
# of these items is absent, the program cannot run sucessfully, so it is
# terminated in this case.
do {
    my @nf;
    for my $ci (qw(DBSERVER DBPORT DBAUTH)) {
	next if exists ($CFG{$ci});
	push @nf, $ci;
    }
    if (@nf) {
	print STDERR "$prog: Missing configuration items: ".
		     (join ', ', @nf)."\n";
	exit 1;
    }
};

# Test if the filename given as command line argument is that of an existing
# regular file; terminate, otherwise.
(-f $file) or usage "$prog: '$file' must be a regular file.";

# Create an IFC-pareser object and store its reference in '$ifcp'.
my $ifcp = new parseifc ($file);

# Add the callback functions for the HEADER and DATA sections.
$ifcp->headercb (\&getHeader);
$ifcp->datacb (\&getGuid);

# Parse the IFC-file '$file'
DbgPrint "Parsing the IFC file '$file' ...";
my $nErrs = $ifcp->parse();
DbgPrint "Parsing complete";

# When the parsing returns errors, it doesn't make much sense to continue,
# so in this case, the program is terminated with an appropriate error
# message.
if ($nErrs) {
    print STDERR "$prog: The parsing completed with errors. It may be".
		 " impossible to get\n".
		 (" " x length $prog)."  any meaningful data\n";
}

my $dsntmpl = sprintf 'DBI:mysql:database=%%s;host=%s;port=%d',
		      $CFG{DBSERVER}, $CFG{DBPORT};

DbgPrint "Connecting the 'IFCModel' database for determing the model id";
# For accessing the correct database and model, a connection to the
# 'IFCModel' database is required first.
my $dsn = sprintf $dsntmpl, 'IFCModel';
my ($user, $pass) = get_authdata ($CFG{DBAUTH});
my $DBH = DBI->connect ($dsn, $user, $pass) or
    myError (undef, "Attempt to connect database failed - $DBI::errstr");

# Get the 'HEADER' elements from the IFC-parser. They were stores under the
# name 'HEADER' because of the callback routine 'getHeader()'.
my $hd = $ifcp->extra ('HEADER');
(exists $hd->{FILE_NAME}) or
    myError ($DBH, "No 'FILE_NAME' header defined.");

# Calculate the 'fileid' value (required for accessing the correct model in
# the corresponding IFC-database) from the 'FILE_NAME' attributes.
my $fnid = get_fileid ($hd->{FILE_NAME});

# Get the (primary) schema name from the parser (the first element of the
# 'schemalist' attribute of the 'FILE_SCHEMA' HEADER instance).
my $schema = $ifcp->schema or
    myError ($DBH, "No schema id defined.");

# Searching the correct model from the 'IFCModel' database. Because i don't
# prohibit storing the same model multiple times yet (even if a warning
# message is generated), i'm using a tolerant method, where user interaction
# is requested.
my @models;
my $sth = $DBH->prepare ("SELECT * FROM Model WHERE fileid = ?");
$sth->execute ($fnid);
while (my $row = $sth->fetchrow_hashref) { push @models, $row; }
$sth->finish;

# No matching model found. This is an error.
(@models > 0) or
    myError ($DBH, "Found no IFC model for fileid = '$fnid'.");

DbgPrint "(List of) Model(s) retrieved";

# If more than one matching 'Model' entry was found, the user is asked for the
# model to be used. This is done by reducing the array '@models' to exactly
# one entry if the user has made his/her choice.
$| = 1;
while (@models > 1) {
    my %models = map { $_->{id} => $_ } @models;
    my @k = sort (map { $_->{id} } @models);
    print "Which model (".(join ', ', map { $_->{id} } @models).")? ";
    local $_ = <STDIN>;
    chomp; s/^\s+//; s/\s+$//;
    unless (/^\d+/) { print STDERR "Invalid input; retry, please!\n"; next; }
    $_ = $_ + 0;
    if (exists $models{$_}) { @models = ($models{$_}); next; }
    print STDERR "$_: No such model; retry, please!\n";
}

# Extract the only entry from '@models'. There are two ways for doing this:
#   1. Using the index '[0]',
#   2. Treating the left hand side of the assignment as a list, by enclosing
#      it in '(' and ')' and forcing an assignment in list context.
# I'm often using the second way, because is older perl versions, the lowest
# index of an array could be changed (using the '$[' variable), so, instead
# of using the horrible '$xyz[$[]' for the first element of an array, i used
# list context assignment. This remained from an early Perl era, but even
# today, i'm seldom doing it the other way.
($_) = @models;
my %model = %{$_};
DbgPrint "Comitting to a specific model id: $model{id}";

# SELECT the first element from the 'FileSchema' list from the database. All
# my (INTEGER) key elements in the IFC databases start with '1'. Any index of
# '0' is a very special case and indicates an empty aggregate.
my $dbschema;
$sth = $DBH->prepare ('SELECT * FROM FileSchema WHERE ModelId = ? AND seq = 1');
$sth->execute ($model{id});
if (my $row = $sth->fetchrow_hashref) { $dbschema = lc $row->{schema}; }
$sth->finish;
DbgPrint "Determined the database where this model was stored: $dbschema";

# No schema found in the database for the given model. This is an error.
(defined $dbschema) or
    myError ($DBH, "Couldn't get a schema id");

# It is also an error, if the schema name retrieved from the database doesn't
# match the (primary) schema name from the IFC-file.
($dbschema eq lc $schema) or
    myError ($DBH, "'FILE_SCHEMA' ($schema) and 'FileSchema' ($dbschema)",
		   "values differ");

# Because there is no such thing as a 'mysql_selectdb()' in the DBI driver,
# the other database is connected by disconnecting 'IFCModel' and opening a
# complete new connection to the 'lc $dbschema' database.
$DBH->disconnect;

# Connect the new database (it's name is the value of '$dbschema').
DbgPrint "Connecting the '$dbschema' database";
my $dsn = sprintf $dsntmpl, $dbschema;
$DBH = DBI->connect ($dsn, $user, $pass) or
    myError (undef, "Attempt to connect database failed -", $DBI::errstr);

# Don't know if this would work ... and don't want to know!
#unless ($DBH->do ("USE $dbschema")) {
#    $DBH->disconnect; print STDERR "No database '$dbschema' found.\n"; exit 1;
#}

# Now, for each entry in '$ifcp->extra ("guids")' something must be extracted
# from the database and either printed to STDOUT, or simply "validated" that
# the corresponding entry exists.

# In the following loop, all elements containing a GUID (the corresponding
# database attribute is 'GlobalId') are selected from the corresponding
# database tables for the selected model. An entry not found is marked in the
# hash '%result' with a single error message. If more than one matching entry
# was found (this should be a problem) all corresponding results from the
# database are stored in '%result', together with an appropriate error
# message. If exactly one entry was found (the good case), then an "undefined"
# error message is stored (together with the data retrieved from the
# database). Additionally, a flag (pre-set) is reset if one of the two error
# conditions occurred.
DbgPrint "Comparison run ...";
my @guids = @{$ifcp->extra ('guids')};
my (%result, $allok);
$allok = 1;
for my $ge (@guids) {
    my ($name, $id, $aNum, $guid) = @$ge;
    my $q = "SELECT * FROM $name WHERE GlobalId = '$guid'".
	    " AND modelid = $model{id}";
    my $sth = $DBH->prepare ($q);
    unless ($sth->execute ())
##was:    unless ($sth->execute ($guid, $model{id}))
    {
	print STDERR "$prog: '$name($guid)' - $DBH::errstr\n";
	undef $allok; next;
    }
    my @results;
    while (my $row = $sth->fetchrow_hashref) {
	if ($row->{GlobalId} eq $guid) {
	    push @results, $row; next;
	}
	DbgPrint "WARNING! Got more than on result for \"$q\".";
    }
    $sth->finish;
    $_ = \$result{$guid};
    unless (@results) {
	$$_ = [ "No matching result found!", $q ]; undef $allok;
    } elsif (@results > 1) {
	$$_ = [ "More than matching result found", $q, [ @results ] ];
	undef $allok;
    } else {
	$$_ = [ undef, @results ];
    }
}

# Disconnect from the database (because this connection is no longer
# required).
$DBH->disconnect;
DbgPrint "Comparison results collected.";

DbgPrint "Printing the result(s) ...";
# Print the result of the retrieval/validation.
if ($allok) {
    print "All GUID entries found for this model.\n";
} else {
    print "Not all is OK!\n";
    for my $guid (sort keys %result) {
	my ($msg, $q) = @{$result{$guid}};
	next unless (defined $msg);
	print "$guid: $msg\n  Query: $q\n";
    }
}
DbgPrint "All DONE.";

exit 0;

# The usage message is simple for this program, but i don't know if other cases
# take place (later), which require issuing usage-errors.
sub usage (@) {
    if (@_) { print STDERR "$prog: ".(join '', @_)."\n"; exit 64; }
    print "Usage: $prog file\n";
    exit 0;
}

# This is the callback subroutine to be used in the IFC-parser during parsing
# the HEADER section. It is invoked for each completed HEADER instance, with
# the exception of 'FILE_SCHEMA'; the latter is handled internally by the
# IFC-parser. The %$rData hash can be later got from the IFC-parser by an
# invocation of the 'extra()' method.
sub getHeader ($$$$) {
    my ($rData, $lc, $name, $rVal) = @_;
    my %values;
    for my $k (keys %{$rVal}) {
	$values{$k} = $ifcp->valof ($rVal->{$k});
    }
    (exists $rData->{HEADER}) or $rData->{HEADER} = {};
    $rData->{HEADER}{$name} = { %values };
    1
}

# This is the callback subroutine to be used in the IFC-parser during parsing
# the DATA section. It is invoked for each completed instance. The %$rData
# hash can be later got from the IFC-parser by an invocation of the 'extra()'
# method.
sub getGuid ($$$$$) {
    my ($rData, $lc, $id, $name, $rVal) = @_;
    if (exists $Instances_with_GUID{$name}) {
	exists $rData->{guids} or $rData->{guids} = [];
	unless (@$rVal) {
	    print STDERR "#$id = $name() ???\n";
	} else {
	    my $aNum = $Instances_with_GUID{$name};
	    my $guid = $rVal->[$aNum - 1];
	    if (ref $guid eq 'HASH' && $ifcp->typeof ($guid) eq 'string') {
		push @{$rData->{guids}},
		     [ $name, $id, $aNum, $ifcp->valof ($guid) ];
	    } else {
		print STDERR "#$id = $name(?...) ???\n";
	    }
	}
    }
    1
}

# Calculate the 'fileid' value from the attributes of 'FILE_NAME'. The order
# of the attribute names in '@fnk' MUST NOT BE CHANGED, or this function will
# no longer return the correct result. (The calculation consists simply of the
# generation of an MD5 digest and returning it in hexadecimal format.)
sub get_fileid ($) {
    (my $fn) = @_;
    local $_;
    my $md5 = new Digest::MD5;
    my @fnk = qw(name timestamp origsys preprocver authorization authors
		 organizations);
    for $_ (@fnk) {
	if (ref $fn->{$_} eq 'ARRAY') {
	    my @lv = sort { $a <=> $b } @{$fn->{$_}};
	    for my $v (@lv) { $md5->add ($v); }
	} elsif (! ref $fn->{$_}) {
	    $md5->add ($fn->{$_});
	}
    }
    my $res = $md5->hexdigest;
    $res
}

# Small error/termination function which disconnects from the database before
# writing the given error message to 'STDERR'.
sub myError ($@) {
    my $DBH = shift;
    $DBH->disconnect if (defined $DBH);
    printf STDERR "%s: %s\n", $prog, (join '', @_);
    exit 1;
}

sub DbgPrint (@) {
    print STDERR @_, "\n" if ($DEBUG);
}
