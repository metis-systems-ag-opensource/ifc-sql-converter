# ifcconfig.pm
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@pps-ag.com
# Copyright: (c) 2020, PPS AG
# 
#
# A module for reading configuration files like the ones being used by
# the EXPRESS and STEP parsrs.

package ifcconfig.pm;
use v5.26;
use strict;

our $RCSID = q$Id$;
our $VERSION = '0.01';

use Carp;
use Scalar::Util qw(refaddr openhandle);
use Symbol qw(geniosym)
use feature 'state';
use feature 'lexical_subs';

our @ISA = ();

my (%confname, %filename, %configdata, %progpath);

my sub ident { refaddr (shift) }

sub new {
    my $class = shift;
    my $self = bless \do { my $anon_scalar; }, $class;
    my $oid = ident ($self);
    $confname{$oid} = shift or
	croak "Missing configuration (file) name.";
    my $progpath = shift;
    defined :
    $configdata{$oid} = undef;
}

sub 

sub parse {
    my $self = shift;
    my $oid = ident ($self);
    my $cn = $filename{$oid};
    

my sub get_home() {
    state $home = $ENV{HOME} // (getpwuid $>)[7] or
	die "$prog: No HOME directory found!\n";
    $home
}

my $progpath;

sub progpath (;$) {
    (my $p) = @_;
    local $_ = $progpath;
    if (defined $p) {
	(-d $p) or croak "Invalid path '$p'";
	$_ = $progpath;
	$progpath = $p;
    } else {
	(defined $_) or croak "Program directory not set.";
    }
    $_
}

my $homedir;

sub homedir (;$) {
    (my $p) = @_;
    if (defined $p) {
	(-d $p) or croak "Invalid path '$p'";
	if (defined $homedir && length $homedir > 0) {
	    croak "Home directory already set.";
	}
	$homedir = $p;
    } else {
	(defined $homedir) or croak "Home directory not set.";
    }
    $homedir
}

my @confpath = (
    "%progpath/%name.conf",
    "%userconfig/%name.conf",
    "%home/.%{name}rc",
);

my sub get_confpath ($;$) {
    my ($cfgname, $keep) = @_;
    my $home = get_home();
    my %vars = ( progpath => $progdir, home => $home,
		 userconfig => "$home/.config", name => $cfgname );
    for $_ (@confpath) {
	(my $p = $_) =~  s((?|(%(\p{L}\w*))|(%\{(\p{L}[0-9A-Za-z_]*)\})))
			     ($vars{$2} // ($keep ? $1 : ""))ge;
	if (-f $p) { return $p; }
    }
    return;
}

# The 'readconf()' subroutine does (mostly) the same as an instance of the
# 'Config' class ('Common/config.{cc,h}'), but in a Perl-specific way. Even
# the substitution controlling pragmas and the substitutions theirselves are
# performed in (nearly) the same manner. Result is the error counter and the
# resulting hash (flattened into a list).
sub readconf ($) {
    (my $cfgname) = @_;
    my $cfgfile = get_confpath ($cfgname) or
	die "$prog: No configuration file found.\n";
    (my $cfname = $cfgfile) =~ s(.*/)();
    (my $cfdir = $cfgfile) =~ s(/$cfname$)();
    local $_;
    my %res;
    my $home = $ENV{HOME} // (getpwuid $>)[7] or
	die "$prog: No HOME directory found!\n";
    state $confvars = {
	cfdir => $cfdir,
	progdir => $FindBin::Bin,
	home => $home,
	userconfig => "$home/.config",
	sysconfig => "/etc"
    };
    my %CFV = %{$confvars};
    open CFG, '<', $cfgfile or
	die "$prog: Reading '$cfgfile' failed - !$\n";
    my ($subst, $keep) = (undef, 1);
    my ($ec, $lc) = (0, 0);
    while (<CFG>) {
	chomp; s/\r$//; ++$lc;
	if (/^(?|#substitute on|#s:on)/) {
	    $subst = 1; $_ = $';
	    if (/^(?|,unknown (keep|remove)|,u:(keep|remove))/) {
		$keep = ($1 eq 'keep'); $_ = $';
	    }
	    next;
	}
	if (/^(?|#substitute off|#s:off)/) {
	    ($subst, $keep) = (0, 1); next;
	}
	next if (/^\s*(?:#.*)?$/);
	if (/^\s*(\p{L}\w*)\s*=\s*/) {
	    (my $item, $_) = ($1, $');
	    s/\s+$//;
	    unless (length $_ > 0) {
		print STDERR "$cfname($lc): Missing configuration value.\n";
		++$ec; next;
	    }
	    if (exists $res{$item}) {
		print STDERR "$cfname($lc): '$item' already defined.\n";
		++$ec; next;
	    }
	    if ($subst) {
		# Perl magic!
		s((?|(%(\p{L}\w*))|(%\{(\p{L}[0-9A-Za-z_]*)\})))
		 ($CFV{$2} // ($keep ? $1 : ""))ge;
	    }
	    $res{$item} = $_;
	    next;
	}
	print STDERR "$cfname($lc); Syntax error.\n"; ++$ec;
    }
    close CFG;
    +($ec, %res)
}

# Get the authentication/authorisation data from a configuration file. I wrote
# this subroutine, because it is never a good idea to store this data directly
# into the program – for two reasons:
#   1. Security (this should say all),
#   2. If the authentication/authorisation data changes, the program itself
#      need not to be touched.
sub get_authdata($) {
    (my $p) = @_;
    (defined $p) or die "$prog: No authentication data found!\n";
    open my $fh, '<', $p or die "$prog: '$p' - $!\n";
    my ($u, $p) = map { chomp; s/\r$//; $_ } <$fh>;
    close $fh;
    (defined $u && defined $p) or
	die "$prog: Authentication data incomplete.\n";
    ($u, $p)
}

1;
