#! /usr/bin/perl
# ifc-validate.pl
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@pps-ag.com
# Copyright: (c) 2020, PPS AG
# 
#
# Small "validation" program for the IFC-data stored by the 'ifc-server' in
# a given database.
#
# Usage:
#
#    ifc-validate.pl [conffile] testdata instance-id
#

use strict;
use feature 'state';
use Cwd;
use DBI;
use Data::Dumper;

use FindBin;
use lib "$FindBin::Bin", "$FindBin::Bin/../lib", "$ENV{HOME}/lib/perl";

use derived_attributes;
use readconf qw(readconf get_authdata);

sub usage (@);
sub dirname ($);
sub get_instance ($$$);
sub print_instance ($$);

(my $prog = $0) =~ s(.*/)();
(my $bn = $prog) =~ s/\.p[lm]$//;
my $cwd = &cwd;
my $ppath = dirname $0;

(@ARGV > 0) or usage;
my $conf = shift if (@ARGV > 2);
(@ARGV > 1) or usage "Missing argument(s)";
my $testdata = shift;
my $instid = shift;
($instid =~ /^#?\d+$/) or usage "Invalid instance-id: $instid";
$instid =~ s/^#//; $instid += 0;

my (%DB_TABLE_SCHEMA, %ENUMVALS);
# Reading the configuration
my ($errs, %CONF) = readconf ($conf // 'test');

# Aborting if the configuration had errors
if ($errs > 0) {
    print STDERR "$prog: The configuration file has errors. Aborting...\n";
    exit 1;
}

# Testing if the configuration contains some certain items, aborting if not
do {
    my @nf;
    for my $ci (qw(DBSERVER DBPORT DBAUTH)) {
	push @nf, $ci unless (exists $CONF{$ci});
    }

    if (@nf) {
	print STDERR "$prog: Missing configuration items: ".
		     (join ' ', @nf)."\n";
    }
};

# Reading the authentication data (separate from the configuration in its own
# file which is referred by the 'DBAUTH' configuration item).
@CONF{qw(USERNAME PASSWORD)} = get_authdata ($CONF{DBAUTH});

# Opening the test data file / standard input for reading
if ($testdata eq '-') {
    open TD, '<&', \&STDIN;
} else {
    open TD, '<', $testdata or die "$prog: '$testdata' - $!\n";
}

# Reading the test data from the just opnened file
my ($modelid, %schema, %inst2db, %db2inst);
while (<TD>) {
    chomp; s/\r$//;
    if (/^MODELID=(\d+)$/) {
	if (defined $modelid) {
	    print STDERR "$prog: Ambiguous MODELID; skipping it.\n"; next;
	}
	$modelid = $1 + 0; next;
    }
    if (/^SCHEMA=([\w-]+)$/) {
	(exists $schema{$1}) or $schema{$1} = 1; next;
    }
    if (/^#(\d+)=(\d+)$/) {
	if (exists $inst2db{$1 + 0}) {
	    print STDERR "$prog: Ambiguous instance id: #$1; skipping it.\n";
	    next;
	}
	$inst2db{$1 + 0} = $2 + 0; $db2inst{$2 + 0} = $1 + 0; next;
    }
    print STDERR "$prog: Invalid data in input file '$testdata'.\n";
}
close TD;

# Test if the test data contains at least the 'MODELID' and an
# association '#<instance-id>=<dbid>' with the given instance id.
(defined $modelid) or die "$prog: No MODELID line found in input.\n";
(my $dbid = $inst2db{$instid}) or
    die "$prog: Instance id #$instid not found in input.\n";

# If the test data contains a 'SCHEMA', this schema is used; otherwise, the
# schema associated with the 'MODELID' is read from the 'IFCModel' database
my @schema = keys %schema;
unless (@schema) {
    my $dsn = sprintf 'DBI:MariaDB:database=IFCModel;host=%s',
		      $CONF{DBSERVER};
#    my $dsn = sprintf 'DBI:MariaDB:database=IFCModel;host=%s;port=%d',
#		      $CONF{DBSERVER}, $CONF{DBPORT};
    my $dbh = DBI->connect ($dsn, $CONF{USERNAME}, $CONF{PASSWORD}) or
	die "$prog: Connecting database 'IFCModel' on".
	    " '$CONF{DBSERVER}:$CONF{DBPORT}' failed\n".
	    "    Error: $DBI::errstr\n";
    my $sth = $dbh->prepare ("SELECT schema FROM FileSchema WHERE ModelId = ?".
			     " AND seq = 1");
    $sth->execute ($modelid);
    while (my $row = $sth->fetchrow_hashref()) {
	$schema{$row->{schema}} = 1;
    }
    $sth->finish; @schema = keys %schema;
    unless (@schema) {
	my $error = $dbh->errstr;
	$dbh->disconnect;
	if ($error) {
	    die "$prog: Accessing 'IFCModel.FileSchema' failed - $error\n";
	}
	die "$prog: No schema for Model Id $modelid found.\n";
    }
    $dbh->disconnect;
}

# Test for inconsistencies.
if (@schema > 1) {
    die "$prog: WARNING! The given model uses more than one schema.\n".
	"  This indicates that the referenced model is not IFC.\n";
}

# The "lower cased" schema name is the name of the database to be accessed
(my $schema) = @schema;
$CONF{DBNAME} = lc $schema;
my %derivedpositions = %{$DerivedAttributes{$schema}};

# The database type is 'MariaDB' (MySQL or MariaDB database)
my $dsn = sprintf 'DBI:MariaDB:database=%s;host=%s',
		  $CONF{DBNAME}, $CONF{DBSERVER};
#my $dsn = sprintf 'DBI:MariaDB:database=%s;host=%s;port=%d',
#		  $CONF{DBNAME}, $CONF{DBSERVER}, $CONF{DBPORT};

# Connect the database
my $dbh = DBI->connect ($dsn, $CONF{USERNAME}, $CONF{PASSWORD}) or
    die "$prog: Connecting database '$CONF{DBNAME}' on".
	" '$CONF{DBSERVER}:$CONF{DBPORT}' failed\n".
	"    Error: $DBI::errstr\n";

# Processing queue
#my (@PQUEUE, %PQHAS);

# The following array/hash combination is used for dynamically collecting the
# database ids of the instances to be processed. Each instance reference found
# is inserted at the end of the array (but only if it was not already inserted
# - this is the reason for the hash). The referenced instances are processed
# from front to end, starting with the DB-id of the instance selected by the
# command line argument. The ids created in the output are the DB-ids of the
# referenced instances, not the original instance ids.
my (@INSTANCES, %INSTANCES, %OBJMETA, %NUMBER);

push @INSTANCES, $dbid; $INSTANCES{$dbid} = 1;

my @res;
while (@INSTANCES) {
    my $id = shift @INSTANCES;
    my $instance = get_instance ($dbh, $modelid, $id);
    push @res, [$id, $instance];
}
@res = sort { $a->[0] <=> $b->[0] } @res;
for (@res) {
    my ($id, $instance) = @$_;
    print_instance ($id, $instance);
}

exit 0;

sub usage (@) {
    if (@_) { print STDERR "$prog: ".(join '', @_)."\n"; exit 64; }
    print "Usage: $prog [configfile] testdata instance-id\n";
    exit 0;
}

sub dirname ($) {
    (local $_) = @_;
    s|[^/]+$||; s|/$||; $_ ||= '.';  s|^\.(/.*)?$|$cwd$1|;
    $_
}

sub tmpl_subst ($$%) {
    (local $_, my $keep_ne, my %vars) = @_;
    my $res = '';
    while (/(%(?:%|[[:alpha:]]\w*\b|\{[[:alpha:]]\w*\}))/) {
	$res .= $`; $_ = $'; my $ph = $1;
	if ($ph eq '%') { $res .= $ph; next; }
	(my $v = $ph) =~ s/^%\{?([^}]+)\}?/$1/;
	if (exists $vars{$v}) { $res .= $vars{$v}; next; }
	$res .= $ph if ($keep_ne);
    }
    $res .= $_;
    $res
}

sub get_objtype ($$$) {
    my ($DBH, $modelid, $objid) = @_;
    unless (exists $OBJMETA{$objid}) {
	my $sth = $DBH->prepare
	    ("SELECT * FROM ObjType WHERE modelid = ? AND objid = ?");
	$sth->execute ($modelid + 0, $objid + 0) or
	    die "$prog: database query failed - $DBH->errstr\n";
	my $row = $sth->fetchrow_hashref;
	unless ($row) {
	    $sth->finish;
	    die "$prog: No object found for ($modelid, $objid) in database.\n";
	}
	$OBJMETA{$objid} = { %{$row} };
	$sth->finish;
    }
    $OBJMETA{$objid};
}
	
sub get_columninfo ($$;$);
sub get_attrval ($$$$$$);

sub get_instance ($$$) {
    my ($DBH, $modid, $id) = @_;
    my $objmeta = get_objtype ($DBH, $modid, $id); # Metadata of the INSTANCE
    my ($typename, $typeclass) = @{$objmeta}{qw(typename typeclass)};
    ($typeclass eq 'entity') or
	die "$prog: Object ($modid, $id) is no entity instance.\n";
    my @tblinfo = get_columninfo ($DBH, $typename);
    my $sth = $DBH->prepare
	("SELECT * FROM $typename WHERE modelid = ? AND objid = ?");
    $sth->execute ($modid, $id) or
	die "$prog: database query failed - $DBH->errstr\n";
    my @names = @{$sth->{NAME}};
    my @types = map { (my $t) = @$_; $t } @tblinfo;
    my @sizes = map { my (undef, $s) = @$_; $s } @tblinfo;
    my @rrow;
    if (my $row = $sth->fetchrow_hashref) {
	for my $ix (0 .. $#names) {
	    local $_ = $names[$ix];
	    next if (/^(?:model|obj)id$/);
	    my ($t, $sz) = ($types[$ix], $sizes[$ix]);
	    my $val = $row->{$_};
	    push @rrow, get_attrval ($DBH, $_, $modid, $val, $t, $sz);
	}
    }
    [ $id, $typename, @rrow ]
###
}

sub get_enumval ($$);
sub get_number ($$$);
sub get_selectval ($$$$$);
sub get_aggval ($$$$$);
sub get_simpleval ($$$$);

sub get_attrval ($$$$$$) {
    my ($DBH, $name, $modid, $val, $type, $size) = @_;
    unless (defined $val) {
	'$'
    } elsif ($type eq 'ENUMID') {
	# Get an ENUMERATION value
	(my $enumval) = get_enumval ($DBH, $val);
	$enumval
    } elsif ($type eq 'NUMBER') {
	# Get a NUMBER value (either INTEGER or REAL)
	get_number ($DBH, $modid, $val)
    } elsif ($type eq 'REFERENCE') {
	# Get a reference.
	# This can be an instance or a SELECT value
	# Getting the data from 'ObjType' ...
	my $objmeta = get_objtype ($DBH, $modid, $val);
	if ($objmeta->{typeclass} eq 'select') {
	    # The reference points to a SELECT value. This means that the
	    # value behind this reference (the current instance) of the SELECT
	    # object must be retrieved.
	    my %objmeta = %$objmeta;
	    my ($tbl, $rtid, $rtype) = @objmeta{qw(typename ctypeid ctypename)};
	    get_selectval ($DBH, $tbl, $rtype, $modid, $val)
	} else {
	    # The reference points to an ENTITY instance. This means that the
	    # instance id is returned here.
	    unless (exists $INSTANCES{$val + 0}) {
		push @INSTANCES, $val + 0;
		$INSTANCES{$val + 0} = $#INSTANCES;
	    }
	    (sprintf '#%d', $val) # (sprintf '#%d', $db2inst{$val + 0})
	}
    } elsif ($type eq 'AGGREGATE') {
	#0# # Need the table name of the multi-value table here!
	# A value of type AGGREGATE is a reference to an AGGREGATE, meaning:
	# The corresponding values must be retrieved from the corresponding
	# '_MV' table and an aggregate value constructed from them.
	my $objmeta = get_objtype ($DBH, $modid, $val);
	my %objmeta = %$objmeta;
	my ($tbl, $rtid, $rtype) =
	    @objmeta{qw(typename ctypeid ctypename)};
	get_aggval ($DBH, $tbl, $rtype, $modid, $val)
    } else {
	# A simple value is constructed by converting the value retrieved from
	# the database into a valid IFC-value.
	get_simpleval ($DBH, $val, $type, $size)
    }
}

sub get_enumval ($$) {
    my ($DBH, $enumid) = @_;
    unless (exists $ENUMVALS{$enumid}) {
	my $sth = $DBH->prepare ("SELECT * FROM Enums WHERE enumval = ?");
	(defined $sth) or
	    die "$prog: $DBH->errstr\n";
	$sth->execute ($enumid);
	my $row = $sth->fetchrow_hashref or
	    die "$prog: No ENUMERATION value for ($enumid) found.\n";
	$ENUMVALS{$enumid} = [ ".$row->{name}.", uc $row->{type} ];
	$sth->finish;
    }
    return unless (exists $ENUMVALS{$enumid});
    @{$ENUMVALS{$enumid}}
}

sub get_number ($$$) {
    my ($DBH, $modid, $numid) = @_;
    unless (exists $NUMBER{$numid}) {
	my $sth = $DBH->prepare
	    ("SELECT * from NUMBER WHERE modelid = ? AND objid = ?");
	$sth->execute ($modid, $numid) or
	    die "$prog: database query failed - $DBH->errstr\n";
	my %row = %{$sth->fetchrow_hashref} or
	    die "$prog: No NUMBER value found for ($modid, $numid).\n";
	$sth->finish;
	my $val;
	if ($row{is_real}) {
	    $val = $row{realval};
	    $val .= '.' unless ($val =~ /[.Ee]/);
	} else {
	    $val = $row{intval};
	}
	$NUMBER{$numid} = $val;
    }
    $NUMBER{$numid}
}

sub get_selectval ($$$$$) {
    my ($DBH, $tbl, $rtype, $modid, $objid) = @_;
    my @tblinfo = get_columninfo ($DBH, $tbl);
    my $sth = $DBH->prepare
	("SELECT * FROM $tbl WHERE modelid = ? AND objid = ?");
    $sth->execute ($modid, $objid) or
	die "$prog: Database query failed - $DBH->errstr\n";
    my @names = @{$sth->{NAME}};
    my @types = map { (my $t) = @$_; $t } @tblinfo;
    my @sizes = map { my (undef, $s) = @$_; $s } @tblinfo;
    my $row = $sth->fetchrow_arrayref or
	die "$prog: No SELECT value found for ($modid, $objid).\n";
    my $fieldno = $row->[2] + 3;
    my $name = $names[$fieldno];
    my $type = $types[$fieldno];
    my $size = $types[$fieldno];
    my $val = $row->[$fieldno];
    $sth->finish;
    unless (defined $val) {
	'$'
    } elsif ($type eq 'ENUMID') {
	my ($enumval, $enumtype) = get_enumval ($DBH, $val);
	(sprintf '%s(%s)', $enumtype, $enumval)
    } elsif ($type eq 'NUMBER') {
	(sprintf '%s(%s)', $rtype, get_number ($DBH, $modid, $val))
    } elsif ($type eq 'REFERENCE') {
	# The value of the SELECT-type is an ENTITY. It can't be a SELECT typed
	# value, because all SELECT-types were "flattened" during generating
	# the database scheme and the insertion of the corresponding values
	# into the database.
	push @INSTANCES, $val; $INSTANCES{$val} = $#INSTANCES;
	(sprintf '#%d', $val)
    } elsif ($type eq 'AGGREGATE') {
	# Should never occur! If my assumption is wrong here, this part may
	# be extended by getting an aggregate value and then enclosing it
	# into '<concrete type>(' and ')'.
	die "$prog: No aggregate (currently) allowed as SELECT value.\n";
    } else {
	# The value is a "simple type", meaning: one of BOOLEAN, LOGICAL,
	# INTEGER, REAL, BINARY or STRING. The conversion of the value into
	# the corresponding STEP representation must be enclosed into
	# '<concrete type>(' and ')'
	(sprintf '%s(%s)', $rtype, get_simpleval ($DBH, $val, $type, $size))
    }
}

sub get_aggval ($$$$$) {
    my ($DBH, $tbl, $rtype, $modid, $objid) = @_;
    return '()' if ($objid == 0);
    my @colinfo = get_columninfo ($DBH, $tbl);
    my $sth = $DBH->prepare
	("SELECT * FROM $tbl WHERE modelid = ? AND objid = ? ORDER by ix");
    $sth->execute ($modid, $objid) or
	die "$prog: Database query failed - $DBH->errstr\n";
    my @names = @{$sth->{NAME}};
    my @types = map { (my $t) = @$_; $t } @colinfo;
    my @sizes = map { my (undef, $s) = @$_; $s } @colinfo;
    my %index; for (0..$#names) { $index{$names[$_]} = $_; }
    my @rrow;
    my $sax = $index{sa_id};
    my $type = $types[$#types];		# The "value" attribute is the last one
    my $size = $sizes[$#sizes];		# The "value" attribute is the last one
    my $name = $names[$#names];		# The "value" attribute is the last one
    while (my $row = $sth->fetchrow_arrayref) {
	my $said = $row->[$sax];
	if (defined $said) {
	    push @rrow, get_aggval ($DBH, $tbl, $rtype, $modid, $said);
	} else {
	    my $val = $row->[$#$row];
	    push @rrow, get_attrval ($DBH, $name, $modid, $val, $type, $size);
	}
    }
    $sth->finish;
    (sprintf '(%s)', (join ',', @rrow))
}

sub get_simpleval ($$$$) {
    (my ($DBH, $val), local $_, my $size) = @_;
    return '$' unless (defined $val);
    if (/^(?:FLOAT|DOUBLE(?:\s+PRECISION)?)$/) {
	($val =~ /[.Ee]/ ? $val : $val . '.')
    } elsif (/^(?:(?:VAR)?CHAR|(?:LONG|MEDIUM|TINY)?TEXT)$/) {
	$val =~ s/'/''/g;
	"'$val'"
    } elsif (/^TINYINT$/) {
	if ($size == 1) {
	    ($val == 0 ? '.F.' : ($val == '1' ? '.T.' : '.U.'))
	} else {
	    $val
	}
    } elsif (/^(?:LONG|MEDIUM|TINY)?INT(?:EGER)?$/) {
	$val
    } elsif (/^(?:(?:VAR)?BINARY|(?:LONG|MEDIUM|TINY)?BLOB)$/) {
	(sprintf '"%s"', $val)
    } elsif (/^BIT$/) {
	$val = ord $val;
	($val == 0 ? '.F.' : ($val == 1 ? '.T.' : '.U.'))
    } else {
	die "$prog: Unable to convert unknown value type '$_'.\n";
    }
}

sub dbtype2ifc ($$) {
    (local $_, my $colsize) = @_;
    if ($_ eq 'FLOAT' || $_ eq 'DOUBLE') {
	'REAL'
    } elsif (/^(?:VAR)?CHAR$/) {
	'STRING' # . " [$colsize]" . ($_ eq 'CHAR' ? ' FIXED' : '')
    } elsif (/^(?:LONG|MEDIUM|TINY)?TEXT$/) {
	'STRING'
    } elsif (/^(?:LONG|MEDIUM|TINY)?INT(?:EGER)?$/) {
	'INTEGER'
    } elsif ($_ eq 'TINYINT') {
	($colsize > 1 ? 'INTEGER' : 'BOOLEAN')
    } elsif ($_ eq 'BIT') {
	($colsize == 1 ? 'BOOLEAN' : 'LOGICAL')
    } elsif (/^(?:LONG|MEDIUM|TINY)?BLOB$/) {
	'BINARY'
    } else {
	return;
    }
}

sub get_columnmeta ($$;$) {
    my ($DBH, $table, $column) = @_;
    unless (exists $DB_TABLE_SCHEMA{$table}) {
	state $attrs = {
	    DATA_TYPE => 1, TYPE_NAME => 1, NULLABLE => 1, COLUMN_NAME => 1,
	    COLUMN_SIZE => 1, ORDINAL_POSITION => 1,
	};
	my $sth = $DBH->column_info (undef, undef, $table, '%') or
	    die "$prog: 'DBI::column_info()' failed - $DBH->errstr\n";
	my $rows = $sth->fetchall_arrayref ($attrs);
	my %row;
	for my $row (@$rows) {
	    my $colname = $row->{COLUMN_NAME};
	    my $ifctype = dbtype2ifc ($row->{TYPE_NAME}, $row->{COLUMN_SIZE});
	    $row{$colname} = {
		TYPEID => $row->{DATA_TYPE}, OPTIONAL => $row->{NULLABLE},
		INDEX => $row->{ORDINAL_POSITION} - 1,
		TYPE => $row->{TYPE_NAME}, SIZE => $row->{COLUMN_SIZE},
		IFCTYPE => $ifctype, NAME => $colname
	    }
	}
	$DB_TABLE_SCHEMA{$table} = { %row };
    }
    if (defined $column) {
	return unless (exists $DB_TABLE_SCHEMA{$table}{$column});
	%{$DB_TABLE_SCHEMA{$table}{$column}}
    } else {
	return unless (exists $DB_TABLE_SCHEMA{$table});
	%{$DB_TABLE_SCHEMA{$table}}
    }
}

sub get_tso ($$) {
    (local $_, my $cinfo) = @_;
    my %cinfo = %$cinfo;
    if (/_enum$/) {
	# This is an ENUMERATION attribute
	['ENUMID', @cinfo{qw(SIZE OPTIONAL)}]
    } elsif (/_num$/) {
	# This is a NUMBER attribute
	['NUMBER', 0, $cinfo{OPTIONAL}]
    } elsif (/_id$/) {
	# This is a REFERENCE to either an ENTITY instance or a SELECT value
	['REFERENCE', @cinfo{qw(SIZE OPTIONAL)}];
    } elsif (/_mvid$/) {
	# This is an AGGREGATE attribute (a reference to an '_MV' table).
	['AGGREGATE', @cinfo{qw(SIZE OPTIONAL)}];
    } else {
	# This is a "simple type". The type returned here is already the
	# EXPRESS data type of the IFC; the invocation of 'dbtype2ifc()' in
	# 'get_columnmeta()' whose result is used here, ensures this.
	[@cinfo{qw(TYPE SIZE OPTIONAL)}];
    }
}

sub get_columninfo ($$;$) {
    my ($DBH, $table, $column) = @_;
    my %tinfo = get_columnmeta ($DBH, $table, $column) or
	die "$prog: Getting table info for '$table' failed - $DBH->errstr\n";
    unless (defined $column) {
	my @colnames = keys %tinfo;
	my @res; $#res = $#colnames;
	for my $colname (@colnames) {
	    my $cinfo = $tinfo{$colname};
	    my $ix = $cinfo->{INDEX};
	    $res[$ix] = get_tso ($colname, $cinfo);
	}
	@res
    } else {
	get_tso ($column, \%tinfo)
    }
}

sub print_instance ($$) {
    my ($id, $instance) = @_;
    my (undef, $name, @attrs) = @$instance;
    my @newattrs;
    if (exists $derivedpositions{$name}) {
	my ($ix, $jx) = (0, 0);
	my @pos = @{$derivedpositions{$name}};
	while ($ix < @attrs) {
	    while (grep { $_ eq $jx } @pos) {
		push @newattrs, '*'; ++$jx;
	    }
	    push @newattrs, $attrs[$ix++]; ++$jx;
	}
    } else {
	@newattrs = @attrs;
    }
    printf "#%d= %s(%s)\n", $id, $name, (join ',', @newattrs);
}
