#! /usr/bin/perl
# rctest.pl
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# License: MIT License
#
# Testing 'readconf.pm'

use strict;
use FindBin;
use lib "$FindBin::Bin";

use readconf qw(readconf);

my @x = readconf ('test');

exit 0;


