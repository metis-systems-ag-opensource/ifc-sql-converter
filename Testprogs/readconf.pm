# readconf.pm
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@pps-ag.com
# Copyright: (c) 2020, PPS AG
# 
#
# A module for reading configuration files like the ones being used by
# the EXPRESS and STEP parsrs.

package readconf;
use v5.26;
use strict;

our $RCSID = q$Id$;
our $VERSION = '0.01';

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = ();
our @EXPORT_OK = qw(readconf get_confpath get_authdata);
our %EXPORT_TAGS = (all => [ @EXPORT, @EXPORT_OK ]);

use Carp;
use Scalar::Util qw(refaddr openhandle);
use Symbol qw(geniosym);
use feature 'state';
use feature 'lexical_subs';

use Cwd;

my sub pp() {
    (my $p = $0) =~ s(.*/)();
    (my $pp = $0) =~ s(\Q$p\E$)();
    $pp =~ s((.)/+$)($1);
    if (length $pp == 0) {
	$pp = cwd;
    } elsif ($pp !~ m(^/)) {
	$pp = cwd . ($pp =~ /^\.$/ ? '' : ($pp ? '/' : '') . $pp);
    }
    ($p, $pp)
}

my sub get_progdir() {
    (undef, my $pp) = pp();
    $pp
}

my sub get_home() {
    (my $prog) = pp();
    state $home = $ENV{HOME} // (getpwuid $>)[7] or
        die "$prog: No HOME directory found!\n";
    $home
}

sub get_confpath ($;$) {
    my ($cfgname, $keep) = @_;
    state $confpath = [
	"%progpath/%name.conf",
	"%userconfig/ifc/%name.conf",
	"%userconfig/%name.conf",
	"%home/.%{name}rc",
    ];
    my $home = get_home();
    my $progdir = get_progdir();
    my %vars = ( progpath => $progdir, home => $home,
		 userconfig => "$home/.config", name => $cfgname );
    for $_ (@$confpath) {
	(my $p = $_) =~  s((?|(%(\p{L}\w*))|(%\{(\p{L}[0-9A-Za-z_]*)\})))
			     ($vars{$2} // ($keep ? $1 : ""))ge;
	if (-f $p) { return $p; }
    }
    return;
}

# The 'readconf()' subroutine does (for the most part) the same as an instance
# of the 'Config' class ('Common/config.{cc,h}'), but in a Perl-specific way.
# Even the substitution controlling pragmas and the substitutions theirselves
# are performed in (nearly) the same manner. Result is the error counter and
# the resulting hash (flattened into a list).
sub readconf ($) {
    (my $cfgname) = @_;
    my ($prog, $ppath) = pp();
    my $cfgfile = get_confpath ($cfgname) or
        die "$prog: No configuration file found.\n";
    (my $cfname = $cfgfile) =~ s(.*/)();
    (my $cfdir = $cfgfile) =~ s(/$cfname$)();
    local $_;
    my %res;
    my $home = get_home();
    state $confvars = {
        cfdir => $cfdir,
        progdir => $ppath,
        home => $home,
        userconfig => "$home/.config",
        sysconfig => "/etc"
    };
    my %CFV = %{$confvars};
    open CFG, '<', $cfgfile or
        die "$prog: Reading '$cfgfile' failed - !$\n";
    my ($subst, $keep) = (undef, 1);
    my ($ec, $lc) = (0, 0);
    while (<CFG>) {
        chomp; s/\r$//; ++$lc;
        if (/^(?|#substitute on|#s:on)/) {
            $subst = 1; $_ = $';
            if (/^(?|,unknown (keep|remove)|,u:(keep|remove))/) {
                $keep = ($1 eq 'keep'); $_ = $';
            }
            next;
        }
        if (/^(?|#substitute off|#s:off)/) {
            ($subst, $keep) = (0, 1); next;
        }
        next if (/^\s*(?:#.*)?$/);
        if (/^\s*(\p{L}\w*)\s*=\s*/) {
            (my $item, $_) = ($1, $');
            s/\s+$//;
            unless (length $_ > 0) {
                print STDERR "$cfname($lc): Missing configuration value.\n";
                ++$ec; next;
            }
            if (exists $res{$item}) {
                print STDERR "$cfname($lc): '$item' already defined.\n";
                ++$ec; next;
            }
            if ($subst) {
                # Perl magic!
                s((?|(%(\p{L}\w*))|(%\{(\p{L}[0-9A-Za-z_]*)\})))
                 ($CFV{$2} // ($keep ? $1 : ""))ge;
            }
            $res{$item} = $_;
            next;
        }
        print STDERR "$cfname($lc); Syntax error.\n"; ++$ec;
    }
    close CFG;
    +($ec, %res)
}

# Get the authentication/authorisation data from a configuration file. I wrote
# this subroutine, because it is never a good idea to store this data directly
# into the program – for two reasons:
#   1. Security (this should say all),
#   2. If the authentication/authorisation data changes, the program itself
#      need not to be touched.
sub get_authdata ($) {
    (my $p) = @_;
    (my $prog) = pp();
    (defined $p) or die "$prog: No authentication data found!\n";
    open my $fh, '<', $p or
	die "$prog: Error reading authentication data - $!\n";
    my ($u, $p) = map { chomp; s/\r$//; $_ } <$fh>;
    close $fh;
    (defined $u && defined $p) or
	die "$prog: Authentication data incomplete.\n";
    ($u, $p)
}

# Perl needs a 'true' value at the end of a package for successful parsing it.
1;
