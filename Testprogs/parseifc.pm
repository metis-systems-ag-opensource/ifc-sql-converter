# parseifc.pm
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@pps-ag.com
# Copyright: (c) 2020, PPS AG
# 
#
# (Very simple) IFC-parser.
# Only the subroutine 'parse_ifcline()' is exported here.
# contain the 'GlobalId' attribute.

package parseifc;
use v5.26;
use strict;

our $RCSID = q$Id$;
our $VERSION = '0.01';
our $DEBUG = 0;

use Carp;
use Scalar::Util qw(refaddr openhandle);
use Symbol qw(geniosym);
use feature 'state';
use feature 'lexical_subs';

#use Data::Dumper;	##DEBUG

our @ISA = ();

my (%lc, %filename, %fh);		# line number, file name, file handle
my (%datasections, %headersection);	# data dections header section
my (%schema, %schemalist);		# schema list from the FILE_SCHEMA ent.
my (%headercb, %datacb);		# header/data section callbacks
my (%extradata);			# Extra data to be used by the callbacks

my $defheadercb = sub { croak "HEADER section callback not defined"; };
my $defdatacb = sub { croak "DATA section callback not defined"; };

my sub ident { refaddr (shift) }

sub new {
    my $class = shift;
    my $self = bless \do { my $anon_scalar; }, $class;
    my $oid = ident ($self);
    $lc{$oid} = 0;
    if (@_) {
	local $_ = shift;
	($filename{$oid}, $fh{$oid}) =
	    (openhandle $_ ? (undef, $_) : ($_, undef));
    } else {
	$filename{$oid};
	$fh{$oid};
    }
    $datasections{$oid} = [];
    $headersection{$oid} = undef;
    $schema{$oid} = undef; $schemalist{$oid} = [];
    $headercb{$oid} = $defheadercb;
    $datacb{$oid} = $defdatacb;
    $extradata{$oid} = {};
    if (@_) {
	local $_ = shift;
	unless (defined $_) {
	    $headercb{$oid} = $defheadercb;
	} elsif (ref $_ eq 'CODE') {
	    $headercb{$oid} = $_;
	} else {
	    croak "The HEADER section callback must be a subroutine reference".
		  " or 'undef'."
	}
    }
    if (@_) {
	local $_ = shift;
	unless (defined $_) {
	    $datacb{$oid} = $defdatacb;
	} elsif (ref $_ eq 'CODE') {
	    $datacb{$oid} = $_;
	} else {
	    croak "The DATA section callback must be a subroutine reference".
		  " or 'undef'.";
	}
    }
    $self
}

sub DESTROY {
    my $self = shift;
    my $oid = ident ($self);
    delete $lc{$oid}; delete $filename{$oid};
    close $fh{$oid} if (openhandle $fh{$oid});
    delete $fh{$oid};
    delete $datasections{$oid}; delete $headersection{$oid};
    delete $schema{$oid}; delete $schemalist{$oid};
    delete $headercb{$oid}; delete $datacb{$oid};
    delete $extradata{$oid};
}

sub headercb {
    my $self = shift;
    my $oid = ident ($self);
    my $oldcb = $headercb{$oid};
    if (@_) {
	local $_ = shift;
	unless (ref $_ eq 'CODE') {
	    croak "The HEADER section callback must be a subroutine reference";
	}
	$headercb{$oid} = $_;
    }
    $oldcb
}

sub datacb {
    my $self = shift;
    my $oid = ident ($self);
    my $oldcb = $datacb{$oid};
    if (@_) {
	local $_ = shift;
	unless (ref $_ eq 'CODE') {
	    croak "The HEADER section callback must be a subroutine reference";
	}
	$datacb{$oid} = $_;
    }
    $oldcb
}

sub filename {
    my $self = shift;
    my $oid = ident ($self);
    my $oldfn = $filename{$oid};
    if (@_) { $filename{$oid} = shift; }
    $oldfn
}

sub open {
    my $self = shift;
    my $oid = ident ($self);
    if (openhandle $fh{$oid}) { return 1; }
    if (@_) {
	no strict 'refs';
	local $_ = $_[0];
	if (openhandle ($_)) {
	    $fh{$oid} = shift;
	    $filename{$oid} = undef;
	} else {
	    my $filename = shift;
	    (defined $filename) or croak "Filename/-handle parameter undefined";
	    my $iosym = geniosym();
	    open $iosym, '<', $filename or return;
		##alt: croak "Attempt to open '$filename' failed - $!";
	    $filename{$oid} = $filename;
	    $fh{$oid} = $iosym;
	}
    } else {
	my $iosym = geniosym();
	open $iosym, '<', $filename{$oid} or return;
	    ##alt: croak "Attempt to open '$filename{$id}' failed - $!";
	$fh{$oid} = $iosym;
    }
    1
}

sub close {
    my $self = shift;
    my $oid = ident ($self);
    if ($fh{$oid}) {
	close $fh{$oid} if (openhandle $fh{$oid});
	$fh{$oid} = undef;
    }
    $lc{$oid} = 0;
    return;	# Force 'undef' or '()' being returned.
}

my sub parse_ifcfile ($*);

sub parse {
    my $self = shift;
    my $oid = ident ($self);
    my $needclose = ! openhandle $fh{$oid};
    unless ($self->open()) {
	unless (defined $filename{$oid}) {
	    croak "No open file handle and no filename defined for parsing";
	}
	croak "Attempt to open '$filename{$oid}' failed - $!";
    }
    my $errs = parse_ifcfile ($oid, $fh{$oid});
    if ($needclose) { $self->close; }
    if ($errs) {
	print STDERR "$errs errors found during parsing\n";
    }
    $errs;
}

# Return the extra data generated/modified by the callback subroutines.
sub extra {
    my $self = shift;
    my $oid = ident ($self);
    unless (@_) { croak "extra(): Missing key argument"; }
    my $k = shift;
    return unless (exists $extradata{$oid}{$k});
    $extradata{$oid}{$k}
}

# Return the keys of all entries of '$extradata{<obj-id>}'
sub extra_keys {
    my $self = shift;
    my $oid = ident ($self);
    keys %{$extradata{$oid}}
}

sub set_extra {
    my $self = shift;
    my $oid = ident ($self);
    unless (@_ >= 2) { croak "set_extra(): Missing argument(s)"; }
    my ($k, $v) = @_;
    $extradata{$oid}{$k} = $v;
}

sub schema {
    my $self = shift;
    my $oid = ident ($self);
    $schema{$oid}
}

# Here follow the definitions/imports/... which are really part of the module
# – not part of the module specification.
use experimental 'switch';


# Write syntax errors (as 'carp()'-warnings).
sub perr ($@) {
    my ($lc, @msg) = @_;
    my $msg = sprintf "%d: %s\n", $lc, (join '', @msg);
    print STDERR "$msg\n";
}

# Some helper functions for an easier access to the parsed attributes, which
# are returned as lists of structures of the format '{ tag => value }' (with
# the exceptions of lists which are returned as '[ value, ... ]').

# The first two functions test for a value being a list of values.
my sub is_array ($) { (local $_) = @_; (ref $_) eq 'ARRAY' }
my sub no_array ($) { (local $_) = @_; (ref $_) ne 'ARRAY' }

# The only function for testing another type here is the test for a value
# being a string.
my sub is_string ($) {
    (local $_) = @_; (ref $_ eq 'HASH') && (exists $_->{string})
}

sub typeof {
    my $self = shift;
    my $oid = ident ($self);
    (@_) or croak "typeof(): Missing an argument.";
    local $_ = shift;
    if (ref $_ eq 'HASH') {
	my @k = keys %{$_};
	(@k == 1) or croak "typeof(): No valid attribute value.";
	$_ = shift @k;
    } elsif (ref $_ eq 'ARRAY') {
	$_ = 'aggregate';
    } else {
	$_ = undef; croak "typeof(): Invalid attribute value '$_'.";
    }
    $_
}

# 'valof()' de-constructs any '{ tag => value }' and returns the corresponding
# 'value'. In list values each element is deconstructed in the same manner as
# is used for simple values.
# 
sub valof ($;$) {
    if (@_ > 1) {
	# Don't need the object handle (except for the outside invocation).
	shift;
    }
    local $_ = shift;
    if (is_array ($_)) {
	my @rr;
	for my $x (@$_) {
	    my $v = valof ($x);
	    push @rr, $v;
	}
	return [ @rr ];
    }
    return unless (ref $_ eq 'HASH');
    for my $tag (qw(logical enum integer real string binary)) {
	return $_->{$tag} if (exists $_->{$tag});
    }
    if (exists $_->{typeval}) {
	my ($w, $sp) = @{$_->{typeval}};
	my $sv = valof ($sp->[0]);
	return +({ $w => $sv });
    }
    return if (exists $_->{empty} || exists $_->{unspecified});
    return $_->{instid} if (exists $_->{instid});
    if (exists $_->{subpar}) {
    }
    return;
}

my sub get_attributes ($$\@@) {
    my ($lc, $entname, $rparams, @an) = @_;
    my %res;
    my ($ec, $ix) = (0, 0);
    while (@an) {
	unless (@$rparams) {
	    perr $lc, "Missing attributes of '$entname'";
	    ++$ec; last;
	}
	++$ix;
	my $k = shift @an; my $v = shift @$rparams;
	# Double aggregate value (used in the list of extended identifiers in
	# the 'SCHEMA_POPULATION' header.
	if (($k =~ s/^\@\@//)) {
	    my $ok = (is_array $v ? 1 : undef);
	    if ($ok) {
		for my $sv (@$v) {
		    unless (is_array $sv) { $ok = undef; last; }
		}
	    }
	    unless ($ok) {
		perr $lc, "Aggregate of aggregates expected for attribute".
			  " $ix of '$entname'";
		$res{$k} = [];
	    }
	    $res{$k} = $v; next;
	}
	# Single aggregate value (used in 'FILE_DESCRIPTION', 'FILE_NAME',
	# 'FILE_SCHEMA', 'FILE_POPULATION', 'SECTION_CONTEXT' and
	# 'SECTION_LANGUAGE').
	if (($k =~ s/^\@//)) {
	    unless (is_array $v) {
		perr $lc, "Aggregate expected for attribute #$ix of '$entname'";
		$res{$k} = []; ++$ec; next;
	    }
	    $res{$k} = $v; next;
	}
	# Single aggregate value
	if (is_array $v) {
	    perr $lc, "Single value expected for attrinute #$ix of '$entname'";
	    $res{$k} = undef;  ++$ec; next;
	}
	$res{$k} = $v;
    }
    # Return the number of errors and the "list" of extracted values.
    +($ec, %res);
}

my %SYNBR = (
    'ISO-10303-21' => 1, 'END-ISO-10303-21', 'HEADER', 'ENDSEC', 'DATA'
);

my sub valid_synbr ($) { (local $_) = @_; +(exists $SYNBR{$_}) }

my %headerdescs = (
    FILE_DESCRIPTION => [ qw(@desc, ilevel) ],
    FILE_NAME => [ qw(name timestamp @authors @organizations preprocver
		      origsys authorization) ],
    FILE_SCHEMA => [ qw(@schemalist) ],
    SCHEMA_POPULATION => [ qw(@@extids) ],
    FILE_POPULATION => [ qw(govschema detmeth @govsections) ],
    SECTION_CONTEXT => [ qw(sectionname @contextidents) ],
    SECTION_LANGUAGE  => [ qw(sectionname defaultlang) ],
);

my sub valid_header ($) { (local $_) = @_; exists $headerdescs{$_} or /^!/ }

my sub parse_params ($$;$);

# Parsing a single "instance" (being either one of the syntactical elements
# like 'HEADER', 'DATA', ..., or a regular instance ('#<instid>=...').
# The return value is a tuple, consisting of:
#   - The remaining part of the line after parsing the "instance",
#   - the line number,
#   - the number of errors found during the parsing,
#   - the instance id (or 'undef' – for non-instance elements),
#   - the name of the entity or other syntactical element,
#   - the parameters of the "entity" (for header entities this is a HASH
#     reference, and for user defined entities or instances, this is an ARRAY
#     reference).
#
# Each of the elements of the tuple (save for the first three) can be empty.
# For regular syntactical elements, the tuple contains:
#   - the remaining part of the line,
#   - the line number,
#   - the number of errors,
#   - 'undef',
#   - the name of the syntactical element,
#   - nothing (same as 'undef').
#
# For header instances, 'undef' has the place for the instance-id.
# For regular instances, all elements are filled.
# For invalid instances, only the first four elements have values.
# For a completely wrong section, only the first three elements of the tuple
# have meaningful values.
my sub parse_ifcline ($$) {
    (local $_, my $lc) = @_;
    my (@res);
    state $headers = "(" . (join '|', (keys %headerdescs)) . ")";
    s/^\s+//; return unless (length $_);
    if (/^--/) { perr $lc, "Skipping comment line", $lc; return; }
    if (/^(END-ISO-10303-21) *;/) {
	# Seems the end of the IFC-file ...
	my $entname = $1; $_ = $'; #perr $lc, "IFC frame found";
	@res = ($_, $lc, 0, undef, $entname);
    } elsif (/^(ISO-10303-21) *;/) {
	# Seems a valid IFC-file (yet) ...
	my $entname = $1; $_ = $'; #perr $lc, "IFC frame found";
	@res = ($_, $lc, 0, undef, $entname);
    } elsif (/^(ENDSEC) *;/) {
	# Seems the end of a section (HEADER, DATA, ...) ...
	my $entname = $1; $_ = $'; #perr $lc, "End of a section reached";
	@res = ($_, $lc, 0, undef, $entname);
    } elsif (/^(HEADER) *;/) {
	# Seems the header section ...
	my $entname = $1; $_ = $'; #perr $lc, "IFC header found";
	@res = ($_, $lc, 0, undef, $entname);
    } elsif (/^(DATA)\b/) {
	# Seems the beginning of a DATA section ...
	my $entname = $1; $_ = $';
	my $ec = 0;
	my @params;
	s/^\s+//;
	if (/^\(/) {
	    ($_, my ($errs, @params)) = parse_params ($_, 1);
	    $ec += $errs;
	    if (/^\)\s*/) {
		$_ = $';
	    } else {
		perr $lc, "Missing ')'"; ++$ec;
	    }
	}
	if (/^;/) { $_ = $'; } else { perr $lc, "Missing ';'"; ++$ec; }
	if (@params) {
	    my ($errs, %params) =
		get_attributes ($lc, $entname, @params,
				    qw(sectionname, @schemaname));
	    $ec += $errs;
	    if (@{$params{schemaname}} != 1) {
		perr $lc, "A parametrised DATA section must have exactly one".
			  " schema name";
		++$ec;
	    }
	    @res = ($_, $lc, $ec, undef, $entname, { %params });
	} else {
	    @res = ($_, $lc, $ec, undef, $entname);
	}
    } elsif (/^$headers\b/o) {
	# One of the standard or extended instances of the HEADER section.
	my $entname = $1; $_ = $'; my $ec = 0;
	($_, my $errs, my @params) = parse_params ($_, $lc);
	$ec += $errs;
	if (/^\) *;/) { $_ = $'; } else { perr $lc, "Missing ');'"; ++$ec; }
	($errs, my %params) =
	    get_attributes ($lc, $entname, @params, @{$headerdescs{$entname}});
	@res = ($_, $lc, $errs + $ec, undef, $entname, { %params });
    } elsif (/^(![A-Z][0-9A-Z_]*)\b/) {
	# A user-defined entity instance in the header section.
	my $entname = $1; my $ec = 0;
	($_, my $errs, my @params) = parse_params ($', $lc);
	if (/^\) *;/) { $_ = $'; } else { perr $lc, "Missing ');'"; ++$ec; }
	@res = ($_, $lc, $ec + $errs, undef, $entname, [ @params ]);
    } elsif (/^\s*(#[0-9]+)\s*=\s*/) {
	my $instid = substr $1, 1; $_ = $';
	my $ec = 0;
	if (/^(!?[A-Z][0-9A-Z]*)\b/) {
	    my $name = $1; my $ec = 0;
	    ($_, my $errs, my @params) = parse_params ($', $lc, "$instid,$name");
	    if (/^\) *;/) { $_ = $'; } else { perr $lc, "Missing ');'"; ++$ec; }
	    @res = ($_, $lc, $ec + $errs, $instid, $name, [ @params ]);
	} elsif (/^\(/) {
	    ($_, my $errs, my @params) = parse_params ($_, $lc);
	    if (/^\) *;/) { $_ = $'; } else { perr $lc, "Missing ');'"; ++$ec; }
	    @res = ($_, $lc, $ec + $errs, $instid, { list => [ @params ] });
	} else {
	    perr $lc, "Invalid instance definition #$instid = ...";
	    @res = ($_, $lc, 1, $instid)
	}
    } else {
	perr $lc, "Syntax error!";
	@res = ($_, $lc, 1);
    }
    @res
}

sub parse_params ($$;$) {
    (local $_, my $lc, my $indata) = @_;
    my @params;
    my ($ec, $pc) = (0, 0);
    s/^\s+//;
    unless (/^\(/) {
       	perr $lc,  "Inserted missing '('\n"; ++$ec;
    } else {
	s/^.//;
    }
    ++$pc;
    while (length $_ > 0) {
	s/^\s+//;
	if (/^(\.[^.]+\.)/) {
	    my $item = $1; $_ = $';
	    # Prefix is an ENUM or a LOGICAL value
	    my $item = $1; $_ = $';
	    if ($item =~ /^\.[FTU]\.$/) {
		push @params, { logical => $item };
	    } elsif ($item =~ /^(\.[A-Z][0-9A-Z_]*\.)$/) {
		push @params, { enum => $item };
	    } else {
		perr $lc, "Invalid ENUMERATION/LOGICAL value '$item'"; ++$ec;
	    }
	} elsif (/^([+-]?[0-9]+(?:\.[0-9]*(?:E[+-]?[0-9]+)?)?)/) {
	    # Prefix is a NUMBER
	    my $item = $1; $_ = $';
	    if ($item =~ /\./) {
		push @params, { real => $item };
	    } else {
		push @params, { integer => $item };
	    }
	} elsif (/^('(?:[^']|'')*')/) {
	    # Prefix is a STRING value
	    my $item = $1; $_ = $';
	    $item =~ s/^'(.*)'$/$1/; $item =~ s/''/'/g;
	    push @params, { string => $item };
	} elsif (/^("[^"]*")/) {
	    # Prefix is a BINARY value
	    my $item = $1; $_ = $';
	    if ($item =~ /^"([0-3][0-9A-Z]*)"$/) {
		push @params, { binary => $1 };
	    } else {
		perr $lc, "Invalid binary value $item"; ++$ec;
	    }
	} elsif (/^([A-Z][0-9A-Z]*)\s*\(/) {
	    # Prefix is an 'IDENT(<parameters>)' construct, so the
	    # construct is decomposed into 'IDENT' and '(<parameters>)' and
	    # the parameters are then recursively parsed with this subroutine
	    my $w = $1; $_ = '('.$'; ++$pc;
	    ($_, my $errs, my @subpar) = parse_params ($_, $lc, $indata);
	    $ec += $errs;
	    if ($errs == 0 && @subpar != 1) {
		perr $lc, "Invalid number of attributes for typed value";
		++$ec;
	    }
	    unless (/^\)/) {
		perr $lc, "Inserted missing ')'\n"; ++$ec;
	    } else {
		s/^.//; --$pc;
	    }
	    push @params, { typeval => [ $w, @subpar ] };
	} elsif (/^\$/) {
	    # The parameter is an empty value, typically specified for an
	    # unset optional attribute
	    s/^.//; push @params, { empty => '$' };
	} elsif (/^\*/) {
	    # The parameter is the mark for a derived attribute (if this is not
	    # at the end of the attribute list of an ENTITY
	    s/^.//; push @params, { unspecified => '*' };
	} elsif (/^(#[0-9]+)/) {
	    # The parameter is a reference to another instance
	    push @params, { instid => (substr $1, 1) };
	    $_ = $';
	} elsif (/^\(\s*\)/) {
	    $_ = $'; push @params, [];
	} elsif (/^\(/) {
	    # The parameter is an anonymous list of values ...

	    ++$pc;
	    ($_, my $errs, my @subpar) = parse_params ($_, $lc, $indata);
	    $ec += $errs;
	    if (/^\)/) {
		s/^.//; --$pc;
	    } else {
		perr $lc, "Missing ')'\n"; ++$ec;
	    }
	    push @params, [ @subpar ];
	} else {
	    perr $lc, "Invalid syntactical prefix of '$_'"; ++$ec;
	    # Skip to the next (hopefully valid) syntactical prefix
	    s/^.*([,)])/$1/; if ($1 eq ')') { --$pc; }
	}
	s/^\s+//;
	if (/^\)/) {
	    if (--$pc < 0) {
		perr $lc, "Inserting missing ')'"; ++$ec;
		$_ = ')'.$_;
	    }
	    last;
	}
	if (/^,/) { s/^.//; } else { perr $lc, "Inserting missing ','"; ++$ec; }
    }
    +($_, $ec, @params)
}

my sub pserr ($$$$) {
    my ($lc, $id, $name, $errt) = @_;
    state $errs = {
	inst => "Instance (#%id) not allowed outside of a DATA section",
	hent => "Header entity (%name) not allowed outside of a HEADER section",
	syni => "Syntactical item (%name) not allowed here",
	invi => "Invalid/unknown syntactical item (%name)",
	nohd => "No HEADER section defined yet",
	nodt => "No DATA section defined",
	incf => "Incomplete IFC file",
    };
    my $err = $errs->{$errt} // "Unknown error";
    $err =~ s/(?:%(\w+))/{id => $id, name => $name}->{$1}/ge;
    perr $lc, $err;
}

my sub get_schemalist ($$$) {
    my ($oid, $lc, $slparam) = @_;
    if (ref $slparam ne 'HASH' || ! exists $slparam->{schemalist}
    ||  @{$slparam->{schemalist}} < 1) {
	perr $lc, "Invalid schemalist"; return;
    }
    my @res;
    my @sl = @{$slparam->{schemalist}};
    my $ix = 0;
    for my $slit (@sl) {
	++$ix;
	unless (is_string ($slit)) {
	    perr $lc, "Invalid schema list item #$ix"; return;
	}
	push @res, valof $slit;
    }
    $schemalist{$oid} = [ @res ];
    if (@{$schemalist{$oid}}) { ($schema{$oid}) = @{$schemalist{$oid}}; }
    1
}

my sub get_dataargs ($$$) {
    my ($oid, $lc, $val) = @_;
    local $_;
    my $dn = '_DEFAULT';
    my $sn = $schema{$oid};
    (defined $val) or $val = [];
    if (@$val) {
	# Non-empty argument of a DATA section
	if (@$val != 2) {
	    perr $lc,
		 "A DATA section header must have no or exact two paramters";
	} else {
	    my @dnsc = @$val;
	    if (is_string $dnsc[0]) {
		$dn = valof $dnsc[0];
	    }
	    shift @dnsc;
	    if (no_array $dnsc[0] || @{$dnsc[0]} < 1 || ! is_string $dnsc[0]) {
		perr $lc, "Invalid second DATA parameter";
		$sn = $schema{$oid};
	    } else {
		$sn = valof $dnsc[0];
	    }
	}
    }
    +($dn, $sn)
}

use constant {
    INITSTATE => 0, PREHEADERSTATE => 1, INHEADERSTATE => 2, PREDATASTATE => 3,
    INDATASTATE => 4, ENDDATASTATE => 5, ENDSTATE => 6
};

use constant {
    IFCBEGIN => 'ISO-10303-21', HEADER => 'HEADER', DATA => 'DATA',
    ENDSEC => 'ENDSEC', IFCEND => 'END-ISO-10303-21',
    FILE_SCHEMA => 'FILE_SCHEMA',
};

# Parse an (open) IFC file completely
sub parse_ifcfile ($*) {
    my ($oid, $fh) = @_;
    # '$id'   - "converted" object reference
    # '$fh'   - open file handle of an IFC file
    local $_;	# I probably need it somewhere
    my $headercb = $headercb{$oid};
    my $datacb = $datacb{$oid};
    my ($lc, $errs) = ($lc{$oid}, 0);
    my ($commentstate, $state, %IfcCTX);
    $state = 0;
    while (<$fh>) {
	chomp; s/\r$//; ++$lc;
	if ($commentstate) {
	    if (m(\*/)) {
		s(^.*\*/)(); $commentstate = 0;
		next if (/^\s*$/);
	    }
	    next;
	} elsif (m(/\*)) {
	    unless (s(/\*.*\*/)()) {
		s(/\*.*$)(); $commentstate = 1;
	    }
	    next if (/^\s*$/);
	}
	while (length $_ > 0) {
	    ($_, undef, my ($ec, $id, $name, $val)) = parse_ifcline ($_, $lc);
	    if ($ec > 0) { $errs += $ec; next; }
	    given ($state) {
		when (INITSTATE) {
		    if (defined $id) {
			pserr $lc, $id, $name, 'inst'; ++$errs;
		    } elsif ($name eq IFCBEGIN) {
			$state = PREHEADERSTATE;
		    } else {
			pserr $lc, $id, $name,
			      (valid_header $name ? 'hent' :
			       valid_synbr $name ? 'syni' : 'invi');
			++$errs;
		    }
		}
		when (PREHEADERSTATE) {
		    if (defined $id) {
			pserr $lc, $id, $name, 'inst'; ++$errs;
		    } elsif ($name eq HEADER) {
			$state = INHEADERSTATE;
		    } else {
			pserr $lc, $id, $name,
			      ($name eq DATA ? 'nohd' :
			       valid_header $name ? 'hent' :
			       valid_synbr $name ? 'syni' : 'invi');
			++$errs;
		    }
		}
		when (INHEADERSTATE) {
		    if (defined $id) {
			# Instance: '#<number>= ...'
			perr $lc, $id, $name, 'inst'; ++$errs;
		    } elsif ($name eq FILE_SCHEMA) {
			# 'FILE_SCHEMA((schemalist))' is handled internally,
			# because the object's 'schema' and 'schemalist'
			# attributes are modified here.
			get_schemalist ($oid, $lc, $val) or ++$errs;
		    } elsif ($name eq ENDSEC) {
			unless (@{$schemalist{$oid}}) {
			    perr $lc, "No schema names found"; ++$errs;
			}
			$state = PREDATASTATE;
		    } elsif (valid_header $name) {
			# All other HEADERs are subject of the header callback
			# – which doesn't know anything about the object, save
			# from the 'extradata' part.
			&$headercb ($extradata{$oid}, $lc, $name, $val) or
			    ++$errs;
		    } else {
			# Any other element found is an error here.
			pserr $lc, $id, $name,
			      (valid_synbr $name ? 'syni' : 'invi');
			++$errs;
		    }
		}
		when (PREDATASTATE) {
		    if (defined $id) {
			perr $lc, $id, $name, 'inst'; ++$errs;
		    } elsif ($name eq DATA) {
			$state = INDATASTATE;
			my ($dn, $sn) = get_dataargs ($oid, $lc, $val);
			unless (defined $sn) {
			    perr $lc,
				 "Can't handle a DATA section without a schema";
			    ++$errs;
			}
			push @{$datasections{$oid}},
			     { name => $dn, schema => $sn, instancecount => 0 };
		    } elsif ($name eq IFCEND) {
			unless (@{$datasections{$oid}}) {
			    pserr $lc, $id, $name, 'nodt'; ++$errs;
			}
			$state = ENDSTATE;
		    } else {
			pserr $lc, $id, $name,
			     (valid_header $name ? 'hent' :
			      valid_synbr $name ? 'syni' : 'invi');
			++$errs;
		    }
		}
		when (INDATASTATE) {
		    if (defined $id) {
			&$datacb ($extradata{$oid}, $lc, $id, $name, $val) or
			    ++$errs;
			my $currds = $#{$datasections{$oid}};
			++$datasections{$oid}[$currds]{instancecount};
		    } elsif ($name eq ENDSEC) {
			my $currds = $#{$datasections{$oid}};
			unless ($datasections{$oid}[$currds]{instancecount}) {
			    perr $lc, "DATA section contains no instances";
			    ++$errs;
			}
			$state = ENDDATASTATE;
			    # If one wants to parse more DATA sections,
			    # 'PREDATASTATE' must be used here
		    } else {
			pserr $lc, $id, $name,
			      (valid_header $name ? 'hent' :
			       valid_synbr $name ? 'syni' : 'invi');
			++$errs;
		    }
		}
		when (ENDDATASTATE) {
		    if (defined $id) {
			pserr $lc, $id, $name, 'inst'; ++$errs;
		    } elsif ($name eq IFCEND) {
			$state = ENDSTATE;
		    } else {
			pserr $lc, $id, $name,
			      (valid_header $name ? 'hent' :
			       valid_synbr $name ? 'syni' : 'invi');
			++$errs;
		    }
		}
		default: { }
	    }
	}
    }
    if ($state != ENDSTATE) {
	pserr $lc, 0, '', 'incf'; ++$errs;
    }
    $lc{$oid} = $lc;
    $errs
}

1;
