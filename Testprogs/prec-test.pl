#! /usr/bin/perl
# prec-test.pl
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# License: MIT License
#
# Small program for testing "integer" rounding

use strict;

sub usage (@);

(my $prog = $0) =~ s(.*/)();

if (@ARGV < 2) {
    print "Usage: $prog number precision\n"; exit 0;
}

(my $number = shift) =~ /^\d+$/ or
    usage "'number' must consist of digits.";

my $prec = shift; 
($prec =~ /^\d+$/ and $prec <= 9) or
    usage "'precision' ($prec) must be a number between 0 and 9.";

my $cc = $prec;
my $d = 0;
if ($cc < 9) {
    my $mx = 5;
    while (++$cc < 9) { $mx *= 10; }
    $number += $mx;
    if ($number >= 1000000000) { $d = 1; $number %= 1000000000; }
}

$cc = $prec;
while ($cc < 9) { $number = int ($number / 10); ++$cc; }

printf "%d.%0*d\n", $d, $prec, $number;

exit 0;

sub usage (@) {
    if (@_) { print STDERR "$prog: ".(join '', @_)."\n"; exit 64; }
    print "Usage: $prog number precision\n";
    exit 0;
}

