# instances_with_guid.pm
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@pps-ag.com
# Copyright: (c) 2020, PPS AG
# 
#
# Module solely consisting of a list (HASH) of names of ENTITY instances which
# contain the 'GlobalId' attribute.

package instances_with_guid;
use 5.16.0;
use strict;
use vars qw($RCSID $VERSION);

$RCSID = q$Id$;
$VERSION = '0.01';

use Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
use Carp;

use Readonly;

@ISA = qw(Exporter);
@EXPORT = qw(%Instances_with_GUID);
@EXPORT_OK = ();
%EXPORT_TAGS = (all => [ @EXPORT, @EXPORT_OK ]);

#use vars qw(%Instances_with_GUID);

Readonly::Hash our %Instances_with_GUID => (
    IFCACTIONREQUEST => 1, IFCACTOR => 1, IFCACTUATOR => 1,
    IFCACTUATORTYPE => 1, IFCAIRTERMINAL => 1, IFCAIRTERMINALBOX => 1,
    IFCAIRTERMINALBOXTYPE => 1, IFCAIRTERMINALTYPE => 1,
    IFCAIRTOAIRHEATRECOVERY => 1, IFCAIRTOAIRHEATRECOVERYTYPE => 1,
    IFCALARM => 1, IFCALARMTYPE => 1, IFCANNOTATION => 1, IFCASSET => 1,
    IFCAUDIOVISUALAPPLIANCE => 1, IFCAUDIOVISUALAPPLIANCETYPE => 1,
    IFCBEAM => 1, IFCBEAMSTANDARDCASE => 1, IFCBEAMTYPE => 1, IFCBOILER => 1,
    IFCBOILERTYPE => 1, IFCBUILDING => 1, IFCBUILDINGELEMENTPART => 1,
    IFCBUILDINGELEMENTPARTTYPE => 1, IFCBUILDINGELEMENTPROXY => 1,
    IFCBUILDINGELEMENTPROXYTYPE => 1, IFCBUILDINGSTOREY => 1,
    IFCBUILDINGSYSTEM => 1, IFCBURNER => 1, IFCBURNERTYPE => 1,
    IFCCABLECARRIERFITTING => 1, IFCCABLECARRIERFITTINGTYPE => 1,
    IFCCABLECARRIERSEGMENT => 1, IFCCABLECARRIERSEGMENTTYPE => 1,
    IFCCABLEFITTING => 1, IFCCABLEFITTINGTYPE => 1, IFCCABLESEGMENT => 1,
    IFCCABLESEGMENTTYPE => 1, IFCCHAMFEREDGEFEATURE => 1, IFCCHILLER => 1,
    IFCCHILLERTYPE => 1, IFCCHIMNEY => 1, IFCCHIMNEYTYPE => 1,
    IFCCIVILELEMENT => 1, IFCCIVILELEMENTTYPE => 1, IFCCOIL => 1,
    IFCCOILTYPE => 1, IFCCOLUMN => 1, IFCCOLUMNSTANDARDCASE => 1,
    IFCCOLUMNTYPE => 1, IFCCOMMUNICATIONSAPPLIANCE => 1,
    IFCCOMMUNICATIONSAPPLIANCETYPE => 1, IFCCOMPLEXPROPERTYTEMPLATE => 1,
    IFCCOMPRESSOR => 1, IFCCOMPRESSORTYPE => 1, IFCCONDENSER => 1,
    IFCCONDENSERTYPE => 1, IFCCONDITION => 1, IFCCONDITIONCRITERION => 1,
    IFCCONSTRUCTIONEQUIPMENTRESOURCE => 1,
    IFCCONSTRUCTIONEQUIPMENTRESOURCETYPE => 1,
    IFCCONSTRUCTIONMATERIALRESOURCE => 1,
    IFCCONSTRUCTIONMATERIALRESOURCETYPE => 1,
    IFCCONSTRUCTIONPRODUCTRESOURCE => 1,
    IFCCONSTRUCTIONPRODUCTRESOURCETYPE => 1,
    IFCCONTROLLER => 1, IFCCONTROLLERTYPE => 1, IFCCOOLEDBEAM => 1,
    IFCCOOLEDBEAMTYPE => 1, IFCCOOLINGTOWER => 1, IFCCOOLINGTOWERTYPE => 1,
    IFCCOSTITEM => 1, IFCCOSTSCHEDULE => 1, IFCCOVERING => 1,
    IFCCOVERINGTYPE => 1, IFCCREWRESOURCE => 1, IFCCREWRESOURCETYPE => 1,
    IFCCURTAINWALL => 1, IFCCURTAINWALLTYPE => 1, IFCDAMPER => 1,
    IFCDAMPERTYPE => 1, IFCDISCRETEACCESSORY => 1,
    IFCDISCRETEACCESSORYTYPE => 1, IFCDISTRIBUTIONCHAMBERELEMENT => 1,
    IFCDISTRIBUTIONCHAMBERELEMENTTYPE => 1, IFCDISTRIBUTIONCIRCUIT => 1,
    IFCDISTRIBUTIONCONTROLELEMENT => 1, IFCDISTRIBUTIONELEMENT => 1,
    IFCDISTRIBUTIONELEMENTTYPE => 1, IFCDISTRIBUTIONFLOWELEMENT => 1,
    IFCDISTRIBUTIONPORT => 1, IFCDISTRIBUTIONSYSTEM => 1, IFCDOOR => 1,
    IFCDOORLININGPROPERTIES => 1, IFCDOORPANELPROPERTIES => 1,
    IFCDOORSTANDARDCASE => 1, IFCDOORSTYLE => 1, IFCDOORTYPE => 1,
    IFCDUCTFITTING => 1, IFCDUCTFITTINGTYPE => 1, IFCDUCTSEGMENT => 1,
    IFCDUCTSEGMENTTYPE => 1, IFCDUCTSILENCER => 1, IFCDUCTSILENCERTYPE => 1,
    IFCELECTRICALBASEPROPERTIES => 1, IFCELECTRICALCIRCUIT => 1,
    IFCELECTRICALELEMENT => 1, IFCELECTRICAPPLIANCE => 1,
    IFCELECTRICAPPLIANCETYPE => 1, IFCELECTRICDISTRIBUTIONBOARD => 1,
    IFCELECTRICDISTRIBUTIONBOARDTYPE => 1, IFCELECTRICDISTRIBUTIONPOINT => 1,
    IFCELECTRICFLOWSTORAGEDEVICE => 1, IFCELECTRICFLOWSTORAGEDEVICETYPE => 1,
    IFCELECTRICGENERATOR => 1, IFCELECTRICGENERATORTYPE => 1,
    IFCELECTRICHEATERTYPE => 1, IFCELECTRICMOTOR => 1,
    IFCELECTRICMOTORTYPE => 1, IFCELECTRICTIMECONTROL => 1,
    IFCELECTRICTIMECONTROLTYPE => 1, IFCELEMENTASSEMBLY => 1,
    IFCELEMENTASSEMBLYTYPE => 1, IFCELEMENTQUANTITY => 1,
    IFCENERGYCONVERSIONDEVICE => 1, IFCENERGYPROPERTIES => 1, IFCENGINE => 1,
    IFCENGINETYPE => 1, IFCEQUIPMENTELEMENT => 1, IFCEQUIPMENTSTANDARD => 1,
    IFCEVAPORATIVECOOLER => 1, IFCEVAPORATIVECOOLERTYPE => 1,
    IFCEVAPORATOR => 1, IFCEVAPORATORTYPE => 1, IFCEVENT => 1,
    IFCEVENTTYPE => 1, IFCEXTERNALSPATIALELEMENT => 1, IFCFAN => 1,
    IFCFANTYPE => 1, IFCFASTENER => 1, IFCFASTENERTYPE => 1, IFCFILTER => 1,
    IFCFILTERTYPE => 1, IFCFIRESUPPRESSIONTERMINAL => 1,
    IFCFIRESUPPRESSIONTERMINALTYPE => 1, IFCFLOWCONTROLLER => 1,
    IFCFLOWFITTING => 1, IFCFLOWINSTRUMENT => 1, IFCFLOWINSTRUMENTTYPE => 1,
    IFCFLOWMETER => 1, IFCFLOWMETERTYPE => 1, IFCFLOWMOVINGDEVICE => 1,
    IFCFLOWSEGMENT => 1, IFCFLOWSTORAGEDEVICE => 1, IFCFLOWTERMINAL => 1,
    IFCFLOWTREATMENTDEVICE => 1, IFCFLUIDFLOWPROPERTIES => 1, IFCFOOTING => 1,
    IFCFOOTINGTYPE => 1, IFCFURNISHINGELEMENT => 1,
    IFCFURNISHINGELEMENTTYPE => 1, IFCFURNITURE => 1,
    IFCFURNITURESTANDARD => 1, IFCFURNITURETYPE => 1, IFCGASTERMINALTYPE => 1,
    IFCGEOGRAPHICELEMENT => 1, IFCGEOGRAPHICELEMENTTYPE => 1, IFCGRID => 1,
    IFCGROUP => 1, IFCHEATEXCHANGER => 1, IFCHEATEXCHANGERTYPE => 1,
    IFCHUMIDIFIER => 1, IFCHUMIDIFIERTYPE => 1, IFCINTERCEPTOR => 1,
    IFCINTERCEPTORTYPE => 1, IFCINVENTORY => 1, IFCJUNCTIONBOX => 1,
    IFCJUNCTIONBOXTYPE => 1, IFCLABORRESOURCE => 1, IFCLABORRESOURCETYPE => 1,
    IFCLAMP => 1, IFCLAMPTYPE => 1, IFCLIGHTFIXTURE => 1,
    IFCLIGHTFIXTURETYPE => 1, IFCMECHANICALFASTENER => 1,
    IFCMECHANICALFASTENERTYPE => 1, IFCMEDICALDEVICE => 1,
    IFCMEDICALDEVICETYPE => 1, IFCMEMBER => 1,
    IFCMEMBERSTANDARDCASE => 1, IFCMEMBERTYPE => 1,
    IFCMOTORCONNECTION => 1, IFCMOTORCONNECTIONTYPE => 1,
    IFCMOVE => 1, IFCOCCUPANT => 1, IFCOPENINGELEMENT => 1,
    IFCOPENINGSTANDARDCASE => 1, IFCORDERACTION => 1,
    IFCOUTLET => 1, IFCOUTLETTYPE => 1, IFCPERFORMANCEHISTORY => 1,
    IFCPERMEABLECOVERINGPROPERTIES => 1, IFCPERMIT => 1, IFCPILE => 1,
    IFCPILETYPE => 1, IFCPIPEFITTING => 1, IFCPIPEFITTINGTYPE => 1,
    IFCPIPESEGMENT => 1, IFCPIPESEGMENTTYPE => 1, IFCPLATE => 1,
    IFCPLATESTANDARDCASE => 1, IFCPLATETYPE => 1, IFCPROCEDURE => 1,
    IFCPROCEDURETYPE => 1, IFCPROJECT => 1, IFCPROJECTIONELEMENT => 1,
    IFCPROJECTLIBRARY => 1, IFCPROJECTORDER => 1, IFCPROJECTORDERRECORD => 1,
    IFCPROPERTYSET => 1, IFCPROPERTYSETTEMPLATE => 1,
    IFCPROTECTIVEDEVICE => 1, IFCPROTECTIVEDEVICETRIPPINGUNIT => 1,
    IFCPROTECTIVEDEVICETRIPPINGUNITTYPE => 1, IFCPROTECTIVEDEVICETYPE => 1,
    IFCPROXY => 1, IFCPUMP => 1, IFCPUMPTYPE => 1, IFCRAILING => 1,
    IFCRAILINGTYPE => 1, IFCRAMP => 1, IFCRAMPFLIGHT => 1,
    IFCRAMPFLIGHTTYPE => 1, IFCRAMPTYPE => 1,
    IFCREINFORCEMENTDEFINITIONPROPERTIES => 1, IFCREINFORCINGBAR => 1,
    IFCREINFORCINGBARTYPE => 1, IFCREINFORCINGMESH => 1,
    IFCREINFORCINGMESHTYPE => 1, IFCRELAGGREGATES => 1,
    IFCRELASSIGNSTASKS => 1, IFCRELASSIGNSTOACTOR => 1,
    IFCRELASSIGNSTOCONTROL => 1, IFCRELASSIGNSTOGROUP => 1,
    IFCRELASSIGNSTOGROUPBYFACTOR => 1, IFCRELASSIGNSTOPROCESS => 1,
    IFCRELASSIGNSTOPRODUCT => 1, IFCRELASSIGNSTOPROJECTORDER => 1,
    IFCRELASSIGNSTORESOURCE => 1, IFCRELASSOCIATES => 1,
    IFCRELASSOCIATESAPPLIEDVALUE => 1, IFCRELASSOCIATESAPPROVAL => 1,
    IFCRELASSOCIATESCLASSIFICATION => 1, IFCRELASSOCIATESCONSTRAINT => 1,
    IFCRELASSOCIATESDOCUMENT => 1, IFCRELASSOCIATESLIBRARY => 1,
    IFCRELASSOCIATESMATERIAL => 1, IFCRELASSOCIATESPROFILEPROPERTIES => 1,
    IFCRELCONNECTSELEMENTS => 1, IFCRELCONNECTSPATHELEMENTS => 1,
    IFCRELCONNECTSPORTS => 1, IFCRELCONNECTSPORTTOELEMENT => 1,
    IFCRELCONNECTSSTRUCTURALACTIVITY => 1,
    IFCRELCONNECTSSTRUCTURALELEMENT => 1, IFCRELCONNECTSSTRUCTURALMEMBER => 1,
    IFCRELCONNECTSWITHECCENTRICITY => 1,
    IFCRELCONNECTSWITHREALIZINGELEMENTS => 1,
    IFCRELCONTAINEDINSPATIALSTRUCTURE => 1, IFCRELCOVERSBLDGELEMENTS => 1,
    IFCRELCOVERSSPACES => 1, IFCRELDECLARES => 1, IFCRELDEFINESBYOBJECT => 1,
    IFCRELDEFINESBYPROPERTIES => 1, IFCRELDEFINESBYTEMPLATE => 1,
    IFCRELDEFINESBYTYPE => 1, IFCRELFILLSELEMENT => 1,
    IFCRELFLOWCONTROLELEMENTS => 1, IFCRELINTERACTIONREQUIREMENTS => 1,
    IFCRELINTERFERESELEMENTS => 1, IFCRELNESTS => 1, IFCRELOCCUPIESSPACES => 1,
    IFCRELOVERRIDESPROPERTIES => 1, IFCRELPROJECTSELEMENT => 1,
    IFCRELREFERENCEDINSPATIALSTRUCTURE => 1, IFCRELSCHEDULESCOSTITEMS => 1,
    IFCRELSEQUENCE => 1, IFCRELSERVICESBUILDINGS => 1, IFCRELSPACEBOUNDARY => 1,
    IFCRELSPACEBOUNDARY1STLEVEL => 1, IFCRELSPACEBOUNDARY2NDLEVEL => 1,
    IFCRELVOIDSELEMENT => 1, IFCROOF => 1, IFCROOFTYPE => 1,
    IFCROUNDEDEDGEFEATURE => 1, IFCSANITARYTERMINAL => 1,
    IFCSANITARYTERMINALTYPE => 1, IFCSCHEDULETIMECONTROL => 1,
    IFCSENSOR => 1, IFCSENSORTYPE => 1, IFCSERVICELIFE => 1,
    IFCSERVICELIFEFACTOR => 1, IFCSHADINGDEVICE => 1,
    IFCSHADINGDEVICETYPE => 1, IFCSIMPLEPROPERTYTEMPLATE => 1,
    IFCSITE => 1, IFCSLAB => 1, IFCSLABELEMENTEDCASE => 1,
    IFCSLABSTANDARDCASE => 1, IFCSLABTYPE => 1, IFCSOLARDEVICE => 1,
    IFCSOLARDEVICETYPE => 1, IFCSOUNDPROPERTIES => 1, IFCSOUNDVALUE => 1,
    IFCSPACE => 1, IFCSPACEHEATER => 1, IFCSPACEHEATERTYPE => 1,
    IFCSPACEPROGRAM => 1, IFCSPACETHERMALLOADPROPERTIES => 1,
    IFCSPACETYPE => 1, IFCSPATIALZONE => 1, IFCSPATIALZONETYPE => 1,
    IFCSTACKTERMINAL => 1, IFCSTACKTERMINALTYPE => 1, IFCSTAIR => 1,
    IFCSTAIRFLIGHT => 1, IFCSTAIRFLIGHTTYPE => 1, IFCSTAIRTYPE => 1,
    IFCSTRUCTURALANALYSISMODEL => 1, IFCSTRUCTURALCURVEACTION => 1,
    IFCSTRUCTURALCURVECONNECTION => 1, IFCSTRUCTURALCURVEMEMBER => 1,
    IFCSTRUCTURALCURVEMEMBERVARYING => 1, IFCSTRUCTURALCURVEREACTION => 1,
    IFCSTRUCTURALLINEARACTION => 1, IFCSTRUCTURALLINEARACTIONVARYING => 1,
    IFCSTRUCTURALLOADCASE => 1, IFCSTRUCTURALLOADGROUP => 1,
    IFCSTRUCTURALPLANARACTION => 1, IFCSTRUCTURALPLANARACTIONVARYING => 1,
    IFCSTRUCTURALPOINTACTION => 1, IFCSTRUCTURALPOINTCONNECTION => 1,
    IFCSTRUCTURALPOINTREACTION => 1, IFCSTRUCTURALRESULTGROUP => 1,
    IFCSTRUCTURALSURFACEACTION => 1, IFCSTRUCTURALSURFACECONNECTION => 1,
    IFCSTRUCTURALSURFACEMEMBER => 1, IFCSTRUCTURALSURFACEMEMBERVARYING => 1,
    IFCSTRUCTURALSURFACEREACTION => 1, IFCSUBCONTRACTRESOURCE => 1,
    IFCSUBCONTRACTRESOURCETYPE => 1, IFCSURFACEFEATURE => 1,
    IFCSWITCHINGDEVICE => 1, IFCSWITCHINGDEVICETYPE => 1, IFCSYSTEM => 1,
    IFCSYSTEMFURNITUREELEMENT => 1, IFCSYSTEMFURNITUREELEMENTTYPE => 1,
    IFCTANK => 1, IFCTANKTYPE => 1, IFCTASK => 1, IFCTASKTYPE => 1,
    IFCTENDON => 1, IFCTENDONANCHOR => 1, IFCTENDONANCHORTYPE => 1,
    IFCTENDONTYPE => 1, IFCTIMESERIESSCHEDULE => 1, IFCTRANSFORMER => 1,
    IFCTRANSFORMERTYPE => 1, IFCTRANSPORTELEMENT => 1,
    IFCTRANSPORTELEMENTTYPE => 1, IFCTUBEBUNDLE => 1, IFCTUBEBUNDLETYPE => 1,
    IFCTYPEOBJECT => 1, IFCTYPEPRODUCT => 1, IFCUNITARYCONTROLELEMENT => 1,
    IFCUNITARYCONTROLELEMENTTYPE => 1, IFCUNITARYEQUIPMENT => 1,
    IFCUNITARYEQUIPMENTTYPE => 1, IFCVALVE => 1,
    IFCVALVETYPE => 1, IFCVIBRATIONISOLATOR => 1,
    IFCVIBRATIONISOLATORTYPE => 1, IFCVIRTUALELEMENT => 1,
    IFCVOIDINGFEATURE => 1, IFCWALL => 1, IFCWALLELEMENTEDCASE => 1,
    IFCWALLSTANDARDCASE => 1, IFCWALLTYPE => 1, IFCWASTETERMINAL => 1,
    IFCWASTETERMINALTYPE => 1, IFCWINDOW => 1, IFCWINDOWLININGPROPERTIES => 1,
    IFCWINDOWPANELPROPERTIES => 1, IFCWINDOWSTANDARDCASE => 1,
    IFCWINDOWSTYLE => 1, IFCWINDOWTYPE => 1, IFCWORKCALENDAR => 1,
    IFCWORKPLAN => 1, IFCWORKSCHEDULE => 1, IFCZONE => 1,
);

1;
