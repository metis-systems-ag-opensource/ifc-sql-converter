/* expr.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Implementation of 'expr.h'
** (Structures for generating expression trees)
**
*/

#include <iostream>

#include "Common/dump.h"

#include <string>
#include <vector>
#include <set>
#include <stdexcept>
#include <utility>

#include "expr.h"

#include "consteval.h"

using namespace std;

//using Token = EXPRESS::Parser::token;

//using Ident = uint32_t;

// Making the stupid 'set.count(element) > 0)' simpler to understand.
// Additionally, this macro uses less (text-)space where applied, even if
// it will be replaced by the original (stupid) code.
//
#define isof(element,set) ((set).count((element)) > 0)

using RType = Expr::RType;

//struct Expr {
//    enum ExprType {
//	binary, unary, std_invoc, func_invoc, interval, query,
//	logical_literal, int_literal, real_literal, binary_literal,
//	string_literal
//    } type;

Expr::Expr() { }

//    Expr (const Expr &x) = delete;

Expr::~Expr() { }

//Expr::Type Expr::type() const { return Expr::invalid; }

//RType Expr::rtype() const { return RType::unknown; }

//Expr *Expr::Clone() { return new Expr; }

//} /*Expr*/;

static const char *typetag (Expr::Type x)
{
    switch (x) {
	case Expr::invalid:	    return "invalid";
	case Expr::binary:	    return "binary";
	case Expr::unary:	    return "unary";
	case Expr::std_invoc:	    return "std_invoc";
	case Expr::func_invoc:	    return "func_invoc";
	case Expr::interval:	    return "interval";
	case Expr::query:	    return "query";
	case Expr::logical_literal: return "logical_literal";
	case Expr::int_literal:	    return "int_literal";
	case Expr::real_literal:    return "real_literal";
	case Expr::binary_literal:  return "binary_literal";
	case Expr::string_literal:  return "string_literal";
	case Expr::unspec_literal:  return "unspec_literal";
	case Expr::instance:	    return "instance";
	case Expr::aggregate:	    return "aggregate";
	case Expr::nxaggregate:	    return "nxaggregate";
	case Expr::enum_value:	    return "enumvalue";
	case Expr::ref2const:	    return "ref2const";
	case Expr::reference:	    return "reference";
	default:		    return "**unknown**";
    }
}

const char *typetag (const Expr *x)
{
    return x ? typetag (x->type()) : "***NULL***";
}

template<class T>
static bool in (const T &e, const set<T> &s) {
    return isof(e, s);
}

//struct BinaryExpr : public Expr {
//    Token op;
//    Expr *left, *right;

BinaryExpr::BinaryExpr (BinOp op, Expr *l, Expr *r)
    : left(l), right(r), op(op)
{ }

//    BinaryExpr (const BinaryExpr &x) = delete;

BinaryExpr::BinaryExpr (BinaryExpr &&x)
    : left(x.left), right(x.right), op(x.op)
{
    where (x.where());
    x.left = nullptr; x.right = nullptr;
}

//    BinaryExpr & operator= (const BinaryExpr &x) = delete;

BinaryExpr & BinaryExpr::operator= (BinaryExpr &&x) {
    op = x.op; left = x.left; right = x.right; where (x.where());
    x.left = nullptr; x.right = nullptr;
    return *this;
}

BinaryExpr::~BinaryExpr() {
    delete left; left = (decltype(left)) 0x0123456789abcdef;
    delete right; right = (decltype(right)) 0xfedcba9876543210;
}

Expr::Type BinaryExpr::type() const { return Expr::binary; }

RType BinaryExpr::rtype() const {
    RType lotyp = left->rtype(), rotyp = right->rtype();
    static const set<RType> aggregate {
	RType::set, RType::bag, RType::list,
	RType::array
    };
    static const set<RType> simple {
	RType::numeric, RType::bitstring, RType::string
    };
    static const set<RType> bagset { RType::bag, RType::set };
    static const set<RType> bagsetlist {
	RType::bag, RType::set, RType::list
    };
    if (lotyp == RType::error || rotyp == RType::error) {
	return RType::error;
    }
    if (lotyp == RType::unknown || rotyp == RType::unknown) {
	return RType::unknown;
    }
    switch (op) {
	case BinOp::lt: case BinOp::gt:
	    if ((isof(lotyp, simple) || lotyp == RType::enumtype)
	    &&  lotyp == rotyp) {
		return RType::logical;
	    }
	    break;
	case BinOp::le: case BinOp::ge:
	    if ((isof(lotyp, simple) || lotyp == RType::enumtype)
	    &&  lotyp == rotyp) {
		return RType::logical;
	    }
	    if (isof(lotyp, bagset) && isof(rotyp, bagset)) {
		return RType::logical;
	    }
	    break;
	case BinOp::eq: case BinOp::ne: case BinOp::ieq: case BinOp::ine:
	    if ((isof(lotyp, simple) || lotyp == RType::enumtype)
	    &&  rotyp == lotyp) {
		return RType::logical;
	    }
	    if (isof(lotyp, aggregate) && isof(rotyp, aggregate)) {
		return RType::logical;
	    }
	    if (lotyp == RType::logical && rotyp == lotyp) {
		return RType::logical;
	    }
	    if (lotyp == RType::entity && rotyp == lotyp) {
		return RType::logical;
	    }
	    break;
	case BinOp::add:
	    if (isof(lotyp, simple) && rotyp == lotyp) {
		return lotyp;
	    }
	    if (isof(lotyp, simple) && isof(rotyp, bagsetlist)) {
		return rotyp;
	    }
	    if (isof(lotyp, bagsetlist) && isof(rotyp, simple)) {
		return lotyp;
	    }
	    if (isof(lotyp, bagsetlist) && isof(rotyp, bagsetlist)) {
		return lotyp;
	    }
	    break;
	case BinOp::sub:
	    if (lotyp == RType::numeric && rotyp == lotyp) { return lotyp; }
	    if (isof(lotyp, bagset) && isof(rotyp, simple)) {
		return lotyp;
	    }
	    if (isof(lotyp, bagset) && isof(rotyp, bagset)) {
		return lotyp;
	    }
	    break;
	case BinOp::mul:
	    if (lotyp == RType::numeric && rotyp == lotyp) { return lotyp; }
	    if (isof(lotyp, bagset) && isof(rotyp, bagset)) {
		return (lotyp == RType::bag ? lotyp : RType::set);
	    }
	    break;
	case BinOp::rdiv: case BinOp::idiv: case BinOp::mod: case BinOp::pow:
	    if (lotyp == RType::numeric && rotyp == lotyp) { return lotyp; }
	    break;
	case BinOp::and_: case BinOp::or_: case BinOp::xor_:
	    if (lotyp == RType::logical && rotyp == lotyp) { return lotyp; }
	    break;
	case BinOp::like:
	    if (lotyp == RType::string && rotyp == lotyp) {
		return RType::logical;
	    }
	    break;
	case BinOp::in:
	    if ((isof(lotyp, simple) || lotyp == RType::enumtype)
	    &&  isof(rotyp, aggregate)) {
		return RType::logical;
	    }
	    if (lotyp == RType::entity && isof(rotyp, aggregate)) {
		return RType::logical;
	    }
	    // Not (yet) implemented. If the left operand is an aggregate and
	    // the right one is an aggregate, too, we must see if the left
	    // operand's dimension is exactly one step below the right one's
	    // and if the aggregate type of the left operand matches that of
	    // the element type of the right one. If this is the case, a
	    // search can be done. Otherwise, the result is an error ...
	    break;
	case BinOp::iconc:
	    if (lotyp == RType::entity && rotyp == lotyp) {
		return RType::entity;
	    }
	    break;
	default:
	    break;
    }
    return RType::error;
}

BinaryExpr *BinaryExpr::Clone() {
    auto res = new BinaryExpr (op, left->Clone(), right->Clone());
    res->where (where());
    return res;
}

//} /*BinaryExpr*/;

//struct UnaryExpr : public Expr {
//    Token op;
//    Expr *right;

UnaryExpr::UnaryExpr (MonOp op, Expr *r)
    : right(r), op(op)
{ }

//    UnaryExpr (const UnaryExpr &x) = delete;

UnaryExpr::UnaryExpr (UnaryExpr &&x)
    : right(x.right), op(x.op)
{
    where (x.where());
    x.right = nullptr;
}

//    UnaryExpr & operator= (const UnaryExpr &x) = delete;

UnaryExpr & UnaryExpr::operator= (UnaryExpr &&x) {
    op = x.op; right = x.right; where (x.where());
    x.right = nullptr;
    return *this;
}

UnaryExpr::~UnaryExpr() {
    delete right; right = (decltype(right)) 0xA0A0A0A00A0A0A0A;
}

Expr::Type UnaryExpr::type() const { return Expr::unary; }

RType UnaryExpr::rtype() const {
    RType rotyp = right->rtype();
    if (rotyp == RType::unknown || rotyp == RType::error) { return rotyp; }
    if ((op == MonOp::plus || op == MonOp::minus) && rotyp == RType::numeric) {
	return rotyp;
    }
    if (op == MonOp::not_ && rotyp == RType::logical) { return rotyp; }
    return RType::error;
}

UnaryExpr *UnaryExpr::Clone() {
    auto res = new UnaryExpr (op, right->Clone());
    res->where (where());
    return res;
}

//} /*UnaryExpr*/;

static void elist_clear (Fifo<Expr *> &el)
{
    while (! el.empty()) {
	Expr *x = el.shift();
	delete x;
    }
}

//struct StdInvoc : public Expr {
//    Fifo<Expr *> arguments;
//    StdFunc stdfunc;
StdInvoc::StdInvoc (Ident sf, StdFunc ftag, Fifo<Expr *> &&args)
    : arguments(args), stdfunc(sf), ftag(ftag)
{ }
//    StdInvoc (const StdInvoc &x) = delete;
StdInvoc::StdInvoc (StdInvoc &&x)
    : arguments(move(x.arguments)), stdfunc(x.stdfunc), ftag(x.ftag)
{
    where (x.where());
}
//    StdInvoc & operator= (const StdInvoc &x) = delete;
StdInvoc & StdInvoc::operator= (StdInvoc &&x) {
    stdfunc = x.stdfunc; ftag = x.ftag; arguments = move(x.arguments);
    where (x.where());
    return *this;
}

StdInvoc::~StdInvoc() {
    elist_clear (arguments);
}

Expr::Type StdInvoc::type() const { return Expr::std_invoc; }

RType StdInvoc::rtype() const {
    return RType::unknown;
}

StdInvoc *StdInvoc::Clone() {
    Fifo<Expr *> cargs;
    for (auto arg: arguments) {
	cargs.push (arg->Clone());
    }
    StdInvoc *res = new StdInvoc (stdfunc, ftag, move(cargs));
    res->where (where());
    return res;
}

//} /*StdInvoc*/;

//struct FuncInvoc : public Expr {
//    Fifo<Expr *> arguments;
//    Ident funcref; bool entity;

FuncInvoc::FuncInvoc (Ident func, Fifo<Expr *> args)
    : arguments(args), funcref(func), entity(false)
{ }

//    FuncInvoc (const FuncInvoc &x) = delete;

FuncInvoc::FuncInvoc (FuncInvoc &&x)
    : arguments(move(x.arguments)), funcref(x.funcref), entity(x.entity)
{
    where (x.where()); x.funcref = 0;
}

//    FuncInvoc & operator= (const FuncInvoc &x) = delete;

FuncInvoc & FuncInvoc::operator= (FuncInvoc &&x) {
    x.funcref = funcref; arguments = move(x.arguments); entity = x.entity;
    where (x.where()); x.funcref = 0;
    return *this;
}

FuncInvoc::~FuncInvoc() {
    elist_clear (arguments);
}

Expr::Type FuncInvoc::type() const {
    // A special case of a function-invocation is an instance (a simple
    // ENTITY-expression). If (while the parsing of an expression or during the
    // type-check) the 'funcref' is determined as that of an ENTITY, the
    // corresponding flag should be set. Is this flag set, the return-value
    // here is 'Expr::instance'. In each other case, it is 'Expr::func_invoc'.
    return entity ? Expr::instance : Expr::func_invoc;
}

RType FuncInvoc::rtype() const { return RType::unknown; }

FuncInvoc *FuncInvoc::Clone() {
    Fifo<Expr *> cargs;
    for (auto arg: arguments) {
	cargs.push (arg->Clone());
    }
    FuncInvoc *res = new FuncInvoc (funcref, cargs);
    res->where (where());
    return res;
}

void FuncInvoc::setEntity (bool isEntity) { entity = isEntity; }
//} /*FuncInvoc*/;

//struct Interval : public Expr {
//    Expr *left, *middle, *right;
//    IntervalOp l_op, r_op;
Interval::Interval (IntervalOp l_op, IntervalOp r_op,
		    Expr *l, Expr *m, Expr *r)
    : left(l), middle(m), right(r), l_op(l_op), r_op(r_op)
{ }

//    Interval (const Interval &x) = delete;

Interval::Interval (Interval &&x)
    : left(x.left), middle(x.middle), right(x.right),
      l_op(x.l_op), r_op(x.r_op)
{
    where (x.where());
    x.left = nullptr; x.middle = nullptr; x.right = nullptr;
}

//    Interval & operator= (const Interval &x) = delete;

Interval & Interval::operator= (Interval &&x) {
    l_op = x.l_op; r_op = x.r_op;
    left = x.left; middle = x.middle; right = x.right;
    where (x.where());
    x.left = nullptr; x.middle = nullptr; x.right = nullptr;
    return *this;
}

Interval::~Interval() {
    delete left; left = (decltype(left)) 0x1234554321ABCCBA;
    delete middle; middle = (decltype(middle)) 0xABC1234554321CBA;
    delete right; right = (decltype(right)) 0xCBAABC5432112345;
}

Expr::Type Interval::type() const { return Expr::interval; }

RType Interval::rtype() const {
    RType lotyp = left->rtype(), motyp = middle->rtype(),
	  rotyp = right->rtype();
    if (lotyp == RType::numeric && motyp == lotyp && rotyp == lotyp) {
	return RType::logical;
    }
    return RType::error;
}

Interval *Interval::Clone() {
    auto res = new Interval (l_op, r_op,
			     left->Clone(), middle->Clone(), right->Clone());
    res->where (where());
    return res;
}

//} /*Interval*/;

//struct Query : public Expr {
//    Expr *source, *cond;
//    Ident id;

Query::Query (Ident id, Expr *src, Expr *cc)
    : source(src), cond(cc), id(id)
{ }

//    Query (const Query &x) = delete;

Query::Query (Query &&x)
    : source(x.source), cond(x.cond), id(x.id)
{
    where (x.where()); x.source = nullptr; x.cond = nullptr; x.id = 0;
}

//    Query & operator= (const Query &x) = delete;

Query & Query::operator= (Query &&x) {
    id = x.id; source = x.source; cond = x.cond;
    where (x.where()); x.source = nullptr; x.cond = nullptr;
    return *this;
}

Query::~Query() {
    delete source; source = (decltype(source)) 0x123123123123123F;
    delete cond; cond = (decltype(source)) 0xF321321321321321;
}

Expr::Type Query::type() const { return Expr::query; }

RType Query::rtype() const {
    static const set<RType> aggregate {
	 RType::bag, RType::set, RType::list, RType::array
    };
    RType sotyp = source->rtype(), cotyp = cond->rtype();
    if (cotyp == RType::error) { return cotyp; }
    if (sotyp == RType::unknown || sotyp == RType::error) { return sotyp; }
    if (cotyp == RType::logical && isof(sotyp, aggregate)) {
	return sotyp;
    }
    return RType::error;
}

Query *Query::Clone() {
    auto res = new Query (id, source->Clone(), cond->Clone());
    res->where (where());
    return res;
}

//} /*Query*/;

//struct LogicalLiteral : public Expr {
//    enum lltag { l_false, l_unknown, l_true } value;

LogicalLiteral::LogicalLiteral (const Logical &v) : value(v) { }

LogicalLiteral::LogicalLiteral (Logical::Value v) : value(Logical(v)) { }

LogicalLiteral::LogicalLiteral (bool b) : value(b) { }

LogicalLiteral::LogicalLiteral (const LogicalLiteral &x)
    : value(x.value)
{
    where (x.where());
}

LogicalLiteral & LogicalLiteral::operator= (const LogicalLiteral &x) {
    value = x.value; where (x.where());
    return *this;
}

LogicalLiteral::~LogicalLiteral() { }

bool LogicalLiteral::to_bool() { return value.to_bool(); }

Expr::Type LogicalLiteral::type() const { return Expr::logical_literal; }

RType LogicalLiteral::rtype() const { return RType::logical; }

LogicalLiteral *LogicalLiteral::Clone() {
    return new LogicalLiteral (*this);
}

//} /*LogicalLiteral*/;

//struct IntLiteral : public Expr {
//    Integer value;

IntLiteral::IntLiteral (Integer v) : value(v) { }

IntLiteral::IntLiteral (const IntLiteral &x)
    : value(x.value)
{
    where (x.where());
}

IntLiteral & IntLiteral::operator= (const IntLiteral &x) {
    value = x.value; where (x.where());
    return *this;
}

IntLiteral::~IntLiteral() { }

Expr::Type IntLiteral::type() const { return Expr::int_literal; }

RType IntLiteral::rtype() const { return RType::numeric; }

IntLiteral *IntLiteral::Clone() {
    return new IntLiteral (*this);
}

//} /*IntLiteral*/;

//struct RealLiteral : public Expr {
//    Real val;

RealLiteral::RealLiteral (Real v) : value(v) { }

RealLiteral::RealLiteral (const RealLiteral &x)
    : value(x.value)
{
    where (x.where());
}

RealLiteral & RealLiteral::operator= (const RealLiteral &x) {
    value = x.value; where (x.where());
    return *this;
}

RealLiteral::~RealLiteral() { }

Expr::Type RealLiteral::type() const { return Expr::real_literal; }

RType RealLiteral::rtype() const { return RType::numeric; }

RealLiteral *RealLiteral::Clone() {
    return new RealLiteral (*this);
}

//} /*RealLiteral*/;

//struct BinaryLiteral : public Expr {
//    std::vector<bool> value;
//    uint32_t length;
BinaryLiteral::BinaryLiteral (const string &s) {
    uint32_t length = s.length();
    value.reserve (length);
    for (unsigned ix = 0; ix < s.length(); ++ix) {
	if (s[ix] != '0' && s[ix] != '1') { continue; }
	value.push_back (s[ix] == '1');
    }
}

BinaryLiteral::BinaryLiteral (const Bitvec &bv) : value(bv) { }

BinaryLiteral::BinaryLiteral (const BinaryLiteral &x)
    : value(x.value)
{
    where (x.where());
}

BinaryLiteral::BinaryLiteral (BinaryLiteral &&x)
    : value(move(x.value))
{
    where (x.where());
}

BinaryLiteral & BinaryLiteral::operator= (const BinaryLiteral &x) {
    value = x.value; where (x.where());
    return *this;
}

BinaryLiteral & BinaryLiteral::operator= (BinaryLiteral &&x) {
    value = move(x.value); where (x.where());
    return *this;
}

BinaryLiteral::~BinaryLiteral() { value.clear(); }

Expr::Type BinaryLiteral::type() const { return Expr::binary_literal; }

RType BinaryLiteral::rtype() const { return RType::bitstring; }

BinaryLiteral *BinaryLiteral::Clone() {
    return new BinaryLiteral (*this);
}

uint32_t BinaryLiteral::length() { return value.size(); }
//} /*BinaryLiteral*/;

//struct StringLiteral : public Expr {
//    std::string value;
StringLiteral::StringLiteral (const string &v) : value(v) { }

StringLiteral::StringLiteral (const StringLiteral &x)
    : value(x.value)
{
    where (x.where());
}

StringLiteral::StringLiteral (StringLiteral &&x)
    : value(move(x.value))
{
    where (x.where());
}

StringLiteral & StringLiteral::operator= (const StringLiteral &x) {
    value = x.value; where (x.where());
    return *this;
}

StringLiteral & StringLiteral::operator= (StringLiteral &&x) {
    value = move(x.value); where (x.where());
    return *this;
}

StringLiteral::~StringLiteral() { }

Expr::Type StringLiteral::type() const { return Expr::string_literal; }

RType StringLiteral::rtype() const { return RType::string; }

StringLiteral *StringLiteral::Clone() {
    return new StringLiteral (*this);
}

//} /*StringLiteral*/;

//struct UnspecLiteral : public Expr {
UnspecLiteral::UnspecLiteral() { }

UnspecLiteral::UnspecLiteral (const UnspecLiteral &x) {
    where (x.where());
}

UnspecLiteral & UnspecLiteral::operator= (const UnspecLiteral &x) {
    where (x.where()); return *this;
}

UnspecLiteral::~UnspecLiteral() { }

Expr::Type UnspecLiteral::type() const { return Expr::unspec_literal; }

RType UnspecLiteral::rtype() const { return RType::unknown; }

UnspecLiteral *UnspecLiteral::Clone() {
    return new UnspecLiteral (*this);
}

//} /*UnspecLiteral*/;

//struct Qualifier {
//    enum QualType { attrqual, groupqual, indexqual } type;

Qualifier::Qualifier() { }

Qualifier::~Qualifier() { }

//QualType QualType::type() const { return Qualifier::invalid; }
//Qualifier *Qualifier::Clone() { return new Qualifier; }
//} /*Qualifier*/;

static const char *typetag (Qualifier::QualType x)
{
    switch (x) {
	case Qualifier::attrqual:	return "attrqual";
	case Qualifier::groupqual:	return "groupqual";
	case Qualifier::indexqual:	return "indexqual";
	default:			return "**unknown**";
    }
}

const char *typetag (const Qualifier *x)
{
    return x ? typetag (x->type()) : "***NULL***";
}

//struct AttrQualifier : public Qualifier {
//    std::string attrid;

AttrQualifier::AttrQualifier() { }

AttrQualifier::AttrQualifier (Ident id) : attrid(id) { }

AttrQualifier::AttrQualifier (const AttrQualifier &x)
    : attrid(x.attrid)
{
    where (x.where());
}

AttrQualifier::~AttrQualifier() { }

Qualifier::QualType AttrQualifier::type() const { return Qualifier::attrqual; }

AttrQualifier *AttrQualifier::Clone() {
    return new AttrQualifier (*this);
}

//} /*AttrQualifier*/;

//struct GroupQualifier : public Qualifier {
//    std::string entityid;

GroupQualifier::GroupQualifier() { }

GroupQualifier::GroupQualifier (Ident id) : entityid(id) { }

GroupQualifier::GroupQualifier (const GroupQualifier &x)
    : entityid(x.entityid)
{
    where (x.where());
}

GroupQualifier::~GroupQualifier() { }

Qualifier::QualType GroupQualifier::type() const { return Qualifier::groupqual; }

GroupQualifier *GroupQualifier::Clone() {
    return new GroupQualifier (*this);
}

//} /*GroupQualifier*/;

//struct IndexQualifier : public Qualifier {
//    Expr *lox, *hix;
IndexQualifier::IndexQualifier() : lox(nullptr), hix(nullptr) { }

IndexQualifier::IndexQualifier (Expr *lo, Expr *hi)
    : lox(lo), hix(hi)
{ }

IndexQualifier::IndexQualifier (const IndexQualifier &x)
    : lox(x.lox ? x.lox->Clone() : nullptr),
      hix(x.hix ? x.hix->Clone() : nullptr)
{
    where (x.where());
}

IndexQualifier::IndexQualifier (IndexQualifier &&x)
    : lox(x.lox), hix(x.hix)
{
    where (x.where());
    x.lox = nullptr; x.hix = nullptr;
}

IndexQualifier::~IndexQualifier() {
    if (lox) { delete lox; lox = (decltype(lox)) 0x123ABC456DEF097; }
    if (hix) { delete hix; hix = (decltype(lox)) 0x097ABC456DEF123; }
}

Qualifier::QualType IndexQualifier::type() const
{
    return Qualifier::indexqual;
}

IndexQualifier *IndexQualifier::Clone() {
    return new IndexQualifier (*this);
}

//} /*IndexQualifier*/;

//struct Reference : public Expr {
//    enum RefType { sinvocref, invocref, varref, typeref, refref };
Reference::Reference() { }
Reference::Reference (const Reference &x)
{
    where (x.where());
}
Reference::~Reference() { }
//    virtual RefType reftype() = 0;
//} /*Reference*/;

//struct SInvocRef : public Reference {
//    StdInvoc *invoc;
SInvocRef::SInvocRef (StdInvoc *invoc) : invoc(invoc) { }

SInvocRef::SInvocRef (const SInvocRef &x)
    : Reference(x), invoc(x.invoc->Clone())
{ }

SInvocRef::SInvocRef (SInvocRef &&x)
    : Reference(x), invoc(x.invoc)
{
    x.invoc = nullptr;
}
SInvocRef::~SInvocRef() {
    delete invoc; invoc = (decltype(invoc)) 0x987323987323DDDA;
}
Expr::Type SInvocRef::type() const { return Expr::reference; }
Expr::RType SInvocRef::rtype() const { return RType::unknown; }
Reference::RefType SInvocRef::reftype() const { return Reference::sinvocref; }
SInvocRef *SInvocRef::Clone() { return new SInvocRef (*this); }
//} /*StdFuncRef*/;

//struct InvocRef : public Reference {
//    FuncInvoc *invoc;
InvocRef::InvocRef (FuncInvoc *invoc) : invoc(invoc) { }

InvocRef::InvocRef (const InvocRef &x)
    : Reference(x), invoc(x.invoc->Clone())
{ }

InvocRef::InvocRef (InvocRef &&x)
    : Reference(x), invoc(x.invoc)
{
    x.invoc = nullptr;
}

InvocRef::~InvocRef() {
    delete invoc; invoc = (decltype(invoc)) 0xDDFE123456CCBA00;
}
Expr::Type InvocRef::type() const { return Expr::reference; }
Expr::RType InvocRef::rtype() const { return RType::unknown; }
Reference::RefType InvocRef::reftype() const { return Reference::invocref; }
InvocRef *InvocRef::Clone() { return new InvocRef (*this); }
//} /*FuncRef*/;

//struct VarRef : public Reference {
//    Ident id, context;
//    bool _isConst;
VarRef::VarRef (Ident id, Ident ctx) : id(id), context(ctx), _isConst(false) { }

VarRef::VarRef (const VarRef &x)
    : Reference(x), id(x.id), context(x.context), _isConst(x._isConst)
{ }

VarRef::~VarRef() { }

Expr::Type VarRef::type() const { return Expr::reference; }

Expr::RType VarRef::rtype() const { return RType::unknown; }

Reference::RefType VarRef::reftype() const { return Reference::varref; }

VarRef *VarRef::Clone() { return new VarRef (*this); }

void VarRef::isConst (bool b) { _isConst = b; }

bool VarRef::isConst() { return _isConst; }
//} /*VarRef*/;

//struct SelfRef : public Reference {
//    Ident atype;
SelfRef::SelfRef (Ident atype) : atype(atype) { }
SelfRef::SelfRef (const SelfRef &x)
    : Reference(x), atype(x.atype)
{ }
SelfRef::~SelfRef() { }
Expr::Type SelfRef::type() const { return Expr::reference; }
Expr::RType SelfRef::rtype() const { return RType::unknown; }
Reference::RefType SelfRef::reftype() const { return Reference::selfref; }
SelfRef *SelfRef::Clone() { return new SelfRef (*this); }
//} /*SelfRef*/;

//struct RefRef : public Reference {
//    Reference *reference;
//    Qualifier *qualifier;
RefRef::RefRef (Reference *ref, Qualifier *qual)
    : reference(ref), qualifier(qual)
{ }

RefRef::RefRef (const RefRef &x)
    : Reference(x), reference(x.reference->Clone()),
      qualifier(x.qualifier->Clone())
{ }

RefRef::RefRef (RefRef &&x)
    : Reference(x), reference(x.reference), qualifier(x.qualifier)
{
    x.reference = nullptr; x.qualifier = nullptr;
}

RefRef::~RefRef() {
    delete reference; reference = (decltype(reference)) 0xFECA00000000ACEF;
    delete qualifier; qualifier = (decltype(qualifier)) 0x3434343434343434;
}
Expr::Type RefRef::type() const { return Expr::reference; }
Expr::RType RefRef::rtype() const { return RType::unknown; }
Reference::RefType RefRef::reftype() const { return Reference::refref; }
RefRef *RefRef::Clone() { return new RefRef (*this); }
//} /*RefRef*/;


#if 0
//struct Instance : public Expr {
//    uint32_t entityid;
//    Fifo<Expr *> args;

//    Instance() = delete;

Instance::Instance (uint32_t id, const Fifo<Expr *> &args)
    : arguments(args), entityid(id)
{ }

Instance::Instance (uint32_t id) : arguments(), entityid(id)
{ }

Instance::Instance (Instance &&x)
    : arguments (move(x.arguments)), entityid(x.entityid)
{
    where (x.where()); entityid = 0;
}

Instance & Instance::operator= (Instance &&x) {
    entityid = x.entityid; arguments = move(x.arguments); where (x.where());
    return *this;
}

Instance::~Instance() {
    elist_clear (arguments);
}

Expr::Type Instance::type() const { return Expr::instance; }

RType Instance::rtype() const {
    bool valid = true;
    for (auto it = arguments.begin(); ! it.atend(); ++it) {
	if ((*it)->rtype() == RType::error) { valid = false; break; }
    }
    return (valid ? RType::entity : RType::error);
}

Instance *Instance::Clone() {
    Fifo<Expr *> cargs;
    for (auto it = arguments.begin(); ! it.atend(); ++it) {
	cargs.push ((*it)->Clone());
    }
    auto res = new Instance (entityid, cargs);
    res->where (where());
    return res;
}

//} /*Instance*/;
#endif

//struct Aggregate : public Expr {
//    Fifo<Expr *> values;

Aggregate::Aggregate() : values() { }

Aggregate::Aggregate (Fifo<Expr *> &&vallist) : values(vallist) { }

Aggregate::Aggregate (const Aggregate &x)
{
    for (auto xp: x.values) { values.push (xp->Clone()); }
    where (x.where());
}

Aggregate::Aggregate (Aggregate &&x)
    : values(move(x.values))
{
    where (x.where());
}

Aggregate & Aggregate::operator= (Aggregate &&x) {
    values = move(x.values); where (x.where());
    return *this;
}

Aggregate::~Aggregate() { elist_clear (values); }

Expr * Aggregate::operator[] (uint32_t ix) {
    return values[ix];
}

Expr::Type Aggregate::type() const { return Expr::aggregate; }

RType Aggregate::rtype() const {
    static const set<RType> aggregate {
	RType::list, RType::bag, RType::set, RType::array
    };
    bool valid = true;
    RType bt = RType::unknown;
    for (Expr *val: values) {
	RType et = val->rtype();
	if (et == RType::error) { valid = false; break; }
	if (bt == RType::unknown) { bt = et; continue; }
	if (et == bt) { continue; }
	if (! isof(bt, aggregate) || val->type() != Expr::aggregate) {
	    valid = false; break;
	}
    }
    return (valid ? RType::bag : RType::error);
}

Aggregate *Aggregate::Clone() { return new Aggregate (*this); }

//} /*Aggregate*/;

//struct AggElement {
//    Expr *value, *repetition;

AggElement::AggElement() : value (nullptr), repetition (nullptr) { }

AggElement::AggElement (Expr *v)
    : value(v), repetition(new IntLiteral (1))
{ }

AggElement::AggElement (Expr *v, Expr *rep)
    : value(v), repetition(rep)
{ }

AggElement::AggElement (const AggElement &x)
    : value(x.value->Clone()), repetition(x.repetition->Clone())
{ }

AggElement::AggElement (AggElement &&x)
    : value(x.value), repetition(x.repetition)
{
    x.value = nullptr; x.repetition = nullptr;
}

AggElement & AggElement::operator= (AggElement &&x) {
    value = x.value; repetition = x.repetition;
    x.value = nullptr; x.repetition = nullptr;
    return *this;
}

AggElement::~AggElement() {
    if (value) { delete value; value = (decltype(value)) 0x5656565656565656; }
    if (repetition) { delete repetition; repetition = (decltype(repetition)) 0x9191919191919191; }
}

Fifo<Expr *> AggElement::expand() {
    consteval (repetition);
    static const set<Expr::Type> num { Expr::int_literal, Expr::real_literal };
    if (numeric_literal (repetition)) {
	/* ERROR: Repetition factor must evaluate to a constant! */
    }
    uint32_t x = int_value (repetition);
    consteval (value);
    if (! is_constant (value)) {
	/* ERROR: Aggregate value must evaluate to a constant! */
    }
    Fifo<Expr *> res;
    for (uint32_t ix = 0; ix < x; ++ix) { res.push (value->Clone()); }
    return res;
}

//} /*AggElement*/;

//struct NxAggregate : public Expr {
//    Fifo<AggElement> rvalues;

NxAggregate::NxAggregate() : rvalues() { }

//NxAggregate::NxAggregate (const Fifo<AggElement> &rvlist) : rvalues(rvlist) { }

NxAggregate::NxAggregate (Fifo<AggElement> &&rvlist)
    : rvalues(move(rvlist))
{ }

NxAggregate::NxAggregate (const NxAggregate &x)
    : rvalues()
{
    for (auto &aggel: x.rvalues) {
	rvalues.push (AggElement (aggel));
    }
    where (x.where());
}

NxAggregate::NxAggregate (NxAggregate &&x)
    : rvalues (move(x.rvalues))
{
    where (x.where());
}

NxAggregate & NxAggregate::operator= (NxAggregate &&x) {
    rvalues = move(x.rvalues); where (x.where());
    return *this;
}

NxAggregate::~NxAggregate() { }

Expr::Type NxAggregate::type() const { return Expr::nxaggregate; }

RType NxAggregate::rtype() const { return RType::list; }

Aggregate * NxAggregate::expand() {
    Fifo<Expr *> rvx;
    for (AggElement &rval: rvalues) {
	rvx.push (rval.expand());
    }
    return new Aggregate (move (rvx));
}

NxAggregate *NxAggregate::Clone() { return new NxAggregate (*this); }

//} /*NxAggregate*/;

//struct EnumValue : public Expr {
//    Ident enumeration, value;

EnumValue::EnumValue() : enumeration(0), value(0) { }

EnumValue::EnumValue (Ident bt, Ident v) : enumeration(bt), value(v) { }

EnumValue::EnumValue (const EnumValue &x)
    : enumeration(x.enumeration), value(x.value)
{
    where (x.where());
}

EnumValue::~EnumValue() { }

Expr::Type EnumValue::type() const { return Expr::enum_value; }

RType EnumValue::rtype() const { return RType::enumtype; }

//    const std::string EnumValue::type_name() { return "EnumValue"; }

EnumValue *EnumValue::Clone() { return new EnumValue (*this); }

//} /*EnumValue*/;

//struct Ref2Const : public Expr {
//    Ident id, ctx;

Ref2Const::Ref2Const() : id(0), ctx(0) { }

Ref2Const::Ref2Const (Ident id, Ident ctx) : id(id), ctx(ctx) { }

Ref2Const::Ref2Const (const Ref2Const &x)
    : id(x.id), ctx(x.ctx)
{
    where (x.where());
}

Ref2Const::~Ref2Const() { }

Expr::Type Ref2Const::type() const { return Expr::ref2const; }

RType Ref2Const::rtype() const { return RType::unknown; }

//    const std::string Ref2Const::type_name() { return "Ref2Const"; }

Ref2Const *Ref2Const::Clone() { return new Ref2Const (*this); }

//} /*Ref2Const*/;
