CREATE DATABASE IFCModel;

USE IFCModel;

CREATE TABLE IdTable (
    id INTEGER NOT NULL,
    type VARCHAR(64) NOT NULL,
    PRIMARY KEY (type)
);

INSERT INTO IdTable (type, id) VALUES ('Authors', 1);
INSERT INTO IdTable (type, id) VALUES ('Organisations', 1);

DELIMITER $$

CREATE FUNCTION `next_id`(idtype varchar(64))
RETURNS INTEGER
BEGIN
    DECLARE res INTEGER;
    SAVEPOINT lockcc;
    SELECT id FROM IdTable WHERE type = idtype into res;
    IF res IS NULL THEN
	SET RES = -1;
    ELSE
	UPDATE IdTable SET id = res + 1 WHERE type = idtype;
    END IF;
    RELEASE SAVEPOINT lockcc;
    RETURN (res);
END$$

DELIMITER ;
    
CREATE TABLE Model (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `fileid` CHAR(32),
    `level` INTEGER NOT NULL,
    `sublevel` INTEGER NOT NULL,
    PRIMARY KEY (`id`)
);
CREATE INDEX Model_fileid ON Model(`fileid`);

CREATE TABLE FileDescription (
    `ModelId` INTEGER NOT NULL,
    `seq` INTEGER NOT NULL,
    `desc` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`ModelId`, `seq`),
    FOREIGN KEY (`ModelId`) REFERENCES Model(`id`)
);

CREATE TABLE FileName (
    `ModelId` INTEGER NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `timestamp` VARCHAR(255) NOT NULL,
    `origsys` VARCHAR(255) NOT NULL,
    `preprocver` VARCHAR(255) NOT NULL,
    `authorization` VARCHAR(255) NOT NULL,
    `authorsid` INTEGER NOT NULL UNIQUE KEY,
    `organisationsid` INTEGER NOT NULL UNIQUE KEY,
    PRIMARY KEY (`ModelId`),
    FOREIGN KEY (`ModelId`) REFERENCES Model(`id`)
);

CREATE TABLE Authors (
    id INTEGER NOT NULL,
    seq INTEGER NOT NULL,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`, `seq`),
    FOREIGN KEY (`id`) REFERENCES FileName(`authorsid`)
);

CREATE TABLE Organisations (
    id INTEGER NOT NULL,
    seq INTEGER NOT NULL,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`, `seq`),
    FOREIGN KEY (`id`) REFERENCES FileName(`organisationsid`)
);

CREATE TABLE FileSchema (
    `ModelId` INTEGER NOT NULL,
    `seq` INTEGER NOT NULL,
    `schema` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`ModelId`, `seq`),
    FOREIGN KEY (`ModelId`) REFERENCES Model(`id`)
);

-- Since 2010-10/2010-11
--
  CREATE TABLE GUIDs (
    guid CHAR(22) NOT NULL,
    tablename VARCHAR(60),
    modelid INTEGER NOT NULL,
    objid INTEGER NOT NULL,
    PRIMARY KEY (GUID, tablename, modelid, objid)
  ) COLLATE = utf8mb4_bin;
  CREATE INDEX GUIDs_guid ON GUIDs(guid);

