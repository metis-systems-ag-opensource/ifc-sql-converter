/* Templates/static/errlist.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** A data type for storing error messages (in a C-alike manner, because this
** is minimal memory consuming)
**
*/
#ifndef _ERRLIST_H
#define _ERRLIST_H

#include <string>

namespace IFC {

struct ErrListItem;

struct ErrorList {
    ErrorList() : errors(nullptr) { };
    ErrorList (const ErrorList &x);
    ErrorList (ErrorList &&x);
    ~ErrorList();
    void add (const std::string &msg);
    void add (const char *msg);
    class iterator {
    public:
	iterator (ErrorList *x, bool end = false) : base(x) {
	    reset (end);
	}
//	iterator (ErrorList &x, bool end = false) : base (&x) {
//	    reset (end);
//	}
	~iterator() { curr = nullptr; base = nullptr; }
	void reset (bool end = false) {
	    curr = (end ? nullptr : base->errors);
	}
	const char * operator*() const;
	iterator &operator++();
	iterator &operator++(int);
	bool operator== (const iterator &r) const {
	    return (base == r.base) && (curr == r.curr);
	}
	bool operator!= (const iterator &r) const {
	    return (base != r.base) || (curr != r.curr);
	}
    private:
	ErrorList *base;
	ErrListItem *curr;
    } /*iterator*/;
    iterator begin() { return iterator (this); }
    iterator end() { return iterator (this, true); }
    ErrorList &operator= (const ErrorList &x);
    ErrorList &operator= (ErrorList &&x);
private:
    void add (const char *msg, size_t msgsize);
    ErrListItem *errors;
};

} /*namespace IFC*/

#endif /*_ERRLIST_H*/
