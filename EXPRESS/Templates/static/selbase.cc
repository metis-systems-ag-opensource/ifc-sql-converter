/* selbase.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Implementation part of 'selbase.h'
** (Base type of all SELECT data types)
**
*/

#include <stdexcept>

#include "plinterface.h"
#include "gentypedvalue.h"

#include "selhelpers.h"

#include "selbase.h"

namespace IFC {

//struct _ExpSelect : ExpBase {
_ExpSelect::_ExpSelect() : v(nullptr) { }

_ExpSelect::_ExpSelect (ExpTypes ttag, const STEP::Value *sv)
    : v(exp_gentypedvalue (sv))
{
    using std::domain_error;
    if (v->type() != ExpTypes::_InstRef && !is_validseltype (ttag, v->type())) {
	throw domain_error ("Invalid SELECT type value");
    }
}

_ExpSelect::~_ExpSelect()
{
    if (v) { delete v; v = nullptr; }
}

//bool _ExpSelect::is_entity() const { return v->is_entity(); }
ExpBase * &_ExpSelect::get() { return v; }

// ATTENTION! This operator is _not_ meant as an exact implementation of
// the EXPRESS (reference) equality operator '=:=' (':=:' ?). It is solely
// being used for the template types in 'aggtype.h'.
bool _ExpSelect::is_same_as (const ExpBase *rx) const
{
    auto x = dynamic_cast<const _ExpSelect *>(rx);
    if (! x) { return false; }
    // Comparing '$' and non-'$' leaves always a 'false' result. This differs
    // slightly from the EXPRESS reference manual, where the result of such a
    // comparison yields an "unknown" LOGICAL result. On the other hand, i need
    // this operation mainly for keeping arrays and lists with unique elements
    // and sets consistent.
    if ((v && ! x->v) || (! v && x->v)) { return false; }
    // For the same reason, i will return 'true' if both values are '$'.
    if (! v) { return true; }
    // Special case. I don't know (yet) if this case will be implemented,
    // but ... if this is the case, 'is_same_as()' will automatically
    // work as expected.
    if (v->is_entity() && x->v->is_entity()) {
	// For instances, the reference comparison is appropriate here, as it
	// is defined as such in the EXPRESS language documentation.
	return v == x->v;
    }
    // In any other case, the correct 'is_same_as()' function will be
    // selected through the runtime type of the object 'v' points to, and
    // this 'is_same_as()' function tests for the correct type of the parameter
    // (which itself is of type 'ExpBase *') and perform the correct
    // comparison.
    return v->is_same_as (rx);
}
//private:
//    ExpBase *v;
//};

} /*namespace IFC*/
