/* plinterface.h
**
** STEP interface for the %{schema} object generation.
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Plugin interface
**
*/
#ifndef PLINTERFACE_H
#define PLINTERFACE_H

#include <cstddef>
#include <string>

#include "exptypes.h"
#include "expbase.h"
#include "ifcmodel.h"
#include "typedefs.h"
#include "utils.h"

namespace IFC {

extern "C" {

extern const char *ExpressSchema;

size_t symsend();

const char *schema();

// Search an entry by name and return an index
size_t findName (const char *n);

// Convert an index as got by 'findName()' into a type tag
ExpTypes index2id (size_t ix);

// Convert a given index back into the name it was determined from
std::string index2name (size_t ix);

// Convert a given type tag into its STEP representation
std::string id2name (ExpTypes t);

// Generate an EXPRESS instance object from a given STEP instance
EntityBase *gen_expinstance (const STEP::Instance *v);

size_t get_instanceid (const EntityBase *v);

// Convert all instance ids either into instance indexes or into
// direct references to the corresponding instances.
void fixrefs (InstanceList *instances);

// This routine is used for storing the IFC metadata and the complete list of
// instances in two databases:
//  - The IFC metadata in a database with the name 'IFCModel',
//  - The IFC instances in a database which is named after the schema name
//    (e.g. 'ifc2x3', 'ifc4').
// The additional argument 'cfg' holds the database connection and
// authentication/authorisation data, and the argument 'testing' is meant to
// enable a simple testing mode (writing the generated model id and for each
// instance the instance id and its corresponding database id to the standard
// output).
void dbstore (const IFCModel *model, const char *schema,
	      InstanceList *instances,
	      ConfData *cfg, OutputMode om);

void instdestroy (EntityBase *p);

} /*extern "C"*/

} /*namespace IFC*/

#endif /*PLINTERFACE_H*/
