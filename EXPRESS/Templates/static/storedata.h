/* storedata.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file of the 'storedata' module.
** (Store the model data of an IFC model (the ENTITY instances from the 'DATA'
**  part of an IFC file) in the selected database.)
**
*/
#ifndef STOREDATA_H
#define STOREDATA_H

#include <vector>

#include "expbase.h"
#include "plctx.h"

namespace IFC {

void store_data (PlCtx &ctx, uint32_t modelid, InstanceList &instances);

} /*namespace IFC*/

#endif /*STOREDATA_H*/
