/* exptypename.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Interface file for 'exptypename.cc'
** (Small utility function which returns the type name (string) of an
**  'ExpBase *'-value or "_NoType" if the pointer was a 'nullptr'.)
**
*/
#ifndef EXPTYPENAME_H
#define EXPTYPENAME_H

#include <string>
#include "expbase.h"

namespace IFC {

std::string exptypename (const ExpBase *v);

} /*namespace IFC*/

#endif /*EXPTYPENAME_H*/
