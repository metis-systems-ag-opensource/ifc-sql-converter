/* valtypes.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** The types of the values used for instances and attributes
**
*/
#ifndef VALTYPES_H
#define VALTYPES_H

#include <string>
#include <vector>
#include <iterator>

#include "mintypes.h"
#include "fifo.h"

namespace STEP {

    enum Logical : int8_t { l_undefined = -1, l_false = 0, l_true = 1 };

    using refid_t = uint32_t;
    extern const refid_t refid_max; // = UINT32_MAX;

    struct Value;

    using valuelist_t = std::vector<Value *>;

    enum class ValType : uint8_t {
	UNSET,
	logical, integer, real, string, bitstring, enumval,
	constvalid, constinstid, valid, instid, null,
	unspecified, list, url, entity, typedparam
    };

    struct Value {
	Value() = default;
	virtual ~Value() { };
	virtual ValType type() const = 0;
	virtual std::string repr() const = 0;
    private:
	Value (const Value &x);
	Value & operator= (const Value&x);
    };

    void val_delete (Value * &&v);

    struct LogicalValue : public Value {
	Logical value;
	LogicalValue() = delete;
	LogicalValue (Logical val);
	LogicalValue (const LogicalValue &x) = delete;
	~LogicalValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*LogicalValue*/;

    struct IntValue : public Value {
	Integer value;
	IntValue();
	IntValue (Integer val);
	IntValue (const IntValue &x) = delete;
	~IntValue ();
	ValType type () const;
	std::string repr() const;
    private:
    } /*IntValue*/;

    struct FloatValue : public Value {
	Real value;
	FloatValue();
	FloatValue (Real val);
	FloatValue (const FloatValue &x) = delete;
	~FloatValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*FloatValue*/;

    struct StringValue : public Value {
	std::string value;
	StringValue();
	StringValue (const std::string &val);
	StringValue (const StringValue &x) = delete;
	~StringValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*StringValue*/;

    struct BitStringValue : public Value {
	BitString value;
	BitStringValue();
	BitStringValue (const BitStringValue &x);
	BitStringValue (const std::string &repr);
	~BitStringValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*BitStringValue*/;

    struct EnumValue : public Value {
	std::string value;

	EnumValue() = delete;
	EnumValue (const std::string &val);
	EnumValue (const EnumValue &x) = delete;
	~EnumValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*EnumValue*/;

    struct ConstValIdValue : public Value {
	std::string id;

	ConstValIdValue() = delete;
	ConstValIdValue (const std::string &refid);
	ConstValIdValue (ConstValIdValue &x) = delete;
	~ConstValIdValue();
	ValType type() const;
	std::string repr() const;
    private:
    } /*ConstValIdValue*/;

    struct ConstInstIdValue : public Value {
	std::string id;

	ConstInstIdValue() = delete;
	ConstInstIdValue (const std::string &refid);
	ConstInstIdValue (const ConstInstIdValue &x) = delete;
	~ConstInstIdValue();
	ValType type() const;
	std::string repr() const;
    private:
    } /*ConstInstIdValue*/;

    struct ValIdValue : public Value {
	refid_t id;

	ValIdValue() = delete;
	ValIdValue (refid_t refid);
	ValIdValue (const ValIdValue &x) = delete;
	~ValIdValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*ValIdValue*/;

    struct InstIdValue : public Value {
	refid_t id;	// An instance-id or an index into an instance map

	InstIdValue() = delete;
	InstIdValue (refid_t refid);	// Value constructor
	InstIdValue (const InstIdValue &x) = delete;	// Copy constructor
	~InstIdValue ();		// Destructor
	ValType type() const;		// The "type" of this object.
	std::string repr() const;
	void set_index (refid_t ix);	// Make this element an "index"
	bool is_index();		// 'true' if this is an "index"
    private:
	uint16_t flags;		// flags (bit 0: 1 - index, 0 - instance id)
    } /*InstIdValue*/;

    struct TypedParamValue : public Value {
	Value *value; Ident name;

	TypedParamValue() = delete;
	TypedParamValue (Ident id, Value *val);
	TypedParamValue (const TypedParamValue &x) = delete;
	~TypedParamValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*TypedParamValue*/;

    struct NullValue : public Value {

	NullValue ();
	NullValue (const NullValue &x) = delete;
	~NullValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*NullValue*/;

    struct UnspecifiedValue : public Value {

	UnspecifiedValue ();
	UnspecifiedValue (const UnspecifiedValue &x) = delete;
	~UnspecifiedValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*UnspecifiedValue*/;

    struct ListValue : public Value {
	valuelist_t valuelist;

	ListValue (Fifo<Value *> * &list);
	ListValue ();
	ListValue (const ListValue &x) = delete;
	~ListValue ();
	ValType type() const;
	std::string repr() const;
	size_t size() const;
	std::vector<Value *>::const_iterator begin() const;
	std::vector<Value *>::iterator begin();
	std::vector<Value *>::const_iterator end() const;
	std::vector<Value *>::iterator end();
    private:
    } /*ListValue*/;

    struct UrlValue : public Value {
	std::string value;

	UrlValue() = delete;
	UrlValue (const std::string &val);
	UrlValue (const UrlValue &x) = delete;
	~UrlValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*UrlValue*/;

    struct EntityValue : public Value {
	valuelist_t attributes; Ident name;
	
	EntityValue () = delete;
	EntityValue (Ident id, Fifo<Value *> * &attrs);
	EntityValue (EntityValue &&x);
	~EntityValue ();
	ValType type() const;
	std::string repr() const;
    private:
    } /*EntityValue*/;

    struct Instance {
	EntityValue entity;
	refid_t id;

	Instance() = delete;
	Instance (refid_t instid, EntityValue &ent);
	Instance (const Instance &x) = delete;
	//Instance & operator= (Instance &&x);
	~Instance ();
    private:
	// Forbid copying (yet) ...
	Instance & operator= (const Instance &x);
	// Forbid using uninitialized instances ...
	//Instance ();
    } /*Instance*/;


} /*namespace STEP*/

#endif /*VALTYPES_H*/
