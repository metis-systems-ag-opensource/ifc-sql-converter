/* exptypename.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Small utility function which returns the type name (string) of an
** 'ExpBase *'-value or "_NoType" if the pointer was a 'nullptr'.
**
*/

#include "plinterface.h"

#include "exptypename.h"

namespace IFC {

using std::string;

string exptypename (const ExpBase *v)
{
    if (v) { return id2name (v->type()); }
    return id2name (ExpTypes::_NoType);
}

} /*namespace IFC*/
