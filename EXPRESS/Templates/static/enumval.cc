/* enumval.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Functions for the associations between ENUMERATION names and values
**
*/

#include <stdexcept>

#include "utils.h"

#include "enumval.h"

namespace IFC {

using std::string, std::to_string;
using std::domain_error;

#include "enumtabs.cc"

uint32_t enumval (uint32_t rngix, const char *name)
{
    // Binary search through the 'enumidx' table - with references to the
    // 'enumnames' list. Here, an index range of '1 .. elements(enumidx)'
    // is used, because this function works completely with 'unsigned' indixes.
    // If an index range of '0 .. elements(enumidx) - 1' would be used, the
    // case that 'm - 1' is negative might occur. This is no limitation, as the
    // (C-typical) index ranges may be simply calculated by a simple
    // subtraction of one.
    EnumRange *erng = &enumrange[rngix - 1];
    uint32_t l = erng->base_ix, r = l + erng->num_el - 1, ev;
    while (l < r) {
	int cres;
	uint32_t m = (l + r) / 2;
	// Need an additional indirection here, because the ENUMERATION values
	// are sorted in the order they were specified, which is not necessary
	// alphabetical.
	ev = enumvalues[m - 1];
	cres = lccmp (enumnames + enumidx[ev - 1], name);
	if (cres < 0) { l = m + 1; } else { r = m; }
    }
    // In this variant of the binary search algorithm, 'l' points either to
    // the element found, or to the next greater element or to the end of the
    // table being searched. This makes an additional comparison necessary.
    ev = enumvalues[l - 1];
    return lccmp (enumnames + enumidx[ev - 1], name) == 0 ? ev : 0;
}

uint32_t enumval (uint32_t rngix, const string &name)
{
    return enumval (rngix, name.c_str());
}

const char *enumname (uint32_t val)
{
    if (val < 1 || val > sizeof(enumidx) / sizeof(*enumidx)) {
	throw domain_error ("Invalid ENUMERATION value: " + to_string (val));
    }
    return enumnames + enumidx[val - 1];
}

uint32_t enumtype (const char *tname)
{
    const size_t numtypes = sizeof(enumtypeidx) / sizeof(*enumtypeidx);
    for (size_t ix = 0; ix < numtypes; ++ix) {
	if (lccmp (tname, enumtypenames + enumtypeidx[ix]) == 0) {
	    return ix + 1;
	}
    }
    return 0;
}

uint32_t enumtype (const string &tname)
{
    return enumtype (tname.c_str());
}

const char *enumtype (uint32_t val)
{
    const size_t numtypes = sizeof(enumtypeidx) / sizeof(*enumtypeidx);
    if (val == 0 || val > numtypes) { return ""; }
    return enumtypenames + enumtypeidx[val - 1];
}

} /*namespace IFC*/
