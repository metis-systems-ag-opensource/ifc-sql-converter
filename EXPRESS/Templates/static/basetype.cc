/* basetype.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Get the base type (tag) of a given 'ExpTypes' type tag.
**
*/

//##DEBUG
//#include <iostream>
//using std::cerr, std::endl, std::flush;
//##

#include "basetype.h"

namespace IFC {

#include "type2basetype.cc"

ExpTypes basetype (ExpTypes t)
{
    // The integer values of the ExpTypes elements start at 1, so the integer
    // value of 't' must be decreased by one for accessing the right element
    // in 't2bt[]'.
    if (t == ExpTypes::_NoType) { return t; }
    return t2bt[(unsigned) t - 1];
}

} /*namespace IFC*/
