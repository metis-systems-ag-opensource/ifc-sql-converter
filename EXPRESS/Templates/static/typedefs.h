/* typedefs.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Some small type (type alias) definitions
**
*/
#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <map>
#include <string>
#include <vector>
#include "expbase.h"

namespace IFC {

using ConfData = std::map<std::string, std::string>;

using InstanceList = std::vector<EntityBase *>;

} /*namespace IFC*/

#endif /*TYPEDEFS_H*/
