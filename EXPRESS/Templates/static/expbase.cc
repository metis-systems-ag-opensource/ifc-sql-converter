/* expbase.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
** Implementation of the member functions of the base types used for
** implementing the EXPRESS type model in C++.
**
*/
#include <cerrno>	// for the error handling routines of 'EntityBase'
#include <cstdlib>	// for the error handling routines of 'EntityBase'
#include <cstring>
#include <vector>
#include <stdexcept>

#include "utils.h"
#include "warning.h"
#include "exptypename.h"

#include "expbase.h"

namespace IFC {

using std::string, std::to_string, std::domain_error, std::range_error;

//class InstErr : public domain_error {
//public:
InstErr::InstErr (size_t instid, uint32_t attrx, const std::string &err)
    : domain_error ("Instance #" + to_string (instid) + ", attribute " +
		    to_string (attrx) + ": " + err)
{ }
//} /*InstErr*/;

//class ValueErr : public InstErr {
//public:
ValueErr::ValueErr (size_t instid, uint32_t attrx, const std::string &ats)
    : InstErr (instid, attrx, "Invalid value '" + ats + "'")
{ }
//} /*ValueErr*/;

//class TypeErr : public std::domain_error {
//public:
TypeErr::TypeErr (const std::string &expected_type)
    : domain_error ("Expected a '" + expected_type + "' value")
{ }
//} /*TypeErr*/;

//class ValIndexErr : public std::exception {
//public:
ValIndexErr::ValIndexErr (size_t index, const std::string &errmsg)
    : domain_error ("At index " + to_string (index) + " - " + errmsg)
{ }
//} /*ValIndexErr*/;

//class IndexError : public std::domain_error {
//public:
IndexError::IndexError (size_t index, size_t min, size_t max)
    : domain_error ("Index " + to_string (index) + " out of range (" +
		    to_string (min) + " ... " + to_string (max) + ")")
{ }
//} /*IndexError*/;

//class AggExpected : public std::domain_error {
//public:
AggExpected::AggExpected()
    : domain_error ("Aggregate value expected")
{ }
//} /*AggExpected*/;

//class RAggValueErr : public std:range_error {
//public:
RAggValueErr::RAggValueErr (const std::string &aggtype)
    : range_error ("Not enough or too many elements for restricted " + aggtype)
{ }
//} /*RAggValueErr*/;

//enum class TypeClass { none, entity, aggregate, select, enumeration, simple,
//                       number, binary, string, instref };
//
string to_string (TypeClass tc)
{
    switch (tc) {
	case TypeClass::none: return "none";
	case TypeClass::entity: return "entity";
	case TypeClass::aggregate: return "aggregate";
	case TypeClass::select: return "select";
	case TypeClass::enumeration: return "enumeration";
	case TypeClass::simple: return "simple";
	case TypeClass::number: return "number";
	case TypeClass::binary: return "binary";
	case TypeClass::string: return "string";
	case TypeClass::instref: return "instref";
	default: return "none<invalid cast>";
    }
}

// Base type of all EXPRESS types (simple, ENUMERATION, SELECT, ENTITY)
//struct ExpBase {
//virtual ExpBase::~ExpBase() { }
//virtual ExpTypes ExpBase::type() = 0;
//virtual bool ExpBase::is_entity() { return false; }
//} /*ExpBase*/;

//struct EntityBase : ExpBase {

bool EntityBase::is_entity() const { return true; }
//size_t EntityBase::instanceid() const { return return _instanceid; }
//void EntityBase::instanceid (size_t id) { _instanceid = id; }
bool EntityBase::is_same_as (const ExpBase *rx) const {
    auto r = dynamic_cast<const EntityBase *>(rx);
    if (! r) { return false; }
    return this == r;
}
//const ErrorListItem &errors() const { return _errors; }
void EntityBase::writeAddErr (const string &msg, uint32_t ix)
{
    _errors.add ("(" + exptypename (this) + ", attribute " +
		 std::to_string (ix) + ") " + msg);
    warning (_instanceid, /*type(),*/ ix, msg);
}
///*???*/
//    std::vector<ExpBase *> attributes;
//    ErrorList _errors;  // 'errors' requires eight bytes if unused!
//    size_t _instanceid;
//    uint32_t _dbid;
//} /*EntityBase*/;

//struct SimpleBase : ExpBase {
/*virtual*/ string SimpleBase::todb() const
{
    throw std::runtime_error
	("Database string conversion is not implemented for this type.");
}
//} /*SimpleBase*/;

//struct ExpInteger : SimpleBase {
ExpInteger::ExpInteger() : v(0) { }
ExpInteger::ExpInteger (const STEP::Value *v) {
    if (v->type() != STEP::ValType::integer) {
	throw domain_error ("Invalid 'ExpInteger' value: " + v->repr());
    }
    auto x = dynamic_cast<const STEP::IntValue *>(v);
    this->v = (long) x->value;
}
ExpInteger::ExpInteger (long v) : v(v) { }
ExpTypes ExpInteger::type() const { return ExpTypes::INTEGER; }
ExpInteger::operator long() const { return v; }
bool ExpInteger::is_same_as (const ExpBase *rx) const
{
    auto r = dynamic_cast<const ExpInteger *>(rx);
    if (! r) { return false; }
    return v == r->v;
}
string ExpInteger::todb() const { return to_string (v); }
//private:
//    long v;
//} /*ExpInteger*/;

//struct ExpReal : SimpleBase {
ExpReal::ExpReal() : v(0.0) { }
ExpReal::ExpReal (const STEP::Value *v) {
    if (v->type() == STEP::ValType::integer) {
	auto x = dynamic_cast<const STEP::IntValue *>(v);
	this->v = (double) x->value;
    } else if (v->type() == STEP::ValType::real) {
	auto x = dynamic_cast<const STEP::FloatValue *>(v);
	this->v = x->value;
    } else {
	throw domain_error ("Invalid 'ExpReal' value: " + v->repr());
    }
}
ExpReal::ExpReal (double v) : v(v) { }
ExpTypes ExpReal::type() const { return ExpTypes::REAL; }
ExpReal::operator double() const { return v; }
bool ExpReal::is_same_as (const ExpBase *rx) const
{
    auto r = dynamic_cast<const ExpReal *>(rx);
    if (! r) { return false; }
    return v == r->v;
}
string ExpReal::todb() const { return float2string (v); }
//private:
//    double v;
//} /*ExpReal*/;

//struct ExpBoolean : SimpleBase {
ExpBoolean::ExpBoolean() : v(false) { }
ExpBoolean::ExpBoolean (const STEP::Value *v) {
    if (v->type() == STEP::ValType::logical) {
	auto x = dynamic_cast<const STEP::LogicalValue *>(v);
	if (x->value == STEP::l_undefined) {
	    throw domain_error
		("LOGICAL '" + v->repr() + "' is not a valid BOOLEAN value.");
	}
	this->v = (x->value == STEP::l_false ? false : true);
    } else {
	throw domain_error ("Invalid 'ExpBoolean' value: " + v->repr());
    }
}
ExpBoolean::ExpBoolean (bool v) : v(v) { }
ExpTypes ExpBoolean::type() const { return ExpTypes::BOOLEAN; }
ExpBoolean::operator bool() const { return v; }
bool ExpBoolean::is_same_as (const ExpBase *rx) const
{

    auto r = dynamic_cast<const ExpBoolean *>(rx);
    if (! r) { return false; }
    return v == r->v;
}
string ExpBoolean::todb() const { return (v ? "TRUE" : "FALSE"); }
//private:
//    bool v;
//} /*ExpBoolean*/;

//struct ExpLogical : SimpleBase {
//    enum Value : uint8_t { l_false, l_unknown, l_true };
ExpLogical::ExpLogical() : v(l_unknown) { }
ExpLogical::ExpLogical (const STEP::Value *v) {
    if (v->type() != STEP::ValType::logical) {
	throw domain_error ("Invalid 'ExpLogical' value: " + v->repr());
    }
    auto x = dynamic_cast<const STEP::LogicalValue *>(v);
    this->v = (x->value == STEP::l_undefined ? ExpLogical::l_unknown :
	       x->value == STEP::l_false ? ExpLogical::l_false :
	       ExpLogical::l_true);
}
ExpLogical::ExpLogical (Value v) : v(v) { }
ExpLogical::ExpLogical (bool v) : v(v ? l_true : l_false) { }
ExpTypes ExpLogical::type() const { return ExpTypes::LOGICAL; }
ExpLogical::operator Value() const { return v; }
ExpLogical::operator bool() const {
    if (v == l_unknown) {
	throw std::runtime_error ("Can't convert to 'bool'");
    }
    return v == l_true;
}
bool ExpLogical::is_same_as (const ExpBase *rx) const
{
    auto r = dynamic_cast<const ExpLogical *>(rx);
    if (! r) { return false; }
    return v == r->v;
}
string ExpLogical::todb() const
{
    return (v == l_unknown ? "NULL" : (v == l_false ? "FALSE" : "TRUE"));
}
//private:
//    Value v;
//} /*ExpLogical*/;

//struct ExpNumber : SimpleBase {
ExpNumber::ExpNumber() : _is_real(false) { v.i = 0; }
ExpNumber::ExpNumber (const STEP::Value *v) {
    STEP::ValType vtype = v->type();
    if (vtype == STEP::ValType::integer) {
	auto x = dynamic_cast<const STEP::IntValue *>(v);
	this->v.i = (long) x->value; _is_real = false;
    } else if (vtype == STEP::ValType::real) {
	auto x = dynamic_cast<const STEP::FloatValue *>(v);
	this->v.r = (double) x->value; _is_real = true;
    } else {
	throw domain_error ("Invalid 'ExpNumber' value: " + v->repr());
    }
}
ExpNumber::ExpNumber (long iv) { v.i = iv; _is_real = false; }
ExpNumber::ExpNumber (double rv) { v.i = rv; _is_real = true; }
ExpTypes ExpNumber::type() const { return ExpTypes::NUMBER; }
ExpNumber::operator long() const { return _is_real ? (long) v.r : v.i; }
ExpNumber::operator double() const { return _is_real ? v.r : (double) v.i; }
bool ExpNumber::is_real() const { return _is_real; }
bool ExpNumber::is_same_as (const ExpBase *rx) const
{
    auto r = dynamic_cast<const ExpNumber *>(rx);
    if (! r) { return false; }
    return (_is_real == r->_is_real) &&
	   (_is_real ? v.r == r->v.r : v.i == r->v.i);
}
string ExpNumber::todb() const
{
    return (is_real() ? float2string (v.r) : to_string (v.i));
}
//private:
//    union { long i; double r; } v;
//    bool _is_real;
//} /*ExpNumber*/;

//struct ExpBinary : SimpleBase {
ExpBinary::ExpBinary() : v() { }
ExpBinary::ExpBinary (const STEP::Value *b)
{
    if (b->type() != STEP::ValType::bitstring) {
	throw domain_error ("Expected a BINARY value");
    }
    auto bv = dynamic_cast<const STEP::BitStringValue *>(b);
    v = bv->value;
}
ExpBinary::ExpBinary (const std::string &x) : v(string2bin (x)) { }
ExpTypes ExpBinary::type() const { return ExpTypes::BINARY; }
ExpBinary::operator string() const { return bin2string (v); }
bool ExpBinary::is_same_as (const ExpBase *rx) const
{
    auto r = dynamic_cast<const ExpBinary *>(rx);
    if (! r) { return false; }
    size_t vsize = v.size();
    bool is_equal = vsize == r->v.size();
    if (is_equal) {
	for (size_t ix = 0; ix < vsize; ++ix) {
	    if (v[ix] != r->v[ix]) { is_equal = false; break; }
	}
    }
    return is_equal;
}
string ExpBinary::todb() const { return "'" + bin2string(v) + "'"; }
//private:
//    std::vector<bool> v;
//} /*ExpBinary*/;

static char *csdup (const char *s)
{
    if (s) {
	char *r = new char[strlen (s) + 1];
	strcpy (r, s);
	return r;
    }
    return nullptr;
}

// Because of the amount of data required even for simple IFC models, i will
// not use 'std::string' here, and also not my 'lmf::string', but a simple
// 'const char *' instead. This is possible, because the strings in an IFC-
// model are
//   a) constant
//   b) typically short.
// So i'm avoiding any additional overhead (except for the pointer, which is
// always required).
//struct ExpString : SimpleBase {
ExpString::ExpString () : v(nullptr) { }
ExpString::ExpString (const STEP::Value *s)
{
    if (s->type() != STEP::ValType::string) {
	throw domain_error ("Expected a string value");
    }
    auto sv = dynamic_cast<const STEP::StringValue *>(s);
    size_t svsize = sv->value.size();
    if (sv->value.size() == 0) {
	v = nullptr;
    } else {
	auto p = new char [svsize + 1];
	memcpy (p, sv->value.data(), svsize);
	p[svsize] = '\0';
	v = p;
    }
}
ExpString::ExpString (const std::string &sv) {
    size_t svsize = sv.size();
    char *p = new char[svsize + 1];
    memcpy (p, sv.data(), svsize); p[svsize] = '\0';
    v = p;
}
ExpString::ExpString (const char *sv) {
    v = csdup (sv);
}
ExpString::ExpString (const ExpString &x) : v(csdup (x.v)) { }
ExpString::ExpString (ExpString &&x) : v(x.v) { x.v = nullptr; }
ExpTypes ExpString::type() const { return ExpTypes::STRING; }
ExpString::~ExpString() {
    if (v) { delete[] v; v = nullptr; }
}
ExpString &ExpString::operator= (const ExpString &x) {
    if (v) { delete[] v; }
    v = csdup (x.v);
    return *this;
}
ExpString &ExpString::operator= (ExpString &&x) {
    if (v) { delete[] v; }
    v = x.v; x.v = nullptr;
    return *this;
}
size_t ExpString::size() const { return v ? strlen (v) : 0; }
ExpString::operator string() const { return string (v ? v : ""); }
bool ExpString::is_same_as (const ExpBase *rx) const
{
    auto r = dynamic_cast<const ExpString *>(rx);
    if (! r) { return false; }
    const char *cpl = v, *cpr = r->v;
    return (cpl == nullptr && cpr == nullptr) || (strcmp (cpl, cpr) == 0);
}
string ExpString::todb() const
{
    auto s = string (v ? v : "");
    return s;
}
    
//private:
//    const char *v;
//} /*ExpString*/;

//struct ExpInstRef : SimpleBase {
ExpInstRef::ExpInstRef() : _is_index(false), _id(0) { }
ExpInstRef::ExpInstRef (const STEP::Value *ii)
    : _is_index(false)
{
    if (ii->type() != STEP::ValType::instid) {
	throw domain_error ("Expected an instance id");
    }
    auto iv = dynamic_cast<const STEP::InstIdValue *>(ii);
    _id = iv->id;
}
ExpInstRef::ExpInstRef (const ExpInstRef &x)
    : _is_index(x._is_index), _id(x._id)
{ }
ExpInstRef::ExpInstRef (uint32_t index)
    : _is_index(true), _id(index)
{ }
ExpInstRef::~ExpInstRef() { }
ExpTypes ExpInstRef::type() const { return ExpTypes::_InstRef; }
ExpInstRef &ExpInstRef::operator= (const ExpInstRef &x) {
    _id = x._id; _is_index = x._is_index;
    return *this;
}
bool ExpInstRef::is_same_as (const ExpBase *rx) const
{
    auto r = dynamic_cast<const ExpInstRef *>(rx);
    if (! r) { return false; }
    // Different '_is_index' values in 'this' and 'r' should probably lead
    // to an exception (don't know this yet).
    return _is_index == r->_is_index && _id == r->_id;
}
//    bool is_index() { return (bool) _is_index; }
//    void set_index (uint32_t ix) { _is_index = 1; _id = ix; }
//    uint32_t id() const { return _id; } 
//    void id (uint32_t iid) { _id = iid; }
//private:
//    uint32_t _is_index : 1, _id : 31;
//} /*ExpInstRef*/;

} /*namespace IFC*/
