/* errlist-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Small test program for 'errorlist.{cc,h}'
**
*/

#include <iostream>
#include <stdexcept>
#include <string>

#include "errlist.h"

//using namespace std;
using std::cerr, std::cout, std::endl, std::flush;
using std::exception, std::runtime_error;
using std::string, std::to_string, std::operator""s;

int main()
{
    cout << "Testing 'IFC::ErrorList' ..." << endl;
    IFC::ErrorList errors;
    cout << endl << "Printing messages on a new 'errors' (no messages yet)" <<
	    endl;
    int ix = 0;
    for (auto msg: errors) {
	cout << "#" << ++ix << ": " << msg << endl;
    }

    cout << endl << "Adding messages ..." << endl;
    errors.add ("This is an ERROR! (const char *)");
    errors.add ("This is another ERROR! (const std::string &)"s);

    cout << endl << "Printing messages (yet filled 'errors')" << endl;
    ix = 0;
    for (auto msg: errors) {
	cout << "#" << ++ix << ": " << msg << endl;
    }

    cout << endl << "Copying an error list (copy construtor)" << endl;
    IFC::ErrorList errors2 (errors);

    cout << endl << "Printing messages (of 'errors2')" << endl;
    ix = 0;
    for (auto msg: errors2) {
	cout << "#" << ++ix << ": " << msg << endl;
    }

    IFC::ErrorList errors3;
    cout << endl << "Copying an error list (copy assignment)" << endl;
    errors3 = errors;

    cout << endl << "Printing messages (of 'errors3')" << endl;
    ix = 0;
    for (auto msg: errors3) {
	cout << "#" << ++ix << ": " << msg << endl;
    }

    cout << endl << "sizeof(errors) = " << sizeof(errors) << endl;
    cout << endl << "sizeof(errors2) = " << sizeof(errors2) << endl;
    cout << endl << "sizeof(errors3) = " << sizeof(errors3) << endl;

    cout << endl << "At END" << endl;
    return 0;
}
