/* enumtest.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Testing the generated enumeration tables
**
*/

#include <iostream>
#include <string>
#include <utility>

#include "utils.h"
#include "enumval.h"

using std::cin, std::cout, std::cerr, std::endl;
using std::string, std::to_string;

using namespace IFC;

void trim (string &s)
{
    static const char blank[] = "\1\2\3\4\5\6\a\b\t\n\v\f\r\16\17\20\21\22\23"
				"\24\25\26\27\30\31\32\33\34\35\36\37 \177";
    size_t xnc = s.find_first_not_of (blank);
    if (xnc > 0) { s.erase (0, xnc); }
    xnc = s.find_last_not_of (blank);
    if (xnc < s.size()) { s.erase (xnc + 1); }
}


bool getword (const string &prompt, string &word)
{
    string line;
    for (;;) {
	cout << prompt << ": "; cout.flush();
	readline (cin, line);
	if (cin.eof()) { cout << "<eof>" << endl;  return false; }
	trim (line);
	if (! line.empty()) { break; }
    }
    word = std::move (line);
    return true;
}

bool gettype (uint32_t &tx)
{
    string type;
    while (getword ("Enum type", type)) {
	uint32_t x = enumtype (type);
	if (x > 0) { tx = x; return true; }
	cerr << "*** No such type '" << type << "'. Try again, please!" << endl;
    }
    return false;
}

int main()
{
    string type, value;
    uint32_t tx = 0;
    if (gettype (tx)) {
	const char *tname = enumtype (tx);
	cout << "Type: " << tx << " (" << enumtype (tx) << ")" << endl;
	while (getword ("Enum value", value)) {
	    const char *name = nullptr;
	    uint32_t vx = enumval (tx, value);
	    if (vx == 0) {
		cerr << "*** '" << enumtype (tx) << "' has no value '" <<
			value << "'" << endl;
		continue;
	    }
	    name = enumname (vx);
	    cout << "'" << tname << "::" << name << "' = " << vx << endl;
	}
    }
    cout << endl << "At END" << endl;
    return 0;
}
