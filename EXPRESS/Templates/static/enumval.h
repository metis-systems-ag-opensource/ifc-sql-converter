/* enumval.h
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** Interface file for 'enumval.cc'
** (Functions for the associations between ENUMERATION names and values).
**
*/
#ifndef ENUMTABS_H
#define ENUMTABS_H

#include <cstdint>
#include <string>

namespace IFC {

uint32_t enumval (uint32_t rngix, const char *name);
uint32_t enumval (uint32_t rngix, const std::string &name);
const char *enumname (uint32_t val);
uint32_t enumtype (const char *itname);
uint32_t enumtype (const std::string &tname);
const char *enumtype (uint32_t val);

} /*namespace IFC*/

#endif /*ENUMTABS_H*/
