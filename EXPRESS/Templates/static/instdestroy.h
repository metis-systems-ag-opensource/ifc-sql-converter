/* instdestroy.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Interface file for 'instdestroy.cc'
** (Destroy an entity instance (an 'EntityBase' object). This function is
**  required from within the STEP parser, but 'delete <instptr>' cannot be used
**  directly, as 'EntityBase' is completely "abstract" whtin it. So, this
**  destroying must be done within the plugin, as the plugin knows the internal
**  structure of 'EntityBase', and so can invoke 'delete' on a pointer of such
**  an object.)
**
*/
#ifndef INSTDESTROY_H
#define INSTDESTROY_H

#include "expbase.h"

namespace IFC {

void destroy_instance (EntityBase *p);

} /*namespace IFC*/

#endif /*INSTDESTROY_H*/
