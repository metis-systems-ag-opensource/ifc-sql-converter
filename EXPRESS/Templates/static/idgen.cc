/* ifcmodel.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Implementation part of the 'ifcmodel' module.
** (Id generator to be used for generating the ids for the database tables.)
**
*/

#include "idgen.h"

namespace IFC {

//enum class IdType { entity = 0, number = 1, aggregate = 2, select = 3 };

//struct IdGen {

#define vsize(x) (sizeof(x) / sizeof(*(x)))

IdGen::IdGen (uint32_t modelid)
  : _modelid(modelid), _objid(1)
{
//##was:    for (unsigned ix = 0; ix < vsize(_ids); ++ix) { _ids[ix] = 1; }
}

IdGen::~IdGen() { }

uint32_t IdGen::next_objid ()
//##was: uint32_t IdGen::next_id (IdType idt)
{
    return _objid++;
//##was:    return _ids[(int) idt]++;
}

uint32_t IdGen::objid()
{
    return _objid;
}

uint32_t IdGen::modelid() { return _modelid; }

//private:
//    uint32_t _modelid; // The id of the IFCModel.Model table in the database
//    uint32_t _objid;
////##was:    uint32_t _ids[4];
//} /*IdGen*/;

} /*namespace IFC*/
