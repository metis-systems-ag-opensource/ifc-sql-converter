/* dbif.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Primitive database interface (based on the MySQL/MariaDB C interface).
**
*/
#ifndef DBIF_H
#define DBIF_H

#include <optional>
#include <string>
#include <mysql.h>

class DBQ {
public:
    DBQ (MYSQL *mysql = nullptr); // Default _and_ value constructor
    ~DBQ();
    bool next();
    std::optional<std::string> operator[] (unsigned ix);
    unsigned int columns();
    DBQ &operator= (const DBQ &x);
    DBQ &operator= (DBQ &&x);
    void end();
private:
    MYSQL *_dbh;		// MySQL/MariaDB database handle
    MYSQL_RES *_qres;		// Result handle of the query
    MYSQL_ROW _qrrow;		// Current result row (or nullptr)
    unsigned int _cc;		// Column count of the result row(s)
    bool _eoq;			// End Of Query result
};

class DBH {
public:
    DBH();

    DBH (const std::string &host, int port,
	 const std::optional<std::string> &db,
	 const std::string &user, const std::string &password);

    DBH (const DBH &x);

    ~DBH();
    void connect();
    bool selectdb (const std::optional<std::string> &db);
    const std::optional<std::string> &currentdb();
    int nrquery (const std::string &q);

    DBQ query (const std::string &q);
    uint64_t insert_id ();
    bool autocommit (bool acon);
    bool commit();
    bool rollback();
    bool begin();
    int errcode();
    std::string errmsg();
    std::string quote (const std::string &s);
private:
    MYSQL *_dbh; MYSQL *_dbh1;
    std::string _host, _username, _password;
    std::optional<std::string> _database;
    unsigned int _port;
};

#endif /*DBIF_H*/
