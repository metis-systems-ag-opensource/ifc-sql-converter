/* fixrefs.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Fix the instance references and type check them along the way
**
*/

//##DEBUG
#include <iostream>
using std::cerr, std::endl, std::flush;
//##

#include <algorithm>
#include <stdexcept>
#include <string>
#include <utility>

#include "chelpers.h"
#include "selbase.h"
#include "utils.h"
#include "aggtypes.h"

#include "fixrefs.h"

#ifndef USING_INDEXREFS
# define USING_INDEXREFS 0
#endif

namespace IFC {

using std::sort;
using std::domain_error, std::range_error, std::runtime_error;
using std::string, std::to_string;

struct RefIndex {
    uint32_t instance_id;
    uint32_t index;
};

using CrossList = std::vector<RefIndex>;

static void fix_instrefs (EntityBase *inst, InstanceList &instances);

void fix_instancerefs (InstanceList &instances)
{
    // Instances are (probably) sorted by their ids. But to be sure is better
    // than an algorithm which does not work because of a wrong assumption, so
    // the list of instances (table) will be sorted here again. This sorting is
    // required for the binary search being effective.
    sort (instances.begin(), instances.end(),
	  [] (EntityBase *a, EntityBase *b) {
	      return a->instanceid() < b->instanceid();
	  });
    // After a sorted instance list, each instance is searched for attributes
    // which are (or consist of) instance references. Any instance id in such
    // an instance reference is then replaced by (either) an index into the
    // instance list (or a direct reference to the matching instance). 
    for (auto inst: instances) {
	fix_instrefs (inst, instances);
    }
}


/*************************************************************
 * Internal functions                                        *
 *************************************************************/

// Return the either the article 'an' or 'a', depending on the word (partial
// sentence) beginning with a vocal (except 'u') or not.
//
static string word_article (const string &word)
{
    const string aeiou ("AEIOaeio");
    if (word.empty()) { return ""; }
    return aeiou.find (word[0]) == string::npos ? "a" : "an";
}

// Return either "<>", or an IFC entity name or a list of the format
// "one of (<entity name>, ..., <entity name>)". This helper function is meant
// solely for the type error exception possibly generated in 'fix_instref()'.
//
static string types2string (const TypeList &tl)
{
    string res;
    size_t ressz;
    Fifo<string> typenames;
    auto [tlp, tlsz] = tl;
    for (size_t ix = 0; ix < tlsz; ++ix) {
	string &&ttname = id2name (tlp[ix]);
	ressz += ttname.size() + 2;
	typenames.push (ttname);
    }
    if (typenames.empty()) { return "<>"; }
    if (typenames.size() == 1) {
	res = typenames.shift();
    } else {
	bool first = true;
	ressz += 10;
	res.reserve (ressz);
	res = "one of (";
	while (! typenames.empty()) {
	    if (first) { first = false; } else { res += ", "; }
	    res += typenames.shift();
	}
	res += ")";
    }
    return res;
}

// Searching for an element with a given instance id ('instid') in the instance
// list 'instances'. I'm using a modification of the classical binary search
// algorithm here which returns the index of the first matching element.
// The result is either the index of the element found or the size of
// 'instances' - marking that the element was not found.
//
static uint32_t find_instance (uint32_t instid, InstanceList &instances)
{
    uint32_t l = 0, r = instances.size() - 1;
    while (l < r) {
	uint32_t m = (l + r) / 2;
	if (instances[m]->instanceid() < instid) { l = m + 1; } else { r = m; }
    }
    return (instances[l]->instanceid() == instid ? l : instances.size());
}

// Fix a single 'ExpInstRef' value, after type checking the value referenced
// by this value. Fixing consists of either replacing the instance id in this
// value by an index into the given instance list, or by replacing this value
// by a pointer to the instance referenced by it.
// The reference to a pointer to the attribute is required here in the case
// i'm making the decision that an instance should be replaced with a direct
// reference (a pointer) to the given instance ... in this case, the pointer
// value must be replaced.
static bool fix_instref (ExpBase * &attr, uint32_t attrx, EntityBase *inst,
			 InstanceList &instances)
{
    auto iref = dynamic_cast<ExpInstRef *>(attr);
    if (! iref) { return false; }
    if (iref->is_index()) { /* All work already done! */ return true; }

    // Get the instance id from the 'ExpInstRef *' value
    uint32_t instid = iref->id();

    // Try to find 'instid' in the instance list ...
    uint32_t instx = find_instance (instid, instances);

    if (instx == instances.size()) {
	// This is an error (for IFC!) In other cases, this must
	// be resolved in some other way.
	throw runtime_error ("Instance #" + to_string (instid) + " not found");
    }

    // Get a reference to the found instance.
    EntityBase *refinst = instances[instx];

    // This code is (currently) unable to detect indirect self references,
    // because this required a more complex graph analysis, but detecting a
    // direct self reference is no problem here, so i'm doing it.
    if (inst == refinst) {
	throw runtime_error
	    ("Self reference (direct) to an instance detected in attribute " +
	     to_string (attrx) + " of this same instance (#" +
	     to_string (inst->instanceid()) + ")");
    }

    // Performing the postponed type check on the instance found in the
    // list. If the type of the instance doesn't match at least one of the
    // types of the given (instance reference) attribute, an exception is
    // generated.
    ExpTypes insttype = inst->type(), refinsttype = refinst->type();
    if (! check_attrtype (refinsttype, insttype, attrx, true)) {
	string instname = id2name (insttype);
	string refinstname = id2name (refinsttype);
	TypeList tl = expected_types (insttype, attrx, true);
	string art = word_article (refinstname);
	throw domain_error
	    ("Type mismatch in attribute " + to_string (attrx) +
	     " of instance #" + to_string (inst->instanceid()) +
	     "(" + instname + ")" + EOL "Found " + art + " " +
	     refinstname + " but expected " + types2string (tl));
    }

    // Now fix the instance reference.
#if USING_INDEXREFS
    // In the case of using indexes, the 'ExpInstRef' entry remains
    // but its content must be converted.
    iref->set_index (instx);
#else
    // In the case of using direct references, the 'ExpInstRef'
    // attribute must be removed in favour of a direct reference
    // (pointer) to the referenced entity. For this reason, i
    // declared the iteration variable 'attr' as 'auto &' above,
    // and not as 'auto'.
    delete attr; attr = refinst;
#endif
    return true;
}

static bool fix_selattr (ExpBase * &attr, uint32_t aggx, EntityBase *inst,
			 InstanceList &instances);

// Fix the 'ExpInstRef' values possibly contained in a given aggregate
// attribute. Because of the EXPRESS type system, i cannot decide yet, if the
// aggregate contains instance references or other values or any combination
// of them, so i must examine each element in each aggregate typed attribute
// separately. Because of the possibility of multi-dimensional aggregates,
// this function is recursive.
//
static bool fix_instaggrefs (ExpBase * &attr, uint32_t aggx, EntityBase *inst,
			     InstanceList &instances)
{
    bool done = true;
    auto aggref = dynamic_cast<AggBase *>(attr);
    if (! aggref) { return false; }
    for (uint32_t ix = 0; ix < aggref->size(); ++ix) {
	ExpBase * &aggel = aggref->at (ix);
	switch (aggel->typeclass()) {
	    case TypeClass::instref: {
		done = fix_instref (aggel, aggx, inst, instances);
		break;
	    }
	    case TypeClass::aggregate: {
		done = fix_instaggrefs (aggel, aggx, inst, instances);
		break;
	    }
	    case TypeClass::select: {
		done = fix_selattr (aggel, aggx, inst, instances);
		break;
	    }
	    default: break;
	}
    }
    return done;
}

static bool fix_selattr (ExpBase * &attr, uint32_t aggx, EntityBase *inst,
			 InstanceList &instances)
{
    bool done = true;
    auto &&selattr = dynamic_cast<_ExpSelect *>(attr);
    auto &selval = selattr->get();
    switch (selval->typeclass()) {
	case TypeClass::instref: {
	    // Found an instance reference. This reference is now
	    // converted (either) into an index into the instance list
	    // (or a direct reference to the corresponding instance).
	    done = fix_instref (selval, aggx, inst, instances);
	    break;
	}
	case TypeClass::aggregate: {
	    done = fix_instaggrefs (attr, aggx, inst, instances);
	    break;
	}
	default: break;
    }
    return done;
}

// Fix the 'ExpInstRef' attributes of a given instance. Because this includes
// a search in the complete list of instances, this list must additionally be
// supplied as argument.
//
static void fix_instrefs (EntityBase *inst, InstanceList &instances)
{
    // Search for instance references (simple type) in the attributes of the
    // examined instance 'inst', search the matching instances in the list,
    // type-check the attribute against this instance and then either replace
    // the instance id in this attribute with the index of the instance found,
    // or with a direct pointer to it. (Don't know which method i will choose,
    // because both have advantages and drawbacks, and i prepared the code in
    // such a way that i can use either of them.)
    uint32_t nx = 0;
    for (auto &attr: inst->attributes) {
	uint32_t ix = nx++;
	if (attr) {
	    switch (attr->typeclass()) {
		case TypeClass::instref: {
		    // Found an instance reference. This reference is now
		    // converted (either) into an index into the instance list
		    // (or a direct reference to the corresponding instance).
		    fix_instref (attr, ix, inst, instances);
		    break;
		}
		case TypeClass::aggregate: {
		    fix_instaggrefs (attr, ix, inst, instances);
		    break;
		}
		case TypeClass::select: {
		    // Because all SELECT-types were flattened, a simple
		    // dereferencing and examination of the dereferenced
		    // element is required here...
		    fix_selattr (attr, ix, inst, instances);
		    break;
		}
		default: break;
	    }
	}
    }
}

} /*namespace IFC*/
