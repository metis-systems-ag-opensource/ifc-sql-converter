/* pds.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Store a single entity instance (with all 
**
*/
#ifndef PDS_H
#define PDS_H

#include <cstdint>
#include <map>
#include <string>

#include "expbase.h"
#include "fixrefs.h"
#include "idgen.h"
#include "plctx.h"

namespace IFC {

using TypeIdMap = std::map<std::string, uint32_t>;

uint32_t store_entinst (PlCtx &ctx, IdGen &id, EntityBase *ent,
			InstanceList &instances, TypeIdMap &tim);

TypeIdMap get_aggtypeids (PlCtx &ctx);

} /*namespace IFC*/

#endif /*PDS_H*/
