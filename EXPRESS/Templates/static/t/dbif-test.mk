# dbif-test.mk
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# License: MIT License
# 
# Small Makefile for testing the 'dbif.{cc,h}' interface.
#

CC = g++
LD = g++

CFLAGS = -std=c++17 -Wall -g
LDFLAGS = -g
INCL = -I. -I..
DBINCL = $(shell mariadb_config --include)
LIBS =
DBLIBS = $(shell mariadb_config --libs)

TARGET = dbif-test
OBJS = dbif-test.o dbif.o

%.o : %.cc
	$(CC) $(CFLAGS) $(INCL) $(DBINCL) -c $< -o $@

%.o : ../%.cc
	$(CC) $(CFLAGS) $(INCL) $(DBINCL) -c $< -o $@

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LD) $(LDFLAGS) $(OBJS) $(LIBS) $(DBLIBS) -o $@

dbif.o: ../dbif.cc ../dbif.h

dbif-test.o: dbif-test.cc ../dbif.h

clean:
	@rm -f $(TARGET) $(OBJS)
