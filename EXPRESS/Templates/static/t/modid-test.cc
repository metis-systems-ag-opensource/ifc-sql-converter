/* modid-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Test program for 'modid.{cc,h}'
**
*/

#include <cstring>
#include <iostream>
#include <string>
#include <stdexcept>

#include "modid.h"

using std::cerr, std::cout, std::endl;

int main (int argc, char *argv[])
{
    const char *prog = strrchr (*argv, '/');
    if (prog) { ++prog; } else { prog = *argv; }

    IFC::ModId mid;

    if (argc < 2) {
	cerr << "Usage: " << prog << " arg..." << endl;
	return 64;
    }

    for (int ix = 1; ix < argc; ++ix) {
	mid.update (argv[ix]);
    }

    mid.finalize();

    cout << mid.outhex() << endl;

    return 0;
}
