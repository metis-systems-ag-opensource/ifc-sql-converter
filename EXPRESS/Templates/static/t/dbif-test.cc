/* dbif-test.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Small program for testing the main functionality of my C++ DB wrapper
** module.
**
*/

#include <iostream>
#include <stdexcept>
#include <string>

#include "dbif.h"

using std::runtime_error;
using std::cout, std::cerr, std::endl, std::flush;
using std::string, std::to_string, std::stoi, std::stol, std::stoul;
using std::stoll, std::stoull;

int main()
{
    cout << "Initialising database handle ..." << flush;
    DBH dbh ("localhost", 3306, "Tests", "test", "trakkatax");
    cout << " done." << endl;

    cout << "Connecting server and database ..." << flush;
    dbh.connect();
    cout << " done." << endl;

    string x = "abc' defäöü\n' 3344";
    cout << "Quoting |" << x << "| => |" << dbh.quote (x) << "|\n";

    cout << "Creating a table ('test1') ..." << flush;
    int rc = dbh.nrquery ("CREATE TABLE IF NOT EXISTS test1 (\n"
			  "  `id` INTEGER NOT NULL AUTO_INCREMENT,\n"
			  "  `name` VARCHAR(128),\n"
			  "  age INTEGER,\n"
			  "  female BOOL,\n"
			  "  PRIMARY KEY (`id`)\n"
			  ")");

    if (rc) {
	throw runtime_error ("Creating 'test1' failed - " + dbh.errmsg());
    }
    cout << " done." << endl;

    cout << "Inserting data into 'test1' ..." << flush;
    rc = dbh.nrquery (
	"INSERT INTO test1 VALUES\n"
	" (NULL, 'Boris', 54, FALSE),\n"
	" (NULL, 'Alexandra', 53, TRUE),\n"
	" (NULL, 'Mirko', 52, FALSE),\n"
	" (NULL, 'Katja', 47, TRUE),\n"
	" (NULL, 'Liam', 6, FALSE),\n"
	" (NULL, 'Maya', 4, TRUE)"
    );

    if (rc) {
	throw runtime_error ("INSERT statement failed - " + dbh.errmsg());
    }
    cout << " done." << endl;

    cout << "Selecting \"females\" from 'test1'" << flush;
    DBQ qh = dbh.query ("SELECT * FROM test1 WHERE female = TRUE");
    cout << " done." << endl;

    cout << endl << "Result:" << endl;
    while (qh.next()) {
	unsigned cols = qh.columns();
	bool first = true;
	for (unsigned cc = 0; cc < cols; ++cc) {
	    if (first) { first = false; } else { cout << " | "; }
	    cout << (qh[cc] ? *qh[cc] : "<null>");
	}
	cout << endl;
    }

    cout << endl << "Cleaning up (removint table 'test1')" << flush;
    rc = dbh.nrquery ("DROP TABLE test1");

    if (rc) {
	throw runtime_error ("Removing 'test1' failed - " + dbh.errmsg());
    }
    cout << " done." << endl;


    //delete qh; qh = nullptr;

    //cout << endl << "Closing database ...";
    //delete dbh; dbh = nullptr;

    cout << endl << "At END" << endl;
    return 0;
}
