# modid-test.mk
#
# $Id$
#
# Author: Boris Jakubith
# E-Mail: b.jakubith@metis-ag.com
# Copyright: (c) 2020, Metis AG
# License: MIT License
# 
# Small Makefile for testing the 'modid.{cc,h}' module.
#

CC = g++
LD = g++

CFLAGS = -std=c++17 -Wall -g
LDFLAGS = -g
INCL = -I. -I..
LIBS =

TARGET = modid-test
OBJS = modid-test.o modid.o

%.o : %.cc
	$(CC) $(CFLAGS) $(INCL) -c $< -o $@

%.o : ../%.cc
	$(CC) $(CFLAGS) $(INCL) -c $< -o $@

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LD) $(LDFLAGS) $(OBJS) $(LIBS) $(DBLIBS) -o $@

modid.o: ../modid.cc ../modid.h

modid-test.o: modid-test.cc ../modid.h

clean:
	@rm -f $(TARGET) $(OBJS)
