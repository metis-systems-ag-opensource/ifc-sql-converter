/* percop.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Small test program for the '%' (format) operator
**
*/

#include <iostream>

#include "utils.h"

using std::cout, std::endl;
using std::string, std::to_string;
using std::vector;

int main()
{
    string s = "$1 ($# : $#) $2";
    vector<string> v { "v1", "x1" };
    string res = s % v;

    cout << "s = \"" << s << "\"" << endl;
    cout << "v = {\"" << v[0] << "\", \"" << v[1] << "\"}" << endl;
    cout << "res = \"" << res << "\"" << endl;

    cout << endl << "At END" << endl;
    return 0;
}
