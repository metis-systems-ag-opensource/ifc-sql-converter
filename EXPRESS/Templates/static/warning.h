/* warning.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Interface file for 'warning.cc'
** (A small function which writes a warning/error message to 'std::cerr'.)
**
*/
#ifndef WARNING_H
#define WARNING_H

#include <string>

namespace IFC {

void warning (uint32_t instid, uint32_t attrx, const std::string &msg,
	      const std::string &query = "", const std::string &errmsg = "");
void warning (uint32_t instid, const std::string &msg,
	      const std::string &query = "", const std::string &errmsg = "");
void warning (const std::string &msg,
	      const std::string &errmsg = "", const std::string &query = "");

} /*namespace IFC*/

#endif /*WARNING_H*/
