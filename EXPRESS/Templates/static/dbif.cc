/* dbif.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Primitive database interface (based on the MySQL/MariaDB C interface).
**
*/

#include <stdexcept>

#include "dbif.h"

using std::runtime_error;
using std::optional;
using std::string, std::to_string;

//class DBQ {
//public:
DBQ::DBQ (MYSQL *mysql)
  : _dbh(mysql), _qres(nullptr), _qrrow(nullptr), _cc(0), _eoq(false)
{
    if (_dbh) {
	if (! (_qres = mysql_use_result (mysql))) {
	    throw runtime_error ("DBQ::DBQ(): Result access failed.");
	}
	_cc = mysql_field_count (mysql);
    }
}

DBQ::~DBQ() { end(); }

// Retrieve the next row from the query result. Return 'true' if there was a
// next row and 'false' otherwise.
bool DBQ::next()
{
    if (_eoq) { return false; }
    _eoq = (_qrrow = mysql_fetch_row (_qres)) == nullptr;
    return ! _eoq;
}

// Access a single value of the current result row of the current query. If no
// row was retrieved yet, this function invokes 'DBQ::next()' for doing this.
// There is no need to declare the result as 'const' here, because it it always
// constructed as a _copy_ from (probably constant) values within the object.
// Additionally, it is not clear (yet) if DBQ values should be marked 'const'
// anywhere (even as function parameters), because even this 'operator[]()' is
// probably modifying a DBQ value.
// The result of 'operator[]()' _must_ be an 'optional<string>' value, because
// the column value selected from the database may contain no value (NULL).
optional<string> DBQ::operator[] (unsigned ix)
{
    if (! _qrrow && ! next()) {
	throw runtime_error ("DBQ::[]: No more data.");
    }
    if (ix < 0 || ix >= _cc) {
	throw runtime_error ("DBQ::[]: Index out of range.");
    }
    const char *x = _qrrow[ix];
    return (x ? optional<string> (string (x)) : optional<string>());
}

// Any query result has a fixed number of columns, which is returned through
// 'DBQ::columns()'.
unsigned int DBQ::columns() { return _cc; }

// Copy the content of a locally created query result (using 'DBH::query()')
// to the current query handle. This allows for using a variable of the type
// DBQ to be used (e.g.) as argument to a function.
DBQ &DBQ::operator= (const DBQ &x)
{
    end();	// End this query (if open) before assigning a new one
    _dbh = x._dbh; _qres = x._qres; _qrrow = x._qrrow; _cc = x._cc;
    _eoq = x._eoq;
    return *this;
}

// Move the content of a locally created query result (using 'DBH::query()')
// to the current query handle. This allows for using a variable of the type
// DBQ to be used more than once.
DBQ &DBQ::operator= (DBQ &&x)
{
    end();	// End this query (if open) before assigning a new one
    _dbh = x._dbh; x._dbh = nullptr;
    _qres = x._qres; x._qres = nullptr;
    _qrrow = x._qrrow; x._qrrow = nullptr;
    _cc = x._cc; x._cc = 0;
    _eoq = x._eoq; x._eoq = false;
    return *this;
}

// End an open query and clean up the query handle, making it available for
// new query.
void DBQ::end()
{
    if (_qres) { mysql_free_result (_qres); _qres = nullptr; }
    _dbh = nullptr; _qrrow = nullptr; _cc = 0; _eoq = false;
}

//private:
//    MYSQL *_dbh;		// MySQL/MariaDB database handle
//    MYSQL_RES *_qres;		// Result handle of the query
//    MYSQL_ROW _qrrow;		// Current result row (or nullptr)
//    unsigned int _cc;		// Column count of the result row(s)
//    bool _eoq;		// End Of Query result
//} /*DBQ*/;

//class DBH {
//public:

DBH::DBH()
  : _dbh(mysql_init (nullptr)), _dbh1(nullptr), _host(),
    _username(), _password(), _database(), _port(0)
{ }

DBH::DBH (const string &host, int port, const optional<string> &db,
	  const string &user, const string &password)
  : _dbh(mysql_init (nullptr)), _dbh1(nullptr), _host(host),
    _username(user), _password(password), _database(db), _port(port)
{ }

DBH::DBH (const DBH &x)
  : _dbh(mysql_init (x._dbh)), _dbh1(x._dbh1), _host(x._host),
    _username(x._username), _password(x._password), _database(x._database),
    _port(x._port)
{ }

DBH::~DBH() { mysql_close (_dbh); }

void DBH::connect()
{
    if (! _dbh1) {
	_dbh1 = mysql_real_connect (
	    _dbh, _host.c_str(), _username.c_str(), _password.c_str(),
	    (_database ? _database->c_str() : nullptr), _port, nullptr,
	    CLIENT_MULTI_RESULTS
	);
	if (! _dbh1) {
	    throw runtime_error
		(string ("DBH::connect(): ") + mysql_error (_dbh));
	}
    }
}

bool DBH::selectdb (const optional<string> &db)
{
    int rc = mysql_select_db (_dbh1, (db ? db->c_str() : nullptr));
    bool rf = rc == 0;
    if (rf) { _database = db; }
    return rf;
}

const std::optional<std::string> &DBH::currentdb()
{
    return _database;
}

int DBH::nrquery (const string &q)
{
    if (! _dbh1) {
	throw runtime_error ("DB::nrquery(): Not connected");
    }
    int qrc = mysql_real_query (_dbh1, q.c_str(), q.size());
    return qrc;
}

DBQ DBH::query (const string &q)
{
    if (!_dbh1) {
	throw runtime_error ("DB::query(): Not connected");
    }
    int qrc = mysql_real_query (_dbh1, q.c_str(), q.size());
    if (qrc) {
	throw runtime_error ("DB::query(): Couldn't get result - " + errmsg());
    }
    
    return DBQ (_dbh1);
}

uint64_t DBH::insert_id () { return mysql_insert_id (_dbh1); }

bool DBH::autocommit (bool acon) { return mysql_autocommit(_dbh1, acon); }

bool DBH::commit() { return mysql_commit (_dbh1) != 0; }

bool DBH::rollback() { return mysql_rollback(_dbh1) != 0; }

bool DBH::begin() { return nrquery ("START TRANSACTION") != 0; }

int DBH::errcode() { return mysql_errno (_dbh1); }

string DBH::errmsg() { return mysql_error (_dbh1); }

string DBH::quote (const string &s)
{
    size_t ssize = s.size();
    char *qcs = new char[ssize * 2 + 1];
    size_t qcslen = mysql_real_escape_string (_dbh1, qcs, s.c_str(), ssize);
    string qs (qcs, qcslen);
    delete[] qcs;
    return qs;
}
//private:
//    MYSQL *_dbh, *_dbh1;
//    string _host, _database, _username, _password;
//    unsigned int _port;
//};
