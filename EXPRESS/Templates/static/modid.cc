/* modid.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generate a model id – not for identifying the IFC model in the 'ifc*'
** databases, but for identifying the model itself in the 'IFCModel' database.
**
*/

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <stdexcept>

#include "modid.h"

namespace IFC {

using std::string;
using std::runtime_error;
using std::vector;

#define MODID_STLEN 4
#define MODID_STSZ (MODID_STLEN * sizeof (uint32_t));
#define RES_SIZE 16
struct ModIdCTX {
    uint32_t state[MODID_STLEN];
    uint32_t count[2];
    unsigned char buffer[64];
    ModIdCTX (const ModIdCTX &x) { *this = x; }
    ModIdCTX() { init(); }
    void init();
    void update (const unsigned char *msg, size_t msglen);
    void finalize (unsigned char *s, size_t ressz = RES_SIZE);
};

ModId::ModId() : modidstate(new ModIdCTX) { }

ModId::ModId (const ModId &x) : modidstate(new ModIdCTX (*x.modidstate)) { }

ModId::ModId (ModId &&x)
    : modidstate (x.modidstate)
{
    x.modidstate = nullptr;
}

ModId::~ModId()
{
    delete modidstate; modidstate = nullptr;
}

void ModId::update (const char *msg, size_t msglen)
{
    modidstate->update ((const unsigned char *) msg, msglen);
}

void ModId::update (const string &msg)
{
    update (msg.c_str(), msg.size());
}

void ModId::finalize()
{
    modidstate->finalize (id);
}

string ModId::outhex()
{
    static const char *hd = "0123456789abcdef";
    string res;
    res.reserve (sizeof(id) * 2);
    for (auto x: id) {
	res.append (1, hd[x >> 4]);
	res.append (1, hd[x & 0xF]);
    }
    return res;
}

string ModId::outb64()
{
    static const char entab[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',	/* 00-07 */
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',	/* 08-0F */
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',	/* 10-17 */
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',	/* 18-1F */
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',	/* 20-27 */
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',	/* 28-2F */
	'w', 'x', 'y', 'z', '0', '1', '2', '3',	/* 30-37 */
	'4', '5', '6', '7', '8', '9', '+', '/',	/* 38-3F */
	'='
    };
    if (sizeof(id) < RES_SIZE) {
	throw runtime_error
	    ("FATAL! size of the internal id is less than 16 bytes");
    }
    string res;
    res.reserve (sizeof(id) * 8 / 6 + 1);
    unsigned char x = 0, s;
    for (uint32_t ix = 0; ix < RES_SIZE;) {
	x = id[ix++]; res.append (1, entab[(x >> 2) & 63]);
	s = x & 3;
	x = id[ix++]; res.append (1, entab[(s << 4) | ((x >> 4) & 15)]);
	s = x & 15;
	x = id[ix++]; res.append (1, entab[(s >> 2) | ((x >> 6) & 3)]);
	res.append (1, entab[x & 63]);
    }
    return res;
}

vector<unsigned char> ModId::outraw()
{
    vector<unsigned char> res;
    res.reserve (sizeof(id));
    for (auto x: id) { res.push_back (x); }
    return res;
}

enum State {
    S11 = 7, S12 = 12, S13 = 17, S14 = 22, S21 = 5,
    S22 = 9, S23 = 14, S24 = 20, S31 = 4, S32 = 11,
    S33 = 16, S34 = 23, S41 = 6, S42 = 10, S43 = 15,
    S44 = 21
};

#define F(x, y, z) (((x) & (y)) | (~(x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & ~(z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | ~(z)))

#define lrot(n, x) (((x) << (n)) | ((x) >> (32 - (n))))

#define FF(a, b, c, d, x, s, ac) do { \
    (a) += F ((b), (c), (d)) + (x) + (uint32_t) (ac); \
    (a) = lrot ((s), (a)); \
    (a) += (b); \
} while (0)

#define GG(a, b, c, d, x, s, ac) do { \
    (a) += G ((b), (c), (d)) + (x) + (uint32_t)(ac); \
    (a) = lrot ((s), (a)); \
    (a) += (b); \
} while (0)

#define HH(a, b, c, d, x, s, ac) do { \
    (a) += H ((b), (c), (d)) + (x) + (uint32_t)(ac); \
    (a) = lrot ((s), (a)); \
    (a) += (b); \
} while (0)

#define II(a, b, c, d, x, s, ac) do { \
    (a) += I ((b), (c), (d)) + (x) + (uint32_t)(ac); \
    (a) = lrot ((s), (a)); \
    (a) += (b); \
} while (0)

void ModIdCTX::init()
{
    count[0] = count[1] = 0;
    state[0] = 0x67452301;
    state[1] = 0xefcdab89;
    state[2] = 0x98badcfe;
    state[3] = 0x10325476;
    memset (buffer, 0, sizeof(buffer));
}

static void Encode (unsigned char *output, uint32_t *input, size_t len)
{
    size_t i, j;

    for (i = 0, j = 0; j < len; i++, j += 4) {
        output[j] = (unsigned char)(input[i] & 0xff);
        output[j+1] = (unsigned char)((input[i] >> 8) & 0xff);
        output[j+2] = (unsigned char)((input[i] >> 16) & 0xff);
        output[j+3] = (unsigned char)((input[i] >> 24) & 0xff);
    }
}

static void Decode (uint32_t *output, const unsigned char *input, size_t len)
{
    size_t i, j;

    for (i = 0, j = 0; j < len; i++, j += 4) {
        output[i] = ((uint32_t)input[j]) |
		    (((uint32_t)input[j+1]) << 8) |
		    (((uint32_t)input[j+2]) << 16) |
		    (((uint32_t)input[j+3]) << 24);
    }
}

static void transform (uint32_t state[4], const unsigned char block[64])
{
    uint32_t a = state[0], b = state[1], c = state[2], d = state[3], x[16];

    Decode (x, block, 64);

/* Round 1 */
    FF (a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
    FF (d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
    FF (c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
    FF (b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
    FF (a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
    FF (d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
    FF (c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
    FF (b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
    FF (a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
    FF (d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
    FF (c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
    FF (b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
    FF (a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
    FF (d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
    FF (c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
    FF (b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

/* Round 2 */
    GG (a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
    GG (d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
    GG (c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
    GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
    GG (a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
    GG (d, a, b, c, x[10], S22,  0x2441453); /* 22 */
    GG (c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
    GG (b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
    GG (a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
    GG (d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
    GG (c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
    GG (b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
    GG (a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
    GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
    GG (c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
    GG (b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

/* Round 3 */
    HH (a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
    HH (d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
    HH (c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
    HH (b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
    HH (a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
    HH (d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
    HH (c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
    HH (b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
    HH (a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
    HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
    HH (c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
    HH (b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
    HH (a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
    HH (d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
    HH (c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
    HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */

/* Round 4 */
    II (a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
    II (d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
    II (c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
    II (b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
    II (a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
    II (d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
    II (c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
    II (b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
    II (a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
    II (d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
    II (c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
    II (b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
    II (a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
    II (d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
    II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
    II (b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */

    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;

    /* Zeroize sensitive information.  */
    memset (x, 0, sizeof (x));
}

void ModIdCTX::update (const unsigned char *msg, size_t msglen)
{
    uint32_t index = (uint32_t) ((count[0] >> 3) & 0x3F);
    uint32_t partLen = 64 - index;
    uint32_t i = 0;

    if ((count[0] += ((uint32_t) msglen << 3)) < ((uint32_t) msglen << 3)) {
	++count[1];
    }
    count[1] += ((uint32_t) msglen >> 29);

    if (msglen >= partLen) {
	memcpy (&buffer[index], msg, partLen);
	transform (state, buffer);
	for (i = partLen; i + 63 < msglen; i += 64) {
	    transform (state, msg + i);
	}
	index = 0;
    } else {
	i = 0;
    }
    memcpy (buffer + index, msg + i, msglen - i);
}

static const unsigned char PADDING[64] = {
    0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

void ModIdCTX::finalize (unsigned char *out, size_t outsz)
{
    unsigned char bits[8];
    uint32_t index, padLen;

    if (outsz < RES_SIZE) {
	throw runtime_error ("ModIdCTX::finalize(): Buffer overflow");
    }

    Encode (bits, count, 8);

    index = (uint32_t) ((count[0] >> 3) & 0x3F);
    padLen = (index < 56 ? 56 : 120) - index;
    update (PADDING, padLen);
    update (bits, 8);

    Encode (out, state, 16);

    init();
}

} /*namespace IFC*/
