/* selhelpers.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Interface file for 'selhelpers.h'
** (Helper functions for checking if the value to be attached attached to a
**  SELECT type object is valid for the SELECT type.)
**
*/
#ifndef SELHELPERS_H
#define SELHELPERS_H

#include <vector>

#include "plinterface.h"

namespace IFC {

// Return the list of types a given SELECT type consists of as std::vector.
// Returns an empty vector if 'seltype' is no SELECT type.
std::vector<ExpTypes> seltypes (ExpTypes seltype);

// Test if a SELECT type contains a given type. Return 'true' if this is the
// case and 'false' if not or if 'seltype' is no SELECT type.
bool is_validseltype (ExpTypes seltype, ExpTypes candidate);

} /*namespace IFC*/

#endif /*SELHELPERS_H*/
