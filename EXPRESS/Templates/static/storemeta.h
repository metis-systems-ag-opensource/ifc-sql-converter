/* storemeta.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file of the 'storemeta' module.
** (Store the 'metadata' of an IFC model (the data from the 'HEADER;' section
**  of an IFC file).)
**
*/
#ifndef STOREMETA_H
#define STOREMETA_H

#include "ifcmodel.h"
#include "plctx.h"

namespace IFC {

uint32_t store_meta (PlCtx &ctx, const IFCModel &model);

} /*namespace IFC*/

#endif /*STOREMETA_H*/
