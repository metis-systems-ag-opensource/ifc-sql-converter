/* expbase.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Definition of the base types used for implementing the EXPRESS type model
** in C++.
**
*/
#ifndef EXPBASE_H
#define EXPBASE_H

#include <vector>
#include <stdexcept>

#include "valtypes.h"
#include "errlist.h"

#include "exptypes.h"

class DBhandle;

namespace IFC {
//using namespace IFC;

class InstErr : public std::domain_error {
public:
    InstErr (size_t instid, uint32_t attrx, const std::string &err);
} /*InstErr*/;

class ValueErr : public InstErr {
public:
    ValueErr (size_t instid, uint32_t attrx, const std::string &ats);
} /*ValueErr*/;

class TypeErr : public std::domain_error {
public:
    TypeErr (const std::string &expected_type);
} /*TypeErr*/;

class ValIndexErr : public std::domain_error {
public:
    ValIndexErr (size_t index, const std::string &errmsg);
} /*ValIndexErr*/;

class IndexError : public std::domain_error {
public:
    IndexError (size_t index, size_t min, size_t max);
} /*IndexError*/;

class AggExpected : public std::domain_error {
public:
    AggExpected();
} /*AggExpected*/;

class RAggValueErr : public std::range_error {
public:
    RAggValueErr (const std::string &aggtype);
} /*RAggValueErr*/;

enum class TypeClass { none, entity, aggregate, select, enumeration, simple,
		       number, binary, string, instref };

std::string to_string (TypeClass tc);

// Base type of all EXPRESS types (simple, ENUMERATION, SELECT, ENTITY)
struct ExpBase {
    // It is better to define the destructor as first virtual member function
    // here, because otherwise, because otherwise, the VMTs for the derived
    // classes wouldn't be created, if the first pure virtual function
    // would not be implemented (stupid optimisation of the C++ compiler).
    // Because it is a NOOP, no harm is done here.
    virtual ~ExpBase() { }
    // Making 'type()' _pure virtual_, because there is no implementation for
    // it which makes sense in 'ExpBase' (there are _never_ objects of
    // 'ExpBase' - only of its descendants.
    virtual ExpTypes type() const = 0;
    virtual TypeClass typeclass() const { return TypeClass::none; }
    virtual bool is_entity() const { return false; }
    virtual bool is_same_as (const ExpBase *rx) const = 0;
protected:
    virtual bool check() const { return true; }
#if 0
    template<typename T> T *cast() {
	return T::Type() == type() ? static_cast<T *>(this) : nullptr;
    }
    template<typename T> const T *cast() const {
	return T::Type() == type() ? static_cast<const T *>(this) : nullptr;
    }
#endif
} /*ExpBase*/;

// C++ base type of all ENTITY types
struct EntityBase : ExpBase {
    EntityBase() : _instanceid(0), _dbid(0) { }
    EntityBase (const EntityBase &x) = delete;
    EntityBase (EntityBase &&x) = delete;
    TypeClass typeclass() const { return TypeClass::entity; }
    bool is_entity() const;
    size_t instanceid() const { return _instanceid; }
    void instanceid (size_t id) { _instanceid = id; }
    bool is_same_as (const ExpBase *rx) const;
    //void db_store(DBhandle *dbh);
    // Non-virtual member functions. These remain the same for _all_ entity
    // instance types!
    const ErrorList &errors() const { return _errors; }
    void writeAddErr (const std::string &msg, uint32_t ix);
/*???*/
    std::vector<ExpBase *> attributes;
    ErrorList _errors;	// 'errors' requires eight bytes if unused!
    size_t _instanceid;
    uint32_t _dbid;
} /*EntityBase*/;

// C++ base type of all non-ENTITY and non-ENUMERATION types
struct SimpleBase : ExpBase {
    TypeClass typeclass() const { return TypeClass::simple; }
    // Need a non-pure basic implementation here, as for some of the "simple"
    // types, an error must be generated (at runtime) but aborting the
    // compilation must be prevented.
    virtual std::string todb() const;
} /*SimpleBase*/;

// Simple type INTEGER
struct ExpInteger : SimpleBase {
    ExpInteger();
    ExpInteger (const STEP::Value *v);
    ExpInteger (long v);
    ExpTypes type() const;
    operator long() const;
    bool is_same_as (const ExpBase *rx) const;
    std::string todb() const;
private:
    long v;
} /*ExpInteger*/;
using INTEGER = ExpInteger;

// Simple type REAL
struct ExpReal : SimpleBase {
    ExpReal();
    ExpReal (const STEP::Value *v);
    ExpReal (double v);
    ExpTypes type() const;
    operator double() const;
    bool is_same_as (const ExpBase *rx) const;
    std::string todb() const;
private:
    double v;
} /*ExpReal*/;
using REAL = ExpReal;

// Simple type BOOLEAN
struct ExpBoolean : SimpleBase {
    ExpBoolean();
    ExpBoolean (const STEP::Value *v);
    ExpBoolean (bool v);
    ExpTypes type() const;
    operator bool() const;
    bool is_same_as (const ExpBase *rx) const;
    std::string todb() const;
private:
    bool v;
} /*ExpBoolean*/;
using BOOLEAN = ExpBoolean;

// Simple type LOGICAL
struct ExpLogical : SimpleBase {
    enum Value : uint8_t { l_false, l_unknown, l_true };
    ExpLogical();
    ExpLogical (const STEP::Value *v);
    ExpLogical (Value v);
    ExpLogical (bool v);
    ExpTypes type() const;
    operator Value() const;
    operator bool() const;
    bool is_same_as (const ExpBase *rx) const;
    std::string todb() const;
private:
    Value v;
} /*ExpLogical*/;
using LOGICAL = ExpLogical;

// Simple type NUMBER
struct ExpNumber : SimpleBase {
    ExpNumber();
    ExpNumber (const STEP::Value *v);
    ExpNumber (long iv);
    ExpNumber (double rv);
    TypeClass typeclass() const { return TypeClass::number; }
    ExpTypes type() const;
    operator long() const;
    operator double() const;
    bool is_real() const;
    bool is_same_as (const ExpBase *rx) const;
    std::string todb() const;
private:
    union { long i; double r; } v;
    bool _is_real;
} /*ExpNumber*/;
using NUMBER = ExpNumber;

// Simple type BINARY
struct ExpBinary : SimpleBase {
    // Need to implement the conversion from string and to string ...
    // I should implement even this here as a simple 'char *' ... or better a
    // 'uint8_t *' ...
    ExpBinary();
    ExpBinary (const STEP::Value *b);
    ExpBinary (const std::string &x);
    TypeClass typeclass() const { return TypeClass::binary; }
    ExpTypes type() const;
    operator std::string() const;
    bool is_same_as (const ExpBase *rx) const;
    std::string todb() const;
private:
    std::vector<bool> v;
} /*ExpBinary*/;
using BINARY = ExpBinary;

// Simple type STRING
// Because of the amount of data required even for medium complex IFC models,
// i will not use 'std::string' here, and also not my 'lmf::string', but a
// simple 'const char *' instead. This is possible, because the strings in an
// IFC-model are
//   a) constant
//   b) typically short.
// So i'm avoiding any additional overhead (except for the pointer, which is
// required because of the nonetheless dynamical nature of strings).
struct ExpString : SimpleBase {
    ExpString();
    ExpString (const STEP::Value *v);
    ExpString (const std::string &sv);
    ExpString (const char *sv);
    ExpString (const ExpString &x);
    ExpString (ExpString &&x);
    ~ExpString();
    TypeClass typeclass() const { return TypeClass::string; }
    ExpTypes type() const;
    ExpString &operator= (const ExpString &x);
    ExpString &operator= (ExpString &&x);
    size_t size() const;
    operator std::string() const;
    bool is_same_as (const ExpBase *rx) const;
    std::string todb() const;
private:
    const char *v;
} /*ExpString*/;
using STRING = ExpString;


// Before a direct reference to another instance can be inserted, all instances
// must be known. In order to insert references nonetheless, in the first step
// the instance references as gotten from the transport model (instance ids)
// are inserted. After reading all ENTITY instances, the references can be
// replaced and the entities being type-checked against the type of the
// corresponding attributes. After this step, the instace ids can be replaced
// with either a direct pointer to the corresponding instances, or with an
// index into a table which holds these instances. In order to keep the amount
// of memory required for this element low, the instance id/index is stored in
// 31 bits of the data and the flag which indicates the reference as "resolved"
// in the lowest 1 bit.
struct ExpInstRef : SimpleBase {
    ExpInstRef();
    ExpInstRef (const STEP::Value *v);
    ExpInstRef (const ExpInstRef &x);
    ExpInstRef (uint32_t index);
    ~ExpInstRef();
    TypeClass typeclass() const { return TypeClass::instref; }
    ExpTypes type() const;
    ExpInstRef &operator= (const ExpInstRef &x);
    bool is_same_as (const ExpBase *rx) const;
    //std::string todb();
    bool is_index() { return _is_index; }
    void set_index (uint32_t ix) { _is_index = 1; _id = ix; }
    uint32_t id() const { return _id; }
    void id (uint32_t iid) { _id = iid; }
    //std::string todb() const;		// NEVER IMPLEMENTED!
private:
    uint32_t _is_index : 1, _id : 31;
} /*ExpInstRef*/;

} /*namespace IFC*/

#endif /*EXPBASE_H*/
