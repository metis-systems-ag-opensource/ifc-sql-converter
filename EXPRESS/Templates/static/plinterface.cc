/* %{ifile}
**
** Implementation for the STEP object generation for %{schema}.
** Copyright: (c) 2019, Metis AG
** License: MIT License
**
** AUTOMATICALLY GENERATED FILE
**
*/

//##DEBUG
#include <iostream>
using std::cerr, std::endl, std::flush;
//##

#include <cstdint>
#include <fstream>
#include <stdexcept>
#include <utility>

#include "utils.h"
#include "expfactory.h"
#include "fixrefs.h"
#include "storemeta.h"
#include "storedata.h"
#include "instdestroy.h"
#include "outputops.h"
#include "warning.h"

#include "valtypes.h"

#include "plinterface.h"

namespace IFC {

using namespace std;

#include "symbols.cc"

size_t symsend()
{
    return _symsend;
}

const char *schema()
{
    return ExpressSchema;
}

// Search for a name in the symbol list. 
size_t findName (const char *name)
{
    size_t l = 1, r = _symsend;
    while (l <= r) {
	size_t m = (l + r) / 2;
	// ATTENTION! Because indexes in C/C++ start at '0', but my 'l' and 'r'
	// have a range between '1' and the value directly behind the last
	// valid index, the entry in 'step_rindex[]' is 'm - 1'.
	size_t rx = step_rindex[m - 1];
	int cres = lccmp (name, step_namelist + step_namerefs[rx]);
	// ATTENTION. 'findName()' returns an index, not the integer value of
	// a type tag.
	if (cres == 0) { return rx; }
	if (cres < 0) { r = m - 1; } else { l = m + 1; }
    }
    return _symsend;
}

// Convert an index returned by 'findName()' into the corresponding symbol id
ExpTypes index2id (size_t index)
{
    string ucfschema = ExpressSchema;
    if (index >= _symsend) {
	throw runtime_error ("Invalid index in " + ucfschema + " table.");
    }
    return exp_typelist[index];
}

// Convert an index returned by 'findName()' into the corresponding symbol name
string index2name (size_t ix)
{
    string ucfschema = ExpressSchema;
    if (ix >= _symsend) {
	throw runtime_error ("Invalid index in " + ucfschema + " table.");
    }
    return string (step_namelist + step_namerefs[ix]);
}

// Convert a symbol id into the corresponding symbol name
string id2name (ExpTypes x)
{
    // The type tags' integer values start with '1', so the integer value of
    // the given type tag 'x' must be decreased by one for getting the right
    // index value (array indexes start at 0 in C and C++).
    if (x == ExpTypes::_NoType) { return "_NoType"; }
    return index2name ((size_t) x - 1);
}

// Generate an 'EntityBase' object from a 'STEP::Instance' object.
EntityBase *gen_expinstance (const STEP::Instance *v)
{
    return IFC::expgen (v);
}

// Return the instance id from a given 'EntityBase' object
size_t get_instanceid (const EntityBase *inst)
{
    return inst->instanceid();
}

// Convert all references from within the given instances into corresponding
// indexes 
void fixrefs (InstanceList *instances)
{
    fix_instancerefs (*instances);
}

static void read_credentials (const string &path, string &user, string &pass)
{
    string line1, line2;
    ifstream cred;
    cred.open (path);
    if (! cred.is_open()) {
	throw runtime_error
	    ("Read attempt from authentication credentials failed.");
    }
    readline (cred, line1);
    trim (line1);
    if (line1.empty()) {
	bool ateof = cred.eof();
	cred.close();
	throw runtime_error
	    (ateof ? "Empty authentication credentials file"
		   : "Invalid authentication credentials - no username");
    }
    readline (cred, line2);
    trim (line2);
    if (line2.empty()) {
	cred.close();
	throw runtime_error
	    ("Invalid authentication credentials - no password");
    }
    cred.close();
    user = line1; pass = line2;
}

void dbstore (const IFCModel *model, const char *schema,
	      std::vector<EntityBase *> *instances,
	      ConfData *_cfg, OutputMode om)
{
    PlCtx ctx (_cfg, om, schema);
    string &credfile = ctx.cfg()["DBAUTH"];
    string user, pass;
    read_credentials (credfile, user, pass);
    try {
	ctx.opendb (user, pass);	// Opens/Connects to 'IFCModel'
	uint32_t modelid = store_meta (ctx, *model);
	if (modelid == 0) {
	    // When(If) i decide that for an already existing
	    // 'IFCModel.Model.fileid' no data is stored, i will activate the
	    // currently deactivated 'return 0;' in 'store_meta()'. This will
	    // lead to this branch, which does nothing except for issuing a
	    // message.
	    warning ("No data is stored.");
	} else {
	    // Each other case will lead to this branch, where the determined
	    // (ifc-)database is selected, which is then used for storing the
	    // data.
	    if (! ctx.use_schemadb()) {
		throw runtime_error
		    ("Selecting database '" + ctx.dbname() + "' failed.");
	    }
	    store_data (ctx, modelid, *instances);
	}
    } catch (exception &e) {
	// Should do more here
	ctx.oops().write_fatal (e.what());
	throw;
    }
}

void instdestroy (EntityBase *p)
{
    destroy_instance (p);
}

} /*namespace IFC*/
