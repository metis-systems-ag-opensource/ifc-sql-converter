/* utils.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Condensed form of the utilities from 'Common' - as 'Common' will probably
** not exist in the directory containing the source files of the IFC plugin.
**
*/

#include <cctype>
#include <cmath>
#include <cstddef>
#include <cstring>

#include <stdexcept>
#include <utility>

#include "utils.h"

using std::istream;
using std::runtime_error, std::domain_error;
using std::move;
using std::string, std::to_string;
using std::vector;

int cscmp (const char *l, const char *r)
{
    return strcmp (l, r);
}

int lccmp (const char *l, const char *r)
{
    // I could use 'strcasecmp()' here, but this is a BSD function and i don't
    // know if it is part of any POSIX standard
    int lc = 0, rc = 0;
    --l; --r;
    while ((lc = tolower (*++l)) == (rc = tolower (*++r)) && lc) { }
    return (lc < rc ? -1 : (rc < lc ? +1 : 0));
}

string bin2string (const vector<bool> &v)
{
    static const char hexdigit[] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };
    size_t btsz = v.size();
    size_t nbsz = (btsz + 3) >> 2;
    size_t rbsz = (nbsz << 2) - btsz;
    uint8_t nibble = 0;
    string res (1, hexdigit[rbsz]);
    res.reserve (nbsz + 1);
    for (auto bit: v) {
	nibble = (nibble << 1) | (bit ? 1 : 0);
	rbsz = (rbsz + 1) % 4;
	if (rbsz == 0) { res.push_back (hexdigit[nibble]); nibble = 0; }
    }
    return res;
}

vector<bool> string2bin (const string &s)
{
    static int hexdigitvalue[256];
    static bool initialised = false;
    if (! initialised) {
	const string hexdigits = "0123456789ABCDEF";
	for (auto &x: hexdigitvalue) { x = -1; }
	for (int ix = 0; ix < (int) hexdigits.size(); ++ix) {
	    int ch = (int) hexdigits[ix];
	    hexdigitvalue[ch] = ix;
	}
	initialised = true;
    }
    size_t ssize = s.size();
    vector<bool> res;
    res.reserve ((ssize + 1) * 4);
    int xdv;
    unsigned rbsz;
    if (s.empty()) { goto INVARG; }
    if ((xdv = hexdigitvalue[(unsigned) s[0]]) < 0 || xdv > 3) { goto INVARG; }
    rbsz = (unsigned) xdv;
    if (rbsz > 0 && ssize < 2) { goto INVARG; }
    for (size_t ix = 1; ix < ssize; ++ix) {
	unsigned ch = (unsigned) s[ix];
	if ((xdv = hexdigitvalue[ch]) < 0) { goto INVARG; }
	int mask = 1 << (4 - rbsz); rbsz = 0;
	while ((mask >>= 1) > 0) {
	    res.push_back (xdv & mask ? true : false);
	}
    }
    return res;
INVARG:
    throw domain_error ("Invalid representation of a STEP-binary.");
}

static const char *hexdigits (bool upper)
{
    return (upper ? "0123456789ABCDEF" : "0123456789abcdef");
}

string u2hex (unsigned long v, unsigned int fw, bool upper)
{
    string res, tmp;
    const char *hexdg = hexdigits (upper);
    int fwc = (int) fw;
    do { tmp = hexdg[v & 0xF] + tmp; v >>= 4; --fwc; } while (v > 0);
    while (fwc > 0) { tmp = '0' + tmp; --fwc; }
    res.reserve (tmp.size() + 1); res = tmp;
    return res;
}

string i2hex (long v, unsigned int fw, bool upper)
{
    if (v > 0) { return u2hex ((unsigned long) v, fw, upper); }
    string res, tmp;
    const char *hexdg = hexdigits (upper);
    int fwc = (int) fw;
    do { tmp = hexdg[v & 0xF] + tmp; v >>= 4; --fwc; } while (v != -1l);
    while (fwc > 0) { tmp = 'F' + tmp; --fwc; }
    res.reserve (tmp.size() + 1); res = tmp;
    return res;
}

static long double ipow (long double x, int n)
{
    long double res = 1.0;
    if (n == 0) { return res; }
    bool inv = (n < 0);
    if (inv) { n = -n; }
    while (n > 0) {
	if ((n & 1) != 0) { res *= x; }
	x *= x; n >>= 1;
    }
    return (inv ? 1.0 / res : res);
}

static const char *dg[] = {
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
};

string float2string (long double x, int maxexp, int maxdigits)
{
    string res("");
    size_t d;
    bool negative = (x < 0);
    if (negative) { x = -x; }
    if (x == 0.0) { return "0."; }
    if (x >= 1.0) {
        if (x <= ipow(10, maxexp)) {
            long double z = 1.0;
            int zp = 0;
            while (x / z >= 10.0) {
                ++zp; z = ipow (10.0, zp);
            }
            x /= z;
            do {
                d = (size_t) floor (x); x -= d;
                res += dg[d]; --maxdigits;
                x *= 10.0;
            } while (zp-- > 0);
            res += ".";
            while (x >= epsilon && maxdigits > 0) {
                d = (size_t) floor (x + epsilon); x -= d;
                res += dg[d]; --maxdigits;
                x *= 10.0;
            }
        } else {
            long double z = 1.0;
            int zp = 0;
            while (x / z >= 10.0) { zp += 1; z = ipow (10.0, zp); }
            x /= z;
            string exp = "E" + to_string (zp);
            d = (size_t) floor (x); x -= d;
            res += string (dg[d]) + "."; --maxdigits;
            x *= 10.0;
            while (x >= epsilon && maxdigits > 0) {
                d = (size_t) floor (x); x -= d;
                res += dg[d]; --maxdigits;
                x *= 10.0;
            }
            res += exp;
        }
    } else if (x >= ipow(10, -maxexp)) {
        res = "0."; --maxdigits;
        while (x >= epsilon && maxdigits > 0) {
            x *= 10.0; d = (size_t) (floor (x + epsilon)); x -= d;
            res += dg[d]; --maxdigits;
        }
    } else {
        long double z = 1.0;
        int zp = 0;
        while (x * z < 1.0) { zp += 1; z = ipow (10.0, zp); }
        x *= z;
        string exp = "E-" + to_string (zp);
        d = (size_t) floor (x + epsilon); x -= d;
        res += string (dg[d]) + ".";
        while (x >= epsilon && maxdigits > 0) {
            x *= 10.0;
            d = (size_t) floor (x + epsilon); x -= d;
            res += dg[d]; --maxdigits;
        }
        res += exp;
    }
    res = (negative ? "-" : "") + res;
    return res;
}

void trim (std::string &s, const std::string &chars)
{   
    size_t lp = s.find_first_not_of (chars);
    size_t rp = s.find_last_not_of (chars);
    if (rp < s.size()) { s.erase (rp + 1); }
    if (lp < s.size()) { s.erase (0, lp); }
}

string trim (const string &in, const string &chars)
{
    string res (in);
    trim (res);
    return res;
}

static void caseconv (const string &in, string &out, int (*ccv) (int))
{
    ssize_t insize = in.size(), ix;
    string tmp(insize + 1, '\0');
    for (ix = 0; ix < insize; ++ix) { tmp[ix] = (char) ccv (in[ix]); }
    tmp.resize(insize);
    out = move (tmp);
}

string uppercase (const string &s)
{
    string res;
    caseconv (s, res, toupper);
    return res;
}

string lowercase (const string &s)
{
    string res;
    caseconv (s, res, tolower);
    return res;
}

string operator% (const string &fmt, const vStringList &values)
{
    size_t xcc = 0, ix, px;
    string tmp, res;
    size_t fmtsz = fmt.size(), valsize = values.size();
    for (ix = 0; ix < fmtsz;) {
        if (fmt[ix] == '$') {
            ++ix;       // Skip the introducing character
            if (ix >= fmtsz) {
                // It was the last one; insert it into the result
                tmp += "$"; 
            } else if (fmt[ix] == '$') {
                // '$$' sequence; Insert the second '$'
                tmp += fmt[ix++]; 
            } else if (fmt[ix] == '#') {
                // '$#'; insert the "next" item from 'values'
                tmp += (xcc < valsize ? values[xcc++] : ""); ++ix;
            } else if (isdigit (fmt[ix])) {
                // '$<number>'; insert the item at position '<number>' from
                // 'values'.
                string v = string (1, fmt[ix++]);
                while (ix < fmtsz && isdigit (fmt[ix])) { v += fmt[ix++]; }
                px = stoul (v);
                tmp += (px > 0 && px <= valsize ? values[px - 1] : "");
            } // else { /*Skip the introducing '$'*/ }
        } else {
            // Any other char; insert the character directly.
            tmp += fmt[ix++];
        }
    }
    return tmp.substr();
}

#undef CR
#define CR '\15'

istream& readline (istream& is, string& str)
{
    string line;
    getline (is, line);
    size_t linelen = line.size();
    if (linelen > 0 && line[linelen - 1] == CR) {
	// A 'CR' here means that the line ended with 'CR/LF', otherwise a
	// single CR wouldn't remain at the end of the line.
	line.resize (linelen - 1);
    }
    str = move (line);
    return is;
}
