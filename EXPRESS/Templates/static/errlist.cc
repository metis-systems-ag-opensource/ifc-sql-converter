/* Templates/static/errlist.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Implementation part of 'errlist.h'
** (A data type for storing error messages (in a C-alike manner, because this
**  is minimal memory consuming))
**
*/

#include <cerrno>
#include <cstdlib>
#include <cstring>

#include <stdexcept>

#include "errlist.h"

namespace IFC {

using std::runtime_error;
using std::string, std::operator""s;

struct ErrListItem {
    ErrListItem *next;
    char msg[1];
};

// Small helper function for copying an entire error list (internal)
static void copyitems (ErrListItem * &dst, const ErrListItem * src,
		       const char *where)
{
    ErrListItem *f = nullptr, *l = nullptr;
    for (auto p = src; p; p = p->next) {
	auto msg = p->msg;
	size_t msgsize = strlen (msg);
	ErrListItem *n = (ErrListItem *) malloc (sizeof(ErrListItem) + msgsize);
	if (! n) {
	    throw runtime_error (string (where) + " - " + strerror (errno));
	}
	n->next = nullptr; strcpy (n->msg, msg);
	if (f && l) { l->next = n; l = n; } else { f = n; l = n; }
    }
    dst = f;
}

// Small helper function for deleting an entire error list (internal)
static void delitems (ErrListItem * &errors)
{
    while (errors) {
	ErrListItem *x = errors;
	errors = x->next;
	free (x);
    }
}

//struct ErrorList {
//    ErrorList() : errors(nullptr) { };

// Copy constructor
ErrorList::ErrorList (const ErrorList &x)
{
    copyitems (errors, x.errors, "ErrorList::ErrorList (const ErrorList &)");
}

// Move constructor
ErrorList::ErrorList (ErrorList &&x)
    : errors(x.errors)
{
    x.errors = nullptr;
}

ErrorList::~ErrorList()
{
    delitems (errors);
}

void ErrorList::add (const std::string &msg)
{
    add (msg.c_str(), msg.size());
}

void ErrorList::add (const char *msg)
{
    add (msg, strlen (msg));
}
//    class iterator {
//    public:
//	iterator (ErrorList *x, bool end = false) : base(x) {
//	    reset (end);
//	}
////	iterator (const errlist &x, bool end = false) : base(&x) {
////	    reset (end);
////	}
//	~iterator() { curr = nullptr; base = nullptr; }
//	void reset (bool end = false) {
//	    curr = (end ? nullptr : base->errors);
//	}
const char * ErrorList::iterator::operator*() const
{
    if (! curr) { return nullptr; }
    return curr->msg;
}

ErrorList::iterator &ErrorList::iterator::operator++()
{
    if (curr) { curr = curr->next; }
    return *this;
}

ErrorList::iterator &ErrorList::iterator::operator++(int)
{
    if (curr) { curr = curr->next; }
    return *this;
}
//	bool operator== (const iterator &r) const {
//	    return (base == r.base) && (curr == r.curr);
//	}
//	bool operator!= (const iterator &r) const {
//	    return (base != r.base) || (curr != r.curr);
//	}
//    private:
//	ErrorList *base;
//	ErrListItem *curr;
//    };
//    iterator begin() { return iterator (this); }
//    iterator end() { return iterator (this, true); }

// Copy assignment operator
ErrorList &ErrorList::operator= (const ErrorList &x)
{
    delitems (errors);
    copyitems (errors, x.errors, "ErrorList::operator= (const ErrorList &)");
    return *this;
}

// Move assignment operator
ErrorList &ErrorList::operator= (ErrorList &&x)
{
    delitems (errors);
    errors = x.errors; x.errors = nullptr;
    return *this;
}
//private:
//    ErrListItem *errors;
void ErrorList::add (const char *msg, size_t msgsize)
{
    ErrListItem *err = (ErrListItem *) malloc (sizeof(ErrListItem) + msgsize);
    if (! err) {
	throw runtime_error
	    ("ErrorList::add() failed - "s + strerror (errno));
    }
    err->next = errors;
    memcpy (err->msg, msg, msgsize); err->msg[msgsize] = '\0';
    errors = err;
}
//};

} /*namespace IFC*/
