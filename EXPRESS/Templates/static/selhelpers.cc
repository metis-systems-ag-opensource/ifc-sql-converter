/* selhelpers.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Helper functions for checking if the value to be attached attached to a
** SELECT type object is valid for the SELECT type.
**
*/

#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <utility>

//#include "plif.h"
//#include "expbase.h"

#include "selhelpers.h"

namespace IFC {

using std::range_error, std::pair, std::make_pair;
using std::vector;

#include "seltypelist.cc"

struct VTypes {
    VTypes(): base(nullptr), size(0) { }
    VTypes (size_t basex)
	: base (nullptr), size(0)
    {
	size_t indexsize = sizeof(seltypeindexes) / sizeof(*seltypeindexes);
	base = seltypelist;
	if (basex >= indexsize - 1) {
	    throw range_error ("VTypes::VTypes(): Index out of range");
	}
	base = seltypelist + seltypeindexes[basex];
	size = seltypeindexes[basex + 1] - seltypeindexes[basex];
    }
    VTypes (const VTypes &x) : base(x.base), size(x.size) { }
    VTypes &operator= (const VTypes &x)
    {
	base = x.base; size = x.size;
	return *this;
    }
    pair<const ExpTypes *, size_t> data() const
    {
	return make_pair (base, size);
    }
private:
    ExpTypes *base;
    size_t size;
};

static pair<bool, VTypes> get_typelist (ExpTypes seltype)
{
    size_t seltypessz = sizeof(_seltypes) / sizeof(*_seltypes);
    uint32_t l = 1, r = (uint32_t) seltypessz;
    while (l < r) {
	uint32_t m = (l + r) / 2;
	if (seltype > _seltypes[m - 1]) { l = m + 1; } else { r = m; }
    }
    if (seltype != _seltypes[l - 1]) {
	return make_pair (false, VTypes());
    }
    return make_pair (true, VTypes (l - 1));
}

vector<ExpTypes> seltypes (ExpTypes seltype)
{
    vector<ExpTypes> res;
    auto [done, vtypes] = get_typelist (seltype);
    if (done) {
	auto [types, size] = vtypes.data();
	res.reserve (size);
	for (size_t ix = 0; ix < size; ++ix) { res.push_back (types[ix]); }
    }
    return res;
}

bool is_validseltype (ExpTypes seltype, ExpTypes candidate)
{
    auto [is_seltype, seltypelist] = get_typelist (seltype);
    if (is_seltype) {
	auto [tbl, tblsz] = seltypelist.data();
	for (size_t ix = 0; ix < tblsz; ++ix) {
	    if (tbl[ix] == candidate) { return true; }
	}
    }
    return false;
}

} /*namespace IFC*/
