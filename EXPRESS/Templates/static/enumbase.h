/* enumbase.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Base type of the ENUMERATION types.
**
*/
#ifndef ENUMBASE_H
#define ENUMBASE_H

#include "expbase.h"

namespace IFC {

// Base type of all ENUMERATIONs (derived from 'SimpleType', so an ENUMERATION
// is a simple type, but has another quality, too).
struct _EnumBase : SimpleBase {
    TypeClass typeclass() const { return TypeClass::enumeration; }
};

} /*namespace IFC*/


#endif /*ENUMBASE_H*/
