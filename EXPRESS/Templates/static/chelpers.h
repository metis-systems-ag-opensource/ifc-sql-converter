/* chelpers.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'chelpers.cc'
** (Helper functions for the generated code. This module is - like
**  'enumval.{cc,h}' - part of the plugin being generated. But - because the
**  functions here are independent from anything other than some tables (which
**  are included) - this code is not templated.)
**
*/

#include <cstdint>

#include "plinterface.h"

namespace IFC {

// This type describes an attribute's properties (beside its type).
// 'mx' is it's real index in the attribute vector. For DERIVE attributes,
// is should be '-1', as these have no place in the attribute vector.
// (The prefix 'EA' (also: 'ea') is a shortcut for 'entity attribute'.)
// 
struct EAProperties {
    int32_t mx;		// The attribute's index in the attribute list
    uint32_t typelist_begin, typelist_end;
    bool is_derive:1,	// 'true' for a DERIVE attribute, 'false' otherwise
	 is_optional:1;	// 'true' for an OPTIONAL attribute, 'false' otherwise
};

// A description of a valid type list. Such lists are stored contiguously in
// a static C array. A 'TypeList' descriptor is meant as a way to acces such
// an embedded list.
struct TypeList { const ExpTypes *list; size_t listsz; };

// Return a constant reference to a property structure for a specific
// attribute of a given ENTITY.
const EAProperties &get_eaproperties (uint32_t ix, ExpTypes t);

// Return the 'is_derive' flag of the 'EAProperties' value for a given
// <'t'(ENTITY), 'ix'(Attribute)> pair.
bool is_derive_attribute (uint32_t ix, ExpTypes t);

// Return the 'is_optional' flag of the 'EAProperties' value for a given
// <'t'(ENTITY), 'ix'(Attribute)> pair.
bool is_optional_attribute (uint32_t ix, ExpTypes t);

// Return the index of an attribute given by its <'t'(ENTITY), 'ix'(Attribute)>
// pair. The index ('ix') in this pair may differ from the result, because the
// 'EAProperties' list also contains DERIVE attributes which are never stored
// anywhere but probably have an attribute entry ('*') in a given IFC instance.
// So, this function translates the index of an entry in an IFC instance into
// the index the attribute has in the attribute in memory - or a database table
// (this index is -1 (invalid) for DERIVE attributes, marking them as non-
// persistent).
int32_t attribute_index (uint32_t ix, ExpTypes t);

// Check if a type 'refett' is member of the list of valid types for the given
// <'ett'(ENTITY), 'attrx'(attribute index)> pair. This is done by getting the
// corresponding list of valid types from 'eapvalidtypes[]' via the 'eaplist[]'
// entry of this attribute and testing if 'refett' matches one of the elements
// of this list.
bool check_attrtype (ExpTypes refett, ExpTypes ett, uint32_t attrx,
		     bool reverse = false);

// Return a description of the list of the valid types of the attribute 'attrx'
// of a given ENTITY 'ett'. This description consists of a pointer to the
// beginning of this list its length.
TypeList expected_types (ExpTypes ett, uint32_t attrx, bool reverse = false);

// Return a list of all SUPERTYPEs for a given type, or an empty TypeList, if
// the given type is no entity type.
TypeList get_supertypes (ExpTypes ett);

} /*namespace IFC*/
