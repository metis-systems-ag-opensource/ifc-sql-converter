/* chelpers.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Helper functions for the generated code. This module is - like
** 'enumval.{cc,h}' - part of the plugin being generated. But - because the
** functions here are independent from anything other than some tables (which
** are included) - this code is not templated.
**
*/

//##DEBUG
//#include <iostream>
//using std::cerr, std::endl, std::flush;
//##

#include <stdexcept>

using std::runtime_error;

#include "chelpers.h"

namespace IFC {

#include "eaplist.cc"

// Small helper function which contains the binary search code on the
// 'eaptags[]' table.
static uint32_t tag_search (ExpTypes t)
{
    uint32_t l = 0, r = sizeof(eaptags) / sizeof(*eaptags) - 1;
    while (l < r) {
	uint32_t m = (l + r) / 2;
	if (eaptags[m] < t) { l = m + 1; } else { r = m; }
    }
    return l;
}

// Return a constant reference to a property structure for a specific
// attribute of a given ENTITY.
const EAProperties &get_eaproperties (uint32_t ix, ExpTypes t)
{
    uint32_t l = tag_search (t);
    if (t != eaptags[l]) {
	throw runtime_error ("Entry '" + id2name (t) + "' is no ENTITY");
    }
    //r = (l < eaptagsz ? eapindex[l] : eaplistsz);
    uint32_t px = eapindex[l];
    const EAProperties &eap = eaplist[px + ix];
    return eap;
}

bool is_derive_attribute (uint32_t ix, ExpTypes t)
{
    const EAProperties &eap = get_eaproperties (ix, t);
    return eap.is_derive;
}

bool is_optional_attribute (uint32_t ix, ExpTypes t)
{
    const EAProperties &eap = get_eaproperties (ix, t);
    return eap.is_optional;
}

int32_t attribute_index (uint32_t ix, ExpTypes t)
{
    const EAProperties &eap = get_eaproperties (ix, t);
    return eap.mx;
}

static const EAProperties &rev_getproperties (uint32_t mx, ExpTypes t)
{
    // 'mx' is always VALID. But there may be gaps in the translation between
    // 'mx' and the STEP instance attribute index.
    // At first, i must translate the type tag into an index. Then, this index
    // must point to a range array (first: first mx index, last last mx index
    // + 1). The index in this array must point into a list of indexes which
    // refer entries in the 'eaplist[]'.
    uint32_t l = tag_search (t);
    if (eaptags[l] != t) {
	throw runtime_error ("Entry '" + id2name (t) + "' is no ENTITY");
    }
    uint32_t first = eaprevindexindex[l], last = eaprevindexindex[l + 1];
    if (mx >= (last - first)) {
	throw runtime_error
	    ("Attribute index out of range for '" + id2name (t) + "'");
    }
    uint32_t px = eaprevindex[first + mx];
    const EAProperties &eap = eaplist[px];
    return eap;
}

bool check_attrtype (ExpTypes refett, ExpTypes ett, uint32_t attrx,
		     bool reverse)
{
    auto get_properties = reverse ? rev_getproperties : get_eaproperties;
    const EAProperties &eap = get_properties (attrx, ett);
    for (uint32_t ix = eap.typelist_begin; ix < eap.typelist_end; ++ix) {
	if (eapvalidtypes[ix] == refett) { return true; }
    }
    return false;
}

TypeList expected_types (ExpTypes ett, uint32_t attrx, bool reverse)
{
    auto get_properties = reverse ? rev_getproperties : get_eaproperties;
    const EAProperties &eap = get_properties (attrx, ett);
    TypeList res { eapvalidtypes + eap.typelist_begin,
		   eap.typelist_end - eap.typelist_begin };
    return res;
}

TypeList get_supertypes (ExpTypes ett)
{
    TypeList res { nullptr, 0 };
    uint32_t l = tag_search (ett);
    if (eaptags[l] == ett) {
	res.list = supertypes + supertype_indexes[l];
	res.listsz = supertype_indexes[l + 1] - supertype_indexes[l];
    }
    return res;
}

} /*namespace IFC*/
