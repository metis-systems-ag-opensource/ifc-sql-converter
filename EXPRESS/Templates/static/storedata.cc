/* storedata.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Store the model data of an IFC model (the ENTITY instances from the 'DATA'
** part of an IFC file) in the selected (...) database.
**
*/

#include <stdexcept>
#include <string>

#include "idgen.h"
#include "pds.h"
#include "utils.h"

#include "storedata.h"

namespace IFC {

using std::runtime_error;
using std::string, std::to_string;

void store_data (PlCtx &ctx, uint32_t modelid, InstanceList &instances)
{
    DBH &dbh = ctx.dbh();
    const OutputOps &oops = ctx.oops();

    IdGen ids (modelid);

    TypeIdMap tim (get_aggtypeids (ctx));
    
    dbh.autocommit (false);
    dbh.begin ();
    // There is probably no need to pass 'outputops' to 'store_entinst()',
    // because – even if an instance was already stored by a recursive
    // invocation of 'store_entinst()' – the corresponding database id
    // is always returned correctly.
    for (auto entinst: instances) {
	uint32_t dbid = store_entinst (ctx, ids, entinst, instances, tim);
	if (dbid <= 0) {
	    // throw an exception
	    string instid = to_string (entinst->instanceid());
	    dbh.rollback ();
	    //oops.write_fatal ("Storing #" + instid + " failed." EOL);
	    throw runtime_error
		("Attempt to store #" + to_string (entinst->instanceid()) +
		 " chain failed.");
	}
	// So, it is sufficient to write the instance id/database is pair
	// in this function.
	oops.write_instdbid (entinst->instanceid(), dbid);
    }
    dbh.commit ();
}

} /*namespace IFC*/
