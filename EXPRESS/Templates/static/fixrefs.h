/* fixrefs.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Fix the instance references and type check them along the way
**
*/
#ifndef FIXREFS_H
#define FIXREFS_H

#include <cstdint>
#include <vector>

#include "expbase.h"
#include "typedefs.h"

#define USING_INDEXREFS true

namespace IFC {

void fix_instancerefs (InstanceList &instances);

} /*namespace IFC*/

#endif /*FIXREFS_H*/
