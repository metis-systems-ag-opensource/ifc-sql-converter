/* fifo.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2017, Metis AG
** License: MIT License
**
** Small template-type implementing a FIFO (first-in-first-out) list.
** The reason for implementing this type of lists in this simple manner is
** easy. YACC-rules are often left-recursive (which is typical for LR-parsers
** in general), and adding an element to a list with keeping the order in such
** a rule is best done with a ‘first in first out’-list. As an extension, one
** can add elements to the beginning of a list - which better matches right-
** recursive rules (or repetitions), often used in EBNF (or Wirth-schema) like
** LL-parsers (LL-syntax descriptions are generally not left-recursive, as this
** would contradict the LL-quality of such a description).
** So, this implementation supports inserting elements at the beginning of the
** Fifo-structure (‘unshift()’) and at the end of it (‘push()’). Additionally,
** elements can be deleted from the start of the list (‘shift()’) and from the
** end of it (‘pop()’). The latter operation is somewhat slow, as the removal
** from the end must be done by a traversal through the complete list. The
** first element is directly accessible through ‘first()’, the last one through
** ‘last()’. The names of the insertion and removal operations follow somewhat
** the names of the corresponding operations for Perl-arrays, which was my
** intention ...
**
*/
#ifndef FIFO_H
#define FIFO_H

#include <cstddef>
//#include <cstdio>

#include <string>
#include <stdexcept>
#include <utility>

template <class T> class Fifo {
    struct Item {
	Item *next; T value;
	Item () : next(nullptr) { }
	Item (const T &val) : next(nullptr), value(val) { }
	Item (T &&val) : next(nullptr), value(std::move(val)) { }
	Item (T &val) : next(nullptr), value(val) { }
	Item (const Item &x) : next(x.next), value(x.value) { }
	Item (Item &&x) : next(x.next), value(std::move(x.value)) { }
    };
    Item *lfirst, *llast;
    size_t llength;

    void check_not_empty (const std::string &s) {
	if (!lfirst) {
	    throw std::runtime_error ("Fifo::" + s + ": Empty fifo");
	}
    }

    inline void copy (Fifo &dst, const Fifo &src) {
	dst.lfirst = dst.llast = nullptr;
	llength = 0;
	for (Item *oit = src.lfirst; oit; oit = oit->next) {
	    Item *nit = new Item (oit->value);
	    if (!lfirst) { dst.lfirst = nit; } else { dst.llast->next = nit; }
	    dst.llast = nit; ++llength;
	}
    }

public:

    // The Iterator-class is declared/defined here, which makes it part of the
    // namespace of the ‘Fifo’-lists. A ‘Fifo’-iterator is declared somewhat
    // like:
    //   Fifo<`some type´>::Iterator iterator ...
    // It mostly works like the Iterators for the list-types of the ‘standard
    // template library’, but there is no comparison with a non-existing item
    // for finding the end of the list. Instead, the function ‘atend()’ exists,
    // which returns true if the iterator reached the end of the ‘Fifo’ and
    // false otherwise. Additionally, there is a function ‘reset()’, which sets
    // the iterator again to the first element of the ‘Fifo’.
    // Attention! Elements of this iterator-class are not immune against
    // modifications in the list itself (removal of the first or last element).
    class Iterator {
	Fifo *_base;
	Item *_current, *_previous;

	// Helper function which does the real work behind both '++' operators
	Iterator &advance () {
	    if (!_current) {
		throw std::runtime_error ("Fifo::Iterator::++: No more items");
	    }
	    _previous = _current;
	    _current = _previous->next;
	    return *this;
	}

	// Helper function which does the real work behind both 'insert()'
	// (public) member functions
	void insert_helper (Item *item) {
	    if (_base->lfirst == nullptr) { _current = _previous = nullptr; }
	    if (_current == _base->lfirst) {
		item->next = _base->lfirst;
		if (! _base->lfirst) { _base->llast = item; }
		_base->lfirst = item;
		_current = item; _previous = nullptr;
	    } else {
		item->next = _current;
		_previous->next = item; _current = item;
	    }
	    ++_base->llength;
	}

    public:

	Iterator () : _base(nullptr), _current(nullptr), _previous(nullptr) { }

	Iterator (Fifo<T> *x, bool toEnd = false)
	    : _base(x), _current(toEnd ? nullptr : _base->lfirst),
	      _previous(toEnd ? _base->llast : nullptr)
	{ }

	// Copying of an iterator is explicitely allowed during an
	// initialization (e.g. when supplied as value-parameter in a
	// function-invocation) ...
	Iterator (const Iterator &x)
	    : _base(x._base), _current(x._current), _previous(x._previous)
	{ }

	// The Move-constructor must remove the '_base' and '_current'-values
	// from its input argument 'x' ...
	Iterator (Iterator &&x)
	    : _base(x._base), _current(x._current), _previous(x._previous)
	{
	    x._base = nullptr; x._current = x._previous = nullptr;
	}

	Iterator & operator= (const Iterator &x) {
	    _base = x._base; _current = x._current; _previous = x._previous;
	    return *this;
	}

	Iterator & operator= (Iterator &&x) {
	    _base = x._base; _current = x._current; _previous = x._previous;
	    x._base = nullptr; x._current = x._previous = nullptr;
	    return *this;
	}

	// Destroying of an iterator (does not much) ...
	~Iterator () { _base = nullptr; _current = _previous = nullptr; }


	// Reset the iterator to the first element of the Fifo it is attached
	// to ...
	void reset (Fifo<T> *b = nullptr) {
	    if (b) { _base = b; }
	    _current = _base->lfirst; _previous = nullptr;
	}

	// Advance the iterator (pre- and postfix versions) ...
	Iterator &operator++() { return advance (); }
	Iterator &operator++(int) { return advance (); }

	// Remove the element at the current iterator position from the list.
	// If this was the first or last element in the list, the corresponding
	// pointers (lfirst, llast) in the Fifo pointed to are adjusted
	// accordingly ...
	void remove () {
	    if (_current) {
		if (_current != _base->lfirst) {
		    // _current points to the first element of the Fifo<T>...
		    // Remove the current element from the list and then
		    // 'delete' it ...
		    _previous->next = _current->next;
		    _current->next = nullptr; delete _current;
		    if (!(_current = _previous->next)) {
			_base->llast = _previous;
		    }
		    --_base->llength;
		} else if (! _base->lfirst) {
		    // The Fifo<T> is empty! This means that the iterator also
		    // is "empty", meaning: it points to no element in the
		    // Fifo<T>, but points to the fifo itself nonetheless.
		    _base->llast = nullptr;
		    _current = _previous = nullptr;
		    _base->llength = 0;
		} else {
		    // The list is not empty!
		    _current = _base->lfirst;
		    if ((_base->lfirst = _current->next)) {
			--_base->llength;
		    } else {
			// The list is now empty!
			 _base->llast = nullptr; _base->llength = 0;
		    }
		    // Delete the (previously) current element
		    _current->next = nullptr; delete _current;
		    _current = _base->lfirst;
		    _previous = nullptr;
		}
	    }
	}

	// Insert a new item at the current position.
	// If the Fifo is empty, this means that the iterator is reset to
	// its beginning (which is identical to its end).
	// After the insertion, the iterator points to the inserted item.
	bool insert (const T &value) {
	    Item *newit = new Item (value);
	    if (! newit) { return false; }
	    insert_helper (newit);
	    return true;
	}

	bool insert (T &&value) {
	    Item *newit = new Item (std::move (value));
	    if (! newit) { return false; }
	    insert_helper (newit);
	    return true;
	}

	Fifo<T> truncate () {
	    if (! _base) {
		throw std::runtime_error
		    ("Iterator is not attached to a Fifo");
	    }
	    Fifo<T> res;
	    if (_current) {
		res.lfirst = _current;
		res.llast = _base->llast;
		if ((_base->llast = _previous)) {
		    _base->llast->next = nullptr;
		} else {
		    // Was at the beginning of the Fifo, the iterator
		    // references. This means that the original Fifo is empty
		    // afterwards.
		    _base->lfirst = nullptr;
		}
		// Now, fix the lengths of both Fifos and reset the iterator to
		// the beginning of the original Fifo ...
		reset(); _base->recount(); res.recount();
	    }
	    return res;
	}

	// Returns ‘true’ at the end of a ‘Fifo’ and false otherwise ...
	bool atend () { return _current == nullptr; }

	// Return the element's value the iterator currently points to ...
	T & operator* () { return _current->value; }

	T * operator-> () { return &_current->value; }

	bool operator!= (const Iterator &r) {
	    return (_base != r._base) || (_current != r._current);
	}

	bool operator== (const Iterator &r) {
	    return (_base == r._base) && (_current == r._current);
	}
    };

    using iterator = Iterator;

    // The ConstIterator class is required, because even when specified as
    // 'const Fifo<T> &'-argument the corresponding value is constant and so
    // it's internal pointers can't be assigned to the normal iterator pointers
    // '_base' and '_current'. And without it, such an argument can't be read
    // or manipulated in any way. But, other than the 'Iterator'-class,
    // 'ConstIterator' does not need funtions like 'remove()' and 'insert()',
    // because the underlying Fifo-objects are 'const' (immutable), and hence
    // no '_previous' pointer.
    class ConstIterator {
	const Fifo *_base;
	const Item *_current;
	ConstIterator &advance () {
	    if (!_current) {
		throw std::runtime_error
		    ("Fifo::ConstIterator::++: No more items");
	    }
	    _current = _current->next;
	    return *this;
	}

    public:

	ConstIterator() : _base(nullptr), _current(nullptr) { }

	ConstIterator (const Fifo<T> *x, bool toEnd = false)
	    : _base(x), _current(toEnd ? nullptr : _base->lfirst)
	{ }

	// Copying of an iterator is explicitely allowed during an
	// initialization (e.g. when supplied as value-parameter in a
	// function-invocation) ...
	ConstIterator (const ConstIterator &x)
	    : _base(x._base), _current(x._current)
	{ }

	// The Move-constructor must remove the '_base' and '_current'-values
	// from its input argument 'x' ...
	ConstIterator (ConstIterator &&x)
	    : _base(x._base), _current(x._current)
	{
	    x._base = nullptr; x._current = nullptr;
	}

	ConstIterator & operator= (const ConstIterator &x) {
	    _base = x._base; _current = x._current; return *this;
	}

	ConstIterator & operator= (ConstIterator &&x) {
	    _base = x._base; _current = x._current;
	    x._base = nullptr; x._current = nullptr;
	    return *this;
	}

	// Destroying of an iterator (does not much) ...
	~ConstIterator () { _base = nullptr; _current = nullptr; }


	// Reset the iterator to the first element of the Fifo it is attached
	// to ...
	void reset (Fifo<T> *b = nullptr) {
	    if (b) { _base = b; }
	    _current = _base->lfirst;
	}

	// Advance the iterator (pre- and postfix versions) ...
	ConstIterator &operator++() { return advance (); }
	ConstIterator &operator++(int) { return advance (); }

	// Returns ‘true’ at the end of a ‘Fifo’ and false otherwise ...
	bool atend () { return _current == nullptr; }

	// Return the element's value the iterator currently points to ...
	const T & operator* () const { return _current->value; }

	const T * operator-> () const { return &_current->value; }

	bool operator!= (const ConstIterator &r) {
	    return (_base != r._base) || (_current != r._current);
	}

	bool operator== (const ConstIterator &r) {
	    return (_base == r._base) && (_current == r._current);
	}
    };

    using const_iterator = ConstIterator;

    // The default constructor ...
    Fifo () {
	lfirst = llast = nullptr; llength = 0;
    }

    // A constructor to be used for lists which are generally not empty, such
    // in some (E)BNF-rules, where some lists must not be empty.
    Fifo (const T &single) {
	Item *newitem = new Item (single);
	lfirst = llast = newitem; llength = 1;
    }

    // A seond constructor for one-element lists, but in this case with an
    // rvalue-reference (for rvalue-objects, should make copying large objects
    // a bit more efficient) ...
    Fifo (T &&single) {
	Item *newitem = new Item (std::move(single));
	lfirst = llast = newitem; llength = 1;
    }

    // Copy constructor: This constructor makes a generates a complete copy
    // of the list in ‘x’ (between first and last of the real representation).
    // This constructor is (e.g.) used when a Fifo-value is supplied as value-
    // argument in a function-invocation ...
    Fifo (const Fifo<T> &x) {
	copy (*this, x);
    }

    // Move constructor: Move the pointers from the (temporary) object ‘x’
    // to the current object. This type of constructor ist used for creating
    // Fifo from a temporary Fifo-object (as, e.g., created by a function
    // invocation). A lot faster than the Copy constructor ...
    //
    Fifo (Fifo<T> &&x) {
	lfirst = x.lfirst; llast = x.llast; llength = x.llength;
	// ‘x’ is a temporarily created object, which guarantees that it is
	// destroyed after the invocation of this constructor, so it is no
	// problem to remove the internal Fifo-list from it. After the problem
	// i had with the “move”-assignment, i want to ensure that the
	// transferred Fifo-list is not accidentally destroyed by the
	// invocation of the destructor ...
	x.lfirst = nullptr; x.llast = nullptr; x.llength = 0;
    }

    // Constructor which uses the C++11 feature ‘std::initializer_list’.
    // This makes initialization a lot easier, e.g.:
    //   Fifo<int> x {1, 3, 5, 7, 9};
    Fifo (std::initializer_list<T> vl)
	: lfirst(nullptr), llast(nullptr), llength(0)
    {
	for (auto it = vl.begin(); it != vl.end(); ++it) {
	    Item *newitem = new Item ((*it));
	    if (lfirst) { llast->next = newitem; } else { lfirst = newitem; }
	    llast = newitem; ++llength;
	}
    }

    // Remove each entry of the list ...
    void clear () {
	Item *item;
	while (lfirst) {
	    item = lfirst->next; lfirst->next = nullptr;
	    delete lfirst; lfirst = item;
	}
	llast = nullptr; llength = 0;
    }

    // Destroy the object. Internally it does the same as ‘clear()’ ...
    ~Fifo () {
	clear ();
    }

    // Copy assignment. Uses ‘clear()’ and (the hidden) ‘copy()’ ...
    Fifo & operator= (const Fifo<T> &x) {
	clear ();
	copy (*this, x);
	return *this;
    }

    // Move assignment. Simply for assigning temporarily created values which
    // are cast away after the assignment. This makes the assignment somewhat
    // easier and faster than the ‘copy assignment’, which must copy the whole
    // list ...
    Fifo & operator= (Fifo<T> &&x) {
	clear ();
	lfirst = x.lfirst; llast = x.llast; llength = x.llength;
	// Need this, because in a return-statement, a Fifo-object is locally
	// created and then destroyed after, which would destroy the object
	// being returned. (Especially with a local variable, we have this
	// problem, but also with temporarily created Fifo-objects. Setting
	// ‘lfirst’ and ‘llast’ of the “temporary” source object guarantees
	// that the internal list (which is returned) is left untouched by
	// the desctruction of the local variable/temporarily created Fifo-
	// object.
	x.lfirst = nullptr; x.llast = nullptr;
	return *this;
    }

    // Append an element to the end of a ‘Fifo’-list ...
    Fifo & push (const T &newval) {
	Item *newitem = new Item (newval);
	if (! lfirst) { lfirst = newitem; } else { llast->next = newitem; }
	llast = newitem; ++llength;
	return *this;
    }
    Fifo & push_back (const T &newval) { return push (newval); }

    // (Hopefully) a more efficient version, which shallow-copies "temporary"
    // objects into the new element which is appended to the end of the list.
    Fifo & push (T &&newval) {
	Item *newitem = new Item (std::move(newval));
	if (! lfirst) { lfirst = newitem; } else { llast->next = newitem; }
	llast = newitem; ++llength;
	return *this;
    }
    Fifo & push_back (T &&newval) { return push (std::move (newval)); }

    // Append a (supplied) list to the current one. The supplied list is copied
    // in the process ...
    Fifo & push (const Fifo<T> &in) {
	for (Item *it = in.lfirst; it; it = it->next) {
	    Item *newitem = new Item (*it);
	    if (lfirst) { llast->next = newitem; } else { lfirst = newitem; }
	    llast = newitem; ++llength;
	}
	return *this;
    }
    Fifo & push_back (const Fifo<T> &in) { return push (in); }

    // (Hopefully) a more efficient version, which assumes the supplied list
    // is a temporary value and appends it directly to the target (by setting/
    // modifying some pointers) ...
    Fifo & push (Fifo<T> &&in) {
	if (lfirst) { llast->next = in.lfirst; } else { lfirst = in.lfirst; }
	if (in.llast) { llast = in.llast; }
	llength += in.llength;
	in.lfirst = in.llast = nullptr; in.llength = 0;
	return *this;
    }
    Fifo & push_back (Fifo<T> &in) { return push (std::move(in)); }

    // Insert an element at the beginning of a ‘Fifo’-list ... 
    Fifo & unshift (const T &newval) {
	Item *newitem = new Item (newval);
	newitem->next = lfirst;
	if (!lfirst) { llast = newitem; }
	lfirst = newitem; ++llength;
	return *this;
    }

    Fifo & unshift (T &&newval) {
	Item *newitem = new Item (std::move(newval));
	newitem->next = lfirst;
	if (!lfirst) { llast = newitem; }
	lfirst = newitem; ++llength;
	return *this;
    }

    Fifo & runshift (const Fifo<T> &in) {
	for (Item *it = in.lfirst; it; it = it->next) {
	    Item *newitem = new Item (*it);
	    newitem->next = lfirst;
	    if (! lfirst) { llast = newitem; }
	    lfirst = newitem; ++llength;
	}
	return *this;
    }

    Fifo & unshift (const Fifo<T> &in) {
	// Why “manually” step by step? I could use the member-functions a lot.
	// The reason is simple. This way, it is faster, because with the
	// exception of ‘Item(const Item &x)’, no other function is invoked.
	Item *ifirst = nullptr, *ilast = nullptr;
	for (Item *it = in.lfirst; it; it = it->next) {
	    Item *newitem = new Item (*it);
	    if (ifirst) { ilast->next = newitem; } else { ifirst = newitem; }
	    ilast = newitem; ++llength;
	}
	if (ifirst) { ilast->next = lfirst; lfirst = ifirst; }
	return *this;
    }

    Fifo & unshift (Fifo<T> &&in) {
	// Temporary value 'in'. Here, the modification of some pointers should
	// be enough ...
	if (in.lfirst) { // For an empty list, 'unshift()' is a NOOP ...
	    // Append '*this' to the rvalue-list ...
	    in.llast->next = lfirst;
	    // ... and then make '*this' point to this rvalue-list
	    lfirst = in.lfirst;
	    // If '*this' was empty, its 'llast'-pointer must be adjusted ...
	    if (! llast) { llast = in.llast; }
	    llength += in.llength;
	    // Reset the pointers of 'in' (required!)
	    in.lfirst = in.llast = nullptr; in.llength = 0;
	}
	return *this;
    }

    // Remove the first element from a ‘Fifo’-list ...
    T shift () {
	check_not_empty ("shift");
	Item *item = lfirst->next;
	lfirst->next = nullptr;
	T firstval = std::move(lfirst->value);
	delete lfirst;
	lfirst = item; --llength;
	if (!item) { llast = nullptr; }
	return firstval;
    }

    // Remove the last element from a ‘Fifo’-list. Somewhat slow, but in the
    // cases where i intend to use ‘Fifo’-lists seldom used ...
    T pop () {
	check_not_empty ("pop");
	Item *item = lfirst;
	if (item == llast) {
	    lfirst = llast = nullptr;
	} else {
	    while (item->next != llast) { item = item->next; }
	    llast = item; item = llast->next; llast->next = nullptr;
	}
	T lastval = std::move(item->value); --llength;
	delete item;
	return lastval;
    }

    // Return the first element of a ‘Fifo’-list ...
    T & first () {
	check_not_empty ("first");
	return lfirst->value;
    }

    // Return the last element of a ‘Fifo’-list ...
    T & last () {
	check_not_empty ("last");
	return llast->value;
    }

    // Return true if the Fifo-list is empty ...
    bool empty () const {
	return lfirst == nullptr;
    }

    // Calculate the (current) length of the list ...
    size_t length () const {
	//size_t res = 0;
	//for (Item *i = lfirst; i; i = i->next) { ++res; }
	//return res;
	return llength;
    }

    size_t size () { return llength; }

    // Retrieve the n'th element of the list. If 'n' is greater than the
    // (current) number of elements in the list, generate an exception.
    T & operator[] (size_t ix) {
	Item *i = lfirst;
	while (i && ix > 0) { --ix; i = i->next; }
	if (! i) { throw std::range_error ("index out of bounds"); }
	return i->value;
    }

    // This function should be used in the (very unlikely) case that the
    // member variable 'llength' isn't in sync with the real list length.
    // Additionally, 'llast' (the pointer to the last element of the list)
    // will be checked and fixed ...
    // ATTENTION! Be aware that any of these cases indicates a very serious
    // problem with the software. Fixing 'llength' and 'llast' will not fix
    // the problem in the software; instead it (likely) will leave some memory
    // leaks. So this function is only meant as a temporary fix in order to
    // allow the software a half-decent shutdown.
    size_t recount() {
	size_t rclength = 0;
	Item *it = lfirst;
	if (! it) { llast = it; }
	while (it) {
	    ++rclength;
	    if (! it->next) { llast = it; }
	    it = it->next;
	}
	llength = rclength;
	return llength;
    }

    // Create (and return) an iterator on the ‘Fifo’-list on which this
    // function is invoked ...
    Iterator begin () { Iterator res (this); return res; }
    ConstIterator begin () const { ConstIterator res (this); return res; }
    Iterator end () { Iterator res (this, true); return res; }
    ConstIterator end () const { ConstIterator res (this, true); return res; }

    // Introduced for a shorter testing for non-empty lists
    operator bool() { return lfirst != nullptr; }
};

template<class T> using FifoIterator = typename Fifo<T>::Iterator;

//template<class T>
//bool operator== (const FifoIterator<T> &lhs, const FifoIterator<T> &rhs)
//{
//    return (lhs.base() == rhs.base()) && (lhs.current() == rhs.current());
//}

#endif /*FIFO_H*/
