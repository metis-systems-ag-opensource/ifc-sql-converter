/* nextid.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** ID generator 'next_id()'. This function uses a special ID table in the
** generated database for generating unique Ids for the database representa-
** tions of ENTITY instances, aggregate values, union (SELECT) typed values.
**
*/


#include <stdexcept>
#include <string>

#include "dbif.h"

using std::runtime_error;
using std::string, std::to_string;

int64_t next_id (DBH &dbh, uint32_t modelid, const string &type)
{
    int ec = 0;
    uint32_t type_id;
    int64_t nxid = -1;
    dbh.begin();
    DBQ qh = dbh.query (
	"SELECT _ModelNextId.type_id, _ModelNextId.next_id "
	"FROM _NameIdTrans, _ModelNextId "
	"WHERE"
	" _NameIdTrans.id = _ModelNextId.type_id AND"
	" _NameIdTrans.name = '" + type + "' AND"
	" _ModelNextId._model_id = " + to_string (modelid));
    if (! qh.next()) {
	dbh.rollback();
	throw runtime_error ("next_id(): No id value for '" + type + "'.");
    }
    type_id = (uint32_t) stoul (*qh[0]);	// ALWAYS valid (not NULL)
    nxid = (int64_t) stoul (*qh[1]);		// ALWAYS valid (not NULL)
    ec = dbh.nrquery ("UPDATE _ModelNextId SET next_id = next_id + 1 "
		      "WHERE _model_id = " + to_string (modelid) +
		      " AND type_id = " + to_string (type_id));
    if (ec) {
	dbh.rollback();
	throw runtime_error ("next_id(): " + dbh.errmsg());
    }
    dbh.commit();
    return nxid;
}

void create_nextid (DBH &dbh, uint32_t modelid, const string &type)
{
    int ec = 0;
    uint32_t mtid = 0;

    dbh.begin();

    // Get the base counter which attaches the 'type' to the
    // '_NameIdTrans.id' and '_ModelNextId.type_id' values. If the
    // corresponding entry doesn't exist, create it.
    DBQ qh = dbh.query (
	"SELECT _ModelNextId.type_id, _ModelNextId.next_id "
	"FROM _NameIdTrans, _ModelNextId "
	"WHERE"
	" _NameIdTrans.id = _ModelNextId.type_id AND"
	" _NameIdTrans.name = '_NextIdId' AND"
	" _ModelNextId._model_id = " + to_string (modelid));
    bool found = qh.next();
    if (! found) {
	qh.end();
	mtid = 1;
	// 'INSERT IGNORE' is used here, because the 'SELECT' statement above
	// doesn't leave enough information if the '_NameIdTrans' entry wasn't
	// found or if both entries didn't exist.
	// assigned to the 'type_id' value of '0' (for each model).
	ec = dbh.nrquery ("INSERT IGNORE INTO _NameIdTrans (id, name)"
			  " VALUES (0, '_NextIdId')");
	if (ec) {
	    dbh.rollback();
	    throw runtime_error
		("create_nextid(): _NameIdTrans, _NextIdId - " + dbh.errmsg());
	}

	// It is always clear (in this branch) that the '_ModelNextId' entry
	// didn't exist.
	ec = dbh.nrquery (
	    "INSERT INTO _ModelNextId (_model_id, type_id, next_id) VALUES"
	    " (" + to_string (modelid) + ", 0, " + to_string (mtid + 1) + ")");
	if (ec) {
	    dbh.rollback();
	    throw runtime_error
		("create_nextid(): _ModelNextId, _NextIdId - " + dbh.errmsg());
	}
    } else {
	uint32_t type_id = stoul (*qh[0]);	// ALWAYS valid (not NULL)
	mtid = stoul (*qh[1]);			// ALWAYS valid (not NULL)
	qh.end();
	ec = dbh.nrquery ("UPDATE _ModelNextId SET next_id = next_id + 1 "
			  "WHERE _model_id = " + to_string (modelid) +
			  " AND type_id = 0");
	if (ec) {
	    dbh.rollback();
	    throw runtime_error ("create_nextid(): " + dbh.errmsg());
	}
    }
    ec = dbh.nrquery ("INSERT IGNORE INTO _NameIdTrans (id, name)"
		      " VALUES (" + to_string (mtid) + ", '" + type + "')");
    if (ec) {
	dbh.rollback();
	throw runtime_error
	    ("create_nextid(): _NameIdTrans - " + dbh.errmsg());
    }

    ec = dbh.nrquery ("INSERT INTO _ModelNextId (_model_id, _type_id, next_id)"
		      " VALUES (" + to_string (modelid) + ", " +
		      to_string (mtid) + ", 1)");
    if (ec) {
	dbh.rollback();
	throw runtime_error
	    ("create_nextid(): _NameIdTrans - " + dbh.errmsg());
    }
}
