/* basetype.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Interface file for 'basetype.cc'
** (Get the base type (tag) of a given 'ExpTypes' type tag.)
**
*/
#ifndef BASETYPE_H
#define BASETYPE_H

#include "exptypes.h"

namespace IFC {

ExpTypes basetype (ExpTypes t);;

} /*namespace IFC*/

#endif /*BASETYPE_H*/
