/* modid.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Class for generating an Id (based on MD5) from some strings.
**
*/
#ifndef MODID_H
#define MODID_H

#include <string>
#include <vector>

namespace IFC {

struct ModIdCTX;

struct ModId {
    ModId();
    ModId (const ModId &x);
    ModId (ModId &&x);
    ~ModId();
    void reset();
    void update (const char *msg, size_t msglen);
    void update (const std::string &msg);
    void finalize();
    std::string outhex();
    std::string outb64();
    std::vector<unsigned char> outraw();
private:
    unsigned char id[16];
    ModIdCTX *modidstate;
};

} /*namespace IFC*/

#endif /*MODID_H*/
