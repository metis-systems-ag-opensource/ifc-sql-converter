/* Templates/static/warning.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** A small function which writes a warning/error message to 'std::cerr'.
**
*/

#include <cstdint>
#include <iostream>

#include "warning.h"

namespace IFC {

using std::cerr, std::endl;
using std::string;

void warning (uint32_t instid, uint32_t attrx, const string &msg,
	      const string &query, const string &errmsg)
{
    cerr << "Instance #" << instid << ", attribute " << attrx << ": ";
    warning (msg, query, errmsg);
}

void warning (uint32_t instid, const string &msg,
	      const string &query, const string &errmsg)
{
    cerr << "Instance #" << instid << ": ";
    warning (msg, query, errmsg);
}

void warning (const std::string &msg,
	      const std::string &query,
	      const std::string &errmsg)
{
    cerr << msg << endl;
    if (! query.empty()) { cerr << "Query: " << query << endl; }
    if (! errmsg.empty()) { cerr << "Error:" << errmsg << endl; }
}

} /*namespace IFC*/
