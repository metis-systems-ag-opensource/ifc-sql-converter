/* nextid.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file for 'nextid.cc'
** (ID generator 'next_id()'. This function uses a special ID table in the
**  generated database for generating unique Ids for the database representa-
**  tions of ENTITY instances, aggregate values, union (SELECT) typed values.)
**
*/
#ifndef NEXTID_H
#define NEXTID_H

#include <string>

#include "dbif.h"

int64_t next_id (DBH &dbh, uint32_t modelid, const std::string &type);

void create_nextid (DBH &dbh, uint32_t modelid, const std::string &type);

#endif /*NEXTID_H*/
