/* plctx.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Definition of a a class for a "context" object. This object is used to
** simplify function prototypes (by reducing thr parameter lists).
**
*/
#ifndef PLCTX_H
#define PLCTX_H

#include <string>
#include <utility>

#include "dbif.h"
#include "outputops.h"
#include "typedefs.h"
#include "utils.h"

namespace IFC {

struct PlCtx {
    PlCtx()
      : _cfg(nullptr), _oops(nullptr), _schema(),
	_username(), _dbname(), _dbh(nullptr)
    { }
    PlCtx (ConfData *cfg, OutputMode om, const std::string &schema)
      : _cfg(cfg), _oops(set_output (om)), _schema(schema),
	_username(), _dbname(), _dbh(nullptr)
    { }
    ~PlCtx() {
	if (_dbh) { delete _dbh; }
    }
    DBH &dbh() { return *_dbh; }
    ConfData &cfg() { return *_cfg; }
    const OutputOps &oops() const { return *_oops; }
    bool opendb (const std::string &user, const std::string &pass,
		 bool use_schemadb = false)
    {
	if (_dbh) { return false; }
	std::string dbname (use_schemadb ? lowercase (_schema) : "IFCModel");
	_username = user; _dbname = dbname;
	_dbh = new DBH ((*_cfg)["DBSERVER"], (int) stol ((*_cfg)["DBPORT"]),
			dbname, user, pass);
	_dbh->connect();
	return true;
    }
    bool use_schemadb() {
	bool rv = false;
	if (_dbh) {
	    _dbname = lowercase (_schema);
	    rv = _dbh->selectdb (_dbname);
	}
	return rv;
    }
    bool use_modeldb() {
	bool rv = false;
	if (_dbh) {
	    _dbname = "IFCModel";
	    rv = _dbh->selectdb (_dbname);
	}
	return rv;
    }
    const std::string &username() const { return _username; }
    const std::string &dbname() const { return _dbname; }
    const std::string &schema() const { return _schema; }
private:
    ConfData *_cfg;
    const OutputOps *_oops;
    std::string _schema, _username, _dbname;
    DBH *_dbh;
} /*PlCtx*/;

} /*namespace IFC*/

#endif /*PLCTX_H*/
