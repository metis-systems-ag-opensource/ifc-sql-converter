/* selbase.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Base type of all SELECT data types
**
*/
#ifndef SELBASE_H
#define SELBASE_H

#include "valtypes.h"

#include "expbase.h"

namespace IFC {

struct _ExpSelect : ExpBase {
    _ExpSelect();
    _ExpSelect (ExpTypes ttag, const STEP::Value *v);
    ~_ExpSelect();
    TypeClass typeclass() const { return TypeClass::select; }
//    bool is_entity() const;
    ExpBase * &get();
    bool is_same_as (const ExpBase *rx) const;
private:
    ExpBase *v;
};

} /*namespace IFC*/


#endif /*SELBASE_H*/
