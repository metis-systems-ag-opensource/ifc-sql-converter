/* storemeta.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Store the 'metadata' of an IFC model (the data from the 'HEADER;' section
** of an IFC file).
**
*/

//##DEBUG
//#include <iostream>
//using std::cerr, std::endl, std::flush;
//##

#include <algorithm>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include "modid.h"
#include "utils.h"
#include "warning.h"

#include "storemeta.h"

namespace IFC {

using std::optional;
using std::exception, std::runtime_error;
using std::string, std::to_string, std::stoul;
using std::vector;

#if 0	// Probably not required!
static uint32_t next_id (DBH &dbh, const string &idtype)
{
    DBQ qh = dbh.query ("SELECT next_id ('" + idtype + "')");
    if (! qh.next()) {
	throw runtime_error ("next_id(): '" + idtype + "' no such id type.");
    }
    return (uint32_t) stoul (*qh[0]); // qh[] is guaranteed to be valid!
}
#endif

// Generate a fileid (based on 'modid.{cc,h}').
static string gen_fileid (const IFCModel &model)
{
    ModId modid;
    vector<string> authors (model.authors);
    vector<string> organisations (model.organisations);

    auto sless = [] (const string &a, const string &b) { return a < b; };

    modid.update (model.filename);
    modid.update (model.timestamp);
    modid.update (model.origsys);
    modid.update (model.preprocver);
    modid.update (model.authorization);
    sort (authors.begin(), authors.end(), sless);
    for (auto a: authors) { modid.update (a); }
    sort (organisations.begin(), organisations.end(), sless);
    for (auto o: organisations) { modid.update (o); }
    modid.finalize();
    return modid.outhex();
}


static bool fileid_exists (DBH &dbh, const string &fileid)
{
    string q = "SELECT COUNT(*) FROM Model WHERE fileid = '$#'" %
	       vStringList { fileid };

    try {
	DBQ sth = dbh.query (q);

	sth.next();
	uint32_t cc = (uint32_t) stoul (*sth[0]);
	sth.end();
	return cc > 0;
    } catch (...) {
	throw;
    }
}

uint32_t store_meta (PlCtx &ctx, const IFCModel &model)
{
    DBH &dbh = ctx.dbh();
    const OutputOps &oops = ctx.oops();
    optional<string> currdb = dbh.currentdb();
    string fileid = gen_fileid (model);
    if (! ctx.use_modeldb()) {
	throw runtime_error
	    ("Changing database context (to the model database) failed" +
	     dbh.errmsg());
    }
    if (fileid_exists (dbh, fileid)) {
	oops.write_error ("Entry with fileid '" + fileid + "' already exists,");
	warning ("store_meta(): There are already entries with fileid = " +
		 fileid);
	//return 0;
    }
    dbh.autocommit (false);
    dbh.begin();
    int errs = 0;
    const string insfailmsg = "store_meta(): INSERT failed.";
    try {
	string q = "INSERT INTO Model (fileid, level, sublevel)"
		   " VALUES ('$#', $#, $#)" %
		   vStringList { fileid,
				 to_string (model.level),
				 to_string (model.sublevel) };
	int ec = dbh.nrquery (q);
	if (ec) { warning (insfailmsg, q, dbh.errmsg()); ++errs; }

	uint32_t modelid = (uint32_t) dbh.insert_id();
	uint32_t ix = 0;
	for (auto &dsc: model.filedescription) {
	    q = "INSERT INTO FileDescription VALUES ($#, $#, '$#')" %
		vStringList { to_string (modelid), to_string (++ix),
			      dbh.quote (dsc) };
	    ec = dbh.nrquery (q);
	    if (ec) { warning (insfailmsg, q, dbh.errmsg()); ++errs; }
	}

	uint32_t authorsid = modelid; //next_id (dbh, "Authors");
	uint32_t organisationsid = modelid; //next_id (dbh, "Organisations");
	q = "INSERT INTO FileName VALUES"
	    " ($#, '$#', '$#', '$#', '$#', '$#', $#, $#)" %
	    vStringList { to_string (modelid),
			  dbh.quote (model.filename),
			  dbh.quote (model.timestamp),
			  dbh.quote (model.origsys),
			  dbh.quote (model.preprocver),
			  dbh.quote (model.authorization),
			  to_string (authorsid),
			  to_string (organisationsid) };
	ec = dbh.nrquery (q);
	if (ec) { warning (insfailmsg, q, dbh.errmsg()); ++errs; }

	ix = 0;
	for (auto &author: model.authors) {
	    q = "INSERT INTO Authors VALUES ($#, $#, '$#')" %
		vStringList { to_string (authorsid),
			      to_string (++ix),
			      dbh.quote (author) };
	    ec = dbh.nrquery (q);
	    if (ec) {
		warning (insfailmsg, q, dbh.errmsg());
	    }
	}

	ix = 0;
	for (auto &organisation: model.organisations) {
	    q = "INSERT INTO Organisations VALUES ($#, $#, '$#')" %
		vStringList { to_string (organisationsid),
			      to_string (++ix),
			      dbh.quote (organisation) };
	    ec = dbh.nrquery (q);
	    if (ec) {
		warning (insfailmsg, q, dbh.errmsg());
	    }
	}

	ix = 0;
	for (auto &sn: model.schemas) {
	    q = "INSERT INTO FileSchema VALUES ($#, $#, '$#')" %
		vStringList { to_string (modelid), to_string (++ix),
			      dbh.quote (sn) };
	    ec = dbh.nrquery (q);
	    if (ec) {
		warning (insfailmsg, q, dbh.errmsg());
	    }
	    oops.write_schema (sn);
	}
	dbh.commit();
	dbh.selectdb (currdb);
	oops.write_modelid (modelid); 
	return modelid;
    } catch (exception &e) {
	dbh.rollback();
	dbh.selectdb (currdb);
	oops.write_fatal (e.what());
	throw;
    }
}

} /*namespace IFC*/
