/* idgen.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Id generator to be used for generating the ids for the database tables.
**
*/
#ifndef IDGEN_H
#define IDGEN_H

#include <cstdint>

namespace IFC {

//enum class IdType { entity = 0, number = 1, aggregate = 2, select = 3 };

struct IdGen {
    IdGen (uint32_t modelid);
    ~IdGen();
    
    uint32_t next_objid ();
//##was:    uint32_t next_id (IdType idt);
    uint32_t objid();
    uint32_t modelid();
private:
    uint32_t _modelid;	// The id of the IFCModel.Model table in the database
    uint32_t _objid;
//##was:    uint32_t _ids[4];
};

} /*namespace IFC*/

#endif /*IDGEN_H*/
