/* pds.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Persistent Data Store (aka: the database access module)
**
*/

//##DEBUG
#include <iostream>
using std::cerr, std::endl, std::flush;
//##

#include <cctype>
#include <stdexcept>
#include <set>
#include <string>
#include <system_error>

#include "aggtypes.h"
#include "basetype.h"
#include "enums.h"
#include "errlist.h"
#include "exptypename.h"
#include "plinterface.h"
#include "chelpers.h"
#include "selbase.h"
#include "selhelpers.h"
#include "utils.h"
#include "warning.h"

#include "pds.h"

namespace IFC {

using std::runtime_error, std::domain_error;
using std::set;
using std::string, std::to_string, std::operator""s;
using std::vector;

static string dbconv (PlCtx &ctx, IdGen &id, ExpBase *val,
		      InstanceList &instances, TypeIdMap &tim);

// Storing the type id associated with the just stored entity object in a
// database table '_ObjectTypeId' which allows for an exact identification
// of the stored object. (This is sometimes necessary for attributes which
// may contain a SUBTYPE of the type they were declared with.)
static void store_idref (DBH &dbh, uint32_t modelid, uint32_t objid,
			 uint32_t tix, ExpTypes rtag = ExpTypes::_NoType)
{
    uint32_t rtindex = (uint32_t) rtag;
    string qtmpl = "INSERT INTO `_ObjectTypeId` VALUES ($#, $#, $#, $#)";
    string rtstr = (rtag == ExpTypes::_NoType ? "NULL" : to_string (rtindex));
    string q = qtmpl % vStringList { to_string (modelid),
				     to_string (objid),
				     to_string (tix),
				     rtstr };
    int ec = dbh.nrquery (q);
    if (ec) {
	throw runtime_error
	    ("store_idref(): Insertion failed."
	     EOL "Query: " + q + EOL "Error: " + dbh.errmsg());
    }
}

static void store_idref (DBH &dbh, uint32_t modelid, uint32_t objid,
			 ExpTypes ttag, ExpTypes rtag = ExpTypes::_NoType)
{
    store_idref (dbh, modelid, objid, (uint32_t) ttag, rtag);
}

uint32_t store_entinst (PlCtx &ctx, IdGen &id, EntityBase *ent,
			InstanceList &instances, TypeIdMap &tim)
{
    DBH &dbh = ctx.dbh();
    const OutputOps &oops = ctx.oops();
    // If the entity was already stored, only its database id is returned.
    if (ent->_dbid > 0) {
	// Entity already stored in the database, so do nothing here.
	return ent->_dbid;
    }
    Fifo<string> vallist;
    uint32_t newid = id.next_objid ();
//##was:    uint32_t newid = id.next_id (IdType::entity);
    uint32_t modelid = id.modelid();
    for (auto attr: ent->attributes) {
	vallist.push (dbconv (ctx, id, attr, instances, tim));
    }
    string entname = exptypename (ent);
    string qstr = "INSERT INTO `" + entname + "` VALUES (" +
		  to_string (newid) + "," + to_string (modelid);
    while (! vallist.empty()) { qstr += "," + vallist.shift(); }
    qstr += ")";
    int ec = dbh.nrquery (qstr);
    if (ec) {
	throw runtime_error
	    ("store_entinst(): Insertion of #" +
	     to_string (ent->instanceid()) +
	     " into '" + entname + "' failed." EOL
	     "Query: " + qstr + EOL "Error: " + dbh.errmsg());
    }
    store_idref (dbh, modelid, newid, ent->type());
    ErrorList errors = ent->errors();
    for (auto msg: errors) {	// 'msg' is of 'const char *'
	//OutputOps oops = ctx.oops();
	string e_ins = "INSERT INTO _ErrorLog VALUES ($#, $#, $#, '$#')" %
		       vStringList { to_string (modelid),
				     to_string (newid),
				     to_string (ent->instanceid()),
				     dbh.quote (msg) };
	if (dbh.nrquery (e_ins)) {
	    warning (ent->instanceid(), "Failed to store an error message.",
		     e_ins, dbh.errmsg());
	}
	oops.write_error ("ModelID=$#, ObjId=$#, InstanceId=$#, Msg=$#" %
			  vStringList { to_string (modelid), to_string (newid),
					to_string (ent->instanceid()), msg });
    }
    // Save the database id of the instance just stored in the instance object
    ent->_dbid = newid;
    // and then return it.
    return newid;
}

uint32_t store_numval (DBH &dbh, IdGen &id, ExpNumber *val, ExpTypes ttag)
{
    uint32_t numid = id.next_objid ();
    uint32_t modelid = id.modelid();
//was:    uint32_t numid = id.next_id (IdType::number);
    bool is_real = val->is_real();
    string sval = val->todb();
    string qstr = "INSERT INTO NUMBER VALUES (" + to_string (numid) + ", " +
		  to_string (modelid) + ", " +
		  (val->is_real() ? "TRUE" : "FALSE") + ", " +
		  (is_real ? "NULL" : sval) + ", " +
		  (is_real ? sval : "NULL") + ")";
    int ec = dbh.nrquery (qstr);
    if (ec) {
	throw runtime_error
	    ("Storing NUMBER attribute failed."
	     EOL "Query: " + qstr + EOL "Error: " + dbh.errmsg());

    }
    store_idref (dbh, modelid, numid, ttag);
    return numid;
}

// Determine the element type of a one- or multi-dimensional array.
static ExpTypes base_eltype (AggBase *agg, InstanceList &instances)
{
    set<ExpTypes> aggtypes
	{ ExpTypes::ARRAY, ExpTypes::LIST, ExpTypes::BAG, ExpTypes::SET,
	  ExpTypes::_InstRef };
    ExpTypes eltype = ExpTypes::_NoType;
    if (agg) {
	eltype = agg->eltype();
	if (has (aggtypes, eltype)) {
	    ExpBase *x = nullptr;	// Forgotten initialisations are BAD!
	    if (agg->size() == 0) { return ExpTypes::_NoType; }
	    for (unsigned ix = 0; ix < agg->size(); ++ix) {
		if ((x = agg->at (ix))) { break; }
	    }
	    if (! x) {
		throw runtime_error
		    ("Can't determine element type of aggregate.");
	    }
	    auto ir = dynamic_cast<ExpInstRef *>(x);
	    if (! ir) {
		auto subagg = dynamic_cast<AggBase *>(x);
		return base_eltype (subagg, instances);
	    }
	    uint32_t id = ir->id();
	    if (! ir->is_index()) {
		throw runtime_error
		    ("Unresolved instance reference #" + to_string (id));
	    }
	    EntityBase *e = instances[id];
	    eltype = e->type();
	}
    }
    return eltype;
}

// Store the values of the given aggregate in a given database table. Because
// the elements of this aggregate may aggregate values themselves, this
// function is recursive.
static uint32_t store_aggt (PlCtx &ctx, IdGen &id, AggBase *agg,
			    const string &tblname, InstanceList &instances,
			    TypeIdMap &tim)
{
    DBH &dbh = ctx.dbh();
    // Store an aggregate value (recursively).
    size_t aggsize = agg->size();
    string stpl = "INSERT INTO `$#` VALUES ($#, $#, $#, $#, $#)";
    uint32_t modid = id.modelid();
    uint32_t newid = id.next_objid ();
//##was:    uint32_t newid = id.next_id (IdType::aggregate);
    for (size_t ix = 0; ix < aggsize; ++ix) {
	ExpBase *el = agg->at (ix);
	uint32_t subid = 0;
	string sa_id, val;
	if (! el) {
	    sa_id = "NULL"; val = "NULL";
	} else {
	    TypeClass tc = el->typeclass();
	    if (tc == TypeClass::aggregate) {
		// Aggregate elements are stored differently from the other
		// types. Here, 'sa_id' is set to the value got by storing
		// the aggregate element recursively 'val' is set to NULL.
		auto aggel = dynamic_cast<AggBase *>(el);
		// Storing empty aggregates (aka '()') somewhat different from
		// optional aggregates values which are empty (have no value).
		// The initialisation 'subid = 0' above guarantees that for
		// an empty aggregate the value '0' is stored instead on
		// 'NULL'. This seems the only mechanism which allows for
		// distinguishing both; it is possible, because this system
		// _never_ uses '0' as a regular id value.
		if (aggel->size() > 0) {
		    subid =
			store_aggt (ctx, id, aggel, tblname, instances, tim);
		}
		sa_id = to_string (subid); val = "NULL";
		break;
	    } else {
		sa_id = "NULL"; val = dbconv (ctx, id, el, instances, tim);
	    }
	    string stmt = stpl % vStringList { tblname,
					       to_string (modid),
					       to_string (newid),
					       to_string (ix),
					       sa_id,
					       val };
	    dbh.nrquery (stmt);
	}
    }
    return newid;
}

static uint32_t store_agg (PlCtx &ctx, IdGen &id, AggBase *agg,
			   InstanceList &instances, TypeIdMap &tim)
{
    DBH &dbh = ctx.dbh();
    // Array<...>, List<...>, Bag<...>, Set<...>, Array<Array<...>>, ...
    // How to handle empty aggregates (aka '()')?
    // In the database, these seem indistinguishable from empty values, and
    // cannot be stored as normal aggregate values. Instead, they must be
    // handled in some other way.
    uint32_t aggid = 0;
    if (agg->size() > 0) {
	ExpTypes eltype = base_eltype (agg, instances);
	// Need to distinguish between the element type of the aggregate and
	// its base type. The latter is used for selecting the corresponding
	// table, where the former is used for storing the extra type in the
	// '_ObjectTypeId' table.
	ExpTypes elbasetype = basetype (eltype);
	string tblname = uppercase (id2name (elbasetype)) + "_MV";
	uint32_t aggtindex = tim[tblname];
	aggid = store_aggt (ctx, id, agg, tblname, instances, tim);
	// Store the element type of the aggregate value.
	store_idref (dbh, id.modelid(), aggid, aggtindex, eltype);
    }
    return aggid;
}


// Special type compatibility checker for 'store_sel()'. A given type 't1'
// ('ExpTypes' type tag) is compatible to a given type 't2', if either 't1'
// specifies an ENTITY type and is member of the list of SUPERTYPE tags of
// 't2' (including 't2'), or if 't1' equals 't2' for any other type.
static bool is_type_compatible (ExpTypes t1, ExpTypes t2)
{
    // Check if two given types are compatible.
    TypeList supertypes = get_supertypes (t2);
    if (supertypes.list != nullptr) {
	// For ENTITY types, this means that the type tag 't1' is member of
	// the list of SUPERTYPE tags of 't2'
	for (uint32_t ix = 0; ix < supertypes.listsz; ++ix) {
	    if (t1 == supertypes.list[ix]) { return true; }
	}
	return false;
    }
    // For any other type, this means that both type tags are equal.
    return t1 == t2;
}

// Store an element of a SELECT type. A SELECT table consists of
// an object id, the id of the model, an index (referring the table column
// which contains a valid value), and, for each type in the (expanded) list
// of type alternatives, either NULL, or the value of the SELECT element.
static uint32_t store_sel (PlCtx &ctx, IdGen &id, _ExpSelect *sv,
			   InstanceList &instances, TypeIdMap &tim)
{
    DBH &dbh = ctx.dbh();
    // The IFC type tag of the SELECT type
    ExpTypes stype = sv->type();
    // The real value of the SELECT type instance.
    ExpBase *v = sv->get();
    // The model id
    uint32_t modid = id.modelid();
    // The id of the database row to be inserted
    uint32_t newid = id.next_objid ();
//##was:    uint32_t newid = id.next_id (IdType::select);
    // The name of the database table
    string tblname = id2name (stype);

    // Beginning with the INSERT statement. The prefix values are the
    // generated id and the id of the IFC-model.
    string stmt = "INSERT INTO `$#` VALUES ($#,$#" %
		    vStringList { tblname,
				  to_string (newid),
				  to_string (modid) };

    // Constructing the remaining statement values
    string stail;
    vector<ExpTypes> types = seltypes (stype);
    size_t typessz = types.size();
    ExpTypes vtype = (v ? v->type() : ExpTypes::_NoType);
    if (vtype == ExpTypes::_InstRef) {
	auto iref = dynamic_cast<ExpInstRef *>(v);
	string irefid = to_string (iref->id()), modids = to_string (modid);
	string newids = to_string (newid);
	if (! iref->is_index()) {
	    throw runtime_error
		("Unresolved instance reference (#"+irefid+")\n" +
		 "TABLE = `" + tblname + "`"
		 ", modid = " + modids +
		 ", newid = " + newids);
	}
	v = instances[iref->id()];
	vtype = v->type();
    }
    size_t cx = 0;
    for (size_t ix = 1; ix <= typessz; ++ix) {
	string cval = "NULL";
	if (is_type_compatible (types[ix - 1], vtype) && cx == 0) {
	    cx = ix; cval = dbconv (ctx, id, v, instances, tim);
	}
	stail += "," + cval;
    }
    if (cx == 0) {
	throw runtime_error (
	    "No valid type found in SELECT type list of '"+exptypename(sv)+"'"
	);
    }
    stmt += "," + to_string (cx - 1) + stail + ")";
    int ec = dbh.nrquery (stmt);
    if (ec) {
	throw runtime_error
	    ("Insertion into '" + tblname + "' failed."
	     EOL "Query: " + stmt + EOL "Error: " + dbh.errmsg());
    }
    // Store the SELECT type and the "real" type of the object in the 'idref'
    // table.
    store_idref (dbh, modid, newid, stype, vtype);
//##was:
//    // Store the "real" type of the SELECT type value.
//    store_idref (dbh, modid, newid, vtype);
//##
    return newid;
}

static string dbconv (PlCtx &ctx, IdGen &id, ExpBase *val,
		      InstanceList &instances, TypeIdMap &tim)
{
    DBH &dbh = ctx.dbh();
    uint32_t valid = 0;
    string res;
    TypeClass tc;
    if (! val) { return "NULL"; }
    switch ((tc = val->typeclass())) {
	case TypeClass::aggregate: {
	    // Aggregate elements are stored differently from the other
	    // types. Here, 'sa_id' is set to the value got by storing
	    // the aggregate element recursively 'val' is set to NULL.
	    auto aggval = dynamic_cast<AggBase *>(val);
	    valid = store_agg (ctx, id, aggval, instances, tim);
	    // 'store_agg()' automatically stores the basetype of its elements.
	    // It doesn't store any aggregate value and returns '0' if the
	    // given aggregate has no value.
	    res = to_string (valid);
	    break;
	}
	case TypeClass::entity: {
	    // Entity instances values are stored via 'store_entinst()'
	    // recursively; the the corresponding id then is used as
	    // 'val'; 'sa_id' is set to NULL.
	    auto entval = dynamic_cast<EntityBase *>(val);
	    // 'store_entinst()' automatically stores the basetype of its
	    // elements.
	    valid = store_entinst (ctx, id, entval, instances, tim);
	    res = to_string (valid);
	    break;
	}
	case TypeClass::select: {
	    // SELECT values are stored with 'store_sel()'; the 'id'
	    // returned is used as 'val'; 'sa_id' is set to NULL.
	    auto selval = dynamic_cast<_ExpSelect *>(val);
	    // 'store_sel()' automatically stores the basetype of its elements.
	    valid = store_sel (ctx, id, selval, instances, tim);
	    res = to_string (valid);
	    break;
	}
	case TypeClass::enumeration: {
	    auto enumval = dynamic_cast<_EnumBase *>(val);
	    res = enumval->todb();
	    // An enumeration attribute does not need its "real" type to be
	    // stored, because the real type can always be deduced by the
	    // type the attribute was declared with (in the "object model").
	    break;
	}
	case TypeClass::simple: {
	    auto simpleval = dynamic_cast<SimpleBase *>(val);
	    res = simpleval->todb();
	    // A simple type attribute does not need its "real" type to be
	    // stored, because the real type can always be deduced by the
	    // type the attribute was declared with (in the "object model").
	    break;
	}
	case TypeClass::number: {
	    auto numval = dynamic_cast<ExpNumber *>(val);
	    // A value of the type NUMBER needs its "real" type to be stored,
	    // because the attribute value consists of an id only. This is done
	    // within 'store_numval()', with the "real" type being given as
	    // fourth argument.
	    valid = store_numval (dbh, id, numval, val->type());
	    res = to_string (valid);
	    break;
	}
	case TypeClass::binary: {
	    auto binval = dynamic_cast<ExpBinary *>(val);
	    res = "'" + binval->todb() + "'";
	    // A BINARY attribute does not need its "real" type to be
	    // stored, because the real type can always be deduced by the
	    // type the attribute was declared with (in the "object model").
	    break;
	}
	case TypeClass::string: {
	    auto strval = dynamic_cast<ExpString *>(val);
	    res = "'" + dbh.quote (strval->todb()) + "'";
	    // A STRING attribute does not need its "real" type to be
	    // stored, because the real type can always be deduced by the
	    // type the attribute was declared with (in the "object model").
	    break;
	}
	case TypeClass::instref: {
	    auto irval = dynamic_cast<ExpInstRef *>(val);
	    // I don't know which way to store instance references is the
	    // better one (index or direct pointer), so i'm currently
	    // allowing for both variants being used.
	    if (! irval->is_index()) {
		// FAILURE! Unconverted Instance reference!
		throw runtime_error
		    ("INTERNAL ERROR! Instance reference not resolved.");
	    }
	    // Get the ENTITY instance from the instance list.
	    EntityBase *entinst = instances[irval->id()];
	    // An instance attribute does not needs its "real" type to be
	    // stored, because the only value stored in the database is an id
	    // of the real value, which is stored separately (probably in
	    // another table). But this is done internally in
	    // 'store_entinst()'.
	    valid = store_entinst (ctx, id, entinst, instances, tim);
	    res = to_string (valid);
	    break;
	}
	default: {
	    throw runtime_error (exptypename (val) +
				 ": Unhandled type class " + to_string (tc));
	}
    }
    return res;
}

TypeIdMap get_aggtypeids (PlCtx &ctx)
{
    DBH &dbh = ctx.dbh();
    TypeIdMap res;
    string q = "SELECT tin.id, tin.name FROM _TypeIdName tin, _TypeClass tc"
	       " WHERE tin.typeclass = tc.id AND tc.name = 'aggregate'";
    DBQ qr = dbh.query (q);
    while (qr.next()) {
	// The index operation leaves a 'std::optional<std::string>', so a
	// dereferencing is required here. There is no test for the existance
	// of the corresponding value, because this test is _not_ _required_.
	// The database schema guarantees this. The dereferencing operation
	// for 'std::optional<T> x' is '*x'.
	uint32_t aggtid = stoul (*qr[0]);
	string aggtname = *qr[1];
	res.emplace (aggtname, aggtid);
    }
    qr.end();
    return res;
}

} /*namespace IFC*/
