/* mintypes.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Basic typedefs required for 'valtypes.{cc,h}'
**
*/
#ifndef MINTYPES_H
#define MINTYPES_H

#include <cstdint>
#include <vector>

using Ident = uint32_t;
using Real = double;
using Integer = int32_t;
using Natural = uint32_t;

using BitString = std::vector<bool>;

#define INTEGER_MIN INT32_MIN
#define INTEGER_MAX INT32_MAX
#define NATURAL_MAX UINT32_MAX

#endif /*MINTYPES_H*/

