/* utils.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Interface file for 'utils.cc'
** (Condensed form of the utilities from 'Common' - as 'Common' will probably
**  not exist in the directory containing the source files of the IFC plugin.)
**
*/
#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <set>
#include <string>
#include <vector>

#define LF "\12"
#define CR "\15"
#define CRLF "\15\12"

#if defined(__WIN32__) || defined(__win64__)
#  define EOL CRLF
#else
#  define EOL LF
#endif

/*! Datatype for controlling the "verbosity" of the plugin's output.
**  This type MUST BE THE SAME type as defined in 'Common/outputmode.h's
**  of the EXPRESS/STEP source code.
*/
enum class OutputMode { nothing, essential, all };

typedef int (cscmp_t) (const char *, const char *);

int cscmp (const char *l, const char *r);

int lccmp (const char *l, const char *r);

std::string bin2string (const std::vector<bool> &v);

std::vector<bool> string2bin (const std::string &s);

std::string u2hex (unsigned long v, unsigned int fw = 1, bool upper = true);

std::string i2hex (long v, unsigned int fw = 1, bool upper = true);

const long double epsilon = 1E-12;

std::string float2string (long double x, int maxexp = 6, int maxdigits = 14);

void trim (std::string &s, const std::string &chars = " ");

std::string trim (const std::string &in, const std::string &chars = " ");

std::string uppercase (const std::string &s);

std::string lowercase (const std::string &s);

using vStringList = std::vector<std::string>;

std::string operator% (const std::string &f, const vStringList &v);

template<class T> bool has (const std::set<T> &s, const T &v)
{
    return s.count (v) > 0;
}

std::istream& readline (std::istream& is, std::string& str);

#endif /*UTILS_H*/
