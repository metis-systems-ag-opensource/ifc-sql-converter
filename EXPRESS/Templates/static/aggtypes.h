/* aggtypes.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Template definitions for the aggregation types ARRAY, LIST, BAG and SET.
**
** PLEASE NOTE: This is a dependent "module". It can only be used if
** 'expbase.h' was #include'd prior to this one. I extracted this part from
** 'expbase.h' for increasing the clarity of the code a bit.
*/
#ifndef AGGTYPES_H
#define AGGTYPES_H

#include "expbase.h"

namespace IFC {

struct AggBase : ExpBase {
    AggBase () : values(nullptr), _size(0) { }
    AggBase (uint32_t sz) : _size(sz) { init(); }
    ~AggBase() {
	if (values) {
	    for (uint32_t ix = 0; ix < _size; ++ix) {
		auto &v = values[ix];
		if (v && ! v->is_entity()) { delete v; v = nullptr; }
	    }
	    delete[] values; values = nullptr;
	}
    }
    TypeClass typeclass() const { return TypeClass::aggregate; }
    size_t size() { return _size; }
    ExpBase * &at (uint32_t ix) {
	if (ix >= _size) { throw IndexError (ix, 0, _size - 1); }
	return values[ix];
    }
    const ExpBase * at (uint32_t ix) const {
	if (ix >= _size) { throw IndexError (ix, 0, _size - 1); }
	return values[ix];
    }
    virtual ExpTypes eltype() const { return ExpTypes::_NoType; }
protected:
    ExpBase **values;
    uint32_t _size;
    virtual bool aggcheck (ExpBase **values, size_t len, ExpBase * v) const = 0;
    void init(uint32_t sz = 0)
    {
	if (sz > 0) { _size = sz; }
	values = new ExpBase*[_size];
	for (uint32_t ix = 0; ix < _size; ++ix) { values[ix] = nullptr; }
    }
};

#define STR(x) #x
#define XSTR(x) STR(x)

template<class T, int32_t M, int32_t N>
struct Array : AggBase {

    // ARRAY with 'null' values.
    Array(bool opt = false, bool uniq = false)
	: AggBase ((uint32_t) (N - M + 1)), optional(opt), unique(uniq)
    {
	using std::domain_error;
	if (! optional) {
	    throw domain_error
		("Can't leave an ARRAY with non-optional elements"
		 " uninitialised");
	}
    }

    // Array 
    Array (const STEP::Value *v, bool opt = false, bool uniq = false)
	: AggBase ((uint32_t) (N - M + 1)), optional(opt), unique(uniq)
    {
	using std::exception, std::domain_error, std::range_error;
	auto lv = dynamic_cast<const STEP::ListValue *>(v);
	if (! lv) { throw AggExpected(); }
	if (lv->size() != _size) {
	    throw range_error ("The number of aggregate elements doesn't"
			       " match the array size");
	}
	size_t ix = 0;
	// How to implement the UNIQUE constraint
	try {
	    for (auto ev: lv->valuelist) {
		STEP::ValType evtype = ev->type();
		if (evtype == STEP::ValType::unspecified) {
		    throw domain_error
			("'*' is not allowed in aggregate values");
		}
		if (evtype == STEP::ValType::null && ! optional) {
		    throw domain_error ("'$' is not allowed because the"
					" ARRAY is not marked OPTIONAL");
		} else {
		    auto nv = new T (ev);
		    // Need to add code for the UNIQUE-check here
		    if (! aggcheck (values, ix, nv)) {
			throw domain_error
			    ("Current value violates the UNIQUE constraint");
		    }
		    values[ix] = nv;
		}
		++ix;
	    }
	} catch (domain_error &e) {
	    throw domain_error
		("Invalid value at " + std::to_string (ix + 1) + " in an " +
		 " aggregate value: " + e.what());
	}
    }
    ExpTypes type() const { return ExpTypes::ARRAY; }
    bool is_same_as (const ExpBase *rx) const {
	auto ra = dynamic_cast<const Array<T, M, N> *>(rx);
	if (! ra) { return false; }
	for (size_t ix = 0; ix < _size; ++ix) {
	    ExpBase *l = values[ix], *r = ra->values[ix];
	    if ((l && !r) || (!l && r) || (l->is_same_as (r))) { return false; }
	}
	return true;
    }
    // Want the 'static' type here...
    ExpTypes eltype() const { T x; return x.type(); }
private:
    bool optional, unique;
//protected:
    bool aggcheck (ExpBase **value, size_t sz, ExpBase *v) const {
	if (unique) {
	    for (size_t ix = 0; ix < sz; ++ix) {
		if (v && value[ix] && v->is_same_as (value[ix])) {
		    return false;
		}
	    }
	}
	return true;
    }
};

template<class T>
bool operator!= (const T &l, const T &r)
{
    return ! (l == r);
}

template<class T>
struct List : AggBase {
    List () : AggBase(), unique(false) { }
    List (const STEP::Value *v, bool unique = false)
	: AggBase(), unique(unique)
    {
	using std::exception, std::domain_error, std::range_error;
	auto lv = dynamic_cast<const STEP::ListValue *>(v);
	if (! lv) { throw AggExpected(); }
	init (lv->size());
	uint32_t ix = 0;
	// How to implement the UNIQUE constraint
	for (auto ev: lv->valuelist) {
	    STEP::ValType evtype = ev->type();
	    if (evtype == STEP::ValType::unspecified
	    ||  evtype == STEP::ValType::null) {
		throw ValIndexErr
		    (ix, "'$' or '*' are not allowed in LIST aggregates");
	    }
	    try {
		auto nv = new T (ev);
		if (! aggcheck (values, ix, nv)) {
		    throw ValIndexErr
			(ix, "Value violates the UNIQUE constraint");
		}
		values[ix] = nv;
	    } catch (std::exception &e) {
		if (dynamic_cast<ValIndexErr *>(&e)) { throw; }
		throw ValIndexErr (ix, e.what());
	    }
	    ++ix;
	}
    }
    ExpTypes type() const { return ExpTypes::LIST; }
    bool is_same_as (const ExpBase *rx) const {
	auto rl = dynamic_cast<const List<T> *>(rx);
	if (! rl) { return false; }
	if (_size != rl->_size) { return false; }
	for (uint32_t ix = 0; ix < _size; ++ix) {
	    if (values[ix]->is_same_as (rl->values[ix])) { return false; }
	}
	return true;
    }
    // Want the 'static' type here...
    ExpTypes eltype() const { T x; return x.type(); }
protected:
    bool unique;
private:
    bool aggcheck (ExpBase **values, size_t sz, ExpBase *v) const {
	if (unique) {
	    for (size_t ix = 0; ix < sz; ++ix) {
		if (v->is_same_as (values[ix])) { return false; }
	    }
	}
	return true;
    }
};

template<class T, uint32_t M, uint32_t N> struct RList : List<T> {
    RList() : List<T>() { }
    RList (const STEP::Value *v, bool unique = false)
	: List<T> (v, unique)
    {
	using std::range_error;
	if (this->_size < M || (N > 0 && this->_size > N)) {
	    throw RAggValueErr ("LIST");
	}
    }
};

template<class T>
struct Bag : AggBase {
    Bag () : AggBase() { }
    Bag (const STEP::Value *v)
	: AggBase()
    {
	using std::exception, std::domain_error, std::range_error;
	auto lv = dynamic_cast<const STEP::ListValue *>(v);
	if (! lv) { throw AggExpected(); }
	init (lv->size());
	size_t ix = 0;
	// How to implement the UNIQUE constraint
	for (auto ev: lv->valuelist) {
	    STEP::ValType evtype = ev->type();
	    if (evtype == STEP::ValType::unspecified) {
		throw ValIndexErr
		    (ix, "'*' is not allowed in BAG aggregates");
	    }
	    if (evtype != STEP::ValType::null) {
		try {
		    auto nv = new T (ev);
		    values[ix++] = nv;
		} catch (std::exception &e) {
		    throw ValIndexErr (ix, e.what());
		}
	    }
	    ++ix;
	}
    }
    ExpTypes type() const { return ExpTypes::BAG; }
    bool is_same_as (const ExpBase *rx) const {
	// Very complex operation:
	// Need to check that each (non-NULL) element of the current BAG occurs
	// in 'rl' in the same quantity, and than that each (non-NULL) element
	// of 'rl' occurs in the same quantity in the current BAG again in the
	// same quantity. Because i have no method for using 'std::map' or
	// 'std::unordered_map', i must rely on a simple collecting element for
	// element, together with a counter.
	using std::make_pair;
	using ECPair = std::pair<T *, int>;
	auto rl = dynamic_cast<const Bag<T> *>(rx);
	if (! rl) { return false; }
	std::vector<ECPair> elcoll;
	for (size_t ix = 0; ix < _size; ++ix) {
	    bool found = false;
	    if (! values[ix]) { continue; }
	    for (auto &[tp, cc]: elcoll) {
		if (tp->is_same_as (values[ix])) { ++cc; found = true; break; }
	    }
	    if (! found) {
		elcoll.push_back (make_pair<ECPair> (values[ix], 1));
	    }
	}
	for (size_t ix = 0; ix < rl->_size; ++ix) {
	    bool found = false;
	    if (! rl->values[ix]) { continue; }
	    for (auto &[tp, cc]: elcoll) {
		if (tp->is_same_as (rl->values[ix])) {
		    --cc; found = true; break;
		}
	    }
	    if (! found) { return false; }
	}
	for (auto &[tp, cc]: elcoll) {
	    if (cc != 0) { return false; }
	}
	elcoll.clear();
	for (size_t ix = 0; ix < rl->_size; ++ix) {
	    bool found = false;
	    if (! rl->values[ix]) { continue; }
	    for (auto &[tp, cc]: elcoll) {
		if (tp->is_same_as (rl->values[ix])) {
		    ++cc; found = true; break;
		}
	    }
	    if (! found) {
		elcoll.push_back (make_pair<ECPair> (rl.values[ix], 1));
	    }
	}
	for (size_t ix = 0; ix < _size; ++ix) {
	    bool found = false;
	    if (! values[ix]) { continue; }
	    for (auto &[tp, cc]: elcoll) {
		if (tp->is_same_as (values[ix])) { --cc; found = true; break; }
	    }
	    if (! found) { return false; }
	}
	for (auto &[tp, cc]: elcoll) {
	    if (cc != 0) { return false; }
	}
	return true;
    }
    // Want the 'static' type here...
    ExpTypes eltype() const { T x; return x.type(); }
private:
    bool aggcheck (ExpBase* values[], size_t sz, ExpBase *v) const {
	return true;
    }
};

template<class T, uint32_t M, uint32_t N> struct RBag : Bag<T> {
    RBag() : Bag<T>() { }
    RBag (const STEP::Value *v)
	: Bag<T> (v)
    {
	using std::range_error;
	if (this->_size < M || (N > 0 && this->_size > N)) {
	    throw RAggValueErr ("BAG");
	}
    }
};

template<class T>
struct Set : AggBase {
    Set () : AggBase() { }
    Set (const STEP::Value *v)
	: AggBase()
    {
	using std::exception, std::domain_error, std::range_error;
	auto lv = dynamic_cast<const STEP::ListValue *>(v);
	if (! lv) { throw AggExpected(); }
	init (lv->size());
	size_t ix = 0;
	// How to implement the UNIQUE constraint
	for (auto ev: lv->valuelist) {
	    STEP::ValType evtype = ev->type();
	    if (evtype == STEP::ValType::unspecified
	    ||  evtype == STEP::ValType::null) {
		throw ValIndexErr
		    (ix, "'$' or '*' are not allowed in SET aggregates");
	    }
	    try {
		auto nv = new T (ev);
		if (! aggcheck (values, ix, nv)) {
		    throw ValIndexErr (ix, "Value is not UNIQUE");
		}
		values[ix] = nv;
	    } catch (std::exception &e) {
		throw ValIndexErr (ix, e.what());
	    }
	    ++ix;
	}
    }
    ExpTypes type() const { return ExpTypes::SET; }
    bool is_same_as (const ExpBase *rx) const {
	// (A little bit) less complex operation:
	// Because in a SET, each element can occur only once, it is enough
	// to check if each element of the left set is also an element of the
	// right set and vice versa.
	auto rl = dynamic_cast<const Set<T> *>(rx);
	if (! rl) { return false; }
	std::vector<T *> elcoll;
	if (_size != rl->_size) { return false; }
	for (size_t ix = 0; ix < _size; ++ix) {
	    if (! aggcheck (rl->values, rl->_size, values[ix])) {
		return false;
	    }
	}
	for (size_t ix = 0; ix < rl->_size; ++ix) {
	    if (! aggcheck (values, _size, rl->values[ix])) { return false; }
	}
	return true;
    }
    // Want the 'static' type here...
    ExpTypes eltype() const { T x; return x.type(); }
private:
    bool aggcheck (ExpBase **values, size_t sz, ExpBase *v) const {
	for (size_t ix = 0; ix < sz; ++ix) {
	    if (v->is_same_as (values[ix])) { return false; }
	}
	return true;
    }
};

template<class T, uint32_t M, uint32_t N> struct RSet : Set<T> {
    RSet() : Set<T>() { }
    RSet (const STEP::Value *v)
	: Set<T> (v)
    {
	using std::range_error;
	if (this->_size < M || (N > 0 && this->_size > N)) {
	    throw RAggValueErr ("SET");
	}
    }
};

} /*namespace IFC*/

#endif /*AGGTYPES_H*/
