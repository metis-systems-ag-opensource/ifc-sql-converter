/* valtypes.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Implementation part of 'valtypes.h'
** (The types of the values used for instances and attributes)
**
*/

#include <stdexcept>

#include "utils.h"

#include "valtypes.h"

namespace STEP {

using std::runtime_error, std::domain_error;
using std::string, std::to_string;
using std::vector;

#if 0 
static const char *valtype_name (Value *v) 
{ 
    switch (v->type()) { 
        case ValType::logical:     return "LogicalValue"; 
        case ValType::integer:         return "IntValue"; 
        case ValType::real:       return "FloatValue"; 
        case ValType::string:      return "StringValue"; 
        case ValType::bitstring:   return "BitSttingValue"; 
        case ValType::enumval:        return "EnumValue"; 
        case ValType::constvalid:  return "ConstValIdValue"; 
        case ValType::constinstid: return "ConstInstIdValue"; 
        case ValType::valid:       return "ValIdValue"; 
        case ValType::instid:      return "InstIdValue"; 
        case ValType::null:        return "NullValue"; 
        case ValType::unspecified: return "UnspecifiedValue"; 
        case ValType::list:        return "ListValue"; 
        case ValType::url:         return "UrlValue"; 
        case ValType::entity:      return "EntityValue"; 
        case ValType::typedparam:  return "TypedParamValue"; 
        default:                     return "***???***"; 
    } 
} 
#endif

const refid_t refid_max = UINT32_MAX;

static void vallist_delete (valuelist_t &l)
{
    for (auto it: l) { delete it; }
    l.clear ();
}

//struct LogicalValue : public Value {
LogicalValue::LogicalValue (Logical val) : value(val) { }

LogicalValue::~LogicalValue() { }

ValType LogicalValue::type() const { return ValType::logical; }

string LogicalValue::repr() const {
    return (value == l_undefined ? ".U." :
	    value == l_false ? ".F." : ".T.");
}

//} /*LogicalValue*/;

//struct IntValue : public Value {
IntValue::IntValue(): value(0) { }

IntValue::IntValue (Integer val) : value(val) { }

IntValue::~IntValue() { }

ValType IntValue::type() const { return ValType::integer; }

string IntValue::repr() const { return to_string (value); }

//} /*IntValue*/;

//struct FloatValue : public Value {
FloatValue::FloatValue () : value(0.0) { }

FloatValue::FloatValue (Real val) : value(val) { }

FloatValue::~FloatValue() { }

ValType FloatValue::type() const { return ValType::real; }

string FloatValue::repr() const { return float2string (value); }

//} /*FloatValue*/;

//struct StringValue : public Value {
StringValue::StringValue() : value("") { }

StringValue::StringValue (const std::string &val) : value(val) { }

StringValue::~StringValue () { value.clear(); }

ValType StringValue::type() const { return ValType::string; }

string StringValue::repr() const {
    string res, tmp;
    for (size_t ix = 0; ix < value.size(); ++ix) {
	char ch = value[ix];
	if ((ch > 0 && ch < 32) || ch == 127) {
	    tmp += "\\x"; tmp += u2hex (ch, 2);
	} else {
	    switch (ch) {
		case '\'': tmp += "''"; break;
		case '\\': tmp += "\\\\"; break;
		default:   tmp.push_back (ch); break;
	    }
	}
    }
    res.reserve (tmp.size() + 1);
    res = tmp;
    return "'" + res + "'";
}

//} /*StringValue*/;

//struct BitStringValue : public Value {
BitStringValue::BitStringValue() : value() { }

BitStringValue::BitStringValue (const string &repr)
  : value(string2bin (repr))
{ }

BitStringValue::BitStringValue (const BitStringValue &x)
  : value(x.value)
{ }

BitStringValue::~BitStringValue() { }

ValType BitStringValue::type() const { return ValType::bitstring; }

string BitStringValue::repr() const { return '"' + bin2string (value) + '"'; }
//} /*BitStringValue*/;

//struct EnumValue : public Value {
EnumValue::EnumValue (const std::string &val) : value(val) { }

EnumValue::~EnumValue() { }

ValType EnumValue::type() const { return ValType::enumval; }

string EnumValue::repr() const { return "." + value + "."; }

//} /*EnumValue*/;


//struct ConstValIdValue : public Value {
ConstValIdValue::ConstValIdValue (const std::string &valid)
  : id(valid)
{ }

ConstValIdValue::~ConstValIdValue() { }

ValType ConstValIdValue::type() const { return ValType::constvalid; }

string ConstValIdValue::repr() const { return "@" + id; }

//} /*ConstValIdValue*/;

//struct ConstInstIdValue : public Value {
ConstInstIdValue::ConstInstIdValue (const std::string &instid)
  : id(instid)
{ }

ConstInstIdValue::~ConstInstIdValue() { }

ValType ConstInstIdValue::type() const { return ValType::constinstid; }

string ConstInstIdValue::repr() const { return "#" + id; }

//} /*ConstInstIdValue*/;

//struct ValIdValue : public Value {
ValIdValue::ValIdValue (refid_t valid) : id(valid) { }

ValIdValue::~ValIdValue () { }

ValType ValIdValue::type() const { return ValType::valid; }

string ValIdValue::repr() const { return "@" + to_string (id); }

//} /*ValIdValue*/;

//struct InstIdValue : public Value {
InstIdValue::InstIdValue (refid_t instid) : id(instid), flags(0) { }

InstIdValue::~InstIdValue () { }

ValType InstIdValue::type() const { return ValType::instid; }

string InstIdValue::repr() const {
    if ((flags & 1) == 0) {
	return "#" + to_string (id);
    }
    throw domain_error ("A direct index cannot be represented.");
}

void InstIdValue::set_index (refid_t ix) {
    id = ix; flags |= 1;
}

bool InstIdValue::is_index() { return (flags & 1) != 0; }

//} /*InstIdValue*/;

//struct TypedParamValue : public Value {
TypedParamValue::TypedParamValue (Ident id, Value *val)
  : value(val), name(id)
{ }

TypedParamValue::~TypedParamValue() {
    delete value;
}

ValType TypedParamValue::type() const { return ValType::typedparam; }

string TypedParamValue::repr() const {
    throw runtime_error ("TypedParamValue::repr() is not implemented yet.");
}

//} /*TypedParamValue*/;

//struct NullValue : public Value {
NullValue::NullValue () { }

NullValue::~NullValue () { }

ValType NullValue::type() const { return ValType::null; }

string NullValue::repr() const { return "?"; }

//} /*NullValue*/;

//struct UnspecifiedValue : public Value {
UnspecifiedValue::UnspecifiedValue () { }

UnspecifiedValue::~UnspecifiedValue () { }

ValType UnspecifiedValue::type() const { return ValType::unspecified; }

string UnspecifiedValue::repr() const { return "*"; }

//} /*UnspecifiedValue*/;

//struct ListValue : public Value {
ListValue::ListValue (Fifo<Value *> * &list)
  : valuelist()
{
    valuelist.reserve (list->length ());
    while (! list->empty ()) {
	Value *val = list->shift ();
	valuelist.push_back (val);
    }
    delete list; list = nullptr;
}

ListValue::ListValue() : valuelist() { }

ListValue::~ListValue() {
    vallist_delete (valuelist);
}

ValType ListValue::type() const { return ValType::list; }

string ListValue::repr() const {
    string tmp = "(", res;
    bool first = true;
    for (auto v: valuelist) {
	if (first) { first = false; } else { tmp += ","; }
	tmp += v->repr();
    }
    res.reserve (tmp.size() + 1); res = tmp;
    return res;
}

size_t ListValue::size() const { return valuelist.size(); }

vector<Value *>::const_iterator ListValue::begin() const {
    return valuelist.begin();
}
vector<Value *>::iterator ListValue::begin() { return valuelist.begin(); }
vector<Value *>::const_iterator ListValue::end() const {
    return valuelist.end();
}
vector<Value *>::iterator ListValue::end() { return valuelist.end(); }

//} /*ListValue*/;

//struct UrlValue : public Value {
UrlValue::UrlValue (const std::string &val) : value(val) { }

UrlValue::~UrlValue () { }

ValType UrlValue::type() const { return ValType::url; }

string UrlValue::repr() const { return "<" + value + ">"; }

//} /*UrlValue*/;

//struct EntityValue : public Value {
EntityValue::EntityValue (Ident id, Fifo<Value *> * &attrs)
{
    name = id;
    if (attrs) {
	attributes.reserve (attrs->length ());
	while (! attrs->empty ()) {
	    Value *attr = attrs->shift ();
	    attributes.push_back (attr);
	}
	delete attrs; attrs = nullptr;
    }
}

EntityValue::EntityValue (EntityValue &&x)
  : attributes(std::move (x.attributes)), name (x.name)
{ }

EntityValue::~EntityValue () {
    vallist_delete (attributes);
}

ValType EntityValue::type() const { return ValType::entity; }

string EntityValue::repr() const {
    throw runtime_error ("EntityType::repr() is not implemented yet.");
}

//} /*EntityValue*/;

//struct Instance : Ptree {
Instance::Instance (refid_t instid, EntityValue &ent)
    : entity(std::move (ent)), id(instid)
{ }
//Instance::Instance (Instance &&x) { *this = x; }
//Instance & operator= (Instance &&x) { *this = x; }
Instance::~Instance () { };
//} /*Instance*/;

} /*namespace STEP*/
