/*! \file stmt.h
**  Types representing statements (in FUNCTIONs, PROCEDUREs, RULEs).
**
**  EXPRESS is a data definition language. Nonetheless, it has the ability to
**  define algorithms (FUNCTIONs, PROCEDUREs, RULEs) which contain statements.
**  This module contains the structure types representing these statements
**  internally.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Structures for generating statement-trees
**
*/
#ifndef STMT_H
#define STMT_H

#include "filepos.h"
#include "expr.h"

/*! Base type of all statement types. */
struct Stmt {
    /*! Like in the other cases of multiple types represented by a base type,
    **  it is easier to use a type tag for identifying the "real" type (runtime
    **  type) behind a `Stmt *`, than a chain of `dynamic_cast<T *>()`
    **  operations.
    */
    enum Type {
	assignment, return_stmt, escape_stmt, loop_stmt,
	if_stmt, case_stmt, compound_stmt, skip_stmt
    };

    /*! The constructor does nothing, because this is only an abstract
    **  base class.
    */
    Stmt() { }

    /*! Declaring the destructore `virtual` – forcing the installation of a
    **  VMT (even in the derived types).
    */
    virtual ~Stmt() { }

    /*! Returns the `Stmt::Type` tag of the corresponding runtime type.
    **  `= 0` here, because the base class is abstract == has never a
    **  representation == doesn't need its own runtime type tag.
    */
    virtual Type type() const = 0;

    /* `type_name()` is (primarily) used for error messages. */
    virtual const std::string type_name() const = 0;
//    Stmt (Stmt &x) = delete;
//    Stmt & operator= (const Stmt &x) = delete;
    void where (const FilePos &p) { position = p; }
    const FilePos & where() { return position; }
/**/
    FilePos position;
} /*Stmt*/;

/*! Type alias for helping to define the other types. */
using ExprPtr = Expr *;

/*! Type alias for helping to define the other types. */
using StmtPtr = Stmt *;

/*! An assignment statement is something like
**
**   reference := expression
*/
struct AssignStmt : public Stmt {
    AssignStmt();
    AssignStmt (Reference *v, ExprPtr e);
    AssignStmt (AssignStmt &x) = delete;
    AssignStmt (AssignStmt &&x);
    ~AssignStmt();
    Type type() const;
    const std::string type_name() const { return "AssignStmt"; }
    AssignStmt & operator= (const AssignStmt &x) = delete;
    AssignStmt & operator= (AssignStmt &&x);
/**/
    Reference *var; ExprPtr expr;
} /*AssignStmt*/;

/*! A return statement when in a FUNCTION is something like
**
**   RETURN expression
**
** whereas in a procedure it consists simply of the keyword
**
**   RETURN
** .
*/
struct ReturnStmt : public Stmt {
    ReturnStmt();
    ReturnStmt (ExprPtr e);
    ReturnStmt (ReturnStmt &x) = delete;
    ReturnStmt (ReturnStmt &&x);
    ReturnStmt & operator= (const ReturnStmt &x) = delete;
    ReturnStmt & operator= (ReturnStmt &&x);
    ~ReturnStmt();
    Type type() const;
    const std::string type_name() const { return "ReturnStmt"; }
/**/
    ExprPtr expr;
} /*ReturnStmt*/;

/*! An escape statement leaves the innermost FOR or WHILE loop immediately.
**  Outside of a loop, it is invalid.
**  It looks like
**
**    ESCAPE
**
*/
struct EscapeStmt : public Stmt {
    EscapeStmt();
//    EscapeStmt (EscapeStmt &x) = delete;
    EscapeStmt (EscapeStmt &&x);
    EscapeStmt & operator= (EscapeStmt &&x);
    ~EscapeStmt();
    Type type() const;
    const std::string type_name() const { return "EscapeStmt"; }
} /*EscapeStmt*/;


/*! A loop range is the association of a loop variable (an `Ident`) with two
**  expressions, denoting the start-value, the end-value, and an increment.
*/
struct LoopRange { Ident id; Expr *from, *to, *increment; };

/*! A loop statement has the general form:
** ```
**   REPEAT <Ident> := <from-expr> TO <to-expr> BY <incr-expr>
**     WHILE <while-cond> UNTIL <until-cond> ;
**      <statement-list>
**   END_REPEAT ;
** ```
** The `BY <incr-expr>` is currently not implemented.
*/
struct LoopStmt : public Stmt {
    LoopStmt();
    // Destructive on first and third statement!
    LoopStmt (LoopRange * &lrng, ExprPtr wcond,
	      Fifo<StmtPtr> * &stmts, ExprPtr ucond);
    LoopStmt (LoopStmt &x) = delete;
    LoopStmt (LoopStmt &&x);
    LoopStmt & operator= (const LoopStmt &x) = delete;
    LoopStmt & operator= (LoopStmt &&x);
    ~LoopStmt();
    Type type() const;
    const std::string type_name() const { return "LoopStmt"; }
/**/
    ExprPtr from, to;
    ExprPtr wcond, ucond;
    Fifo<StmtPtr> stmtlist;
    Ident id;
} /*LoopStmt*/;

/* An `IGrdStmts` object consists of a condition and an associated statement
** list.
*/
struct IGrdStmts {
    IGrdStmts();
    IGrdStmts (ExprPtr cc, Fifo<StmtPtr> &stmts);
    IGrdStmts (ExprPtr cc, Fifo<StmtPtr> &&stmts);
    IGrdStmts (IGrdStmts &x);
    IGrdStmts (IGrdStmts &&x);
    //IGrdStmts & operator= (const IGrdStmts &x);
    IGrdStmts & operator= (IGrdStmts &&x);
    ~IGrdStmts();
/**/
    ExprPtr cond;
    Fifo<StmtPtr> stmtlist;
} /*IGrdStmts*/;

/* An `IF` statement has the general form:
** ```
**   IF <expression> THEN <stamenent list> ELSE <statement list> END_IF;
** ```
** or
** ```
**   IF <expression> THEN <stamenent list> END_IF;
** ```
** This type allows for a bit more than the EXPRESS `IF` statements.
*/
struct IfStmt : public Stmt {
    IfStmt() = delete;
    IfStmt (ExprPtr cond, Fifo<StmtPtr> * &stmts);
    IfStmt (IfStmt &x) = delete;
    IfStmt (IfStmt &&x);
    ~IfStmt();
    Type type() const;
    const std::string type_name() const { return "IfStmt"; }
    IfStmt & operator= (const IfStmt &x) = delete;
    IfStmt & operator= (IfStmt &&x);
    void addElif (ExprPtr cond, Fifo<StmtPtr> * &stmts);
    void addElse (Fifo<StmtPtr> * &stmts);
/**/
    Fifo<IGrdStmts> ifelifpart;
    Fifo<StmtPtr> elsepart;
} /*IfStmt*/;

/*! Helper type for defining `CASE` statements. */
struct CGrdStmts {
    CGrdStmts();
    CGrdStmts (Fifo<ExprPtr> &cll, StmtPtr stmt);
    CGrdStmts (Fifo<ExprPtr> * &cll, StmtPtr stmt);
    CGrdStmts (const CGrdStmts &x);
    CGrdStmts (CGrdStmts &&x);
    CGrdStmts & operator= (const CGrdStmts &x);
    CGrdStmts & operator= (CGrdStmts &&x);
    ~CGrdStmts();
/**/
    Fifo<ExprPtr> caselabels;
    StmtPtr stmt;
} /*CGrdStmts*/;

/*! General form of a `CASE` statement:
**  ```
**    CASE <selector expression> OF
**      <case label>, ... : <statement>
**      ...
**      OTHERWISE : <statement>
**    END_CASE;
** ```
*/
struct CaseStmt : public Stmt {
    ExprPtr test;
    Fifo<CGrdStmts> caseparts;
    StmtPtr otherwise;

    CaseStmt();
    CaseStmt (ExprPtr t, Fifo<CGrdStmts> * &cases, StmtPtr others);
    CaseStmt (ExprPtr t, Fifo<CGrdStmts> * &cases);
    CaseStmt (CaseStmt &x) = delete;
    CaseStmt (CaseStmt &&x);
    CaseStmt & operator= (const CaseStmt &x) = delete;
    CaseStmt & operator= (CaseStmt &&x);
    ~CaseStmt();
    Type type() const;
    const std::string type_name() const { return "CaseStmt"; }
} /*CaseStmt*/;

/*! A coumpound statement is a (non-empty) statement list, enclosed in `BEGIN`
**  and `END`, and terminated with a `;`.
*/
struct CompoundStmt : public Stmt {
    Fifo<StmtPtr> stmtlist;
    CompoundStmt();
    CompoundStmt (Fifo<StmtPtr> * &s);
    CompoundStmt (CompoundStmt &x) = delete;
    CompoundStmt (CompoundStmt &&x);
    CompoundStmt & operator= (const CompoundStmt &x) = delete;
    CompoundStmt & operator= (CompoundStmt &&x);
    ~CompoundStmt();
    Type type() const;
    const std::string type_name() const { return "CompoundStmt"; }
    CompoundStmt & addStmt (StmtPtr stmt);
} /*CompoundStmt*/;

/* The `SKIP` statement is something like a "no operation" statement. It is
** used where a statement is required, but any action makes no further sense.
** General format:
** ```
**   SKIP ;
** ```
*/
struct SkipStmt : public Stmt {
    SkipStmt();
    ~SkipStmt();
    Type type() const;
    const std::string type_name() const { return "SkipStmt"; }
} /*SkipStmt*/;

#endif /*STMT_H*/
