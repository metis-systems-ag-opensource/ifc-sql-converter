/*! \file inteval.h
**  Evaluating the bounds of ARRAY variables/attributes to integer values
**  (if possible).
**
**  The bounds in ARRAY declarations are not always constant, which made it
**  impossible to evaluate them in the parsing phase. Instead, in the second
**  pass, these bounds are evaluated (if they are literal values).
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'inteval.cc'
** (Evaluate (INTEGER) constant expressions)
**
** This function returns an 'std::optional<long>', because some expressions
** can't be const-evaluated to integer values. In this case, 'std::nullopt'
** is returned. The type embedded into the return type is 'long', because
** with this type, 'intEval()' is usable in a more flexible way.
** Remember: This function is _not_ a complete expression evaluator. It is
** used only during the parsing process, because in a limited set of cases
** integer values are required.
**
*/
#ifndef INTEVAL_H
#define INTEVAL_H

#include <optional>

#include "expr.h"

/*! This function evaluates an integer expression to an integer (`long`) value.
**  It returns an optional value, which consists of a `long` value if the
**  evaluation succeeded, and no value if the expression was no integer
**  literal.
*/
std::optional<long> intEval (Expr *x);

#endif /*INTEVAL_H*/
