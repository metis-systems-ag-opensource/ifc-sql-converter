/*! \file logical.h
**  — NO LONGER USED —
**
** This module was an experiment in the implementation of the EXPRESS type
** LOGICAL. It can be safely removed.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Small implementation of the type 'Logical' (including operations)
**
*/
#ifndef LOGICAL_H
#define LOGICAL_H

// A small support type (to be used in the parser and for the LogicalLiteral-
// struct ...
class Logical {
public:
    enum Value : char { false_, unknown, true_ };
    friend bool operator< (const Logical &l, const Logical &r);
    friend bool operator== (const Logical &l, const Logical &r);
    friend Logical operator& (const Logical &l, const Logical &r);
    friend Logical operator| (const Logical &l, const Logical &r);
    friend Logical operator^ (const Logical &l, const Logical &r);
    friend Logical operator~ (const Logical &r);
protected:
    Value v;
public:
    Logical () = default;
    Logical (bool x);
    Logical (Value x);
    Logical (const Logical &x);
    Logical & operator= (const Logical &x);
    bool to_bool ();
    operator bool();
} /*Logical*/;

#endif /*LOGICAL_H*/
