/* depgraph.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Implementation part of the 'depgraph' module.
**
*/

#include <iostream>

#include <algorithm>
#include <utility>
#include <stdexcept>

#include "identbitset.h"

#include "depgraph.h"

using namespace std;

//struct DepGraphEl {
//    std::set<Ident> deps;
//    std::set<Ident> rev_deps;
DepGraphEl::DepGraphEl() : deps(), rev_deps() { }
//    DepGraphEl (const DepGrapEL &) = default;
DepGraphEl::DepGraphEl (DepGraphEl &&x)
    : deps(move(x.deps)), rev_deps(move(x.rev_deps))
{ }

bool DepGraphEl::depends (Ident id) const { return deps.count (id) > 0; }
//} /*DepGraphEl*/;

//struct DepGraph {
//    DepGraph() = default
DepGraph::DepGraph (const DepGraph &x) : dg(x.dg) { }
DepGraph::DepGraph (DepGraph &&x) : dg(move(x.dg)) { }

DepGraph &DepGraph::operator= (const DepGraph &x) { dg = x.dg; return *this; }
DepGraph &DepGraph::operator= (DepGraph &&x) { dg = move(x.dg); return *this; }

DepGraphEl &DepGraph::operator[] (Ident id) { return dg.at (id); }
const DepGraphEl &DepGraph::operator[] (Ident id) const { return dg.at (id); }

DepGraphEl &DepGraph::at (Ident id) { return dg.at (id); }
const DepGraphEl &DepGraph::at (Ident id) const { return dg.at (id); }

bool DepGraph::has (Ident id) const { return dg.count (id) > 0; }
bool DepGraph::depends (Ident id, Ident from) const {
//    if (! has (id)) {
//	throw runtime_error
//	    (string (id) + ": Dependency graph contains no such element");
//    }
    if (! has (from)) {
	throw runtime_error
	    (to_string (id) + ": Can't depend on an unknown element " +
	     to_string (from));
    }
    return dg.at (id).depends (from);
}

bool DepGraph::insert (Ident id) {
    if (has (id)) { return false; }
    dg.insert (make_pair (id, DepGraphEl()));
    return true;
}

bool DepGraph::remove (Ident id) {
    if (! has (id)) { return false; }
    for (Ident depid: dg[id].rev_deps) {
	if (id == depid) { continue; }
	dg[depid].deps.erase (id);
    }
    dg.erase (id);
    return true;
}

bool DepGraph::addDependency (Ident id, Ident from) {
    if (! has (id) || ! has (from)) { return false; }
    set<Ident> &deps = dg[id].deps;
    set<Ident> &rev_deps = dg[from].rev_deps;
    deps.insert (from);
    rev_deps.insert (id);
    return true;
}

const set<Ident> &DepGraph::dependencies (Ident id) const {
    if (! has (id)) {
	throw runtime_error
	    (to_string (id) + ": No such element in the dependency graph");
    }
    return dg.at(id).deps;
}

size_t DepGraph::size() const { return dg.size(); }

DepGraph::DgIterator DepGraph::begin() { return dg.begin(); }
DepGraph::DgIterator DepGraph::end() { return dg.end(); }

DepGraph::DgConstIterator DepGraph::begin() const { return dg.begin(); }
DepGraph::DgConstIterator DepGraph::end() const { return dg.end(); }

//private:
//    std::map<Ident, DepGraphEl> dg;
//} /*DepGraph*/;


// Helper functions which make the expression 'X.count(Y) > 0' a bit more
// clear.

static bool has (const set<Ident> &deps, Ident el)
{
    return deps.count (el) > 0;
}

// Returns 'true' if the given 'TypeStruct *' points to a simple type (BOOLEAN,
// LOGICAL, INTEGER, REAL, NUMBER, BINARY or STRING).
static bool is_simple_type (TypeStruct *ts)
{
    return dynamic_cast<TsSimple *>(ts) != nullptr ||
	   dynamic_cast<TsStringLike *>(ts) != nullptr;
}

// Returns 'true' if the given 'TypeStruct *' points to a simple type (BOOLEAN,
// LOGICAL, INTEGER, REAL, NUMBER, BINARY or STRING) or an ENUMERATION type.
static bool is_simple_or_enum (TypeStruct *ts)
{
    return is_simple_type (ts) || ts->type() == TypeStruct::enumeration;
}

using IdType = pair<Ident, TypeStruct *>;

// De-construct an aggregate, returning its 'basetype' (aka
// '(Ident, TypeStruct *)' pair).
static IdType decompose_aggregate (Symtab &st, TypeStruct *ts)
{
    switch (ts->type()) {
	case TypeStruct::array: {
	    auto ats = dynamic_cast<TsArray2 *>(ts);
	    TypeStruct *bts = ats->basetype;
	    return decompose_aggregate (st, bts);
	}
	case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
	    auto ats = dynamic_cast<TsAggregateLike *>(ts);
	    TypeStruct *bts = ats->basetype;
	    return decompose_aggregate (st, bts);
	}
	case TypeStruct::aggregate: {
	    auto ats = dynamic_cast<TsAggregate *>(ts);
	    TypeStruct *bts = ats->basetype;
	    return decompose_aggregate (st, bts);
	}
	case TypeStruct::typeref: {
	    auto trts = dynamic_cast<TsTypeRef *>(ts);
	    Ident id = trts->id;
	    TypeStruct *rts = st[id];
	    return make_pair (id, rts);
	}
	default: {
	    // Don't know what to do here ... maybe, the type model needs some
	    // refactoring – and, consequently, the parser
	    //if (is_simple_type (ts)) { /* ... * / }
	    return make_pair (0, ts);
	}
    }
}

static IdType decompose_attr (Symtab &st, Attr *attr)
{
    switch (attr->type()) {
	case Attr::explicit_attr: {
	    auto eattr = dynamic_cast<ExplicitAttr *>(attr);
	    return decompose_aggregate (st, eattr->definition.get());
	}
	case Attr::derived_attr: {
	    auto dattr = dynamic_cast<DerivedAttr *>(attr);
	    return decompose_aggregate (st, dattr->definition.get());
	}
	default: {
	    IdType nothing (0, nullptr);
	    return nothing;
	}
    }
}

static DepGraph gen_basegraph (Symtab &st)
{
    DepGraph res;
    // First step: Insert all non-trivial 
    for (Ident id = st.first(); id <= st.last(); ++id) {
	auto ts = dynamic_cast<TsType *>(st[id]);
	if (ts) {
	    res.insert (id);
	}
    }
    for (auto &it: res) {
	Ident id = it.first;
	TypeStruct *ts = st[id];
	set<Ident> typeids;
	switch (ts->type()) {
	    case TypeStruct::defined: {
		auto dts = dynamic_cast<TsDefined *>(ts);
		typeids.insert (dts->basetype);
		break;
	    }
	    case TypeStruct::select: {
		auto sts = dynamic_cast<TsSelect *>(ts);
		for (Ident btid: sts->types) {
		    typeids.insert (btid);
		}
		break;
	    }
	    case TypeStruct::array: case TypeStruct::list:
	    case TypeStruct::bag: case TypeStruct::set:
	    case TypeStruct::aggregate: {
		Ident btid;
		TypeStruct *bt;
		tie (btid, bt) = decompose_aggregate (st, ts);
		if (btid > 0 && ! is_simple_or_enum (bt)) {
		    typeids.insert (btid);
		}
		break;
	    }
	    case TypeStruct::entity: {
		auto ets = dynamic_cast<TsEntity *>(ts);
		for (Ident btid: ets->mysupertypes) {
		    typeids.insert (btid);
		}
		break;
	    }
	    default: {
		// Don't do anything here (ignore the remaining entries ...)
		break;
	    }
	}
	for (Ident btid: typeids) {
	    if (! res.has (btid)) {
		throw runtime_error
		    ("INTERNAL ERROR: Unmatched base type (" +
		     st.symname (btid) + ") for defined type '" +
		     st.symname (id) + "'");
	    }
	    res.addDependency (id, btid);
	}
    }
    return res;
}

static bool has_circulardep (DepGraph &dg, Ident id,
			     set<Ident> &visited, set<Ident> &checked)
{
    if (has (visited, id)) { return true; }
    bool is_circular = false;
    visited.insert (id);
    for (auto depid: dg.dependencies (id)) {
	if (has_circulardep (dg, depid, visited, checked)) {
	    is_circular = true; break;
	}
    }
    visited.erase (id);
    checked.insert (id);
    return is_circular;
}

static
bool has_circulardeps (DepGraph &dg)
{
    // The "checked" set allows for skipping the elements which were already
    // checked for circular dependencies during the recursive invocation of
    // 'has_circulardep()' ...
    set<Ident> checked;
    for (auto &it: dg) {
	// Here, only the 'id' part of the map is required.
	Ident id = it.first;
	// Skip the already checked elements
	if (has (checked, id)) { continue; }
	// The "visited" set is used for detecting circular dependencies.
	// It is used as a temporary value and should have on exit (of
	// 'has_circulardep()') the same value as on entry (of this function).
	set<Ident> visited;
	if (has_circulardep (dg, id, visited, checked)) { return true; }
    }
    return false;
}


// Insert the dependencies generated through the types of the attributes of
// ENTITY types. Because the 'depgraph' already contans all type-alike symbols
// from symbol table, a walk through it is already enough. But because only
// ENTITY types have attributes, only these are examined. I splitted this part
// of the dependency analysis, because an ENTITY may very well have recursive
// dependencies through its attributes, as the attributes refererring to other
// entity types are even that: references to instances of the corresponding
// entity types.
static void add_entityattrdep (Symtab &st, DepGraph &depgraph)
{
    for (auto &it: depgraph) {
	Ident id = it.first;
	auto ets = dynamic_cast<TsEntity *>(st[id]);
	if (ets) {
	    // Other types than entities make no sense, because only entities
	    // have attributes ...
	    for (auto &it: ets->attributes) {
		Ident btid; TypeStruct *bts;
		tie (btid, bts) = decompose_attr (st, it.second);
		if (btid > 0 && ! is_simple_or_enum (bts)) {
		    // Insert the dependency on the attribute type and add the
		    // reverse dependency to the attribute type's dependency
		    // list
		    depgraph.addDependency (id, btid);
		}
	    }
	}
    }
}

// Generate a dependency graph on the symbols which refer to types in the
// EXPRESS schema.
DepGraph gen_depgraph (Symtab &st)
{
    DepGraph res = gen_basegraph (st);
    if (has_circulardeps (res)) {
	throw runtime_error
	    ("Circular dependencies detected in the type system.");
    }
    add_entityattrdep (st, res);
    return res;
}

static
size_t count_unresolved (const DepGraphEl &el, IdentBitSet &resolved)
{
    size_t res = 0;
    for (Ident id: el.deps) {
	if (! resolved.has (id)) { ++res; }
    }
    return res;
}

// Order the symbols collected in the dependency graph by the number of
// dependencies they have (least to most dependencies)
IdentList resolve_deps (const DepGraph &depgraph)
{
    IdentList res;
    IdentBitSet resolved;
    // Declare and initialize the element counter and the counter save value
    // from the previous round.
    size_t tc = depgraph.size(), prev_tc = 0;
    // While the element counter is not 0, we have elements which have elements
    // which were not "resolved" yet, meaning, their remaining dependencies,
    // after "virtually" removing the already extracted elements were not
    // empty.
    while (tc > 0) {
	// If the last round didn't reduce the number of "unresolved" elements
	// (elements not in the 'resolved' bitset) in the dependency graph, we
	// have some "circular" dependencies. With the exception of type
	// hierarchies, this is no problem, as these are the elements which
	// require forward declarations for solving these circular
	// dependencies.
	if (tc == prev_tc) { break; }
	prev_tc = tc;
	// First step: collect all elements which no longer have any dependency
	// from other elements ...
	IdentList tmp;
	for (auto &it: depgraph) {
	    Ident id = it.first;
	    // DON'T examine the already examined elements
	    if (resolved.has (id)) { continue; }
	    // Get the dependency lists
	    const DepGraphEl &el = it.second;
	    if (count_unresolved (el, resolved) == 0) {
		tmp.push (id);
	    }
	}
	// Second step: Insert the collected elements into "resolved" bitset
	resolved.fill (tmp);
	// Third step: Decrease the element counter.
	tc -= tmp.size();
	if (tc > prev_tc) {
	    throw runtime_error ("Counting failed: tc = " + to_string(tc) +
				 ", prev_tc = " + to_string (prev_tc));
	}
	// Fourth step: Append the collected elements (Identifier) to the
	// output list 'res'.
	res.push (move (tmp));
    }
    if (tc > 0) {
	// Mark the end of the normal dependencies with the "null" identifier.
	res.push (0);
	// Then append all remaining elements of the dependency graph (the
	// identifiers of the types with probably circular dependencies) to
	// the output list 'res'.
	for (auto it: depgraph) {
	    Ident id = it.first;
	    if (! resolved.has (id)) { res.push (id); }
	}
    }
    return res;
}
