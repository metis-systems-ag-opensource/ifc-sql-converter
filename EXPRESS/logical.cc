/* logical.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Implementation of 'logical.h'
** (Small implementation of the type 'Logical' (including operations))
**
*/

#include <stdexcept>
#include "logical.h"

using namespace std;

//class Logical {
//public:
//    enum Value { false_, unknown, true_ };
//protected:
//    Value v;
//public:
Logical::Logical (bool x) : v(x ? true_ : false_) { }
Logical::Logical (Value x) : v(x) { }
Logical::Logical (const Logical &x) : v(x.v) { }
Logical & Logical::operator= (const Logical &x) {
    v = x.v; return *this;
}
bool Logical::to_bool () {
    if (v == Logical::unknown) {
	throw runtime_error ("Can't convert Logical::unknown to bool");
    }
    return v == Logical::true_;
}

Logical::operator bool() { return to_bool(); }

bool operator< (const Logical &l, const Logical &r)
{
    switch (l.v) {
	case Logical::false_:  return r.v != l.v;
	case Logical::unknown: return r.v == Logical::true_;
	default:               return false;
    }
}

bool operator== (const Logical &l, const Logical &r)
{
    return l.v == r.v;
}

Logical operator& (const Logical &l, const Logical &r)
{
    switch (l.v) {
	case Logical::false_:
	case Logical::unknown:
	    return l;
	default:
	    return r;
    }
}

Logical operator| (const Logical &l, const Logical &r)
{
    switch (l.v) {
	case Logical::false_: return r;
	case Logical::unknown: return l;
	default:
	    return (r.v == Logical::unknown ? r : l);
    }
}

Logical operator^ (const Logical &l, const Logical &r)
{
    switch (l.v) {
	case Logical::unknown: return l;
	case Logical::false_: return r;
	default: {
	    switch (r.v) {
		case Logical::false_: return l;
		case Logical::unknown: return r;
		default: return Logical (Logical::false_);
	    }
	}
    }
}

Logical operator~ (const Logical &l)
{
    switch (l.v) {
	case Logical::unknown: return l;
	case Logical::false_:  return Logical (Logical::true_);
	default:               return Logical (Logical::false_);
    }
}

//} /*Logical*/;
