/* util.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Utility functions 'enter()' (creating a new declaration context in the
** symbol table and putting all local definitions of a 'TsAlgorithm *' into
** this context) and 'leave()' (removing the current declaration context).
**
*/

#include "util.h"

void enter (Symtab &st, Ident id, TsAlgorithm *ts)
{
    IdentKeyMap<TypeStruct *> &localdefs = ts->localdefs;
    // Define a new declaration context in the symbol table for the given
    // algorithm <id, ts>
    st.new_context (id, ts);

    // Put all local symbols of the algorithm into the symbol table (within
    // the new declaration context).
    for (auto &localdef: localdefs) {
	Ident localid = localdef.first;
	TypeStruct *localts = localdef.second;
	(void) st.push_value (localid, localts);
    }
}

void leave (Symtab &st)
{
    // Remove the current declaration context but _don't_ destroy the
    // definitions inserted.
    st.end_context (false);
}
