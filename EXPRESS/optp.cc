/* optp.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Option parser for the command line arguments, based on 'getopt()'
**
*/

#include <cstring>
#include <unistd.h>
#include <utility>
#include <stdexcept>

#include "optp.h"

using namespace std;

static bool has (OptMap m, char opt)
{
    return m.count (opt) > 0;
}

OptMap getopts (int argc, char *argv[], const char *opts, int &optx)
{
    int opt;
    OptMap res;
    char *opts1 = new char[strlen (opts) + 2], *p;
    p = opts1;
    if (*opts == '+' || *opts == '-') { *p++ = *opts++; }
    if (*opts == ':') { *p++ = *opts++; } else { *p++ = ':'; }
    strcpy (p, opts);
    optind = optx; opterr = 0;
    while ((opt = getopt (argc, argv, opts1)) != -1) {
	switch (opt) {
	    case '?': {
		delete[] opts1;
		string os (1, optopt);
		throw runtime_error ("Invalid option '-" + os + "'");
	    }
	    case ':': {
		delete[] opts1;
		string os (1, optopt);
		throw runtime_error
		    ("Missing argument for option '-" + os + "'");
	    }
	    default: {
		if (has (res, opt)) {
		    delete[] opts1;
		    string os (1, opt);
		    throw runtime_error ("Ambiguous option '-" + os + "'");
		}
		string arg (optarg ? optarg : "");
		res.insert (make_pair (opt, arg));
		break;
	    }
	}
    }
    delete[] opts1;
    optx = optind;
    return res;
}
