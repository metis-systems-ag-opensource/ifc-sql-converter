/*! \file filepos.h
**  Datatype for storing references to file positions of syntactical elements
**  (like, e.g. statements, expressions etc.).
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** My own definition of the "file position" of a parsed object. I'm using this
** definition, because it makes 'types.h', 'stmt.h' and 'expr.h' independent
** from the generated 'position.hh' ...
**
*/
#ifndef FILEPOS_H
#define FILEPOS_H

#include <iostream>
#include <string>

const std::string UNSET_FILE = "#UNSET#";

/*! The file position of a syntactical element conststs of
**    - the file containing the element,
**    - the line and column where it begins within the file, and
**    - the line and column where it ends.
** Such an element is set by the parser, and read by either the parser, or
** (in later passes) by the context analysis.
*/
struct FilePos {
    // Default constructor ...
    FilePos ()
	: filename(&UNSET_FILE), line1(0), column1(0), line2(0), column2(0)
    { }
    FilePos (const FilePos &x)
	: filename(x.filename), line1(x.line1), column1(x.column1),
	  line2(x.line2), column2(x.column2)
    { }
    ~FilePos() {
	filename = (decltype(filename)) 0xA3A3A34B3C3C3C19;
	line1 = 0x40414243; column1 = 0x30313233;
	line2 = 0x44454647; column2 = 0x34353637;
    }
/**/
    const std::string *filename;
    unsigned int line1, column1;
    unsigned int line2, column2;
} /*FilePos*/;

/*! Converting a `FilePos` object into a string, to be used in (e.g.) exception
**  messages.
*/
std::string to_string (const FilePos &pos);

/*! A `std::ostream` output operator for `FilePos` objects. */
std::ostream & operator<< (std::ostream &out, const FilePos &pos);

#endif /*FILEPOS_H*/
