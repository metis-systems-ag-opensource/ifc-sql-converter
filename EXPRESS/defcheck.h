/*! \file defcheck.[ch]
**
**  \brief Validator which checks iff all global entries in the symbol table
**         have a definition.
**
**  The reason for such a validation is that the order in which declarations
**  can occur is *unspecified*, meaning that a declaration of a type
**  (especially ENTITY types) may refer to another declaration which occurs
**  later in the program text.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface for 'defcheck.cc'
** (Fast validator with the sole purpose of testing if all data-structures used
**  in the EXPRESS-file were defined somewhere. This exported function returns
**  a boolean flag indicating success (true) or failure (false) and writes the
**  required error messages directly to 'std::cerr'.)
**
*/
#ifndef DEFCHECK_H
#define DEFCHECK_H

#include "symtab.h"

/*! Tests all entries in the symbol table for definedness, and issues an error
**  message for each entry found which lacks a definition.
**
**  @param st - the symbol table to be validated.
**
**  @returns `false` if any entry in the symbol table has a proper definition;
**           if all entries have a proper definition, `true` is returned.
*/
bool defcheck (Symtab &st);

#endif /*DEFCHECK_H*/
