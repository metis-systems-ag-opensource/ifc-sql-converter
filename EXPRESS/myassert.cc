/* myassert.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** My own version of an 'assertion' function
**
*/
#include <stdexcept>

#include "myassert.h"

using namespace std;

void _myassert (const char *file, int line, bool bf, const std::string &msg)
{
    if (! bf) {
	throw runtime_error
	    (string(file) + "(" + to_string (line) + "): " + msg);
    }
}
