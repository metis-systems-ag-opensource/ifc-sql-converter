/*! \file consteval.[ch]
**
**  \brief A very simple evaluator for constant expressions.
**
**  Sole purpose of this evaluator is the calculation of the bound values
**  of aggregate (LIST, BAG, SET), STRING and BINARY types in declarations.
**  The aggregate type ARRAY is excluded, because this type may have non-
**  constant bound values, so the corresponding checkn are done later in
**  the compilation process.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Interface file for 'consteval.cc'
** (Evaluator for constant expressions)
**
*/
#ifndef CONSTEVAL_H
#define CONSTEVAL_H

#include "expr.h"

using namespace std;

/*! Returns true if 'x' is a literal of some kind and false otherwise ...
*/
bool is_constant (Expr *x);

/*! Returns true if 'x' is a numeric literal and false otherwise ...
*/
bool numeric_literal (Expr *x);

/*! Returns the Integer value of a numeric literal (if possible)
*/
Integer int_value (Expr *x);

/*! Evaluates an expression being assumed to be a constant (literal)
**  expression.
**  WARNING! Eval modifies the original expression, partially or completely
**  replacing it with the new (constant-folded) (sub-)expressions.
*/
void consteval (Expr * &x);

#endif /*CONSTEVAL_H*/
