/*! \file opmode.h
**  Definition of a datatype (and operations) for the operation modes of the
**  EXPRESS compiler `xpp`.
**
**  The `xpp` program has more than one job to perform. Because of this,
**  it has some operation modes, which are described by this datatype.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'opmode.cc'
** (Convert a string ('const char *' or 'const std::string &') into an element
**  of the 'OpMode' enum.)
**
*/
#ifndef OPMODE_H
#define OPMODE_H

#include <string>

namespace EXPRESS {

    /*! The operation mode:
    **    - `none` – default (invalid) operation mode,
    **    - `db` – for generating a database schema,
    **    - `cpp` – for generating the C++ (backend) code of the IFC
    **              "interpreter"
    **    - `cstar` – For generating a list of ENTITY-names and positions of
    **                DERIVE attributes which occur directly in instances.
    **
    **  The other operation modes are/were used for testing purposes during
    **  the development of `xpp`. They remain here, because the development
    **  of `xpp` is not complete yet.
    */
    enum class OpMode : uint8_t { none, db, cpp, };

    /*! Converts an operation mode tag into a string. This function is
    **  primarily used in error messages.
    */
    std::string to_string (OpMode op);

    /*! Returns a list of all operation modes (comma-delimited) as a string.
    **  This function simplifies the generation of a 'usage' message.
    */
    const std::string &valid_ops ();

    /*! Converts a string into an operation mode tag. If the string does not
    **  contain a textual representation of the operation mode, `OpMode::none`
    **  is returned.
    */
    OpMode opmode (const std::string &op);

} /*namespace EXPRESS*/

#endif /*OPMODE_H*/
