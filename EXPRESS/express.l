/* express.l
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Lexer for my (reduced) EXPRESS-parser
**
*/
%{
#include <iostream>
#include <string>

#include "types.h"

#include "scanner.h"

using TokenVal = EXPRESS::Parser::semantic_type;
using Location = EXPRESS::Parser::location_type;
using Token = EXPRESS::Parser::token;
using TokenType = EXPRESS::Parser::token_type;

#undef YY_DECL
#define YY_DECL int EXPRESS::Scanner::yylex(TokenVal *const lval, Location *location)

#define YY_NO_UNISTD_H

#define YY_USER_ACTION do { location->columns(yyleng); } while (0);
#define NEXTLINE do { location->lines(); location->step(); } while (0)

/*int sym_lookup (const char *tktext, int tklength, int *_symidx);*/

/* Because of the unspecified order in which declarations can be given in an
** EXPRESS-specification, a two-pass parser is mandatory. Because of this, the
** parser needs to generate a parse-tree and handle a symbol table. On the
** other hand, this allows for conversion to be made as late as possible
** (probably never ;) ). For this reason, i'm letting the lexer (scanner)
** return the unmodified token value (as a dynamically generated string). The
** parser (or even the tree-runner - invoked in the second pass) then can do
** the conversion and then report conversion errors (such as overflows on
** numerical values, invalid UNICODE-sequences in strings and so on).
*/

%}

/* EXPRESS is CASE-independent ASCII (with the exception of strings, which
** are UTF-8 encoded – as far as i know) ...
*/
%option caseless

%option debug
%option nodefault
%option yyclass="EXPRESS::Scanner"
%option noyywrap
%option c++

FLOATLIT    [0-9]+\.[0-9]*(E[+-][0-9]+)?
INTLIT	    [0-9]+
BINLIT	    \%[01]+
IDENT	    [A-Z][0-9A-Z_]*
STRINGLIT   \'([^']|\'\')*\'
XSTRINGLIT  \"([0-9A-Fa-f]{8})*\"
WS	    [\a\b\f\r\t\v ]

/* The "comment" state is used when a comment is to be skipped, i.e. when a
** comment opening brace is reached in the inpur stream ...
*/
%x COMMENT
%%

%{
    /* This level-counter is inserted before any rule, which means that
    ** flex puts it into the generated scanner before the scanning loop
    ** This is exact the right position for such a variable in a reentrant
    ** scanner. Beware of enclosing this declaration into curly braces, as
    ** it would make 'comment_level' local to this so inserted block; this
    ** would contradict the desired effect ...
    */
    int comment_level = 0;
    yylval = lval;
    location->step();
%}

"(*"		    {
    /* Nested comments.
    ** Like the programming language Pascal, 'EXPRESS' allows for nested
    ** comments. These must be handled by keeping book about the number of
    ** opening and closing comment-braces. Additionally, the lexer must
    ** change into another state for handling the content of a multi-line
    ** multi-level comment ...
    */
		      BEGIN(COMMENT); comment_level = 1;
}
<COMMENT>{
    /* Multiline comments can be nested, like in (later versions) of Pascal,
    ** Module-2 and Oberon/Oberon-2 ...
    */
"(*"	    %{ ++comment_level; %}
"*)"	    %{ if (--comment_level <= 0) { BEGIN(INITIAL); } %}
    /* Skipping the EOL character(s) ... */
    /* Fast skipping the text within a comment ... */
[^(*)\n]+   ; // Eat up comment data
"("	    ;
"*"	    ;
")"	    ;
\n	    %{ NEXTLINE; %}
<<EOF>>    { return Token::PREMEOF; }
}
    /* Skipping single-line comments ... */
"--".*\n	    { NEXTLINE; }

    /* EXPECT keywords ... */
ABSTRACT	return Token::ABSTRACT;
ARRAY		return Token::ARRAY;
BAG		return Token::BAG;
BASED_ON	return Token::BASED_ON;
BEGIN		return Token::CBEGIN;
CASE		return Token::CASE;
CONSTANT	return Token::CONSTANT;
DERIVE		return Token::DERIVE;
ELSE		return Token::ELSE;
END		return Token::CEND;
END_CASE	return Token::END_CASE;
END_CONSTANT	return Token::END_CONSTANT;
END_ENTITY	return Token::END_ENTITY;
END_FUNCTION	return Token::END_FUNCTION;
END_IF		return Token::END_IF;
END_LOCAL	return Token::END_LOCAL;
END_PROCEDURE	return Token::END_PROCEDURE;
END_REPEAT	return Token::END_REPEAT;
END_RULE	return Token::END_RULE;
END_SCHEMA	return Token::END_SCHEMA;
END_SUBTYPE_CONSTRAINT	return Token::END_SUBTYPE_CONSTRAINT;
END_TYPE	return Token::END_TYPE;
ENTITY		return Token::ENTITY;
ENUMERATION	return Token::ENUMERATION;
ESCAPE		return Token::ESCAPE;
EXTENSIBLE	return Token::EXTENSIBLE;
FIXED		return Token::FIXED;
FOR		return Token::FOR;
FUNCTION	return Token::FUNCTION;
GENERIC		return Token::GENERIC;
GENERIC_ENTITY	return Token::GENERIC_ENTITY;
IF		return Token::IF;
INVERSE		return Token::INVERSE;
LIST		return Token::LIST;
LOCAL		return Token::LOCAL;
OF		return Token::OF;
ONEOF		return Token::ONEOF;
OPTIONAL	return Token::OPTIONAL;
OTHERWISE	return Token::OTHERWISE;
PROCEDURE	return Token::PROCEDURE;
QUERY		return Token::QUERY;
RENAMED		return Token::RENAMED;
REPEAT		return Token::REPEAT;
RETURN		return Token::RETURN;
RULE		return Token::RULE;
SCHEMA		return Token::SCHEMA;
SELECT		return Token::SELECT;
SELF		return Token::SELF;
SET		return Token::SET;
SKIP		return Token::SKIP;
SUBTYPE		return Token::SUBTYPE;
SUBTYPE_CONSTRAINT  return Token::SUBTYPE_CONSTRAINT;
SUPERTYPE	return Token::SUPERTYPE;
THEN		return Token::THEN;
TO		return Token::TO;
TOTAL_OVER	return Token::TOTAL_OVER;
TYPE		return Token::TYPE;
UNIQUE		return Token::UNIQUE;
UNTIL		return Token::UNTIL;
VAR		return Token::VAR;
WHERE		return Token::WHERE;
WHILE		return Token::WHILE;
WITH		return Token::WITH;

    /* EXPECT Standard types ... */
BINARY		%{
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::BINARY;
%}
BOOLEAN		%{
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::BOOLEAN;
%}
LOGICAL		%{
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::LOGICAL;
%}
INTEGER		%{
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::INTEGER;
%}
REAL		%{
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::REAL;
%}
NUMBER		%{
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::NUMBER;
%}
STRING		%{
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::STRING;
%}

    /* EXPECT Standard functions ... */
ABS		return Token::ABS;
ACOS		return Token::ACOS;
ASIN		return Token::ASIN;
ATAN		return Token::ATAN;
BLENGTH		return Token::BLENGTH;
COS		return Token::COS;
EXISTS		return Token::EXISTS;
EXP		return Token::EXP;
FORMAT		return Token::FORMAT;
HIBOUND		return Token::HIBOUND;
HIINDEX		return Token::HIINDEX;
LENGTH		return Token::LENGTH;
LOBOUND		return Token::LOBOUND;
LOINDEX		return Token::LOINDEX;
LOG		return Token::LOG;
LOG2		return Token::LOG2;
LOG10		return Token::LOG10;
NVL		return Token::NVL;
ODD		return Token::ODD;
ROLESOF		return Token::ROLESOF;
SIN		return Token::SIN;
SIZEOF		return Token::SIZEOF;
SQRT		return Token::SQRT;
TAN		return Token::TAN;
TYPEOF		return Token::TYPEOF;
USEDIN		return Token::USEDIN;
VALUE		return Token::VALUE;
VALUE_IN	return Token::VALUE_IN;
VALUE_UNIQUE	return Token::VALUE_UNIQUE;

    /* EXPECT keyword literals */
FALSE		return Token::FALSE;
TRUE		return Token::TRUE;
UNKNOWN		return Token::UNKNOWN;

    /* EXPECT keyword operators */
IN		return Token::IN;
LIKE		return Token::LIKE;
OR		return Token::OR;
XOR		return Token::XOR;
DIV		return Token::DIV;
MOD		return Token::MOD;
AND		return Token::AND;
ANDOR		return Token::ANDOR;
NOT		return Token::NOT;

    /* EXPECT identifier. In order to avoid reduce/reduce conflicts, the
    ** known types in expect have their own token: TYPE_IDENT ...
    */
{IDENT}		%{
    /* Token::IDENT is a token with a value, the name of the
    ** identifier detected ...
    */
    yylval->tokenstring = new std::string (yytext, yyleng);
    return Token::IDENT;
%}

{BINLIT}	%{
    /* Token::BIN_LITERAL is a token with a value ... */
    yylval->tokenstring = new std::string (yytext + 1, yyleng - 1); 
    return Token::BIN_LITERAL;
%}

{FLOATLIT}	%{
    /* Token::FLOAT_LITERAL is a token with a value ... */
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::FLOAT_LITERAL;
%}

{INTLIT}	%{
    /* Token::INT_LITERAL is a token with a value ... */
    yylval->tokenstring = new std::string (yytext, yyleng); 
    return Token::INT_LITERAL;
%}

{STRINGLIT}	%{
    /* Token::STRG_LITERAL is a token with a value ... */
    yylval->tokenstring = new std::string (yytext + 1, yyleng - 2); 
    return Token::STRG_LITERAL;
%}

{XSTRINGLIT}	%{
    yylval->tokenstring = new std::string (yytext + 1, yyleng - 2);
    return Token::XSTRG_LITERAL;
%}

    /* The remaining tokens; first, the Multi-character symbol tokens ... */
":="		return Token::BECOMES;
"<*"		return Token::LTSTAR;
"<>"		return Token::NEQ;
"<="		return Token::LEQ;
">="		return Token::GEQ;
":<>:"		return Token::CNEQ;
":=:"		return Token::CEQ;
"||"		return Token::ICONC;
[<=>+*/,;.:?(){}-]  %{
    /* The one-character tokens (with the exceptions of the
    ** symbols which must be handled specially) are returned
    ** here ...
    */
    return static_cast<TokenType>(*yytext);
%}
"["|"]"|\\|"|"	    %{
    /* The square brackets, as well as the '\' and '|' are special characters
    ** which are used as meta-symbols in regular expressions. Because of this,
    ** they need to be quoted for allowing the scanner to recognize them as
    ** regular characters. This is the sole reason for this rule ...
    */
    return static_cast<TokenType>(*yytext);
%}
{WS}+		; // Ignore sequences of HT- and BL-characters
\n		NEXTLINE;
.		%{
    /* Let the parser do the error reporting on invalid characters in the input
    ** file ...
    */
    return static_cast<TokenType> (*yytext);
%}

%%

/* No extra functions required here ... the rest must be done by the parser. */
