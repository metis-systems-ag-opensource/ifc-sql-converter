/*! \file db.h
**  Code generator module for the database schema.
**
**  The database schema is defined as an SQL script (currently for
**  MySQL/MariaDB only). The generated SQL script is sufficient for
**  a complete generation of an IFC schema database.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'CodeGen/db.cc'
** (Create a database schema from the symbol table ...
**
**  The difference to languages like C++ and C# is the fact that
**   a) arrays, lists, bags and sets are the same in a database. They must be
**      represented in some form, but don't have any differences otherwise.
**   b) all values are inherently optional.
**
**  The model should consist of database tables, each of them representing an
**  ENTITY, and consisting of _all_ attributes the corresponding object has
**  (including the attributes of its supertypes). 'ABSTRACT SUPERTYPE' entities
**  should not be represented, as they never contribute directly to the data
**  representing an IFC model.
**  Attributes of entities must be represented as a key into a table into the
**  "target" entity. If these attributes consist of arrays or lists, they must
**  point inst a table whose attributes consist of key-tuples, of which one
**  element is the original key, another one must point into the "target"
**  entity and the other ones are self-referencing into the intermediate
**  database. Simple datatypes should be reduced to their base-types and stored
**  in value-tables (one for each simple datatype).
**  There should be a base-table, containing the metadata of the IFC-Model and
**  each entity-table _must_ have a reference to an entry of this table. How
**  the model can be uniquely identified is yet unknown, but the STEP-file
**  header should contain sufficient information for such a thing.
*/
#ifndef DB_H
#define DB_H

#include <string>

#include "symtab.h"

namespace CodeGen::DB {

/*! Generates an SQL script for generating a database for the IFC schema whose
**  deffinitions are stored in the given symbol table.
**
**  @param st - (a reference to) the symbol table containing an IFC schema
**  @param template_base - currently unused, later the base directory of
**              templates used for creating the SQL script.
**  @param outfile_base - the filename/pathname of the of the SQL script
**              being created.
*/
bool cgen (Symtab &st,
	   const std::string &template_base,
	   const std::string &outfile_base);

} /*namespace CodeGen::DB*/

#endif /*DB_H*/
