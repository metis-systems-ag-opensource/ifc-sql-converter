/* copyfile.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Copying one of the "fixed" files from a given directory to another one.
**
*/

#include <cerrno>
#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <system_error>

#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "copyfile.h"

namespace fs = std::filesystem;
using std::ios_base, std::ifstream, std::ofstream, std::streamsize;
using std::runtime_error;
using std::system_error, std::system_category, std::error_code;
using std::string, std::to_string;

streamsize copyfile (const string &src, const string &dst)
{
    streamsize bytes_copied = 0;
    string dst1;
    error_code ec;
    if (! fs::exists (src, ec)) {
	throw runtime_error ("'" + src + "' - file not found.");
    }
    if (! fs::is_regular_file (src)) {
	throw runtime_error ("'" + src + "' - regular file expected.");
    }
    if (! fs::exists (dst) || fs::is_regular_file (dst)) {
	dst1 = dst;
    } else if (fs::is_directory (dst)) {
	dst1 = dst / basename (src);
    } else {
	throw runtime_error ("'" + dst + "'"
			     " - regular file or directory expected.");
    }

    // 'src' contains the pathname of a valid regular file!
    // 'dst1' contains a valid target pathname! (An already existing regular
    // file 'dst1' will be overwritten.

    char *buf = new char[65536];

    ifstream infile (src, ios_base::in | ios_base::binary);
    if (! infile.is_open()) {
	throw system_error (errno, system_category(), src);
    }
    ofstream outfile (dst1, ios_base::out | ios_base::binary);
    if (! outfile.is_open()) {
	throw system_error (errno, system_category(), dst1);
    }
    bool failure = false;
    for (bool atend = false; ! atend;) {
	infile.read (buf, sizeof(buf));
	if (infile.fail() && ! infile.eof()) {
	    failure = true; break;
	}
	streamsize bytes_read = infile.gcount();
	outfile.write (buf, bytes_read);
	if (outfile.fail()) {
	    failure = true; break;
	}
	bytes_copied += bytes_read;
	atend = infile.eof();
    }
    
    outfile.close();
    infile.close();

    return (failure ? -bytes_copied : bytes_copied);
}
