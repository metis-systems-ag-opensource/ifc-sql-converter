/*! \file sel.h
**  Selector module for the code generation
**
**  This module uses the operation mode gotten from the main program for
**  selecting the appropriate code generator.
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'CodeGen/sel.cc'
** (Selector module for the code generation)
**
*/
#ifndef SEL_H
#define SEL_H

#include <string>

#include "opmode.h"
#include "symtab.h"

namespace CodeGen {

bool cgen (EXPRESS::OpMode tag,
	   Symtab &symtab,
           const std::string &template_base,
           const std::string &outfile_base);

} /*namespace CodeGen*/

#endif /*SEL_H*/
