/* gen-factory.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Generate the factory for thre 'EntityBase' objects
**
*/

#include <iostream>

#include <stdexcept>
#include <fstream>

#include "Common/sutil.h"
#include "Common/fnutil.h"

#include "gen-factory.h"

using std::string, std::to_string;
using std::runtime_error, std::ofstream;

static const char *defnfile =
    "/* %hfile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, Metis AG" EOL
    "**" EOL
    "**" EOL
    "** Interface file for '%filename.cc'" EOL
    "** (Implementation of the Factory function which generates 'EntityBase'" EOL
    "**  objects.)" EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE" EOL
    "**" EOL
    "*/" EOL
    "#ifndef %HFILE" EOL
    "#define %HFILE" EOL
    "" EOL
    "#include \"valtypes.h\"" EOL
    "#include \"expbase.h\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "EntityBase *expgen (const STEP::Instance *inst);" EOL
    "" EOL
    "} /*namespace IFC*/" EOL
    "" EOL
    "#endif /*%HFILE*/" EOL
;

static const char *implhead =
    "/* %ifile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com"
    "** Copyright: (c) 2019, Metis AG" EOL
    "**" EOL
    "**" EOL
    "** Implementation of the Factory function which generates 'EntityBase'" EOL
    "** objects." EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE" EOL
    "**" EOL
    "*/" EOL
    "" EOL
    "#include \"plinterface.h\"" EOL
    "" EOL
    "#include \"entities.h\"" EOL
    "" EOL
    "#include \"%hfile\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "EntityBase *expgen (const STEP::Instance *inst)" EOL
    "{" EOL
    "    EntityBase *res = nullptr;" EOL
    "    ExpTypes et = index2id (inst->entity.name - 1);" EOL
    "" EOL
    "    switch (et) {" EOL
;

static const char *swcase =
    "\tcase ExpTypes::%{typetag}: {" EOL
    "\t    res = new %{typename} (inst->id, inst->entity);" EOL
    "\t    break;" EOL
    "\t}" EOL
;

static const char *implfoot =
    "\tdefault: {" EOL
    "\t    // DO NOTHING HERE!" EOL
    "\t    break;" EOL
    "\t}" EOL
    "    }" EOL
    "    return res;" EOL
    "}" EOL
    "" EOL
    "} /*namespace IFC*/" EOL
;

void gen_expfactory (Symtab &st, const TypeIndexList &til,
		     const string &fnprefix)
{
    ofstream hout, iout;
    const string &schema = st.schema();
    string defname = fnprefix + ".h", implname = fnprefix + ".cc";
    string hfile = basename (defname), ifile = basename (implname);
    Substitutes fvars {
	{ "hfile", hfile },
	{ "HFILE", trans_filename (hfile) },
	{ "ifile", ifile },
	{ "lcschema", lowercase (schema) }
    };

    hout.open (defname);
    if (! hout.is_open()) {
	throw runtime_error ("Write attempt to '" + defname + "' failed.");
    }

    iout.open (implname);
    if (! iout.is_open()) {
	throw runtime_error ("Write attempt to '" + implname + "' failed.");
    }

    hout << tmpl_subst (defnfile, fvars);
    iout << tmpl_subst (implhead, fvars);

    for (auto [id, ix]: til) {
	auto ets = dynamic_cast<TsEntity *>(st[id]);
	if (ets && ! ets->abstract && ets->flatAttributes.size() > 0) {
	    const string &etsname = st.symname (id);
	    Substitutes cvars {
		{ "typetag", etsname },
		{ "typename", etsname }
	    };
	    iout << tmpl_subst (swcase, cvars);
	}
    }

    iout << tmpl_subst (implfoot, fvars);
    
    iout.close(); hout.close();
}
