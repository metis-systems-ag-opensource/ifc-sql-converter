/*! \file outstrings.h
**  Part of the plugin and database code generators.
**
**  This module creates a human readable string (or output)  from a list of
**  strings, by splitting the output into lines of a length which doesn't
**  exceed a specified maximum, and by adding user defined indentation to the
**  beginning of each new line.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file rof 'outstrings.cc'
** (Small utility for writing a list of strings into an output stream in a
**  human-readable format (with a line width, an indent and a delimiter
**  - specifiable by the user).
**
*/
#ifndef OUTSTRINGS_H
#define OUTSTRINGS_H

#include <ostream>
#include <string>

#include "Common/fifo.h"

#define DEF_DELIM ""
#define DEF_INDENT 4
#define DEF_MAXLL 80

std::ostream &out_strings (std::ostream &out, const Fifo<std::string> &strlist,
			   const std::string &delim = DEF_DELIM,
			   unsigned maxll = DEF_MAXLL,
			   unsigned indent = DEF_INDENT);

std::string gen_strlist (const Fifo<std::string> &strlist,
			 const std::string &delim = DEF_DELIM,
			 unsigned maxll = DEF_MAXLL,
			 unsigned indent = DEF_INDENT);

#endif /*OUTSTRINGS_H*/
