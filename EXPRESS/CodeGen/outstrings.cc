/* outstrings.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Small utility for writing a list of strings into an output stream in a
** human-readable format (with a line width, a 
**
*/

#include <cstring>
#include <sstream>

#include "Common/sutil.h"

#include "outstrings.h"

using std::ostream, std::stringstream, std::ostringstream;
using std::string;
using std::endl;

ostream &out_strings (ostream &out, const Fifo<string> &strlist,
		      const string &delim, unsigned maxll, unsigned indent)
{
    // Even if the delimiter is empty, i'm writing it out, so i need this
    // variable (my flip-flop) which enables an output of the delimiter for
    // each element except the first one ...
    bool first = true, firstline = true;
    // I'm using the variable 'cc' (for 'c'olumn 'c'ounter) for the decision
    // when an 'eol' must be inserted. I'm initialising it to the maximum value
    // of columns allowed in a line, because then it generates the first
    // element at the correct position.
    unsigned cc = maxll;
    // Now, the output loop walks through the complete string list.
    for (auto str: strlist) {
	// All elements except the first need a leading delimiter.
	if (first) { first = false; } else { out << delim; ++cc; }
	if (cc + str.size() + delim.size() + 1 >= maxll) {
	    // If there os not enough room for the current element in the line,
	    // an 'eol' is inserted and the output restarted at the first
	    // column in the new line. This output then starts with an
	    // indentation of four blanks.
	    cc = indent; out << (firstline ? "" : EOL) << string(cc, ' ');
	    firstline = false;
	} else {
	    // Otherwise a blank is inserted for (visibly) delimiting the
	    // current element from the previous one...
	    out << " "; ++cc;
	}
	out << str; cc += str.size();
    }
    return out;
}

string gen_strlist (const Fifo<string> &strlist, const string &delim,
		    unsigned maxll, unsigned indent)
{
    ostringstream res;
    out_strings (res, strlist, delim, maxll, indent);
    return res.str();
}
