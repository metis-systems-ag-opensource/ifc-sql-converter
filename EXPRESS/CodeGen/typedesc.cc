/* CodeGen/typedesc.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Implementation part of 'CodeGen/typedesc.h'
** (The type of the 'type description' parameter used in the functions which
**  are used in the C++ code-generator.)
**
*/

#include <cctype>

#include "Common/sutil.h"

#include "typedesc.h"

using namespace std;

static size_t tdcnt = 0;

void TypeDesc::init()
{
    tdcnt = 0;
}

TypeDesc::TypeDesc (Ident vid, const std::string &rtag, TypeStruct *ptts)
    : tag(&rtag), ts(ptts), idx(tdcnt++), id(vid)
{ }

/* letter case independent (ASCII) comparison of two strings ('<').
** I checked (in a benchmark test) that using an index on a string is between
** two and three times faster than using iterators, so i'm using the index
** method for iterators. Maybe, if this would be a UTF-8 comparison, i would
** use iterators (better abstraction), but in this case, they are far too slow
** for being usable ...
*/
bool tdless (const TypeDesc &tdl, const TypeDesc &tdr)
{
    return lccmp (*tdl.tag, *tdr.tag) < 0;
}
