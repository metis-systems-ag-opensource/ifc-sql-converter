/* gen-enumtabs.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Generate the table for a fast access to the EXPRESS ENUMERATION constants.
**
*/

#include <iostream>

#include <stdexcept>
#include <fstream>

#include "Common/sutil.h"
#include "Common/fnutil.h"

#include "outstrings.h"

#include "gen-enumtabs.h"

using std::string, std::to_string;
using std::runtime_error;

static const char *implhead =
    "/* %ifile" EOL
    "**" EOL
    "** Author: Boris jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, 2020, Metis AG" EOL
    "**" EOL
    "** The tables used by the functions 'enumval()' and 'enumname()'." EOL
    "** GENERATED FILE. MODIFICATIONS ARE AT YOUR OWN RISK!" EOL
    "**" EOL
    "*/" EOL
    "#ifndef %IFILE" EOL
    "#define %IFILE" EOL
;
static const char *implfoot =
    "" EOL
    "#endif /*%IFILE*/" EOL
;

static const char *enumnamestmpl =
    "" EOL
    "// A simple name list for the ENUMERATION names (a long string with NUL-"
    EOL
    "// terminated segments, which specify the names of the ENUMERATION values"
    EOL
    "// as C-strings)." EOL
    "static const char *enumnames =" EOL
    "%ENUMNAMES" EOL
    ";" EOL
    "" EOL
;

static const char *enumidxtmpl =
    "" EOL
    "// A table of indexes into 'enumnames[]'. The index of an element of this"
    EOL
    "// table (+ 1) is the ENUMERATION value, the value (as already announced)"
    EOL
    "// points into the list of ENUMERATION names 'enumnames[]'." EOL
    "static %index_type enumidx[] = {" EOL
    "%ENUMIDX" EOL
    "};" EOL
    "" EOL
;

static const char *enumvaluestmpl =
    "" EOL
    "// The table of ENUMERATION values (for _all_ ENUMERATIONs). This table"
    EOL
    "// is (virtually) splitted into segments, such that each segment contains"
    EOL
    "// the (integer) values of a specific ENUMERATION type." EOL
    "static %value_type enumvalues[] {" EOL
    "%ENUMVALUES" EOL
    "};" EOL
    "" EOL
;

static const char *enumrangetmpl =
    "" EOL
    "// The type for the enumrange table" EOL
    "struct EnumRange { %basex_type base_ix; %num_type num_el; };" EOL
    "" EOL
    "// This table \"implements\" the splitting of 'enumvalues[]' into the" EOL
    "// different segments of the ENUMERATION types." EOL
    "static struct EnumRange enumrange[] = {" EOL
    "%ENUMRNG" EOL
    "};" EOL
;

static const char *enumtnametmpl =
    "" EOL
    "// This name list is not for the ENUMERATION values, but for the" EOL
    "// ENUMERATION type names." EOL
    "static const char *enumtypenames =" EOL
    "%ENUMTYPENAMES" EOL
    ";" EOL
;

static const char *enumtidxtmpl =
    "" EOL
    "static %typeindex_type enumtypeidx[] = {" EOL
    "%ENUMTYPEIDX" EOL
    "};" EOL
;

using std::string, std::to_string, std::pair;

struct EnumTypeData {
    Ident id; ExpIndex tidx; uint32_t nidx;
};

static inline void setmax (size_t max, size_t newval)
{
    if (newval > max) { max = newval; }
}

TypeIndexList gen_enumtables (Symtab &st, const TypeIndexList &til,
			      const string &pathname)
{
    map<string, uint32_t> nameidx_map;
    vector<EnumTypeData> enumtypedata;
    vector<const char *> enumnames, nameseg;
    vector<uint32_t> enumidx, enumvalues, valseg;
    struct Range { uint32_t base, numel; };
    vector<Range> enumrange;
    TypeIndexList enums;
    Fifo<string> outlist, outlist2;

    size_t valmax, valmax2;

    string filename = basename (pathname);
    ofstream out;
    Substitutes vars {
	{ "ifile", filename },
	{ "IFILE", trans_filename (filename) }
    };

    out.open (pathname);
    if (! out.is_open()) {
	throw runtime_error ("Write attempt to '" + pathname + "' failed.");
    }

    uint32_t rngx = 0, valx = 1, strx = 0, basex = 0, tstrx = 0;
    for (auto [id, tidx]: til) {
	auto ets = dynamic_cast<TsEnum *>(st[id]);
	if (! ets) { continue; }
	// Collect data from the typenames for creating a table(-pair) which
	// permits the access to the typenames
	const string &tname = st.symname (id);
	enumtypedata.push_back (EnumTypeData { id, tidx, tstrx });
	tstrx += tname.size() + 1;

	// Collect the data for the names, indexes, ... of the ENUMERATION
	// values.
	++rngx; basex = valx;
	valseg.clear(); nameseg.clear();
	for (auto evid: ets->values) {
	    const string &name = st.symname (evid);
	    auto [ iter, done ] = nameidx_map.insert (make_pair (name, strx));
	    if (done) {
		enumidx.push_back (strx); strx += name.size() + 1;
		enumnames.push_back (iter->first.c_str());
	    } else {
		enumidx.push_back (iter->second);
	    }
	    nameseg.push_back (iter->first.c_str());
	    valseg.push_back (valx++);
	}
	sort (valseg.begin(), valseg.end(),
	      // Capture 'nameseg' and 'basex' from the environment of
	      // this lambda expression - meaning: 'nameseg' and 'basex'
	      // are visible within the lambda expression without explicitely
	      // stating them as arguments. This is important here, as a 'less'
	      // function can have exctly two arguments. The C++ compiler
	      // translates the lambda expression into an object with a local
	      // state which consists of these two references and an
	      // 'operator() (uint32_t l, uint32_t r)', which then is invoked
	      // as "method" of the lambda object each time a comparison is
	      // required.
	      [&nameseg, &basex](uint32_t l, uint32_t r) {
		  // Using 'lccmp (const char *, const char *)' from 'sutil'...
		  return lccmp (nameseg[l - basex],
				nameseg[r - basex]) < 0;
	      });
	// The "sorted" list of elements is then appended to the 'enumvalues[]'
	// table, and a new 'Range' element appended to 'enumrange[]' which
	// describes this list of elements.
	enumvalues.insert (enumvalues.end(), valseg.begin(), valseg.end());
	enumrange.push_back (Range { basex, valx - basex });
	enums.push_back (make_pair (id, rngx));

    }

    // After collecting the data, the string containing the complete file
    // 'enumtabs.cc' must be generated. This is done with an intermediate step,
    // Creating a ('Fifo'-)list of strings and then applying 'gen_strlist()' on
    // this list. 'gen_strlist()' is another variant of 'out_strings()' from
    // 'outstrings.{cc,h}' which allows a formatted "output" of strings.
    for (auto name: enumnames) { outlist.push ("\""s + name + "\\0\""); }
    vars["ENUMNAMES"] = gen_strlist (outlist); outlist.clear();

    valmax = 0;
    for (auto ix: enumidx) {
	setmax (valmax, ix);
	outlist.push (to_string (ix));
    }
    vars["index_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["ENUMIDX"] = gen_strlist (outlist, ","); outlist.clear();

    valmax = 0;
    for (auto val: enumvalues) {
	setmax (valmax, val);
	outlist.push (to_string (val));
    }
    vars["value_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["ENUMVALUES"] = gen_strlist (outlist, ","); outlist.clear();

    valmax = 0; valmax2 = 0;
    for (auto rng: enumrange) {
	setmax (valmax, rng.base);
	setmax (valmax2, rng.numel);
	outlist.push ("{ " + to_string (rng.base) +
		      ", " + to_string (rng.numel) + " }");
    }
    vars["basex_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["num_type"] = (valmax2 >= 65536 ? "uint32_t" : "uint16_t");
    vars["ENUMRNG"] = gen_strlist (outlist, ","); outlist.clear();

    valmax = 0;
    for (auto [ id, tidx, nidx ]: enumtypedata) {
	const string &tname = st.symname (id);
	outlist.push ("\""s + tname + "\\0\"");
	setmax (valmax, nidx);
	outlist2.push (to_string (nidx));
    }
    vars["typeindex_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["ENUMTYPENAMES"] = gen_strlist (outlist); outlist.clear();
    vars["ENUMTYPEIDX"] = gen_strlist (outlist2, ","); outlist2.clear();

    out << tmpl_subst (implhead, vars);
    out << tmpl_subst (enumnamestmpl, vars);
    out << tmpl_subst (enumidxtmpl, vars);
    out << tmpl_subst (enumvaluestmpl, vars);
    out << tmpl_subst (enumrangetmpl, vars);
    out << tmpl_subst (enumtnametmpl, vars);
    out << tmpl_subst (enumtidxtmpl, vars);
    out << tmpl_subst (implfoot, vars);
    out.close();
    return enums;
}
