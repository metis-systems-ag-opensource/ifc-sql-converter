/*! \file gen-stepsyms.h
**  Part of the plugin code generator
**
**  Creating static tables containing all STEP symbols and corresponding
**  type tags. The generated table is something like a simple symbol table
**  as an interface to the STEP interpreter `stp`.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'gen-stepsyms.cc'
** (Generate tables with the step symbols and the corresponding tags (and later:
**  object generators). Background is that there only a fixed number of
**  different symbol types, which can be stored in static tables. The symbol
**  names will be stored in one long C-string in the generated code.)
**
*/
#ifndef GEN_STEPSYMS_H
#define GEN_STEPSYMS_H

#include <string>

#include "symtab.h"
#include "gen-typedeps.h"

void gen_stepsyms (Symtab &st, const TypeIndexList &til, const string &outdir,
		   const string &exptypes, const string &stepif);

#endif /*GEN_STEPSYMS_H*/
