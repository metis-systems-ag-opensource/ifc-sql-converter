/* db-utils.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Collect the entity type hierarchy
**
*/

#include <iostream>

#include <utility>
#include <stdexcept>

#include "myassert.h"
#include "db-utils.h"

using namespace std;

namespace CodeGen::DB {

//DEBUG-flag
bool dbgswitch = false;
Symtab *dbgst = nullptr;
bool dbgon() { return dbgswitch; }
void dbgon (Symtab &st) { dbgswitch = true; dbgst = &st; }
void dbgoff() { dbgswitch = false; dbgst = nullptr; }

// Completing the 'struct' class definition from the header file by
// implementing its member functions ...
//
//struct EthElement {

EthElement::EthElement (Ident id)
    : subtypes(), abstract(false), edone(false)
{
    if (id != 0) { subtypes.insert (id); }
}

EthElement::EthElement (bool isabs, Ident id)
    : subtypes(), abstract(isabs), edone(false)
{
    if (id != 0) { subtypes.insert (id); }
}

EthElement::EthElement (EthElement &&x)
    : subtypes(move(x.subtypes)), abstract(x.abstract), edone(x.edone)
{ }

EthElement::~EthElement () { }

bool EthElement::addSubtype (Ident id)
{
    myassert (id != 0, "Can't add the invalid identifier.");
    auto rr = subtypes.insert (id);
    return rr.second;
}

bool EthElement::hasSubtype (Ident id)
{
    return id != 0 && subtypes.count (id) > 0;
}

bool EthElement::isleaf ()
{
    return ! abstract && subtypes.size() > 0;
}

//} /*EthElement*/;

// Helper function (better that XYZ.count (x) > 0)!!!
//
static bool has (EtHierarchy idethmap, Ident element)
{
    return idethmap.count (element) > 0;
}

// Insert the type hierarchy of a single ENTITY into an existing type
// hierarchy map. This must be done recursively, as the supertypes of
// the element being inserted must also be inserted (with the corresponding
// subtype relationship being created). This means that (e.g.) with an
// ENTITY hierarchy like the following:
//
//   ENTITY a;
//     ...
//   END_ENTITY;
//
//   ENTITY b;
//     SUBTYPE OF (a);
//     ...
//   END_ENTITY;
//
//   ENTITY c;
//     SUBTYPE OF (b);
//     ...
//   END_ENTITY;
//
// and 'c' being found first in the symbol table, 'b' must be inserted prior to
// 'c', because only then there is a valid 'EthElement'-entry which the 'c' can
// be added as subtype to. Also, 'a' must be inserted prior to 'b' (if it not
// already exists), for the same reason. This only can be done recursively.
//
static void ethcollect_single (Symtab &st, Ident id, EtHierarchy &eth)
{
    auto ets = dynamic_cast<TsEntity *>(st[id]);
    if (ets) {
	IdentList &supertypes = ets->mysupertypes;
	for (Ident supertype: supertypes) {
	    auto supertts = dynamic_cast<TsEntity *>(st[supertype]);
	    myassert (supertts != nullptr,
		      "Undefined supertype '" + st.symname (supertype) + "'.");
	    ethcollect_single (st, supertype, eth);
	    eth[id].addSubtype (supertype);
	}
	if (! has (eth, id)) {
	    eth.emplace (id, EthElement (ets->abstract));
	}
    }
}

// For each element in the ENTITY type hierarchy, add the subtype-sets of each
// of its subtypes to its subtype-set. This is done because if (e.g.) B is a
// subtype of A and C is a subtype of B, then C is also a subtype of A, and in
// the tables to be generated, defining SELECT-tables whose members contain
// indices to SELECT-tables whose members contain indices to ... (you know what
// i want to say?) Such a hierarchy is very difficult to handle, so i'm (again)
// "flattening" something (in this case it is the type hierarchy of the ENTITY
// types).
static void eembed_single (EtHierarchy &eth, Ident id)
{
    EthElement &ethel = eth[id];
    if (! ethel.edone) {
	set<Ident> newsubt;
	set<Ident> &subtypes = ethel.subtypes;
	for (Ident subtype: subtypes) {
	    eembed_single (eth, subtype);
	    newsubt.insert (subtype);
	    set<Ident> &subsubt = eth[subtype].subtypes;
	    for (Ident subsubtype: subsubt) {
		newsubt.insert (subsubtype);
	    }
	}
	// Why does the following work? Because 'ethel' is an alias of
	// 'eth[id]' and 'subtypes' is an alias of 'ethel.subtypes', so it is
	// also an alias of 'eth[id].subtypes'. This means that an assignment
	// to 'subtypes' replaces the value of 'eth[id].subtypes' and one to
	// 'ethel.edone' replaces the value of 'eth[id].edone'.
	subtypes = move(newsubt); ethel.edone = true;
    }
}

void ethembed (EtHierarchy &eth)
{
    for (auto el: eth) {
	Ident eid = el.first;
	eembed_single (eth, eid);
    }
}

// Collect ENTITY types in a map and create the their type hierarchies while
// doing this. This is done by walking through the symbol table and collecting
// each ENTITY, using the (internal) helper function 'ethcollect_single()'
// above. The result from this function is the created type hierarchy, which
// simply is a 'map<Ident, EthElement>'.
//
EtHierarchy ethcollect (Symtab &st)
{
    EtHierarchy res;
    for (Ident id = st.first(); id <= st.last(); ++id) {
	ethcollect_single (st, id, res);
    }
    return res;
}

Fifo<Ident> filter_entities (EtHierarchy &eth,
			     bool (*pred) (const EthElement &))
{
    Fifo<Ident> res;
    for (auto &ethel: eth) {
	Ident id = ethel.first;
	EthElement &el = ethel.second;
	if (pred (el)) { res.push (id); }
    }
    return res;
}

bool is_leaf (const EthElement &el)
{
    return ! el.abstract && el.subtypes.size() == 0;
}

Fifo<Ident> get_leaf_entities (EtHierarchy &eth)
{
    return filter_entities (eth, is_leaf);
}

bool has_multiple_subtypes (const EthElement &el)
{
    return el.subtypes.size() > 1;
}

Fifo<Ident> get_multisubtype_entities (EtHierarchy &eth)
{
    return filter_entities (eth, has_multiple_subtypes);
}

bool has_single_subtype (const EthElement &el)
{
    return el.subtypes.size() == 1;
}

Fifo<Ident> get_singlesubtype_entities (EtHierarchy &eth)
{
    return filter_entities (eth, has_single_subtype);
}

#if 0
struct RevElement {
    set<Ident> supertypes;
    //bool done;
    RevElement () : supertypes()/*, done(false)*/ { }
    RevElement (const RevElement &x)
	: supertypes(x.supertypes)/*, done(x.done)*/
    { }
    RevElement (RevElement &&x)
	: supertypes(move (x.supertypes))/*, done(x.done)*/
    { }
    ~RevElement () { }
    void add_supertype (Ident supertype) {
	if (st != 0) {
	    supertypes.insert (supertype);
	}
    }
};

using RevMap = map<Ident, RevElement>;
#endif

static bool has (const RevHierarchy &rm, Ident id)
{
    return rm.count (id) > 0;
}

static bool get_supertypes_single (Symtab &st, Ident id, RevHierarchy &rm)
{
    // Assuming that 'id' is really an ENTITY symbol here (no further checks).
    auto ets = dynamic_cast<TsEntity *>(st[id]);
    if (has (rm, id)) { return true; }
    if (! ets) { return false; }
    rm.insert (make_pair (id, set<Ident>()));
    for (Ident supertype: ets->mysupertypes) {
	if (! get_supertypes_single (st, supertype, rm)) { return false; }
	rm[id].insert (supertype);
	for (Ident supsupertype: rm[supertype]) {
	    rm[id].insert (supsupertype);
	}
    }
    return true;
}

static bool get_supertypes (Symtab &st, RevHierarchy &rm)
{
    for (Ident id = st.first(); id <= st.last(); ++id) {
	auto ets = dynamic_cast<TsEntity *>(st[id]);
	if (! ets) { continue; }
	if (! get_supertypes_single (st, id, rm)) { return false; }
    }
    return true;
}

RevHierarchy get_revhierarchy (Symtab &st)
{
    RevHierarchy res;
    if (! get_supertypes (st, res)) {
	throw runtime_error ("get_supertypes() failed.");
    }
    return res;
}

set<Ident> get_roottypes (const RevHierarchy &rh, Ident id)
{
    set<Ident> res;
    for (Ident supertype: rh.at(id)) {
	if (rh.at(supertype).size() == 0) {
	    res.insert (supertype);
	}
    }
    return res;
}

set<Ident> get_roottypes (const RevHierarchy &rh)
{
    set<Ident> res;
    for (auto el: rh) {
	Ident id = el.first;
	set<Ident> &supertypes = el.second;
	if (supertypes.size() == 0) {
	    res.insert (id);
	}
    }
    return res;
}

set<Ident> get_subtypes (const RevHierarchy &rh)
{
    set<Ident> res;
    for (auto el: rh) {
	Ident id = el.first;
	set<Ident> &supertypes = el.second;
	if (supertypes.size() > 0) {
	    res.insert (id);
	}
    }
    return res;
}

ostream &dump_roottypes (Symtab &st, const set<Ident> &roottypes,
			 ostream &out)
{
    //bool first = true;
    for (Ident eid: roottypes) {
	//if (first) { first = false; } else { out << ", "; }
	out << "    ";
	out << st.symname (eid) << "(" << eid << ")";
	out << endl;
    }
    return out;
}

} /*namespace CodeGen::DB*/
