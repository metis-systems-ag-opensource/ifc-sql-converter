/*! \file attrtype.h
**  Generating C++ code which corresponds to type references in the
**  declarations of attributes and TYPE definitions.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Interface file for 'attrtype.cc'
** (Generate code for defined types/ENTITY attribute types/...).
**
*/
#ifndef ATTRTYPE_H
#define ATTRTYPE_H

#include <string>

#include "symtab.h"

/*! Translates the simple types (which are names `BOOLEAN`, `LOGICAL`, ...)
**  to the type names used in the fixed part of the Plugin source code:
**  `ExpBoolean`, `ExpLogical`, ...
*/
const char *tntrans (Symtab &st, Ident tid);

/*! Generates the C++ analogue of the types used in TYPE definitions and
**  attribute declarations.
*/
std::string gen_attrtype (Symtab &st, Ident eid, Ident aid, TypeStruct *ts);

#endif /*ATTRTYPE_H*/
