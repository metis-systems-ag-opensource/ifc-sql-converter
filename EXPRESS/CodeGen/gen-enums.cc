/* gen-enums.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Generating the ENUMERATION types
**
*/

#include <iostream>

#include <stdexcept>
#include <fstream>

#include "Common/sutil.h"
#include "Common/fnutil.h"

#include "gen-enums.h"

using std::string, std::to_string;
using std::runtime_error;

static const char *defnhead =
    "/* %hfile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, Metis AG" EOL
    "**" EOL
    "** Specifications of the ENUMERATION classes." EOL
    "** AUTOMATICALLY GENERATED FILE. Modifications at your own risk." EOL
    "**" EOL
    "*/" EOL
    "#ifndef %HFILE" EOL
    "#define %HFILE" EOL
    "" EOL
    "#include <cstdint>" EOL
    "#include <string>" EOL
    "" EOL
    "#include \"enumbase.h\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
;

static const char *defnfoot =
    "" EOL
    "} /*namespace IFC*/" EOL
    "" EOL
    "#endif /*%HFILE*/" EOL
;

static const char *implhead =
    "/* %ifile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, Metis AG" EOL
    "**" EOL
    "** Implementations of the member functions of the ENUMERATION classes." EOL
    "** AUTOMATICALLY GENERATED FILE. Modifications at your own risk." EOL
    "**" EOL
    "*/" EOL
    "" EOL
    "#include <stdexcept>" EOL
    "" EOL
    "#include \"enumval.h\"" EOL
    "" EOL
    "#include \"%hfile\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "using std::string, std::to_string;" EOL
    "using std::domain_error, std::runtime_error;" EOL
;

static const char *implfoot =
    "" EOL
    "} /*namespace IFC*/" EOL
;

static const char *enumdef =
    "" EOL
    "// ENUMERATION type %type" EOL
    "struct %type : _EnumBase {" EOL
    "    %type (const STEP::Value *v);" EOL
    "    %type (const std::string &name);" EOL
    "    %type (const %type &x) = default;" EOL
    "    uint32_t value();" EOL
    "    std::string name() const;" EOL
    "    operator uint32_t();" EOL
    "    operator std::string();" EOL
    "    ExpTypes type() const;" EOL
    "    bool is_same_as (const ExpBase *rx) const;" EOL
    "    std::string todb() const;" EOL
    "private:" EOL
    "    //uint32_t rngdef();" EOL
    "    uint32_t _value;" EOL
    "};" EOL
;

static const char *enumimpl =
    "" EOL
    "/* Member functions of %type */" EOL
    "%type::%type (const STEP::Value *v)" EOL
    "{" EOL
    "    auto x = dynamic_cast<const STEP::EnumValue *>(v);" EOL
    "    if (! x) { throw domain_error (\"Invalid non-enumeration value\"); }"
    EOL
    "    _value = enumval (%index, x->value);" EOL
    "}" EOL
    "" EOL
    "%type::%type (const std::string &name)" EOL
    "{" EOL
    "    _value = enumval (%index, name);" EOL
    "}" EOL
    "" EOL
    "uint32_t %type::value() { return _value; }" EOL
    "%type::operator uint32_t() { return _value; }" EOL
    "" EOL
    "string %type::name() const { return enumname (_value); }" EOL
    "" EOL
    "ExpTypes %type::type() const { return ExpTypes::%type; }" EOL
    "%type::operator string() { return enumname (_value); }" EOL
    "bool %type::is_same_as (const ExpBase *rx) const" EOL
    "{" EOL
    "    auto r = dynamic_cast<const %type *>(rx);" EOL
    "    if (! r) { return false; }" EOL
    "    return _value == r->_value; " EOL
    "}" EOL
    "" EOL
    "string %type::todb() const" EOL
    "{" EOL
    "    return to_string (_value);" EOL
    "}" EOL
;

void gen_enums (Symtab &st, const TypeIndexList &til, const string &fnprefix)
{
    ofstream hout, iout;
    string defname = fnprefix + ".h";
    string implname = fnprefix + ".cc";
    string hfile = basename (defname), ifile = basename (implname);

    Substitutes ftvars {
	{ "hfile", hfile },
	{ "HFILE", trans_filename (hfile) },
	{ "ifile", ifile }
    };

    hout.open (defname);
    if (! hout.is_open()) {
	// Need a better error output (based on 'errno').
	throw runtime_error ("Write attempt to '" + defname + "' failed.");
    }
    iout.open (implname);
    if (! iout.is_open()) {
	// Need a better error output (based on 'errno').
	throw runtime_error ("Write attempt to '" + implname + "' failed.");
    }

    hout << tmpl_subst (defnhead, ftvars);
    iout << tmpl_subst (implhead, ftvars);

    for (auto [id, rx]: til) {
	Substitutes svars {
	    { "type", st.symname (id) },
	    { "index", to_string (rx) }
	};
	hout << tmpl_subst (enumdef, svars);
	iout << tmpl_subst (enumimpl, svars);
    }

    hout << tmpl_subst (defnfoot, ftvars);
    iout << tmpl_subst (implfoot, ftvars);

    iout.close(); hout.close();
}
