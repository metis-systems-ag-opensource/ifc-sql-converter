/* gen-netype.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generate (a C++ class) for a Non-Entity type.
**
*/

#include <iostream>
using std::cout, std::cerr, std::endl;

#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <string>

#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "types.h"

#include "attrtype.h"

#include "gen-netype.h"

using std::string, std::to_string, std::runtime_error, std::domain_error;
namespace fs = std::filesystem;

static const char *defnhead =
    "/* %hfile" EOL
    "**" EOL
    "** $Id$" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2020, Metis AG" EOL
    "** " EOL
    "** Types which are neither 'simple types', nor ENUMERATION types, nor" EOL
    "** ENTITY types." EOL
    "**" EOL
    "*/" EOL
    "#ifndef %HFILE" EOL
    "#define %HFILE" EOL
    "" EOL
    "#include \"plinterface.h\"" EOL
    "#include \"expbase.h\"" EOL
    "#include \"aggtypes.h\"" EOL
    "#include \"selbase.h\"" EOL
    "" EOL
    "#include \"valtypes.h\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
;

static const char *defnfoot =
    "" EOL
    "} /*namespace IFC*/" EOL
    "" EOL
    "#endif /*%HFILE*/" EOL
;

static const char *implhead =
    "/* %ifile" EOL
    "**" EOL
    "** $Id$" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2020, Metis AG" EOL
    "** " EOL
    "** Types which are neither 'simple types', nor ENUMERATION types, nor" EOL
    "** ENTITY types." EOL
    "**" EOL
    "*/" EOL
    "" EOL
    "#include <stdexcept>" EOL
    "#include <string>" EOL
    "" EOL
    "#include \"selhelpers.h\"" EOL
    "" EOL
    "#include \"%hfile\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "using std::domain_error;" EOL
    "" EOL
;

static const char *implfoot =
    "" EOL
    "} /*namespace IFC*/" EOL
;

static const char *seltdefn =
    "" EOL
    "// SELECT type '%type'" EOL
    "struct %type : _ExpSelect {" EOL
    "    %type();" EOL
    "    %type (const STEP::Value *sv);" EOL
    "    ExpTypes type() const;" EOL
    "};" EOL
;

static const char *seltimpl =
    "" EOL
    "" EOL
    "// %type member function implementations" EOL
    "" EOL
    "%type::%type() : _ExpSelect() { }" EOL
    "" EOL
    "%type::%type (const STEP::Value *sv)" EOL
    "    : _ExpSelect (ExpTypes::%type, sv) { }" EOL
    "" EOL
    "ExpTypes %type::type() const { return ExpTypes::%type; }" EOL
;

static const char *defdtdefn =
    "" EOL
    "struct %type : %basetype {" EOL
    "    %type();" EOL
    "    %type (const STEP::Value *v);" EOL
//    "    ~%type();" EOL
    "    bool check() const;" EOL
    "    ExpTypes type() const;" EOL
    "};" EOL
;

static const char *defdtimpl =
    "" EOL
    "// Implementation part of '%type'" EOL
    "%type::%type() : %basetype() { }" EOL
    "" EOL
    "%type::%type (const STEP::Value *v)" EOL
    "    : %basetype (v)" EOL
    "{" EOL
    "    if (! check()) {" EOL
    "\tthrow std::runtime_error (\"WHERE condition of '%type' failed.\");" EOL
    "    }" EOL
    "}" EOL
    "" EOL
    "bool %type::check() const" EOL
    "{" EOL
    "    if (! %basetype::check ()) { return false; }" EOL
    "    // Currently, no check is performed." EOL
    "    return true;" EOL
    "}" EOL
    "" EOL
    "ExpTypes %type::type() const { return ExpTypes::%type; }" EOL
;

static const char *aggtdefn =
    "" EOL
    "// Definition part of '%type'" EOL
    "struct %type : %aggtype {" EOL
    "    %type();" EOL
    "    %type (const STEP::Value *sv);" EOL
    "    ExpTypes type() const;" EOL
#if 0
    "    bool is_same_as (const ExpBase *rx) const;" EOL
#endif
    "protected:" EOL
    "    bool check() const;" EOL
    "};" EOL
;

static const char *aggtimpl =
    "" EOL
    "// Implementation part of '%type'" EOL
    "%type::%type() : %aggtype() { }" EOL
    "" EOL
    "%type::%type (const STEP::Value *sv) " EOL
    "    : %aggtype (sv)" EOL
    "{   " EOL
    "    if (! check()) {" EOL
    "\tthrow domain_error (\"Constraints check for %type failed\");" EOL
    "    }    " EOL
    "}   " EOL
    "" EOL
    "ExpTypes %type::type() const { return ExpTypes::%type; }" EOL
    "" EOL
    // 'check()' currently returns 'true', but this will change the moment i
    // completed the code generator for expressions/functions/statements/...
    "bool %type::check() const" EOL
    "{   " EOL
    "    return true;" EOL
    "}   " EOL
    "" EOL
#if 0
    "bool %type::is_same_as (const ExpBase *rx) const" EOL
    "{" EOL
#if 1
    "    return v.is_same_as (rx);" EOL
#else
    "    auto r = dynamic_cast<const %type *>(rx);" EOL
    "    if (! r) { return false; }" EOL
    "    return v.is_same_as (r);" EOL
#endif
    "}" EOL
#endif
;

void gen_netype (Symtab &st, Ident id, std::ostream &hout, std::ostream &iout)
{
    TypeStruct *ts = st[id];
    TypeStruct::Type tstype = ts->type();

    // The types handled here are SELECT types, aggregate types (ARRAY, LIST
    // BAG and SET), and simple "defined" types.

    switch (tstype) {
	case TypeStruct::defined: {
	    auto dts = dynamic_cast<TsDefined *>(ts);
	    Substitutes vars {
		{ "type", st.symname (id) },
		{ "basetype", tntrans (st, dts->basetype) }
	    };
	    hout << tmpl_subst (defdtdefn, vars);
	    iout << tmpl_subst (defdtimpl, vars);
	    break;
	}
	case TypeStruct::select: {
	    //auto sts = dynamic_cast<TsSelect *>(ts);
	    Substitutes vars {
		{ "type", st.symname (id) }
	    };
	    hout << tmpl_subst (seltdefn, vars);
	    iout << tmpl_subst (seltimpl, vars);
	    break;
	}
	case TypeStruct::array: {
	    // For arrays, i need the normal 'get_attrtypes()' function i used
	    // in 'gen-entities.cc', so i think i must put this function in
	    // its own module.
	    Substitutes vars {
		{ "type", st.symname (id) },
		{ "aggtype", gen_attrtype (st, id, 0, ts) }
	    };
	    hout << tmpl_subst (aggtdefn, vars);
	    iout << tmpl_subst (aggtimpl, vars);
	    break;
	}
	case TypeStruct::list: {
	    // Same as for arrays...
	    Substitutes vars {
		{ "type", st.symname (id) },
		{ "aggtype", gen_attrtype (st, id, 0, ts) }
	    };
	    hout << tmpl_subst (aggtdefn, vars);
	    iout << tmpl_subst (aggtimpl, vars);
	    break;
	}
	case TypeStruct::bag: {
	    // Same as for arrays...
	    Substitutes vars {
		{ "type", st.symname (id) },
		{ "aggtype", gen_attrtype (st, id, 0, ts) }
	    };
	    hout << tmpl_subst (aggtdefn, vars);
	    iout << tmpl_subst (aggtimpl, vars);
	    break;
	}
	case TypeStruct::set: {
	    // Same as for arrays...
	    Substitutes vars {
		{ "type", st.symname (id) },
		{ "aggtype", gen_attrtype (st, id, 0, ts) }
	    };
	    hout << tmpl_subst (aggtdefn, vars);
	    iout << tmpl_subst (aggtimpl, vars);
	    break;
	}
	default: break;
    }
}

void gen_netypes (Symtab &st, const TypeIndexList &til, const string &ofpfx)
{
    ofstream iout, hout;
    string ifname, hfname;
    std::error_code ec;
    if (fs::is_directory (ofpfx, ec)) {
	ifname = ofpfx / "netypes.cc";
	hfname = ofpfx / "netypes.h";
    } else {
	ifname = ofpfx + ".cc";
	hfname = ofpfx + ".h";
    }
    if ((fs::exists (ifname, ec) && ! fs::is_regular_file (ifname, ec))) {
	throw runtime_error ("Regular of no file '" + ifname + "' expected");
    }
    if ((fs::exists (hfname, ec) && ! fs::is_regular_file (hfname, ec))) {
	throw runtime_error ("Regular of no file '" + hfname + "' expected");
    }
    iout.open (ifname);
    if (! iout.is_open()) {
	throw runtime_error ("Failed to open '" + ifname + "'");
    }
    hout.open (hfname);
    if (! hout.is_open()) {
	throw runtime_error ("Failed to open '" + hfname + "'");
    }
    Substitutes vars {
	{ "ifile", basename (ifname) },
	{ "hfile", basename (hfname) },
	{ "HFILE", trans_filename (basename (hfname)) }
    };

    iout << tmpl_subst (implhead, vars);
    hout << tmpl_subst (defnhead, vars);

    TypeIndexList stil = depsort_types (st, til);
    for (auto [id, ix]: stil) {
	//TypeStruct *ts = st[id];
	gen_netype (st, id, hout, iout);
    }

    iout << tmpl_subst (implfoot, vars);
    hout << tmpl_subst (defnfoot, vars);

    hout.close(); iout.close();
}
