/* db-utils.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'db-utils.cc'
** Collect the entity type hierarchy
**
*/
#ifndef DB_UTILS_H
#define DB_UTILS_H

#include <utility>
#include <set>
#include <map>
#include <iostream>

#include "symtab.h"

namespace CodeGen::DB {

//DEBUG-flag
extern bool dbgswitch;
extern Symtab *dbgst;
bool dbgon();
void dbgon (Symtab &st);
void dbgoff();
#define ifdbg if (dbgswitch)

// EthElement => 'Entity Type Hierarchy Element'
struct EthElement {
    std::set<Ident> subtypes;
    bool abstract, edone;
    EthElement (Ident id = 0);
    EthElement (bool isabs, Ident id = 0);
    EthElement (const EthElement &x) = default;
    EthElement (EthElement &&x);
    ~EthElement ();
    bool addSubtype (Ident id);
    bool hasSubtype (Ident id);
    bool isleaf();
} /*EthElement*/;

// EtHierarchy => 'Entity Type Hierarchy'
using EtHierarchy = std::map<Ident, EthElement>;

// Retrieve the type hierarchy of all ENTITY entries in the symbol table.
// This is not done by checking the (often incomplete and far too complex)
// SUPERTYPE constraints, but by the SUBTYPE specification. Remember: A SUBTYPE
// specification consists of a list of entities which are direct ancestors
// (parents) of the entity containing the SUBTYPE specifiation. By recursively
// collecting the parents of each entity, the entity type hierarchy can be
// retrieved completely. In order to avoid an entry being processed more than
// once, each entry is marked with the boolean flag 'edone' after processing.
// An ABSTRACT marked entity in the symbol table is also marked abstract in
// the 'EtHierarchy' result. With this step, later accesses of elements in the
// symbol table can be avoided.
EtHierarchy ethcollect (Symtab &st);

// For each entity, embed the entity hierarhies of all of its subtypes
// (recursively) in a way which makes all of its subtypes directly visible
// in this entity.
void ethembed (EtHierarchy &eth);

// A filter function which allows entity identifiers being selected from an
// 'EthHierarchy'-argument with a predicate which is (also) given as argument.
// I don't know yet if this function is really required or not. In the latter
// case, i will remove it later ...
Fifo<Ident> filter_entities (EtHierarchy &eth,
			     bool (*pred) (const EthElement &));

// Specific filters implemented with the 'filter_entities()' function above.
Fifo<Ident> get_leaf_entities (EtHierarchy &eth);

Fifo<Ident> get_multisubtype_entities (EtHierarchy &eth);

Fifo<Ident> get_singlesubtype_entities (EtHierarchy &eth);

using RevHierarchy = std::map<Ident, std::set<Ident>>;

RevHierarchy get_revhierarchy (Symtab &st);

// Get the symbols of the top-level ENTITY types from the 'RevHierarchy &'
// argument, either all (no 'Ident' argument), or for a specific entry in
// the hierarchy (with the 'Ident' argument).
std::set<Ident> get_roottypes (const RevHierarchy &rh);
std::set<Ident> get_roottypes (const RevHierarchy &rh, Ident id);

// Get the symbols from the 'RevHierarchy &' argument which are referring
// to secondary-level ENTITY types.
std::set<Ident> get_subtypes (const RevHierarchy &rh);

ostream &dump_roottypes (Symtab &st, const std::set<Ident> &roottypes,
			 std::ostream &out);

} /*namespace CodeGen::DB*/

#endif /*DB_UTILS_H*/
