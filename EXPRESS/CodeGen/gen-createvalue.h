/*! \file gen-createvalue.h
**  Part of the plugin code generator.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Interface file for 'gen-createvalue.cc'
** (Generate the generator for (non-ENTITY) EXPRESS values (or, rather their
**  C++ counterparts).)
**
*/
#ifndef GEN_CREATEVALUE_H
#define GEN_CREATEVALUE_H

#include <fstream>
#include <string>
#include <utility>

#include "symtab.h"
#include "gen-typedeps.h"

void gen_createvalue (Symtab &st, const TypeIndexList &til,
		      const std::string &fnpfx, const std::string &hdeffile);

#endif /*GEN_CREATEVALUE_H*/
