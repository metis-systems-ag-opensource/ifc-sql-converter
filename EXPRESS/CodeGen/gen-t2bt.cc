/* gen-t2bt.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generate tables for associating each EXPRESS type of STEP with its
** base type.
**
*/

#include <iostream>
using std::cerr, std::cout, std::endl;

#include <fstream>
#include <set>
#include <stdexcept>
#include <tuple>
#include <vector>

#include "Common/fifo.h"
#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"
#include "db-utils.h"
#include "myassert.h"
#include "outstrings.h"

#include "gen-t2bt.h"

//namespace CodeGen::CPP {

using std::tuple;
using std::runtime_error;
using std::string, std::to_string;

using CodeGen::DB::RevHierarchy, CodeGen::DB::get_roottypes;
using CodeGen::DB::get_revhierarchy;

// The 'get_basetype()' version of the C++ code generator. It is very similar
// to the version of the DB code generator, but the symbol table and reverse
// hierarchy structure are direct arguments instead of via the 'GenCtx'
// structure and it returns only the 'Ident' value of the base type.
static Ident get_basetype (Symtab &st, RevHierarchy &rh, Ident id)
{
    TypeStruct *ts = st[id];
    TypeStruct::Type tstype = ts->type();
    switch (tstype) {
	case TypeStruct::boolean: { return st["BOOLEAN"]; }
	case TypeStruct::logical: { return st["LOGICAL"]; }
	case TypeStruct::integer: { return st["INTEGER"]; }
	case TypeStruct::real:    { return st["REAL"]; }
	case TypeStruct::number:  { return st["NUMBER"]; }
	case TypeStruct::binary:  { return st["BINARY"]; }
	case TypeStruct::string:  { return st["STRING"]; }
	case TypeStruct::defined: {
	    Ident btid = dynamic_cast<TsDefined *>(ts)->basetype;
	    return get_basetype (st, rh, btid);
	}
	case TypeStruct::entity: {
	    set<Ident> root_types (get_roottypes (rh, id));
	    size_t rt_size = root_types.size();
	    myassert (rt_size <= 1,
		      st.symname (id) + ": Can't determine the base type in a"
		      " multi-inheritance type hierarchy.");
	    Ident rtid = (rt_size == 0 ? id : *(root_types.begin()));
	    return rtid;
	}
	default: {
	    return id;
	}
    }
}

static const char *t2bt_templ =
    "/* %ifile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2020, Metis AG" EOL
    "**" EOL
    "** Table(s) for a fast determination of the base type of a given type." EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE." EOL
    "**" EOL
    "*/" EOL
    "#ifndef %IFILE" EOL
    "#define %IFILE" EOL
    "" EOL
    "#define t2bt_size(t) (sizeof(t2bt) / sizeof(*t2bt))" EOL
    "static const ExpTypes t2bt[] = {" EOL
    "%basetypes" EOL
    "};" EOL
    "" EOL
    "#endif /*%IFILE*/" EOL
;

void gen_t2bt (Symtab &st, const TypeIndexList &til, const string outdir)
{
    string ifile ("type2basetype.cc");
    string ipath (outdir / ifile);
    string macname (trans_filename (ifile));
    RevHierarchy rh = get_revhierarchy (st);
    ofstream ifh;

    Substitutes vars {
	{ "ifile", ifile },
	{ "IFILE", trans_filename (ifile) },
    };

    // Collect the base types for each type in 'til'.
    Fifo<string> outlist;
    for (auto [id, ix]: til) {
	Ident btid = get_basetype (st, rh, id);
	outlist.push ("ExpTypes::" + st.symname (btid));
    }
    ifh.open (ipath);
    if (! ifh.is_open()) {
	throw runtime_error ("gen_t2bt(): Opening output file failed.");
    }
    vars["basetypes"] = gen_strlist (outlist, ",");
    ifh << tmpl_subst (t2bt_templ, vars);
    ifh.close();
}

//} /*namespace CodeGen::CPP*/
