/*! \file gen-t2bt.h
**  Part of the plugin code generator.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
** Definition part of 'gen-t2bt.cc'
** (Generate tables for associating each EXPRESS type of STEP with its
**  base type.)
**
*/
#ifndef GEN_T2BT_H
#define GEN_T2BT_H

#include <string>

#include "gen-typedeps.h"
#include "symtab.h"

//namespace CodeGen::CPP {

void gen_t2bt (Symtab &st, const TypeIndexList &til, const std::string outdir);

//} /*namespace CodeGen::CPP*/

#endif /*GEN_T2BT_H*/
