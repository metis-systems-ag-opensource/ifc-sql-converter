/* gen-seltypelist.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generating the 'type lists' for validating assignments to a attributes of
** SELECT types.
**
*/

#include <iostream>
using std::cerr, std::endl, std::flush;

#include <fstream>
#include <utility>

#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "outstrings.h"

#include "gen-seltypelist.h"

using std::ofstream, std::string, std::to_string, std::move;

static const char *implhead =
    "/* %ifile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, 2020, Metis AG" EOL
    "** " EOL
    "** Lists of the valid types of SELECT types" EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE" EOL
    "**" EOL
    "*/" EOL
    "#ifndef %IFILE" EOL
    "#define %IFILE" EOL
    "" EOL
;

static const char *implfoot =
    "" EOL
    "#endif /*%IFILE*/" EOL
;

static const char *seltypelist =
    "" EOL
    "// The list (of lists) of the valid select type elements" EOL
    "static ExpTypes seltypelist[] = {" EOL
    "%seltypelist" EOL
    "};" EOL
;

static const char *seltypeindexes =
    "" EOL
    "// The index list whose elements point into 'seltypelist'. Any sub-list"
    " of" EOL
    "// seltypelist is extracted through a given index and the difference"
    " between" EOL
    "// two subsequent entries, the first one being the value the index points"
    " to." EOL
    "static %unsigned_type seltypeindexes[] = {" EOL
    "%selindexes" EOL
    "};" EOL
;

static const char *seltypes =
    "" EOL
    "// The type tags of the SELECT types being used for finding and"
    " extracting" EOL
    "// the corresponding type lists." EOL
    "ExpTypes _seltypes[] = {" EOL
    "%seltypes" EOL
    "};" EOL
;

static TypeIndexList extract_selects (Symtab &st, const TypeIndexList &til)
{
    TypeIndexList res;
    size_t reslen = 0;

    // Calculate the length of the resulting "list"
    for (auto [id, ix]: til) {
	TypeStruct *ts = st[id];
	if (ts->type() == TypeStruct::select) { ++reslen; }
    }

    // "Allocate" enough memory for the result
    res.reserve (reslen);

    // Fill the result with the id/index pairs of the SELECT types.
    for (auto [id, ix]: til) {
	TypeStruct *ts = st[id];
	if (ts->type() == TypeStruct::select) {
	    res.push_back (make_pair (id, ix));
	}
    }

    // Sort the elements of the result by the "index" (not the 'id'), because
    // this allows for a faster search later ...
    sort (res.begin(), res.end(),
	  [] (const TypeIndexPair &l, const TypeIndexPair &r) {
	      return l.second < r.second;
	  });
    return res;
}

static inline void setmax (size_t &max, size_t newval)
{
    if (newval > max) { max = newval; }
}

void gen_seltypelist (Symtab &st, const TypeIndexList &til, const string fnpfx)
{
    ofstream iout;

    string filename = "seltypelist.cc";
    string hdmacname = trans_filename (filename);
    string filepath = fnpfx / filename;

    size_t valmax;

    Substitutes vars {
	{ "ifile", filename },
	{ "IFILE", hdmacname }
    };

    // Extract the SELECT types from the TypeIndexList 'til'.
    TypeIndexList selects = extract_selects (st, til);

    vector<Ident> typelist;
    Fifo<uint32_t> indexes;
    // Don't need an extra list for the SELECT types, as 'selects' already
    // contains them in the correct order

    size_t ix = 0;
    for (auto [id, xx]: selects) {
	auto sts = dynamic_cast<TsSelect *>(st[id]);
	// Assuming here that sts is _not_ NULL, as 'extract_selects()' already
	// enforced this
	vector<Ident> seltl;
	seltl.reserve (sts->flatTypes.length());
	for (auto id: sts->flatTypes) { seltl.push_back (id); }
//	sort (seltl.begin(), seltl.end(),
//	      [] (Ident l, Ident r) { return l < r; });
	typelist.reserve (typelist.size() + seltl.size());
	typelist.insert (typelist.end(), seltl.begin(), seltl.end());
	indexes.push (ix); ix = typelist.size();
    }
    // Append the additional index for making the length calculation of each
    // sub-list somewhat easier.
    indexes.push (ix);

    iout.open (filepath);
    if (! iout.is_open()) {
	throw runtime_error ("Write attempt to '" + filepath + "' failed.");
    }

    // Write the prefix ('implhead') to the output file - after substituting
    // the placeholders with the corresponding elements of 'vars'.
    iout << tmpl_subst (implhead, vars);

    // Generate the list of type lists of valid types and insert its value
    // under the name 'seltypelist' into 'vars'.
    Fifo<string> outlist;
    for (auto id: typelist) { outlist.push ("ExpTypes::" + st.symname (id)); }
    vars["seltypelist"] = gen_strlist (outlist, ",");

    // Generate the list of type lists, using the corresponding template and
    // 'vars' and write it to the file.
    iout << tmpl_subst (seltypelist, vars);

    // Same actions as for the list of type lists, but for the index list ...
    outlist.clear(); vars.erase ("seltypelist");
    valmax = 0;
    for (auto ix: indexes) {
	setmax (valmax, ix);
	outlist.push (to_string (ix));
    }
    vars["unsigned_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["selindexes"] = gen_strlist (outlist, ",");

    iout << tmpl_subst (seltypeindexes, vars);

    // Same actions as for the list of type lists, but for the (sorted) list
    // of the SELECT types.
    outlist.clear(); vars.erase ("selindexes");
    for (auto [id, ix]: selects) {
	outlist.push ("ExpTypes::" + st.symname (id));
    }
    vars["seltypes"] = gen_strlist (outlist, ",");

    iout << tmpl_subst (seltypes, vars);

    outlist.clear(); vars.erase ("seltypes");

    // Write the code suffix to the output file.
    iout << tmpl_subst (implfoot, vars);

    // Finished.
    iout.close();
}
