/*! \file gen-typedeps.h
**  Part of the plugin code generator.
**
**  Helper module for generating the type dependencies (hierarchies). This
**  module doesn't generate plugin code, but is used by several modules which
**  use information collected by this module for generating the code.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'gen-typedeps.cc'
** (Generate the lists of the type dependencies (hierarchies). This list is
**  used for determining if an object of some type is assignment compatible
**  to some other type. The generated lists no refer the symbol table, which
**  is not part of the generated module, but are used for generating static
**  tables which then are used for the conversion of STEP-objects into objects
**  of the generated type(s).)
**
*/
#ifndef GEN_TYPEDEPS_H
#define GEN_TYPEDEPS_H

#include <vector>
#include <map>

#include "identbitset.h"
#include "symtab.h"

using ExpIndex = uint32_t;
using TypeIndexMap = std::map<Ident, ExpIndex>;
using IndexTypeMap = std::map<ExpIndex, Ident>;
using ValidTypes = std::vector<ExpIndex>;
using ValidLists = std::vector<ValidTypes>;
using Entities = BitSet<ExpIndex, 0>;
using TypeIndexPair = std::pair<Ident, ExpIndex>;
using TypeIndexList = std::vector<TypeIndexPair>;

TypeIndexList gen_typeindexassoc (Symtab &st);

TypeIndexList get_entities (Symtab &st, const TypeIndexList &til);

std::pair<ValidLists, Entities> gen_hierlists (Symtab &st, TypeIndexList &til);

TypeIndexList depsort_types (Symtab &st, const TypeIndexList &til);

#endif /*GEN_TYPEDEPS_H*/
