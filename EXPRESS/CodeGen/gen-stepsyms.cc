/* gen-stepsyms.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Generate tables with the step symbols and the corresponding tags (and
** later: object generators). Background is that there only a fixed number of
** different symbol types, which can be stored in static tables. The symbol
** names will be stored in one long C-string in the generated code.
**
*/

#if 1 // Only for debugging purposes
#include <cstdlib>
#include <iostream>
using std::cerr, std::cout, std::endl;
#endif

#include <cstring>
#include <stdexcept>
#include <string>
#include <utility>
#include <algorithm>
#include <fstream>

#include "Common/fifo.h"
#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "outstrings.h"

#include "gen-stepsyms.h"

using std::string, std::to_string;

using RefNamePair = pair<unsigned, const string *>;

static const char *exptypes_tmpl =
    "/* %exptfile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2020, Metis AG" EOL
    "**" EOL
    "** Type definition of the express type tags (as an 'enum class' type)." EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE" EOL
    "**" EOL
    "*/" EOL
    "#ifndef %EXPTFILE" EOL
    "#define %EXPTFILE" EOL
    "" EOL
    "#include <cstdint>" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "enum class ExpTypes : %unsigned_type {" EOL
    "%EXPTYPES" EOL
    "} /*ExpTypes*/;" EOL
    "" EOL
    "} /*namespace IFC*/" EOL
    "" EOL
    "#endif /*%EXPTFILE*/" EOL
;

static const char *symbols_head =
    "/* %symsfile" EOL
    "**" EOL
    "** Symbols being included into the STEP interface file (via" EOL
    "** #include \"%symbols\")" EOL
    "** Copyright: (c) 2019, METIS AG" EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE" EOL
    "**" EOL
    "*/" EOL
    "#ifndef %SYMSFILE" EOL
    "#define %SYMSFILE" EOL
    "" EOL
    "const size_t _symsend = %symsend;" EOL
    "" EOL
    "const char *ExpressSchema = \"%schema\";" EOL
    "" EOL
;
static const char *symbols_foot =
    "" EOL
    "#endif /*%SYMSFILE*/" EOL
;


// The model used here is very C-like. The reason is the reduction of memory
// usage to a minimum. Instead of creating a _constant_ table of 'std::string',
// a static table of 'char' (a string list) with two index tables is generated,
// the first one for creating the references (pointers) to the names within the
// string list (a string reference table), the second one for allowing an
// alphabetically "sorted" access to the strings in the string list (through
// the reference table). So, for an "unsorted" access to the string list (e.g.
// via the also generated enum type), the following expression can be used:
//
//   string_list + string_refs[(size_t) «ExpType value»]
//
// For a "sorted" access to the string list (during a binary search), the
// expression
//
//   string_list + string_refs[srefix[«index value»]]
//
// is used. Together with a table containing the valid type tags ('ExpType'
// values), getting the type tag from a type name as well as converting a
// type tag into its string representation is made possible by these tables.
//
// In a later step, a table of functions for converting values (instances) from
// the STEP-parser into 'ExpBase *'-derived values will be added.

// Generate the 'ExpTypes' enumeration as well as the interface to the STEP
// parser.
void gen_stepsyms (Symtab &st, const TypeIndexList &til, const string &outdir,
		   const string &exptypes, const string &symbols)
{
    const string &schema = st.schema();
    ofstream expf, symf;
    vector<unsigned> refs, srefix;
    vector<const string *> names;
    vector<string> ucnames;

    string expttags;
    //string lcschema = lowercase (schema);
    //string fnbase = fnprefix / lcschema;
    string exptpath = outdir / exptypes + ".h";
    string symspath = outdir / symbols + ".cc";
//    string fnprefix = outdir / stepif;
//    string defnpath = fnprefix + ".h", implpath = fnprefix + ".cc";
    string exptfile = basename (exptpath);
    string symsfile = basename (symspath);
//    string hfile = basename (defnpath), ifile = basename (implpath);
    Substitutes vars {
	{ "exptfile", exptfile },
	{ "EXPTFILE", trans_filename (exptfile) },
	{ "symsfile", symsfile },
	{ "SYMSFILE", trans_filename (symsfile) },
//	{ "hfile", hfile },
//	{ "HFILE", trans_filename (hfile) },
//	{ "ifile", ifile },
	{ "schema", schema },
	{ "ucfschema", ucfirst (schema) },
    };

    // The index for the next element in the string list 'sindex' and the
    // index into the string index table to be used for filling 'srefix'
    // - before sorting - 'rindex'.
    size_t sindex = 0, rindex = 0;

    expf.open (exptpath);
    if (! expf.is_open()) {
	throw runtime_error ("Write attempt to '" + exptpath + "' failed.");
    }
    
    symf.open (symspath);
    if (! symf.is_open()) {
	throw runtime_error ("Write attempt to '" + symspath + "' failed.");
    }
    
//    outh.open (defnpath);
//    if (! outh.is_open()) {
//	throw runtime_error ("Write attempt to '" + defnpath + "' failed.");
//    }

//    outi.open (implpath);
//    if (! outi.is_open()) {
//	throw runtime_error ("Write attempt to '" + implpath + "' failed.");
//    }
    // Get the number of entries in the "types" table and reserve enough space
    // for this number of entries in the tables 'names' (pointers to the names
    // (strings) of the types), 'refs' indexes into the name list and 'srefix'
    // (a table if indexes  into the table 'refs' which is sorted by the names
    // this table 'refs' refers to). The latter table is used, because it is
    // important to keep the direct order of the type tags (keeping this order
    // is required somewhere else).
    size_t tilsize = til.size();
    names.reserve (tilsize);
    refs.reserve (tilsize);
    srefix.reserve (tilsize);	// Sorted REFerence IndeX (srefix)
    uint32_t maxx = 0;
    for (auto [id, ix]: til) {
	const string &name = st.symname (id);
	//const string *name = ea.first;
	// Put the name (pointer/ref) into the table 'names',
	names.push_back (&name);
	// its index (position in the name list) into 'refs',
	refs.push_back (sindex);
	// set the index for the next string in the name list,
	sindex += name.size() + 1;
	// and put the index into the table 'refs' into 'srefix', advancing
	// to the next index in the same step.
	srefix.push_back (rindex++);
	if (ix > maxx) { maxx = ix; }
    }
    // Virtually add the "types" ARRAY, LIST, BAG and SET to the lists.
    Fifo<string> albs {"_InstRef", "ARRAY", "LIST", "BAG", "SET"};
    for (auto &name: albs) {
	names.push_back (&name);
	refs.push_back (sindex); sindex += name.size() + 1;
	srefix.push_back (rindex++);
    }
    // At the end of the loop, on the other hand, it contains the total size
    // of the name list, and can so be used for optimisation purposes (e.g.
    // the name reference table as 'uint16_t' instead of 'uint32_t' if the
    // whole name list is short enough (< 64K (aka 65536 bytes)). Because most
    // of this code is encoding-agnostic, this holds true even for UTF-8
    // encoded names ... which are nowhere used in IFC.

    // Now, 'srefix[]' must be sorted. I'm using a "lambda" construct as
    // comparison function, which uses references to 'names[]' - not 'refs',
    // as the latter is only used for finding the entries in the name list
    // and need not be referenced yet.
    auto refixComp = [&] (uint32_t l, uint32_t r) {
	return lccmp (*names[l], *names[r]) < 0;
    };
    sort (srefix.begin(), srefix.end(), refixComp);

    // Prepare the generation of the 'enum class ExpTypes' ...
    Fifo<string> strlist;
    for (auto [id, ix]: til) {
	const string &name = st.symname (id);
	strlist.push (name + " = " + to_string (ix));
    }
    // Insert the additional "types"
    for (auto &name: albs) {
	strlist.push (name + " = " + to_string (++maxx));
    }

    // 'maxx' is now the highest value of all elements in 'ExtTypes'.
    // This means that 'maxx + 1' should be the value of '%symsend'

    // Insert an element for the "invalid" type. This is sometimes better than
    // generating an exception. This element has no representation, so it is
    // inserted only here (into the type tags enum 'ExpTypes'). This should not
    // disturb operations on the tables generated below.
    strlist.unshift ("_NoType = 0");

    vars["symsend"] = to_string (rindex);
    vars["unsigned_type"] = (maxx + 1 < 65536 ? "uint16_t" : "uint32_t");
//    vars["symsend"] = to_string (rindex);
//    vars["unsigned_type"] = (rindex < 65536 ? "uint16_t" : "uint32_t");
    expttags = gen_strlist (strlist, ",");
    if (is_prefix (EOL, expttags)) {
	expttags.erase (0, sizeof(EOL) - 1);
    }
    vars["EXPTYPES"] = std::move (expttags);
    expf << tmpl_subst (exptypes_tmpl, vars);
    vars.erase ("EXPTYPES");

    // Now for the '.cc' (aka implementation) file. This file also starts with
    // a head part ...
    symf << tmpl_subst (symbols_head, vars);

    // The next item is the list of (unique) strings. Each of these strings
    // is NUL-terminated. Because of the special characteristics of C (and C++)
    // to concatenate two contiguous string constants into one string, the NUL-
    // terminators must be specified explicitely. Additionally, the string list
    // is defined as 'const char []', meaning the identifier ('step_namelist')
    // which refers the string list is itself treated as constant. Because this
    // string list (as well as the other definitions here) is defined for the
    // STEP parser, the strings in this list are all upper case.
    strlist.clear();
    for (auto name: names) {
	strlist.push ("\"" + uppercase (*name) + "\\0\"");
    }

    // Writing the 'step_namelist is not exactly the same as writing any other
    // list, because it has no delimiter (beside the blank delimiter, which
    // rather depends on the column counter than a 'first'-flipflop.
    symf << EOL "// Name list of the STEP symbol table"
	    EOL "const char step_namelist[] = {" EOL;
    out_strings (symf, strlist, "") <<
	    EOL "} /*step_namelist*/;" EOL;

    // 'strlist' is now filled directly from the (stringified) name 'refs'
    // instead of the "first" value of 'sorted_pairs'.
    strlist.clear(); for (auto rx: refs) { strlist.push (to_string (rx)); }

    // Write the (C-definition) of the string references to the implementation
    // file (table 'step_namerefs[]').
    symf << EOL "// Index table for the 'step_namelist' which allows for"
		" a binary search" EOL <<
	    (sindex < 65536 ? "uint16_t" : "uint32_t") <<
	    " step_namerefs[] = {" EOL;
    out_strings (symf, strlist, ",") << EOL "} /*step_namerefs*/;" EOL;

    // Write the "sorted" list of indexes into 'step_namerefs[]' to the
    // implementation table ('step_rindex[]').
    strlist.clear();
    for (auto ix: srefix) { strlist.push (to_string (ix)); }

    symf << EOL "// Reverse index table (sorted by the names in the name list"
	    EOL << (rindex < 65536 ? "uint16_t" : "uint32_t") <<
	    " step_rindex[] = {" EOL;
    out_strings (symf, strlist, ",") << EOL "} /*step_rindex*/;" EOL;

    // Last not least, write the table of type tags ('ExpTypes' values) is
    // written to the implementation file ('exp_typelist[]'). This table is
    // used for getting the result of a binary search in the 'step_namelist[]'.
    strlist.clear();
    for (auto [id, ix]: til) {
	strlist.push ("ExpTypes::" + st.symname (id));
    }
    symf << EOL "// Tag-table which associates the names in the step name"
		" table (step_namelist,"
	    EOL "// step_namerefs) with the corresponding type tags (later: an"
		" action for"
	    EOL "// generating the C++-object which implements the EXPRESS"
		" datatype)."
	    EOL "ExpTypes exp_typelist[] = {" EOL;
    out_strings (symf, strlist, ",") << EOL "} /*exp_typelist*/;" EOL;
//    outi << EOL "// Tag-table which associates the names in the step name"
//		" table (step_namelist,"
//	    EOL "// step_namerefs) with the corresponding type tags (later: an"
//		" action for"
//	    EOL "// generating the C++-object which implements the EXPRESS"
//		" datatype)."
//	    EOL "ExpTypes exp_typelist[] = {" EOL;
//    out_strings (outi, strlist, ",") << EOL "} /*exp_typelist*/;" EOL;

    // Complete the implementation file by writing the "foot" to it (generated
    // by filling the 'impl_foot' template with the required values).
    symf << tmpl_subst (symbols_foot, vars);
//    outi << tmpl_subst (impl_foot, vars);

    // Close the 'definition' and 'implementation' files, and the file
    // containing the definition of the type tags.
    symf.close(); expf.close();
//    outi.close(); outh.close(); expf.close();
}
