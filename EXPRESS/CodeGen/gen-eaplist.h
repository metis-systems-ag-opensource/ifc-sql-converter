/*! \file gen-eaplist.h
**  Part of the plugin code generator.
**
**  Code generator for the static ENTITY attribute lists.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'gen-eaplist.cc'
** (Generate 'eaplist.cc')
**
*/
#ifndef GEN_EAPLIST_H
#define GEN_EAPLIST_H

#include <string>

#include "symtab.h"
#include "gen-typedeps.h"

void gen_eaplist (Symtab &st, const TypeIndexList &til, const string fnpfx);

#endif /*GEN_EAPLIST_H*/
