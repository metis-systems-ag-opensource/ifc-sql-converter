/*! \file gen-mk.h
**  Part of the plugin code generator
**
**  Generating a `Makefile` from the data collected by the `cgen()` code
**  generator function.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Interface file for 'gen-mk.cc'
** (Generate a Makefile for compiling the generated code)
**
*/
#ifndef GEN_MK_H
#define GEN_MK_H

#include <string>

#include "symtab.h"

using ResList = Fifo<string>;
using Deps = map<string, ResList>;

void gen_make (Symtab &st, const Deps &deps, const std::string &objsfx,
	       const std::string &dllsfx, const std::string &fnpfx);

#endif /*GEN_MK_H*/
