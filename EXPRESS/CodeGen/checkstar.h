/*! \file checkstar.h
**  Code generator module which writes the positions of DERIVE attributes of
**  ENTITY types which have such attributes between explicit attributes¹
**  to either a file or the standard output stream.
**
**  ¹ Such situations occur when DERIVE attributes occur in an ENTITY when
**    one of the SUPERTYPEs of this ENTITY has DERIVE attributes, and the
**    ENTITY itself has explicit attributes.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Interface file for 'checkstar.cc'
** (Utility module which writes the positions of DERIVE attributes of entites
**  which have them to stdout, but only there are EXPLICIT attributes following
**  these DERIVE attributes.)
**
*/
#ifndef CHECKSTAR_H
#define CHECKSTAR_H

#include <string>

#include "symtab.h"

namespace CodeGen::CheckStar {

bool cgen (Symtab &st,
	   const std::string &_unused1,
	   const std::string &outfile_base);

} /*namespace CodeGen::CheckStar*/

#endif /*CHECKSTAR_H*/
