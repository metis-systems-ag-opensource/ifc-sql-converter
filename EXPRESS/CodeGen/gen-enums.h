/*! \file gen-enums.h
**  Part of the plugin code generator.
**
**  Generating the C++ type definitions for the EXPRESS `ENUMERATION` types
**  of a given IFC schema.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'gen-enums.cc'
** (Generating the ENUMERATION types)
**
*/
#ifndef GEN_ENUMS_H
#define GEN_ENUMS_H

#include <string>

#include "symtab.h"
#include "gen-typedeps.h"

void gen_enums (Symtab &st, const TypeIndexList &til,
		const std::string &fnprefix);

#endif /*GEN_ENUMS_H*/
