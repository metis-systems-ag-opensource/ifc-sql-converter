/* gen-eaplist.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Generate 'eaplist.cc'
**
*/

#include <iostream>
using std::cerr, std::endl;

#include <fstream>
#include <utility>

#include "Common/bitset.h"
#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"
#include "Common/fifo.h"
#include "Common/fifoout.h"

#include "outstrings.h"

#include "gen-eaplist.h"

#define MERGEORT 1

using std::ofstream, std::string, std::to_string, std::move;

static const char *implhead =
    "/* %ifile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, 2020, Metis AG" EOL
    "**" EOL
    "** Property lists (tables) of the ENTITY types" EOL
    "** AUTOMATICALLY GENERATED FILE - modify at your own risk." EOL
    "**" EOL
    "*/" EOL
    "#ifndef %IFILE" EOL
    "#define %IFILE" EOL
    "" EOL
;

static const char *implfoot =
    "" EOL
    "#endif /*%IFILE*/" EOL
;

static const char *eaplist_tmpl =
    "" EOL
    "// DERIVE and OPTIONAL properties of the different ENTITY types." EOL
    "// Each element consists of:" EOL
    "//   - The index of the corresponding attribute of a given ENTITY"
    " type," EOL
    "//   - the index of the first element in the \"valid\" types list for" EOL
    "//     this attribute (in 'eapvalidtypes[]')," EOL
    "//   - the index of the last element in the \"valid\" types (+ 1)," EOL
    "//   - a flag which says if the given attribute is a DERIVE attribute," EOL
    "//   - a flag which specifies this attribute as optional ('true') or" EOL
    "//     mandatory ('false')." EOL
    "// SIZE: %tblsize entries" EOL
    "static EAProperties eaplist[] = {" EOL
    "%EAPELEMENTS" EOL
    "};" EOL
;

static const char *eapindex_tmpl =
    "" EOL
    "// Index for each \"row\" in the 'eaplist'. The element got via a"
    " binary" EOL
    "// search in 'eaptags[]' is the index of the first attribute of a"
    " given" EOL
    "// ENTITY in 'eaplist[]', the following element then is the index"
    " of the" EOL
    "// last attribute (+ 1)." EOL
    "// SIZE: %tblsize entries" EOL
    "static %unsigned_type eapindex[] = {" EOL
    "%EAPINDEXES" EOL
    "};" EOL
;

static const char *eaptags_tmpl =
    "" EOL
    "// List of ENTITY type-tags (used for determining the correct \"row\""
    " from the" EOL
    "// 'eapindex[]' table)." EOL
    "// SIZE: %tblsize entries" EOL
    "static ExpTypes eaptags[] = {" EOL
    "%EAPTAGS" EOL
    "};" EOL
;

static const char *revindex_tmpl =
    "" EOL
    "// Reverse index => Index from the 'EntityBase' attribute list to the" EOL
    "// index of the original 'STEP::EntityValue' attribute value list." EOL
    "// SIZE: %tblsize entries" EOL
    "static %unsigned_type eaprevindex[] = {" EOL
    "%EAPREVINDEXES" EOL
    "};" EOL
;

static const char *revindexindex_tmpl =
    "" EOL
    "// Entity index into the reverse index" EOL
    "// SIZE: %tblsize entries" EOL
    "static %unsigned_type eaprevindexindex[] = {" EOL
    "%EAPREVINDEXINDEXES" EOL
    "};" EOL
;

static const char *vtypes_tmpl =
    "" EOL
    "// List of valid types (base types only) for each attribute of each" EOL
    "// ENTITY" EOL
    "// SIZE: %tblsize entries" EOL
    "static ExpTypes eapvalidtypes[] = {" EOL
    "%EAPVALIDTYPES" EOL
    "};" EOL
;

static const char *supertypes_tmpl =
    "" EOL
    "// List of supertypes of an ENTITY type" EOL
    "// SIZE: %tblsize entries" EOL
    "static ExpTypes supertypes[] = {" EOL
    "%SUPERTYPES" EOL
    "};" EOL
;

static const char *superindexes_tmpl =
    "" EOL
    "// List of indexes into the 'supertypes' table" EOL
    "// SIZE: %tblsize entries" EOL
    "static %unsigned_type supertype_indexes[] = {" EOL
    "%SUPERINDEXES" EOL
    "};" EOL
;

struct SingleAttr { uint32_t ix; bool derive, optional; };

struct EntAttrs {
    Ident tid; uint32_t base_index;
    map<Ident, SingleAttr> attrspecs;
};

template<typename KeyT, typename ValT>
bool has (const map<KeyT, ValT> &kv, const KeyT &k)
{
    return kv.count (k) > 0;
}

static inline void setmax (size_t &max, size_t newval)
{
    if (newval > max) { max = newval; }
}

static Fifo<Ident> get_vtypes (Symtab &st, Ident tid, TypeStruct *ts);
static Fifo<Ident> get_supertypes (Symtab &st, Ident tid);

// 'gen_eaplist (st, til, fnpfx)'
//    generiert eine '#include'-Datei (allerdings keine Header-Datei), die
//    Metadaten zu den Attributen der jeweiligen ENTITY-Datentypen enthält
//    (z.Zt. allerdings nur explizite und DERIVE-Attribute). Diese Attribute
//    bestehen aus dem Index des Attributes in dem Attribut-Array einer
//    generierten Instanz, sowie um Flags, die anzeigen, ob das jeweilige
//    Attribut optional ist (Speicherung eines leeren Wertes möglich), bzw.
//    ob es sich um ein DERIVE-Attribut handelt; letztere werden überhaupt
//    nicht im Attribut-Array gespeichert. Die generierten Tabellen sollen
//    bei ser Implementierung der "setter"-Funktion des jeweiligen C++-Objektes
//    helfen.
//    Da es sich bei diesen Metadaten um _statische_ Daten handelt, sind
//    C-Arrays für diesen Zweck durchaus angemessen. Es werden drei C-Arrays
//    generiert:
//      - Das Array mit den tatsächlichen Metadaten (Index, DERIVE-Flag,
//        OPTIONAL-Flag),
//      - ein Index-Array mit den Anfangspositionen der jeweiligen in den
//        Metadaten gespeicherten Teile (Segmente), die die Attribute der
//        jeweiligen ENTITY beschreiben (dieses Array enthält einen
//        zusätzlichen Eintrag, der an das Ende des Metadaten-Arrays zeigt,
//        damit die Ende-Positionen der jeweiligen Segmente einheitlich
//        bestimmt werden können),
//      - Ein Array, das die 'ExpTypes'-Werte der gespeicherten ENTITY-Typen
//        enthält. Da diese die gleiche Sortierung haben sollten, wie die
//        Einträge in der 'TypeIndexList' ('til'), kann dann (in dem Modul
//        'chelpers.{cc,h}') eine einfache binäre Suche zur Bestimmung dieser
//        Metadaten verwendet werden.
//
// Die Ergebnisdatei wird aus den oben definierten Template-Strings (durch
// Substitution der jeweiligen Platzhalter ('%name') generiert.
//
void gen_eaplist (Symtab &st, const TypeIndexList &til, const string fnpfx)
{
    ofstream iout;

    string filename = "eaplist.cc";
    string hdmacname = trans_filename (filename);
    string filepath = fnpfx / filename;
    size_t valmax;

    Substitutes vars {
	{ "ifile", filename },
	{ "IFILE", hdmacname }
    };

    iout.open (filepath);
    if (! iout.is_open()) {
	throw runtime_error ("Write attempt to '" + filepath + "' failed.");
    }

    iout << tmpl_subst (implhead, vars);

    struct EAProp {
	int32_t mx;
	Ident eid, aid;
	bool derived, optional;
	uint32_t typelist_begin, typelist_end;
    };
    using EAPlist = Fifo<EAProp>;
    using EAPtypes = Fifo<Ident>;
    using EAPindexes = Fifo<uint32_t>;
    uint32_t list_first = 0, suix = 0;
    // List of the Entity Attributes' Properties
    EAPlist eaplist;
    // List of the Entity types (symtab entry identifiers)
    EAPtypes types, supertypes;
    // Index of the beginning of the attribute property list of a specific
    // ENTITY
    EAPindexes eapindex, revindex, entrevindexindex, superindex;

    Fifo<Ident> vtypes;
    for (auto [id, ix]: til) {
	auto ts = dynamic_cast<TsEntity *>(st[id]);
	if (ts) {
	    superindex.push (suix);
	    supertypes.push (get_supertypes (st, id));
	    suix = supertypes.size();
	    eapindex.push (list_first);
	    types.push (id);
	    // 'mx' is the attribute entry in the 'attributes' vector in the
	    // resulting 'EntityBase' derived type, whereas 'rx (short for
	    // 'reverse index') is the index in the original STEP::EntityValue
	    // structure. The latter one is required for correctly identifying
	    // the matching 'EAProp' entry, and thus the list of valid types an
	    // _explicit_ attribut can have. It is necessary, because the type
	    // checking for entity instance references cannot be done during
	    // the conversion of a STEP value into an instance, as the
	    // corresponding attribute may refer to an instance which was not
	    // yet parsed and converted, which makes a second pass necessary
	    // for type checking the entity attributes which refer to other
	    // entity instances. Because of the original attribute indexes
	    // being unavailable in this second pass, a translation table must
	    // be introduced which translates between the indexes of the
	    // entries in the 'attributes' vector of the 'EntityBase' derived
	    // types and the indexes of the explicit attributes in the
	    // 'eaplist'.
	    // Last not least, 'rxbase' identifies the beginning of the
	    // entries in this translation table for each 'EntityBase' derived
	    // type (and is stored in 'entrevindexindex').
	    int mx = 0, rx = 0;

	    // Push the beginning of a new segment of the 'revindex'
	    // translation table in 'entrevindexindex'. There is no end index
	    // required for this table, as each ENTITY has a fixed number of
	    // attributes and can access this table only through the indexes
	    // of these attributes.
	    entrevindexindex.push (revindex.size());
	    for (auto &flAttr: ts->flatAttributes) {
		// Single attribute property
		EAProp eaprop { 0,
				id/*flAttr.entid*/, flAttr.attrid,
				false, false,
				0, 0
			      };
		switch (flAttr.attr->type()) {
		    case Attr::explicit_attr: {
			auto attr = dynamic_cast<ExplicitAttr *>(flAttr.attr);
			eaprop.optional = attr->optional;
			// Add the index of the current entry of the 'eaplist'
			// to 'revindex'. (The table generated from this value
			// will later be used for addressing the correct
			// entity attribute property ...
			revindex.push (rx + list_first);
			// Only here, i have a list of valid types, as derived
			// attributes are calculated, and thus don't need such
			// a list.
			// Keep the index of the first entry of the valid
			// types list of this explicit attribute in its
			// 'typelist_begin' attribute.
			eaprop.typelist_begin = vtypes.size();
			// Append the valid types for this attribute (only the
			// base types - even the base types fo the structures)
			// to the valid types list.
			vtypes.push (get_vtypes (st, 0,
						 attr->definition.get()));
			// Keep the index behind the last entry of the valid
			// types list in the 'typelist_end' attribute in this
			// 'EAProp' structure.
			eaprop.typelist_end = vtypes.size();
			eaprop.mx = mx++;
			break;
		    }
		    case Attr::derived_attr: {
			eaprop.derived = true;
			// Empty valid types list for DERIVE attributes
			eaprop.typelist_begin = eaprop.typelist_end = 0;
			eaprop.mx = -1;
			break;
		    }
		    default:
			continue;
		}
		++rx;
		eaplist.push (eaprop);
	    }
	    list_first = eaplist.size();
	}
    }
    // Additional entry in the index list for an easier determination of the
    // end of _all_ list segments.
    eapindex.push (list_first);
    superindex.push (suix);

    Fifo<string> outlist;

    // Insert the 'eaplist[]' table into the output file. This table contains
    // simple property descriptions for each explicit and DERIVE attribute
    // in each ENTITY type.
    for (auto [mx, eid, aid, derived, optional, tlbegin, tlend]: eaplist) {
	string el = "{ " + to_string (mx) + ", " +
		    to_string (tlbegin) + ", " + to_string (tlend) + ", " +
		    (derived ? "true" : "false") + ", " +
		    (optional ? "true" : "false") + "}"
		    " /*" + st.symname (eid) + "." + st.symname (aid) + "*/";
	outlist.push (move (el));
    }
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("EAPELEMENTS", gen_strlist (outlist, ",")));
    iout << tmpl_subst (eaplist_tmpl, vars);

    // Reset 'outlist' and "EAPELEMENTS" in 'vars' (reducing memory consumption
    // within the parser).
    outlist.clear(); vars.erase ("EAPELEMENTS");

    // Insert the 'eapindex[]' table into the output file. This table describes
    // the begin and end of each attribute property list within 'eaplist[]'
    // – using the element at a given index as the index of the begin of the
    // attribute properties of a given ENTITY type and the difference between
    // the element at the given index + 1 and the element at the given index
    // as its length.
    valmax = 0;
    for (uint32_t index: eapindex) {
	setmax (valmax, index);
	outlist.push (to_string (index));
    }
    vars["unsigned_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("EAPINDEXES", gen_strlist (outlist, ",")));
    iout << tmpl_subst (eapindex_tmpl, vars);

    // Reset 'outlist' and "EAPINDEXES" in 'vars' (reducing memory consumption
    // within the parser).
    outlist.clear(); vars.erase ("EAPINDEXES");

    // The table 'eaptags[]' created here contains the 'ExpTypes' tags of all
    // existing ENITTY types. It is used (via binary search) for determining
    // the index to be used in 'eapindex[]' from the corresponding type tag.
    for (Ident tid: types) { outlist.push ("ExpTypes::" + st.symname (tid)); }
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("EAPTAGS", gen_strlist (outlist, ",")));
    iout << tmpl_subst (eaptags_tmpl, vars);

    // Reset 'outlist' and "EAPTAGS" in 'vars' (reducing memory consumption
    // within the parser).
    outlist.clear(); vars.erase ("EAPTAGS");

    // The reverse index is required for determining the attribute property of
    // an explicit attribute without knowing the original index as used in the
    // STEP value. This table, together with the following one form a mechanism
    // for converting the index of an attribute in an EXPRESS-object into the
    // position of the 'eaplist[]' entry of this attribute.
    valmax = 0;
    for (uint32_t ix: revindex) {
	setmax (valmax, ix);
	outlist.push (to_string (ix));
    }
    vars["unsigned_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("EAPREVINDEXES", gen_strlist (outlist, ",")));
    iout << tmpl_subst (revindex_tmpl, vars);

    // Reset 'outlist' and "EAPREVINDEXES" in 'vars' (reducing memory
    // consumption within the parser).
    outlist.clear(); vars.erase ("EAPREVINDEXES");

    // Inserting the table 'revindexindex[]' for referring to elements in
    // 'revindex[]'
    valmax = 0;
    for (uint32_t ix: entrevindexindex) {
	setmax (valmax, ix);
	outlist.push (to_string (ix));
    }
    vars["unsigned_type"] = (valmax >= 65536 ? "uint32_t" : "uint16_t");
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("EAPREVINDEXINDEXES", gen_strlist (outlist, ",")));
    iout << tmpl_subst (revindexindex_tmpl, vars);

    // Reset 'outlist' and "EAPREVINDEXINDEXES" in 'vars' (reducing memory
    // consumption within the parser).
    outlist.clear(); vars.erase ("EAPREVINDEXINDEXES");

    // The next table specifies the valid types lists for each explicit
    // attribute of each (non-abstract) ENTITY. It is addressed by the
    // entries in the 'eaplist[]'.
    for (Ident id: vtypes) { outlist.push ("ExpTypes::" + st.symname (id)); }
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("EAPVALIDTYPES", gen_strlist (outlist, ",")));
    iout << tmpl_subst (vtypes_tmpl, vars);

    // Reset 'outlist' and "EAPVALIDTYPES" in 'vars' (reducing memory
    // consumption within the parser).
    outlist.clear(); vars.erase ("EAPVALIDTYPES");

    // For determining the correct database attribute of a SELECT table entry,
    // a further pair of tables is required. The first one ('supertypes[]')
    // specifies lists of all SUPERTYPEs of each ENTITY type.
    for (Ident id: supertypes) {
	outlist.push ("ExpTypes::" + st.symname (id));
    }
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("SUPERTYPES", gen_strlist (outlist, ",")));
    iout << tmpl_subst (supertypes_tmpl, vars);

    outlist.clear(); vars.erase ("SUPERTYPES");

    // The second table ('superindexes[]') is again an index (selected by the
    // corresponding type tag in 'eaptags[]' for a given ENTITY type),
    // referring the first entry of the list of valid types in the list of
    // SUPERTYPEs for a given ENTITY type. The difference of this entry's
    // index and the entry index of the following ENTITY type is the length
    // of this list.
    for (uint32_t ix: superindex) { outlist.push (to_string (ix)); }
    vars["unsigned_type"] =
	(superindex.last() > 65535 ? "uint32_t" : "uint16_t");
    vars["tblsize"] = to_string (outlist.length());
    vars.insert (make_pair ("SUPERINDEXES", gen_strlist (outlist, ",")));
    iout << tmpl_subst (superindexes_tmpl, vars);

    // Last 'outlist.clear()' and 'vars.erase()' operation.
    outlist.clear(); vars.erase ("SUPERINDEXES");

    // Insert the footer into the output file, and then release all template
    // varibles and 'close()' the i/o channel
    iout << tmpl_subst (implfoot, vars); vars.clear();
    iout.close();
}

static Ident type2id (Symtab &st, TypeStruct::Type t)
{
    switch (t) {
	case TypeStruct::boolean: return st["BOOLEAN"];
	case TypeStruct::logical: return st["LOGICAL"];
	case TypeStruct::integer: return st["INTEGER"];
	case TypeStruct::real: return st["REAL"];
	case TypeStruct::number: return st["NUMBER"];
	case TypeStruct::binary: return st["BINARY"];
	case TypeStruct::string: return st["STRING"];
	default: return 0;
    }
}

// Get the type of an ENTITY attribute. This is either an aggregate (ARRAY,
// LIST, BAG, SET) of something, or a type reference (where the "something"
// is again an aggregate of something or a type reference).
static Ident get_paramtype (Symtab &st, TypeStruct *ts)
{
    TypeStruct::Type tstype = ts->type();
    switch (tstype) {
	case TypeStruct::array: case TypeStruct::list:
	case TypeStruct::bag: case TypeStruct::set: {
	    TypeStruct *bt = dynamic_cast<TsAggregateLike *>(ts)->basetype;
	    return get_paramtype (st, bt);
	}
	case TypeStruct::typeref: {
	    return dynamic_cast<TsTypeRef *>(ts)->id;
	}
	// Old scheme: using simple types as direct targets (instead of their
	// symbol table entries). In this case, the symbol corresponding idents
	// (symbol table indexes) must be returned, which is done by searching
	// these types names in the symbol table. (These entries are created
	// even before the parser itself starts to work.)
	default: return type2id (st, tstype);
    }
}

static Fifo<Ident> get_entvtypes (Symtab &st, Ident entid, TsEntity *ts)
{
    Fifo<Ident> res;
    if (! ts->abstract) { res.push (entid); }
    for (auto subtid: ts->mysubtypes) {
	auto subtts = dynamic_cast<TsEntity *>(st[subtid]);
	if (! subtts) {
	    //ERROR!
	}
	res.push (get_entvtypes (st, subtid, subtts));
    }
    return res;
}

static Fifo<Ident> get_vtypes1 (Symtab &st, Ident tid, TypeStruct *ts)
{
    Fifo<Ident> res;
    if (dynamic_cast<TsStringLike *>(ts)) {
	 tid = type2id (st, ts->type());
    } else if (dynamic_cast<TsSimple *>(ts)) {
	if (tid == 0) { tid = type2id (st, ts->type()); }
	res.push (tid);
    } else if (auto btts = dynamic_cast<TsAggregateLike *>(ts); btts) {
	res.push (get_vtypes1 (st, tid, btts->basetype));
    } else if (auto xrts = dynamic_cast<TsTypeRef *>(ts); xrts) {
	Ident btid = xrts->id;
	if (tid != 0) { res.push (tid); }
	res.push (get_vtypes1 (st, btid, st[btid]));
    } else if (auto selts = dynamic_cast<TsSelect *>(ts); selts) {
	for (auto tid1: selts->flatTypes) {
	    res.push (get_vtypes1 (st, tid1, st[tid]));
	}
    } else if (auto enumts = dynamic_cast<TsEnum *>(ts); enumts) {
	res.push (tid);
    } else if (auto defdts = dynamic_cast<TsDefined *>(ts); defdts) {
	Ident btid = defdts->basetype;
	if (tid != 0) { res.push (tid); }
	res.push (get_vtypes1 (st, btid, st[btid]));
    } else if (auto entts = dynamic_cast<TsEntity *>(ts); entts) {
	res.push (get_entvtypes (st, tid, entts));
    } else {
	// Do nothing here, as this case should never occur.
    }
    return res;
}

static Fifo<Ident> get_vtypes (Symtab &st, Ident tid, TypeStruct *ts)
{
    Fifo<Ident> res;
    // Collect the "valid" types in a temporary list
    Fifo<Ident> tmp (get_vtypes1 (st, tid, ts));
    // Sort the list
    //sort_idlist (tmp);
    // Create a the resulting list by transferring all but the duplicate
    // elements from the temporary list. Because 'tmp' is sorted, removing
    // duplicate entries simply consists of ignoring elements which equal
    // the last element transferred, and continuing moving from the next
    // element which differs.
    IdentBitSet typelist;
    //Ident last = 0;
    while (! tmp.empty()) {
	Ident tid = tmp.shift();
	if (typelist.has (tid)) { continue; }
	res.push (tid); typelist.insert (tid);
	//if (tid != last) { res.push (tid); last = tid; }
    }
    // Returning the resulting list.
    return res;
}

// Recursively collect all supertypes of a given ENTITY type. The lists
// retrieved by invocations of this function are used for creating the
// 'supertypes[]' array from the corresponding template, and this array
// is required for determining the type compatibility of a given SELECT type
// entry. The result includes the given ENTITY type itself. The reason for this
// is that a type is always compatible to itself. 'get_supertypes_()' is used
// internally in 'get_supertypes()', which is the function really used,
// otherwise. The reason is that i don't want to create a new bit set in each
// recursive invocation, especially because the bit set used is always the
// same and should not be re-constructed after every invocation. So, in
// 'get_supertypes_()', this bit set is supplied as a reference parameter, and
// in 'get_supertypes()' there is exactly one bit set created which is then
// used as actual argument for this function. Additionally, the bit set
// supplied and returned by this parameter can directly be used for creating
// the result of 'get_supertypes()'.
// 
static void get_supertypes_ (Symtab &st, Ident tid,
			     BitSet<Ident, 1> &supertypeset)
{
    auto ets = dynamic_cast<TsEntity *>(st[tid]);
    if (ets) {
	supertypeset.include (tid);
	for (auto supertypeid: ets->mysupertypes) {
	    // The reference parameter helps avoiding an inner loop otherwise
	    // required for combining the results of all recursive invocations
	    // of 'get_supertypes_()', and also, the bit set really used allows
	    // for collecting the supertype idents ordered by their id in the
	    // symbol table, which is also the way the ExpTypes elements are
	    // ordered, and thus probably allows for a binary search being
	    // used for the detection the tables 'supertypes[]' and
	    // 'supertype_indexes[]' are created for (type checking an
	    // attribute for inserting it at the correct position into the
	    // 'VALUE'-list of a database 'INSERT' query for a SELECT-typed
	    // table.
	    get_supertypes_ (st, supertypeid, supertypeset);
	}
    }
}

// Collect all supertypes for a given entity type 'tid', and also the type
// itself and return the collected Ident-values as a 'Fifo<Ident>'-list.
static Fifo<Ident> get_supertypes (Symtab &st, Ident tid)
{
    BitSet<Ident, 1> supertypeset;
    get_supertypes_ (st, tid, supertypeset);
    return supertypeset.elements();
}
