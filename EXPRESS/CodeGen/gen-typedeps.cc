/* gen-typedeps.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Generate the lists of the type dependencies (hierarchies). This list is
** used for determining if an object of some type is assignment compatible
** to some other type. The generated lists no refer the symbol table, which
** is not part of the generated module, but are used for generating static
** tables which then are used for the conversion of STEP-objects into objects
** of the generated type(s).
**
*/

//##DEBUG
//#include <iostream>
//using std::cout, std::cerr, std::endl;
//##

#include <utility>
#include <string>
#include <set>
#include <stdexcept>

#include "Common/fifo.h"

#include "gen-typedeps.h"

using std::runtime_error;
using std::to_string;

using Entities = BitSet<ExpIndex, 0>;

// Construct translation tables (maps) between 'Ident' values (indices of
// symbol table entries) and simple index values (symbol references for the
// generated static tables).
TypeIndexList gen_typeindexassoc (Symtab &st)
{
    Fifo<TypeIndexPair> tmp;
    TypeIndexList res;
    uint16_t index = 0;
    for (Ident id = st.first(); id <= st.last(); ++id) {
	auto ts = dynamic_cast<TsType *>(st[id]);
	if (ts) { tmp.push (make_pair (id, ++index)); }
    }
    res.reserve (tmp.length());
    while (! tmp.empty()) { res.push_back (tmp.shift()); }
    return res;
}

// De-construct an aggregate type
static Ident get_basetype (Symtab &st, Ident id, TypeStruct *ts)
{
    TypeStruct::Type tstype = ts->type();
    switch (tstype) {
	case TypeStruct::array: {
	    auto ats = dynamic_cast<TsArray2 *>(ts);
	    return get_basetype (st, id, ats->basetype);
	}
	case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
	    auto ats = dynamic_cast<TsAggregateLike *>(ts);
	    return get_basetype (st, id, ats->basetype);
	}
	case TypeStruct::typeref: {
	    auto trts = dynamic_cast<TsTypeRef *>(ts);
	    return trts->id;
	}
	case TypeStruct::boolean: { return st["BOOLEAN"]; }
	case TypeStruct::logical: { return st["LOGICAL"]; }
	case TypeStruct::integer: { return st["INTEGER"]; }
	case TypeStruct::real: { return st["REAL"]; }
	case TypeStruct::number: { return st["NUMBER"]; }
	case TypeStruct::binary: { return st["BINARY"]; }
	case TypeStruct::string: { return st["STRING"]; }
	default: {
	    return id;	    // Not clear yet
	}
    }
}

// Construct a list of each valid "substitute" for a type ... meaning each
// value (or reference) which is (probably) assignment compatible with a
// given type. The result is a list of indices which will be used for
// generating static tables which are used for type-checking STEP objects,
// before they are converted into corresponding EXPRESS objects.
static ValidTypes gen_typehier (Symtab &st, TypeIndexMap &txmap, Ident id)
{
    ValidTypes res;
    BitSet<ExpIndex, 0> included;
    auto ts = dynamic_cast<TsType *>(st[id]);
    if (! ts) {
	throw runtime_error ("INTERNAL ERROR! " + to_string (id) + " (" +
			     st.symname (id) + ") is no type identifier.");
    }
    switch (ts->type()) {
	case TypeStruct::entity: {
	    IdentList &subtypes = dynamic_cast<TsEntity *>(ts)->coll_subtypes;
	    res.reserve (subtypes.length() + 1);
	    res.push_back (txmap[id]);
	    for (Ident stid: subtypes) {
		ExpIndex ix = txmap[stid];
		if (included.has (ix)) { continue; }
		res.push_back (txmap[stid]);
		included.insert (ix);
	    }
	    break;
	}
	case TypeStruct::array: {
	    Ident btid = get_basetype (st, id, ts);
	    res = gen_typehier (st, txmap, btid);
	    break;
	}
	case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
	    Ident btid = get_basetype (st, id, ts);
	    res = gen_typehier (st, txmap, btid);
	    break;
	}
	case TypeStruct::enumeration: {
	    res.reserve (1); res.push_back (txmap[id]);
	    break;
	}
	case TypeStruct::select: {
	    auto sts = dynamic_cast<TsSelect *>(ts);
	    Fifo<ValidTypes> vtll;
	    size_t ressz = 0;
	    if (! sts->flattened) {
		throw runtime_error ("INTERNAL ERROR! Missing the '"s +
				     "flattening' step for the type" +
				     " hierarchies.");
	    }
	    // The recursive call must be done nonetheless, but the recursion
	    // is not as deep as without the "flattening" step.
	    for (Ident tid: sts->flatTypes) {
		vtll.push (gen_typehier (st, txmap, tid));
		ressz += vtll.last().size();
	    }
	    // Now, enough space is reserved for the result ...
	    res.reserve (ressz);
	    // In the last step, each type from 'vtll' is inserted into 'res'.
	    while (! vtll.empty()) {
		ValidTypes vtl = vtll.shift();
		for (auto ix: vtl) {
		    if (included.has (ix)) { continue; }
		    res.push_back (ix); included.insert (ix);
		}
		//res.insert (res.end(), vtl.begin(), vtl.end());
	    }
	    break;
	}
	case TypeStruct::defined: {
	    auto dts = dynamic_cast<TsDefined *>(ts);
	    ValidTypes vtl = gen_typehier (st, txmap, dts->basetype);
	    res.reserve (vtl.size() + 1);
	    if (! included.has (txmap[id])) {
		res.push_back (txmap[id]); included.insert (txmap[id]);
	    }
	    for (auto ix: vtl) {
		if (included.has (ix)) { continue; }
		res.push_back (ix); included.insert (ix);
	    }
	    break;
	}
	case TypeStruct::logical: case TypeStruct::boolean: {
	    res.reserve (2);
	    static const char *logicals[] = { "BOOLEAN", "LOGICAL" };
	    for (auto tname: logicals) {
		ExpIndex ix = txmap[st[tname]];
		if (included.has (ix)) { continue; }
		res.push_back (ix); included.insert (ix);
	    }
	    break;
	}
	case TypeStruct::integer: {
	    res.reserve (2); res.push_back (txmap[id]);
	    res.push_back (txmap[st["NUMBER"]]);    // Not clear yet!
	    break;
	}
	case TypeStruct::real: {
	    res.reserve (3); res.push_back (txmap[id]);
	    res.push_back (txmap[st["INTEGER"]]);
	    res.push_back (txmap[st["NUMBER"]]);    // Not clear yet!
	    break;
	}
	case TypeStruct::number: {
	    res.reserve (3); res.push_back (txmap[id]);
	    res.push_back (txmap[st["INTEGER"]]);
	    res.push_back (txmap[st["REAL"]]);
	    break;
	}
	case TypeStruct::binary: {
	    auto bts = dynamic_cast<TsBinary *>(ts);
	    res.reserve (2); res.push_back (txmap[id]);
	    if (bts->size != 0) { res.push_back (txmap[st["BINARY"]]); }
	    break;
	}
	case TypeStruct::string: {
	    auto sts = dynamic_cast<TsString *>(ts);
	    res.reserve (2); res.push_back (txmap[id]);
	    if (sts->size != 0) { res.push_back (txmap[st["STRING"]]); }
	    break;
	}
	default: {
	    break;
	}
    }
    return res;
}

static TypeIndexMap gen_txmap (const TypeIndexList &til)
{
    TypeIndexMap res;
    for (auto [id, ix]: til) { res.insert (make_pair (id, ix)); }
    return res;
}

std::pair<ValidLists, Entities> gen_hierlists (Symtab &st, TypeIndexList &til)
{
    //uint32_t max = 0;
    ValidLists res; Entities is_an_entity;
    Fifo<ValidTypes> hlist;
    TypeIndexMap txmap = gen_txmap (til);
    for (auto [id, ix]: til) {
	auto ts = dynamic_cast<TsType *>(st[id]);
	auto zz = gen_typehier (st, txmap, id);
	hlist.push (zz); // (gen_typehier (st, txmap, id));
	if (ts->type() == TypeStruct::entity) {
	    is_an_entity.insert (ix);
	}
    }
    res.reserve (hlist.size());
    while (! hlist.empty()) {
	auto zz = hlist.shift();
	res.push_back (zz); // (hlist.shift());
    }
    return make_pair (res, is_an_entity);
}

TypeIndexList get_entities (Symtab &st, const TypeIndexList &til)
{
    TypeIndexList res;
    unsigned entc = 0;
    for (auto [id, ix]: til) {
	TypeStruct *ts = st[id];
	if (ts->type() == TypeStruct::entity) { ++entc; }
    }
    res.reserve (entc);
    for (auto [id, ix]: til) {
	TypeStruct *ts = st[id];
	if (ts->type() == TypeStruct::entity) {
	    res.push_back (make_pair (id, ix));
	}
    }
    return res;
}

static Ident get_aggbasetype (Symtab &st, TypeStruct *ts)
{
    auto aggts = dynamic_cast<TsAggregateLike *>(ts);
    if (aggts) {
	return get_aggbasetype (st, aggts->basetype);
    } else {
	auto trefts = dynamic_cast<TsTypeRef *>(ts);
	if (trefts) {
	    return trefts->id;
	}
	// Old mechanism ... i need to remove direct references to the
	// basic types and use the pre-declared symbol table entries instead
	// (don't know if i've this done yet).
	switch (ts->type()) {
	    case TypeStruct::boolean: return st["BOOLEAN"];
	    case TypeStruct::logical: return st["LOGICAL"];
	    case TypeStruct::integer: return st["INTEGER"];
	    case TypeStruct::real: return st["REAL"];
	    case TypeStruct::number: return st["NUMBER"];
	    case TypeStruct::binary: return st["BINARY"];
	    case TypeStruct::string: return st["STRING"];
	    default:
		throw runtime_error ("Invalid aggregate base type");
	}
    }
}

using IdSet = std::set<Ident>;
struct EntDep { ExpIndex ix; IdSet ids; };
using EntDepMap = std::map<Ident, EntDep>;

TypeIndexList depsort_types (Symtab &st, const TypeIndexList &til)
{
    TypeIndexList res;
    EntDepMap edmap;
    for (auto [id, ix]: til) {
	EntDep ed { ix, IdSet() };
	TypeStruct *ts = st[id];
	switch (ts->type()) {
	    case TypeStruct::defined: {
		auto dts = dynamic_cast<TsDefined *>(ts);
		ed.ids.insert (dts->basetype);
		break;
	    }
	    case TypeStruct::entity: {
		auto ets = dynamic_cast<TsEntity *>(ts);
		for (auto supertype: ets->mysupertypes) {
		    ed.ids.insert (supertype);
		}
		break;
	    }
	    case TypeStruct::array: case TypeStruct::list:
	    case TypeStruct::bag: case TypeStruct::set: {
		ed.ids.insert (get_aggbasetype (st, ts));
		break;
	    }
	    default: break;
	}
	edmap.insert (make_pair (id, ed));
    }
    res.reserve (edmap.size());
    while (edmap.size() > 0) {
	Fifo<Ident> rmv;
	// Collect the Entries which have no dependencies, as these must
	// occur first in the dependency list. Additionally, collect the ids
	// of these elements for the later operations
	for (auto &[id, ed]: edmap) {
	    if (ed.ids.size() == 0) {
		res.push_back (make_pair (id, ed.ix));
		rmv.push (id);
	    }
	}
	// Remove the already collected entries
	for (auto id: rmv) { edmap.erase (id); }
	// Remove the ids collected in this round from the dependency lists of
	// the remaining entries. This creates entries with no (remaining)
	// dependencies for the next round.
	for (auto id: rmv) {
	    for (auto &[id1, ed]: edmap) {
		ed.ids.erase (id);
	    }
	}
    }
    return res;
}
