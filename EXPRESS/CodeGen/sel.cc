/* CodeGen/sel.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Selector module for the code generation
**
*/

#include <stdexcept>

#include "sel.h"

#include "checkstar.h"
#include "db.h"
#include "cpp.h"

using namespace std;

namespace CodeGen {

using namespace EXPRESS;

typedef bool (*cgen_t) (Symtab &, const string &, const string &);
typedef bool (*preproc_t) (Symtab &);

struct Selector { OpMode op; preproc_t preproc; cgen_t cgen; };

static Selector selector_table[] = {
    { OpMode::db, nullptr, DB::cgen },
    { OpMode::cpp, nullptr, CPP::cgen },
    { OpMode::none, nullptr, nullptr }
};

bool cgen (OpMode op,
	   Symtab &symtab,
           const string &template_base,
           const string &outfile_base)
{
    Selector *s;
    for (int ix = 0; (s = &selector_table[ix])->op != OpMode::none; ++ix) {
	if (s->op == op) {
	    bool done = true;
	    if (s->preproc) { done = s->preproc (symtab); }
	    if (done) { done = s->cgen (symtab, template_base, outfile_base); }
	    return done;
	}
    }
    throw runtime_error
	("No code generator module '" + to_string (op) + "' found.");
    return false;
}

} /*namespace CodeGen*/
