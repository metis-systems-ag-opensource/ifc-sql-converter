/* CodeGen/checkstar.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Utility module which writes the positions of DERIVE attributes of entites
** which have them to stdout, but only there are EXPLICIT attributes following
** these DERIVE attributes.
**
*/

#include <fstream>
#include <iostream>
#include <stdexcept>

#include "Common/fifo.h"
#include "Common/sutil.h"
#include "Common/pathcat.h"

#include "checkstar.h"

namespace CodeGen::CheckStar {

using std::ofstream;
using std::cout, std::flush;
using std::runtime_error;
using std::string;

struct NameStruct {
    const string *name;
    TsEntity *ts;
};

bool cgen (Symtab &st,
	   const string &_unused1,
	   const string &outfile_base)
{
    Fifo<NameStruct> ce;
    const string &schema = st.schema();
    ofstream out;
    ostream *_out = &cout;
    bool to_file = false;
    if (outfile_base != "-") {
	string outfile = outfile_base / lowercase (schema) + ".cstar";
	out.open (outfile);
	if (! out.is_open()) {
	    throw runtime_error ("Opening '" + outfile + "' failed.");
	}
	to_file = true;
	_out = &out;
    }

    for (Ident id = st.first(); id <= st.last(); ++id) {
	auto ts = dynamic_cast<TsEntity *>(st[id]);
	if (! ts || ts->abstract) { continue; }
	bool dafound = false;
	for (auto &fa: ts->flatAttributes) {
	    if (fa.attr->type() == Attr::derived_attr && ! dafound) {
		dafound = true;
	    } else if (dafound && fa.attr->type() == Attr::explicit_attr) {
		ce.push (NameStruct {&st.symname (id), ts});
		// Need no further examination, because this is already a
		// candidate to be issued.
		break;
	    }
	}
    }
    for (auto ns: ce) {
	*_out << *ns.name << ": " << flush;
	unsigned ix = 0;
	TsEntity *ts = ns.ts;
	bool first = true;
	for (auto fa: ts->flatAttributes) {
	    if (fa.attr->type() == Attr::derived_attr) {
		if (first) { first = false; } else { *_out << ", "; }
		*_out << ix;
	    }
	    ++ix;
	}
	*_out << EOL;
    }
    if (to_file) { out.close(); }
    return true;
}

} /*namespace CodeGen::CheckStar*/
