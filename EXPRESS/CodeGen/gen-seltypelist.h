/*! \file gen-seltypelist.h
**  Part of the plugin code generatos
**
**  Generating the type lists for validating assignments to attributes of
**  SELECT types.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Interface file for 'gen-seltypelist.cc'
** (Generating the 'type lists' for validating assignments to a attributes of
**  SELECT types.)
**
*/
#ifndef GEN_SELTYPELIST_CC
#define GEN_SELTYPELIST_CC

#include <fstream>
#include <string>
#include <utility>

#include "symtab.h"
#include "gen-typedeps.h"

void gen_seltypelist (Symtab &st, const TypeIndexList &til,
		      const std::string fnpfx);

#endif /*GEN_SELTYPELIST_CC*/
