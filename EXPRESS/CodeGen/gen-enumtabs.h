/*! \file gen-enumtabs.h
**  Part of the plugin code generator
**
**  ENUMERATION types are completely static. This module generates C-tables
**  for `gen-enums.{cc,h}`, which allow for a memory conserving handling
**  of the ENUMERATION types (string -> enum, enum -> string, returning the
**  static lists of the values of the members of a given ENUMERATION type,
**  returning the ENUMERATION type name associated with a specific ENUMERATION
**  value, ...)
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
** Interface file for 'gen-enumtabs.cc'
** (Generate tables for a fast access to the EXPRESS ENUMERATION constants.)
**
*/
#ifndef GEN_ENUMTABS_H
#define GEN_ENUMTABS_H

#include <string>

#include "symtab.h"
#include "gen-typedeps.h"

TypeIndexList gen_enumtables (Symtab &st, const TypeIndexList &til,
			      const std::string &pathname);

#endif /*GEN_ENUMTABS_H*/
