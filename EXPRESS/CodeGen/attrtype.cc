/* attrtype.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generate code for defined types/ENTITY attribute types/...
**
*/

#include <stdexcept>

#include "attrtype.h"

using std::runtime_error;
using std::string, std::to_string;


// Converts simple types into the corresponding 'ExpXXX' typenames and
// simply returns the corresponding symbol name otherwise.
const char *tntrans (Symtab &st, Ident tid)
{
    TypeStruct *ts = st[tid];
    switch (ts->type()) {
        case TypeStruct::boolean: return "ExpBoolean";
        case TypeStruct::logical: return "ExpLogical";
        case TypeStruct::integer: return "ExpInteger";
        case TypeStruct::real:    return "ExpReal";
        case TypeStruct::number:  return "ExpNumber";
        case TypeStruct::binary:  return "ExpBinary";
        case TypeStruct::string:  return "ExpString";
        default: {
            const string &tn = st.symname (tid);
            return tn.c_str();
        }
    }
}

static string tattrname (Symtab &st, Ident eid, Ident aid)
{
    if (aid == 0) { return st.symname (eid); }
    return st.symname (eid) + "." + st .symname (aid);
}

string gen_attrtype (Symtab &st, Ident eid, Ident aid, TypeStruct *ts)
{
    TypeStruct::Type tstype = ts->type();
    switch (tstype) {
	case TypeStruct::array: {
	    auto ats = dynamic_cast<TsArray2 *>(ts);
	    if (! ats->lh_eval()) {
		throw runtime_error (
		    "Can't generate an ARRAY with non-constant bounds for '" +
		    tattrname (st, eid, aid) + "'");
	    }
	    string eltype = gen_attrtype (st, eid, aid, ats->basetype);
	    return "Array<" + eltype + ", " +
			      to_string (ats->ilwb) + ", " +
			      to_string (ats->iupb) + ">";
	}
	case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
	    // Construct the corresponding C++ template instantiation for the
	    // given aggregate type. If the aggregate type has bounds ('[X:Y]'
	    // given and not '[0:?]'), the "restricted" variants of the
	    // corresponding template types are used, allong with the
	    // additional template arguments.
	    auto lbsts = dynamic_cast<TsLBS *>(ts);
	    string eltype = gen_attrtype (st, eid, aid, lbsts->basetype);
	    string typeargs = "<" + eltype + ">", rpfx;
	    // 'typeargs' - the template arguments
	    // 'rpfx' - "R" if a "restricted" type is used and "", otherwise
	    if (lbsts->min > 0 || lbsts->max > 0) {
		// Either of the member variables 'min' or 'max' being > 0
		// means that there are restrictions. This means that,
		// additionally to the type parameter, these values must be
		// put in the template argument list.
		typeargs = "<" + eltype + "," + to_string (lbsts->min) + "," +
			   to_string (lbsts->max) + ">";
		// In this case, the template type name is 'RList', 'RBag' or
		// 'RSet'.
		rpfx = "R";
	    }
	    // Construct the correct template instantiations as strings.
	    switch (tstype) {
		case TypeStruct::list: return rpfx + "List" + typeargs;
		case TypeStruct::bag: return rpfx + "Bag" + typeargs;
		case TypeStruct::set: return rpfx + "Set" + typeargs;
		default:
		    ; // DOES NEVER OCCUR, only for avoiding compiler warnings
	    }
	}
	// The only other valid "type" for an attribute type is a
	// "type reference", meaning: a reference to a named type.
	case TypeStruct::typeref: {
	    auto rts = dynamic_cast<TsTypeRef *>(ts);
	    Ident tid = rts->id;
	    bool entity = rts->isentity;
	    if (! entity) {
		// Don't know for sure that the parser always set this flag
		// correctly.
		TypeStruct *refts = st[tid];
		if (refts->type() == TypeStruct::entity) { entity = true; }
	    }
	    if (entity) {
		// If the STEP-value is correct cannot determined at compile
		// time, but must rather be verified at runtime, so for all
		// ENTITY types, the answer is the same ...
		return "ExpInstRef";
	    }
	    // In all other cases, the parser generated a reference to a normal
	    // entry in the symbol table (even for the simple types), so the
	    // name of this type is the correct answer.
	    return tntrans (st, tid);
	}
	// A previous version of the parser generated direct entries for
	// the simple types. Because of this, i'm catching these here
	// explicitely
	case TypeStruct::integer: return "ExpInteger";
	case TypeStruct::real:    return "ExpReal";
	case TypeStruct::boolean: return "ExpBoolean";
	case TypeStruct::logical: return "ExpLogical";
	case TypeStruct::number:  return "ExpNumber";
	case TypeStruct::binary:  return "ExpBinary";
	case TypeStruct::string:  return "ExpString";
	default: break;
    }
    throw runtime_error (
	    "FATAL! Unknown/Invalid type to be used as type for '" +
	    tattrname (st, eid, aid) + "'");
}
