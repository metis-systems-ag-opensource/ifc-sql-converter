/* gen-createvalue.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generate the generator for (non-ENTITY) EXPRESS values (or, rather their
** C++ counterparts).
**
*/

#include <fstream>
#include <utility>

#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "outstrings.h"

#include "gen-createvalue.h"


static const char *deffile =
    "/* %hfile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2020, Metis AG" EOL
    "** " EOL
    "** Interface file for '%ifile'" EOL
    "** (Generate EXPRESS values from 'typed parameter' STEP values.)" EOL
    "**" EOL
    "*/" EOL
    "#ifndef %HFILE" EOL
    "#define %HFILE" EOL
    "" EOL
    "#include \"valtypes.h\"" EOL
    "" EOL
    "#include \"expbase.h\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "ExpBase *exp_gentypedvalue (const STEP::Value *v);" EOL
    "" EOL
    "} /*namespace IFC*/" EOL
    "" EOL
    "#endif /*%HFILE*/" EOL
;

static const char *implhead =
    "/* %cfile" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2020, Metis AG" EOL
    "**" EOL
    "** Interface file for '%ifile'" EOL
    "** (Generate EXPRESS values from 'typed parameter' STEP values.)" EOL
    "**" EOL
    "*/" EOL
    "" EOL
    "#include <stdexcept>" EOL
    "" EOL
    "#include \"plinterface.h\"" EOL
    "#include \"enums.h\"" EOL
    "#include \"%hdeftypes\"" EOL
    "" EOL
    "#include \"%hfile\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "using std::runtime_error;" EOL
    "" EOL
    "ExpBase *exp_gentypedvalue (const STEP::Value *v)" EOL
    "{" EOL
    "    STEP::ValType vtype = v->type();" EOL
    "    if (vtype == STEP::ValType::instid) {" EOL
    "\t// Generate an instance reference object ..." EOL
    "\tauto ev = dynamic_cast<const STEP::InstIdValue *>(v);" EOL
    "\treturn new ExpInstRef (ev);" EOL
    "    } else if (vtype == STEP::ValType::typedparam) {" EOL
    "\tauto tv = dynamic_cast<const STEP::TypedParamValue *>(v);" EOL
    "\tExpTypes ttag = index2id (tv->name - 1);" EOL
    "\tSTEP::Value *ev = tv->value;" EOL
    "\tswitch (ttag) {" EOL
    "\t    case ExpTypes::BOOLEAN: return new ExpBoolean (ev);" EOL
    "\t    case ExpTypes::LOGICAL: return new ExpLogical (ev);" EOL
    "\t    case ExpTypes::INTEGER: return new ExpInteger (ev);" EOL
    "\t    case ExpTypes::REAL: return new ExpReal (ev);" EOL
    "\t    case ExpTypes::NUMBER: return new ExpNumber (ev);" EOL
    "\t    case ExpTypes::BINARY: return new ExpBinary (ev);" EOL
    "\t    case ExpTypes::STRING: return new ExpString (ev);" EOL
;

static const char *stdcase =
    "\t    case ExpTypes::%type: return new %type (ev);" EOL
;

static const char *stddeflt =
    "\t    default: return nullptr;" EOL
;

static const char *implfoot =
    "\t}" EOL
    "    } else {" EOL
    "\tthrow runtime_error (\"Invalid type for 'exp_gentypedvalue()'\");" EOL
    "    }" EOL
    "}" EOL
    "" EOL
    "} /*namespace IFC*/" EOL
;

void gen_createvalue (Symtab &st, const TypeIndexList &til,
		      const string &fnpfx, const string &hdeffile)
{
    string deftypes = basename (hdeffile);
    string hfile = "gentypedvalue.h", hpath = fnpfx / hfile;
    string ifile = "gentypedvalue.cc", ipath = fnpfx / ifile;
    string hmac = trans_filename (hfile);

    ofstream hout, iout;

    Substitutes vars {
	{ "hfile", hfile },
	{ "HFILE", hmac },
	{ "ifile", ifile },
	{ "hdeftypes", deftypes }
    };

    hout.open (hpath);
    if (! hout.is_open()) {
	throw runtime_error ("Attempt to write '" + hpath + "' failed.");
    }

    iout.open (ipath);
    if (! iout.is_open()) {
	throw runtime_error ("Attempt to write '" + ipath + "' failed.");
    }

    hout << tmpl_subst (deffile, vars);
    hout.close();

    iout << tmpl_subst (implhead, vars);

    for (auto [id, ix]: til) {
	TypeStruct *ts = st[id];
	TypeStruct::Type tstype = ts->type();
	switch (tstype) {
	    case TypeStruct::enumeration: case TypeStruct::defined:
	    case TypeStruct::array: case TypeStruct::list:
	    case TypeStruct::bag: case TypeStruct::set: {
		vars["type"] = st.symname (id);
		iout << tmpl_subst (stdcase, vars);
		break;
	    }
	    default: break;
	}
    }
    vars.erase ("type");

    iout << tmpl_subst (stddeflt, vars);

    iout << tmpl_subst (implfoot, vars);

    iout.close();
}
