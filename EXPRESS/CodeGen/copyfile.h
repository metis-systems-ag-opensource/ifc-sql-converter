/*! \file copyfile.h
**  Creating a copy of a file.
**
**  This module is used in the `cpp.{cc,h}` code generator module for copying
**  the static parts of the plugin source code to the directory where the
**  plugin is to be created.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Interface file for 'copyfile.cc'
** (Copying one of the "fixed" files from a given directory to another one.)
**
*/
#ifndef COPYFILE_H
#define COPYFILE_H

#include <string>

/*! Creates a copy of a file.
**
**  @param src - the pathname of the file to be copied
**
**  @param dst - the pathname of the copy being created
**
**  @returns the number of bytes copied.
*/
std::streamsize copyfile (const std::string &src, const std::string &dst);

#endif /*COPYFILE_H*/
