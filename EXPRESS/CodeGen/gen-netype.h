/*! \file gen-netype.h
**  Part of the plugin code generator
**
**  Generating code of all non-ENUMERATION and non-ENTITY types which are
**  part of an IFC schema.
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generate (a C++ class) for a Non-Entity type.
**
*/
#ifndef GEN_NETYPE_H
#define GEN_NETYPE_H

#include <ostream>
#include <string>

#include "symtab.h"
#include "gen-typedeps.h"

void gen_netype (Symtab &st, Ident id, std::ostream &hout, std::ostream &iout);
void gen_netypes (Symtab &st, const TypeIndexList &til,
		  const std::string &ofpfx);

#endif /*GEN_NETYPE_H*/
