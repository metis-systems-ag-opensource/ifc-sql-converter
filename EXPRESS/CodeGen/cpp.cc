/* cpp-neu.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** New version of the main module of the code generator for C++
** This version will use the code generators i wrote since May 2019.
**
*/

#include <iostream>
using std::cout, std::endl, std::flush;

#include <filesystem>
#include <stdexcept>

namespace fs = std::filesystem;
using std::string, std::to_string;
using std::domain_error, std::runtime_error, std::range_error,
      std::system_error;

#include "cpp.h"

#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "copyfile.h"
#include "gen-typedeps.h"
#include "gen-createvalue.h"
#include "gen-eaplist.h"
#include "gen-entities.h"
#include "gen-enums.h"
#include "gen-enumtabs.h"
#include "gen-netype.h"
#include "gen-seltypelist.h"
#include "gen-stepsyms.h"
#include "gen-factory.h"
#include "gen-mk.h"
#include "gen-t2bt.h"


namespace CodeGen::CPP {

bool cgen (Symtab &st, const string &template_base, const string &outfile_base)
{
    // In this module, i must check if 'outfile_base' is a directory, because
    // this is the only option left here. The given directory is possible
    // specified through a configuration file.

    cout << "Generating code (c++) " << flush;

    if (! fs::exists (outfile_base) || ! fs::is_directory (outfile_base)) {
	throw runtime_error ("'" + outfile_base + "'"
			     " doesn't exist or is no directory.");
    }
    string ofdir = outfile_base / lowercase (st.schema());
    if (! fs::exists (ofdir)) {
	std::error_code ec;
	bool done = fs::create_directory (ofdir, ec);
	if (! done) {
	    throw runtime_error ("cgen(): fs::create_directory() - " +
				 ec.message());
	}
    } else if (! fs::is_directory (ofdir)) {
	throw runtime_error ("Existing '" + ofdir + "' is no directory.");
    }

    if (! fs::exists (template_base) || ! fs::is_directory (template_base)) {
	throw runtime_error ("'" + template_base + "'"
			     " doesn't exist or is no directory.");
    }

    string ffdir = template_base / "static";
    if (! fs::exists (ffdir) || ! fs::is_directory (ffdir)) {
	throw runtime_error ("'" + ffdir + "'"
			     " doesn't exist or is no directory.");
    }

    // After checking the existance of the target directory, the fixed parts
    // 'expbase.{cc,h}', 'chelpers.{cc,h}', 'selhelpers.{cc,h}', 'aggtypes.h',
    // etc. must be copied into this target directory.
    string copied[] = {
	"aggtypes.h",
	"basetype.cc", "basetype.h",
	"chelpers.cc", "chelpers.h",
	"dbif.cc", "dbif.h",
	"enumbase.h", "enumtest.cc",
	"enumval.cc", "enumval.h",
	"errlist.cc", "errlist.h", "errlist-test.cc",
	"expbase.cc", "expbase.h",
	"exptypename.cc", "exptypename.h",
	"fifo.h",
	"fixrefs.cc", "fixrefs.h",
	"idgen.cc", "idgen.h",
	"ifcmodel.h",
	"instdestroy.cc", "instdestroy.h",
	"mintypes.h",
	"modid.cc", "modid.h",
	"outputops.cc", "outputops.h",
	"pds.cc", "pds.h",
	"plctx.h", /*"plctx.cc",*/
	"plinterface.cc", "plinterface.h",
	"selbase.cc", "selbase.h",
	"selhelpers.cc", "selhelpers.h",
	"storedata.cc", "storedata.h",
	"storemeta.cc", "storemeta.h",
	"typedefs.h",
	"utils.cc", "utils.h",
	"valtypes.cc", "valtypes.h",
	"warning.cc", "warning.h",
    };

    for (string f2bc: copied) {
	// Copy the file
	copyfile (ffdir / f2bc, ofdir);
	//streamsize sz = copyfile (ffdir / f2bc, ofdir);
    }
    cout << "." << flush;

    // Thereafter, the dynamic parts (the files which depend on the IFC
    // specification) must be generated in this same directory. (Currently,
    // i'm using hard-coded template files, but i will change this as soon as
    // possible, using 'template_base' as source directory for the templates.
    // This also requires a template mechanism (type with parser and evaluator)
    // to be written.)
    TypeIndexList til = gen_typeindexassoc (st);
    cout << "." << flush;

    // Now, the different source files of the different modules, the "plugin"
    // consists of, are generated.
    TypeIndexList enumlist = gen_enumtables (st, til, ofdir / "enumtabs.cc");
    cout << "." << flush;
    gen_enums (st, enumlist, ofdir / "enums");
    cout << "." << flush;
    gen_seltypelist (st, til, ofdir);
    cout << "." << flush;
    gen_netypes (st, til, ofdir);
    cout << "." << flush;
    gen_eaplist (st, til, ofdir);
    cout << "." << flush;
    gen_entities (st, til, ofdir);
    cout << "." << flush;
    gen_createvalue (st, til, ofdir, "netypes.h");
    cout << "." << flush;
    gen_expfactory (st, til, ofdir / "expfactory");
    cout << "." << flush;
    gen_stepsyms (st, til, ofdir, "exptypes", "symbols");
    cout << "." << flush;
    gen_t2bt (st, til, ofdir);
    cout << "." << flush;


    // Preparing the dependency list for the 'Makefile' being generated.
    // '%m' is replaced with the corresponding module implementation and
    // interface files. E.g.:
    //    { "chelpers", ResList { "%m", "eaplist.cc" } }
    // is semantically equivalent to
    //    { "chelpers", ResList { "chelpers.cc", "chelpers.h", "eaplist.cc" } }
    //
    Deps deplist {
	{ "basetype", ResList { "%m", "exptypes.h", "type2basetype.cc", } },
	{ "chelpers", ResList { "%m", "eaplist.cc", "expbase.h", "exptypes.h",
				"fifo.h", "ifcmodel.h", "mintypes.h",
				"plinterface.h", "typedefs.h", "valtypes.h" } },
	{ "dbif", ResList { "%m" } },
	{ "entities", ResList { "%m", "aggtypes.h", "chelpers.h", "enums.h",
				"expbase.h", "exptypes.h", "fifo.h",
				"ifcmodel.h", "mintypes.h", "netypes.h",
				"plinterface.h", "selbase.h", "typedefs.h",
				"valtypes.h" } },
	{ "enums", ResList { "%m", "enumbase.h", "enumval.h", "expbase.h",
			     "exptypes.h", "fifo.h", "mintypes.h",
			     "valtypes.h" } },
	{ "enumval", ResList { "%m", "enumtabs.cc", "utils.h" } },
	{ "errlist", ResList { "%m" } },
	{ "expbase", ResList { "%m", "errlist.h", "exptypes.h", "fifo.h",
			       "mintypes.h", "utils.h", "valtypes.h",
			       "warning.h" } },
	{ "expfactory", ResList { "%m", "aggtypes.h", "chelpers.h",
				  "entities.h", "expbase.h", "exptypes.h",
				  "fifo.h", "ifcmodel.h", "mintypes.h",
				  "netypes.h", "plinterface.h", "selbase.h",
				  "typedefs.h", "valtypes.h" } },
	{ "exptypename", ResList { "%m", "expbase.h", "exptypes.h", "fifo.h",
				   "ifcmodel.h", "mintypes.h", "plinterface.h",
				   "typedefs.h", "valtypes.h" } },
	{ "fixrefs", ResList { "%m", "aggtypes.h", "chelpers.h", "expbase.h",
			       "exptypes.h", "fifo.h", "ifcmodel.h",
			       "mintypes.h", "plinterface.h", "selbase.h",
			       "typedefs.h", "utils.h", "valtypes.h" } },
	{ "gentypedvalue", ResList { "%m", "aggtypes.h", "enumbase.h",
				     "enums.h", "expbase.h", "exptypes.h",
				     "fifo.h", "ifcmodel.h", "mintypes.h",
				     "netypes.h", "plinterface.h", "selbase.h",
				     "typedefs.h", "valtypes.h" } },
	{ "idgen", ResList { "%m" } },
	{ "instdestroy", ResList { "%m", "expbase.h", "exptypes.h", "fifo.h",
				   "mintypes.h", "valtypes.h", } },
	{ "modid", ResList { "%m" } },
	{ "netypes", ResList { "%m", "aggtypes.h", "expbase.h", "exptypes.h",
			       "fifo.h", "ifcmodel.h", "mintypes.h",
			       "plinterface.h", "selbase.h", "selhelpers.h",
			       "typedefs.h", "valtypes.h" } },
	{ "pds", ResList { "%m", "aggtypes.h", "basetype.h", "chelpers.h",
			   "dbif.h", "enumbase.h", "enums.h", "expbase.h",
			   "exptypes.h", "fifo.h", "fixrefs.h", "idgen.h",
			   "ifcmodel.h", "mintypes.h", "plctx.h",
			   "plinterface.h", "selbase.h", "selhelpers.h",
			   "typedefs.h", "utils.h",
			   "valtypes.h" } },
	{ "plinterface", ResList { "%m", "dbif.h", "expbase.h", "expfactory.h",
				   "exptypes.h", "fifo.h", "fixrefs.h",
				   "ifcmodel.h", "instdestroy.h", "mintypes.h",
				   "storedata.h", "storemeta.h", "symbols.cc",
				   "outputops.h", "typedefs.h", "utils.h",
				   "valtypes.h" } },
	{ "selbase", ResList { "%m", "expbase.h", "exptypes.h", "fifo.h",
			       "gentypedvalue.h", "ifcmodel.h", "mintypes.h",
			       "plinterface.h", "selhelpers.h", "typedefs.h",
			       "valtypes.h" } },
	{ "selhelpers", ResList { "%m", "expbase.h", "exptypes.h", "fifo.h",
				  "ifcmodel.h", "mintypes.h", "plinterface.h",
				  "seltypelist.cc", "typedefs.h",
				  "valtypes.h" } },
	{ "storedata", ResList { "%m", "dbif.h", "expbase.h", "exptypes.h",
				 "fifo.h", "fixrefs.h", "idgen.h", "mintypes.h",
				 "pds.h", "plctx.h", "outputops.h",
				 "typedefs.h", "utils.h", "valtypes.h" } },
	{ "storemeta", ResList { "%m", "dbif.h", "ifcmodel.h", "plctx.h",
				 "outputops.h" } },
	{ "outputops", ResList { "%m" } },
	{ "utils", ResList { "%m" } },
	{ "valtypes", ResList { "%m", "fifo.h", "mintypes.h", "utils.h" } },
	{ "warning", ResList { "%m" } },
    };

    // Generate the 'Makefile'. The third argument is the suffix used for the
    // object files
    gen_make (st, deplist, ".o", ".so", ofdir);
    cout << ". done" EOL << flush;

    // Last not least, a 'Makefile' must be generated, which allows for the
    // IFC "plugin" being generated. The generation of the "plugin" itself
    // will not be done here, as this would deny an implementor the inclusion
    // of extra files, optimisations, etc.

    return true;
}

} /*namespace CodeGen::CPP*/
