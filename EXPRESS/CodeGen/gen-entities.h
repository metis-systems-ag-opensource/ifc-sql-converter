/*! \file gen-entities.h
**  Part of the plugin code generator
**
**  Generate C++ counterparts for the non-abstract ENTITY definitions.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
** Interface file for 'gen-entities.cc'
** (Code generator for a single entity. Because all entities, SELECT types and
**  defined types are written to a single module source, this generator doesn't
**  use the "output path" scheme the other generators use. Instead, it writes
**  to two output channels. The generator for the remaining types - which uses
**  this ENTITY generator - will have the same output scheme as the other
**  generators.)
**
*/
#ifndef GEN_ENTITY_H
#define GEN_ENTITY_H

#include "symtab.h"
#include "gen-typedeps.h"

void gen_entities (Symtab &st, const TypeIndexList &til, const string ofpfx);

#endif /*GEN_ENTITY_H*/
