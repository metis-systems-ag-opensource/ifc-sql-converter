/* gen-entities.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Code generator for a single entity. Because all entities, SELECT types and
** defined types are written to a single module source, this generator doesn't
** use the "output path" scheme the other generators use. Instead, it writes
** to two output channels. The generator for the remaining types - which uses
** this ENTITY generator - will have the same output scheme as the other
** generators.
**
*/

#include <fstream>
#include <string>
#include <stdexcept>
#include <filesystem>


#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "attrtype.h"

#include "gen-entities.h"

using std::string, std::to_string, std::runtime_error, std::domain_error;
namespace fs = std::filesystem;

static const char *defn_head =
    "/* %hfile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, Metis AG" EOL
    "** " EOL
    "** Implementations of the member functions of the ENTITY classes." EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE." EOL
    "**" EOL
    "*/" EOL
    "#ifndef %HFILE" EOL
    "#define %HFILE" EOL
    "" EOL
    "#include <cstddef>" EOL
    "#include <cstdint>" EOL
    "" EOL
    "#include \"valtypes.h\"" EOL
    "" EOL
    "#include \"chelpers.h\"" EOL
    "#include \"expbase.h\"" EOL
    "#include \"netypes.h\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
;

static const char *defn_foot =
    "" EOL
    "} /*namespace IFC*/" EOL
    "" EOL
    "#endif /*%HFILE*/" EOL
;

static const char *emptystruct_tmpl =
    "" EOL
    "// %{abstract}ENTITY %type (no attributes)" EOL
    "struct %type : %basetypes {" EOL
    "};" EOL
;

static const char *struct_tmpl =
    "" EOL
    "// %{abstract}ENTITY %type" EOL
    "struct %type : %basetypes {" EOL
    "    %type();" EOL
    "    %type (size_t instid, const STEP::EntityValue &ent);" EOL
    //"    %type (const %type &x) = delete;" EOL
    //"    %type (%type &&x) = delete;" EOL
    "    ~%type();" EOL
    "    ExpTypes type() const;" EOL
    "    void set (uint32_t ix, const STEP::Value *v);" EOL
    "};" EOL
;

static const char *impl_head =
    "/* %ifile" EOL
    "**" EOL
    "** Author: Boris Jakubith" EOL
    "** E-Mail: b.jakubith@metis-ag.com" EOL
    "** Copyright: (c) 2019, Metis AG" EOL
    "** " EOL
    "** Implementations of the member functions of the ENTITY classes." EOL
    "**" EOL
    "** AUTOMATICALLY GENERATED FILE." EOL
    "**" EOL
    "*/" EOL
    "" EOL
    "#include <stdexcept>" EOL
    "#include <string>" EOL
    "" EOL
    "#include \"enums.h\"" EOL
    "#include \"plinterface.h\"" EOL
    "#include \"aggtypes.h\"" EOL
    "" EOL
    "#include \"%hfile\"" EOL
    "" EOL
    "namespace IFC {" EOL
    "" EOL
    "using std::domain_error, std::exception;" EOL
    "using std::to_string;" EOL
    "" EOL
;

static const char *impl_foot =
    "" EOL
    "} /*namespace IFC*/" EOL
;

static const char *memfunc1_tmpl = EOL
    "// %{abstract}ENTITY %type" EOL
    // The empty constructor
    "%type::%type()" EOL
    "{" EOL
    "    attributes.resize (%size, nullptr);" EOL
    "}" EOL
    "" EOL
    // The only valid value constructor
    "%type::%type (size_t instid, const STEP::EntityValue &ent)" EOL
    "{" EOL
    "    size_t nattrs = ent.attributes.size(), nxpattrs = %virtsize;" EOL
    "    if (nattrs != nxpattrs) {" EOL
    "\tstd::string ename = id2name (ExpTypes::%type);" EOL
    "\tthrow domain_error (" EOL
    "\t    \"Instance #\" + to_string (instid) + \" (\" + ename + \"): \"" EOL
    "\t    \"STEP-Value has the wrong number of attribute values (\" +" EOL
    "\t    to_string (nattrs) + \", expected: \" + to_string (nxpattrs) + \")\"" EOL
    "\t);" EOL
    "    }" EOL
    "    attributes.resize (%size, nullptr);" EOL
    "    instanceid (instid);" EOL
    "    uint32_t ix = 0;" EOL
    "    for (auto val: ent.attributes) {" EOL
    "\tset (ix++, val);" EOL
    "    }" EOL
    "}" EOL
    "" EOL
    // There is _no_ copy constructor, because such an ENTITY-object should
    // never be copied (this model works through pointers).
    //"%type::%type (const %type &x) = delete;" EOL
    //"" EOL
    // There is _no_ move constructor
    //"%type::%type (%type &&x) = delete;" EOL
    //"" EOL
    // The destructor must remove all values, save from the ENTITY references
    "%type::~%type()" EOL
    "{" EOL
    "    uint32_t ix = 0;" EOL
    "    for (ExpBase *attrv: attributes) {" EOL
    "\t// Ignore empty values and references of other ENTITY-values" EOL
    "\tif (attrv && ! dynamic_cast<EntityBase *>(attrv)) {" EOL
    "\t    delete attrv;" EOL
    "\t}" EOL
    "\tattributes[ix++] = nullptr;" EOL
    "    }" EOL
    "    attributes.clear();" EOL
    "}" EOL
    "" EOL
    // The type tag of the type
    "ExpTypes %type::type() const" EOL
    "{" EOL
    "    return ExpTypes::%type;" EOL
    "}" EOL
    "" EOL
;

static const char *memfunc2_tmpl = EOL
    // The setter member function (requires an index and a 'STEP::Value *')
    "void %type::set (uint32_t ix, const STEP::Value *v)" EOL
    "{" EOL
    "    //ExpBase *xv = nullptr;" EOL
    "    STEP::ValType vtype = v->type();" EOL
    "    const EAProperties &eap = get_eaproperties (ix, ExpTypes::%type);"
    EOL
    "    uint32_t mx = eap.mx;" EOL
    "    if (eap.is_derive) {" EOL
    "\tif (vtype != STEP::ValType::unspecified) {" EOL
    "\t    writeAddErr (\"Can't specify any value save from '*' for a\"" EOL
    "\t\t\t \" DERIVE attribute\", ix);" EOL
    "\t}" EOL
    "\treturn;" EOL
    "    }" EOL
    "    if (vtype == STEP::ValType::unspecified) {" EOL
    "\twriteAddErr (\"The value '*' is invalid here\", ix);" EOL
    "\treturn;" EOL
    "    }" EOL
    "    if (attributes[mx]) { delete attributes[mx]; attributes[mx] ="
    " nullptr; }" EOL
    "    if (vtype == STEP::ValType::null) {" EOL
    "\tif (! eap.is_optional) {" EOL
    "\t    writeAddErr (\"'$' is invalid for mandatory attributes\", ix);" EOL
    "\t}" EOL
    "\treturn;" EOL
    "    }" EOL
    "    try {" EOL
    "\tswitch (ix) {" EOL
;

static const char *memfunc3_tmpl = 
    "\t    default: {" EOL
    "\t\twriteAddErr (\"There is no such attribute\", ix); break;" EOL
    "\t    }" EOL
    "\t}" EOL
    "    } catch (exception &e) {" EOL
    "\twriteAddErr (e.what(), ix);" EOL
    "    }" EOL
    "}" EOL
;

static const char *setcase_tmpl =
    "\t    case %index: {" EOL
    "\t\tattributes[mx] = new %attrtype (v);" EOL
    "\t\tbreak;" EOL
    "\t    }" EOL
;

static const char *nofuncs_tmpl =
    "" EOL
    "// %{abstract}ENTITY %type (no attributes)" EOL
;


struct IdIndexAttr {
    Ident id; uint32_t index; Attr *attr;
};

static void gen_entity (Symtab &st, Ident id, //const TypeIndexList &til,
			std::ostream &hout, std::ostream &iout)
{
    auto ets = dynamic_cast<TsEntity *>(st[id]);
    if (! ets) { return; }
    // Traversing the complete dependency tree for getting the minimum index
    // to begin with ...
    string basetypes;
    bool first = true;
    IdentList &supertypes = ets->mysupertypes;
    // Damit das Ganze korrekt abläuft, müssen die Attribute der SUPERTYPEs
    // (flatAttributes) eingesammelt werden, und zwar in der Reihenfolge, in
    // der die SUPERTYPEn vorliegen. Da die Attribute eines SUPERTYPEs jedoch
    // evtl. versetzt angeordnet werden müssen (gegenüber ihrer normalen
    // Position, die für ein Attribut ausschließlich des jeweilige SUPERTYPEs
    // gilt) ergibt sich das Problem, das die Setter-Funktion nicht einfach
    // die entsprechende Funktion eines SUPERTYPEs verwenden kann, da diese
    // natürlich nur davon ausgehen kann, dass die Attributpositionen bei
    // 0 beginnen, und nicht versetzt irgendwo mittendrin. Die einzige Lösung
    // die mir hierzu einfällt, besteht darin, dass jede Setter-Funktion jedes
    // Attribut des Attributvektors der ENTITY setzten kann, zu der sie
    // gehört. Alle anderen Möglichkeiten bräuchten irgendwelche Offsets, die
    // irgendwie an die Setter-Funktion des jeweiligen SUPERTYPEs weitergegeben
    // werden müssten (z.B. per Argument). Das wäre jedoch eine sehr unschöne
    // Lösung meines Problems. Letzendlich bleibt mir also nur die zuerst
    // genannte Lösung übrig. (Mal abgesehen von der vergrößerung des
    // generierten Codes stellt das jedoch kein Problem dar, da der Code eben
    // das ist - generiert. Dabei muss dann keine Rücksicht mehr auf eine
    // Optimierung des jeweiligen Codes bzw. auf "Fehleranfälligkeit" geachtet
    // werden, da dieser Code ja nicht manuell verändert wird (bzw. werden
    // sollte).
    // Auch wegen der re-deklarierten DERIVE-Attribute ist die zuerst genannte
    // Lösung notwendig, da diese ja im Attributvektor keinerlei Platz mehr
    // belegen, auch wenn sie es als Attribut des SUPERTYPEs ursprünglich
    // taten.
    if (supertypes.empty()) {
	basetypes = "EntityBase";
    } else {
	for (auto sid: supertypes) {
	    if (first) { first = false; } else { basetypes += ", "; }
	    basetypes += st.symname (sid);
	}
    }
    Substitutes vars {
	{ "type", st.symname (id) },
	{ "basetypes", basetypes },
	{ "index", "0" },
	{ "attrtype", "" }
    };
    // Nächste Schritte:
    // Anzahl der Attribute berechnen.
    uint32_t virtsize = ets->flatAttributes.length();
    uint32_t size = virtsize;
    uint32_t statix = 0;
    Fifo<string> cases;
    for (auto [eid, aid, attr]: ets->flatAttributes) {
	Attr::Type attrtype = attr->type();
	if (attrtype == Attr::derived_attr) {
	    --size;	    // Adjust the size of the attribute vector
	} else if (attrtype == Attr::explicit_attr) {
	    auto eattr = dynamic_cast<ExplicitAttr *>(attr);
	    vars["index"] = to_string (statix);
	    vars["attrtype"] = gen_attrtype (st, id, aid,
					     eattr->definition.get());
	    cases.push (tmpl_subst (setcase_tmpl, vars));
	}
	// else { /*attrtype == Attr::inverse_attr!*/ ... } //YET UNIMPLEMENTED
	++statix;
    }
    // What to do if 'size == 0'?
    vars["abstract"] = (ets->abstract ? "ABSTRACT " : "");
    if (size == 0) {
	hout << tmpl_subst (emptystruct_tmpl, vars);
	iout << tmpl_subst (nofuncs_tmpl, vars);
    } else {
	hout << tmpl_subst (struct_tmpl, vars);
	vars["size"] = to_string (size);
	vars["virtsize"] = to_string (virtsize);
	iout << tmpl_subst (memfunc1_tmpl, vars);
	iout << tmpl_subst (memfunc2_tmpl, vars);
	while (! cases.empty()) { iout << cases.shift(); }
	iout << tmpl_subst (memfunc3_tmpl, vars);
    }
}

void gen_entities (Symtab &st, const TypeIndexList &til, const string ofpfx)
{
    ofstream iout, hout;
    string ifname, hfname;
    std::error_code ec;

    if (fs::is_directory (ofpfx, ec)) {
	ifname = ofpfx / "entities.cc";
	hfname = ofpfx / "entities.h";
    } else {
	ifname = ofpfx + ".cc";
	hfname = ofpfx + ".h";
    }
    if ((fs::exists (ifname, ec) && ! fs::is_regular_file (ifname, ec))) {
	throw runtime_error ("Regular or no file '" + ifname + "' expected");
    }
    if ((fs::exists (hfname, ec) && ! fs::is_regular_file (hfname, ec))) {
	throw runtime_error ("Regular or no file '" + ifname + "' expected");
    }
    iout.open (ifname);
    if (! iout.is_open()) {
	throw runtime_error ("Failed to open '" + ifname + "'");
    }
    hout.open (hfname);
    if (! hout.is_open()) {
	throw runtime_error ("Failed to open '" + hfname + "'");
    }
    Substitutes vars {
	{ "ifile", basename (ifname) },
	{ "hfile", basename (hfname) },
	{ "HFILE", trans_filename (basename (hfname)) }
    };

    iout << tmpl_subst (impl_head, vars);
    hout << tmpl_subst (defn_head, vars);

    TypeIndexList stil = depsort_types (st, til);
    for (auto [id, ix]: stil) {
	TypeStruct *ts = st[id];
	if (ts->type() != TypeStruct::entity) { continue; }
	gen_entity (st, id, /*stil,*/ hout, iout);
    }

    iout << tmpl_subst (impl_foot, vars);
    hout << tmpl_subst (defn_foot, vars);

    hout.close(); iout.close();
}
