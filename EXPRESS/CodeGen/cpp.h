/*! \file cpp.h
**  Code genetator module which creates the C++ plugin source code for a
**  specific IFC schema.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'CodeGen/cpp.cc'
** (Generate the Classes of the IFC-model (C++ style) ...)
**
*/
#ifndef CPP_H
#define CPP_H

#include <string>

#include "symtab.h"

namespace CodeGen::CPP {

/*! Generate the plugin code for the IFC-schema stored in the given symbol
**  table.
**
**  @param st - (a refference to) the symbol table which contains all symbols
**              of the IFC schema.
**
**  @param template_base - the base directory of the source code of the
**              static parts of the plugin code. Later, this will also be the
**              base directory of the templates being used for the parts of
**              the plugin code which are generated from the IFC schema.
*/
bool cgen (Symtab &st,
	   const std::string &template_base,
	   const std::string &outfile_base);

} /*namespace CodeGen::CPP*/

#endif /*CPP_H*/
