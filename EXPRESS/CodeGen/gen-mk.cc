/* gen-mk.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
** 
**
** Generate a Makefile for compiling the generated code
**
*/

#include <iostream>
using std::cerr;

#include <filesystem>
#include <fstream>
#include <map>
#include <stdexcept>

#include "Common/fifo.h"
#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "outstrings.h"

#include "gen-mk.h"

using std::map, std::runtime_error, std::string;
namespace fs = std::filesystem;

static const char *maketmpl =
    "# Makefile" EOL
    "#" EOL
    "# Author: Boris Jakubith" EOL
    "# E-Mail: b.jakubith@metis-ag.com" EOL
    "# Copyright: (c) 2020, Metis AG" EOL
    "#" EOL
    "#" EOL
    "# Makefile for compiling the generated code and generating a plugin" EOL
    "# from it" EOL
    "#" EOL
    "# AUTOMATICALLY GENERATED FILE." EOL
    "" EOL
    "CPP = g++" EOL
    "CPPFLAGS = -std=c++17 -Wall" EOL
    "" EOL
    "LD = g++" EOL
    "LDFLAGS = -g " EOL
    "" EOL
    "DBINCL = $(shell mariadb_config --include)" EOL
    "DBLIBS = $(shell mariadb_config --libs)" EOL
    "" EOL
    "INCLUDES = $(DBINCL)" EOL
    "LIBS = $(DBLIBS)" EOL
    "" EOL
    "OBJS = %OBJS" EOL
    "" EOL
    "TARGET = %plugin" EOL
    "" EOL
    "PLUGINDIR =" EOL
    "" EOL
    "" EOL
    "#%%.o : %%.cc" EOL
    ".cc.o:" EOL
    "\t$(CPP) $(CPPFLAGS) $(INCLUDES) -DPIC -fpic -c $< -o $@" EOL
    "" EOL
    "all: $(TARGET)" EOL
    "" EOL
    "$(TARGET): $(OBJS)" EOL
    "\t$(LD) $(LDFLAGS) $(OBJS) $(LIBS) -shared -o $(TARGET)" EOL
    "" EOL
    "%DEPRULES" EOL
    "" EOL
    "enumtest: enumtest.o enumval.o utils.o" EOL
    "\t$(LD) $(LDFLAGS) $^ -o $@" EOL
    "" EOL
    "errlist-test: errlist-test.o errlist.o" EOL
    "\t$(LD) $(LDFLAGS) $^ -o $@" EOL
    "" EOL
    "clean:" EOL
    "\trm -f *.o $(TARGET) enumtest errlist-test" EOL
    "" EOL
    "install:" EOL
    "\t@if [ -z \"$(PLUGINDIR)\" ]; then \\" EOL
    "\t    echo 1>&2 'Please specify PLUGINDIR=<directory> as argument' \\" EOL
    "\t\t      \"to '$(MAKE)'\"'!'; \\" EOL
    "\t    exit 1; \\" EOL
    "\tfi" EOL
    "\t@echo 'Installing $(TARGET) in $(PLUGINDIR)'" EOL
    "\t@install -c -s '$(TARGET)' '$(PLUGINDIR)'" EOL
;

static string join (const Fifo<string> &l, const string &delim)
{
    string res;

    if (! l.empty()) {
	size_t elc = 0, allsz = 0;
	for (auto &s: l) { ++elc; allsz += s.size(); }
	allsz += (elc - 1) * delim.size();
	res.reserve (allsz + 1);
	auto it = l.begin();
	res = *it;
	for (++it; ! it.atend(); ++it) {
	    res += delim; res += *it;
	}
    }
    return res;
}

void gen_make (Symtab &st, const Deps &deps, const string &objsfx,
	       const string &dllsfx, const string &fnpfx)
{
    string makefile = fnpfx / "Makefile";
    string plugin = lowercase (st.schema()) + dllsfx;

    ofstream mout;

    Substitutes vars {
	{ "plugin", plugin },
    };

    Fifo<string> objlist, deprules;

    mout.open (makefile);
    if (! mout.is_open ()) {
	throw runtime_error ("Attempt to open '" + makefile + "' failed.");
    }

    for (auto &[of, dp]: deps) {
	string ofile = of + (is_suffix (of, objsfx) ? "" : objsfx);
	string bn = ofile.substr (0, ofile.size() - objsfx.size());
	objlist.push (ofile);
	ResList rdeps;
	for (auto &s: dp) {
	    if (s == "%m") {
		rdeps.push (bn + ".cc"); rdeps.push (bn + ".h");
	    } else if (s == "%c") {
		rdeps.push (bn + ".cc");
	    } else {
		rdeps.push (s);
	    }
	}
	string dep = ofile + ": " + join (rdeps, " ");
	deprules.push (dep);
    }
    string objs;
    size_t cc = 6;
    for (auto obj: objlist) {
	size_t objsize = obj.size();
	if (cc + objsize + 2 >= DEF_MAXLL) {
	    objs += " \\" EOL "      "; cc = 6;
	}
	objs += " " + obj; cc += objsize + 1;
    }
    objs.erase (0, 1);

    vars["OBJS"] = objs;
    vars["DEPRULES"] = join (deprules, EOL EOL);

    mout << tmpl_subst (maketmpl, vars);

    mout.close();
}
