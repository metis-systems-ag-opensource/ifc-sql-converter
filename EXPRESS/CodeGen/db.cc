/* CodeGen/db.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Create a database schema from the symbol table ...
**
** The difference to languages like C++ and C# is the fact that
**  a) arrays, lists, bags and sets are the same in a database. They must be
**     represented in some form, but don't have any differences otherwise.
**  b) all values are inherently optional.
**
** The model should consist of database tables, each (non-ABSTRACT) ENTITY
** represented by one of them. 'ABSTRACT SUPERTYPE' entities should not
** represented by tables, as these never have any instances in any IFC-model.
** Multi value attributes require additional tables, but only for the basetype
** of the (scalar) element type. This limits the number of such tables.
** 
** An ENTITY-Attribute must be represented as a (FOREIGN) key into the table
** which represents the ENTITY.
**
** There is a base-table, containing the metadata of the IFC-Model (represented
** by the HEADER section in a STEP file). Each ENTITY table must have an
** (FOREIGN KEY) attribute which points into this table.
*/

//##DEBUG
#include <iostream>
using std::cerr, std::endl, std::flush;
//##

#include <cstddef>
//#include <cctype>
#include <cstdint>
#include <cstring>
#include <vector>
#include <set>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <system_error>
//#include <utility>

#include "Common/fifo.h"
#include "Common/ncless.h"
#include "Common/sutil.h"
#include "typedesc.h"
#include "identbitset.h"
#include "Common/pathcat.h"
#include "Common/sutil.h"
#include "Common/sysutils.h"
#include "db-utils.h"
#include "myassert.h"
#include "gen-typedeps.h"

#include "db.h"

#define SQLSTRINGMAX 32768
#define VARCHARMAX 256
#define VARBINARYMAX 256
#define GUIDATTRNAME "GlobalId"

using namespace std;

namespace CodeGen::DB {

using StringSet = set<string, NCLess>;
template<class T> using LCStringMap = map<string, T, NCLess>;

using IdPair = pair<Ident, Ident>;

struct IdPairLess {
    bool operator() (const IdPair &l, const IdPair &r)
    {
	if (l.first < r.first) { return true; }
	if (l.first > r.first) { return true; }
	return l.second < r.second;
    }
};
using IdPairSet = set<IdPair, IdPairLess>;

using StringPair = pair<string, string>;

using IdName = pair<Ident, string>;


struct StringPairLess : binary_function<StringPair, StringPair, bool> {
    bool operator() (const StringPair &l, const StringPair &r) const
    {
	NCLess ltop;
	if (ltop (l.first, r.first)) { return true; }
	if (ltop (r.first, l.first)) { return false; }
	if (ltop (l.second, r.second)) { return true; }
	return false;
    }
};

using StringPairSet = set<StringPair, StringPairLess>;

using IdentSet = set<Ident>;

struct GenCtx {
    Symtab &st;
    RevHierarchy rh;
    StringSet defined_tables;
    StringSet defined_types;
    StringPairSet fkpairs;
    IdentSet mvtables, mvt_created;
    Fifo<IdName> mvtypes;
    LCStringMap<uint64_t> counters;
    LCStringMap<StringPairSet> table_fk_constraints;
    uint32_t next_typeid;
};

template<class T> static bool has (set<T> itemset, const T &item)
{
    return itemset.count (item) > 0;
}

static bool has (const StringSet &itemset, const string &item)
{
    return itemset.count (item) > 0;
}

template<class K, class V> static bool has (map<K, V> itemmap, K item)
{
    return itemmap.count (item) > 0;
}

template<class V> static bool has(LCStringMap<V> itemmap, string item)
{
    return itemmap.count (item) > 0;
}

static void gen_enumtbldefs (GenCtx &ctx, ofstream &out);
static bool gen_enum (GenCtx &ctx, uint32_t enumtypeid, const string &name,
		      TsEnum *ets, ofstream &out);
static bool gen_select (GenCtx &ctx, const string &name, TsSelect *sts,
			ofstream &out);

static void ins_mvtypes (GenCtx &ctx, ofstream &out);
static bool gen_mvtable (GenCtx &ctx, Ident id, ofstream &out);

static void gen_fkconstraints (GenCtx &ctx, ofstream &out);
static bool gen_entity (GenCtx &ctx, Ident id, const string &name,
			TsEntity *ets, ofstream &out);
void add_views (GenCtx &ctx, ofstream &out);

// Type class enum (defining the constants to be inserted into the
// '_TypeClass' table. These constants are used in 'cgen' directly and also
// in 'gen_mvtable()'.
enum tc { tcnone = 0, tcsimple = 1, tcnumber = 2, tcenumeration = 3,
	  tcentity = 4, tcselect = 5, tcaggregate = 6 };

bool cgen (Symtab &st,
	   const string &template_base,
	   const string &outfile_base)
{
    TypeDesc::init();
    // Collect the datatype objects.
    GenCtx ctx { st };
    //ctx.st = st;
    ctx.rh = get_revhierarchy (st);
    TypeIndexList til = gen_typeindexassoc (st);
    // Calculate the next id for a type description to be inserted into the
    // '_TypeIdName' table.
    ctx.next_typeid = 0;
    for (auto [id, ix]: til) {
	if (ix > ctx.next_typeid) { ctx.next_typeid = ix; }
    }
    ++ctx.next_typeid;

    const string &schema = st.schema();
    string lcschema = lowercase (schema);
/*TODO:
    Case 1: outfile_base is empty
	=> Use the (lowercased) schema name plus the suffix '.sql'
    Case 2: outfile_base is a directory
	=> Append the (lowercased) schema name plus the suffix '.sql'
    Case 3: outfile_base points to no existing file or is a regular file
	=> Check if the name already has the suffix '.sql' and add it if
	   this is not the case.
*/
    FileInfo finfo (outfile_base);
    if (finfo.error()) {
	// This is a *real* error, meaning something went wrong while getting
	// the file information.
	throw system_error
	    (finfo.sys_errno(), system_category(), outfile_base);
    }
    string ofname;
    if (finfo.directory()) {
	ofname = outfile_base / lcschema + ".sql";
    } else {
	ofname = outfile_base;
	if (is_suffix (ofname, ".sql")) { ofname += ".sql"; }
    }

    ofstream out (ofname);
    if (! out.is_open()) {
	// ERROR!!!
	throw runtime_error ("Attempt to open '" + ofname + "'");
    }

    out << "-- Creating the database for " + schema + " ..." EOL
	   "CREATE DATABASE " + lcschema + ";" EOL;

    out << EOL "-- Selecting the creates database ..."
	   EOL "USE " + lcschema + ";"
	   EOL;

    // Create a logging table for noting problems during the conversion (e.g.
    // using '$' (aka NULL) for non-optional attributes).
    out << EOL "-- ERROR logging table"
	   EOL "CREATE TABLE _ErrorLog ("
	   EOL "  modelid INTEGER NOT NULL,"
	   EOL "  objid INTEGER NOT NULL,"
	   EOL "  instid INTEGER,"
	   EOL "  msg VARCHAR(255)"
	   EOL ");"
	   EOL "CREATE INDEX _ErrorLog_modelobj ON _ErrorLog(modelid, objid);"
	   EOL "CREATE INDEX _ErrorLog_instid ON _ErrorLog(instid);"
	   EOL;

    // The following three tables are meant as a way to to determine the exact
    // type of any attribute of a SELECT/aggregate, or even ENTITY value.
    // The first table contains an entry for each type in the IFCxyz schema;
    // its entries are invariant for all models of this schema, so it is a part
    // of the schema of the IFCxyz database and is filled with INSERT queries
    // within the geberated SQL script.

    // The type classes distinguish the class of a type of an object (e.g.
    // entity, aggregate etc.).
    out << EOL "-- Type classes (none, simple, entity, select, aggregate)" EOL
	   "CREATE TABLE _TypeClass (" EOL
	   "  `id` TINYINT NOT NULL PRIMARY KEY," EOL
	   "  `name` VARCHAR(20) NOT NULL" EOL
	   ");" EOL
	   "CREATE UNIQUE INDEX _TypeClass_name ON _TypeClass(`name`);" EOL;

    //out << EOL "-- Deactivating AUTOCOMMIT ..."
    //       EOL "SET AUTOCOMMIT = 0;" EOL;
    //out << EOL "BEGIN;" EOL;
    //out << EOL "LOCK TABLE `_TypeClass` WRITE;" EOL;
    // The (currently) valid entries of the '_TypeClass' table
    out << "INSERT INTO `_TypeClass` VALUES" EOL
	   " (" << tcnone << ", 'none')," EOL
	   " (" <<tcsimple << ", 'simple')," EOL
	   " (" <<tcnumber << ", 'number')," EOL
	   " (" <<tcenumeration << ", 'enumeration')," EOL
	   " (" <<tcentity << ", 'entity')," EOL
	   " (" <<tcselect << ", 'select')," EOL
	   " (" <<tcaggregate << ", 'aggregate');" EOL;
    //out << "UNLOCK TABLES;" EOL;
    //out << "COMMIT;" EOL;

    // The type name/type id association. The additional 'typeclass' attribute
    // is a reference into the '_TypeClass' table.
    out << EOL "-- Type name/id association" EOL
	   EOL "-- Case-independent search is good here." EOL
	   "CREATE TABLE _TypeIdName (" EOL
	   "  `id` INTEGER NOT NULL PRIMARY KEY," EOL
	   "  `name` VARCHAR(255) NOT NULL," EOL
	   "  `typeclass` TINYINT NOT NULL," EOL
	   "  FOREIGN KEY (`typeclass`) REFERENCES _TypeClass(`id`)" EOL
	   ");" EOL
	   "CREATE UNIQUE INDEX _TypeIdName_name ON _TypeIdName(name ASC);"
	   EOL EOL;

    //out << "BEGIN;" EOL;
    //out << "LOCK TABLE `_TypeIdName` WRITE;" EOL;
    out << "INSERT INTO _TypeIdName VALUES" EOL
	   " (0, '_NoType', 0)";
    const char *tin_instmpl = "," EOL " (%id, '%name', %tc)";
    for (auto [id, ix]: til) {
	const string &tname = st.symname (id);
	if (is_prefix ("_STRING", tname) || is_prefix ("_BINARY", tname)) {
	    // Excluding the automatically generated types, e.g. '_STRING_f36'
	    // for 'STRING[36] FIXED'.
	    continue;
	}
	uint8_t tc;
	// Fast resolver for 'defined' types. I need the correct type class
	// for these types in the '_TypeIdName' table ...
	TypeStruct *ts = st[id];
	while (ts->type() == TypeStruct::defined) {
	    Ident btid = dynamic_cast<TsDefined *>(ts)->basetype;
	    ts = st[btid];
	}
	switch (ts->type()) {
	    case TypeStruct::enumeration: tc = tcenumeration; break;
	    case TypeStruct::select:      tc = tcselect; break;
	    case TypeStruct::boolean:     //tc = tcsimple; break;
	    case TypeStruct::logical:     //tc = tcsimple; break;
	    case TypeStruct::integer:     //tc = tcsimple; break;
	    case TypeStruct::real:        //tc = tcsimple; break;
	    case TypeStruct::binary:      //tc = tcsimple; break;
	    case TypeStruct::string:      tc = tcsimple; break;
	    case TypeStruct::number:      tc = tcnumber; break;
	    case TypeStruct::entity:      tc = tcentity; break;
	    case TypeStruct::array:       //tc = tcaggregate; break;
	    case TypeStruct::list:        //tc = tcaggregate; break;
	    case TypeStruct::bag:         //tc = tcaggregate; break;
	    case TypeStruct::set:         tc = tcaggregate; break;
	    default:                      tc = tcnone; break;
	}
	Substitutes vars {
	    { "id", to_string (ix) },
	    { "name", tname },
	    { "tc", to_string (tc) },
	};
	out << tmpl_subst (tin_instmpl, vars);
    }
    out << ";" EOL;
    //out << "UNLOCK TABLES;" EOL;
    //out << "COMMIT;" EOL;

    // The second table defines an association between an '<objid, modelid>'
    // and an entry into the first table. It should be used for entries in all
    // 'multi value' tables and also all values of SELECT tables during the
    // insertion.
    out << EOL "-- Object to type-id assiciation" EOL
	   "CREATE TABLE _ObjectTypeId (" EOL
	   "  `modelid` INTEGER NOT NULL," EOL
	   "  `objid` INTEGER NOT NULL," EOL
	   "  `typeid` INTEGER NOT NULL," EOL
	   "  `stypeid` INTEGER," EOL
	   "  PRIMARY KEY (`modelid`, `objid`)," EOL
	   "  FOREIGN KEY (`typeid`) REFERENCES _TypeIdName(`id`)" EOL
	   ");" EOL
	   "CREATE INDEX _ObjectTypeId_objid ON _ObjectTypeId(`objid`);" EOL
	   "CREATE INDEX _ObjectTypeId_typeid ON _ObjectTypeId(`typeid`);" EOL
	   "CREATE INDEX _ObjectTypeId_stypeid ON _ObjectTypeId(`stypeid`);"
	   EOL;

    bool done = true;

    // Generate the '_EnumValues' table here (outside of 'gen_enum()', because
    // then the INSERT queries for all ENUMERATION values (into '_EnumValues')
    // can be enclosed in one 'LOCK TABLE'/'UNLOCK TABLES' pair.
    gen_enumtbldefs (ctx, out);
    ctx.defined_tables.insert ("_EnumValues");

    //out << EOL "BEGIN;";
    //out << EOL "LOCK TABLE `_EnumValues` WRITE;" EOL;
    // The ENUM tables don't need a back-reference to '_IfcModel', because
    // ENUMERATION values are constants.
    for (auto [id, ix]: til) {
	auto ets = dynamic_cast<TsEnum *>(st[id]);
	if (ets) {
	    const string &etname = st.symname (id);
	    //Ident id = it->id;
	    if (! gen_enum (ctx, ix, etname, ets, out)) { done = false; }
	}
    }
    //out << "UNLOCK TABLES;" EOL;
    //out << "COMMIT;" EOL;

    // CREATE a table for the base-type NUMBER (which is a -hidden- SELECT-
    // type).
    out << EOL "-- Creating a table for 'NUMBER'-values, which cannot"
	   " be represented directly" EOL "-- in SQL" EOL
	   "CREATE TABLE `NUMBER` (" EOL
	   "  `objid` INTEGER NOT NULL," EOL
	   "  `modelid` INTEGER NOT NULL," EOL
	   "  `is_real` BOOLEAN NOT NULL," EOL
	   "  `intval` INTEGER," EOL
	   "  `realval` DOUBLE PRECISION," EOL
	   "  PRIMARY KEY (`modelid`, `objid`)" EOL
	   ");" EOL;

    // Creating a "reference table" which allows for accessing the correct
    // attribute of a SELECT table through the element type of the given
    // instance.
    out << EOL
	   "-- Reference table for allowing an easier access to SELECT tables"
	   EOL "CREATE TABLE `_SelectAccess` ("
	   EOL "  `select_type` VARCHAR(191) NOT NULL,"
	   EOL "  `attribute_type` VARCHAR(191) NOT NULL,"
	   EOL "  `attribute_number` INTEGER NOT NULL,"
	   EOL "  PRIMARY KEY (`select_type`, `attribute_type`)"
	   EOL ");"
	   EOL;

    for (auto [id, ix]: til) {
	auto ts = dynamic_cast<TsSelect *>(st[id]);
	if (ts) {
	    done = gen_select (ctx, st.symname (id), ts, out);
	}
    }

/* Instead of the splitting of entities into tables which contain parts of
** a concrete ENTITY type, i will use here tables which contain all attributes
** the attributes of the current ENTITY as well as well as the attributes of
** the SUPERTYPEs.
*/
    if (done) {
	for (auto [id, ix]: til) {
	    // Skip all non-'TsEntity *'-values and all 'ABSTRACT ENTITY'-types
	    auto ts = dynamic_cast<TsEntity *>(st[id]);
	    if (ts && ! ts->abstract) {
		// Create the table corresponding to the given ENTITY ...
		gen_entity (ctx, id, st.symname (id), ts, out);
	    }
	}
    }

    if (done) {
	// Generate the multi value tables, referenced by the some of the
	// entity attributes are generated here.
	IdentSet &mvtables = ctx.mvtables;
	for (Ident id: mvtables) {
	    //Ident id = *it;
	    if (! gen_mvtable (ctx, id, out)) { done = false; }
	}
	ins_mvtypes (ctx, out);
    }

    // Add some views for an easier access to some data via symbolic values
    // (aka names) instead of numeric values (ids)
    add_views (ctx, out);

    // Last not least, the remaining FOREIGN KEY constraints (the ones not
    // applying to the SUPERTYPE/SUBTYPE relationships and the basic tables)
    // are generated here.
    gen_fkconstraints (ctx, out);
    return done;
}

static void gen_enumtbldefs (GenCtx &ctx, ofstream &out)
{
    out << EOL "-- Defining the table which holds all ENUMERATION values of"
	   " the '" << ctx.st.schema() << "' schema."
	   EOL "-- The association to the ENUMERATION type is done through"
	   " the table"
	   EOL "-- '_TypeIdName'"
	   EOL "CREATE TABLE `_EnumValues` ("
	   EOL "  `enumtypeid` INTEGER NOT NULL,"
	   EOL "  `enumval` INTEGER NOT NULL,"
	   EOL "  `ordernum` INTEGER NOT NULL,"
	   EOL "  `name` CHARACTER VARYING(120) NOT NULL,"
	   EOL "  PRIMARY KEY (`enumval`),"
	   EOL "  FOREIGN KEY (`enumtypeid`) REFERENCES `_TypeIdName` (`id`)"
	   EOL ");"
	   EOL "CREATE INDEX `_EnumValues_enumtypeid` ON"
	   " _EnumValues(`enumtypeid`);"
	   EOL;
}

// I don't know if an enumeration table should be created. On the other hand,
// it may be reasonable for documentation purposes ...
// ENUMERATIONs can be reduced to two tables:
//   a) a base-table which contains a base-id (PRIMARY KEY) and the enum-name,
//      and
//   b) a table which contains a reference to the base-table (the base-table's
//      PRIMARY KEY), a unique id which is a reference to the enumeration
//      value, an order number and the name of the ENUMERATION-value
bool gen_enum (GenCtx &ctx, uint32_t enumtypeid, const string &name,
	       TsEnum *ets, ofstream &out)
{
    Symtab &st = ctx.st;

    // If there is no counter for the ENUMERATION values yet, create one.
    if (! has (ctx.counters, "enumvalid")) {
	ctx.counters.insert (make_pair ("enumvalid"s, (uint64_t)0));
    }

    // Write the INSERT queries for the ENUMERATION values to 'out'.
    out << EOL "-- ENUMERATION type '" + name + "':" EOL;
    uint64_t enumvalid = ctx.counters["enumvalid"];	// Get the counter
    out << "INSERT INTO `_EnumValues` VALUES";
    int order = -1; bool first = true;
    for (Ident id: ets->values) {
	const string &vname = st.symname (id);
	if (first) { first = false; } else { out << ","; }
	out << EOL " (" << enumtypeid << ", " << ++enumvalid << "," <<
	       ++order << ", '" << vname << "')";
//	out << "INSERT INTO _EnumValues VALUES (" << enumtypeid << ", " <<
//	       ++enumvalid << ", " << ++order << ", '" << vname << "');" EOL;
    }
    out << ";" EOL;
    ctx.counters["enumvalid"] = enumvalid;	// Store the modified counter
    return true;
}

using BtTuple = tuple<Ident, string, bool, TypeStruct *>;

BtTuple get_basetype (GenCtx &ctx, Ident id)
{
    Symtab &st = ctx.st;
    TypeStruct *ts = st[id];
    TypeStruct::Type tstype = ts->type();
    switch (tstype) {
	case TypeStruct::boolean: {
	    return make_tuple (st["BOOLEAN"], "BOOLEAN", true, ts);
	}
	case TypeStruct::logical: {
	    return make_tuple (st["LOGICAL"], "BOOLEAN", true, ts);
	}
	case TypeStruct::integer: {
	    return make_tuple (st["INTEGER"], "INT", true, ts);
	}
	case TypeStruct::real: {
	    return make_tuple (st["REAL"], "DOUBLE PRECISION", true, ts);
	}
	case TypeStruct::number: {
	    return make_tuple (st["NUMBER"], "NUMBER", false, ts);
	}
	case TypeStruct::binary:
	case TypeStruct::string: {
	    bool is_binary = tstype == TypeStruct::binary;
	    auto bts = dynamic_cast<TsStringLike *>(ts);
	    size_t size = bts->size;
	    bool varying = ! bts->fixed;
	    string dbtype = (is_binary ? "BINARY" : "CHAR" /*"CHARACTER"*/);
	    if (size == 0) {
		// NOTE: EXPRESS strings without explicitely specified size are
		// always varying in size, so there is no problem in specifying
		// a maximum size if the type wasn't specified with a size.
		size = SQLSTRINGMAX;
		if (is_binary) { size = (size >> 3) + (size & 7 ? 1 : 0); }
	    }
	    if (size >= VARCHARMAX) {
		// Due to restrictions in MariaDB, i had to change (most of)
		// the VARCHAR and VARBINARY attribute types into 'TEXT' and
		// 'BLOB'.
		dbtype = (is_binary ? "BLOB" : "TEXT");
	    } else if (varying) {
		dbtype = (is_binary ? "VARBINARY" : "VARCHAR");
	    }
	    dbtype += "(" + to_string (size) + ")";
	    Ident tid = st[is_binary ? "BINARY" : "STRING"];
	    return make_tuple (tid, dbtype, true, ts);
	}
	case TypeStruct::defined: {
	    Ident btid = dynamic_cast<TsDefined *>(ts)->basetype;
	    return get_basetype (ctx, btid);
	}
	case TypeStruct::entity: {
	    // NEW: For any entity, the corresponding ROOT in the object tree
	    //      must be found. This leads to an invocation of the function
	    //      'get_roottypes (const RevHierarchy &, Ident)' on 'ctx.rh'.
	    //      The result then is used as basetype instead of the original
	    //      ENTITY type, meaning, there is an INTEGER key attribute
	    //      which points to the corresponding root entity (table).
	    RevHierarchy &rh = ctx.rh;
	    set<Ident> root_types = get_roottypes (rh, id);
	    size_t rt_size = root_types.size();
	    myassert (rt_size <= 1,
		      st.symname (id) + ": Can't determine an attribute type"
		      " if the basetype has more than one 'root' node.");
	    Ident rtid = (rt_size == 0 ? id : *(root_types.begin()));
	    string dbtype = uppercase (ctx.st.symname (rtid));
	    return make_tuple (rtid, dbtype, false, ctx.st[rtid]);
	}
	default: {
	    string dbtype = uppercase (st.symname (id));
	    return make_tuple (id, dbtype, false, ts);
	}
    }
}



static Ident decompose_list (GenCtx &ctx, TypeStruct *ts);

// SELECT-types need to be handled like ENTITY-types in some way, meaning:
// they need their own "instance ids", but must also be attached to a specific
// model. Any attribute of an ENTITY-instance of a specific SELECT-type must be
// expressed with an 'objid'; the necessary 'modelid' is always the same as
// that of the ENTITY.
// CREATE TABLE <select type> (
//   objid INTEGER NOT NULL,
//   modelid INTEGER NOT NULL,
//   attrid INTEGER NOT NULL,
//   ...
//   <type>_id INTEGER NOT NULL,
//   PRIMARY KEY (modelid, objid),
//   FOREIGN KEY (modelid) REFERENCES _IfcModel(id)
// );
// ...
// CREATE INDEX <select type>_<type>_idx ON <select type> (<type>_id);
// ...
static bool gen_select (GenCtx &ctx, const string &name, TsSelect *sts,
			ofstream &out)
{
//    auto sts = dynamic_cast<TsSelect *>(ts);
    string tblname = uppercase (name);
    //string tblname = (name[0] == '_' ? "X" : "") + uppercase (name);
    if (has (ctx.defined_types, name)) {
	return true;
    }
    if (sts->flatTypes.empty()) {
	throw runtime_error (name + "'s 'flatTypes' list is empty!");
    }
    out << EOL "-- IFC Type: " << name << " (SELECT)" EOL
	   "CREATE TABLE `"  << tblname << "` (" EOL
	   "  `objid` INTEGER NOT NULL," EOL
	   "  `modelid` INTEGER NOT NULL," EOL;
    // Need something for identifying the concrete type behind the instance
    // of a SELECT type object.
    out << "  `fieldno` INTEGER NOT NULL," EOL;
    // IMPORTANT! Need to decompose 'defined types', because in the database
    // is no place for tables which contain (apart from an 'objid') only a
    // single simple value
    Fifo<pair<string, string>> indexlist, enumindexlist;
    Fifo<pair<string, uint16_t>> selectlist;
    IdentList &types = sts->flatTypes;
    uint16_t ix = 0;
    //IdentSet inserted;
    for (Ident id: types) {
	Ident t_id; //Ident id = *it;
	string dbtype, attrname;
	bool is_elementary;
	TypeStruct *attrts;
	tie (t_id, dbtype, is_elementary, attrts) = get_basetype (ctx, id);
	const string &t_name = ctx.st.symname (t_id);
	const string &name = ctx.st.symname (id);
	if (is_elementary) {
	    attrname = lowercase (name) + "_val";
	    //if (has (inserted, t_id)) { continue; }
	    //inserted.insert (t_id);
	    //attrname = "val" + to_string (ix);
	    selectlist.push (make_pair (name, ix++));
	    out << "  `" << attrname << "` " << dbtype << "," <<
		   " -- " << name << EOL;
	} else {
	    switch (attrts->type()) {
		case TypeStruct::array:
		case TypeStruct::list:
		case TypeStruct::bag:
		case TypeStruct::set: {
		    Ident etid = decompose_list (ctx, attrts), btid;
		    string dbtype; bool simple; TypeStruct *ts;
		    tie (btid, dbtype, simple, ts) = get_basetype (ctx, etid);
		    //if (has (inserted, t_id)) { continue; }
		    //inserted.insert (t_id);
		    string attrname = lowercase (t_name) + "_mvid";
		    //string attrname = "mvid" + to_string(ix++);
		    const string &btname = ctx.st.symname (btid);
		    out << "  `" << attrname << "` INTEGER," <<
			   " -- AGGREGATE OF " << name << EOL;
		    indexlist.push (make_pair (attrname, btname + "_MV"));
		    ctx.mvtables.insert (btid);
		    selectlist.push (make_pair (name, ix++));
		    break;
		}
		case TypeStruct::entity: {
		    //if (has (inserted, t_id)) { continue; }
		    //inserted.insert (t_id);
		    string attrname = lowercase (name) + "_id";
		    out << "  `" << attrname << "` INTEGER," <<
			   " -- " << name << EOL;
		    indexlist.push (make_pair (attrname, dbtype));
		    selectlist.push (make_pair (name, ix++));
		    break;
		}
		case TypeStruct::enumeration: {
		    //if (has (inserted, t_id)) { continue; }
		    //inserted.insert (t_id);
		    string attrname = lowercase (name) + "_enum";
		    out << "  `" << attrname << "` INTEGER," <<
			   " -- " << name << EOL;
		    enumindexlist.push (make_pair (attrname, dbtype));
		    selectlist.push (make_pair (name, ix++));
		    break;
		}
		case TypeStruct::number: {
		    string attrname = "numid" + to_string (ix++);
		    out << "  `" << attrname << "` INTEGER," <<
			   " -- " << name << EOL;
		    indexlist.push (make_pair (attrname, dbtype));
		    selectlist.push (make_pair (name, ix++));
		    break;
		}
		default: {
		    //Ident id1; string tname; bool is_simple; TypeStruct *ts;
		    //tie (id1, tname, is_simple, ts) =
		    //    get_basetype (ctx, id);
		    //if (has (inserted, t_id)) { continue; }
		    //inserted.insert (t_id);
		    string attrname = lowercase (t_name) + "_id";
		    //string attrname = "id" + to_string(ix++);
		    out << "  `" << attrname << "` INTEGER," <<
			   " -- " << name << EOL;
		    indexlist.push (make_pair (attrname, dbtype));
		    selectlist.push (make_pair (name, ix++));
		    break;
		}
	    }
	}
    }
    // Define the foreign key constraints
    StringPairSet &fkpairs = ctx.fkpairs; fkpairs.clear();
    for (auto &ixpair: indexlist) {
	const string &attrname = ixpair.first;
	const string &dbtype = ixpair.second;
	//tie (attrname, dbtype) = *it;
	fkpairs.insert (make_pair (attrname, dbtype));
    }
    for (auto &ixpair: enumindexlist) {
	const string &attrname = ixpair.first;
	const string &dbtype = ixpair.second;
	out << "  -- Enumeration value of type '" << dbtype << "'" EOL
	       "  FOREIGN KEY (`" << attrname << "`) REFERENCES `_EnumValues`"
	       "(`enumval`)," EOL;
    }
    out << "  -- Primary key specification" EOL
	   "  PRIMARY KEY (`modelid`, `objid`)" EOL
	   ");" EOL;
    out << EOL "-- Inserting into the helper table..."
	   EOL "INSERT INTO `_SelectAccess`"
	   // " (select_type, attribute_type, attribute_number)"
	   " VALUES";
    bool first = true;
    for (auto nxpair: selectlist) {
	const string &elname = nxpair.first;
	uint16_t elx = nxpair.second;
	if (first) { first = false; } else { out << ","; }
	out << EOL " ('" << name << "', '" << elname << "', " << elx << ")";
    }
    out << ";" EOL;
    ctx.table_fk_constraints.insert (make_pair (tblname, fkpairs));
    return true;
}

// 'gen_mvtable()' collects the names of the generated '_MV' tables, along with
// their (new) type ids, in a list 'mvtypes' in 'ctx'. This routine creates
// a bulk insert statement for '_TypeIdName' for these new types.
static void ins_mvtypes (GenCtx &ctx, ofstream &out)
{
    bool first = true;
    string tinvaltmpl = EOL "  (%id, '%name', %tc)";
    out << EOL "-- Insert all aggregate types into the '_TypeIdName' table"
	   EOL "INSERT INTO `_TypeIdName` VALUES";
    for (auto &[id, name]: ctx.mvtypes) {
	if (first) { first = false; } else { out << ","; }
	Substitutes vars {
	    { "id", to_string (id) },
	    { "name", name },
	    { "tc", to_string (tcaggregate) }
	};
	out << tmpl_subst (tinvaltmpl, vars);
    }
    out << ";" EOL;
}

// 'gen_mvtable()' generates an '_MV' table; this type of table is the database
// representation of a (probably multi-dimensional) aggregate structure. For
// each table, two values in the "context" argument 'ctx' are modified. The
// first one is a 'set', which holds all already "created" '_MV' tables. The
// second one is a 'Fifo'-list of pairs, consisting of a type id (generated in
// this function) and the name of the generated table. This list of pairs is
// later used in 'ins_mvtypes()' for creating the '_TypeIdName' entries for
// these "aggregate" with a bulk INSERT query. The name of the value attribute
// depends on the basetype of the corresponding '_MV' table. For simple types
// the attribute's name is simply 'val'; for ENUMERATION values, it consists of
// the type name of the 'ENUMERATION' type and a suffix '_enum'. For all other
// types, it consists or the type name of the basetype with a suffix '_id'
// appended.
static bool gen_mvtable (GenCtx &ctx, Ident id, ofstream &out)
{
    Symtab &st = ctx.st;
    Ident id1; string tname; bool is_simple; TypeStruct *ts;
    tie (id1, tname, is_simple, ts) = get_basetype (ctx, id);
    if (has (ctx.mvt_created, id1)) { return true; }
    string name = st.symname (id1);
    string mvname = (name[0] == '_' ? "X" : "") + uppercase (name) + "_MV";
    string rtype, valname;
    if (is_simple) {
	rtype = tname; valname = "val";
    } else {
	StringPairSet fkpairs;
	if (ts->type() == TypeStruct::enumeration) {
	    valname = name + "_enum";
	    fkpairs.insert (make_pair (valname, "_EnumValues"));
//##was:	    fkpairs.insert (make_pair ("val", "_EnumValues"));
	} else {
	    valname = name + "_id";
	    fkpairs.insert (make_pair (valname, tname));
//##was:	    fkpairs.insert (make_pair ("val", tname));
	}
	ctx.table_fk_constraints.insert (make_pair (mvname, move(fkpairs)));
	rtype = "INTEGER";
    }
//cerr<<"##A3: |"<<mvname<<"|: valname = '"<<valname<<"', rtype = "<<rtype<<EOL;
    // Like with the SELECT-tables, the MV (multi value) tables need its own
    // `objid` which is used to refer MV values (ARRAY, LIST, BAG, SET) in
    // multi-value attributes of ENTITY instances. And again, each entry in
    // these tables is attached to a specific model (identified by the
    // 'modelid'). The pair <modelid, objid> references the multi-value
    // object uniquely within the database, whereas the tuple
    // <modelid, objid, ix> identifies a single value within this
    // multi-value objects. The identifies a multi-value sub-value of
    // the dimension of ...
    // Again, the 'modelid' is the same as the one of the ENTITY-instance
    // which refers such a multi-value object.
    out << EOL;
    out << "-- Multi value table (ARRAY, ...) for '" << name << "' values" EOL;
    out << "CREATE TABLE `" << mvname << "` (" EOL
	   "  `modelid` INTEGER NOT NULL," EOL
	   "  `objid` INTEGER NOT NULL," EOL
	   "  `ix` INTEGER NOT NULL," EOL
	   "  `sa_id` INTEGER," EOL
	   "  `" << valname << "` " << rtype << "," EOL
	   "  PRIMARY KEY (`modelid`, `objid`, `ix`)" EOL
	   ");" EOL;
    out << "CREATE INDEX `" << mvname << "_IDX` ON `" << mvname <<
	   "`(`objid`);" EOL;
    out << "CREATE INDEX `" << mvname << "_IXX` ON `" << mvname <<
	   "`(`ix`);" EOL;
    out << "CREATE INDEX `" << mvname << "_SAX` ON `" << mvname <<
	   "`(`sa_id`);" EOL;

    // Collect the generated type ids and names for a later bulk insertion into
    // the 'TypeIdName' table ...
    ctx.mvtypes.push (make_pair (ctx.next_typeid++, mvname));

    // Note the table with the element type 'id1' as created.
    ctx.mvt_created.insert (id1);
    return true;
}


static bool get_attrdata (GenCtx &ctx, Ident aid, ExplicitAttr *attr,
			  stringstream &out);

static bool add_attributes (GenCtx &ctx, const string &name, TsEntity *ets,
			    stringstream &tmp);

// This routine is _VERY_ IFC-specific. It searches for the id of the 'IfcRoot'
// entity in the collected list of SUPERTYPEs of the given ENTITY '*ets'.
static bool has_guid (Symtab &st, TsEntity *ets)
{
    Ident ifcroot = st["IfcRoot"];
    for (auto id: ets->mysupertypes) {
	if (id == ifcroot) { return true; }
    }
    for (auto id: ets->mysupertypes) {
	if (has_guid (st, dynamic_cast<TsEntity *>(st[id]))) { return true; }
    }
    return false;
}
    

/* I have no longer any distinction between SUPERTYPEs and SUBTYPEs, and also
** no longer between "leaf" entities and non-"leaf" entities. Instead, for any
** ENTITY a table is generated with the full list of (explicit) attributes.
** But now, any instance (for a specific model) has a unique Id (i no longer
** distinguish between the tables) which is retrieved using a special table
** '_ModelNextId', which has an instance-counter for each model. As before,
** instance-ids will start with '1'. This leaves the possibility to use '0'
** as an "invalid" reference (a 'nulltpr'-analogue ... not the database
** 'NULL'). The names of all special tables (tables handling the metadata
** - such as this '_ModelNextId' table) will begin with an '_'. The reason
** is very simple: Because EXPRESS identifiers may contain '_', but _always_
** begin with a letter, this schema can be used to prevent name clashes
** between these metadata tables and the normal ENTITY- and SELECT- tables.
** In order to retrieve data via a reference to an instance of a (yet unknown)
** ENTITY, the expected attribute type and all possible sub-types can be
** joined in a UNION; this should (partially?!?) solve problems with the
** SUPERTYPE/SUBTYPE scheme of EXPRESS.
*/
static bool gen_entity (GenCtx &ctx, Ident id, const string &name,
			TsEntity *ets, ofstream &out)
{
    stringstream tmp;
    //EthElement &ethel = ctx.eth[id];
    if (ets->attrOrder.empty()) {
	// This is a special case. If an ENTITY contains no own attribute
	// definitions, it may nonetheless contain attributes inherited from
	// its supertypes. In this case, i'm counting all explicit attributes
	// from the 'flatAttributes' list. If this count is 0, it makes no
	// sense to create a database table for the corresponding entity.
	// This "algorithm" is very simple and may lead to errors, as an
	// ENTITY may contain derived attributes which were all re-declared
	// as DERIVE attributes, thus making the the result again an empty
	// attribute list. So i need to rethink it and (maybe) use an algorithm
	// which results in the number of explicit attributes after all
	// attributes re-declares as DERIVE attributes being removed.
#if 0
	cerr << "Warning: " << name << " doesn't have explicit attributes"
		" - nothing generated." EOL;
#endif
	unsigned xpatcc = 0;
	for (auto &[eid, aid, attr]: ets->flatAttributes) {
	    if (attr->type() == Attr::explicit_attr) { ++xpatcc; }
	}
	if (xpatcc == 0) { return false; }
    }
    myassert (! ets->flatAttributes.empty() ,
	      name + "'s 'flatAttributes' list is empty!");
    string ucname = uppercase (name);
    tmp << EOL "-- (ENTITY-)Table '" << name << "'" EOL
	   "CREATE TABLE `" << ucname << "` (" EOL
	   "  `objid` INTEGER NOT NULL," EOL
	   "  `modelid` INTEGER NOT NULL," EOL;
    if (! add_attributes (ctx, name, ets, tmp)) { return false; }
    tmp << "  PRIMARY KEY (`objid`, `modelid`)" EOL;
    if (has_guid (ctx.st, ets)) {
	tmp << ") COLLATE = utf8mb4_bin;" EOL
	       "CREATE INDEX `" << ucname << "_guid` ON `" << ucname <<
	       "`(`" GUIDATTRNAME "`);" EOL;
    } else {
	tmp << ");" EOL;
    }
    out << tmp.str();
    return true;
}

static bool add_attributes (GenCtx &ctx, const string &name, TsEntity *ets,
			    stringstream &tmp)
{
    bool done = true;
    StringPairSet &fkpairs = ctx.fkpairs;
    fkpairs.clear();
    for (auto [eid, attrid, attr]: ets->flatAttributes) {
	switch (attr->type()) {
	    case Attr::explicit_attr: {
		auto eattr = dynamic_cast<ExplicitAttr *>(attr);
		// Get the attribute's definition
		/*TypeStruct *atts = eattr->definition.get();*/
		/*bool optAttr = eattr->optional;*/
		if (! get_attrdata (ctx, attrid, eattr, tmp)) {
		    done = false;
		}
		tmp << EOL;
		break;
	    }
	    case Attr::derived_attr: {
		// DERIVE attributes are not stored in the database. They are
		// more like stored procedures/constants because they return a
		// value but can't be modified in any way. Attributes of this
		// type are never stored as attributes in a database table.
		// But there is a problem: Re-declarations.
		// A re-declaration can make an attribute of a supertype a
		// DERIVE attribute. In STEP, this is expressed by using a '*'
		// for this re-declared attribute instead of a value. In a
		// database scheme, this means that the corresponding
		// attributes either must have NULL-values (this must be the
		// case if ENTITY-tables contain only values for any concrete
		// ENTITY and the ENTITY data sets are constructed with complex
		// join/... operations), or should not be present (a method
		// which should be used when each ENTITY-table contains all
		// attributes of a concrete ENTITY). Because i'm using the
		// second model, ignoring DERIVE attributes should be the
		// right way.
		// In a later step, stored procedures/functions may be used for
		// generating the corresponding values, but this can also be
		// done in the generated object model (the C++-code - when it
		// is ready).
		break;
	    }
	    case Attr::inverse_attr: {
		// INVERSE attributes ... these should be handled by the
		// database in some way (by defining corresponding constraints
		// or something like that). On the other hand, these
		// constraints must also be handled by the object-model (later),
		// so i // currently don't know what to do here. But because
		// INVERSE attributes are _not_ part of the object attribute
		// structure, no code is generated for them.
		break;
	    }
	    default: break;
	}
    }
    if (done) {
	ctx.table_fk_constraints[uppercase (name)] = fkpairs;
    }
    return done;
}

// Reduce a "defined" type to its base type, return the symbol identifying this
// base type (its index in the symbol table).
//
static Ident decompose_defined (GenCtx &ctx, Ident id)
{
    if (id != 0) {
	TypeStruct *ts = ctx.st[id];
	while (ts->type() == TypeStruct::defined) {
	    id = dynamic_cast<TsDefined *>(ts)->basetype;
	    ts = ctx.st[id];
	}
    }
    return id;
}

// Decompose an AGGREGATE type (ARRAY, LIST, BAG, SET), returning the symbol
// identifying its base type.
static Ident decompose_list (GenCtx &ctx, TypeStruct *ts)
{
    switch (ts->type()) {
	case TypeStruct::array: {
	    auto ats = dynamic_cast<TsArray2 *>(ts);

	    return decompose_list (ctx, ats->basetype);
	}
	case TypeStruct::list:
	case TypeStruct::bag:
	case TypeStruct::set: {
	    auto aggts = dynamic_cast<TsAggregateLike *>(ts);
	    return decompose_list (ctx, aggts->basetype);
	}
	case TypeStruct::typeref: {
	    auto trefts = dynamic_cast<TsTypeRef *>(ts);
	    return trefts->id;
	}
	case TypeStruct::boolean: {
	    return ctx.st["BOOLEAN"];
	}
	case TypeStruct::logical: {
	    return ctx.st["LOGICAL"];
	}
	case TypeStruct::integer: {
	    return ctx.st["INTEGER"];
	}
	case TypeStruct::real: {
	    return ctx.st["REAL"];
	}
	case TypeStruct::number: {
	    return ctx.st["NUMBER"];
	}
	case TypeStruct::binary: {
	    return ctx.st["BINARY"];
	}
	case TypeStruct::string: {
	    return ctx.st["STRING"];
	}
	default: {
	    // ERROR! Should never occur
	    return 0;
	}
    }
}

// Result type for 'get_attrtype()'
using IdType = pair<Ident, TypeStruct *>;

// (Recursively) Resolve the datatype identified ty the symbol 't_in'. This
// means: Deconstruct the type references and 'defined' types and return the
// corresponding <Ident, TypeStruct *> pair. Any other type is returned
// directly (together with the supplied id 't_id') ...
static IdType get_attrtype (GenCtx &ctx, Ident t_id, TypeStruct *t_in)
{
    Ident out_id = 0;
    TypeStruct *out = nullptr;
    switch (t_in->type()) {
	case TypeStruct::typeref: {
	    // Get the corresponding type from the symbol table
	    Ident trefid = dynamic_cast<TsTypeRef *>(t_in)->id;
	    out = ctx.st[trefid];
	    return get_attrtype (ctx, trefid, out);
	}
	case TypeStruct::defined: {
	    Ident trefid = dynamic_cast<TsDefined *>(t_in)->basetype;
	    out = ctx.st[trefid];
	    return get_attrtype (ctx, trefid, out);
	}
	default: {
	    out = t_in; out_id = t_id;
	    break;
	}
    }
    return make_pair (out_id, out);
}

static bool get_attrdata (GenCtx &ctx, Ident aid, ExplicitAttr *attr,
			  stringstream &out)
{
    bool done = true;
    StringPairSet &fkpairs = ctx.fkpairs;
    // 'get_attrtype()' resolves type references and 'defined' types ...
    TypeStruct *ts;
    Ident tid, tid1;
    //bool optAttr = attr->optional;
    const string &name = ctx.st.symname (aid);
    tie (tid, ts) = get_attrtype (ctx, 0, attr->definition.get());
    tid1 = decompose_defined (ctx, tid);
    if (tid1 != tid) { tid = tid1; ts = ctx.st[tid]; }
    switch (ts->type()) {
        // The basic types ('simple types') can (in most cases) directly
        // represented in the database. The only exception is 'NUMBER', which
        // must be represented by a (previously) defined table ...
        case TypeStruct::boolean: {
            out << "  `" << name << "` BOOLEAN" <<
		   /*(optAttr ? "" : " NOT NULL") <<*/ ",";
            break;
        }
        case TypeStruct::logical: {
            // 'LOGICAL' is treated equally to OPTIONAL BOOLEAN...
            out << "  `" << name << "` BOOLEAN" << ",";
            break;
        }
        case TypeStruct::integer: {
            // 'INTEGER' is translated to INT
            out << "  `" << name << "` INTEGER" <<
		   /*(optAttr ? "" : " NOT NULL") <<*/ ",";
            break;
        }
        case TypeStruct::real: {
            // 'REAL' is translated to FLOAT/DOUBLE PRECISION
            out << "  `" << name << "` DOUBLE PRECISION" <<
                   /*(optAttr ? "" : " NOT NULL") <<*/ ",";
            break;
        }
        case TypeStruct::number: {
            // The values of the EXPRESS datatype 'NUMBER' can either be of the
            // type 'INTEGER' or of the type 'REAL'. This means, that 'NUMBER'
            // is merely a pre-defined SELECT datatype of the following
            // (EXPRESS-like) definition:
            //   TYPE NUMBER = SELECT (INTEGER, REAL); END_TYPE;
            // This means that SELECT-values of this types must be represented
            // like all SELECT-typed values ... by the number of elements the
            // SELECT-type consists of:
            //out << "  `" << name << "_id` INTEGER" <<
            //       (optAttr ? "" : " NOT NULL");
	    // On the other hand: Representing each NUMBER-value via an index
	    // into a table doesn't make much sense. Instead, a simpler
	    // solution, using a VARCHAR(20) or VARBINARY(20) should be used.
	    out << "  `" << name << "_num` VARBINARY(20)" <<
		   /*(optAttr ? "" : " NOT NULL") <<*/ ",";
            break;
        }
        case TypeStruct::binary: {
            // 'BINARY' values are normally represented by BIT, BIT (n),
	    // BIT VARYING or BIT VARYING (n), but because
	    // MySQL doesn't have BIT VARYING(n) or VARBIT (n), 'BINARY' or
	    // VARBINARY will be used instead ...
            auto bitts = dynamic_cast<TsBinary *>(ts);
	    size_t size = (bitts->size ? bitts->size : SQLSTRINGMAX);
	    // Calculate the number of bytes required for the datatype
	    // Every four bits are represented by a single hexadecimal digit
	    // which means the size is (bitsize + 3) / 4 * 2 plus one digit in
	    // the range '0' ... '3' preceding the real value.
	    size = (size + 3) >> 2; size = (size << 1) + 1;
	    // NOTE: If the size is not given, the EXPECT-type BINARY is
	    // always varying in size.

	    /* Changed because of restrictions in MariaDB!
	    ** In MariaDB, the maximum size of a (the data of) a DDL statement
	    ** is 65535 bytes. Because BINARY values may lead to greater
	    ** lengths, i'm using the type 'BLOB' (with a given maximum length)
	    ** if the (maximum) length of the attribute is >= VARBINARYMAX
	    ** the standard maximum value for VARCHAR and VARBINARY in most DB
	    ** systems. The drawback is that 'BLOB'-values cannot be used as
	    ** key attributes.
	    */
	    if (size >= VARBINARYMAX) {
		out << "  `" << name << "` BLOB(" << size << ")";
	    } else {
		out << "  `" << name << "` " <<
		       (bitts->fixed ? "BINARY" : "VARBINARY") <<
		       "(" << size << ")";
	    }
	    //if (! optAttr) { out << " NOT NULL"; }
	    out << ",";
            break;
	}
        case TypeStruct::string: {
            // 'STRING' values are represented by CHARACTER, CHARACTER (n),
            // CHARACTER VARYING or CHARACTER VARYING (n)
            auto strts = dynamic_cast<TsString *>(ts);
	    size_t size = (strts->size ? strts->size : SQLSTRINGMAX);
	    // NOTE: If the size is not given, the EXPECT-type STRING is
	    // always varying in size.
	    /* Changed because of restrictions in MariaDB!
	    ** The same restrictions as above (vor BINARY values) hold here
	    ** - for STRING values, so: for a maximum size < VARCHARMAX, i'm
	    ** using 'CHAR' or 'VARCHAR', for greater lengths, i'm using 'TEXT'
	    ** (with a length specification).
	    */
	    if (size >= VARCHARMAX) {
		out << "  `" << name << "` TEXT(" << size << ")";
	    } else {
		out << "  `" << name << "` " << 
		       (strts->fixed ? "CHAR" : "VARCHAR") <<
		       "(" << size << ")";
	    }
            //if (! optAttr) { out << " NOT NULL"; }
	    out << ",";
            break;
        }
        // Row-like structures will be represented by references into another
        // table which is structured in a form to represent such structures ...
        case TypeStruct::array:
        case TypeStruct::list:
        case TypeStruct::bag:
        case TypeStruct::set: {
	    // Decompose the multi-value type of the attribute until a
	    // single value type remains ...
	    Ident eltype = decompose_list (ctx, ts);

	    // Define 'attrname' and declare 'tname' and 'is_simple'. The
	    // latter one is only required, because 'get_basetype()' returns
	    // such a value.
	    string attrname = name + "_mvid", tname; bool is_simple;

	    // Reduce the single value type to its base type. The values of
	    // 'is_simple' and 'ts' are unused, and because the function
	    // already returns after completing this action, the original
	    // value of 'ts' (from above) can promptly be overwritten.
	    tie (eltype, tname, is_simple, ts) = get_basetype (ctx, eltype);

	    // The value of 'tname' returned from 'get_basetype()' is a
	    // database type, which is not used here. Instead, define 'tname'
	    // as the symbol name of the (new) 'eltype' value.
	    tname = ctx.st.symname (eltype);

	    // Mark the requirement to create a multi-value table for 'eltype',
	    // but only if there does not already exists such a mark.
	    if (! has (ctx.mvtables, eltype)) {
		// Collect the data for the generation of the multi value
		// table
		ctx.mvtables.insert (eltype);
	    }

	    // Complete the operation by generating the attribute for the
	    // database table...
	    //out << "  -- Index into a multi-value table of type '" <<
	    //	     tname << "'" " for this attribute" EOL;
	    out << "  `" << attrname << "` INTEGER" <<
                   /*(optAttr ? "" : " NOT NULL") <<*/ ",";
	    out << " -- AGGREGATE OF " << tname;

	    // ... and marking it to be a FOREIGN KEY.
	    StringPair sp = make_pair (attrname, uppercase (tname) + "_MV");
	    fkpairs.insert (move(sp));
	    break;
        }
	case TypeStruct::enumeration: {
	    myassert (tid != 0,
		      "INTERNAL ERROR: Unexpected undefined symbol reference");
	    out << "  `" << name << "_enum` INTEGER" <<
		   /*(optAttr ? "" : " NOT NULL") <<*/ ",";
	    out << " -- ENUMERATION " << ctx.st.symname (tid);
	    break;
	}
        case TypeStruct::select: {
	    // tid == 0 => ERROR (which should never occur)
	    myassert (tid != 0,
		      "INTERNAL ERROR: Unexpected undefined symbol reference");
	    const string &tname = ctx.st.symname (tid);
	    string attrname = name + "_id";
	    out << "  `" << attrname << "` INTEGER" <<
		   /*(optAttr ? "" : " NOT NULL") <<*/ ",";
	    out << " -- SELECT " << tname;
	    string tblname = uppercase (tname);
	    StringPair sp = make_pair (attrname, tblname);
	    fkpairs.insert (move(sp));
	    break;
	}
	case TypeStruct::entity: {
	    // NEW: We no longer have a SELECT-table for the entity type.
	    // Instead, the 'root'-type of the entity must be determined and
	    // a key into the corresponding table be generated.
	    RevHierarchy &rh = ctx.rh;

	    // tid == 0 => ERROR (which should never occur)
	    myassert (tid != 0,
		      "INTERNAL ERROR: Unexpected undefined symbol reference");

	    set<Ident> rootents = get_roottypes (rh, tid);
	    size_t re_size = rootents.size();
	    myassert (re_size <= 1,
		      name + ": Attribute cannot have two or more types.");

	    tid1 = (re_size == 1 ? *(rootents.begin()) : tid);
	    string attrname = name + "_id";
	    const string &ename = ctx.st.symname (tid);
	    string tblname = uppercase (ctx.st.symname (tid1));
	    myassert (attr != nullptr,
		      "INTERNAL ERROR! Attribute '" + ename + "." + attrname +
		      "' is not 'explicit'.");
	    //out << "  -- Attribute of ENTITY '" << ctx.st.symname (tid) <<
	    //	     "'" EOL;
	    out << "  `" << attrname << "` INTEGER" <<
		   /*(attr->optional ? "" : " NOT NULL") <<*/ ",";
	    out << " -- " << ctx.st.symname (tid);
	    fkpairs.insert (make_pair (attrname, tblname));
	    break;
	}
	default:
	    // Ignore the remaining 'TypeStruct' sub-types here!
	    break;
    }
    return done;
}

static void gen_fkconstraints (GenCtx &ctx, ofstream &out)
{
    LCStringMap<StringPairSet> &fk_constraints = ctx.table_fk_constraints;
    for (auto fk_constr: fk_constraints) {
	StringPairSet &cslist = fk_constr.second;
	const string name = fk_constr.first;
	auto listlength = cslist.size();
	if (listlength > 62) {
	    // !0! MySQL allows for a maximum of 64 keys per table. With the
	    // already defined PRIMARY key and '_IfcModel' foreign key, this
	    // allows for a remaining number of 62 keys being defined for this
	    // table.
	    out << EOL
		   "-- FAILED to add FOREIGN KEY constraints for table `" <<
		   name << "`" EOL
		   "\\! echo \"Can't create FOREIGN keys for table '" <<
		   name << "': too many keys (" << cslist.size() <<
			   ", max: 64)\"" EOL;
	    continue;
	}
#if 0	// FOREIGN KEY constraints are impossible for multi-value keys!
	if (listlength > 0) {
	    out << EOL "-- FOREIGN KEY constraints for table `" <<
		   /*(name[0] == '_' ? "X" : "") <<*/ name << "`" EOL;
	    out << "ALTER TABLE `" << /*(name[0] == '_' ? "X" : "") <<*/
		   name << "`";
	    bool first = true;
	    for (auto &fkpair: cslist) {
		const string &attrname = fkpair.first;
		const string &refname = fkpair.second;
		if (first) { first = false; } else { out << ","; }
		out << EOL
		       "  ADD FOREIGN KEY (`" << attrname <<
		       "`, `modelid`) REFERENCES `" <<
		       (refname[0] == '_' ? "X" : "") <<
		       refname << "`(`objid`, `modelid`)";
	    }
	    out << EOL ";" EOL;
	}
#endif
    }
}

void add_views (GenCtx &ctx, ofstream &out)
{
    out << EOL "-- Small helper VIEW for the '_ObjectTypeId' table ..."
	   EOL "CREATE VIEW ObjType AS"
	   EOL "  SELECT O.objid, O.modelid modelid, O.typeid typeid,"
	   EOL "         UPPER(T.name) typename, O.stypeid ctypeid,"
	   EOL "         UPPER(T1.name) ctypename, TC.name typeclass"
	   EOL "  FROM _ObjectTypeId O, _TypeIdName T, _TypeIdName T1,"
	   EOL "       _TypeClass TC"
	   EOL "  WHERE O.typeid = T.id"
	   EOL "    AND IFNULL(O.stypeid, 0) = T1.id"
	   EOL "    AND T.typeclass = TC.id;"
	   EOL;

    out << EOL "-- Small helper VIEW, combining '_EnumValues' with the type"
	   EOL "-- name of the corresponding type name"
	   EOL "CREATE VIEW Enums AS"
	   EOL "  SELECT T.`id` `typeid`, T.name `type`, V.enumval,"
	       " V.name `name`, V.ordernum"
	   EOL "  FROM _TypeIdName T, _EnumValues V, _TypeClass TC"
	   EOL "  WHERE TC.name = 'enumeration'"
	   EOL "    AND TC.id = T.typeclass"
	   EOL "    AND V.enumtypeid = T.id;"
	   EOL;
}

} /*namespace CodeGen::DB*/
