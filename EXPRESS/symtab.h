/*! \file symtab.h
**  Definition of the type of a symbol table.
**
**  The symbol table holds definitions for all symbols defined during the
**  parsing the EXPRESS (IFC-)specification, and also all definitions of
**  pre-defined symbols (like `SIN`, `LENGTH`, ...). Because an EXPRESS
**  specification can have algorithms with their own local definitions
**  which comprisee algorithms (with local definitions ...) symbol table
**  entries must have the ability to hold more than one definition, but
**  with only one of these definitions being visible at a specific point
**  in the EXPRESS specification. This problem is solved by introducing
**  a *stack* of symbol definitions, where each entry contains a `TypeStruct`
**  value (the semantic type of a symbol, see `types.{cc,h}`=, as well as
**  a `depth` value which attaches the given symbol definition to a
**  definition context.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Symbol table for EXPRESS
**
*/
#ifndef SYMTAB_H
#define SYMTAB_H

#include <map>
#include <vector>
#include <string>
#include <cstring>
#include <cstdint>

#include "Common/mintypes.h"
#include "Common/symbols.h"
#include "types.h"

using namespace std;

/*! Symbol table definition
**
*/
class Symtab {
public:
    /*! Each symbol has its own stack of definitions. When using a symbol,
    **  only its top level (top of stack) definition is visible. The
    **  symbol definition stack is implemented as a simple LIFO list.
    */
    struct TSStack {
	TSStack *next;		/*!> Definitions below the current visible. */
	TypeStruct *ts;		/*!> The symbol definition at level `depth` */
	uint32_t depth;		/*!> The depth of the current definition */
	bool checked;		/*!> `true` if validated, `false` otherwise */
    };
private:
    /*! The `context` of a symbol is an additional structure used in some
    **  places in the parser.
    */
    Fifo<std::pair<Ident, TypeStruct *>> context;

    /*! The Name of the EXPRESS schema this symbol table is attached to. */
    string schema_name;

    /*! The (inner) implementation of the definition of symbols, based on
    **  the template type `Symbols` defined in the `Common/symbols.{cc,h}`
    **  module.
    */
    Symbols<TSStack *> symbols;

    /*! The current declaration context level (...) */
    uint32_t depth;

public:
    /*! Default constructor */
    Symtab();

    /*! Explicitely forbid copying by construction. */
    Symtab(Symtab &x) = delete;

    /*! Explicitely forbid copying by assignment. */
    Symtab & operator= (const Symtab &x) = delete;

    /*! Allow moving by construction, because initalization, arguments
    **  and return values should be possible.
    */
    Symtab(Symtab &&x);

    /*! Allow moving by assignment, because initalization, arguments
    **  and return values should be possible.
    */
    Symtab & operator= (Symtab &&x);

    /*! Destroys a symbol table. */
    ~Symtab();

    /*! Returns the name of the (EXPRESS) schema the symbol table is attached
    **  to,
    */
    const string & schema ();

    /*! Attaches the symbol table to a specific (EXPRESS) schema. */
    void schema (const string &schema_name);

    /*! Get the current depth (declaration level) */
    uint32_t level ();

    /*! Begin a new declaration context, pushing the current function/
    **  procedure-id and its definition on the context stack.
    */
    void new_context (Ident id, TypeStruct *def);

    /*! Begin a new declaration context, but without attaching it to a
    **  specific identifier.
    */
    void new_context ();

    /*! Returns the function/procedure-id of the current declaration context.
    */
    Ident contextid ();

    /*! Returns the function/procedure-id of the declaration context at a
    **  specific declaration level.
    */
    Ident contextid (uint32_t level);

    /* Returns the symbol definition of the function/procedure the current
    ** declaration context is attached to.
    */
    TypeStruct *contextdef ();

    /* Returns the symbol definition of the function/procedure the declaration
    ** context at a specific declaration level is attached to.
    */
    TypeStruct *contextdef (uint32_t level);

    // End the current declaration environment, poping the top-level
    // <Ident, Definition>-pair from the context stack.
    void end_context (bool destroy = false);

    /*! Inserts a new symbol into the symbol table.
    **
    **  @returns 'true' if a new entry could be created and 'false' if such an
    **           entry already exists at the current declaration level.
    */
    bool newsymbol (const string &s, Ident &symid, TypeStruct *ts,
		    uint32_t &odepth);

    /*! Makes a symbol in a local context global (if this symbol does not
    **  already have a global definition; otherwise raise an exception.
    */
    void set_toplevel (Ident symid);

    /*! Searches for a symbol by its name.
    **
    **  @returns either the `Ident` value of the symbol, or `0` (the `NULL`
    **           symbol.
    */
    Ident get_symbol (const string &s);

    /*! Replaces the entry with the name in 's' with a new entry (with the name
    **  in 'ns') in the symbol table. Returns 'true' if this replacement was
    **  successful and 'false', otherwise.
    */
    bool replace (const string &s, const string &ns, TypeStruct *ts);

    /*! Returns a symbol's definition, or `nullptr` if the symbol does not have
    **  a definition (yet).
    */
    TypeStruct * operator[] (Ident sid);

    /*! Searches for a symbol (by the symbol name) in the symbol table. If
    **  the symbol does not exist, it is created with an empty definition.
    **
    **  @returns the symbol's `Ident` value.
    */
    Ident operator[] (const std::string &name);

    /*! Re-defines a symbol in a new declaration context, shadowing the
    **  current definition of the symbol.
    */
    uint32_t push_value (Ident sid, TypeStruct *ts);

    /*! Removes the current definition of a given symbol. */
    void pop_value (Ident sid, bool destroy_value = false);

    /*! Returns the context level of a symbol's current definition. */
    uint32_t symlevel (Ident sid);

    /*! Returns a reference to a symbol's name (a constant (immutable) string).
    */
    const string & symname (Ident sid);

    /*! Returns a pointer to a symbol's name (constant (immutable) string).
    */
    const string * psymname (Ident sid);

    /*! Sets the checked-flag of a symbol's current definition. */
    void checked (Ident id, bool ok = true);

    /*! Returns the checked-flag of a symbol's current definition. */
    bool checked (Ident id);

    /*! Returns the `Ident`-value of the first entry in the symbol table. */
    Ident first ();

    /*! Returns the `Ident`-value of the last entry in the symbol table. */
    Ident last ();
} /*Symtab*/;

#endif /*SYMTAB_H*/
