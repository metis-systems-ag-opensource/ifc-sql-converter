/* stmt.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Implementation part of 'stmt.h'
** (Structures for generating statement-trees)
**
*/
#include <iostream>
#include <utility>

#include "stmt.h"

#include "Common/fifoout.h"

using namespace std;

//struct Stmt {
//    enum Type {
//	assignment, return_stmt, escape_stmt, loop_stmt,
//	if_stmt, case_stmt, compound_stmt
//    };
//    Stmt() { }
//    virtual ~Stmt();
//    virtual StmtType type() = 0;
////    Stmt (Stmt &x) = delete;
////    Stmt & operator= (const Stmt &x) = delete;
//} /*Stmt*/;

//struct AssignStmt : public Stmt {
//    VarRef *var; ExprPtr expr;
AssignStmt::AssignStmt() : var(nullptr), expr(nullptr) { }

AssignStmt::AssignStmt (Reference *v, ExprPtr e) : var(v), expr(e) { }

//    AssignStmt (AssignStmt &x) = delete;
AssignStmt::AssignStmt (AssignStmt &&x)
    : var(x.var), expr(x.expr)
{
    where (x.where());
    x.var = nullptr; x.expr = nullptr;
}

AssignStmt::~AssignStmt() { delete var; delete expr; }

Stmt::Type AssignStmt::type() const { return Stmt::assignment; }

//    AssignStmt & operator= (const AssignStmt &x) = delete;

AssignStmt & AssignStmt::operator= (AssignStmt &&x) {
    var = x.var; expr = x.expr; where (x.where());
    x.var = nullptr; x.expr = nullptr;
    return *this;
}
//} /*AssignStmt*/;

//struct ReturnStmt : public Stmt {
//    ExprPtr expr;
ReturnStmt::ReturnStmt() : expr(nullptr) { }
ReturnStmt::ReturnStmt (ExprPtr e) : expr(e) { }
//    ReturnStmt (ReturnStmt &x) = delete;
ReturnStmt::ReturnStmt (ReturnStmt &&x) : expr(x.expr) {
    where (x.where()); x.expr = nullptr;
}
//    ReturnStmt & operator= (const ReturnStmt &x) = delete;
ReturnStmt & ReturnStmt::operator= (ReturnStmt &&x) {
    expr = x.expr; where (x.where());
    x.expr = nullptr;
    return *this;
}
ReturnStmt::~ReturnStmt() {
    if (expr) { delete expr; }
}
Stmt::Type ReturnStmt::type() const { return Stmt::return_stmt; }
//} /*ReturnStmt*/;

//struct EscapeStmt : public Stmt {
EscapeStmt::EscapeStmt() { }
//    EscapeStmt (EscapeStmt &x) = delete;
EscapeStmt::EscapeStmt (EscapeStmt &&x) { where (x.where()); }
EscapeStmt & EscapeStmt::operator= (EscapeStmt &&x) {
    where (x.where()); return *this;
}
EscapeStmt::~EscapeStmt() { }
Stmt::Type EscapeStmt::type() const { return Stmt::escape_stmt; }
//} /*EscapeStmt*/;

static void slist_clear (Fifo<StmtPtr> &sl)
{
    while (! sl.empty()) {
	StmtPtr x = sl.shift();
	delete x;
    }
}

//struct LoopStmt : public Stmt {
//    ExprPtr from, to;
//    ExprPtr wcond, ucond;
//    Fifo<StmtPtr> stmtlist;
//    Ident id;

LoopStmt::LoopStmt()
    : from(nullptr), to(nullptr), wcond(nullptr), ucond(nullptr), stmtlist()
{ }
LoopStmt::LoopStmt (LoopRange * &lrng, ExprPtr wcond,
	  Fifo<StmtPtr> * &stmts, ExprPtr ucond)
    : from(lrng->from), to(lrng->to), wcond(wcond), ucond(ucond),
      stmtlist(move(*stmts)), id(lrng->id)
{
    if (lrng) { delete lrng; lrng = nullptr; }
    if (stmts) { delete stmts; stmts = nullptr; }
}
//    LoopStmt (LoopStmt &x) = delete;
LoopStmt::LoopStmt (LoopStmt &&x)
    : from(x.from), to(x.to), wcond(x.wcond), ucond(x.ucond),
      stmtlist(x.stmtlist)
{
    where (x.where());
    x.from = nullptr; x.to = nullptr; x.wcond = nullptr; x.ucond = nullptr;
}
//    LoopStmt & operator= (const LoopStmt &x) = delete;
LoopStmt & LoopStmt::operator= (LoopStmt &&x) {
    from = x.from; to = x.to; wcond = x.wcond; ucond = x.ucond;
    stmtlist = x.stmtlist; where (x.where());
    x.from = nullptr; x.to = nullptr; x.wcond = nullptr; x.ucond = nullptr;
    return *this;
}
LoopStmt::~LoopStmt() {
    if (from) { delete from; }
    if (to) { delete to; }
    if (wcond) { delete wcond; }
    if (ucond) { delete ucond; }
    slist_clear (stmtlist);
}
Stmt::Type LoopStmt::type() const { return Stmt::loop_stmt; }
//} /*LoopStmt*/;

//struct IGrdStmts {
//    ExprPtr cond;
//    Fifo<StmtPtr> stmtlist;
IGrdStmts::IGrdStmts() : stmtlist() { cond = nullptr; }
IGrdStmts::IGrdStmts (ExprPtr cc, Fifo<StmtPtr> &stmts)
    : cond(cc), stmtlist(stmts)
{ }

IGrdStmts::IGrdStmts (ExprPtr cc, Fifo<StmtPtr> &&stmts)
    : cond(cc), stmtlist(move(stmts))
{ }

IGrdStmts::IGrdStmts (IGrdStmts &x)
    : cond(x.cond->Clone()), stmtlist(x.stmtlist)
{ }

IGrdStmts::IGrdStmts (IGrdStmts &&x)
    : cond(x.cond), stmtlist(move(x.stmtlist))
{
    x.cond = nullptr;
}
//IGrdStmts & IGrdStmts::operator= (const IGrdStmts &x) {
//    cond = x.cond; stmtlist = x.stmtlist;
//    return *this;
//}
IGrdStmts & IGrdStmts::operator= (IGrdStmts &&x) {
    cond = x.cond; stmtlist = x.stmtlist;
    x.cond = nullptr; x.stmtlist.clear();
    return *this;
}
IGrdStmts::~IGrdStmts() {
    delete cond; slist_clear (stmtlist);
}
//} /*IGrdStmts*/;

//struct IfStmt : public Stmt {
//    Fifo<IGrdStmts> ifelifpart;
//    Fifo<StmtPtr> elsepart;
//    IfStmt() = delete;
IfStmt::IfStmt (ExprPtr cond, Fifo<StmtPtr> * &stmts)
    : ifelifpart(Fifo<IGrdStmts>(IGrdStmts(cond, move(*stmts)))),
      elsepart()
{
    // Because of the 'move(*stmts)' above, '*stmts' should be empty,
    // so it can be deleted without the danger of unwantedly destroying
    // something ...
    delete stmts; stmts = nullptr;
}

//    IfStmt (IfStmt &x) = delete;
IfStmt::IfStmt (IfStmt &&x)
    : ifelifpart(move(x.ifelifpart)), elsepart(move(x.elsepart))
{
    where (x.where());
}

IfStmt::~IfStmt() {
    ifelifpart.clear();	    // Automatically invokes 'IGrdStmts::~IGrdStmts()'
    slist_clear (elsepart);
}

Stmt::Type IfStmt::type() const { return Stmt::if_stmt; }
//    IfStmt & operator= (const IfStmt &x) = delete;
IfStmt & IfStmt::operator= (IfStmt &&x) {
    ifelifpart = x.ifelifpart; elsepart = x.elsepart; where (x.where());
    x.ifelifpart.clear(); x.elsepart.clear();
    return *this;
}
void IfStmt::addElif (ExprPtr cond, Fifo<StmtPtr> * &stmts) {
    //IGrdStmts x(cond, stmts);
    ifelifpart.push (IGrdStmts (cond, move(*stmts)));
    delete stmts; stmts = nullptr;
}
void IfStmt::addElse (Fifo<StmtPtr> * &stmts) {
    if (stmts) {
	elsepart = move(*stmts); delete stmts; stmts = nullptr;
    }
}
//} /*IfStmt*/;

//struct CGrdStmts {
//    Fifo<ExprPtr> caselabels;
//    Fifo<StmtPtr> stmtlist;

CGrdStmts::CGrdStmts() : caselabels(), stmt(nullptr) { }

CGrdStmts::CGrdStmts (Fifo<ExprPtr> &cll, StmtPtr stmt)
    : caselabels (cll), stmt(stmt)
{ }

CGrdStmts::CGrdStmts (Fifo<ExprPtr> * &cll, StmtPtr stmt)
    : caselabels (move(*cll)), stmt(stmt)
{
    delete cll; cll = nullptr;
}

CGrdStmts::CGrdStmts (const CGrdStmts &x)
    : caselabels(x.caselabels), stmt(x.stmt)
{ }

CGrdStmts::CGrdStmts (CGrdStmts &&x)
    : caselabels(move(x.caselabels)), stmt(x.stmt)
{
    x.stmt = nullptr;
}

CGrdStmts & CGrdStmts::operator= (const CGrdStmts &x) {
    caselabels = x.caselabels;
    stmt = x.stmt;
    return *this;
}

CGrdStmts & CGrdStmts::operator= (CGrdStmts &&x) {
    caselabels = move(x.caselabels);
    stmt = x.stmt; x.stmt = nullptr;
    return *this;
}

CGrdStmts::~CGrdStmts() {
    while (! caselabels.empty()) {
	ExprPtr x = caselabels.shift();
	delete x;
    }
    delete stmt;
}
//} /*CGrdStmts*/;

//struct CaseStmt : public Stmt {
//    ExprPtr test;
//    Fifo<CGrdStmts> caseparts;
//    StmtPtr otherwise;

CaseStmt::CaseStmt() : test(nullptr), caseparts(), otherwise() { }

CaseStmt::CaseStmt (ExprPtr t, Fifo<CGrdStmts> * &cases, StmtPtr others)
    : test(t), caseparts(move(*cases)), otherwise(others)
{
    delete cases; cases = nullptr;
}

CaseStmt::CaseStmt (ExprPtr t, Fifo<CGrdStmts> * &cases)
    : test(t), caseparts(move(*cases)), otherwise(nullptr)
{
    delete cases; cases = nullptr;
}

//    CaseStmt (CaseStmt &x) = delete;
CaseStmt::CaseStmt (CaseStmt &&x)
    : test(x.test), caseparts(move(x.caseparts)), otherwise(x.otherwise)
{
    where (x.where());
    x.test = nullptr; x.otherwise = nullptr;
}
//    CaseStmt & operator= (const CaseStmt &x) = delete;
CaseStmt & CaseStmt::operator= (CaseStmt &&x) {
    test = x.test; caseparts = move(x.caseparts); otherwise = x.otherwise;
    where (x.where());
    x.test = nullptr; x.otherwise = nullptr;
    return *this;
}

CaseStmt::~CaseStmt() {
    delete test;
    caseparts.clear();	    // Automatically invokes 'CGrdStmts::~CGrdStmts()'
    delete otherwise;
}
Stmt::Type CaseStmt::type() const { return Stmt::case_stmt; }
//} /*CaseStmt*/;

//struct CompoundStmt : public Stmt {
//    Fifo<StmtPtr> stmtlist;
CompoundStmt::CompoundStmt() : stmtlist() { }
CompoundStmt::CompoundStmt (Fifo<StmtPtr> * &s)
    : stmtlist(move(*s))
{
    delete s; s = nullptr;
}
//    CompoundStmt (CompoundStmt &x) = delete;
CompoundStmt::CompoundStmt (CompoundStmt &&x)
    : stmtlist(move(x.stmtlist))
{
    where (x.where());
}
//    CompoundStmt & operator= (const CompoundStmt &x) = delete;
CompoundStmt & CompoundStmt::operator= (CompoundStmt &&x) {
    stmtlist = move(x.stmtlist); where (x.where());
    return *this;
}
CompoundStmt::~CompoundStmt() {
    while (! stmtlist.empty()) {
	StmtPtr x = stmtlist.shift();
	delete x;
    }
}
Stmt::Type CompoundStmt::type() const { return Stmt::compound_stmt; }
CompoundStmt & CompoundStmt::addStmt (StmtPtr stmt) {
    stmtlist.push (stmt);
    return *this;
}
//} /*CompoundStmt*/;

//struct SkipStmt : public Stmt {
    SkipStmt::SkipStmt() { }
    SkipStmt::~SkipStmt() { }
    Stmt::Type SkipStmt::type() const { return Stmt::skip_stmt; }
//} /*SkipStmt*/;
