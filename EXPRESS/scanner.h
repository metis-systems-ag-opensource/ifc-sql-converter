/*! \file scanner.h
**  The Interface of the (*flex*-based) Scanner as a C++ class.
**
**  A scanner is (formally) a part of the syntactical analysis which converts
**  a stream of characters into a stream of tokens, which is then used as input
**  for the syntactical analysis. Simple errors (like invalid characters or
**  character character sequences) can already be detected during this
**  conversion, which is the reason why this part is also called the lexical
**  analysis (part).
*/
/*
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
** Scanner-definition adapted from something i found in the internet (public
** domain). This, together with the code generated from the scanner definition
** ('express.l') form the (completely C++-compliant) scanner ...
**
*/
#ifndef SCANNER_H
#define SCANNER_H

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "express.tab.h"
#include "location.hh"

namespace EXPRESS {

class Scanner: public yyFlexLexer {
public:
    /*! A scanner is (must be) always initialised with the input stream of
    **  characters it analyses/converts.
    */
    Scanner (std::istream *in) : yyFlexLexer (in)
    {
	loc = new EXPRESS::Parser::location_type();
    };

    using FlexLexer::yylex;

    /*! This is the function which returns the next valid (meaningful) token
    **  from the input stream.
    **
    **  @param lval - A pointer to a location holding the semantic value of
    **                a token. Most lexical symbols are solely defined by their
    **                token alone, but some tokens represent symbol classes
    **                (like e.g. identifier/string/integer/...) In this case,
    **                the representative of the current symbol must be stored
    **                somewhere. In a reentrant `flex`-scanner like this one,
    **                this location is supplied as an argument – `lval` in this
    **                case.
    **
    **  @param location - The location where the symbol occurred in the input
    **                    stream is returned through this parameter.
    **
    **  @returns the token of the symbol just extracted from the input stream.
    */
    virtual int yylex (EXPRESS::Parser::semantic_type *const lval,
		       EXPRESS::Parser::location_type *location);
    // YY_DECL defined in step.l
    // Method body created by 'flex' in Scanner.yy.cc

private:
    /* yylval ptr */
    EXPRESS::Parser::semantic_type *yylval = nullptr;
    /* location ptr */
    EXPRESS::Parser::location_type *loc = nullptr;
} /*Scanner*/;

} /*namespace EXPRESS*/

#endif /*SCANNER_H*/
