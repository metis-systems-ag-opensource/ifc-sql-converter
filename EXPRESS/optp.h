/*! \file optp.h
**  A command line option parser, based on the C library function `getopt()`.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'optp.cc'
** (Option parser for the command line arguments, based on 'getopt()')
**
*/
#ifndef OPTP_H
#define OPTP_H

#include <string>
#include <map>

/*! The result type of the option parser. */
using OptMap = std::map<char, std::string>;

/*! The option parser.
**
**  @param argc - the argument counter from the `main()` function
**
**  @param argv - the argument vector from the `main()` function
**
**  @param opts - the list of valid (single character) options in the
**                format used by the `getopt()` C library function.
**
**  @param optx - the index (in `argv[]`) of the first non-option argument.
**
**  @returns a list of option/value pairs found in the command line. Options
**           without a value simply have an empty string as value part.
**
**  This function uses `getopt()` in non-POSIX mode, meaning: options and
**  non-option arguments may occur in any order, but a `--` ends the option
**  parsing. Any invalid option, option without argument where required, or
**  option specified twice (or more) leads to a `std::runtime_error()`.
*/
OptMap getopts (int argc, char *argv[], const char *opts, int &optx);

#endif /*OPTP_H*/
