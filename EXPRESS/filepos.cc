/* filepos.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Implementation part of 'filepos.h'
** (My own definition of the "file position" of a parsed object. I'm using this
**  definition, because it makes 'types.h', 'stmt.h' and 'expr.h' independent
**  from the generated 'position.hh' ...)
**
*/
#include "filepos.h"

#include <cstdio>

using std::ostream;
using std::string;

string to_string (const FilePos &pos) {
    char res[(pos.filename ? pos.filename->size() + 2 : 0) +
	     3*4*sizeof(unsigned int) +
	     8];
    if (pos.filename) {
	snprintf (res, sizeof(res), "%s<(%d,%d)-(%d,%d)>",
		  pos.filename->c_str(), pos.line1, pos.column1, pos.line2,
		  pos.column2);
    } else {
	snprintf (res, sizeof(res), "(%d,%d)-(%d,%d)",
		  pos.line1, pos.column1, pos.line2, pos.column2);
    }
    return res;
}

ostream & operator<< (ostream &out, const FilePos &pos) {
    return out << to_string (pos);
}
