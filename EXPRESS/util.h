/*! \file util.h
**  Helper functions which insert a declaration context (local definitions of
**  a PROCEDURE/FUNCTION/RULE, the attributes of an ENTITY, the selection
**  variable of a `QUERY` statement, or the index variable of a `REPEAT`
**  statement) into the symbol table, or remove the top level declaration
**  context from it.
**
**  \remark
**  This module is currently unused.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'util.cc'
** (Utility functions 'enter()' (creating a new declaration context in the
**  symbol table and putting all local definitions of a 'TsAlgorithm *' into
**  this context) and 'leave()' (removing the current declaration context).)
**
*/
#ifndef UTIL_H
#define UTIL_H

#include "symtab.h"
#include "types.h"

/*! Open a new declaration context, and insert all symbol definitions of this
**  context into the symbol table.
*/
void enter (Symtab &st, Ident id, TsAlgorithm *ts);

/*! Remove all symbol definitions of the current (top level) declaration
**  context from the symbol table, and then close this context.
*/
void leave (Symtab &st);

#endif /*UTIL_H*/
