/*! \file depgraph.[ch]
**
**  \brief An *obsolate* (or currently unused) module for generating a
**         dependency graph of symbol table entries.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface part of the 'depgraph' module.
**
*/
#ifndef DEPTREE_H
#define DEPTREE_H

#include <set>
#include <map>

#include "symtab.h"

struct DepGraphEl {
    std::set<Ident> deps;
    std::set<Ident> rev_deps;
    DepGraphEl();
    DepGraphEl (const DepGraphEl &) = default;
    DepGraphEl (DepGraphEl &&);
    bool depends (Ident) const;
} /*DepGraphEl*/;

struct DepGraph {
    DepGraph() = default;
    DepGraph (const DepGraph &);
    DepGraph (DepGraph &&);
    DepGraph &operator= (const DepGraph &);
    DepGraph &operator= (DepGraph &&);
    DepGraphEl &operator[] (Ident);
    const DepGraphEl &operator[] (Ident) const;
    DepGraphEl &at (Ident);
    const DepGraphEl &at (Ident) const;

    bool has (Ident) const;
    bool depends (Ident id, Ident from) const;

    bool insert (Ident);
    bool remove (Ident);
    bool addDependency (Ident id, Ident from);

    const std::set<Ident> &dependencies (Ident id) const;

    size_t size() const;

    using DgIterator = std::map<Ident, DepGraphEl>::iterator;
    using DgConstIterator = std::map<Ident, DepGraphEl>::const_iterator;

    DgIterator begin();
    DgIterator end();

    DgConstIterator begin() const;
    DgConstIterator end() const;

private:
    std::map<Ident, DepGraphEl> dg;
} /*DepGraph*/;

DepGraph gen_depgraph (Symtab &st);

IdentList resolve_deps (const DepGraph &depgraph);

#endif /*DEPTREE_H*/
