/* readconf.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Read and check the configuration
**
*/

#include <set>
#include <exception>
#include <stdexcept>
#include <filesystem>

#include "Common/confsp.h"
#include "Common/fnutil.h"

#include "readconf.h"

using std::string, std::runtime_error, std::set, std::exception;

namespace fs = std::filesystem;

static string to_string (const set<string> &xs, string delim = ", ")
{
    bool first = true;
    string res;
    for (auto &x: xs) {
	if (first) { first = false; } else { res += delim; }
	res += x;
    }
    return res;
}

Config readconf (const std::string &cfgfile)
{
    Config cfgdata;
    set<string> required { "CGENDIR" };
    set<string> missing;
    auto [path, found] = findFile (cfgfile);
    if (! found) {
	throw runtime_error
	    ("No configuration file \"" + cfgfile + "\" found.");
    }
    cfgdata.load (path);
    if (auto [mf, missing] = cfgdata.check_missing (required); mf) {
	throw runtime_error
	    ("Missing configuration items: " + to_string (missing));
    }
    string cgendir = cfgdata["CGENDIR"];
#ifdef FORCE_ABSPATH
    if (! is_abspath (cgendir)) {
	throw runtime_error
	    ("Configuration value of CGENDIR is no absolute pathname");
    }
#endif
    if (! fs::exists (cgendir)) {
	throw runtime_error
	    ("Configuration value of CGENDIR points to no existing"
	     " filesystem entry");
    }
    if (! fs::is_directory (cgendir)) {
	throw runtime_error
	    ("Configuration value of CGENDIR doesn't point to a directory");
    }
    return cfgdata;
}

#ifdef TESTING

#include <iostream>
#include <string>

using std::cerr, std::cout, std::endl, std::string;

int main (int argc, char *argv[])
{
    if (argc < 2) {
	cout << "Usage: " << *argv << " filename" << endl;
	return 0;
    }
    string cfgfile (argv[1]);

    try {
	Config cfg = readconf (cfgfile);

	cout << "The configuration contains the following items:" << endl;
	for (auto &&it: cfg.item_names()) {
	    cout << "  " << it << ": " << cfg[it] << endl;
	}
    } catch (exception &exc) {
	cerr << *argv << ": " << exc.what() << endl;
	return 1;
    }

    return 0;
}

#endif /*TESTING*/
