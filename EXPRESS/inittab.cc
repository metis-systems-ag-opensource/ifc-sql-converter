/* inittab.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Initialize the symbol table with some standard values ...
**
*/

#include <utility>
#include <stdexcept>
#include <string>

#include "types.h"

#include "inittab.h"

namespace EXPRESS {

using std::move;
using std::runtime_error;
using std::string;

using TypeTag = TypeStruct::Type;

const string mt = "";

static
TypeStruct *gensubtype (TypeStruct::Type typetag, const string &typelabel = mt)
{
    TypeStruct *res = nullptr;
    switch (typetag) {
	case TypeStruct::boolean: {
	    res = new TsBoolean (); break;
	}
	case TypeStruct::logical: {
	    res = new TsLogical (); break;
	}
	case TypeStruct::integer: {
	    res = new TsInteger (); break;
	}
	case TypeStruct::real: {
	    res = new TsReal (); break;
	}
	case TypeStruct::number: {
	    res = new TsReal (); break;
	}
	case TypeStruct::binary: {
	    res = new TsBinary (); break;
	}
	case TypeStruct::string: {
	    res = new TsString (); break;
	}
	case TypeStruct::generic: {
	    res = new TsGeneric (typelabel);
	    break;
	}
	case TypeStruct::generic_entity: {
	    res = new TsGeneric (typelabel, true);
	    break;
	}
	default: {
	    // This point should never be reached!
	    throw runtime_error ("INTERNAL ERROR: Invalid subtype (" +
				 to_string (typetag) + ")");
	}
    }
    return res;
}

static
TypeStruct *gentype (TypeTag ttag, TypeTag subttag = TypeStruct::unset,
		     const string &typelabel = mt)
{
    TypeStruct *res = nullptr;
    switch (ttag) {
	case TypeStruct::unset: {
	    res = nullptr; break;
	}
	case TypeStruct::boolean: {
	    res = new TsBoolean (); break;
	}
	case TypeStruct::logical: {
	    res = new TsLogical (); break;
	}
	case TypeStruct::integer: {
	    res = new TsInteger (); break;
	}
	case TypeStruct::real: {
	    res = new TsReal (); break;
	}
	case TypeStruct::number: {
	    res = new TsReal (); break;
	}
	case TypeStruct::binary: {
	    res = new TsBinary (); break;
	}
	case TypeStruct::string: {
	    res = new TsString (); break;
	}
	case TypeStruct::generic: {
	    res = new TsGeneric (typelabel);
	    break;
	}
	case TypeStruct::generic_entity: {
	    res = new TsGeneric (typelabel, true);
	    break;
	}
	case TypeStruct::aggregate: {
	    TypeStruct *subtype = gensubtype (subttag, typelabel);
	    res = new TsAggregate (mt, move(subtype));
	    break;
	}
	case TypeStruct::set: {
	    TypeStruct *subtype = gensubtype (subttag, typelabel);
	    res = new TsSet (subtype);
	    break;
	}
	case TypeStruct::bag: {
	    TypeStruct *subtype = gensubtype (subttag, typelabel);
	    res = new TsBag (subtype);
	    break;
	}
	case TypeStruct::list: {
	    TypeStruct *subtype = gensubtype (subttag, typelabel);
	    res = new TsList (subtype);
	    break;
	}
	default: {
	    // This point should never be reached!
	    throw runtime_error ("INTERNAL ERROR: Invalid argument type (" +
				 to_string (ttag) + ")");
	}
    }
    return res;
}

struct TDesc {
    TypeTag ttag, subttag;
    const std::string *lblp;
    bool isvar;
    TDesc ()
	: ttag(TypeStruct::unset), subttag(TypeStruct::unset), lblp(&mt),
	  isvar(false)
    { }
    TDesc (TypeTag ttag, TypeTag subttag, const std::string &label = mt,
	   bool isvar = false)
	: ttag(ttag), subttag(subttag), lblp(&label), isvar(isvar)
    { }
    TDesc (TypeTag ttag, TypeTag subttag, bool isvar)
	: ttag(ttag), subttag(subttag), lblp(&mt), isvar(isvar)
    { }
    TDesc (TypeTag ttag, const std::string &label = mt, bool isvar = false)
	: ttag(ttag), subttag(TypeStruct::unset), lblp(&label), isvar(isvar)
    { }
    TDesc (TypeTag ttag, bool isvar)
	: ttag(ttag), subttag(TypeStruct::unset), lblp(&mt), isvar(isvar)
    { }
    TDesc (const TDesc &x)
	: ttag(x.ttag), subttag(x.subttag), lblp(x.lblp), isvar(x.isvar)
    { }
} /*TDesc*/;
    

static
TypeStruct *onearg_func (TDesc ttpar, TDesc ttret)
{
    TypeStruct *partype = gentype (ttpar.ttag, ttpar.subttag, *ttpar.lblp);
    ProcArg arg (partype, ttpar.isvar);
    TypeStruct *rettype = gentype (ttret.ttag, ttret.subttag, *ttret.lblp);
    return new TsStdFunc (rettype, Fifo<ProcArg>(arg));
}

static
TypeStruct *twoarg_func (TDesc ttpar1, TDesc ttpar2, TDesc ttret)
{
    TypeStruct *par1type = gentype (ttpar1.ttag, ttpar1.subttag, *ttpar1.lblp);
    ProcArg arg1 (par1type, ttpar1.isvar);
    TypeStruct *par2type = gentype (ttpar2.ttag, ttpar2.subttag, *ttpar2.lblp);
    ProcArg arg2 (par2type, ttpar2.isvar);
    TypeStruct *rettype = gentype (ttret.ttag, ttret.subttag, *ttret.lblp);
    return new TsStdFunc (rettype, Fifo<ProcArg> { arg1, arg2 });
}

static
TypeStruct *threearg_func (TDesc ttpar1, TDesc ttpar2, TDesc ttpar3,
			   TDesc ttret)
{
    TypeStruct *par1type = gentype (ttpar1.ttag, ttpar1.subttag, *ttpar1.lblp);
    ProcArg arg1 (par1type, ttpar1.isvar);
    TypeStruct *par2type = gentype (ttpar2.ttag, ttpar2.subttag, *ttpar2.lblp);
    ProcArg arg2 (par2type, ttpar2.isvar);
    TypeStruct *par3type = gentype (ttpar3.ttag, ttpar3.subttag, *ttpar3.lblp);
    ProcArg arg3 (par3type, ttpar3.isvar);
    TypeStruct *rettype = gentype (ttret.ttag, ttret.subttag, *ttret.lblp);
    return new TsStdFunc (rettype, Fifo<ProcArg> { arg1, arg2, arg3 });
}



void init_symtab (Symtab &symtab)
{
#define TS TypeStruct::
#define TSNUMBER TDesc (TS number)
#define TSREAL TDesc (TS real)
#define TSBOOLEAN TDesc (TS boolean)
#define TSLOGICAL TDesc (TS logical)
#define TSBINARY TDesc (TS binary)
#define TSSTRING TDesc (TS string)
#define TSINTEGER TDesc (TS integer)
#define TSGENERIC TDesc (TS generic)
#define TSGENERIC_ENTITY TDesc (TS generic_entity)
#define TSGENERIC_AGGREGATE TDesc (TS aggregate, TS generic)
#define TSGEN TDesc (TS generic, "gen")
#define TSGEN1 TDesc (TS generic, "gen1")
#define TSGENAGG TDesc (TS aggregate, TS generic, "gen")
#define TSSTRINGSET TDesc (TS set, TS string)
#define TSENTITYBAG TDesc (TS bag, TS generic_entity)
#define TSVGENLIST TDesc (TS list, TS generic, "gen", true)
#define TSVGENERIC_LIST TDesc (TS list, TS generic, true)
#define TSNONE TDesc (TS unset)
#define NEWSYMBOL(name, type) \
    (symtab.newsymbol ((name), id, (type), depth))

    Ident id = 0;	    // Placeholder for the newsymbol()-invocation
    uint32_t depth = 0;	    // (same as above)

    // Declare the simple types
    NEWSYMBOL ("BOOLEAN", new TsBoolean);
    NEWSYMBOL ("LOGICAL", new TsLogical);
    NEWSYMBOL ("INTEGER", new TsInteger);
    NEWSYMBOL ("REAL", new TsReal);
    NEWSYMBOL ("NUMBER", new TsNumber);
    NEWSYMBOL ("BINARY", new TsBinary);
    NEWSYMBOL ("STRING", new TsString);

    // Declare the stadard functions
    NEWSYMBOL ("ABS", onearg_func (TSNUMBER, TSNUMBER));
    NEWSYMBOL ("ACOS", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("ASIN", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("ATAN", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("BLENGTH", onearg_func (TSBINARY, TSINTEGER));
    NEWSYMBOL ("COS", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("EXISTS", onearg_func (TSGENERIC, TSBOOLEAN));
    NEWSYMBOL ("EXP", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("FORMAT", twoarg_func (TSNUMBER, TSSTRING, TSSTRING));
    NEWSYMBOL ("HIBOUND", onearg_func (TSGENERIC_AGGREGATE, TSINTEGER));
    NEWSYMBOL ("HIINDEX", onearg_func (TSGENERIC_AGGREGATE, TSINTEGER));
    NEWSYMBOL ("LENGTH", onearg_func (TSSTRING, TSINTEGER));
    NEWSYMBOL ("LOBOUND", onearg_func (TSGENERIC_AGGREGATE, TSINTEGER));
    NEWSYMBOL ("LOG", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("LOG2", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("LOG10", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("LOINDEX", onearg_func (TSGENERIC_AGGREGATE, TSINTEGER));
    NEWSYMBOL ("NVL", twoarg_func (TSGEN1, TSGEN1, TSGEN1));
    NEWSYMBOL ("ODD", onearg_func (TSINTEGER, TSLOGICAL));
    NEWSYMBOL ("ROLESOF", onearg_func (TSGENERIC_ENTITY, TSSTRINGSET));
    NEWSYMBOL ("SIN", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("SIZEOF", onearg_func (TSGENERIC_AGGREGATE, TSINTEGER));
    NEWSYMBOL ("SQRT", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("TAN", onearg_func (TSNUMBER, TSREAL));
    NEWSYMBOL ("TYPEOF", onearg_func (TSGENERIC, TSSTRINGSET));
    NEWSYMBOL ("USEDIN", twoarg_func (TSGENERIC_ENTITY, TSSTRING, TSENTITYBAG));
    NEWSYMBOL ("VALUE", onearg_func (TSSTRING, TSNUMBER));
    NEWSYMBOL ("VALUE_IN", twoarg_func (TSGENAGG, TSGEN, TSLOGICAL));

    // Declare the standard procedures
    NEWSYMBOL ("INSERT", threearg_func (TSVGENLIST, TSGEN, TSINTEGER, TSNONE));
    NEWSYMBOL ("REMOVE", twoarg_func (TSVGENERIC_LIST, TSINTEGER, TSNONE));

#undef NEWSYMBOL
#undef TSNONE
#undef TSVGENERIC_LIST
#undef TSVGENLIST
#undef TSENTITYBAG
#undef TSSTRINGSET
#undef TSGENAGG
#undef TSGEN1
#undef TSGEN
#undef TSGENERIC_AGGREGATE
#undef TSGENERIC_ENTITY
#undef TSGENERIC
#undef TSINTEGER
#undef TSSTRING
#undef TSBINARY
#undef TSLOGICAL
#undef TSBOOLEAN
#undef TSREAL
#undef TSNUMBER
#undef TS
}


} /*namespace EXPRESS*/
