/* opmode.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Convert a string ('const char *' or 'const std::string &') into an element
** of the 'OpMode' enum.
**
*/

#include <cstring>

#include "opmode.h"

using std::string;

namespace EXPRESS {

    static struct Ops { const char *name; OpMode opmode; } ops[] = {
	{ "c++", OpMode::cpp },
	{ "cpp", OpMode::cpp },
	{ "db", OpMode::db },
	{ nullptr, OpMode::none }
    };

    string to_string (OpMode op)
    {
	switch (op) {
	    case OpMode::db: return "db";
	    case OpMode::cpp: return "c++";
	    default: return "<none>";
	}
    }

    const string &valid_ops ()
    {
	static string res;
	if (res.size() == 0) {
	    int ix; Ops *pop; bool first = true;
	    for (ix = 0; (pop = &ops[ix])->name; ++ix) {
		if (first) { first = false; } else { res += ", "; }
		res += pop->name;
	    }
	}
	return res;
    }

    OpMode opmode (const char *opname)
    {
	int ix; Ops *pop;
	for (ix = 0; (pop = &ops[ix])->name; ++ix) {
	    if (strcmp (pop->name, opname) == 0) { break; }
	}
	return pop->opmode;
    }

    OpMode opmode (const string &opname)
    {
	return opmode (opname.c_str());
    }

} /*namespace EXPRESS*/
