/* pass2.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Performing the second pass of the Compiler
**
*/

#include "defcheck.h"
#include "flatten.h"

#include "pass2.h"

//using namespace std;

namespace EXPRESS {

bool do_pass2 (OpMode op, Symtab &st)
{
    // Import 'cout' and 'endl' from the namespace 'std'.
    using std::cout;
    using std::endl;

    // Status variable indicating the success/failure of each action and
    // of 'do_pass2()' as a whole.
    bool done;

    // Send a message to 'cout' that pass two started ...
    cout << "Pass two ";

    // Check if there are undefined symbols in the symbol table ...
    done = defcheck (st);

    // Complete the (simple) ssuper/subtype relationships
    if (done) { cout << "."; done = complete_supersubtypes (st); }

    // Collect all element types of SELECT types, all attributes of ENTITY
    // types internal and external (meaning: attributes of an ENTITY's
    // supertypes upto te base type(s) of the hierarchy) and store them in
    // special member variables for later use.
    if (done) { cout << "."; done = flatten (st); }

    // Later invocations should follow this scheme:
    //   if (done) { cout << "."; done = XYZ (st); }
    // Here 'XYZ' is the action to be performed (a function with a sole
    // argument of type 'Symtab &', which returns a boolean value.

    // Last progress indicating '.'
    if (done) { std::cout << "."; }

    // End the message with either ' done.' or ' failed.' (indicating the
    // success status on 'cout').
    std::cout << " " << (done ? "done" : "failed") << "." << std::endl;

    // Success status of 'do_pass2()' as a whole
    return done;
}

} /*namespace EXPRESS*/
