/*! \file pass2.h
**  Actions performed after the sole parsing of an EXPRESS (IFC-)specification
**  is complete. This currently consists of simple context checks.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'pass2.cc'
** (Performing the second pass of the Compiler)
**
*/
#ifndef PASS2_H
#define PASS2_H

#include "symtab.h"
#include "opmode.h"

namespace EXPRESS {

/*! Performs all pass 2 operations (checks which exceed the simple syntactical
**  analysis).
**
**  @param op - the operation mode (in the case that some checks depend on
**              the type of operation selected – which is currently not the
**              case).
**
**  @param st - the symbol table generated during the syntactical analysis
**              (the parsing pass, or pass 1). This symbol table contains
**              all symbols defined in pass 1.
**
**  @returns `true` if all checks done in pass 2 succeed, and `false` if
**           any of these checks fails.
*/
bool do_pass2 (OpMode op, Symtab &st);

} /*namespace EXPRESS*/

#endif /*PASS2_H*/
