/* consteval.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Evaluator for constant expressions
**
*/
#include <iostream>	// Delete when no longer used ...

#include <cmath>
#include <stdexcept>

#include "consteval.h"

using namespace std;

bool is_constant (Expr *x)
{
    switch (x->type()) {
	case Expr::logical_literal:
	case Expr::int_literal:
	case Expr::real_literal:
	case Expr::binary_literal:
	case Expr::string_literal:
	case Expr::unspec_literal:
	    return true;
	case Expr::aggregate: {
	    Aggregate *agg = dynamic_cast<Aggregate *>(x);
	    bool const_items = true;
	    for (Expr *val: agg->values) {
		if (! is_constant (val)) { const_items = false; break; }
	    }
	    return const_items;
	}
	case Expr::nxaggregate: {
	    NxAggregate *agg = dynamic_cast<NxAggregate *>(x);
	    bool const_items = true;
	    for (AggElement &rval: agg->rvalues) {
		if (! is_constant (rval.value)
		||  ! is_constant (rval.repetition)) {
		    const_items = false; break;
		}
	    }
	    return const_items;
	}
	default:
	    return false;
    }
}

static Integer numlit2int (Expr *x);

Integer int_value (Expr *x)
{
    return numlit2int (x);
}

static bool num_type (Expr::Type t);

bool numeric_literal (Expr *x)
{
    return num_type (x->type());
}

static Expr *apply (BinOp op, Expr *l, Expr *r);
static Expr *apply (MonOp op, Expr *r);
static Expr *apply (StdFunc op, Fifo<Expr *> &args);

// Evaluate an expression
// WARNING! Eval modifies the original expression, partially or completely
// replacing it with the new (constant-folded) (sub-)expressions.
void consteval (Expr * &x) {
    Expr::Type xt = x->type ();
    if (xt == Expr::binary) {
	BinaryExpr *b = dynamic_cast<BinaryExpr *>(x);
	consteval (b->left); consteval (b->right);
	if (is_constant (b->left) && is_constant (b->right)) {
	    Expr *newval = apply (b->op, b->left, b->right);
	    if (newval) { delete x; x = newval; }
	}
    } else if (xt == Expr::unary) {
	UnaryExpr *u = dynamic_cast<UnaryExpr *>(x);
	consteval (u->right);
	if (is_constant (u->right)) {
	    Expr *newval = apply (u->op, u->right);
	    if (newval) { delete x; x = newval; }
	}
    } else if (xt == Expr::std_invoc) {
	StdInvoc *si = dynamic_cast<StdInvoc *>(x);
	bool args_are_constant = true;
	for (Expr *arg: si->arguments) {
	    consteval (arg);
	    if (! is_constant (arg)) { args_are_constant = false; }
	}
	if (args_are_constant) {
	    Expr *newval = apply (si->ftag, si->arguments);
	    if (newval) { delete x; x = newval; }
	}
    } else if (xt == Expr::aggregate) {
	Aggregate *agg = dynamic_cast<Aggregate *>(x);
	for (Expr *val: agg->values) {
	    consteval (val);
	}
    } else if (xt == Expr::nxaggregate) {
	NxAggregate *agg = dynamic_cast<NxAggregate *>(x);
	for (AggElement &rval: agg->rvalues) {
	    consteval (rval.value); consteval (rval.repetition);
	}
    } else if (xt == Expr::func_invoc) {
	FuncInvoc *fi = dynamic_cast<FuncInvoc *>(x);
	for (Expr *arg: fi->arguments) {
	    consteval (arg);
	}
    }
    // Expr::interval, Expr::query Expr::lvalue and Expr::instance are assumed
    // to be non-constants and the literal-values need not be modified, as they
    // are already (elementary) constants. So, we are finished here ...
}

static bool int_type (Expr::Type t)
{
    return t == Expr::int_literal;
}

static bool real_type (Expr::Type t)
{
    return t == Expr::real_literal;
}

static bool num_type (Expr::Type t)
{
    return t == Expr::int_literal || t == Expr::real_literal;
}

static bool bin_type (Expr::Type t)
{
    return t == Expr::binary_literal;
}

static bool strg_type (Expr::Type t)
{
    return t == Expr::string_literal;
}

static bool logical_type (Expr::Type t)
{
    return t == Expr::logical_literal;
}

static bool is_unspec (Expr::Type lt, Expr::Type rt)
{
    return lt == Expr::unspec_literal || rt == Expr::unspec_literal;
}

#if 0
static bool is_unspec (Expr::Type rt)
{
    return rt == Expr::unspec_literal;
}
#endif

static Real numlit2real (Expr *x);
static Bitvec bitconc (Bitvec l, Bitvec r);
static Logical bcmp (BinOp op, Logical l, Logical r);
static Logical bcmp (BinOp op, Integer l, Integer r);
static Logical bcmp (BinOp, Real l, Real r);
static Logical bcmp (BinOp op, string l, string r);
static Logical bcmp (BinOp op, Bitvec l, Bitvec r);

static Expr *apply (MonOp op, Expr *r)
{
    Expr::Type rt = r->type ();
    switch (op) {
	case MonOp::plus: case MonOp::minus: {
	    bool neg = op == MonOp::minus;
	    if (int_type (rt)) {
		IntLiteral *v = dynamic_cast<IntLiteral *>(r);
		return new IntLiteral (neg ? -v->value : v->value);
	    }
	    if (real_type (rt)) {
		RealLiteral *v = dynamic_cast<RealLiteral *>(r);
		return new RealLiteral (neg ? -v->value : v->value);
	    }
	    break;
	}
	case MonOp::not_:
	    if (logical_type (rt)) {
		LogicalLiteral *v = dynamic_cast<LogicalLiteral *>(r);
		return new LogicalLiteral (~ v->value);
	    }
	    break;
    }
    return nullptr;
}

static Expr *apply (StdFunc op, Fifo<Expr *> &args)
{
    Expr *arg;
    Expr::Type argtype;
    switch (op) {
	case StdFunc::abs:
	    if (args.length() != 1) { break; }
	    arg = args.first ();
	    argtype = arg->type ();
	    if (int_type (argtype)) {
		IntLiteral *rv = dynamic_cast<IntLiteral *>(arg);
		Integer res = (rv->value < 0 ? -rv->value : rv->value);
		return new IntLiteral (res);
	    }
	    if (real_type (argtype)) {
		RealLiteral *rv = dynamic_cast<RealLiteral *>(arg);
		Real res = (rv->value < 0.0 ? -rv->value : rv->value);
		return new RealLiteral (res);
	    }
	    break;
	case StdFunc::odd:
	    if (args.length() != 1) { break; }
	    arg = args.first ();
	    argtype = arg->type ();
	    if (num_type (argtype)) {
		Integer res = numlit2int (arg);
		return new LogicalLiteral (((int) res & 1) != 0);
	    }
	    break;
	case StdFunc::exists:
	    if (args.length() != 1) { break; }
	    arg = args.first ();
	    argtype = arg->type ();
	    return new LogicalLiteral (argtype == Expr::unspec_literal);
	case StdFunc::blength:
	    if (args.length() != 1) { break; }
	    arg = args.first ();
	    argtype = arg->type ();
	    if (bin_type (argtype)) {
		BinaryLiteral *b = dynamic_cast<BinaryLiteral *>(arg);
		return new IntLiteral (b->value.size());
	    }
	    break;
	case StdFunc::length:
	    if (args.length() != 1) { return nullptr; }
	    arg = args.first ();
	    argtype = arg->type ();
	    if (strg_type (argtype)) {
		StringLiteral *s = dynamic_cast<StringLiteral *>(arg);
		return new IntLiteral (s->value.size());
	    }
	    break;
	default:
	    break;
    }
    return nullptr;
}

Expr *apply (BinOp op, Expr *l, Expr *r)
{
    Expr::Type lt = l->type (), rt = r->type ();
    switch (op) {
	case BinOp::lt: case BinOp::gt: case BinOp::le: case BinOp::ge:
	case BinOp::ne: case BinOp::eq: case BinOp::ine: case BinOp::ieq:
	    if (is_unspec (lt, rt)) {
		return new LogicalLiteral (Logical::unknown);
	    }
	    if (int_type (lt) && int_type (rt)) {
		Integer lv = numlit2int (l), rv = numlit2int (r);
		return new LogicalLiteral (bcmp (op, lv, rv));
	    }
	    if (num_type (lt) && num_type (rt)) {
		Real lv = numlit2real (l), rv = numlit2real (r);
		return new LogicalLiteral (bcmp (op, lv, rv));
	    }
	    if (lt == Expr::logical_literal && rt == Expr::logical_literal) {
		LogicalLiteral *lv = dynamic_cast<LogicalLiteral *>(l);
		LogicalLiteral *rv = dynamic_cast<LogicalLiteral *>(r);
		return new LogicalLiteral (bcmp (op, lv->value, rv->value));
	    }
	    if (bin_type (lt) && bin_type (rt)) {
		BinaryLiteral *lv = dynamic_cast<BinaryLiteral *>(l);
		BinaryLiteral *rv = dynamic_cast<BinaryLiteral *>(r);
		return new LogicalLiteral (bcmp (op, lv->value, rv->value));
	    }
	    if (strg_type (lt) && strg_type (rt)) {
		StringLiteral *lv = dynamic_cast<StringLiteral *>(l);
		StringLiteral *rv = dynamic_cast<StringLiteral *>(r);
		return new LogicalLiteral (bcmp (op, lv->value, rv->value));
	    }
	    break;
	case BinOp::pow:
	    if (is_unspec (lt, rt)) {
		return new UnspecLiteral;
	    }
	    if (num_type (lt) && num_type (rt)) {
		Real lv = numlit2real (l), rv = numlit2real (r);
		return new RealLiteral (powl (lv, rv));
	    }
	    break;
	case BinOp::add:
	    if (is_unspec (lt, rt)) {
		return new UnspecLiteral;
	    }
	    if (int_type (lt) && int_type (rt)) {
		Integer lv = numlit2int (l), rv = numlit2int (r);
		return new IntLiteral (lv + rv);
	    }
	    if (num_type (lt) && num_type (rt)) {
		Real lv = numlit2real (l), rv = numlit2real (r);
		return new RealLiteral (lv + rv);
	    }
	    if (bin_type (lt) && bin_type (rt)) {
		BinaryLiteral *lv = dynamic_cast<BinaryLiteral *>(l);
		BinaryLiteral *rv = dynamic_cast<BinaryLiteral *>(r);
		return new BinaryLiteral (bitconc (lv->value, rv->value));
	    }
	    if (strg_type (lt) && strg_type (rt)) {
		StringLiteral *lv = dynamic_cast<StringLiteral *>(l);
		StringLiteral *rv = dynamic_cast<StringLiteral *>(r);
		return new StringLiteral (lv->value + rv->value);
	    }
	    break;
	case BinOp::sub: case BinOp::mul:
	    if (is_unspec (lt, rt)) {
		return new UnspecLiteral;
	    }
	    if (int_type (lt) && int_type (rt)) {
		Integer lv = numlit2int (l), rv = numlit2int (r);
		Integer res = (op == BinOp::sub ? lv - rv : lv * rv);
		return new IntLiteral (res);
	    }
	    if (num_type (lt) && num_type (rt)) {
		Real lv = numlit2real (l), rv = numlit2real (r);
		Real res = (op == BinOp::sub ? lv - rv : lv * rv);
		return new RealLiteral (res);
	    }
	    break;
	case BinOp::rdiv:
	    if (is_unspec (lt, rt)) {
		return new UnspecLiteral;
	    }
	    if (num_type (lt) && num_type (rt)) {
		Real lv = numlit2real (l), rv = numlit2real (r);
		return new RealLiteral (lv / rv);
	    }
	    break;
	case BinOp::idiv: case BinOp::mod:
	    if (is_unspec (lt, rt)) {
		return new UnspecLiteral;
	    }
	    if (int_type (lt) && int_type (rt)) {
		Integer lv = numlit2int (l), rv = numlit2int (r);
		Integer res = (op == BinOp::idiv ? lv / rv : lv % rv);
		return new IntLiteral (res);
	    }
	    break;
	case BinOp::and_: case BinOp::or_: case BinOp::xor_:
	    if (is_unspec (lt, rt)) {
		return new UnspecLiteral;
	    }
	    if (lt == Expr::logical_literal && rt == Expr::logical_literal) {
		Logical lv = (dynamic_cast<LogicalLiteral *>(l))->value;
		Logical rv = (dynamic_cast<LogicalLiteral *>(r))->value;
		Logical res (Logical::unknown);
		switch (op) {
		    case BinOp::and_: res = lv & rv; break;
		    case BinOp::or_: res = lv | rv; break;
		    case BinOp::xor_: res = lv ^ rv; break;
		    default: break;
		}
		return new LogicalLiteral (res);
	    }
	    break;
	default:
	    break;
    }
    return nullptr;
}

static Real numlit2real (Expr *x) {
    if (x->type() == Expr::real_literal) {
	return (dynamic_cast<RealLiteral *>(x))->value;
    }
    if (x->type() == Expr::int_literal) {
	return (Real) (dynamic_cast<IntLiteral *>(x))->value;
    }
    throw runtime_error ("Numerical value expected!");
}

static Integer numlit2int (Expr *x) {
    if (x->type() == Expr::int_literal) {
	return (dynamic_cast<IntLiteral *>(x))->value;
    }
    if (x->type() == Expr::real_literal) {
	Natural max = -1ll;
	Real z = (dynamic_cast<RealLiteral *>(x))->value;
	max >>= 1;
	if (abs (z) > (Real) max) {
	    throw runtime_error ("Numerical value out if range");
	}
	return (Integer) z;
    }
    throw runtime_error ("Numerical value expected");
}

static Bitvec bitconc (Bitvec l, Bitvec r)
{
    Bitvec res;
    int tx = 0;
    res.resize (l.size () + r.size ());
    for (size_t ix = 0; ix < l.size (); ++ix) { res[tx++] = l[ix]; }
    for (size_t ix = 0; ix < r.size (); ++ix) { res[tx++] = r[ix]; }
    return res;
}

static Logical bcmp (BinOp op, Logical l, Logical r)
{
    switch (op) {
	case BinOp::lt: return Logical (l < r);
	case BinOp::gt: return Logical (r < l);
	case BinOp::le: return Logical (! (r < l));
	case BinOp::ge: return Logical (! (l < r));
	case BinOp::eq: case BinOp::ieq: return Logical (l == r);
	case BinOp::ne: case BinOp::ine: return Logical (! (l == r));
	default: // Should NEVER arise
	    return Logical (Logical::unknown);
    }
}

static Logical bcmp (BinOp op, Integer l, Integer r)
{
    switch (op) {
	case BinOp::lt: return Logical (l < r);
	case BinOp::gt: return Logical (l > r);
	case BinOp::le: return Logical (l <= r);
	case BinOp::ge: return Logical (l >= r);
	case BinOp::eq: case BinOp::ieq: return Logical (l == r);
	case BinOp::ne: case BinOp::ine: return Logical (l != r);
	default: // Should NEVER arise
	    return Logical (Logical::unknown);
    }
}

static Logical bcmp (BinOp op, Real l, Real r)
{
    switch (op) {
	case BinOp::lt: return Logical (l < r);
	case BinOp::gt: return Logical (l > r);
	case BinOp::le: return Logical (l <= r);
	case BinOp::ge: return Logical (l >= r);
	case BinOp::eq: case BinOp::ieq: return Logical (l == r);
	case BinOp::ne: case BinOp::ine: return Logical (l != r);
	default: // Should NEVER arise
	    return Logical (Logical::unknown);
    }
}

static Logical bcmp (BinOp op, string l, string r)
{
    switch (op) {
	case BinOp::lt: return Logical (l < r);
	case BinOp::gt: return Logical (l > r);
	case BinOp::le: return Logical (l <= r);
	case BinOp::ge: return Logical (l >= r);
	case BinOp::eq: case BinOp::ieq: return Logical (l == r);
	case BinOp::ne: case BinOp::ine: return Logical (l != r);
	default: // Should NEVER arise
	    return Logical (Logical::unknown);
    }
}

static Logical bcmp (BinOp op, Bitvec l, Bitvec r)
{
    switch (op) {
	case BinOp::lt: return Logical (l < r);
	case BinOp::gt: return Logical (l > r);
	case BinOp::le: return Logical (l <= r);
	case BinOp::ge: return Logical (l >= r);
	case BinOp::eq: case BinOp::ieq: return Logical (l == r);
	case BinOp::ne: case BinOp::ine: return Logical (l != r);
	default: // Should NEVER arise
	    return Logical (Logical::unknown);
    }
}
