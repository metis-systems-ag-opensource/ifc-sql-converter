/*! \file main.cc
**  Main program of the EXPRESS "compiler" `xpp`.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Main program of the 'EXPRESS'-parser.
**
*/
#include <filesystem>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <stdexcept>

#include <cstdlib>

#include "Common/sutil.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "readconf.h"
#include "driver.h"
#include "optp.h"
#include "opmode.h"
//#include "depgraph.h"

namespace fs = std::filesystem;
using namespace std;

static bool has (OptMap m, char opt)
{
    return m.count (opt) > 0;
}

using EXPRESS::OpMode;

static int check_arg (const string &prog,
		      string &arg, const char *itname, const char *itdsc,
		      OptMap &opts, char opt, Config &cfg, const string &ci);

static int process_spec (const string &prog, Config &config, const char *spec,
			 OpMode op, const string &tmpldir, const string &outdf);

int main (const int argc, char **argv)
{
    Config config;
    using namespace EXPRESS;
    const char *prog = strrchr (*argv, '/');
    if (prog) { ++prog; } else { prog = *argv; }

    // Load a configuration file
    try {
	config = readconf ("express");
    } catch (runtime_error &e) {
	string what = e.what();
	if (is_prefix (what, "No configuration file")) {
	    config = readconf (prog);
	} else {
	    throw;
	}
    }
    if (argc < 2 || string (argv[1]) == "-h") {
	cout << "Usage: " << prog << " gen-mode [options] express-spec" EOL
		"       " << prog << " -h" EOL
		EOL "where:" <<
		EOL "  -h writes this usage message." <<
		EOL "  gen-mode is one of (" + valid_ops() + ")." <<
		EOL "  " << prog << " gen-mode -h" <<
		EOL "    writes a usage message for the generator 'gen-mode'."
		EOL;
	return 0;
    }
    //string mode = argv[1];
    const char *gmode = argv[1];
    OpMode op = opmode (gmode);
    if (op == OpMode::none) {
	cerr << prog << ": Invalid gen-mode '" << gmode << "'." EOL;
	return 64;
    }

    if (op == OpMode::db) {
	OptMap opts; int optx = 2;
	try {
	    opts = getopts (argc, argv, "ho:t:", optx);
	} catch (runtime_error &e) {
	    cerr << prog << ": " << e.what() << EOL; return 64;
	}
	if (has (opts, 'h')) {
	    cout << "Usage: " << prog <<
		    " db [-o outfile] [-t template_dir] express-spec" EOL;
	    return 0;
	}

	// We have the following things:
	//   a) A configuration option 'dbschema' (or option '-o') which is
	//      either a pathname or a filename without path or 'ifc.sql'.
	//   b) A pathname pointing to a directory which is used if a) is not
	//      a pathname. This pathname.
	// 1st choice: option, 2nd choice: configured filename,
	// 3rd option: "ifc.sql"
	// (In the expression, the 2nd and 3rd choices are reveresed due to the
	//  nature of 'string::empty()'.)
	string outfile = (has (opts, 'o') ?  opts['o'] :
			  config.has ("DBSCHEMA") ? config["DBSCHEMA"] :
			  "ifc.sql");
	// If 'outfile' is already a pathname, do nothing here; otherwise,
	// (path-)prepend either a configured directory or the current working
	// directory ('.') here.
	if (! is_pathname (outfile)) {
	    string outdir = config.has ("DBGENDIR") ? config["DBGENDIR"] :
			    config.has ("CGENDIR") ? config["CGENDIR"] :
			    ".";
	    outfile = outdir / outfile;
	}
	string tplname = config["TEMPLATEDIR"];
	string tmpldir = (has (opts, 't') ? opts['t'] :
			  config.has ("TEMPLATEDIR") ? config["TEMPLATEDIR"] :
			  "./Templates");
	if (optx >= argc) {
	    cerr << prog << ": Missing an argument" EOL; return 64;
	}
	const char *filename = argv[optx];
	int rc = process_spec (prog, config, filename, op, tmpldir, outfile);
	return rc;
    }
    if (op == OpMode::cpp) {
	OptMap opts; int optx = 2;
	try {
	    opts = getopts (argc, argv, "ho:t:", optx);
	} catch (runtime_error &e) {
	    cerr << prog << ": " << e.what() << EOL; return 64;
	}
	if (has (opts, 'h')) {
	    cout << "Usage: " << prog <<
		    " cpp [-o outputdir] [-t templatedir] express-spec" EOL;
	    return 0;
	}
	string outdir;
	int rc = check_arg (prog, outdir, "outputdir", "an output directory",
			    opts, 'o', config, "CGENDIR");
	if (rc != 0) { return rc; }
	string tmpldir;
	rc = check_arg (prog, tmpldir, "templatedir", "a template directory",
			opts, 't', config, "TEMPLATEDIR");
	if (rc != 0) { return rc; }
	if (optx >= argc) {
	    cerr << prog << ": Missing an argument" EOL; return 64;
	}
	const char *expspec = argv[optx];
	rc = process_spec (prog, config, expspec, op, tmpldir, outdir);
	return rc;
    }
    cerr << prog << ": gen-mode '" << gmode << "' not implemented." EOL;
    return 64;
}

static int check_arg (const string &prog,
		      string &arg, const char *itname, const char *itdsc,
		      OptMap &opts, char opt, Config &cfg, const string &ci)
{
    if (has (opts, opt)) {
	arg = opts[opt];
    } else if (cfg.has (ci)) {
	arg = cfg[ci];
    } else {
	cerr << prog << ": You must specify " << itdsc << " - either by"
			" configuration ('" << ci << "')\n    or by option"
			" ('-" << opt << " " << itname << "')." EOL;
	return 64;
    }
    if (! fs::is_directory (arg)) {
	cerr << prog << ": '" << arg <<
		"' doesn't exist or is no directory." EOL;
	return 1;
    }
    return 0;
}

static int process_spec (const string &prog, Config &config, const char *spec,
			 OpMode op, const string &tmpldir, const string &outdf)
{
    EXPRESS::Driver driver(config);
    driver.init_symtab();
    driver.init_symtab();
    if (! driver.parse (spec)) {
	cerr << prog << ": Parsing of '" << driver.streamname <<
		"' failed." EOL;
	return 1;
    }
    if (! driver.pass2 (op)) {
	cerr << prog << ": Pass2 (validation) of '" << driver.streamname <<
		"' failed." EOL;
	return 1;
    }
    if (! driver.cgen (op, tmpldir, outdf)) {
	cerr << prog << ": " << to_string (op) <<
		"-code generation failed for '" << driver.streamname <<
		"'" EOL;
	return 1;
    }
    return 0;
}
