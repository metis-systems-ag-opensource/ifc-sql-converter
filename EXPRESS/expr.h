/*! \file expr.h
**
**  \brief Structures which hold parsed expressions (like `a + b`, `3 * x`,
**         ...).
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Structures for generating expression trees
**
*/
#ifndef EXPR_H
#define EXPR_H

#include <cstdint>
#include <string>
#include <vector>

#include "Common/mintypes.h"
#include "Common/fifo.h"

#include "filepos.h"
#include "logical.h"

/*! Alias for bit arrays (used to represent the EXPRESS type `BINARY`. */
using Bitvec = std::vector<bool>;

/*! Base class (struct) of expressions. Any type of expression (`a + b`,
**  `SIN(x)`, `3`, ``'abc'``, ...) is a specialisation of the (struct)
**  class `Expr`.
*/
struct Expr {
    /*! An enumeration allows for a faster identification of the specialisation
    ** of the "real" type of an expression (being it a unary or binary
    ** expression, a literal, a function invocation, ...) than successive
    ** invocations of `dynamic_cast<T *>(expr)`, so this type is introduced
    ** here.
    */
    enum Type : uint8_t {
	invalid, binary, unary, std_invoc, func_invoc, interval, query,
	logical_literal, int_literal, real_literal, binary_literal,
	string_literal, unspec_literal, instance, aggregate,
	nxaggregate, enum_value, ref2const, reference
    } /*Type*/;

    /*! The type of the value of an expression. This is required for the
    **  partial evaluation of constant expressions (literals) and also for
    **  the context analysis (second pass).
    */
    enum class RType : uint8_t {
	error, unknown,
	logical, numeric, bitstring, string, entity, enumtype,
	set, bag, list, array
    } /*RType*/;

    /*! Constructor */
    Expr();

    /* Because of tha nature of expressions trees, all expression types have
    ** a VMT (virtual method table). Because there are situations where a 
    ** virtual method table is not created if there is not a virtual method
    ** with implementation, the destructor is declared virtual, even if it
    ** does not much.
    */
    virtual ~Expr();

    /*! Returns the type (of `Expr::Type`) of the current expression structure
    **  (`unary`, `binary` etc.).
    */
    virtual Type type() const = 0;

    /*! Returns the type name of an expression. This function is primarily
    **  used in error messages.
    */
    virtual const std::string type_name() const { return "UNKNOWN Expr"; }

    /*! Returns the value type (of `Expr::RType`) of an expression. */
    virtual RType rtype() const = 0;

    /*! Creates a copy of an expression (sub-)tree. */
    virtual Expr *Clone() = 0;
//private:
    FilePos position;
public:
    /*! This function is used for storing the the file position(s) of the
    **  expression. It is invoked by the parser, because only there the
    **  position of a particular syntactical element is known.
    */
    void where (const FilePos &p) { position = p; }

    /*! Returns the file position(s) where the expression was found. This
    **  position is required for referring the correct syntactical element
    **  in error messages.
    */
    const FilePos & where() const { return position; }
};

/*! An external function which returns the "name" of the (structure) type of
**  an expression. It is used for error messages (and for debugging).
*/
const char *typetag (const Expr *x);

/*! The type of binary operator which combines two expression trees in a
**  binary expression. Some of the tags end in '_', because there are keywords
**  with the same name in C++.
*/
enum class BinOp : uint8_t {
    lt, gt, le, ge, ne, eq, ine, ieq, pow, add, sub, mul, rdiv, idiv, mod,
    and_, andor, or_, xor_, in, like, iconc, grp, idx, sel
} /*BinOp*/;

/*! The type structure of a binary expression. It consists of two sub-
**  expressions which are combined with a binary operator.
*/
struct BinaryExpr : public Expr {
    /*! Creating a binary expression directly from two sub-expressions and a
    **  binary operator.
    */
    BinaryExpr (BinOp op, Expr *l, Expr *r);

    /*! Forbid copying through construction. */
    BinaryExpr (const BinaryExpr &x) = delete;

    /*! Allow moving through construction */
    BinaryExpr (BinaryExpr &&x);

    /*! Forbid copying through assignment */
    BinaryExpr & operator= (const BinaryExpr &x) = delete;

    /*! Allow moving through assignment */
    BinaryExpr & operator= (BinaryExpr &&x);

    /*! Destroys the current `BinaryExpr` object. */
    ~BinaryExpr();

    /*! Returns `Expr::binary` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "BinaryExpr"; }

    /*! Returns the value type of the binary expression. This type depends
    **  on the operands as well as on the operator.
    */
    RType rtype() const;

    /*! Creates a copy of the (binary) expression (tree). */
    BinaryExpr *Clone();
/**/
    Expr *left, *right;
    BinOp op;
} /*BinaryExpr*/;

/*! The type of a unary operator. Some of the tags end in '_', because there
**  are keywords with the same name in C++.
*/
enum class MonOp : uint8_t {
    plus, minus, not_
};

/*! The type structure of a unary expression. This type of expression combines
**  an expression with a unary operator, which may change the value of this
**  expression.
*/
struct UnaryExpr : public Expr {
    /*! Creates a unary expression directly */
    UnaryExpr (MonOp op, Expr *r);

    /*! Forbid copying through construction. */
    UnaryExpr (const UnaryExpr &x) = delete;

    /*! Allow moving through construction. */
    UnaryExpr (UnaryExpr &&x);

    /*! Forbid copying through assignment. */
    UnaryExpr & operator= (const UnaryExpr &x) = delete;

    /*! Allow moving through assignment. */
    UnaryExpr & operator= (UnaryExpr &&x);

    /*! Destroys the unary expression it is applied on. */
    ~UnaryExpr();

    /*! Returns `Expr::unary` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "UnaryExpr"; }

    /*! Returns the value type of the unary expression. */
    RType rtype() const;

    /*! Creates a copy of the (unary) expression (tree). */
    UnaryExpr *Clone();
/**/
    Expr *right;
    MonOp op;
} /*UnaryExpr*/;


struct Qualifier;

/*! The "type" of a standard function. This comprises all pre-defined functions
**  specified by the EXPRESS data definition language.
*/
enum class StdFunc : uint8_t {
    abs, acos, asin, atan, blength, cos, exists, exp, format, hibound, hiindex,
    length, lobound, log, log2, log10, loindex, nvl, odd, rolesof, sin,
    sizeof_, sqrt, tan, typeof_, usedin, value, value_in, value_unique
};

/*! The invocation of a standard function is another type of an expression.
*/
struct StdInvoc : public Expr {
    /*! Constructing a standard invocation directly. The additional `Ident`
    **  argument allows for an identification of the invoked function through
    **  the symbol table.
    */
    StdInvoc (Ident sf, StdFunc ftag, Fifo<Expr *> &&args);

    /*! Forbid copying through construction. */
    StdInvoc (const StdInvoc &x) = delete;

    /*! Allow moving through construction. */
    StdInvoc (StdInvoc &&x);

    /*! Forbid copying through assignment. */
    StdInvoc & operator= (const StdInvoc &x) = delete;

    /*! Allow moving through assignment. */
    StdInvoc & operator= (StdInvoc &&x);

    /*! Destroys the `StdInvoc` object. */
    ~StdInvoc();

    /*! Returns `Expr::std_invoc` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "StdInvoc"; }

    /*! Returns the value type of a standard invocation. This type primarily
    **  depends on the standard function used.
    */
    RType rtype() const;

    /*! Creates a copy of the (standard invocation) expression (tree). */
    StdInvoc *Clone();
/**/
    Fifo<Expr *> arguments;
    Ident stdfunc;
    StdFunc ftag;
} /*StdInvoc*/;

/*! Structure of the invocation of a "user defined" function. This structure is
**  used for all invocations of functions defined in an express specification.
**  This comprises the construction of ENTITY instances, which are a special
**  type of functions (somewhat like the object constructor functions in C++).
*/
struct FuncInvoc : public Expr {
    /*! Construct a function invocation expression directly.
    **  At this point, it is (probably) not known if this is a normal function
    **  invocation, or an entity construction, so the constructor defaults to
    **  "normal".
    */
    FuncInvoc (Ident func, Fifo<Expr *> args);

    /*! Forbid copying through construction. */
    FuncInvoc (const FuncInvoc &x) = delete;

    /*! Allow moving through construction. */
    FuncInvoc (FuncInvoc &&x);

    /*! Forbid copying through assignment. */
    FuncInvoc & operator= (const FuncInvoc &x) = delete;

    /*! Allow moving through assignment. */
    FuncInvoc & operator= (FuncInvoc &&x);

    /*! Destroys the current function invocation object. */
    ~FuncInvoc();

    /*! Returns `Expr::func_invoc` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "FuncInvoc"; }

    /*! Returns the value type of the function invocation. This type depends
    **  primarily on the return type specified in the function definition, or
    **  the ENTITY the instance construtor function is part of.
    */
    RType rtype() const;

    /*! Creates a copy of the (function invocation) expression (tree). */
    FuncInvoc *Clone();

    /*! Mark this function invocation an ENTITY instance construction */
    void setEntity (bool isEntity = true);
/**/
    Fifo<Expr *> arguments;
    Ident funcref; bool entity;
} /*FuncInvoc*/;

/*! The type of the relational operators used in interval expressions. */
enum class IntervalOp : uint8_t {
    lt, le, gt, ge
};

/*! EXPRESS has a special type of expressions: Intervals. These have the
**  characteristic that they are composed of two relational operators (being
**  either "less than" or "less or equal") and three operands, and – always –
**  a BOOLEAN value.
*/
struct Interval : public Expr {
    /*! Constructs an interval expression directly. */
    Interval (IntervalOp l_op, IntervalOp r_op, Expr *l, Expr *m, Expr *r);

    /*! Forbid copying through construction. */
    Interval (const Interval &x) = delete;

    /*! Allow moving through construction. */
    Interval (Interval &&x);

    /*! Forbid copying through assignment. */
    Interval & operator= (const Interval &x) = delete;

    /*! Allow moving through assignment. */
    Interval & operator= (Interval &&x);

    /*! Destroys the current interval expression. */
    ~Interval();

    /*! Returns `Expr::interval` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "Interval"; }

    /*! Returns the value type of the interval expression, which is always
    **  `Expr::logical' (of `Expr::RType`).
    */
    RType rtype() const;

    /*! Creates a copy of the (interval) expression (tree). */
    Interval *Clone();
/**/
    Expr *left, *middle, *right;
    IntervalOp l_op, r_op;
} /*Interval*/;


/*! A query is a filter expression, applied on an aggregate (ARRAY, LIST, BAG
**  or SET). It defines a local variable (a selector) which is used in a
**  LOGICAL expression to select elements from an aggregate expression, which
**  themselves then form a new aggregate. The aggregate expression used as the
**  source can itself be a query expression.
*/
struct Query : public Expr {
    /*! Constructs a query expression directly.*/
    Query (Ident id, Expr *src, Expr *cc);

    /*! Forbid copying through construction. */
    Query (const Query &x) = delete;

    /*! Allow moving through construction. */
    Query (Query &&x);

    /*! Forbid copying through assignment. */
    Query & operator= (const Query &x) = delete;

    /*! Allow moving through assignment. */
    Query & operator= (Query &&x);

    /*! Destructs the current query expression.*/
    ~Query();

    /*! Returns `Expr::query` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "Query"; }

    /*! Returns the value type of the query expression. */
    RType rtype() const;

    /*! Creates a copy of the (query) expression (tree). */
    Query *Clone();
/**/
    Expr *source, *cond;
    Ident id;
} /*Query*/;

/*! Logical literals comprises the values 'FALSE', 'TRUE' and 'UNKNOWN'. */
struct LogicalLiteral : public Expr {
    /*! Constructs a `LogicalLiteral` expression from a `Logical` value
    ** (defined in `logical.{cc,h})`.
    */
    LogicalLiteral (const Logical &v);

    /*! Construct a `LogicalLiteral` from one of the values `Logical::false_`,
    **  `Logical::unknown`, or `Logical::true_`.
    */
    LogicalLiteral (Logical::Value v);

    /*! Construct a `LogicalLiteral` from either `true` or `false` (from the
    **  C++ standard type `bool`).
    */
    LogicalLiteral (bool b);

    /*! Allow copying through construction, because there is no difference
    **  between "copying" and "moving" for this type.
    */
    LogicalLiteral (const LogicalLiteral &x);

    /*! Allow copying through assignment, because there is no difference
    **  between "copying" and "moving" for this type.
    */
    LogicalLiteral & operator= (const LogicalLiteral &x);

    /*! Destroys the current `LogicalLiteral` value. */
    ~LogicalLiteral();

    /*! Converts a `LogicalLiteral` value to a value of the standard type
    **  `bool`
    */
    bool to_bool();

    /*! Returns `Expr::logical_literal` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "LogicalLiteral"; }

    /*! Returns the value type (aka `Expr::logical` of `Expr::RType`) */
    RType rtype() const;

    /*! Creates a copy of the (logical literal) expression. */
    LogicalLiteral *Clone();
/**/
    Logical value;
} /*LogicalLiteral*/;

/*! An integer literals comprises values like `1`, `-2`, `26`, etc. */
struct IntLiteral : public Expr {
    /*! Construct an `IntLiteral` object directly. The type `Integer` is
    **  defined in `Common/mintypess.h`.
    */
    IntLiteral (Integer v);

    /*! Allow copying through construction because there is no difference
    **  between "copying" and "moving" for this type.
    */
    IntLiteral (const IntLiteral &x);

    /*! Allow copying through assignment because there is no difference
    **  between "copying" and "moving" for this type.
    */
    IntLiteral & operator= (const IntLiteral &x);

    /* Destructs an `IntLiteral` object. */
    ~IntLiteral();

    /* Returns `Expr::int_literal` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "IntLiteral"; }

    /*! Returns `Expr::integer` (of type `Expr::RType`). */
    RType rtype() const;

    /*! Creates a copy of the (integer literal) expression. */
    IntLiteral *Clone();
/**/
    Integer value;
};

/* A real literal comprises values like `1.` `3.14`, `1.414e6`, `1.e-10`, etc.
*/
struct RealLiteral : public Expr {
    /*! Construct an `RealLiteral` object directly. The type `Real` is
    **  defined in `Common/mintypess.h`.
    */
    RealLiteral (Real v);

    /*! Allow copying through construction because there is no difference
    **  between "copying" and "moving" for this type.
    */
    RealLiteral (const RealLiteral &x);

    /*! Allow copying through assignment because there is no difference
    **  between "copying" and "moving" for this type.
    */
    RealLiteral & operator= (const RealLiteral &x);

    /* Destructs an `IntLiteral` object. */
    ~RealLiteral();

    /* Returns `Expr::real_literal` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "RealLiteral"; }

    /*! Returns `Expr::real` (of type `Expr::RType`). */
    RType rtype() const;

    /*! Creates a copy of the (real literal) expression. */
    RealLiteral *Clone();
/**/
    Real value;
} /*RealLiteral*/;

struct BinaryLiteral : public Expr {
    /*! Constructs a binary literal value from the EXPRESS representation of
    **  such a value.
    */
    BinaryLiteral (const std::string &s);

    /*! Constructs a binary literal from a `BitVec` value. */
    BinaryLiteral (const Bitvec &bv);

    /*! Allow copying through construction. */
    BinaryLiteral (const BinaryLiteral &x);

    /*! Allow moving through construction. */
    BinaryLiteral (BinaryLiteral &&x);

    /*! Allow copying through assignment. */
    BinaryLiteral & operator= (const BinaryLiteral &x);

    /*! Allow moving through assignment. */
    BinaryLiteral & operator= (BinaryLiteral &&x);

    /* Destructs a `BinaryLiteral` value. */
    ~BinaryLiteral();

    /* Returns `Type::binary_literal` (of `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "BinaryLiteral"; }

    /*! Returns `Expr::bitstring` (of `Expr::RType`). */
    RType rtype() const;

    /*! Creates a copy of the (binary literal) expression. */
    BinaryLiteral *Clone();

    /*! Returns the length of the `Bitvec value` of the `BinaryLiteral`. */
    uint32_t length();
/**/
    Bitvec value;
} /*BinaryLiteral*/;

/* A string literal comprises of all values enclosed in two single quotes. */
struct StringLiteral : public Expr {
    /*! Constructs a `StringLiteral` value directly. A string literal returned
    **  from the scanner may (or may not) be converted into an internal UTF-8
    **  representation.
    */
    StringLiteral (const std::string &v);

    /*! Allow copying through construction. */
    StringLiteral (const StringLiteral &x);

    /*! Allow moving through construction. */
    StringLiteral (StringLiteral &&x);

    /*! Allow copying through assignment. */
    StringLiteral & operator= (const StringLiteral &x);

    /*! Allow moving through assignment. */
    StringLiteral & operator= (StringLiteral &&x);

    /* Destructs a `StringLiteral` object. */
    ~StringLiteral();

    /* Returns `Expr::string_literal` (of type `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "StringLiteral"; }

    /*! Returns `Expr::string` (of type `Expr::RType`). */
    RType rtype() const;

    /*! Creates a copy of the (string literal) expression. */
    StringLiteral *Clone();
/**/
    std::string value;
} /*StringLiteral*/;

/*! The `UnspecLiteral` is the internal representation of the "value" `?`. It
**  says that the value at the given position is unspecified.
*/
struct UnspecLiteral : public Expr {
    /*! The only valid "value" constructor is the default constructor, because
    **  the `UnspecLiteral` has no value.
    */
    UnspecLiteral();

    /* Allow copying through construction. In this case, this is the same as
    ** "moving" through construction.
    */
    UnspecLiteral (const UnspecLiteral &x);

    /* Allow copying through assignment. In this case, this is the same as
    ** "moving" through assignment.
    */
    UnspecLiteral & operator= (const UnspecLiteral &x);

    /*! ... */
    ~UnspecLiteral();

    /*! Returns `Expr::unspec_literal` (of the Type `Expr::Type`). */
    Type type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "UnspecLiteral"; }

    /*! Returns `Expr::unknown` (if the type `Expr::RType`). */
    RType rtype() const;

    /* Create a copy of the (unspecified literal) expression. */
    UnspecLiteral *Clone();
} /*UnspecLiteral*/;

/*! Qualifier are the constructs for acessing elements of
**    - an aggregate (index qualifier, the `[m]` in `a[m]`),
**    - an ENTITY (attribute qualifier, the `.attr` in `x.attr`),
**  or a supertype of an ENTITY (group qualifier, the `\\st` in `attr\\st`).
**
** This is the abstract super class of all three qualifier types.
*/
struct Qualifier {
    /*! Accessing a type tag is faster than chains of `if (...) else ...`
    **  statements with `dynamic_cast<T *>()` operations for identifying
    **  the real implementation, so here is – again – a type for identifying
    **  the real type behind a `Qualifier *`.
    */
    enum QualType : uint8_t { attrqual, groupqual, indexqual };

    /*! ... */
    Qualifier();

    /*! Because this is an abstract super class, an anchor for generating the
    **  *virtual method table* (VMT) is required. The best candidate for this
    **  is the destructor function, so it is declared as a `virtual` function
    **  here.
    */
    virtual ~Qualifier();

    /*! Returns the type tag (`Qualifier::QualType`) of the real implementation
    **  behind a `Qualifier *`.
    */
    virtual QualType type() const = 0;

    /*! Creates a copy of the qualifier object. */
    virtual Qualifier *Clone() = 0;

    /*! Invoked by the parser, this function stores the file position where
    **  this qualifier can be found in the express specification.
    */
    void where (const FilePos &p) { position = p; }

    /*! Returns the file position where the given qualifier can be found in the
    **  EXPRESS specification. This function is required for an exact
    **  localisation of this syntactical element, in the case of an error.
    */
    const FilePos & where() const { return position; }
/**/
    FilePos position;
} /*Qualifier*/;


/*! An attribute qualifier is used for accessing an ENTITY attribute. Because
**  attributes are local to an ENTITY, this identifier does not necessarily
**  have a symbol table entry in the environment of the instance whose
**  attribute is selected.
*/
struct AttrQualifier : public Qualifier {
    /*! Default constructor */
    AttrQualifier();

    /*! Construct an attribute qualifier directly, using the identifier of the
    **  attribute bring accessed.
    */
    AttrQualifier (Ident id);

    /*! Copying and moving is the same for `AttrQualifier` objects, so the
    **  definition of the copy constructor is sufficient here.
    */
    AttrQualifier (const AttrQualifier &x);

    /*! Destroys an `AttrQualifier` object. */
    ~AttrQualifier();

    /* Returns `Qualifier::attrqual` (of Type `Qualifier::QualType`). */
    QualType type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "AttrQualifier"; }

    /*! Creates a copy of the `AttrQUalifier` object. */
    AttrQualifier *Clone();
/**/
    Ident attrid;
} /*AttrQualifier*/;

/*! A group qualifier is (primarily) used for selecting attributes of the
**  SUPERTYPE of an ENTITY, especially when the attribute of the SUPERTYPE
**  is to be re-declared in the current ENTITY, but also in expressions
**  referring elements of a SUPERTYPE.
*/
struct GroupQualifier : public Qualifier {
    GroupQualifier();
    GroupQualifier (Ident id);
    GroupQualifier (const GroupQualifier &x);
    ~GroupQualifier();

    /*! Returns `Qualifier::groupqual` )of the type `Qualifier::QualType`). */
    QualType type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "GroupQualifier"; }

    /*! Creates a copy of the `GroupQualifier` object. */
    GroupQualifier *Clone();
/**/
    Ident entityid;
} /*GroupQualifier*/;

/*! An index qualifier is not only used for selecting single elements, but for
**  complete slices of an aggregate.
*/
struct IndexQualifier : public Qualifier {
    /*! Default constructor. */
    IndexQualifier();

    /*! Constructs an index qualifier directly. */
    IndexQualifier (Expr *lo, Expr *hi = nullptr);

    /*! Allow copying through construction. */
    IndexQualifier (const IndexQualifier &x);

    /*! Allow moving through construction. */
    IndexQualifier (IndexQualifier &&x);

    /*! Destroys an `IndexQUalifier` object. */
    ~IndexQualifier();

    /*! Returns `Qualifier::indexqual` )of the type `Qualifier::QualType`). */
    QualType type() const;

    /*! Returns a type name primarily to be used in error messages. */
    const std::string type_name() const { return "IndexQualifier"; }

    /*! Creates a copy of the `IndexQUalifier` object. */
    IndexQualifier *Clone();
/**/
    Expr *lox, *hix;
};

/*! `Reference` objects used where expressions denote an object which is
**  further used for accessing parts of it (slices, attributes, ...).
**  E.g., a function invocation returning an instance can itself be part of
**  an index or attribute qualifying expression. But also ar variables often
**  used in such expressions. Even a variable name alone is probably a
**  reference value.
*/
struct Reference : public Expr {
    /*! References may be invocations of standard functions, invocations of
    **  user defined functions, variables, the keyword `SELF`, and references
    **  to qualified variables.
    */
    enum RefType { sinvocref, invocref, varref, selfref, refref };
    Reference();
    Reference (const Reference &x);
    ~Reference();
    virtual RefType reftype() const = 0;
    virtual Reference *Clone() = 0;
} /*Reference*/;

/*! Reference of a standard function invocation. */
struct SInvocRef : public Reference {
    StdInvoc *invoc;
    SInvocRef (StdInvoc *invoc);
    SInvocRef (const SInvocRef &x);
    SInvocRef (SInvocRef &&x);
    ~SInvocRef();
    Type type() const;
    const std::string type_name() const { return "SInvocRef"; }
    RType rtype() const;
    RefType reftype() const;
    SInvocRef *Clone();
} /*StdFuncRef*/;

/*! Reference of the invocation of a user defined function. */
struct InvocRef : public Reference {
    FuncInvoc *invoc;
    InvocRef (FuncInvoc *invoc);
    InvocRef (const InvocRef &x);
    InvocRef (InvocRef &&x);
    ~InvocRef();
    Type type() const;
    const std::string type_name() const { return "InvocRef"; }
    RType rtype() const;
    RefType reftype() const;
    InvocRef *Clone();
} /*FuncRef*/;

/* Reference of a `variable` */
struct VarRef : public Reference {
    Ident id, context;
    bool _isConst;
    VarRef (Ident id, Ident ctx);
    VarRef (const VarRef &x);
    ~VarRef();
    Type type() const;
    const std::string type_name() const { return "VarRef"; }
    RType rtype() const;
    RefType reftype() const;
    VarRef *Clone();
    void isConst (bool b = true);
    bool isConst();
} /*VarRef*/;

/* Reference to the instance itself (expressed by the keyword `SELF`). */
struct SelfRef : public Reference {
    Ident atype;
    SelfRef (Ident atype);
    SelfRef (const SelfRef &x);
    ~SelfRef();
    Type type() const;
    const std::string type_name() const { return "SelfRef"; }
    RType rtype() const;
    RefType reftype() const;
    SelfRef *Clone();
} /*SelfRef*/;

/* Reference of a variable with additional qualifiers. */
struct RefRef : public Reference {
    Reference *reference;
    Qualifier *qualifier;
    RefRef (Reference *ref, Qualifier *qual);
    RefRef (const RefRef &x);
    RefRef (RefRef &&x);
    ~RefRef();
    Type type() const;
    const std::string type_name() const { return "RefRef"; }
    RType rtype() const;
    RefType reftype() const;
    RefRef *Clone();
} /*RefRef*/;

/*! Aggregate expressions are lists of expressions where elements are
**  optionally qualified with a repetition factor.
*/
struct Aggregate : public Expr {
    Aggregate();
    Aggregate (Fifo<Expr *> &&vallist);
    Aggregate (const Aggregate &x);
    Aggregate (Aggregate &&x);
    Aggregate & operator= (Aggregate &&x);
    ~Aggregate();
    Expr * operator[] (uint32_t ix);
    Type type() const;
    RType rtype() const;
    const std::string type_name() const { return "Aggregate"; }
    Aggregate *Clone();
/**/
    Fifo<Expr *> values;
} /*Aggregate*/;

struct AggElement {
    AggElement();
    AggElement (Expr *v);
    AggElement (Expr *v, Expr *rep);
    AggElement (const AggElement &x);
    AggElement (AggElement &&x);
//    AggElement & operator= (const AggElement &x);
    AggElement & operator= (AggElement &&x);
    ~AggElement();
    Fifo<Expr *> expand();
/**/
    Expr *value, *repetition;
} /*AggElement*/;

/*! An expression of the type `NxAggregate` is not constructed by the parser,
**  but by converting each element of an `Aggregate` object into a list of
**  `AggElement::repetition` values and concatenating these lists in the
**  correct order. It is used internally, because the access of aggregate
**  elements (as well as the construction of vluwa of this type is easier to
**  handle with all `AggElement`-values expanded.
*/
struct NxAggregate : public Expr {
    Fifo<AggElement> rvalues;

    NxAggregate();
//    NxAggregate (const Fifo<AggElement> &rvlist);
    NxAggregate (Fifo<AggElement> &&rvlist);
    NxAggregate (const NxAggregate &x);
    NxAggregate (NxAggregate &&x);
    NxAggregate & operator= (NxAggregate &&x);
    ~NxAggregate();
    Type type() const;
    const std::string type_name() const { return "NxAggregate"; }
    RType rtype() const;
    Aggregate * expand();
    NxAggregate *Clone();
} /*NxAggregate*/;

/*! An ENUMERATION value is always a pair of two identifiers, even if the
** type identifier of the ENUMERATION value is seldom used directly. (It
** is determined from the context in most cases.)
*/
struct EnumValue : public Expr {
    EnumValue();
    EnumValue (Ident bt, Ident v);
    EnumValue (const EnumValue &x);
    ~EnumValue();
    Type type() const;
    RType rtype() const;
    const std::string type_name() const { return "EnumValue"; }
    EnumValue *Clone();
/**/
    Ident enumeration, value;
} /*EnumValue*/;

/*! A reference to o a pre-defined (standard?) constant. */
struct Ref2Const : public Expr {
    Ident id, ctx;
    Ref2Const();
    Ref2Const (Ident id, Ident ctx = 0);
    Ref2Const (const Ref2Const &x);
    ~Ref2Const();
    Type type() const;
    RType rtype() const;
    const std::string type_name() const { return "Ref2Const"; }
    Ref2Const *Clone();
} /*Ref2Const*/;

#endif /*EXPR_H*/
