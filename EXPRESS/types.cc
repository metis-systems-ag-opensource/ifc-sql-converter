/* types.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Implementation of 'types.h'
** (Types required for the parser (the symbol table generated by the parser).)
**
*/

#include <iostream>	// delete if no longer required!
using std::cerr, std::endl;

#include "Common/dump.h"

#include <utility>
#include <vector>

#include "types.h"
#include "identbitset.h"
#include "inteval.h"

//using namespace std;
using std::string;
using std::move;
using std::vector;
using std::make_pair;

#if 0
void idset2list (const IdentSet &ids, IdentList &idl)
{
    for (auto it = ids.begin(); it != ids.end(); ++it) { idl.push (*it); }
}
#endif

// Add Ident-values from a list to an Ident-set. The list is destroyed during
// this operation. The return-value is 'true' if all Ident-values from the list
// could be inserted, and 'false' for ambiguous Ident-values in the list.
// The ambiguous Ident-values are returned through the third argument
// ('ambig').
static bool addToIdents (IdentList &in, IdentList &out, IdentList &ambig)
{
    IdentBitSet has, ambigs;
    if (! out.empty()) { has.fill (out); }
    while (! in.empty()) {
	Ident id = in.shift();
	if (has.insert (id)) {
	    out.push (id);
	} else {
	    ambigs.insert (id);
	}
    }
    ambig = ambigs.elements();
    return (ambig.empty() ? true : false);
}

bool idunify (IdentList &idl)
{
    bool unique = true;
    IdentBitSet has;
    for (auto it = idl.begin(); ! it.atend();) {
	if (has.insert (*it)) { ++it; continue; }
	it.remove(); unique = false;
    }
    return unique;
}

static void delete_whereclause (WhereClause &wc)
{
    while (! wc.empty()) {
	WhereCond wcc = wc.shift();
	delete wcc.condition; wcc.label.clear();
    }
}

//struct TypeStruct {
//    FilePos position;
//    Ident symid; uint32_t symctx;
//    bool defined_;
//public:
//    enum Type : uint8_t {
//	// A value to be used if the referenced symbol has no definition (yet)
//	unset,
//	// Real types
//	unknown_type, enumeration, select, defined, indeterminate, boolean,
//	logical, integer, real, number, binary, string, entity,
//	// Elements of type definitions which may occur in type_refs
//	aggregate, array, list, set, bag,
//	// All other elements
//	enum_value, attribute, generic, generic_entity, procedure, function,
//	variable, typeref, rule, stdconst, stdfunc, stdproc, self, queryitem
//    };
TypeStruct::TypeStruct() : position(), _backref(0) { }
TypeStruct::TypeStruct (const TypeStruct &x)
    : position(x.position), _backref(x._backref)
{ }
TypeStruct::~TypeStruct() { }
//    TypeStruct::Type TypeStruct::type() = 0;
//    void where (const FilePos &p) { position = p; }
//    const FilePos & where() { return position; }
//} /*TypeStruct*/;

const char *to_cstring (TypeStruct::Type ttag)
{
    switch (ttag) {
	case TypeStruct::unset:		    return "unset";
	case TypeStruct::unknown_type:	    return "unknown_type";
	case TypeStruct::enumeration:	    return "enumeration";
	case TypeStruct::select:	    return "select";
	case TypeStruct::defined:	    return "defined";
	case TypeStruct::indeterminate:	    return "indeterminate";
	case TypeStruct::boolean:	    return "boolean";
	case TypeStruct::logical:	    return "logical";
	case TypeStruct::integer:	    return "integer";
	case TypeStruct::real:		    return "real";
	case TypeStruct::number:	    return "number";
	case TypeStruct::binary:	    return "binary";
	case TypeStruct::string:	    return "string";
	case TypeStruct::entity:	    return "entity";
	case TypeStruct::aggregate:	    return "aggregate";
	case TypeStruct::array:		    return "array";
	case TypeStruct::list:		    return "list";
	case TypeStruct::set:		    return "set";
	case TypeStruct::bag:		    return "bag";
	case TypeStruct::enum_value:	    return "enum_value";
	case TypeStruct::attribute:	    return "attribute";
	case TypeStruct::generic:	    return "generic";
	case TypeStruct::generic_entity:    return "generic_entity";
	case TypeStruct::procedure:	    return "procedure";
	case TypeStruct::function:	    return "function";
	case TypeStruct::variable:	    return "variable";
	case TypeStruct::typeref:	    return "typeref";
	case TypeStruct::rule:		    return "rule";
	case TypeStruct::stdconst:	    return "stdconst";
	case TypeStruct::stdfunc:	    return "stdfunc";
	case TypeStruct::stdproc:	    return "stdproc";
	case TypeStruct::self:		    return "self";
	case TypeStruct::queryitem:	    return "queryitem";
    }
    return "";
}

std::string to_string (TypeStruct::Type ttag)
{
    return to_cstring (ttag);
}

//class TsType : public TypeStruct {
//    Ident symid; uint32_t symctx;
TsType::TsType() : TypeStruct(), symid(0), symctx(0) { }

TsType::TsType (const TsType &x)
    : TypeStruct(x), symid(x.symid), symctx(x.symctx)
{  }

TsType::~TsType() { }

void TsType::setBackref (Ident id, uint32_t ctx) {
    symid = id; symctx = ctx;
}

void TsType::getBackref (Ident &id, uint32_t &ctx) {
    id = symid; ctx = symctx;
}
//} /*TsType*/;

// To be used for new (yet undefined) names, such as defined types, entities,
// function and the like.
//struct TsUnset : public TsType {
//    Type _expected_type;
TsUnset::TsUnset (Type exp_type) : TypeStruct(), _expected_type(exp_type) { }
TsUnset::TsUnset (TsUnset &x)
    : TypeStruct(x), _expected_type(x._expected_type)
{ }

TsUnset::~TsUnset() { }

TypeStruct::Type TsUnset::type() const { return TypeStruct::unset; }

TypeStruct::Type TsUnset::expected_type() { return _expected_type; }

void TsUnset::expected_type (TypeStruct::Type exp_type) {
    _expected_type = exp_type;
}
//} /*TsUnset*/;

//struct TsWithWhereClause : public TsType {
//    WhereClause whereclause;
TsWithWhereClause::TsWithWhereClause() : TsType(), whereclause() { }

TsWithWhereClause::TsWithWhereClause (WhereClause &wc)
    : TsType(), whereclause(move(wc))
{ }

TsWithWhereClause::TsWithWhereClause (TsWithWhereClause &&x)
    : TsType(x), whereclause(move(x.whereclause))
{ }

TsWithWhereClause::~TsWithWhereClause() { clear(); }

void TsWithWhereClause::clear() { delete_whereclause (whereclause); }

void TsWithWhereClause::addWhereClause (WhereClause * &wc) {
    if (wc) {
	whereclause = move(*wc); delete wc; wc = nullptr;
    }
}
//} /*TsWithWhereClause*/;

//struct TsAggregate : public TsType {
//    TypeStruct *basetype;
//    std::string label;
TsAggregate::TsAggregate() : label() { basetype = nullptr; }

TsAggregate::TsAggregate (const std::string &lbl, TypeStruct *bt)
    : TsType(), basetype(bt), label(lbl)
{ }

TsAggregate::TsAggregate (std::string * &lbl, TypeStruct *bt)
    : TsType(), basetype(bt), label(lbl ? *lbl : "")
{
    if (lbl) { delete lbl; lbl = nullptr; }
}

TsAggregate::TsAggregate (TsAggregate &&x)
    : TsType(x), basetype (x.basetype), label(move(x.label))
{
    x.basetype = nullptr;
}

TsAggregate::~TsAggregate() { delete basetype; }

//void TsAggregate::addLabel (const std::string &lbl) { label = lbl; }

TypeStruct::Type TsAggregate::type() const { return TypeStruct::aggregate; }
//} /*TsAggregate*/;

//struct TsAggregateLike : public TsWithWhereClause {
//    TypeStruct *basetype;
//    Expr *lwb, *upb;

TsAggregateLike::TsAggregateLike()
    : TsWithWhereClause(), basetype(nullptr), lwb(nullptr), upb(nullptr)
{ }

TsAggregateLike::TsAggregateLike (Expr *low, Expr *high, TypeStruct *bt)
    : TsWithWhereClause(), basetype(bt), lwb(low), upb(high)
{ }

TsAggregateLike::TsAggregateLike (TsAggregateLike &&x)
    : TsWithWhereClause(move(x)), basetype(x.basetype), lwb(x.lwb), upb(x.upb)
{
    x.basetype = nullptr;
    x.lwb = nullptr;
    x.upb = nullptr;
}

TsAggregateLike::~TsAggregateLike()
{
    if (basetype) { delete basetype; }
    if (lwb) { delete lwb; }
    if (upb) { delete upb; }
}
//} /*TsAggregateLike*/;

//struct TsArray2 : public TsAggregateLike {
//    int32_t ilwb, iupb;
//    bool optional, unique, evaluated;
TsArray2::TsArray2 (Expr *low, Expr *high, TypeStruct *bt)
    : TsAggregateLike (low, high, bt), ilwb(1), iupb(0),
      optional(false), unique(false), evaluated(false)
{ }

TsArray2::TsArray2 (TsArray2 &&x)
    : TsAggregateLike (move(x)), ilwb(x.ilwb), iupb(x.iupb),
      optional(x.optional), unique(x.unique), evaluated(x.evaluated)
{ }

TsArray2::~TsArray2() { }

TypeStruct::Type TsArray2::type() const { return TypeStruct::array; }

void TsArray2::optionalElements (bool opt) { optional = opt; }

void TsArray2::uniqueElements (bool uniq) { unique = uniq; }

bool TsArray2::lh_eval() {
    if (! evaluated) {
	std::optional<long> ov;
	bool ldone = false, rdone = false;
	if (lwb && (ov = intEval (lwb))) { ilwb = *ov; ldone = true; }
	if (upb && (ov = intEval (upb))) { iupb = *ov; rdone = true; }
	evaluated = ldone && rdone;
    }
    return evaluated;
}
//} /*TsArray2*/;

//struct TsLBS : public TsAggregateLike {
//    uint32_t min, max;
    TsLBS::TsLBS (Expr *low, Expr *high, TypeStruct *bt)
	: TsAggregateLike(low, high, bt), min(0), max(0)
    { }

    TsLBS::TsLBS (TsLBS &&x)
	: TsAggregateLike (move(x)), min(x.min), max(x.max)
    { }

    bool TsLBS::lh_eval() {
	std::optional<long> ov;
	if (lwb) {
	    if (! (ov = intEval (lwb))) { return false; }
	    min = (uint32_t) *ov;
	} else {
	    min = 0;
	}
	if (upb && upb->type() != Expr::unspec_literal) {
	    if (! (ov = intEval (upb))) { return false; }
	    max = (uint32_t) *ov;
	} else {
	    max = UINT32_MAX;
	}
	return true;
    }
//} /*TsLBS*/;

//struct TsList : public TsLBS {
//    bool unique;

TsList::TsList (Expr *low, Expr *high, TypeStruct *bt)
    : TsLBS(low, high, bt), unique(false)
{ }

TsList::TsList (Expr *low, TypeStruct *bt)
    : TsLBS(low, nullptr, bt), unique(false)
{ }

TsList::TsList (TypeStruct *bt)
    : TsLBS(nullptr, nullptr, bt), unique(false)
{ }

TsList::TsList (TsList &&x) : TsLBS(move(x)), unique(x.unique) { }

TsList::~TsList() { }

TypeStruct::Type TsList::type() const { return TypeStruct::list; }

void TsList::uniqueElements (bool uniq) { unique = uniq; }
//} /*TsList*/;

//struct TsBag : public TsLBS {
TsBag::TsBag (Expr *low, Expr *high, TypeStruct *bt) : TsLBS(low, high, bt) { }

TsBag::TsBag (uint32_t min, uint32_t max, TypeStruct *bt)
    : TsLBS(new IntLiteral (min), new IntLiteral (max), bt)
{
    this->min = min; this->max = max;
}

TsBag::TsBag (Expr *low, TypeStruct *bt) : TsLBS(low, nullptr, bt) { }

TsBag::TsBag (TypeStruct *bt) : TsLBS(nullptr, nullptr, bt) { }

TsBag::TsBag (TsBag &&x) : TsLBS(move(x)) { }

TsBag::~TsBag() { }

TypeStruct::Type TsBag::type() const { return TypeStruct::bag; }
//} /*TsBag*/;

//struct TsSet : public TsLBS {
TsSet::TsSet (Expr *low, Expr *high, TypeStruct *bt) : TsLBS(low, high, bt) { }

TsSet::TsSet (uint32_t min, uint32_t max, TypeStruct *bt)
    : TsLBS(new IntLiteral (min), new IntLiteral (max), bt)
{
    this->min = min; this->max = max;
}

TsSet::TsSet (Expr *low, TypeStruct *bt) : TsLBS(low, nullptr, bt) { }

TsSet::TsSet (TypeStruct *bt) : TsLBS(nullptr, nullptr, bt) { }

TsSet::TsSet (TsSet &&x) : TsLBS(move(x)) { }

TsSet::~TsSet() { }

TypeStruct::Type TsSet::type() const { return TypeStruct::set; }
//} /*TsSet*/;


//struct TsEnum : public TsWithWhereClause {
//    IdentSet values;    // Verweise auf die Symboltabellen-Einträge
//    WhereClause whereclause;
//    Ident basedon;
//    bool extensible;            // Nicht für IFC
TsEnum::TsEnum()
    : TsWithWhereClause(), values(), basedon(0), extensible(false)
{ }

TsEnum::TsEnum(IdentList &envals)
    : TsWithWhereClause(), values(envals), basedon(0), extensible(false)
{ }

TsEnum::TsEnum(TsEnum &&x)
    : TsWithWhereClause(move(x)), values(x.values), basedon(x.basedon),
      extensible(x.extensible)
{ }

TsEnum::~TsEnum() { clear(); }

TypeStruct::Type TsEnum::type() const { return TypeStruct::enumeration; }

// WARNING! This function is _destructive_ on its first argument.
bool TsEnum::addToValues (IdentList &idl, IdentList &ambig)
{
    return addToIdents (idl, values, ambig);
}

bool TsEnum::addToValues (IdentList * &ids, IdentList &ambig)
{
    if (ids) {
	bool res = addToIdents (*ids, values, ambig);
	delete ids; ids = nullptr;
	return res;
    }
    return true;
}

bool TsEnum::has (Ident enid) {
    for (Ident id: values) {
	if (id == enid) { return true; }
    }
    return false;
}

void TsEnum::setBasetype (Ident id) { basedon = id; }

void TsEnum::setExtensible (bool ext) { extensible = ext; }
//} /*TsEnum*/;

//struct TsEnumValue : public TypeStruct {
//    IdentList basetypes;     // NEED a list/a set!
//    uint32_t orderval;
//    WhereClause whereclause;
TsEnumValue::TsEnumValue()
    : basetypes(), orderval (0)
{ }
TsEnumValue::TsEnumValue (TsEnumValue &&x)
    : TypeStruct(x), basetypes(move(x.basetypes)), orderval(x.orderval)
{ }
TsEnumValue::~TsEnumValue() { }
TypeStruct::Type TsEnumValue::type() const { return TypeStruct::enum_value; }
void TsEnumValue::set_basetype (Ident bt) {
    IdentBitSet has;
    if (! basetypes.empty()) { has.fill (basetypes); }
    if (has.insert (bt)) {
	basetypes.push (bt);
    }
}
void TsEnumValue::set_orderval (uint32_t ov) { orderval = ov; }
//} /*TsEnumVal*/;

//struct TsSelect : public TsWithWhereClause {
//    IdentSet types, flatTypes;
//    Ident basetype;
//    bool isextensible, isgeneric, flattened;
TsSelect::TsSelect()
    : TsWithWhereClause(), types(), flatTypes(), basetype(0),
      isextensible(false), isgeneric(false), flattened(false)
{ }

TsSelect::TsSelect (const IdentList &seltypes)
    : TsWithWhereClause(), types(seltypes), flatTypes(), basetype(0),
      isextensible(false), isgeneric(false), flattened(false)
{ }

TsSelect::TsSelect (TsSelect &&x)
    : TsWithWhereClause(move(x)), types(move(x.types)),
      flatTypes(move(x.flatTypes)), basetype(x.basetype),
      isextensible(x.isextensible), isgeneric(x.isgeneric),
      flattened(x.flattened)
{ }

TsSelect::~TsSelect() { clear(); }

TypeStruct::Type TsSelect::type() const { return TypeStruct::select; }

// Insert the (type-)idents from the supplied ident-list into the set 'types',
// destroying the list in this process ...
bool TsSelect::addToTypes (IdentList &idl, IdentList &ambig) {
    return addToIdents (idl, types, ambig);
}

bool TsSelect::addToTypes (IdentList * &idl, IdentList &ambig) {
    if (idl) {
	bool res = addToIdents (*idl, types, ambig);
	delete idl; idl = nullptr;
	return res;
    }
    return true;
}

void TsSelect::setBasetype (Ident bt) { basetype = bt; }

void TsSelect::setExtensible (bool isExtensible) {
    isextensible = isExtensible;
}

void TsSelect::setGeneric (bool isGeneric) { isgeneric = isGeneric; }
//} /*TsSelect*/;

//struct TsDefined : public TsWithWhereClause {
//    Ident basetype;
TsDefined::TsDefined(Ident fromtype)
    : TsWithWhereClause(), basetype(fromtype)
{ }

TsDefined::TsDefined(TsDefined &&x)
    : TsWithWhereClause(move(x)), basetype(x.basetype)
{ }

TsDefined::~TsDefined() { clear(); }

TypeStruct::Type TsDefined::type() const { return TypeStruct::defined; }
//} /*TsDefined*/;

//struct TsGeneric : public TypeStruct {
//    bool entity;
//    std::string label;
TsGeneric::TsGeneric (bool isEntity) : label(), entity(isEntity) { }

TsGeneric::TsGeneric (const std::string &lbl, bool isEntity)
    : label(lbl), entity(isEntity)
{ }

TsGeneric::TsGeneric (std::string * &lbl, bool isEntity)
    : label(lbl ? *lbl : ""), entity(isEntity)
{
    if (lbl) { delete lbl; lbl = nullptr; }
}

TsGeneric::TsGeneric (TsGeneric &&x)
    : TypeStruct(x), label(move(x.label)), entity(x.entity)
{ }

TsGeneric::~TsGeneric() { }

TypeStruct::Type TsGeneric::type() const {
    return entity ? TypeStruct::generic_entity : TypeStruct::generic;
}
//} /*TsGeneric*/;

//struct TsSimple : public TsWithWhereClause {
TsSimple::TsSimple() : TsWithWhereClause() { }

TsSimple::TsSimple (TsSimple &&x)
    : TsWithWhereClause(move(x))
{ }

TsSimple::~TsSimple() { clear(); }
//} /*TsSimple*/;

//struct TsIndeterminate : public TsSimple {
TsIndeterminate::TsIndeterminate() : TsSimple() { }
TsIndeterminate::TsIndeterminate (TsIndeterminate &&x)
    : TsSimple(move(x))
{ }
TsIndeterminate::~TsIndeterminate() { };
TypeStruct::Type TsIndeterminate::type() const {
   return TypeStruct::indeterminate;
}
//} /*TsIndeterminate*/;

//struct TsBoolean : public TsSimple {
TsBoolean::TsBoolean() : TsSimple() { }
TsBoolean::TsBoolean (TsBoolean &&x) : TsSimple(move(x)) { }
TsBoolean::~TsBoolean() { }
TypeStruct::Type TsBoolean::type() const { return TypeStruct::boolean; }
//} /*TsBoolean*/;

//struct TsLogical : public TsSimple {
TsLogical::TsLogical() : TsSimple() { }
TsLogical::TsLogical (TsLogical &&x) : TsSimple(move(x)) { }
TsLogical::~TsLogical() { }
TypeStruct::Type TsLogical::type() const { return TypeStruct::logical; }
//} /*TsLogical*/;

//struct TsInteger : public TsSimple {
TsInteger::TsInteger() { }
TsInteger::TsInteger (TsInteger &&x) : TsSimple(move(x)) { }
TsInteger::~TsInteger() { clear(); }
TypeStruct::Type TsInteger::type() const { return TypeStruct::integer; }
//} /*TsInteger*/;

//struct TsReal : public TsSimple {
TsReal::TsReal() { }
TsReal::TsReal (TsReal &&x) : TsSimple(move(x)) { }
TsReal::~TsReal() { }
TypeStruct::Type TsReal::type() const { return TypeStruct::real; }
//} /*TsReal*/;

//struct TsNumber : public TsSimple {
TsNumber::TsNumber() { }
TsNumber::TsNumber (TsNumber &&x) : TsSimple(move(x)) { }
TsNumber::~TsNumber() { }
TypeStruct::Type TsNumber::type() const { return TypeStruct::number; }
//} /*TsNumber*/;

//struct TsStringLike : public TsWithWhereClause {
//    uint32_t size;      // 0 means unlimited
//    bool fixed;         // fixed for fixed size strings
TsStringLike::TsStringLike (uint32_t maxsz, bool fixed)
    : size(maxsz), fixed(fixed)
{ }

TsStringLike::TsStringLike (TsStringLike &&x)
    : TsWithWhereClause(move(x)), size(x.size), fixed(x.fixed)
{ }

TsStringLike::~TsStringLike() { }
//} /*TsStringLike*/;

//struct TsBinary : public TsStringLike {
TsBinary::TsBinary (uint32_t maxsz, bool fixed)
    : TsStringLike (maxsz, fixed)
{ }

TsBinary::TsBinary (TsBinary &&x) : TsStringLike (move(x)) { }

TsBinary::~TsBinary() { }

TypeStruct::Type TsBinary::type() const { return TypeStruct::binary; }

//    const std::string type_name() { return "TsBinary"; }
//} /*TsBinary*/

//struct TsString : public TsStringLike {
TsString::TsString (uint32_t maxsz, bool fixed)
    : TsStringLike (maxsz, fixed)
{ }

TsString::TsString (TsString &&x) : TsStringLike (move(x)) { }

TsString::~TsString() { }

TypeStruct::Type TsString::type() const { return TypeStruct::string; }

//    const std::string type_name() { return "TsString"; }
//} /*TsString*/

//struct Attr { 
//    enum Type { explicit_attr, derived_attr, inverse_attr };
//    std::string origname;
//    Ident baseentity;
Attr::~Attr() { }
//    virtual Type type() = 0;
//} /*Attr*/;

//// Explicit attributes directly contribute to an ENTITY value.
//struct ExplicitAttr : public Attr {
//    shared_ptr<TypeStruct> definition;
//    bool optional;
ExplicitAttr::~ExplicitAttr() { definition.reset(); }
Attr::Type ExplicitAttr::type() const { return explicit_attr; }
//} /*ExplicitAttr*/;

//// DERIVE attributes contribute in another way. They represent formulas
//// (expressions) which need to be evaluated at runtime.
//struct DerivedAttr : public Attr {
//    shared_ptr<TypeStruct> definition;
//    Expr *value;
DerivedAttr::DerivedAttr ()
    : definition(), value(nullptr)
{ }
#if 0 // NEVER USED
DerivedAttr::DerivedAttr (const DerivedAttr &x)
    : definition(x.definition), value(x.value->Clone())
{ }

DerivedAttr::DerivedAttr (DerivedAttr &&x)
    : definition(x.definition), value(x.value)
{
    x.value = nullptr;
}
#endif // NEVER USED

DerivedAttr::~DerivedAttr() {
    definition.reset();
    delete value;
}

Attr::Type DerivedAttr::type() const { return derived_attr; }
//} /*DerivedAttr*/;

//// INVERSE attributes form relationships (and as such constraints) on entities
//// (and ENTITY-values). They have a more complex structure, which is
//// represented with the following two types.
//struct SetBag {
//    Bounds b;
//    bool isabag;
//} /*SetBag*/;
//
//struct InverseAttr : public Attr {
//    SetBag *aggopt;
//    struct { Ident name, entity; } forAttr;
//    Ident entityref;
InverseAttr::~InverseAttr() { if (aggopt) { delete aggopt; } }
Attr::Type InverseAttr::type() const { return inverse_attr; }
//} /*InverseAttr*/;

//struct TsEntity : public TypeStruct {
//    bool abstract;
//    IdentSet subtypes;	// must be replaced with supertype-expr
//    IdentSet supertypes;
//    IdentKeyMap<Attr *> attributes;
//    Fifo<UniqueRel> uniques;
//    WhereClause whereclause;
TsEntity::TsEntity (bool isAbstract)
    : TsWithWhereClause(), mysubtypes(), coll_subtypes(), mysupertypes(),
      attributes(), attrOrder(), flatAttributes(), uniques(),
      abstract(isAbstract), flattened(false)
{ }

TsEntity::TsEntity (TsEntity &&x)
  : TsWithWhereClause(move(x)), mysubtypes(move(x.mysubtypes)),
    coll_subtypes(move(x.coll_subtypes)),
    mysupertypes(move(x.mysupertypes)), attributes(move(x.attributes)),
    attrOrder(move(x.attrOrder)), flatAttributes(move(x.flatAttributes)),
    uniques(move(x.uniques)), abstract(x.abstract), flattened(x.flattened)
{ }

TsEntity::~TsEntity() {
    mysubtypes.clear(); mysupertypes.clear();

    // In this new 
    while (! flatAttributes.empty()) {
	Attr *attr = (flatAttributes.shift()).attr;
	delete attr;
    }

#if 0
    for (auto &idattrpair: attributes) {
	delete idattrpair.second;
    }
#endif
    for (auto [id, attr]: attributes) {
	delete attr;
    }
    attributes.clear(); attrOrder.clear();
    uniques.clear();
//    while (! uniques.empty()) {
//	UniqueRel x = uniques.shift();
//	//delete x.label;
//    }
    clear();	// Delete the where-conditions
}

TypeStruct::Type TsEntity::type() const { return TypeStruct::entity; }

// Add the values from the SUPERTYPE-specification, meaning the types of which
// this type is the SUPERTYPE, thus the list of subtypes
void TsEntity::addSupertypes (IdentList &supert)
{
    IdentBitSet has;
    if (! mysubtypes.empty()) { has.fill (mysubtypes); }
    while (! supert.empty()) {
	Ident id = supert.shift();
	if (has.insert (id)) { mysubtypes.push (id); }
    }
}

void TsEntity::addSupertypes (IdentList * &supert)
{
    if (supert) {
	addSupertypes (*supert); delete supert; supert = nullptr;
    }
}

// Add the (list of the) type(s) of which this type is the SUBTYPE, thus
// the supertypes
void TsEntity::addSubtypes (IdentList &subt)
{
    IdentBitSet has;
    if (! mysupertypes.empty()) { has.fill (mysupertypes); }
    while (! subt.empty()) {
	Ident id = subt.shift();
	if (has.insert (id)) { mysupertypes.push (id); }
    }
}

void TsEntity::addSubtypes (IdentList * &subt)
{
    if (subt) {
	addSubtypes (*subt); delete subt; subt = nullptr;
    }
}

bool TsEntity::addAttributes (Fifo<NameAttr> &nameattrs, IdentList &ambig) {
    IdentBitSet has, ambigs;
    if (! attrOrder.empty()) { has.fill (attrOrder); }
    if (! ambig.empty()) { ambigs.fill (ambig); }
    while (! nameattrs.empty()) {
	NameAttr na = nameattrs.shift();
	Ident &key = na.first;
	auto ir = attributes.insert (attributes.begin(), na);
	if ((*ir).second) {
	    attrOrder.push (key);
	} else {
	    delete na.second;
	    ambigs.insert (key);
	}
    }
    ambig = ambigs.elements();
    return (ambig.empty() ? true : false);
}

void TsEntity::addUniques (const Fifo<UniqueRel> &uniq) {
    uniques = uniq;
}
//} /*TsEntity*/;

//// This is the common part of 'TsProcedure' and 'TsRule' ...
//struct TsAlgorithm : public TypeStruct {
//    IdentKeyMap<TypeStruct *> localdefs;
//    Fifo<Stmt *> body;
TsAlgorithm::TsAlgorithm() : localdefs(), body() { }

TsAlgorithm::TsAlgorithm (TsAlgorithm &&x)
    : TypeStruct(x), localdefs(move(x.localdefs)), body(move(x.body))
{ }

TsAlgorithm::~TsAlgorithm()
{
    for (auto &localdef: localdefs) {
	delete localdef.second; localdef.second = nullptr;
    }
    localdefs.clear();
    while (! body.empty()) {
	Stmt *s = body.shift();
	delete s;
    }
}

bool TsAlgorithm::addLocal (Ident id, TypeStruct *ts)
{
    auto ir = localdefs.insert (localdefs.begin(), make_pair (id, ts));
    return (*ir).second;
}

bool TsAlgorithm::addLocals (IdentKeyMap<TypeStruct *> &locals,
			     IdentList &ambig)
{
    IdentBitSet ambigs;
    for (auto &local: locals) {
	Ident id = local.first;
	auto ir = localdefs.insert (localdefs.begin(), local);
	if (! (*ir).second) {
	    ambigs.insert (id);
	}
    }
    ambig = ambigs.elements();
    return (ambig.empty() ? true : false);
}

// DESTRUCTIVE on its argument
void TsAlgorithm::addBody (Fifo<Stmt *> &stmts) {
    body.push (move (stmts));
}

// DESTRUCTIVE on its argument
void TsAlgorithm::addBody (Fifo<Stmt *> * &stmts) {
    if (stmts) {
	addBody (*stmts); delete stmts; stmts = nullptr;
    }
}
//} /*TsAlgorithm*/;


//struct ProcArg {
//    TypeStruct *arg;
//    bool isvararg;
ProcArg::ProcArg() : arg(nullptr), isvararg(false) { }
ProcArg::ProcArg (TypeStruct *arg, bool isvar)
    : arg (arg), isvararg (isvar)
{ }
ProcArg::ProcArg (const ProcArg &x) : arg (x.arg), isvararg (x.isvararg) { }
ProcArg::ProcArg (ProcArg &&x)
    : arg(x.arg), isvararg(x.isvararg)
{
    x.arg = nullptr;
}
//    ProcArg & operator= (const ProcArg &x) = default;
ProcArg::~ProcArg() { }
//} /*ProcArg*/;

// A procedure consists of
//   - a parameter list
//   - a set of local declarations (including variables)
//   - a statement list (the procedure body)
//   - a set of WHERE-conditions
// The parameter list of a procedure consists of associations between 'Ident'-
// values and operational modes: the position of a parameter within the
// parameter list and if this parameter is a VAR-parameter.
// Because procedures functions are so similar, i will use this 'TypeStruct'
// sub-type for both.
//struct TsProcedure : public TsAlgorithm {
//    struct Param { uint32_t pos; bool isvar; };
//    IdentKeyMap<Param> parameters;
//    TypeStruct *returntype;
TsProcedure::TsProcedure()
    : TsAlgorithm(), parameters(), returntype(nullptr)
{ }

TsProcedure::TsProcedure(TsProcedure &&x)
    : TsAlgorithm(move(x)), parameters(move(x.parameters)),
      returntype(x.returntype)
{
    x.returntype = nullptr;
}

TsProcedure::~TsProcedure() {
    parameters.clear();
    if (returntype) { delete returntype; }
}

TypeStruct::Type TsProcedure::type() const {
    return returntype ? TypeStruct::function : TypeStruct::procedure;
}

//    TsProcedure &operator= (const TsProcedure &x) = delete;

TsProcedure &TsProcedure::operator= (TsProcedure &&x) {
    localdefs = x.localdefs; x.localdefs.clear();
    body = x.body; x.body.clear();
    parameters = x.parameters; x.parameters.clear();
    returntype = x.returntype; x.returntype = nullptr;
    return *this;
}

bool TsProcedure::addParameters (Fifo<ParamType> &params, IdentList &ambig)
{
    uint32_t pos = 0;
    IdentBitSet ambigs;
    while (! params.empty()) {
	ParamType pt = params.shift();
	TsProcedure::Param p;
	p.pos = pos++; p.isvar = pt.isvar;
	auto ir = parameters.insert (parameters.begin(), make_pair (pt.id, p));
	if (! (*ir).first) {
	    ambigs.insert (pt.id);
	}
    }
    ambig = ambigs.elements();
    return (ambig.empty() ? true : false);
}

bool TsProcedure::addParameters (Fifo<ParamType> * &params, IdentList &ambig)
{
    if (params) {
	bool res = addParameters (*params, ambig);
	delete params; params = nullptr;
	return res;
    }
    return true;
}

void TsProcedure::addReturntype (TypeStruct *rtt) {
    returntype = rtt;
}

Fifo<ProcArg> TsProcedure::getProcargs()
{
    size_t arity = parameters.size();
    vector<ProcArg> patmp (arity);
    for (auto &param: parameters) {
	Ident parid = param.first;
	Param pv = param.second;
	auto pvtype = dynamic_cast<TsVar *>(localdefs[parid]);
	TypeStruct *ts = pvtype->vtype.get();
	patmp[pv.pos] = ProcArg (ts, pv.isvar);
    }
    Fifo<ProcArg> res;
    for (size_t ix = 0; ix < arity; ++ix) { res.push (patmp[ix]); }
    return res;
}
    
//} /*TsProcedure*/;

//struct TsRule : public TsAlgorithm {
//    IdentSet entity_refs;
//    WhereClause whereclause;
    TsRule::TsRule() : TsAlgorithm(), entity_refs(), whereclause() { }

    TsRule::TsRule (TsRule &&x)
        : TsAlgorithm(move(x)), entity_refs(move(x.entity_refs)),
          whereclause(move(x.whereclause))
    { }

    TsRule::~TsRule() {
        entity_refs.clear();
        delete_whereclause (whereclause);
    }

    TypeStruct::Type TsRule::type() const { return TypeStruct::rule; }

    // DESTRUCTIVE on the first argument!
    bool TsRule::addEntities (IdentList &erefs, IdentList &ambig) {
	IdentBitSet has, ambigs;
	while (! erefs.empty()) {
	    Ident entid = erefs.shift();
	    if (has.insert (entid)) {
		entity_refs.push (entid);
	    } else {
		ambigs.insert (entid);
	    }
	}
	ambig = ambigs.elements();
	return (ambig.empty() ? true : false);
    }

    void TsRule::setEntities (IdentList &erefs) {
	IdentList ambig;
	entity_refs.clear();
	(void) addEntities (erefs, ambig);
    }

    // DESTRUCTIVE on the first argument!
    bool TsRule::addEntities (IdentList * &erefs, IdentList &ambig) {
	bool done = true;
	if (erefs) {
	    done = addEntities (*erefs, ambig); delete erefs; erefs = nullptr;
	}
	return done;
    }

    // DESTRUCTIVE on its argument!
    void TsRule::addWhereClause (WhereClause &wc) {
	whereclause = move(wc);
    }

    // DESTRUCTIVE on its argument!
    void TsRule::addWhereClause (WhereClause * &wc) {
	if (wc) {
	    whereclause = move(*wc); delete wc; wc = nullptr;
	}
    }
//} /*TsRule*/;

//struct TsVar : public TypeStruct {
//    TypeStruct *vtype;
TsVar::TsVar() : vtype(), init(nullptr) { }
TsVar::TsVar (TsVar &&x)
    : TypeStruct(move(x)), vtype(x.vtype), init(x.init)
{
    // Force the original value being unset! This is necessary, because in
    // the invocation of its destructor, the 'init'-expression may be
    // destroyed, which would be fatal for the object it was moved to.
    // I assume that the handling of the 'shared_ptr' initialization is
    // handled correctly by the initialization above.
    x.vtype.reset(); x.init = nullptr;
}
TsVar::~TsVar() {
    if (init) { delete init; }
}
TypeStruct::Type TsVar::type() const { return TypeStruct::variable; }
void TsVar::addType (shared_ptr<TypeStruct> &t) { vtype = t; }
void TsVar::initValue (Expr *e) { init = e; }
//} /*TsVar*/;

//struct TsAttribute : public TypeStruct {
//    shared_ptr<TypeStruct> vtype;
//    Ident entity_id;
//    bool optional;
TsAttribute::TsAttribute() : vtype(), entity_id(0), optional(false) { }
//TsAttribute::TsAttribute (shared_ptr<TypeStruct> &t);
TsAttribute::TsAttribute (TsAttribute &&x)
    : TypeStruct(x), vtype(x.vtype), entity_id(x.entity_id),
      optional(x.optional)
{
    x.vtype.reset();
}
TsAttribute::~TsAttribute() { vtype.reset(); }
TypeStruct::Type TsAttribute::type() const { return TypeStruct::attribute; }
void TsAttribute::addType (shared_ptr<TypeStruct> &t) { vtype = t; }
void TsAttribute::setEntity (Ident id) { entity_id = id; }
//} /*TsAttribute*/;

//struct TsQueryItem : public TypeStruct {
//    TypeStruct *itype;
TsQueryItem::TsQueryItem() : itype(nullptr) { }
TsQueryItem::TsQueryItem (TsQueryItem &&x)
    : TypeStruct(x), itype(x.itype)
{
    x.itype = nullptr;
}
TsQueryItem::~TsQueryItem() {
    // Don't know what to do with 'itype' yet ...
    delete itype;
}
TypeStruct::Type TsQueryItem::type() const { return TypeStruct::queryitem; }
//    const std::string TsQueryItem::type_name() { return "TsQueryItem"; }
void TsQueryItem::addType (TypeStruct *t) { itype = t; }
//} /*TsQueryItem*/;

//struct TsTypeRef : public TypeStruct {
//    Ident id;
//    bool isentity;
TsTypeRef::TsTypeRef(): id(0), isentity(false) { }
TsTypeRef::TsTypeRef (Ident id, bool isentity)
    : id(id), isentity(isentity)
{ }
TsTypeRef::TsTypeRef (const TsTypeRef &x)
    : TypeStruct(x), id(x.id), isentity(x.isentity)
{
    where (x.where());
}
TsTypeRef::~TsTypeRef() { }
TypeStruct::Type TsTypeRef::type() const { return TypeStruct::typeref; }
void TsTypeRef::references (Ident id) { this->id = id; }
void TsTypeRef::setEntity (bool isEntity) { isentity = isEntity; }
//} /*TsTypeRef*/;

#if 1
//struct TsStdConst : public TypeStruct {
//    enum VType { integer, real, logical, indeterminate };
TsStdConst::TsStdConst() /*: vtype (nullptr)*/ { }
TsStdConst::TsStdConst (TsStdConst &&x) : TypeStruct(move(x)) { }
TsStdConst::~TsStdConst() { }
//TypeStruct::Type TsStdConst::type() const { return TypeStruct::stdconst; }
//} /*TsStdConst*/;
#endif

//struct TsIntegerConst : public TsStdConst {
//    long long value;
TsIntegerConst::TsIntegerConst (long long val) : value (val) { }
//    TsIntegerConst (const TsIntegerConst &x) = delete;
TsIntegerConst::TsIntegerConst (TsIntegerConst &&x)
    : TsStdConst(move(x)), value(x.value)
{ }
TsIntegerConst::~TsIntegerConst() { /*delete vtype;*/ }
TypeStruct::Type TsIntegerConst::type() const { return TypeStruct::stdconst; }
TsStdConst::VType TsIntegerConst::vtype() const { return TsStdConst::integer; }
//} /*TsIntegerConst*/;

//struct TsRealConst : public TsStdConst {
//    long double value;
TsRealConst::TsRealConst (long double val) : value (val) { }
//    TsRealConst (const TsRealConst &x) = delete;
TsRealConst::TsRealConst (TsRealConst &&x)
    : TsStdConst(move(x)), value(x.value)
{ }
TsRealConst::~TsRealConst() { }
TypeStruct::Type TsRealConst::type() const { return TypeStruct::stdconst; }
TsStdConst::VType TsRealConst::vtype() const { return TsStdConst::real; }
//} /*TsRealConst*/;

//struct TsLogicalConst : public TsStdConst {
//    Logical value;
TsLogicalConst::TsLogicalConst (bool val) : value (val) { }
TsLogicalConst::TsLogicalConst (Logical val) : value (val) { }
//    TsStdLogicalConst (const TsStdLogicalConst &x) = delete;
TsLogicalConst::TsLogicalConst (TsLogicalConst &&x)
    : TsStdConst(move(x)), value(x.value)
{ }
TsLogicalConst::~TsLogicalConst() { }
TypeStruct::Type TsLogicalConst::type() const { return TypeStruct::stdconst; }
TsStdConst::VType TsLogicalConst::vtype() const { return TsStdConst::real; }
//} /*TsLogicalConst*/;

//struct TsIndeterminateConst : public TsStdConst {
TsIndeterminateConst::TsIndeterminateConst() { }
//    TsIndeterminateConst (const TsIndeterminateConst &x) = delete;
TsIndeterminateConst::TsIndeterminateConst (TsIndeterminateConst &&x)
    : TsStdConst(move(x))
{ }
TsIndeterminateConst::~TsIndeterminateConst() { }
TypeStruct::Type TsIndeterminateConst::type() const {
    return TypeStruct::stdconst;
}
TsStdConst::VType TsIndeterminateConst::vtype() const {
    return TsStdConst::indeterminate;
}
//} /*TsIndeterminateConst*/;

//struct TsSelf : public TypeStruct {
TsSelf::TsSelf() { }
TsSelf::TsSelf (TsSelf &&x) : TypeStruct(x) { }
TsSelf::~TsSelf() { }
TypeStruct::Type TsSelf::type() const { return TypeStruct::self; }
//    const std::string TsSelf::type_name() { return "TsSelf"; }
//} /*TsSelf*/;

//struct TsStdFunc : public TypeStruct {
//    TypeStruct *resulttype;
//    Fifo<ProcArg> arguments;
//    TsStdFunc() = delete;
TsStdFunc::TsStdFunc (TypeStruct *rt, const Fifo<ProcArg> &args)
    : TypeStruct(), resulttype(rt), arguments(args)
{ }

TsStdFunc::TsStdFunc (TypeStruct *rt, Fifo<ProcArg> &&args)
    : TypeStruct(), resulttype(rt), arguments(args)
{ }

//    TsStdFunc (const TsStdFunc &x) = delete;

TsStdFunc::TsStdFunc (TsStdFunc &&x)
    : TypeStruct(x), resulttype(x.resulttype), arguments(move(x.arguments))
{
    x.resulttype = nullptr;
}

TsStdFunc::~TsStdFunc() {
    if (resulttype) { delete resulttype; }
    while (! arguments.empty()) {
	ProcArg arg = arguments.shift();
	delete arg.arg;
    }
}

TypeStruct::Type TsStdFunc::type() const {
    return resulttype ? TypeStruct::stdfunc : TypeStruct::stdproc;
}
//} /*TsStdFunc*/;
