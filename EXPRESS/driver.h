/*! \file driver.[ch]
**
**  The class which controls the parser and the further evaluation of the
**  parsed EXPRESS specification.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Driver part of the EXPRESS generator.
**
** The driver is the central structure which controls the parser, the symbol
** tables and further passes in the EXPRESS generator. Structures like the
** symbol table are not directly visible outside of code which is controlled
** by it.
**
*/


#ifndef DRIVER_H
#define DRIVER_H

#include <cstddef>
#include <cstdint>
#include <string>
#include <istream>
#include <unordered_set>
#include <unordered_map>

#include "Common/config.h"

#include "opmode.h"
#include "symtab.h"
#include "scanner.h"
#include "express.tab.h"
#include "pass2.h"

namespace EXPRESS {

    /*! The driver module connects the different parts of the "compiler",
    **  namely:
    **    - the parser,
    **    - the symbol table,
    **    - the second pass operations,
    **    - the code generator, and
    **    - the main program.
    **
    */
    class Driver {
    public:
	//! The file or stream name (mainly used for error reporting)
	std::string streamname;

	//! Default constructor ...
	Driver();

	//! This value constructor adds a `Config` object, allowing for
	//  the "compiler" being controlled by a configuration file (which
	//  is loaded somewhere else).
	Driver (const Config &config);

	//! Destructor
	virtual ~Driver();

	/*! Parses a file
	**
	**  @param filename - string filename of a valid input file
	**
	**  @returns `true` on success, `false` on failure.
	*/
	bool parse (const char *filename);

	/*! Parses an input stream.
	**
	**  @param is - std::istream&, valid `C++` input stream
	**
	**  @returns `true` on success, `false` on failure.
	*/
	bool parse (std::istream &is);

	/*! After parsing operation(s)
	**
	**  This function performs further analysis `parse()` could not do
	**  (context analysis, ...)
	**
	**  @param op - the operation mode. Normally, this parameter is more
	**              interesting in the code generator, but there may be
	**              some operations to be performed which depend on the
	**              operation mode.
	**
	**  @returns `true` if all operations performed completed successfully,
	**           and `false` if *any* part of the second pass operations
	**           failed.
	*/
	bool pass2 (OpMode op);

	/*! The "code generator".
	**
	**  @param op - the type of code being generated:
	**    - `OpMode::cstar` dumps any a list of positions of DERIVE
	**      attribute which precede explicit attributes for an ENTITY. This
	**      is done for any entity which is structured this way. Background
	**      for this is the fact that DERIVE attributes are not stored in
	**      the database, which means that reconstructing an ENTITY
	**      instance is at least difficult without this information.
	**   - `OpMode::db` creates a database schema (MySQL/MariaDB) as an
	**      SQL-script for the parsed IFC (EXPRESS) definition.
	**   - `OpMode::cpp` generates the (`C++`) source code in a given
	**     directory.
	**   - `OpMode::dpa`, `OpMode::syms`, `OpMode::enums`, `OpMode::fac`,
	**     `OpMode::eaplist`, `OpMode::t2bt`: These operation modes were
	**     introduced during the (initial) development phase of `xpp`, and
	**     are solely meant for testing/debugging purposes.
	**
	**  @param tmpldir - a directory which contains file templates
	**                   used for the code generation, and source files
	**                   which don't depend on the EXPRESS data, but are
	**                   nonetheless required for generating the plugin
	**                   (e.g., the database interface, ...)
	**
	**  @param outpath - depending on the operation mode either a pathname
	**                   prefix of the file(s) to be generated, or that of
	**                   a directory where the generated code is stored.
	**
	**  @returns `true` on success and `false` on failure (currently
	**           unused).
	*/
	bool cgen (OpMode op,
		   const std::string &tmpldir,
		   const std::string &outpath);

	/*! Testing operations.
	**
	**  This function is used – instead of `cgen()` – for the testing
	**  operation modes.
	**
	**  @param op - the operation mode (see `cgen()` for more).
	**
	**  @param outputfile - the output pathname(-prefix) of the files
	**                      generated during the operation performed.
	*/
	void testing (OpMode op, const std::string &outputfile);

	// Functions which are used by the EXPRESS-parser 'express.y'. Because
	// Yacc(Bison) generated parsers allow only for restricted access to
	// variables outside (without opening these variables to the outside
	// world), i defined these functions which make the symbol table
	// accessible within the parser.

	/*! Gets symbol from the symbol (tag) table. If a symol of the given
	**  does not already exist, it is created.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	**
	**  @param s - the name of the symbol being retrieved/created.
	*/
	Ident get_symbol (const std::string &s);

	/*! Makes a symbol global (if possible).
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	**
	**  @param symid - the symbol being processed.
	*/
	void set_toplevel (Ident symid);

	/*! Returns the level of the current declaration context.
	**
	**  Because of the existance of FUNCTIONs, PROCEDUREs and RULEs, there
	**  are mechanisms for entering new declaration contexts which allow
	**  for shadowing identifiers (giving an identifier locally a new
	**  definition). This means that mechanisms are required for handling
	**  such local contexts.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	**
	**  @returns the level number of the declaration context currently
	**           active
	*/
	uint32_t decllevel ();

	/*! Introduces a new declaration context.
	**
	**  @param `id` the symbol the declaration context is attached to
	**
	**  @param `ts` (a pointer to) the type structure the new declaration
	**         context is attached to (e.g. a FUNCTION, an ENTITY, ...).
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	void new_declcontext (Ident id, TypeStruct *ts);

	/*! Introduces a new declaration context.
	**
	**  Sometimes, a declaration context is required, which isn't attached
	**  to a FUNCTION, etc.. This function is used in these cases.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	void new_declcontext ();

	/*! Getting the identifier (symbol table entry) the current declaration
	**  context is associated with. This identifier can be a FUNCTION, a
	**  PROCEDURE, a RULE, a FOR loop, or something else.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	Ident get_contextid ();

	/*! Getting the identifier (symbol table entry) of the context in the
	**  given  level.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	Ident get_contextid (uint32_t level);

	/*! Geting the type structure of the symbol the current declaration
	**  context is associated with.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	TypeStruct *get_contextdef ();

	/*! Getting the type structure of the symbol the declaration at a
	**  given level is associated with.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	TypeStruct *get_contextdef (uint32_t level);

	/*! Ending a declaration context, removing all its symbol definitions
	**  from the symbol table (but not their names!).
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	void end_declcontext (bool destroy = false);

	/*! Define a new symbol in the current declaration context.
	**
	**  @param s - the name of the sambol to be inserted.
	**
	**  @param ident - The identifier (symbol table entry) of the created
	**                 symbol
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	bool new_symbol (const std::string &s, Ident &symid, TypeStruct *ts,
			 uint32_t &odepth);

	/*! Getting the type structure of a specific identifier.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	TypeStruct * get_symvalue (Ident symid);

	/*! Overlaying the definition of an identifier with a new one.
	**  The previous definition isn't lost. With the reverse operation
	**  (`pop_symvalue()`) the previous definition of an identifier
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	**  can be restored.
	*/
	uint32_t push_symvalue (Ident symid, TypeStruct *ts);

	/*! Restores the previous definition of an identifier. With the
	**  second (optional) argument, the decision can be made if the
	**  removed definition is kept ir destroyed.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	void pop_symvalue (Ident symid, bool destroy_value = false);

	/*! Returns the level of the context the current definition of a
	**  symbol is resides in.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	uint32_t get_symlevel (Ident symid);


	/*! Returns the (visible) name of an identifier.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	const std::string & get_symname (Ident symid);

	/*! Stores the name of an IFC schema (in the symbol table).
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	void schema (const std::string & sname);

	/*! Returns the name of the stored IFC schema.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	const std::string & schema ();

	/*! Creates a string containing a comma-separated list of identifier
	**  names from a list of identifiers. This function is primarily used
	** for generating error messages.
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	std::string get_symnames (IdentList &id_set);

	//std::string get_symnames (IdentSet &id_set);

	/*! Returns the first identifier in a symbol table.
	**
	**  Together with the complementary function `last_ident()`, this
	**  function allows for an iteration through the symbol table
	**  (somewhat like 'begin()' and 'end()' on the elements of a
	**  container class in the standard C++ template library).
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	Ident first_ident ();

	/*! Returns the last identifier in a symbol table.
	**
	**  Together with the complementary function `first_ident()`,
	**  this
	**  function allows for an iteration through the symbol table
	**  (somewhat like 'begin()' and 'end()' on the elements of a
	**  container class in the standard C++ template library).
	**
	**  This function is part of a set of functions which connect the
	**  parser with the symbol table.
	*/
	Ident last_ident ();

	/*! Checking if an identifier is new (without definition).
	**
	**  Sometimes, identifiers are introduced before they have a valid
	**  definition. In this case, a dummy definition is created.
	**  This function returns a tri-state return-value:
	**    -  0  the symbol is indeed new (or was used in a matching context
	**          prior to any definition or has a definition in an outer
	**          context),
	**    - +1  the symbol has yet no definition, but is was used in a
	**          context which doesn't match the requirements of the symbol's
	**          definition,
	**    - -1  the symbol has a valid definition in the current context.
	*/
	int check_newid (Ident id,
			 const std::set<TypeStruct::Type> &valid_types);

	/*! This function checks if a given symbol matches the requirements of
	**  the current context (where it is used), and returns 'true' in this
	**  case, and symbol's the definition through the third argument (if it
	**  has one, otherwise 'nullptr' is returned). If the symbol's
	**  definition doesn't match the requirements, 'false' is returned, and
	**  the third argument remains unchanged.
	*/
	bool check_type (Ident id,
			 const std::set<TypeStruct::Type> &valid_types,
			 TypeStruct * &tsout);

	/*! Initalises the symbol table with the standard names (functions,
	**  procedures, etc.).
	*/
	void init_symtab ();

	/*! Sometimes, the actions of a parser rule require something directly
	**  from the rule which contained its non-terminal. Because Yacc(Bison)
	**  doesn't support such inherited(?) attributes, two variables were
	**  introduced here.
	**  Until a better version was found, these variables take the role
	**  of inherited attributes. They transport a symbol and its definition
	**  between diffenrent actions (within a rule).
	*/
	Ident keep_symid;
	TypeStruct *keep_symvalue;

    private:
	/*! (Re-)Creates a scanner and a parser and then parses an EXPRESS
	**  (IFC) specification given as input stream.
	**
	**  @param istream - the input stream which contains (should contain)
	**                   an EXPRESS (IFC) specification.
	**
	**  @returns `true` if the parsing succeeded, and `false` if errors
	**           were found.
	*/
	bool parse_helper (std::istream &istream);

	/*! Configuration data */
	Config config;

	/*! The symbol table used by all passes of the IFC Plugin generator. */
	Symtab symtab;

	/*! The C++ scanner class. */
	EXPRESS::Scanner *scanner = nullptr;

	/*! The C++ parser class. */
	EXPRESS::Parser *parser = nullptr;

    };

} /*namespace EXPRESS*/

#endif /*DRIVER_H*/
