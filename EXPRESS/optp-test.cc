/*! \file optp-test.cc
**  A small test program for the option parser module `optp.{cc,h}`.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Small test program for 'optp.{cc,h}'
**
*/

#include <iostream>

#include "optp.h"

using namespace std;

static string basename (const string &path)
{
    size_t slpos = path.find_last_of ('/');

    if (slpos == string::npos) { return path; }
    return path.substr (slpos + 1);
}

int main (int argc, char *argv[])
{
    int ix = 0, optx;
    const char *opts;
    OptMap options;
    const string prog = basename (*argv);
    if (argc < 2) {
	cout << "Usage: " << prog << " optstr [arg...]" << endl; return 0;
    }
    opts = argv[1]; optx = 2;
    try {
	options = getopts (argc, argv, opts, optx);
    } catch (runtime_error e) {
	cerr << prog << ": Option parsing failed - " << e.what() << endl;
	return 64;
    }
    for (auto &ovpair: options) {
	string opts (1, ovpair.first);
	string optval = ovpair.second;
	cout << ++ix << ". Found option '-" << opts << "'";
	if (optval.size() > 0) {
	    cout << " with the value \"" << optval << "\"";
	} else {
	    cout << " without a value";
	}
	cout << endl;
    }
    if (optx < argc) {
	cout << "Remaining (non-option) arguments:" << endl;
	for (ix = optx; ix < argc; ++ix) {
	    cout << (ix - optx + 1) << ". \"" << argv[ix] << "\"" << endl;
	}
    }
    return 0;
}
