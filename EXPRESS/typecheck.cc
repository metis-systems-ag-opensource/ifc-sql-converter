/* typecheck.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Typechecking of expressions
**
*/

#include <utility>
#include <stdexcept>
#include <string>
#include <set>

#include <Common/fifo.h>

//#include "expr.h"
//#include "stmt.h"
//#include "types.h"
#include "symtab.h"
#include "setops.h"

using std::move;
using std::runtime_error;
using std::string;
using std::set;

using TSBase = Ident;
using TypeSet = set<TSBase>;

static TypeStruct *nametype (Symtab &s, const string &name)
{
    Ident id = s[name];
    if (id == 0 || id > s.size() { return nullptr; }
    TypeStruct *ts = s[id];
    return ts;
}

static const string &ts_name (const TypeStruct *ts)
{
    TypeStruct::type tstype = ts->type();
    switch (tstype) {
	case TypeStruct::boolean: return "BOOLEAN";
	case TypeStruct::logical: return "LOGICAL";
	case TypeStruct::integer: return "INTEGER";
	case TypeStruct::real: return "REAL";
	case TypeStruct::number: return "NUMBER";
	case TypeStruct::binary: return "BINARY";
	case TypeStruct::string: return "STRING";
	

static bool insertTypeSet (Symtab &s, TypeSet &ts, const string &name)
{
    TypeStruct *ts = nametype (

using ExprType = Expr::Type;


bool check_expr (Symtab &s, Expr *e, TypeSet &outtypes)
{
    Expr::Type etype = e->type();
    switch (etype) {
	case Expr::invalid: {
	    outtypes.insert (/*
	}
