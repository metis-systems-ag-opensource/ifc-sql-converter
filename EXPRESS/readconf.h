/*! \file readconf.h
**  The application-dependent part of the configuration parser
**  (module `Common/config.{cc,h}`).
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'readconf.cc'
** (Read and check the configuration)
**
*/
#ifndef READCONF_H
#define READCONF_H

#include <string>

#include "Common/config.h"

/*! Parses a configuration (using `Common/config.{cc,h}`) and performs
**  additional checks on the configuration read (mandatory configuration
**  items, probably value checks, and so on).
**
**  @param cfgfile - the name (*not* pathname) of a configuration.
**
**  @returns a `Config`-object containing the configuration read from
**           `cfgfile`.
*/
Config readconf (const std::string &cfgfile);

#endif /*READCONF_H*/
