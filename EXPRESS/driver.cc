/* driver.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Implementation part of 'driver.h'
** (Driver part of the EXPRESS generator.
**
**  The driver is the central structure which controls the parser, the symbol
**  tables and further passes in the EXPRESS generator. Structures like the
**  symbol table are not directly visible outside of code which is controlled
**  by it.)
**
*/

#include <iostream>
using std::cerr, std::endl;

#include <cctype>
#include <fstream>
#include <cassert>
#include <utility>
#include <stdexcept>

#include "Common/config.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"

#include "driver.h"

#include "inittab.h"

#include "CodeGen/sel.h"

using namespace std;

namespace EXPRESS {

    Driver::Driver ()
	: streamname("<input stream>")
    { }

    Driver::Driver (const Config &config)
	: streamname("<input stream>"), config(config)
    { }

    Driver::~Driver()
    {
	delete scanner;
	scanner = nullptr;
	delete parser;
	parser = nullptr;
    }

    bool Driver::parse (const char *const filename)
    {
	assert (filename != nullptr);
	ifstream in_file (filename);
	if (! in_file.good ()) {
	    //ERROR
	    exit (1);
	}
	streamname = filename;
	return parse_helper (in_file);
    }

    bool Driver::parse (istream &stream)
    {
	if (! stream.good() && stream.eof()) {
	    return false;
	}
	return parse_helper (stream);
    }

    bool Driver::pass2 (OpMode op)
    {
	return do_pass2 (op, symtab);
    }

    bool Driver::cgen (OpMode op,
		       const string &tmpldir,
		       const string &outpath)
    {
	return CodeGen::cgen (op, symtab, tmpldir, outpath);
    }

    bool Driver::parse_helper (istream &stream)
    {
	delete scanner;
	try {
	    scanner = new Scanner (&stream);
	} catch (bad_alloc &ba) {
	    cerr << "Failed to allocate scanner: " << ba.what() <<
		endl << "Exiting!" << endl;
	    exit (1);
	}

	delete parser;
	try {
	    parser = new Parser ((*scanner), (*this));
	} catch (bad_alloc &ba) {
	    cerr << "Failed to allocate parser: " << ba.what() <<
		endl << "Exiting!" << endl;
	    exit (1);
	}

	const int accept(0);
	if (parser->parse() != accept) {
	    cerr << "Parsing failed!" << endl; return false;
	}

	return true;
    }

    Ident Driver::get_symbol (const string &s) {
	return symtab.get_symbol (s);
    }

    void Driver::set_toplevel (Ident symid) {
	symtab.set_toplevel (symid);
    }

    uint32_t Driver::decllevel () { return symtab.level (); }

    void Driver::new_declcontext (Ident id, TypeStruct *def) {
	symtab.new_context (id, def);
    }

    Ident Driver::get_contextid () { return symtab.contextid(); }

    Ident Driver::get_contextid (uint32_t level) {
	return symtab.contextid (level);
    }

    TypeStruct *Driver::get_contextdef () { return symtab.contextdef(); }

    TypeStruct *Driver::get_contextdef (uint32_t level) {
	return symtab.contextdef (level);
    }

    void Driver::end_declcontext (bool destroy) {
	symtab.end_context (destroy);
    }

    bool Driver::new_symbol (const string &s, Ident &symid, TypeStruct *ts,
			     uint32_t &odepth)
    {
	return symtab.newsymbol (s, symid, ts, odepth);
    }

    TypeStruct * Driver::get_symvalue (Ident symid) { return symtab[symid]; }

    uint32_t Driver::push_symvalue (Ident symid, TypeStruct *ts) {
	return symtab.push_value (symid, ts);					    }

    void Driver::pop_symvalue (Ident symid, bool destroy_value) {
	symtab.pop_value (symid, destroy_value);
    }

    uint32_t Driver::get_symlevel (Ident symid) {
	return symtab.symlevel (symid);
    }

    const string & Driver::get_symname (Ident symid) {
	return symtab.symname (symid);
    }

    void Driver::schema (const string & sname) { symtab.schema (sname); }

    const string & Driver::schema() { return symtab.schema (); }

    string Driver::get_symnames (IdentList &id_list) {
	string res;
	size_t reslen;
	bool first = true;
	for (Ident id: id_list) {
	    if (first) { first = false; } else { reslen += 2; }
	    reslen += get_symname (id).length();
	}
	res.reserve (reslen + 1);
	first = false;
	for (Ident id: id_list) {
	    if (first) { first = false; } else { res += ", "; }
	    res += get_symname (id);
	}
	return res;
    }

    Ident Driver::first_ident () { return symtab.first(); }

    Ident Driver::last_ident () { return symtab.last(); }

    int Driver::check_newid (Ident id,
			     const set<TypeStruct::Type> &valid_types)
    {
	// Get the symbol's definition
	TypeStruct *ts = symtab[id];
	// If the symbol has (yet) no definition, it is definitvely NEW, so
	// so all is OK!
	if (!ts) { return 0; }
	// Check if the (already defined) symbol is on the current declaration
	// level.
	uint32_t currlevel = symtab.level(), symlevel = symtab.symlevel (id);
	// If this is not the case (the symbol's definition comes from an
	// outer environment), then the symbol is assumed as NEW, because in
	// the current environment, there is no definition for it.
	if (currlevel > symlevel) { return 0; }
	// Next step: get the type of the symbol's defintion.
	TypeStruct::Type objType = ts->type();
	if (objType == TypeStruct::unset) {
	    // The symbol's definition is that of a pre-used symbol (used in a
	    // specific context before it was defined). In this case, the
	    // expected type of the symbol decides about the validity of the
	    // symbol.
	    auto uts = dynamic_cast<TsUnset *>(ts);
	    objType = uts->expected_type();
	    // If the symbol's expected type is unset or in the given set of
	    // valid symbols, its definition is removed and then '0'
	    // (indicating that it is indeed NEW) is returned.
	    if (objType == TypeStruct::unset
	    ||  valid_types.count (objType) > 0) {
		symtab.pop_value (id, true); return 0;
	    }
	    // The symbol is NEW, but the context in which it was used doesn't
	    // match the requirements. In this case, '+1' is returned.
	    return +1;
	}
	// In any other case, the result is '-1', because the symbol already
	// has a valid definition in the current context.
	return -1;
    }

    bool Driver::check_type (Ident id,
			     const set<TypeStruct::Type> &valid_types,
			     TypeStruct * &tsout)
    {
	TypeStruct *ts = symtab[id];
	if (!ts) {
	    // Assume an id without definition as always valid. This id's
	    // definition then must be filled in later (during the parsing
	    // process) ...
	    tsout = ts; return true;
	} else {
	    // An id-definition of type 'TsUnset' means that the id was
	    // somewhere used, but its real type was now known at this point.
	    // In this case, the "expected" type of the object is used instead
	    // of the object's type.
	    TypeStruct::Type objType = ts->type();
	    if (objType == TypeStruct::unset) {
		auto uts = dynamic_cast<TsUnset *>(ts);
		objType = uts->expected_type();
	    }
	    uint32_t symlevel = symtab.symlevel (id),
		     decllevel = symtab.level();
	    if (symlevel == decllevel && objType == TypeStruct::unset) {
		tsout = ts; return true;
	    }
	    if (valid_types.count (objType) > 0) {
		tsout = ts; return true;
	    }
	}
	return false;
    }

    void Driver::init_symtab () { EXPRESS::init_symtab (symtab); }

} /*namespace EXPRESS*/
