/* defcheck.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Fast validator with the sole purpose of testing if all data-structures used
** in the EXPRESS-file were defined somewhere. This exported function returns
** a boolean flag indicating success (true) or failure (false) and writes the
** required error messages directly to 'std::cerr'
**
*/

#include <cstring>
#include <iostream>
#include <string>

#include "defcheck.h"

using namespace std;

// Check the names locally defined/used in a procedure/function/rule ...
static bool defcheck_algorithm (Symtab &st, Ident id, TypeStruct *ts);

// Test all entries in the symbol table for definedness. Issue an error message
// for each undefined entry found. If there is any entry which lacks a proper
// definition, the return value is 'false'. Otherwise it is 'true' (to indicate
// success).
bool defcheck (Symtab &st)
{
    bool done = true;
    for (Ident id = st.first(); id <= st.last(); ++id) {
	const string &name = st.symname (id);
	TypeStruct *ts = st[id];
	// There may be entries which were created temporarily. These entries
	// should be skipped ...
	if (! ts) { continue; }
	TypeStruct::Type tstype = ts->type();
	if (dynamic_cast<TsUnset *>(ts)) {
	    cerr << ts->where() << ": Undefined symbol table entry '" <<
		    name << "' found.";
	    if (tstype != TypeStruct::unset
	    &&  tstype != TypeStruct::unknown_type) {
		auto *uts = dynamic_cast<TsUnset *>(ts);
		string expected = to_string (uts->expected_type());
		string nopt =
		    string("aeiou").find (expected[0]) == string::npos ?
			"n"
		    :   "";
		cerr << "Expected was a" << nopt << " '" << expected << "'" <<
			endl;
		done = false;
		continue;
	    }
	}
	switch (tstype) {
	    case TypeStruct::procedure:
	    case TypeStruct::function:
	    case TypeStruct::rule: {
		if (! defcheck_algorithm (st, id, ts)) { done = false; }
		break;
	    }
	    default: break;
	}
    }
    return done;
}

// Test all local definitions in an algorithm (PROCEDURE/FUNCTION/RULE) for
// definedness. Issue an error message for each undefined entry found. If
// any entry lacks a proper definition, 'false' is returned. Otherwise, this
// function returns 'true' (to indicate success).
// Because algorithms may contain other algorithms, this function is recursive,
// meaning: it invokes itself for each locally defined algorithm found.
static bool defcheck_algorithm (Symtab &st, Ident id, TypeStruct *ts)
{
    bool done = true;
    auto ats = dynamic_cast<TsAlgorithm *>(ts);
    //if (! ats) { return true; } /* Maybe i should issue an error here */
    IdentKeyMap<TypeStruct *> &localdefs = ats->localdefs;
    for (auto &localdef: localdefs) {
	Ident localid = localdef.first;
	const string &name = st.symname (localid);
	TypeStruct *localts = localdef.second;
	// localts can nevver be empty (aka 'nullptr')!
	TypeStruct::Type tstype = localts->type();
	if (dynamic_cast<TsUnset *>(localts)) {
	    cerr << localts->where() << ": Undefined local symbol '" <<
		    name << "' in " << st.symname (id) << " found.";
	    if (tstype != TypeStruct::unset
	    &&  tstype != TypeStruct::unknown_type) {
		auto uts = dynamic_cast<TsUnset *>(localts);
		string expected = to_string (uts->expected_type());
		string nopt =
		    string("aeiou").find (expected[0]) == string::npos ?
			"n"
		    :   "";
		cerr << "Expected was a" << nopt << " '" << expected << "'" <<
			endl;
		done = false;
		continue;
	    }
	}
	switch (tstype) {
	    case TypeStruct::procedure:
	    case TypeStruct::function:
	    case TypeStruct::rule: {
		// Recursive invocation
		if (! defcheck_algorithm (st, localid, localts)) {
		    done = false;
		}
		break;
	    }
	    default: break;
	}
    }
    return done;
}
