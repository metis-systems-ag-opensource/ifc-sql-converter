/*! \file identbitset.h
**  A instantiation of the template `BitSet<T,Minval>` for `Ident` values.
**
**  This type is used for a fast identifier detection in some of the finctions
**  of the `TypeStruct`-derived types.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** A specialized bit-set for identifiers. This class is intended to be used
** in implementation files only (for temporarily used values, such as local
** variables of functions), so it is not relevant that it sometimes may require
** some hundreds of bytes of memory.
**
*/
#ifndef IDENTBITSET_H
#define IDENTBITSET_H

#include "Common/mintypes.h"
#include "Common/fifo.h"

#include "Common/bitset.h"

/* Identifiers always start with `1`; the `0` is used as the "null" symbol in
** the symbol table, so it can never occur in a `BitSet` concerning `Ident`
** values.
*/
using IdentBitSet = BitSet<Ident, 1>;

#endif /*IDENTBITSET_H*/
