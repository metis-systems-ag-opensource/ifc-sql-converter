/*! \file myassert.h
**  An `assert()` function/macro which is used instead of the corresponding
**  version from the C library.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** My own version of an 'assertion' function
**
*/
#ifndef MYASSERT_H
#define MYASSERT_H

#include <string>

/*! This function is not used directly, but through the following macro
**  definition.
*/
void _myassert (const char *file, int line, bool bf, const std::string &msg);

/*! This macro is used instead of `assert()`. It issues a `std::runtime_error`
**  exception, containing the filename, the line number, and a given message.
*/
#define myassert(bf, msg) (_myassert (__FILE__, __LINE__, (bf), (msg)))

#endif /*MYASSERT_H*/
