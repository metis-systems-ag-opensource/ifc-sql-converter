/*! \file inittab.h
**  Function for initialising the symbol table with the standard names (types,
**  constants, functions, procedures, ...).
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Interface file for 'inittab.cc'
** (Initialize the symbol table with some standard values ...)
**
*/
#ifndef INITTAB_H
#define INITTAB_H

#include "symtab.h"

namespace EXPRESS {

/*! Initialises the given symbol table with all pre-defines names. */
void init_symtab (Symtab &symtab);

} /*namespace EXPRESS*/

#endif /*INITTAB_H*/
