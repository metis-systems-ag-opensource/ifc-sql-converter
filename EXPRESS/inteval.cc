/* inteval.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Evaluate (INTEGER) constant expressions
**
*/

// I'm _no_ _longer_ using exceptions here, because sometimes (like in the
// case of this function) it makes more sense to return an optional value.
// (I simply don't want any exception being generated in the parser pass,
// because i have the 'yyerror()'/'YYERROR' pair there which is more
// appropriate for a (Yacc/Bison) generated parser and an exception must be
// caught in a 'try'/'catch' statement which seems inappropriate there, too.

#include <string>

#include "inteval.h"

using std::runtime_error, std::optional, std::nullopt;

static optional<std::string> stringEval (Expr *x);

optional<long> intEval (Expr *x)
{
    if (x) {
	switch (x->type()) {
	    case Expr::int_literal: {
		long v = dynamic_cast<IntLiteral *>(x)->value;
		return optional<long>(v);
	    }
	    case Expr::unary: {
		auto x1 = dynamic_cast<UnaryExpr *>(x);
		MonOp op = x1->op;
		if (op == MonOp::plus || op == MonOp::minus) {
		    optional<long> subx = intEval (x1->right);
		    if (subx) { return op == MonOp::minus ? -*subx : *subx; }
		}
		break;
	    }
	    case Expr::binary: {
		auto x1 = dynamic_cast<BinaryExpr *>(x);
		BinOp op = x1->op;
		optional<long> l = intEval (x1->left), r = intEval (x1->right);
		if (l && r) {
		    switch (op) {
			case BinOp::add:  return *l + *r;
			case BinOp::sub:  return *l - *r;
			case BinOp::mul:  return *l * *r;
			case BinOp::idiv: return *l / *r;
			case BinOp::mod:  return *l % *r;
			default:
			    return nullopt;
		    }
		}
	    }
	    case Expr::std_invoc: {
		auto x1 = dynamic_cast<StdInvoc *>(x);
		Expr *arg = x1->arguments.first();
		StdFunc sfn = x1->ftag;
		switch (sfn) {
		    case StdFunc::abs: {
			optional<long> xv = intEval (arg);
			if (xv) { return *xv < 0 ? -*xv : *xv; }
			break;
		    }
		    case StdFunc::length: {
			optional<std::string> xv = stringEval (arg);
			if (xv) { return xv->size(); }
			break;
		    }
		    default: {
			break;
		    }
		}
	    }
	    default: {
		break;
	    }
	}
    }
    // Return 'nullopt' (no valid expression) in all cases not handled above.
    // An 'error'-like value would be better here (returning an error message
    // with the reason for the evaluation failure), but this is nothing C++
    // has included yet, and i haven't enough time for implementing such a
    // type.
    return nullopt;
}

// This function is used internally only. It performs simple string constant
// expression evaluations (into 'std::string').
static optional<std::string> stringEval (Expr *x)
{
    if (x) {
	switch (x->type()) {
	    case Expr::string_literal: {
		std::string v = dynamic_cast<StringLiteral *>(x)->value;
		return optional<std::string>(v);
	    }
	    case Expr::binary: {
		auto x1 = dynamic_cast<BinaryExpr *>(x);
		BinOp op = x1->op;
		optional<std::string> l = stringEval (x1->left),
				      r = stringEval (x1->right);
		if (op == BinOp::add) {
		    if (l && r) { return *l + *r; }
		}
		break;
	    }
	    default: {
		break;
	    }
	}
    }
    // Same here as for 'intEval()'.
    return nullopt;
}
