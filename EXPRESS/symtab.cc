/* symtab.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2018, Metis AG
** License: MIT License
** 
**
** Symbol table for EXPRESS (Implementation part)
**
*/
#include <iostream>	// delete if no longer required

#include "symtab.h"

#include <map>
#include <string>
#include <cstring>
#include <cstdint>
#include <functional>
#include <stdexcept>

#include "types.h"

using namespace std;

// class Symtab {

// Default constructor
Symtab::Symtab()
    : symbols ()
{
    depth = 0; //symbols.clear ();
}

// Constructor and assignment opertaor with 'move semantics'
Symtab::Symtab(Symtab &&x)
    : symbols (x.symbols)
{
    depth = x.depth;
//    symidmap = x.symidmap;
//    symvaltab = x.symvaltab;
//    //this->symbols = x.symbols;
}

Symtab & Symtab::operator= (Symtab &&x)
{
    depth = x.depth;
    symbols = x.symbols;
//    symidmap = x.symidmap;
//    symvaltab = x.symvaltab;
//    //this->symbols = x.symbols;
    return *this;
}

// The destrutor must destroy a symbol table completely ...
Symtab::~Symtab()
{
    for (Ident ix = 1; ix <= symbols.size (); ++ix) {
	TSStack *its = symbols[ix];
	// Need this, because - in order to block a function/procedure being
	// redefined within itself - i duplicate its definition within itself.
	TypeStruct *prevts = nullptr;
	while (its) {
	    TSStack *next = its->next; its->next = nullptr;
	    if (its->ts != prevts) {
		delete its->ts; }
	    prevts = its->ts; delete its;
	    its = next;
	}
    }
}

const string & Symtab::schema ()
{
    return schema_name;
}

void Symtab::schema (const string &schema_name)
{
    this->schema_name = schema_name;
}

// Get the current depth (level)
uint32_t Symtab::level ()
{
    return depth;
}

// Begin a new declaration environment
void Symtab::new_context (Ident id, TypeStruct *def)
{
    ++depth; context.unshift (make_pair(id, def));
}

void Symtab::new_context ()
{
    ++depth;
}

// Get the current function/procedure-id and its definition from the
// context stack.
Ident Symtab::contextid () {
    return context.empty() ? 0 : context.first().first;
}

Ident Symtab::contextid (uint32_t level)
{
    return context.empty() || level == 0 ? 0 : context[depth - level].first;
}

TypeStruct *Symtab::contextdef () {
    return context.empty() ? nullptr : context.first().second;
}

TypeStruct *Symtab::contextdef (uint32_t level) {
    return context.empty() || level == 0 ?
	       nullptr
	   :   context[depth - level].second;
}

// End the current declaration environment
void Symtab::end_context (bool destroy)
{
    for (Ident ix = symbols.first(); ix <= symbols.last(); ++ix) {
	auto its = symbols[ix];
	while (its && its->depth >= depth) {
	    auto nxt = its->next; its->next = nullptr;
	    if (destroy) { delete its->ts; }
	    delete its;
	    its = nxt;
	}
	symbols[ix] = its;
    }
    if (depth > 0) { --depth; }
    if (! context.empty()) { (void) context.shift(); }
}

// Insert a new symbol into the symbol table. Returns 'true' if a new entry
// could be created and 'false' if a symbol with the same name already
// exists ...
bool Symtab::newsymbol (const string &s, Ident &symid, TypeStruct *ts,
			uint32_t &odepth)
{
    Ident nx;
    TSStack *its = nullptr;
    if (symbols.has (s)) {
	nx = symbols[s];
	its = symbols[nx];
	if (its && its->depth >= depth) {
	    return false;
	}
    } else {
	nx = symbols[s];
    }
    TSStack *newel = new TSStack;
    symid = nx;
    symbols[nx] = newel;
    newel->next = its;
    newel->ts = ts;
    ts->backref (nx);
    odepth = newel->depth = depth;
    newel->checked = false;
    return true;
}

void Symtab::set_toplevel (Ident symid)
{
    if (symid < 1 || symid > symbols.size()) {
	throw runtime_error ("Invalid argument of 'Symtab::set_toplevel()'");
    }
    TSStack *tsit = symbols[symid];
    if (! tsit) {
	throw runtime_error ("Can't set the top level for an undefined symbol");
    }
    if (tsit->depth > 0) {
	if (tsit->next) {
	    throw runtime_error (
		"Can't set the top level for a symbol with outer level"
		" definitions."
	    );
	}
	tsit->depth = 0;
    }
}

// Search for a name in the symbol table.
// On success, the symbol's 'Ident'-value (a simple integer) is returned.
// If the symbol was not yet in the symbol table, it is created (with an empty
// type structure). 
Ident Symtab::get_symbol (const string &s)
{
    bool already_inserted = symbols.has (s);
    Ident nx = symbols[s];
    if (! already_inserted) { symbols[nx] = nullptr; }
    return nx;
}


// Replace the entry with the name in 's' with a new entry (with the name
// in 'ns') in the symbol table. Returns 'true' if this replacement was
// successful and 'false', otherwise.
bool Symtab::replace (const string &s, const string &ns, TypeStruct *ts)
{
    return false;
}

TypeStruct * Symtab::operator[] (Ident sid) {
    TSStack *tss = symbols[sid];
    return (tss ? tss->ts : nullptr);
}

Ident Symtab::operator[] (const string &name)
{
    return symbols.has (name) ? symbols[name] : 0;
}

// Give a symbol 'id' a new semantic.
uint32_t Symtab::push_value (Ident sid, TypeStruct *ts) {
    TSStack *ots = symbols[sid];
    ts->backref (sid);
    TSStack *nts = new TSStack;
    nts->next = ots;
    nts->ts = ts;
    nts->depth = depth;
    nts->checked = false;
    symbols[sid] = nts;
    return nts->depth;
}

// Remove the current semantic from the given symbol 'id'.
void Symtab::pop_value (Ident sid, bool destroy_entry) {
    TSStack *ttss = symbols[sid];
    if (! ttss) {
	// Can't remove a symbol definition if the symbol doesn't have one.
	throw runtime_error ("Symbol has no definition which can be removed");
    }
//    if (ttss->depth == 0 && ttss->ts->is_defined()) {
//	// Can't remove global symbols (except when they aren't marked as
//	// "defined" yet).
//	throw runtime_error ("Attempt to remove a global symbol definition");
//    }
    symbols[sid] = ttss->next; ttss->next = nullptr;
    if (destroy_entry) { delete ttss->ts; }
    delete ttss;
}

// Get the symbol's current (declaration) level ...
uint32_t Symtab::symlevel (Ident sid) {
    TSStack *tss = symbols[sid];
    return (tss ? tss->depth : 0);
}

// Return a symbol's name (as a constant (immutable) string).
const string & Symtab::symname (Ident sid) {
    static const string nullid = "(null)"s;
    const string &res = (sid == 0 ? nullid : symbols.symname (sid));
    return res;
}
const string *Symtab::psymname (Ident sid) {
    const string *res = symbols.psymname (sid);
    return res;
}

// Set the checked-flag of a symbol's definition ...
void Symtab::checked (Ident id, bool ok) {
    Symtab::TSStack *sTop = symbols[id];
    if (sTop) { sTop->checked = ok; }
}

// Get the checked-flag of a symbol's definition ...
bool Symtab::checked (Ident id) {
    Symtab::TSStack *sTop = symbols[id];
    return sTop ? sTop->checked : false;
}

// Quasi-iterator, consisting of the member-functions 'first()' and 'last()'.
Ident Symtab::first () { return symbols.first(); }

Ident Symtab::last () { return symbols.last(); }
// } /*Symtab*/;
