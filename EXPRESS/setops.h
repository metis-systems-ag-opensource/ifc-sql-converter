/*! \file setops.h
**  A (currently unused) module for making operations on the template type
**  `std::set<T>` a bit more clear – by defining elementary set operations
**  on instances of `set<T>`. This module is intended to be used in the
**  type checker module `typecheck.{cc,h}`.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Defining the elementary operations on sets:
**   +	    union
**   *	    intersection
**   /	    difference
**   -	    symetrical diffenrence
**   +	    union with a single element
**   *	    intersection with a single element
**   <=	    subset
**   
*/
#ifndef SETOPS_H
#define SETOPS_H

#include <utility>
#include <set>

namespace std {

template<class T> set<T> operator+ (const set<T> &s1, const set<T> &s2)
{
    set<T> res;
    for (auto it = s1.begin(); it != s1.end(); ++it) { res.insert (*it); }
    for (auto it = s2.begin(); it != s2.end(); ++it) { res.insert (*it); }
    return move(res);
}

template<class T> set<T> operator+ (const set<T> &s, const T &e)
{
    set<T> res;
    for (auto it = s.begin(); it != s.end(); ++it) { res.insert (*it); }
    res.insert (e);
    return res;
}

template<class T> set<T> operator+ (const T &e, const set<T> &s)
{
    return s + e;
}

template<class T> set<T> &operator+= (set<T> &o, const set<T> &i)
{
    for (auto it = i.begin(); it != i.end(); ++it) { o.insert (*it); }
    return o;
}

template<class T> set<T> &operator+= (set<T> &o, const T &e)
{
    o.insert (e);
    return o;
}

template<class T> set<T> &operator/= (set<T> &o, const T &e)
{
    o.erase (e);
    return o;
}

template<class T> set<T> &operator/= (set<T> &o, const set<T> &i)
{
    for (auto it = i.begin(); it != it.end(); ++it) { o.erase (*it); }
    return o;
}

template<class T> set<T> &operator-= (set<T> &o, const set<T> &i)
{
    set<T> u = o * i;
    o += i; o /= u;
    return o;
}

template<class T> set<T> &operator-= (set<T> &o, const T &i)
{
    return o /= i;
}

template<class T> set<T> operator* (const set<T> &s1, const set<T> &s2)
{
    set<T> res;
    const set<T> &x = (s1.size() < s2.size() ? s1 : s2);
    const set<T> &y = (s1.size() >= s2.size() ? s1 : s2);
    for (auto it = x.begin(); it != x.end(); ++it) {
	if (y.count (*it) > 0) { res.insert (*it); }
    }
    return move(res);
}

template<class T> bool operator* (const set<T> &s, const T &e)
{
    return s.count (e) > 0;
}

template<class T> bool operator* (const T &e, const set<T> &s)
{
    return s.count (e) > 0;
}

template<class T> bool operator <= (const set<T> &p, const set<T> &s)
{
    for (auto it = p.begin(); it != p.end(); ++it) {
	if (s.count (*it) == 0) { return false; }
    }
    return true;
}

template<class T> bool empty (const set<T> &s)
{
    return s.size() == 0;
}

} /*namespace std*/

#endif /*SETOPS_H*/
