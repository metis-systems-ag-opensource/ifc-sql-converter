/*! \file flatten.h
**
**  Flattening is here called the process of embedding
**    - all attributes of all SUPERTYPEs of an ENTITY into this ENTITY,
**    - all elements of a SELECT type which are themselves SELECT types into
**      this type.
**
**  This "flattening" process is mandatory for the context analysis as well as
**  for the code generation, because 1 one-to-one translation into *C++* is
**  impossible (and also inapprotriate) for some reasons, and the database
**  model used for representing ENTITY instances requires all attributes of
**  each instance expanded.
**  The "flattened" attribute/SELECT lists are stored in extra member variables
**  of the corresponding `TypeStruct`-derived objects.
*/
/*
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Interface file for 'flatten.cc'
** (Flatten ENTITY-types by inserting the attributes of all of its supertypes
** and SELECT-types by expanding embedded SELECT-types.
**
** Flattening is a rather complex operation:
**
** a) For an ENTITY-type it means that the attributes of all of its supertypes
**    must be integrated, with the exception of attributes which were directly
**    imported from one of its supertypes. Additionally, the WHERE-clauses of
**    its supertypes must be integrated, with directly imported attributes
**    replacing the names of the attributes of the corresponding supertype.
**
** b) For SELECT-types, this means that the components of each component of the
**    type which itself is a SELECT-type, must be integrated. This must be done
**    recursively until the components of the SELECT-type no longer contain a
**    SELECT-type. Additionally, any (SELECT-type's) component's WHERE-clause
**    must be integrated into the SELECT-type's WHERE-clause, too.
**
** ...)
**
*/
#ifndef FLATTEN_H
#define FLATTEN_H

#include "symtab.h"

/*! `flatten(Symtab &)` walks through a complete symbol table and flattens each
**  ENTITY, SELECT type and local variable of an algorithm (FUNCTION, RULE).
*/
bool flatten (Symtab &st);

/*! An EXPRESS specification does not always specify all SUPER-/SUBTYPE
**  relations explicitly, which makes it necessary to complete these relations
**  after all ENTITYs are parsed (and stored). This is the sole purpose of
**  `complete_supersubtypes()`.
*/
bool complete_supersubtypes (Symtab &st);

#endif /*FLATTEN_H*/
