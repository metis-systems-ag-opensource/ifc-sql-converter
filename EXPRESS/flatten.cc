/* flatten.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2019, Metis AG
** License: MIT License
** 
**
** Flatten ENTITY-types by inserting the attributes of all of its supertypes
** and SELECT-types by expanding embedded SELECT-types. The resulting lists
** of "flattened" attributes/types are stored in the corresponding type
** structures, which were extended by two member variables, a boolean flag for
** marking the type structure as "flattened" and the resulting list of
** attributes/types. The list-typed member-variable of 'TsEntity' isn't handled
** by the particular object's destructor, because it shares (doesn't own) the
** refenced 'Attr'-elements. This should avoid problems with destructing these
** objects (and i don't need the pseudo-pointer template class
** 'shared_ptr<T>').
**
** Flattening is a rather complex operation:
**
** a) For an ENTITY-type it means that the attributes of all of its supertypes
**    must be integrated, with the exception of attributes which were directly
**    imported from one of its supertypes. Additionally, the WHERE-clauses of
**    its supertypes must be integrated, with directly imported attributes
**    replacing the names of the attributes of the corresponding supertype.
**
** b) For SELECT-types, this means that the components of each component of the
**    type which itself is a SELECT-type, must be integrated. This must be done
**    recursively until the components of the SELECT-type no longer contain a
**    SELECT-type. Additionally, any (SELECT-type's) component's WHERE-clause
**    must be integrated into the SELECT-type's WHERE-clause, too.
**    Additionally, the type-lists of SELECT-type extensions must be combined,
**    too
**    
** ...
**
*/

//##DEBUG
//#include <iostream>
//using std::cerr, std::endl, std::flush;
//##

#include "Common/dump.h"

#include <iostream>
#include <functional>
#include <stdexcept>
#include <utility>

#include "types.h"
#include "identbitset.h"
#include "util.h"

#include "flatten.h"

using namespace std;

static bool flatten_entry (Symtab &st, Ident id, TypeStruct *ts);
static bool flatten_locals (Symtab &st, Ident id, TsAlgorithm *ts);

bool flatten (Symtab &st)
{
    bool done = true;
    for (Ident id = st.first(); id <= st.last(); ++id) {
	TypeStruct *ts = st[id];
	if (! ts) { continue; }
	if (! flatten_entry (st, id, ts)) {
	    done = false;
	} else {
	    auto ats = dynamic_cast<TsAlgorithm *>(ts);
	    if (ats) {
		if (! flatten_locals (st, id, ats)) {
		    done = false;
		}
	    }
	}
    }
    return done;
}

static bool flatten_locals (Symtab &st, Ident id, TsAlgorithm *ts)
{
    bool done = true;
    IdentKeyMap<TypeStruct *> &localdefs = ts->localdefs;

    enter (st, id, ts);
    for (auto &localdef: localdefs) {
	Ident localid = localdef.first;
	TypeStruct *localts = localdef.second;
	// localts can't be empty (aka 'nullptr')
	if (! flatten_entry (st, localid, localts)) {
	    done = false;
	} else {
	    auto ats = dynamic_cast<TsAlgorithm *>(localts);
	    if (ats) {
		if (! flatten_locals (st, localid, ats)) {
		    done = false;
		}
	    }
	}
    }
    leave (st);

    return done;
}

static bool flatten_entity (Symtab &st, Ident eid, TsEntity *ets);
static bool flatten_select (Symtab &st, Ident id, TsSelect *ts);

static bool flatten_entry (Symtab &st, Ident id, TypeStruct *ts)
{
    switch (ts->type()) {
	case TypeStruct::entity: {
	    auto ets = dynamic_cast<TsEntity *>(ts);
	    return flatten_entity (st, id, ets);
	}
	case TypeStruct::select: {
	    auto stts = dynamic_cast<TsSelect *>(ts);
	    return flatten_select (st, id, stts);
	}
	default: {
	    // Nothing to be done here, but all OK
	    return true;
	}
    }
}

// Add the given list of ENTITY-Ids to the list (set) of collected subtypes
// (attribute 'coll_subtypes') of an ENTITY.
//
static void collect_subtypes (const IdentList &stl, TsEntity *ets)
{
    IdentBitSet subs;
    for (auto ststid: ets->coll_subtypes) { subs.insert (ststid); }
    for (auto id: stl) {
	if (! subs.has (id)) { ets->coll_subtypes.push (id); }
    }
}

// Check if a given TypeStruct describes a scalar type.
static bool is_a_scalar (Symtab &st, const TypeStruct *ts)
{
    if (ts) {
	TypeStruct::Type tstype = ts->type();
	switch (tstype) {
	    case TypeStruct::enumeration: case TypeStruct::boolean:
	    case TypeStruct::logical: case TypeStruct::integer:
	    case TypeStruct::real: case TypeStruct::number:
	    case TypeStruct::binary: case TypeStruct::string:
	    case TypeStruct::entity: case TypeStruct::generic_entity: {
		return true;
	    }
	    case TypeStruct::defined: {
		Ident dtid = dynamic_cast<const TsDefined *>(ts)->basetype;
		return is_a_scalar (st, st[dtid]);
	    }
	    case TypeStruct::select: {
		auto sts = dynamic_cast<const TsSelect *>(ts);
		for (Ident tid: sts->types) {
		    if (is_a_scalar (st, st[tid])) { return true; }
		}
		return false;
	    }
	    default: break;
	}
    }
    return false;
}

// Check if a given "type" ('sp') is a specialisation of another one ('gen').
static bool is_specialised (Symtab &st, const TypeStruct *sp,
					const TypeStruct *gen)
{
    TypeStruct::Type sptype = sp->type(), gentype = gen->type();
    // Same type ==> no!
    //if (sptype == gentype) { return true; }
    if (sptype == TypeStruct::defined) {
	auto spdts = dynamic_cast<const TsDefined *>(sp);
	return is_specialised (st, st[spdts->basetype], gen);
    }
    switch (gentype) {
	case TypeStruct::generic: {
	    return is_a_scalar (st, sp);
	}
	case TypeStruct::logical: {
#if 0
	    TypeStruct *xs = deref_type (st, sp);
	    return xs != nullptr && xs->type() == TypeStruct::boolean;
#endif
	    return sp != nullptr && sp->type() == TypeStruct::boolean;
	}
	case TypeStruct::number: {
#if 0
	    TypeStruct *xs = deref_type (st, sp);
	    return xs != nullptr && (xs->type() == TypeStruct::integer ||
				     xs->type() == TypeStruct::real);
#endif
	    return sp != nullptr && (sp->type() == TypeStruct::integer ||
				     sp->type() == TypeStruct::real);
	}
	case TypeStruct::generic_entity: {
#if 0
	    TypeStruct *xs = deref_type (st, sp);
	    return xs != nullptr && xs->type() == TypeStruct::entity;
#endif
	    return sp != nullptr && sp->type() == TypeStruct::entity;
	}
	case TypeStruct::string: case TypeStruct::binary: {
	    if (sptype != gentype) { return false; }
	    auto gensts = dynamic_cast<const TsStringLike *>(gen);
	    if (gensts->size > 0) { return false; }
	    auto spsts = dynamic_cast<const TsStringLike *>(sp);
	    return spsts->size > 0;
	}
	case TypeStruct::enumeration: {
	    if (sptype != gentype) { return false; }
	    auto genents = dynamic_cast<const TsEnum *>(gen);
	    auto spents = dynamic_cast<const TsEnum *>(sp);
	    if (! genents->extensible) { return false; }
	    for (Ident bx = spents->basedon; bx > 0;) {
		auto ents = dynamic_cast<const TsEnum *>(st[bx]);
		if (! ents) {
		    throw runtime_error
			(to_string (spents->where()) + ": '" +
			 st.symname (bx) + "' is no ENUMERATION type.");
		}
		if (ents == genents) { return true; }
		bx = ents->basedon;
	    }
	    break;
	}
	case TypeStruct::defined: {
#if 0
	    return is_specialised (st, sp, deref_type (st, gen));
#endif
	    Ident btid = dynamic_cast<const TsDefined *>(gen)->basetype;
	    return is_specialised (st, sp, st[btid]);
	}
	case TypeStruct::aggregate: {
	    auto spbt = dynamic_cast<const TsAggregateLike *>(sp)->basetype;
	    if (! spbt) { return false; }
	    auto genbt = dynamic_cast<const TsAggregate *>(gen)->basetype;
	    return is_specialised (st, spbt, genbt);
	}
	case TypeStruct::array: {
	    if (sptype != gentype) { return false; }
	    auto spats = dynamic_cast<const TsArray2 *>(sp);
	    auto genats = dynamic_cast<const TsArray2 *>(sp);
	    if (spats->ilwb == genats->ilwb && spats->iupb == genats->iupb) {
		return is_specialised (st, spats->basetype, genats->basetype);
	    }
	    break;
	}
	case TypeStruct::list: case TypeStruct::bag: case TypeStruct::set: {
	    if (sptype != gentype) { return false; }
	    auto splbsts = dynamic_cast<const TsLBS *>(sp);
	    auto genlbsts = dynamic_cast<const TsLBS *>(gen);
	    if (genlbsts->min == 0 && genlbsts->max == 0) {
		if (splbsts->min > 0 || splbsts->max > 0) {
		    return is_specialised (st, splbsts->basetype,
					       genlbsts->basetype);
		}
	    }
	    if (splbsts->min == genlbsts->min
	    &&  splbsts->max == genlbsts->max) {
		return is_specialised (st, splbsts->basetype,
					   genlbsts->basetype);
	    }
	    break;
	}
	default: break;
    }
    return false;
}

static bool is_specialised (Symtab &st, shared_ptr<TypeStruct> sp,
					shared_ptr<TypeStruct> gen)
{
    return is_specialised (st, sp.get(), gen.get());
}

// Data structures and special functions required for collecting the (explicit)
// attributes in preparation for creating the "flattened" attribute lists.

// Data structure for collecting the elementary data of a single attribute.
struct EAColl {
    shared_ptr<TypeStruct> def;
    Expr *x;
    Ident entid, attid;
    bool optional, derived;
};

// A 'std::vector<EAColl>' for collecting the elementary data of all explicit
// attributes of a single ENTITY.
using ColList = vector<EAColl>;

// For a faster access, we need
// a) a "comparison value",
struct EntAttId { Ident entid, attid; };

// b) a comparison function(-class),
struct EntAttIdLess : binary_function<EntAttId, EntAttId, bool> {
    bool operator() (EntAttId l, EntAttId r) const
    {
	if (l.entid == r.entid) { return l.attid < r.attid; }
	return l.entid < r.entid;
    }
};

// c) a mapping between the "comparison value" and the index it in the
//    'ColList'.
using AttPos = map<EntAttId, size_t, EntAttIdLess>;

// My convenient 'has()'-function, which is more clear than the stupid
// '<collection>.count (<key>) > 0;'.
static bool has (const AttPos &p, const EntAttId &eaid)
{
    return p.count (eaid) > 0;
}

// Collect all attributes of a given ENTITY (and all of its SUPERTYPEs) in a
// 'ColList' and then modifying the entries which were redeclared by less deep
// ENTITY attribute declarations. Instead of a function value, an "accumulator"
// pair 'coll'/'p' is used here.
static void collect_attributes (Symtab &st, Ident eid, ColList &coll, AttPos &p)
{
    TypeStruct *x = st[eid];
    if (! x) {
	// No symbol table entry => ERROR
	throw runtime_error ("FATAL! Undefined '" + st.symname (eid) +"'");
    }
    auto ts = dynamic_cast<TsEntity *>(x);
    if (! ts) {
	// Symbol table entry is not an ENTITY => ERROR
	throw runtime_error (to_string (x->where()) + ": " + st.symname (eid) +
			     " is not an ENTITY");
    }

    // First step: Recursively collect all explicit (non-redeclared)
    //   attributes from the ENTITY's SUPERTYPEs.
    for (Ident stid: ts->mysupertypes) {
	collect_attributes (st, stid, coll, p);
    }

    // Second step: collect all explicit attributes of the given directly
    // declared in the given ENTITY.
    for (Ident aid: ts->attrOrder) {
	auto attr = dynamic_cast<ExplicitAttr *>(ts->attributes[aid]);
	// No DERIVE or INVERSE attributes and no redeclared ones.
	if (attr != nullptr && attr->refname == 0) {
	    p.insert (make_pair (EntAttId { eid, aid }, coll.size()));
	    coll.push_back (EAColl { attr->definition, nullptr, eid, aid,
				     attr->optional, false });
	}
    }

    // Now, force the re-declarations to take place in the collected list.
    // This means that some of the (former explicit) attributes can become
    // DERIVE attributes and also that the typedefs or "optional" flags may
    // change.
    for (Ident aid: ts->attrOrder) {
	Attr *attr = ts->attributes[aid];
	Attr::Type attrtype = attr->type();
	Ident refid = attr->refname;
	// Only explicit and DERIVE attribute re-declarations are examined.
	if (attrtype != Attr::inverse_attr && attr->refname != 0) {
	    Ident refent = attr->baseentity;
	    TypeStruct *x = st[refent];
	    if (! x) {
		throw runtime_error
		    (to_string (ts->where()) + "(" + st.symname (aid) +
		     "): FATAL! Referencing an unknown identifier '" +
		     st.symname (refent) + ")");
	    }
	    EntAttId px { refent, refid };
	    if (! has (p, px)) {
		// The redeclaration referenced an ENTITY which is not a
		// SUPERTYPE of the current one or it has no attribute which
		// matches that in the redeclaration.
		throw runtime_error
		    (to_string (ts->where()) + "(" + st.symname (aid) +
		     "): Either '" + st.symname (refent) +
		     "' is not a SUPERTYPE of '" + st.symname (eid) +
		     "' or it has no attribute '" + st.symname (refid) + "'");
	    }
	    // Retrieve the Index of the attribute to be modified in the
	    // collection list.
	    size_t ix = p[px];
	    // Avoid too much indexing by defining a reference on it.
	    // ('ce' is the 'coll'-entry of the "referenced" attribute.
	    EAColl &ce = coll[ix];
	    switch (attrtype) {
		case Attr::explicit_attr: {
		    if (ce.derived) {
			// Can't change an DERIVE attribute back into an
			// explicit one (...)
			throw runtime_error
			    (to_string (ts->where()) + "(" + st.symname (aid) +
			     "): Can't make a DERIVE attribute explicit.");
		    }
		    // Make the 'explicit attr' data visible.
		    auto ea = dynamic_cast<ExplicitAttr *>(attr);
		    // Probably changed the 'optional' flag
		    ce.optional = ea->optional;
		    // If the redeclaration contains a specialised version of
		    // the type of the original attribute, the original
		    // attribute gets the type specified in the redeclaration.
		    if (is_specialised (st, ea->definition, ce.def)) {
			ce.def = ea->definition;
		    }
		    // The redeclaration is an explicit attribute, so there is
		    // no value to be set.
		    ce.x = nullptr;
		    break;
		}
		case Attr::derived_attr: {
		    // Need to test if the referenced attribute is an explicit
		    // one, and then to copy the definition data into 'ce'.
		    auto da = dynamic_cast<DerivedAttr *>(attr);
		    // Can a DERIVE attribute be redeclared as a DERIVE
		    // attribute? I'm (currently) assuming this is the case,
		    // here.
		    ce.optional = false; ce.derived = true;
		    // If the redeclaration contains a specialised version of
		    // the type of the original attribute, the original
		    // attribute gets the type specified in the redeclaration.
		    if (is_specialised (st, da->definition, ce.def)) {
			ce.def = da->definition;
		    }
		    // The redeclaration is a derived attribute, so the its
		    // value must become the value of the original attribute.
		    //##ALT 1:
		    //if (ce.x) { delete ce.x; }
		    //ce.x = da->value->Clone();
		    //##ALT 2:
		    ce.x = da->value;
		    break;
		}
		// Avoid needless compiler warnings, because of the used 'enum'
		// not being an 'enum class', and so always been seen as "not
		// completely exhausted".
		default: break;
	    }
	}
    }
}

// Helper functions for 'complete_supersubtypes()' (see below).
static bool fix_subtypes (Symtab &st, Ident suptid, Ident id)
{
    auto suptts = dynamic_cast<TsEntity *>(st[suptid]);
    if (! suptts) { return false; }
    for (auto subtid: suptts->mysubtypes) {
	if (subtid == id) { return true; }
    }
    suptts->mysubtypes.push (id);
    return true;
}

static bool fix_supertypes (Symtab &st, Ident subtid, Ident id)
{
    auto subtts = dynamic_cast<TsEntity *>(st[subtid]);
    if (! subtts) { return false; }
    for (auto suptid: subtts->mysupertypes) {
	if (suptid == id) { return true; }
    }
    subtts->mysupertypes.push (id);
    return true;
}

// Need this as the SUPERTYPE and/or SUBTYPE lists are not always completely
// specified in the IFC specs. This is a BAD HACK, because the real
// SUB/SUPERTYPE relationships are far more complex in EXPRESS. But until i
// understood them completely, i stick to the classical object-oriented
// "multiple subtypes"/"multiple supertypes" relationships.
//
bool complete_supersubtypes (Symtab &st)
{
    bool done = true;
    for (Ident id = st.first(); id <= st.last(); ++id) {
	auto ets = dynamic_cast<TsEntity *>(st[id]);
	if (ets) {
	    for (Ident suptid: ets->mysupertypes) {
		if (! fix_subtypes (st, suptid, id)) { done = false; }
	    }
	    for (Ident subtid: ets->mysubtypes) {
		if (! fix_supertypes (st, subtid, id)) { done = false; }
	    }
	}
    }
    return done;
}

static bool flatten_entity (Symtab &st, Ident eid, TsEntity *ets)
{
    // "Import" the 'supertypes'-list of 'ts' into the local scope
    // (by aliasing it)
    //const string &ename = st.symname (eid);
    //IdentList &supertypes = ets->mysupertypes;

    ColList coll; AttPos attpos;

    // Don't try an already flattened TsEntity entry
    if (ets->flattened) { return true; }

    collect_attributes (st, eid, coll, attpos);

    for (EAColl &ec: coll) {
	Ident aid = ec.attid;
	Attr *na = nullptr;
	if (ec.derived) {
	    auto da = new DerivedAttr;
	    da->definition = ec.def;
	    //##ALT 1:
	    //da->value = ec.x;
	    //##ALT 2:
	    da->value = ec.x->Clone();
	    na = da;
	} else {
	    auto ea = new ExplicitAttr;
	    ea->definition = ec.def;
	    ea->optional = ec.optional;
	    na = ea;
	}
	na->refname = 0; na->baseentity = ec.entid; // ... = eid???
	ets->flatAttributes.push (FlatAttr { ec.entid, aid, na });
    }

    // Add the list of subtypes to the new attribute 'coll_subtypes'.
    // This part must be removed and replaced by something better when
    // subtype expressions are enabled.
    collect_subtypes (ets->mysubtypes, ets);

    // Mark the entity as "flattened"
    ets->flattened = true;

    // Return "true", indicating a successful operation ...
    return true;
}

static bool flatten_select (Symtab &st, Ident id, TsSelect *ts)
{
    if (ts->flattened) { return true; }

    const string &name = st.symname (id);
    IdentList newList, tmpList;
    IdentBitSet typeIds;

    // First step: integrate the type-values of the SELECT-type, the current
    // SELECT-type is an extension of, into this current type.
    if (ts->basetype) {	// Ident-value != 0 means: existing/valid value
	Ident btid = ts->basetype;
	const string &btname = st.symname (btid);
	TsSelect *btts = dynamic_cast<TsSelect *>(st[btid]);
	if (! btts) {
	    throw runtime_error
		("INTERNAL ERROR! Basetype '" + btname + "' of '" + name +
		 "' is itself not a SELECT-type.");
	}
	if (! btts->isextensible) {
	    // ERROR! The base SELECT-type is not extensible!
	    return false;
	}
	if (! flatten_select (st, btid, btts)) { return false; }
	for (Ident typeId: btts->flatTypes) {
	    if (typeIds.insert (typeId)) { tmpList.push (typeId); }
	}
    }
    for (Ident typeId: ts->types) {
	if (typeIds.insert (typeId)) { tmpList.push (typeId); }
    }

    // Now, we have 'tmpList', which contains all types of the "extended"
    // SELECT-type and a an IdentBitSet 'typeIds', which contains bits set
    // for each of the types in 'tmpList'.
    // In the next step, each element of 'tmpList' must be tested for being
    // a SELECT-type itself. This type's elements must (after flattening the
    // type) again included into the list. Any "defined" type must be resolved
    // and if it leads to a SELECT-type, being replaced by this type...

    typeIds.clear();
    while (! tmpList.empty()) {
	Ident eTypeId = tmpList.shift();
	TypeStruct *ts1 = st[eTypeId];
	if (! ts1) {
	    // Don't know if this is a FATAL error or a standard error (yet).
	    return false;
	}
	Ident btId = eTypeId;
	while (ts1->type() == TypeStruct::defined) {
	    btId = dynamic_cast<TsDefined *>(ts1)->basetype;
	    ts1 = st[btId];
	}
	if (ts1->type() != TypeStruct::select) {
	    if (typeIds.insert (eTypeId)) {
		newList.push (eTypeId);
	    }
	} else {
	    eTypeId = btId;
	    auto etts = dynamic_cast<TsSelect *>(ts1);
	    if (! flatten_select (st, eTypeId, etts)) { return false; }
	    for (Ident eTypeId: etts->flatTypes) {
		if (typeIds.insert (eTypeId)) {
		    newList.push (eTypeId);
		}
	    }
	}
    }
    ts->flatTypes = move(newList);
    ts->flattened = true;
    return true;
}
