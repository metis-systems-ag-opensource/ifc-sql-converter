/* ifc-conf.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: runkharr@googlemail.com
** Copyright: (c) 2021, Boris Jakubith <runkharr@googlemail.com>
** License: GNU General Public License, version 2
**
** Get the configuration values either from the IFC-Reader (step.conf) or
** from the IFC-EXPRESS-Parser (express.conf)
**
*/

#include <iostream>
#include <map>
#include <stdexcept>
#include <string>

#include "Common/config.h"
#include "Common/confsp.h"
#include "Common/sutil.h"
#include "Common/sysutils.h"

using std::cerr, std::cout, std::endl;
using std::map;
using std::exception, std::runtime_error;
using std::string, std::operator""s, std::to_string;

[[noreturn]] void usage()
{
    const string prog = appname();
    string optmsg = "Options/Arguments:" EOL
		    "  -h (alt: --help)" EOL
		    "    Writes this usage message to the standard error"
		    " stream and terminates." EOL
		    "  --has `item-name`" EOL
		    "    Returns true (0) if the item `item-name` exists and"
		    " false (1), otherwise." EOL
		    "  -p (alt: --path, --pathname)" EOL
		    "    Prints the pathname of the configuration file." EOL
		    "  -l (alt: --list, --list-items" EOL
		    "    Lists all existing configuration items." EOL
		    "  `item-name` ..." EOL
		    "    Prints the values of the selected items. If only a"
		    " single item is specified," EOL
		    "    only the value of this item is printed. If the given"
		    " item doesn't exist," EOL
		    "    '" + prog + "' terminates with a non-zero exit-code."
		    EOL
		    "    If more than one item was specified, for each"
		    " existing item, a line in the" EOL
		    "    format `item-name`<blank><item-value> is printed. If"
		    " any specified item does" EOL
		    "    not exist, an error message concerning this item is"
		    " written to the standard" EOL
		    "    error stream, and '" + prog + "' terminates with a"
		    " non-zero exit-code.";
    if (is_prefix ("xpp", prog) || is_prefix ("express", prog) ||
	is_prefix ("stp", prog) || is_prefix ("step", prog)) {
	cerr << "Usage: " << prog << " -h|--help" << endl <<
		"       " << prog << " -p|--path|--pathname" << endl <<
		"       " << prog << " -l|--list|--list-items" << endl <<
		"       " << prog << " `item-name` ..." << endl <<
		"       " << prog << " -has `item-name`" << endl << endl <<
		optmsg << endl;
	exit (0);
    }
    cerr << "Usage: " << prog << " -h|--help" << endl <<
	    "       " << prog << " `tag` -p|--path|--pathname" << endl <<
	    "       " << prog << " `tag` -l|--list|--list-items" << endl <<
	    "       " << prog << " `tag` `item-name`..." << endl <<
	    "       " << prog << " `tag` --has `item-name`" << endl << endl <<
	    optmsg << endl <<
	    "  `tag`" << endl <<
	    "    Select the configuration file of either 'express' (`tag` =="
	    " 'xpp'|'express')" << endl <<
	    "    or 'step' (`tag` == 'stp'|'step')" << endl <<
	    endl;
    exit (0);
}

[[noreturn]] void usage (const string &msg)
{
    if (msg.empty()) { usage(); }
    cerr << appname() << ": " << msg << endl;
    exit (64);
}

#if 0
static
string tagnames (const map<string, string> m)
{
    string res;
    bool first = true;
    res.reserve (128);
    for (auto &[k, v]: m) {
	if (first) { first = false; } else { res += ", "; }
	res += "`" + k + "`";
    }
    res.shrink_to_fit();
    return res;
}
#endif

static
Config readconf (const string &confname)
{
    auto [path, found] = findFile (confname);
    if (! found) {
	throw runtime_error
	    ("No configuration named \"" + confname + "\" found.");
    }
    Config cfg;
    cfg.load (path);
    return cfg;
}

int main (int argc, char *argv[])
{
    const string prog = appname();
    string arg, tag;

    int optx = 1;

    // Map which associates a `tag` with a configuration name
    const std::map<string, string> tag2conf {
	{ "xpp", "express" }, { "express", "express" },
	{ "stp", "step" }, { "step", "step" },
    };

    cerr.setf (std::ios::boolalpha);

    if (optx >= argc) {
	usage ("Missing argument(s); Try '" + prog + " -h', please!");
    }

    arg = argv[optx];
    if (arg == "-h" || arg == "--help") { usage(); }

    if (is_prefix ("xpp", prog) || is_prefix ("express", prog)) {
	tag = "xpp";
    } else if (is_prefix ("stp", prog) || is_prefix ("step", prog)) {
	tag = "stp";
    } else {
	++optx; tag = arg;
    }

    auto it = tag2conf.find (tag);
    if (it == tag2conf.end()) {
	usage ("Invalid `tag` (" + tag + "). Try '" + prog +
	       " -h' for help. please!");
    }

    const string &confname = it->second;

    Config cfg;
    try {
	cfg = readconf (confname);
    } catch (exception &e) {
	cerr << prog << ": Reading `" << tag <<
		"` configuration failed - " << e.what() << endl;
	return 1;
    }

    if (optx >= argc) { usage ("Missing argument(s)."); }

    arg = argv[optx++];
    if (arg == "-p" || arg == "--path" || arg == "--pathname") {
	cout << cfg.filename() << endl; return 0;
    }

    if (arg == "-l" || arg == "--list" || arg == "--list-items") {
	auto cfitems = cfg.item_names();
	bool first = true;
	for (auto &ci: cfitems) {
	    if (first) { first = false; } else { cout << " "; }
	    cout << ci;
	}
	cout << endl; return 0;
    }

    if (arg == "--has") {
	if (optx >= argc) { usage ("'--has' requires an argument."); }
	string item = argv[optx];
	for (auto &it: cfg.item_names()) {
	    if (lccmp (it, item) == 0) { return 0; }
	}
	return 1;
    }

    map<string, string> tagtrans;
    for (auto &ci: cfg.item_names()) {
	tagtrans.emplace (lowercase (ci), ci);
    }

    unsigned ec = 0;
    // Special mode if only one `item-name` was given.
    auto kv = tagtrans.find (lowercase (arg));
    if (kv == tagtrans.end()) {
	if (optx < argc) {
	    cerr << prog << ": '" << kv->second << "' - no such item." << endl;
	}
	++ec;
    } else {
	if (optx < argc) { cout << kv->second << " "; }
	cout << cfg[kv->second] << endl;
    }

    // Handle the remaining `item-name` arguments.
    for (int ix = optx; ix < argc; ++ix) {
	arg = argv[ix];
	kv = tagtrans.find (lowercase (arg));
	if (kv == tagtrans.end()) {
	    cerr << prog << ": '" << kv->second << "' - no such item." <<
		    endl;
	    ++ec;
	} else {
	    cout << kv->second << " " << cfg[kv->second] << endl;
	}
    }
    return (ec > 0 ? 1 : 0);
}
