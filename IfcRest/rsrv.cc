/* rsrv.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** REST Server ...
**
*/

//#include <cstring>
//#include <algorithm>
#include <exception>
#include <filesystem>
#include <pistache/net.h>
#include <pistache/http.h>
#include <pistache/peer.h>
#include <pistache/http_headers.h>
#include <pistache/cookie.h>
#include <pistache/description.h>
#include <pistache/endpoint.h>
#include <pistache/common.h>
//#include <pistache/stream.h>
#include <pistache/thirdparty/serializer/rapidjson.h>
//#include <string>

#include "Common/fifo.h"
#include "Common/fnutil.h"
#include "Common/pathcat.h"
#include "Common/sutil.h"

#include "readconf.h"
#include "readqueue.h"
#include "itemname.h"
#include "tofile.h"

namespace fs = std::filesystem;
using namespace std;
//using std::ends;
//using std::string, std::operator""s, std::stoi;
using namespace Pistache;

using QW::QueueItem, QW::readqueue, QW::itemname, QW::ItemStatus;
using QW::update_item, QW::as_cstring, QW::change_itemstatus, QW::findItem;

#define DEFAULTCFG "ifcrest"

namespace Rsrv {

void handleReady(const Rest::Request &req, Http::ResponseWriter rsp)
{
    rsp.send (Http::Code::Ok, "1");
}

struct IfcRest {
    explicit IfcRest (Address addr)
      : httpEndpoint(std::make_shared<Http::Endpoint>(addr)),
	desc("IFC REST Server", "0.50")
    { }

    void init (const Config &_cfg, size_t nthr = 2)
    {
	auto opts = Http::Endpoint::options().threads (static_cast<int>(nthr));
	httpEndpoint->init (opts);
	cfg = _cfg;
	createDescription();
    }

    void start()
    {
	router.initFromDescription (desc);

	Rest::Swagger swagger (desc);
	swagger
	    .uiPath ("/doc")
	    .uiDirectory ("/path/to/swagger-ui/dist")
	    .apiPath ("ifcrest-api.json")
	    .serializer (&Rest::Serializer::rapidJson)
	    .install (router);

	httpEndpoint->setHandler (router.handler());
	httpEndpoint->serve();
    }

private:
    void createDescription()
    {
	desc
	    .info()
	    .license ("GPL", "https://www.gnu.org/licenses/gpl-3.0");

	auto backendErrorResponse =
	    desc.response (Http::Code::Internal_Server_Error,
			   "An error occured in the backend");

	desc
	    .schemes (Rest::Scheme::Http)
	    .basePath ("/ifcrest")
	    .produces (MIME (Application, Json))
	    .consumes (MIME (Application, Json))
	;

	auto ifcPath = desc.path ("/ifcrest");

	ifcPath
	    .route (desc.post ("/upload/:name/:content"), "Upload an IFC file")
	    .bind (&IfcRest::upload, this)
	    .produces (MIME (Application, Json))
	    .consumes (MIME (Multipart, FormData), MIME (Text, Plain), MIME (Application, OctetStream))
	    .parameter<Rest::Type::String> ("name", "Name of the IFC file")
	    .parameter<Rest::Type::Binary> ("content", "IFC file data")
	    .response (Http::Code::Ok, "The Queue-Id")
	    .response (backendErrorResponse);
	;

	ifcPath
	    .route (desc.get ("/status/:id"),
		    "Current processing status of a given queue entry")
	    .bind (&IfcRest::status, this)
	    .produces (MIME (Application, Json))
	    .parameter<Rest::Type::String> ("id", "Id of the queue entry")
	    .response (Http::Code::Ok,
		       "The Status of the item and probably some error"
		       " messages")
	    .response (backendErrorResponse);

	ifcPath
	    .route (desc.get ("/list"), "List all current queue entries")
	    .bind (&IfcRest::list, this)
	    .produces (MIME (Application, Json))
	    .response (Http::Code::Ok, "List all current queue entries")
	    .response (backendErrorResponse);

	ifcPath
	    .route (desc.get ("/test"), "Testing the server")
	    .bind (&IfcRest::test, this)
	    .produces (MIME (Application, Json))
	    .response (Http::Code::Ok, "Server test successful")
	    .response (backendErrorResponse);
	    
	// Only a test!
    }

    void upload (const Rest::Request &req, Http::ResponseWriter rsp)
    {
	// Upload a file and put it into the queue to be processed by
	// 'QueueWorker'
	// DON'T KNOW if this works. I haven's seen a mechanism for cleanly
	// transferring the data of huge files without braking 'Pistache'
	string filename = req.param (":name").as<string>();
	string content = req.param (":content").as<string>();
	string ifcpath = cfg["IFCFILES"] / filename;
	try {
	    to_file (req.param (":content").as<string>(), ifcpath);
	} catch (std::exception &e) {
	    rsp.send (Http::Code::Internal_Server_Error, e.what());
	    return;
	}
	string itempath = gen_itempath (cfg["WORKDIR"],
					as_cstring (ItemStatus::wip));
	QueueItem newItem { itempath, ifcpath, 0, Fifo<string>(), 0,
				ItemStatus::wip };
	update_item (newItem);
	change_itemstatus (newItem, ItemStatus::inQueue);
	rsp.send (Http::Code::Ok, itemname (newItem));
    }

    void status (const Rest::Request &req, Http::ResponseWriter rsp)
    {
	// Returnse the current processing status of a queue item identified
	// by the 'id' parameter
	auto itemid = req.param(":id").as<string>();
	string workdir = cfg["WORKDIR"];
	auto item = findItem (workdir / itemid + ".");
	if (item) {
	    // Construct a result
//	    auto rstream = rsp.stream (Http::Code::Ok);
	    ItemStatus st = item->status;
	    switch (st) {
		case ItemStatus::done:
		case ItemStatus::failed:
		case ItemStatus::err: {
		    auto s = json_string (as_cstring (st), item->messages);
		    rsp.send (Http::Code::Ok, s);
//		    for (string &msg: item->messages) {
//			//rstream << msg;
//			rstream << EOL;
//		    }
//		    rstream.ends();
		    break;
		}
		case ItemStatus::inQueue:
		    rsp.send (Http::Code::Ok, as_cstring (st));
		default:
		    rsp.send (Http::Code::Internal_Server_Error,
			      "Invalid status of '" + itemid);
		    break;
	    }
	} else {
	    rsp.send (Http::Code::Not_Found,
		       "\""s + itemid + "\" not found.");
	}
    }

    void list (const Rest::Request &req, Http::ResponseWriter rsp)
    {
	// Send the current configuration to the sender 'rsp'.
	string wd = cfg["WORKDIR"];
	rsp.send (Http::Code::Ok,
		  json_string (readqueue (wd, ItemStatus::all)));
    }

    void test (const Rest::Request &req, Http::ResponseWriter rsp)
    {
	// Sends the configuration data to the requester using the
	// ResponseWriter 'rsp'
	rsp.send (Http::Code::Ok, json_string (cfg));
    }

    Config cfg;
    std::shared_ptr<Http::Endpoint> httpEndpoint;
    Rest::Description desc;
    Rest::Router router;

} /*IfcRest*/;

} /*namespace Rsrv*/



int main (int argc, char *argv[])
{
    Config cfg;
    string prog (basename (*argv)), cfname;
    if (argc > 1) {
	string arg = argv[1];
	if (arg == "-h" || arg == "--help") {
	    cerr << "Usage: " << prog << " [confname/-file]" EOL
		    "       " << prog << " -h/--help  # writing this message"
		    " to stderr" EOL << std::flush;
	    return 0;
	}
	cfname = std::move (arg);
    } else {
	cfname = DEFAULTCFG;
    }
    try {
	cfg = std::move (readconf (cfname));
    } catch (ConfigError &e) {
	cerr << prog << " configuration error: " << e.what() << std::endl;
	return 0;
    }
    Port port(stoi (cfg["PORTNUM"]));
    int thr = 2;
    Address addr(Ipv4::any(), port);

    cout << "Cores = " << hardware_concurrency() << std::endl;
    cout << "Using " << thr << " threads" << std::endl;

    Rsrv::IfcRest server(addr);
    server.init (cfg, thr);
    server.start();
}
