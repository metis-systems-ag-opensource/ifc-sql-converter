/* itemname.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of `itemname.{cc,h}`.
** (Generating the pathname of a 'QueueItem' file.)
**
*/
#ifndef ITEMNAME_H
#define ITEMNAME_H

#include <string>

std::string gen_itempath (const std::string &workdir,
			  const std::string &suffix);

#endif /*ITEMNAME_H*/
