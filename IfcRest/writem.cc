/* writem.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Low-level (system dependent) backend of `update_item()` from
** `readqueue.{cc,h}`
**
*/

#include <cstdio>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <string>

#include "lockopen.h"

#include "writem.h"

namespace QW {

using std::operator""s;
using std::runtime_error;

void write_itemdata (const QueueItem &qit, const char *filename)
{
    char buf[30];
    FILE *fp = lock_fopen (filename, "r+b");
    if (! fp) {
	throw runtime_error ("Attempt to open \""s + filename + "\" failed - " +
			     strerror (errno));
    }
    fseek (fp, 0, SEEK_SET);
    fwrite (qit.ifcfile.c_str(), qit.ifcfile.size(), 1, fp); fputc ('\0', fp);
    fwrite (qit.fileid.c_str(), qit.fileid.size(), 1, fp); fputc ('\0', fp);
    snprintf (buf, sizeof(buf), "%ld", (long) qit.modelid);
    fwrite (buf, strlen (buf), 1, fp); fputc ('\0', fp);
    for (auto &msg: qit.messages) {
	fwrite (msg.c_str(), msg.size(), 1, fp); fputc ('\0', fp);
    }
    truncate_here (fp);
    fclose (fp);
}

} /*namespace QW*/
