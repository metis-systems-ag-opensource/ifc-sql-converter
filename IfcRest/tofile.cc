/* tofile.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Smann utility function which writes a string to a file ...
**
*/

#include <iostream>
#include <filesystem>
#include <fstream>
#include <stdexcept>

#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include "readqueue.h"

#include "tofile.h"

namespace fs = std::filesystem;
using std::runtime_error;
using std::ofstream, std::flush;
using std::string;

void to_file (const string &data, const string &file)
{
    if (fs::exists (file)) {
	throw runtime_error ("File \"" + file + "\" already exists.");
    }

    ofstream fout(file);
    if (! fout.is_open()) {
	throw runtime_error ("Failed to create \"" + file + "\".");
    }

    fout << data << flush;
    fout.close();
}

using namespace rapidjson;

string json_string (const string status, const Fifo<string> &messages)
{
    StringBuffer s;
    Writer<StringBuffer> w(s);

std::cerr << "##A0: Starting item status output\n";
    w.StartObject();
    w.Key ("status"); w.String (status.c_str());
    w.Key ("messages");
    w.StartArray();
    for (auto &msg: messages) {
	w.String (msg.c_str());
    }
    w.EndArray();
    w.EndObject();
std::cerr << "##A1: Item status output complete\n";
    return s.GetString();
}

string json_string (const QW::Queue &rq)
{
    StringBuffer s;
    Writer<StringBuffer> w(s);

std::cerr << "##B0: Starting queue output\n";
    w.StartArray();
    for (auto &qit: rq) {
	w.StartObject();
	w.Key ("id"); w.String (itemname (qit).c_str());
	w.Key ("status"); w.String (QW::as_cstring (qit.status));
	w.EndObject();
    }
    w.EndArray();
std::cerr << "##B0: Queue output complete\n";
    return s.GetString();
}

string json_string (const Config &cfg)
{
    StringBuffer s;
    Writer<StringBuffer> w(s);

std::cerr << "##C0: Starting config output\n";
    w.StartObject();
    w.Key ("config");
    w.StartArray();
    for (auto &[name, value]: cfg.data()) {
	w.StartObject();
	w.Key (name.c_str());
	w.String (value.c_str());
	w.EndObject();
    }
    w.EndArray();
    w.EndObject();
std::cerr << "##C0: Config output complete\n";
    return s.GetString();
}
