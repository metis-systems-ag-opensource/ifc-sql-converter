/* lockopen.c
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** C function for open and locking a file
**
*/


//#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/types.h>

#include "lockopen.h"

/* Using advisory locking here! */

FILE *lock_fopen (const char *filename, const char *mode)
{
    FILE *res = NULL;
    struct flock lock;
    int fd, omode = 0, lm = 0;
    switch (*mode) {
	case 'a': /* Append mode! */
	    omode = O_CREAT|O_APPEND|(mode[1] == '+' ? O_RDWR : O_WRONLY);
	    lm = F_WRLCK;
	    break;
	case 'r': /* Read mode! */
	    omode = (mode[1] == '+' ? O_RDWR : O_RDONLY);
	    lm = F_RDLCK;
	    break;
	case 'w': /* Write mode! */
	    omode = O_CREAT|(mode[1] == '+' ? O_RDWR : O_WRONLY);
	    lm = F_WRLCK;
	    break;
	default:
	    errno = EINVAL; return res;
    }

    fd = open (filename, omode, 0666);
    if (fd < 0) { return res; }
    lock.l_type = lm;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 0;
    if (fcntl (fd, F_SETLKW, &lock) < 0) { close (fd); return res; }
    if (! (res = fdopen (fd, mode))) { close (fd); }
    return res;
}

int truncate_here (FILE *fp)
{
    off_t pos;
    fflush (fp);
    pos = lseek (fileno (fp), 0, SEEK_CUR);
    if (pos < 0) { return -1; }
    return ftruncate (fileno (fp), pos);
}
