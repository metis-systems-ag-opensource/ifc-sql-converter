/* readqueue.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Read the descriptions of files from a "queue" directiory
**
*/

#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <regex>
#include <stdexcept>
#include <utility>

#include "Common/fnutil.h"
#include "Common/sutil.h"

#include "writem.h"

#include "readqueue.h"

#if 0 //Definitions from the header file...
enum class ItemStatus : uint8_t {
    err = 0,
    inQueue = 1,
    wip = 2,
    done = 4,
    failed = 8,
    processed = done | failed,
    all = inQueue | wip | done | failed,
};
struct QueueItem {
    std::string filename, ifcfile, fileid;
    Fifo<std::string> messages;
    size_t modelid;
    ItemStatus status;
};
using Queue = std::vector<QueueItem>;
#endif

namespace QW {

namespace fs = std::filesystem;
namespace regex_constants = std::regex_constants;
namespace regex_constants = std::regex_constants;
using std::ifstream, std::istream, std::ios_base;
using std::cerr, std::cout, std::endl, std::flush;
using std::exception, std::runtime_error;
using std::optional;
using std::regex, std::regex_match;
using std::string, std::operator""s, std::to_string;
using std::move, std::swap, std::pair, std::make_pair;
using std::vector;

/*! Conversion of each element of 'ItemStatus' into a C-string (`const char *`).
**
**  @param s - the `ItemStatus` element to be converted
**  @returns the item name in the same format as used in the program text
**           (e.g., `"ItemStatus::inQueue"` for `ItemStatus::inQueue`).
**
**  This function is solely used for generating expressive error messages.
*/
const char *as_cstring (ItemStatus s)
{
    switch (s) {
	case ItemStatus::err:       return "ItemStatus::err";
	case ItemStatus::inQueue:   return "ItemStatus::inQueue";
	case ItemStatus::wip:       return "ItemStatus::wip";
	case ItemStatus::done:      return "ItemStatus::done";
	case ItemStatus::failed:    return "ItemStatus::failed";
	case ItemStatus::processed: return "ItemStatus::processed";
	case ItemStatus::all:       return "ItemStatus::all";
	default:                    return "ItemStatus::err";
    }
}

/*! Conversion of an `ItemStatus` element into a regular expression string used
**  for the recognition of the suffix in a queue element (filename).
*/ 
static const char *regSuffix (ItemStatus s)
{
    switch (s) {
	case ItemStatus::inQueue:   return "INQUEUE";
	case ItemStatus::wip:       return "WIP";
	case ItemStatus::done:      return "DONE";
	case ItemStatus::failed:    return "FAILED";
	case ItemStatus::processed: return "(DONE|FAILED)";
	case ItemStatus::all:       return "(INQUEUE|WIP|DONE|FAILED)";
	default:
	    throw runtime_error
		("Unconvertible status: '"s + as_cstring (s) + "'");
    }
}

static const char *toSuffix (ItemStatus s)
{
    switch (s) {
	case ItemStatus::inQueue: return ".INQUEUE";
	case ItemStatus::wip:     return ".WIP";
	case ItemStatus::done:    return ".DONE";
	case ItemStatus::failed:  return ".FAILED";
	default:
	    throw runtime_error ("'"s + as_cstring (s) + "' has no suffix.");
    }
}

/* Reading a NUL-terminated string from a (binary) input stream.
** @returns the string read.
** @param in - the input stream the string is read from.
**
** After this operation, the current read position is either the next
** character position after the terminating 'NUL' byte or the end of the
** input stream.
*/
static string get_bstring (istream &in)
{
    char ch;
    string t, res;
    while (! in.eof() && in.good()) {
	in.get (ch);
	if (ch == 0) { break; }
	t.push_back (ch);
    }
    t.shrink_to_fit();
    swap (t, res);
    return res;
}

/*! Extracting the `ItemStatus` element from the queue elements path name.
*/
static ItemStatus get_itemstatus (const string &fpath)
{
    string pstate = uppercase (suffix (fpath));
    if (pstate == "INQUEUE") { return ItemStatus::inQueue; }
    if (pstate == "WIP") { return ItemStatus::wip; }
    if (pstate == "DONE") { return ItemStatus::done; }
    if (pstate == "FAILED") { return ItemStatus::failed; }
    return ItemStatus::err;
}

/* Reading the the content of a queue element, and constructing the internal
** representation of this queue item (from the content and the filename of
** this element.
*/
QueueItem readstatus (const string &fpath)
{
    QueueItem res;
    ifstream in(fpath, ios_base::in | ios_base::binary);
    if (! in.is_open()) {
	int ec = errno; in.close(); errno = ec;
	throw runtime_error ("Opening \"" + fpath + "\" failed.");
    }
    if ((res.status = get_itemstatus (fpath)) == ItemStatus::err) {
	in.close();
	throw runtime_error ("Unrecognised status: " + suffix (fpath));
    }
    res.filename = fpath;
    res.ifcfile = get_bstring (in);
    if (! in.eof()) {
	string modelid;
	res.fileid = get_bstring (in);
	if (in.eof()) {
	    in.close();
	    throw runtime_error ("Missing model id in \"" + fpath + "\".");
	}
	modelid = get_bstring (in);
	try {
	    res.modelid = (size_t) stoul (modelid);
	} catch (exception &e) {
	    throw runtime_error ("Invalid model id in \"" + fpath + "\".");
	}
	while (! in.eof()) {
	    res.messages.push (get_bstring (in));
	}
    }
    in.close();
    return res;
}

optional<QueueItem> findItem (const string &fppfx)
{
    string fpdir = dirname (fppfx);
    string itempath;
    bool found = false;
    for (auto p: fs::directory_iterator (fpdir)) {
	itempath = p.path().string();
	if (is_prefix (fppfx, itempath)) { found = true; break; }
    }
    if (found) {
	return optional(readstatus (itempath));
    }
    return optional<QueueItem>();
}

/*! `less` function to be used in sorting the queue items.
*/
static bool path_lt (const string &l, const string &r)
{
    return basename (l) < basename (r);
}

/*! Reading the persistent queue (or part of it) into memory, returning its
**  internal representation.
*/
Queue readqueue (const string &dpath, const ItemStatus &s)
{
    Queue res;
    Fifo<string> path_list;	// Path list (unsorted)
    vector<string> path_sorted;	// Path list
    try {
	/* Collect the item files which have a specific status (indicated by
	** the corresponding suffixes).
	*/
	string fnfmt = "^[[:digit:]]{8}-[[:digit:]]{6}\\.[[:digit:]]{4}\\."s +
		       regSuffix (s) + "$";
	regex re (fnfmt, regex_constants::extended);
	for (auto file: fs::directory_iterator (dpath)) {
	    auto &fpath = file.path();
	    if (regex_match (fpath.filename().string(), re)) {
		path_list.push (fpath.string());
	    }
	}
    } catch (exception &e) {
	//## Need to think about this
	throw;
    }
    /* The collected list of items is currently unsorted. In order to get the
    ** same result in two contiguous invocations of `readqueue()`, the result
    ** must be sorted. This is done by moving the content into a
    ** `std::vector<string>` (`path_sorted`), and then sorting this array.
    ** This step is required, because a `Fifo<T>` doesn't have a mechanism for
    ** sorting its elements (it is simply not meant for accessing elements in
    ** a random order).
    */
    size_t num_entries = path_list.size();
    path_sorted.reserve (num_entries);
    while (! path_list.empty()) {
	path_sorted.push_back (path_list.shift());
    }
    sort (path_sorted.begin(), path_sorted.end(), path_lt);
    /* The result consists of the contents of the corresponding files, so these
    ** must be read here
    */
    //res.reserve (num_entries); // Only if `Queue` is a `std::vector<...>`!
    unsigned errs = 0;
    for (auto &path: path_sorted) {
	try {
	    //QueueItem qitem = readstatus (path);
	    res.push_back (readstatus (path));
	} catch (exception &e) {
	    cerr << "ERROR reading \"" << path << "\": " << e.what() << endl;
	    ++errs;
	}
    }
    /* Issue any errors as WARNINGs to the standard error output */
    if (errs > 0) {
	cerr << errs << " errors found in the queue." << endl;
	cerr << "The corresponding entries were ignored." << endl;
    }
    /* Return the `Queue` ... */
    return res;
}

/*! Changes the `ItemStatus` of a `QueueItem`.
**
**  @param `qit` - the queue item whose status is to be changed.
**  @param `newstat` - the new item status.
**
**  @returns the old status of the item.
*/
ItemStatus change_itemstatus (QueueItem &qit, ItemStatus newstat)
{
    ItemStatus old = qit.status;
    size_t sfx_start = qit.filename.find_last_of ('.');
    string newname = qit.filename.substr (0, sfx_start) + toSuffix (newstat);
    std::error_code ec;
    fs::rename (qit.filename, newname, ec);
    if (ec.value() != 0) {
	throw runtime_error ("Status change failed: " + ec.message());
    }
#if 0
    string tmp;
    swap (tmp, newname);
    qit.filename = tmp;
#else
    qit.filename = newname;
#endif
    return old;
}

/* Because i don't know for sure how to open a file exclusively in C++,
** i'm using functions from C. Additionally, this part is system dependent,
** so the real implementation is done in another file.
*/
void update_item (QueueItem &qit)
{
    write_itemdata (qit, qit.filename.c_str());
}

/* I didn't store the ID of the queue item directly, but because it's part of
** the 'filename' field, it can be retrieved in a rather simple way. This is
** exactly what is done inside of this function. Result is the ID of the
** queue item.
*/
string itemname (const QueueItem &qit)
{
    string bn = basename (qit.filename);
    size_t sx = bn.find_last_of ('.');
    if (sx < bn.size()) { bn.erase (sx); bn.shrink_to_fit(); }
    return bn;
}

} /*namespace QW*/
