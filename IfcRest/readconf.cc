/* readconf.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2020, Metis AG
** License: MIT License
**
** Read and check the configuration of the 'QueueWorker' program
**
*/

#include <set>
#include <exception>
#include <filesystem>

#include "Common/confsp.h"
#include "Common/sutil.h"

#include "readconf.h"

using std::runtime_error, std::exception;
using std::set;
using std::string, std::operator""s, std::stoul;

namespace fs = std::filesystem;


static string to_string (const set<string> &xs, string delim = ", ")
{
    bool first = true;
    string res;
    for (auto &x: xs) {
        if (first) { first = false; } else { res += delim; }
        res += x;
    }
    return res;
}

#if 0
static bool directory_exists (const string &pathname)
{
    if (! fs::exists (pathname)) { return false; }
    return fs::is_directory (pathname);
}
#endif


static bool is_no_directory (const string &pathname)
{
    if (! fs::exists (pathname)) { return true; }
    return ! fs::is_directory (pathname);
}

static bool is_program_file (const string &pathname)
{
    fs::perms p;
    if (! fs::exists (pathname)) { return false; }
    if (! fs::is_regular_file (pathname)) { return false; }
    p = fs::status (pathname).permissions();
    return (p & fs::perms::owner_exec) != fs::perms::none ||
	   (p & fs::perms::group_exec) != fs::perms::none ||
	   (p & fs::perms::others_exec) != fs::perms::none;
}

static bool is_a_portnum (const string &portnum)
{
    try {
	size_t ix, res;
	res = stoul (portnum, &ix);
	if (ix < portnum.size() || res >= 65536l) { return false; }
	return true;
    } catch (exception &e) {
	return false;
    }
}

#if 0
class ConfigError : public runtime_error {
public:
    ConfigError (const string &msg)
        : runtime_error (msg)
    { }
    string what() { return "Configuration ERROR: "s + runtime_error::what(); }
};
#endif

Config readconf (const string &cfgname)
{
    Config cfgdata;
    set<string> required { "WORKDIR", "IFCFILES", "PORTNUM" };
    auto [path, found] = findFile (cfgname);
    if (! found) {
	throw runtime_error ("No configuration '" + cfgname + "' found.");
    }
    cfgdata.load (path);
    if (auto [mf, missing] = cfgdata.check_missing (required); mf) {
	throw ConfigError ("Missing items: " + to_string (missing));
    }
    if (is_no_directory (cfgdata["WORKDIR"])) {
	throw ConfigError ("Invalid 'WORKDIR' value (no directory).");
    }
    if (is_no_directory (cfgdata["IFCFILES"])) {
	throw ConfigError ("Invalid 'IFCFILES' value (no directory).");
    }
    if (! is_a_portnum (cfgdata["PORTNUM"])) {
	throw ConfigError
	    ("Invalid 'PORTNUM' value (not numeric or out of range).");
    }
    return cfgdata;
}
