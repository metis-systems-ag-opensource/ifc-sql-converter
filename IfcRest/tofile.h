/* tofile.h
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Interface part of 'tofile.{cc,h}'
** (Small utility function which writes a string to a file ...)
**
*/
#ifndef TOFILE_H
#define TOFILE_H

#include <string>

#include "Common/config.h"
#include "Common/fifo.h"
#include "readqueue.h"

void to_file (const std::string &data, const std::string &file);

std::string json_string (const std::string status,
			 const Fifo<std::string> &messages);

std::string json_string (const QW::Queue &rq);

std::string json_string (const Config &cfg);

#endif /*TOFILE_H*/
