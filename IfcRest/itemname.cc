/* itemname.cc
**
** $Id$
**
** Author: Boris Jakubith
** E-Mail: b.jakubith@metis-ag.com
** Copyright: (c) 2021, Metis AG
** License: MIT License
**
** Generating the pathname of a 'QueueItem' file.
**
*/

#include <cerrno>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <glob.h>
#include <filesystem>
#include <system_error>

#include "Common/pathcat.h"

#include "itemname.h"

namespace fs = std::filesystem;
using std::string;
using std::system_category, std::system_error;

static string gen_itemprefix ()
{
    char buf[128];
    struct timespec ct;
    struct tm lt;

    if (clock_gettime (CLOCK_REALTIME, &ct)) {
	throw system_error (errno, system_category(), strerror (errno));
    }

    unsigned long sfrac = ct.tv_nsec / 100000;
    struct tm *ts = localtime_r (&ct.tv_sec, &lt);
    size_t len = strftime (buf, sizeof(buf), "%Y%m%d-%H%M%S", ts);
    snprintf (buf + len, sizeof(buf) - len, ".%04lu.", sfrac);

    return buf;
}

string gen_itempath (const string &workdir, const string &suffix)
{
    for (;;) {
	glob_t matches;
	string itempfx = gen_itemprefix();
	string pathprefix = workdir / itempfx;
	string patt = pathprefix + "*";
	int rc = glob (patt.c_str(), GLOB_ERR|GLOB_NOSORT, nullptr, &matches);
	globfree (&matches);
	switch (rc) {
	    case GLOB_NOSPACE:
		throw system_error
		    (ENOMEM, system_category(), strerror (ENOMEM));
	    case GLOB_ABORTED:
		throw system_error (EIO, system_category(), strerror (EIO));
	    case GLOB_NOMATCH:
		return pathprefix + suffix;
	    default:
		;
	}
    }
}
